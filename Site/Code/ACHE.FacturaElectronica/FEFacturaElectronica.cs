﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using ACHE.FacturaElectronica.Lib;
using ACHE.FacturaElectronica.WSFEV1;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using iTextSharp.text;
using ACHE.Extensions;
using ACHE.FacturaElectronicaModelo;

namespace ACHE.FacturaElectronica
{
    public class FEFacturaElectronica
    {
        private static object bloqueo = new object();

        /*public void GenerarComprobante(FEComprobante comprobante, string template)
        {
            GenerarComprobante(comprobante, ConfigurationManager.AppSettings["FE.wsaa"], ConfigurationManager.AppSettings["FE.CertificadoAFIP"], template);//ConfigurationManager.AppSettings["FE.Template"]);
        }*/

        static object lockLogXML = new object();

        public void GetArchivoXML(string xmlCAERequest, string xmlAuthRequest, string xmlCAEResponse, string entorno)
        {
            lock (lockLogXML)
            {
                string pathXML = ConfigurationManager.AppSettings["FE.PatLoghXML"];
                XmlDocument doc = new XmlDocument();
                XmlElement root;
                XmlNode xmlLOGS;
                //string entorno = ConfigurationManager.AppSettings["FE.wsfev1"].Contains("homo.afip.gov.ar") ? "TESTING" : "PRODUCCION";
                //string entorno = ConfigurationManager.AppSettings["FE.wsfev1"].Contains("homo.afip.gov.ar") ? "TESTING" : "PRODUCCION";

                if (!File.Exists(pathXML))
                {
                    XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-16", null);
                    root = doc.DocumentElement;
                    doc.InsertBefore(xmlDeclaration, root);

                    xmlLOGS = doc.CreateElement("LOGS");
                    doc.AppendChild(xmlLOGS);
                }
                else
                {
                    doc.Load(pathXML);
                    //root = doc.DocumentElement;
                    xmlLOGS = doc.GetElementsByTagName("LOGS")[0];
                }

                XmlElement xmlFECAERequest = doc.CreateElement("LogFECAERequest");
                xmlFECAERequest.SetAttribute("Entorno", entorno);
                xmlFECAERequest.InnerText = xmlCAERequest;
                xmlLOGS.AppendChild(xmlFECAERequest);

                XmlElement FEAuthRequest = doc.CreateElement("LogFEAuthRequest");
                FEAuthRequest.SetAttribute("Entorno", entorno);
                FEAuthRequest.InnerText = xmlAuthRequest;
                xmlLOGS.AppendChild(FEAuthRequest);

                XmlElement FECAEResponse = doc.CreateElement("LogFECAEResponse");
                FECAEResponse.SetAttribute("Entorno", entorno);
                FECAEResponse.InnerText = xmlCAEResponse;
                xmlLOGS.AppendChild(FECAEResponse);

                doc.Save(pathXML);
            }
        }

        private bool ValidarComprobante(FEComprobante comprobante, out string error)
        {
            List<FETipoComprobante> tiposCUITObligatorio = new List<FETipoComprobante>() { FETipoComprobante.FACTURAS_A, FETipoComprobante.NOTAS_CREDITO_A, FETipoComprobante.NOTAS_DEBITO_A };
            error = "";

            //var auxTotal = Math.Round(comprobante.ImpNeto + comprobante.TotalIva + +comprobante.ImpTotConc + comprobante.Tributos.Sum(x => x.Importe), 2);

            if (comprobante == null)
                error = "Error al validar el comprobante.";
            else if (comprobante.ImpTotal == 0)
                error = "No se puede generar una factura con importe 0.";
            else if (comprobante.Cuit == comprobante.DocNro)
                error = "El CUIT del emisor no puede ser el mismo cuit o documento de quien recibe la factura";
            else if (comprobante.TipoComprobante == FETipoComprobante.FACTURAS_C && comprobante.TotalIva != 0)
                error = "El Importe de IVA para comprobantes tipo C debe ser igual a cero (0).";
            //else if (comprobante.TipoComprobante != FETipoComprobante.FACTURAS_C && comprobante.ImpTotal != comprobante.ImpNeto + comprobante.TotalIva)
            //else if (comprobante.TipoComprobante != FETipoComprobante.FACTURAS_C && comprobante.ImpTotal != auxTotal)
            //    error = "El importe total no coincide con el neto + iva.";
            //else if (comprobante.TipoComprobante != FETipoComprobante.FACTURAS_C && comprobante.ImpNeto != 0 && comprobante.DetalleIva.Count == 0)
            //    error = "Si ImpNeto es mayor a 0 el IVA es obligatorio.";
            else if (tiposCUITObligatorio.Contains(comprobante.TipoComprobante) && comprobante.DocTipo != 80)
                error = "Para comprobantes tipo A, se debe informar el CUIT";// el campo DocTipo debe ser igual a 80 (CUIT)
            else if (comprobante.Concepto == FEConcepto.Producto && comprobante.Fecha < DateTime.Now.AddDays(-6))
                error = "La fecha de emisión del comprobante no puede ser anterior al " + DateTime.Now.AddDays(-6).ToString("dd/MM/yyyy");
            else if (comprobante.Concepto == FEConcepto.Producto && comprobante.Fecha > DateTime.Now.AddDays(6))
                error = "La fecha de emisión del comprobante no puede ser superior al " + DateTime.Now.AddDays(6).ToString("dd/MM/yyyy");
            else if (comprobante.Concepto != FEConcepto.Producto && comprobante.Fecha < DateTime.Now.AddDays(-11))
                error = "La fecha de emisión del comprobante no puede ser anterior al " + DateTime.Now.AddDays(-11).ToString("dd/MM/yyyy");
            else if (comprobante.Concepto != FEConcepto.Producto && comprobante.Fecha > DateTime.Now.AddDays(11))
                error = "La fecha de emisión del comprobante no puede ser superior al " + DateTime.Now.AddDays(11).ToString("dd/MM/yyyy");
            else if (comprobante.FchVtoPago.HasValue && comprobante.FchVtoPago.Value < comprobante.Fecha)
                error = "La fecha de vencimiento no puede ser menor a la fecha de emisión del comprobante";

            /* else if (comprobante.TipoComprobante != FETipoComprobante.FACTURAS_C && comprobante.DetalleIva.Sum(d => d.BaseImp) != comprobante.ImpNeto)
                 error = "La suma de los campos BaseImp en AlicIva debe ser igual al valor ingresado en ImpNeto.";
             */
            return error == "";
        }

        public FEComprobante GetComprobante(long cuit, long CbtNro, int PtoVta, FETipoComprobante tipoComprobante)
        {
            Service objWSFEV1 = new Service();
            FEAuthRequest objFEAuthRequest = new FEAuthRequest();
            var ticket = FEAutenticacion.GetTicket(cuit, "wsfe", /*urlWsaaWsdl, certificadoAfip,*/ "PROD");
            FEComprobante comprobante = new FEComprobante();
            FECompConsultaReq filtros = new FECompConsultaReq();

            objWSFEV1.Url = ConfigurationManager.AppSettings["FE.PROD.wsfev1"];

            objFEAuthRequest.Token = ticket.Token;
            objFEAuthRequest.Sign = ticket.Firma;
            objFEAuthRequest.Cuit = long.Parse(ticket.Cuit);

            filtros.CbteTipo = (int)tipoComprobante;
            filtros.CbteNro = CbtNro;
            filtros.PtoVta = PtoVta;
            FECompConsultaResponse respuesta = objWSFEV1.FECompConsultar(objFEAuthRequest, filtros);

            if (respuesta.Errors != null && respuesta.Errors.Length > 0)
                throw new Exception(respuesta.Errors[0].Msg);
            else
            {

                comprobante.CAE = respuesta.ResultGet.CodAutorizacion;
                comprobante.PtoVta = respuesta.ResultGet.PtoVta;
                comprobante.TipoComprobante = (FETipoComprobante)respuesta.ResultGet.CbteTipo;
                comprobante.NumeroComprobante = (int)respuesta.ResultGet.CbteDesde;
                comprobante.Concepto = (FEConcepto)respuesta.ResultGet.Concepto;
                comprobante.DocTipo = respuesta.ResultGet.DocTipo;
                comprobante.DocNro = respuesta.ResultGet.DocNro;
                comprobante.Fecha = DateTime.ParseExact(respuesta.ResultGet.CbteFch, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                //comprobante.ImpTotal = respuesta.ResultGet.ImpTotal;
                comprobante.ImpTotConc = respuesta.ResultGet.ImpTotConc;
                //comprobante.ImpNeto = respuesta.ResultGet.ImpNeto;
                comprobante.ImpOpEx = respuesta.ResultGet.ImpOpEx;
                comprobante.FchServDesde = respuesta.ResultGet.FchServDesde == string.Empty ? (DateTime?)null : DateTime.ParseExact(respuesta.ResultGet.FchServDesde, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                comprobante.FchServHasta = respuesta.ResultGet.FchServHasta == string.Empty ? (DateTime?)null : DateTime.ParseExact(respuesta.ResultGet.FchServHasta, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                comprobante.FchVtoPago = respuesta.ResultGet.FchVtoPago == string.Empty ? (DateTime?)null : DateTime.ParseExact(respuesta.ResultGet.FchVtoPago, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                comprobante.CodigoMoneda = respuesta.ResultGet.MonId;
                comprobante.CotizacionMoneda = respuesta.ResultGet.MonCotiz;
                comprobante.DocTipo = respuesta.ResultGet.DocTipo;
                comprobante.DocNro = respuesta.ResultGet.DocNro;

                comprobante.FechaVencCAE = DateTime.ParseExact(respuesta.ResultGet.FchVto, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                if (respuesta.ResultGet.Observaciones != null && respuesta.ResultGet.Observaciones.Length > 0)
                    comprobante.Observaciones = respuesta.ResultGet.Observaciones[0].Msg;
                else
                    comprobante.Observaciones = string.Empty;


                var lregIva = new List<FERegistroIVA>();
                if (respuesta.ResultGet.Iva != null)
                {
                    foreach (AlicIva alicIva in respuesta.ResultGet.Iva)
                    {
                        var regIva = new FERegistroIVA();
                        regIva.BaseImp = alicIva.BaseImp;
                        regIva.Importe = alicIva.Importe;
                        regIva.TipoIva = (FETipoIva)alicIva.Id;
                        lregIva.Add(regIva);
                    }
                }
                else if (respuesta.ResultGet.ImpNeto > 0)//para el caso de FCC
                {
                    var regIva = new FERegistroIVA();
                    regIva.BaseImp = respuesta.ResultGet.ImpNeto;
                    regIva.TipoIva = FETipoIva.Iva0;
                    lregIva.Add(regIva);
                }

                if (respuesta.ResultGet.ImpTotConc > 0)//para el caso de FC con importe NO gravadi
                {
                    var regIva = new FERegistroIVA();
                    regIva.BaseImp = respuesta.ResultGet.ImpTotConc;
                    regIva.TipoIva = FETipoIva.Iva0;
                    lregIva.Add(regIva);
                }

                comprobante.DetalleIva = lregIva;

                var lregTrib = new List<FERegistroTributo>();
                if (respuesta.ResultGet.Tributos != null)
                {
                    foreach (Tributo tributo in respuesta.ResultGet.Tributos)
                    {
                        var regTrib = new FERegistroTributo();
                        regTrib.BaseImp = tributo.BaseImp;
                        regTrib.Importe = tributo.Importe;
                        regTrib.Alicuota = tributo.Alic;
                        regTrib.Descripcion = tributo.Desc;
                        lregTrib.Add(regTrib);
                    }
                }
                comprobante.Tributos = lregTrib;
            }

            return comprobante;
        }

        public void GenerarComprobante(FEComprobante comprobante, /*string urlWsaaWsdl, string certificadoAfip,*/ string templateFc, string modo, bool showUsuBonif)
        {
            GenerarComprobante(comprobante, /*urlWsaaWsdl, certificadoAfip,*/ templateFc, null, modo, showUsuBonif,null);
        }
        
        public void GenerarComprobante(FEComprobante comprobante, /*string urlWsaaWsdl, string certificadoAfip,*/ string templateFc, string pathLogo, string modo, bool showUsuBonif,string pathMarcaDeAgua)
        {
            lock (bloqueo)
            {
                string error;

                if (!ValidarComprobante(comprobante, out error))
                    throw new Exception(error);
                else
                {
                    Service objWSFEV1 = new Service();
                    FEAuthRequest objFEAuthRequest = new FEAuthRequest();
                    var ticket = FEAutenticacion.GetTicket(comprobante.Cuit, "wsfe", /*urlWsaaWsdl, certificadoAfip,*/ modo);

                    objWSFEV1.Url = (modo == "QA" ? ConfigurationManager.AppSettings["FE.QA.wsfev1"] : ConfigurationManager.AppSettings["FE.PROD.wsfev1"]);

                    objFEAuthRequest.Token = ticket.Token;
                    objFEAuthRequest.Sign = ticket.Firma;
                    objFEAuthRequest.Cuit = comprobante.Cuit;

                    FECAECabRequest objFECAECabRequest = new FECAECabRequest();
                    FECAERequest objFECAERequest = new FECAERequest();
                    FECAEResponse objFECAEResponse = new FECAEResponse();

                    objFECAECabRequest.CantReg = 1;
                    objFECAECabRequest.PtoVta = comprobante.PtoVta;
                    objFECAECabRequest.CbteTipo = (int)comprobante.TipoComprobante;


                    FECAEDetRequest[] objFECAEDetRequest = new FECAEDetRequest[1];
                    comprobante.NumeroComprobante = GetUltimoComprobante(comprobante.Cuit, comprobante.PtoVta, comprobante.TipoComprobante, objFEAuthRequest, objWSFEV1.Url) + 1;

                    objFECAEDetRequest[0] = new FECAEDetRequest();
                    objFECAEDetRequest[0].Concepto = (int)comprobante.Concepto;
                    objFECAEDetRequest[0].DocTipo = comprobante.DocTipo;
                    objFECAEDetRequest[0].DocNro = comprobante.DocNro;
                    objFECAEDetRequest[0].CbteDesde = comprobante.NumeroComprobante;
                    objFECAEDetRequest[0].CbteHasta = comprobante.NumeroComprobante;
                    objFECAEDetRequest[0].CbteFch = comprobante.Fecha.ToString("yyyyMMdd");
                    objFECAEDetRequest[0].ImpTotal = Math.Round(comprobante.ImpTotal, 2);
                    objFECAEDetRequest[0].ImpTotConc = Math.Round(comprobante.ImpTotConc, 2);
                    objFECAEDetRequest[0].ImpNeto = Math.Round(comprobante.ImpNeto, 2);
                    objFECAEDetRequest[0].ImpOpEx = comprobante.ImpOpEx;
                    objFECAEDetRequest[0].ImpTrib = Math.Round(comprobante.Tributos.Sum(t => t.Importe), 2);
                    objFECAEDetRequest[0].ImpIVA = Math.Round(comprobante.DetalleIva.Sum(i => i.Importe), 2);
                    objFECAEDetRequest[0].FchServDesde = comprobante.FchServDesde == null ? "" : comprobante.FchServDesde.Value.ToString("yyyyMMdd");
                    objFECAEDetRequest[0].FchServHasta = comprobante.FchServHasta == null ? "" : comprobante.FchServHasta.Value.ToString("yyyyMMdd");
                    objFECAEDetRequest[0].FchVtoPago = comprobante.FchVtoPago == null ? "" : comprobante.FchVtoPago.Value.ToString("yyyyMMdd");
                    objFECAEDetRequest[0].MonId = comprobante.CodigoMoneda;
                    objFECAEDetRequest[0].MonCotiz = comprobante.CotizacionMoneda;

                    var auxIva = comprobante.DetalleIva.Where(x => x.TipoIva != FETipoIva.Iva0).ToList();

                    if (auxIva.Count > 0)
                    {
                        objFECAEDetRequest[0].Iva = new AlicIva[auxIva.Count];

                        int regNo = 0;
                        foreach (FERegistroIVA regIva in auxIva)
                            objFECAEDetRequest[0].Iva[regNo++] = new AlicIva() { BaseImp = Math.Round(regIva.BaseImp, 2), Importe = Math.Round(regIva.Importe, 2), Id = (int)regIva.TipoIva };
                    }

                    if (comprobante.Tributos.Count > 0)
                    {
                        objFECAEDetRequest[0].Tributos = new Tributo[comprobante.Tributos.Count];

                        int regNo = 0;
                        foreach (FERegistroTributo regTributo in comprobante.Tributos)
                            objFECAEDetRequest[0].Tributos[regNo++] = new Tributo() { BaseImp = regTributo.BaseImp, Importe = regTributo.Importe, Id = (short)regTributo.Tipo };
                    }

                    objFECAERequest.FeCabReq = objFECAECabRequest;
                    objFECAERequest.FeDetReq = objFECAEDetRequest;

                    //Invoco al método FECAEARegInformativo
                    try
                    {


                        objFECAEResponse = objWSFEV1.FECAESolicitar(objFEAuthRequest, objFECAERequest);

                        if (bool.Parse(ConfigurationManager.AppSettings["FE.LoguearWS"]))
                        {
                            try
                            {
                                string xmlCAERequest, xmlAuthRequest, xmlCAEResponse;
                                StringWriter textWriter = new StringWriter();
                                XmlSerializer xml;

                                xml = new XmlSerializer(objFECAERequest.GetType());
                                xml.Serialize(textWriter, objFECAERequest);
                                xmlCAERequest = textWriter.ToString();

                                xml = new XmlSerializer(objFEAuthRequest.GetType());
                                xml.Serialize(textWriter, objFEAuthRequest);
                                xmlAuthRequest = textWriter.ToString();

                                xml = new XmlSerializer(objFECAEResponse.GetType());
                                xml.Serialize(textWriter, objFECAEResponse);
                                xmlCAEResponse = textWriter.ToString();

                                GetArchivoXML(xmlCAERequest, xmlAuthRequest, xmlCAEResponse, modo);
                            }
                            catch (Exception ex) { }
                        }

                        if (objFECAEResponse != null && objFECAEResponse.FeDetResp != null && !string.IsNullOrEmpty(objFECAEResponse.FeDetResp[0].CAE))
                        {
                            comprobante.CAE = objFECAEResponse.FeDetResp[0].CAE;
                            comprobante.FechaVencCAE = DateTime.ParseExact(objFECAEResponse.FeDetResp[0].CAEFchVto, "yyyyMMdd", null);
                            try
                            {
                                comprobante.ArchivoFactura = GetStreamPDF(comprobante, templateFc, pathLogo, showUsuBonif, true,pathMarcaDeAgua);
                            }
                            catch (Exception ex)
                            {
                                comprobante.Observaciones = "No se pudo generar el PDF. Contacte al administrador";
                            }
                        }

                        if (objFECAEResponse.Errors != null)
                        {
                            var auxMsg = objFECAEResponse.Errors[0].Msg.ToString();
                            auxMsg = auxMsg.Replace("Ãº", "ú").Replace("Ã³", "ó").Replace("Ã©", "é");
                            var CodigoError = "Error FE: " + objFECAEResponse.Errors[0].Code;
                            var errorCode = objFECAEResponse.Errors[0].Code;

                            if (errorCode == 500 || errorCode == 501 || errorCode == 502 || errorCode == 503 || errorCode == 600 || errorCode == 601 || errorCode == 602)
                                throw new Exception("###Error### " + objFECAEResponse.Errors[0].Code + " - " + auxMsg);
                            else if (errorCode == 10005)
                                throw new Exception("Error: El punto de venta utilizado no se encuentra creado correctamente en AFIP para Factura Electrónica. <a href='http://www.contabilium.com/ayuda/tutoriales/como-dar-de-alta-el-punto-de-venta-en-afip/' target='_blank'>Solucionarlo</a>");
                            else if (errorCode == 10016)
                                throw new Exception("Error: No se puede emitir la factura con la fecha ingresada ya que existe otra factura emitida con fecha posterior.");
                            //ekse if (errorCode == 503)
                            //    throw new Exception("###Error### " + objFECAEResponse.Errors[0].Code + " - " + auxMsg);
                            else
                                throw new Exception("Error: " + objFECAEResponse.Errors[0].Code + " - " + auxMsg);

                            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), CodigoError, auxMsg);
                        }
                        else if (objFECAEResponse != null && objFECAEResponse.FeDetResp != null) //string.IsNullOrEmpty(objFECAEResponse.FeDetResp[0].CAE))
                        {

                            if (objFECAEResponse.FeDetResp[0].Observaciones != null)
                            {
                                StringBuilder sb = new StringBuilder();

                                foreach (Obs obs in objFECAEResponse.FeDetResp[0].Observaciones)
                                    sb.AppendLine(obs.Msg);

                                var auxMsg = sb.ToString();
                                auxMsg = auxMsg.Replace("Ãº", "ú").Replace("Ã³", "ó").Replace("Ã©", "é");

                                if (string.IsNullOrEmpty(objFECAEResponse.FeDetResp[0].CAE))
                                    throw new Exception("No se pudo obtener el CAE, revise los datos cargados.\n" + auxMsg);
                                else
                                    comprobante.Observaciones = auxMsg;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        private int GetUltimoComprobante(long cuit, int ptoVta, FETipoComprobante tipoComprobante, FEAuthRequest ticket, string url)
        {
            try
            {
                Service objWSFE = new Service();

                objWSFE.Url = url;// ConfigurationManager.AppSettings["FE.wsfev1"];
                FERecuperaLastCbteResponse ultimo = objWSFE.FECompUltimoAutorizado(ticket, ptoVta, (int)tipoComprobante);
                if (ultimo.Errors != null && ultimo.Errors.Any())
                {
                    var auxMsg = ultimo.Errors[0].Msg.ToString();
                    auxMsg = auxMsg.Replace("Ãº", "ú").Replace("Ã³", "ó").Replace("Ã©", "é");
                    var CodigoError = "Error FE: " + ultimo.Errors[0].Code;
                    var errorCode = ultimo.Errors[0].Code;

                    if (errorCode == 503)
                        throw new Exception("###Error### " + ultimo.Errors[0].Code + " - " + auxMsg);
                    else
                        throw new Exception("Error: " + ultimo.Errors[0].Code + " - " + auxMsg);
                }
                return ultimo.CbteNro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GrabarEnDisco(FEComprobante comprobante, string archivoDestino, string templateFc, bool showUsuBonif, bool imprimirEncabezado,string marcaAgua)
        {
            GrabarEnDisco(comprobante, archivoDestino, templateFc, null, showUsuBonif, imprimirEncabezado, marcaAgua);
        }

        public void GrabarEnDisco(FEComprobante comprobante, string archivoDestino, string templateFc, string pathLogo, bool showUsuBonif, bool imprimirEncabezado,string pathMarcaAgua)
        {
            Stream streamPDF = GetStreamPDF(comprobante, templateFc, pathLogo, showUsuBonif, imprimirEncabezado,pathMarcaAgua);

            using (Stream destination = File.Create(archivoDestino))
            {
                for (int a = streamPDF.ReadByte(); a != -1; a = streamPDF.ReadByte())
                    destination.WriteByte((byte)a);
            }
        }

        public void GrabarEnDisco(Stream streamPDF, string archivoDestino, string templateFc, string pathLogo)
        {
            //Stream streamPDF = GetStreamPDF(comprobante, templateFc, pathLogo);

            using (Stream destination = File.Create(archivoDestino))
            {
                for (int a = streamPDF.ReadByte(); a != -1; a = streamPDF.ReadByte())
                    destination.WriteByte((byte)a);
            }
        }

        public void ImprimirFacturaElectronicaToResponse(FEComprobante comprobante, string templateFc, string pathLogo, bool showUsuBonif, bool imprimirEncabezado, string pathMarcaAgua)
        {
            FEFacturaElectronica fe = new FEFacturaElectronica();
            Stream stream = GetStreamPDF(comprobante, templateFc, pathLogo, showUsuBonif, imprimirEncabezado, pathMarcaAgua);
            HttpResponse Response = HttpContext.Current.Response;

            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
            }

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=Comprobante.pdf");
            Response.BinaryWrite(buffer);
            // myMemoryStream.WriteTo(Response.OutputStream); //works too
            Response.Flush();
            Response.Close();
            Response.End();
        }
        public void InsertarHeader(FEComprobante comprobante, NFPDFWriter pdf, int indexPdf, string letra, string codigo, string pathLogo, bool imprimirEncabezado, string comp, string pathMarcaAgua)
        {
            bool esRecibo = comprobante.TipoComprobante == FETipoComprobante.RECIBO_A || comprobante.TipoComprobante == FETipoComprobante.RECIBO_B || comprobante.TipoComprobante == FETipoComprobante.RECIBO_C || comprobante.TipoComprobante == FETipoComprobante.SIN_DEFINIR_RECIBO || comprobante.TipoComprobante == FETipoComprobante.PAGO;
            bool esRemito = comprobante.TipoComprobante == FETipoComprobante.REMITO;
            bool esPresupuesto = comprobante.TipoComprobante == FETipoComprobante.PRESUPUESTO;
            bool esOrdenDeCompra = comprobante.TipoComprobante == FETipoComprobante.ORDEN_COMPRAS || comprobante.TipoComprobante == FETipoComprobante.ORDEN_VENTA;

            var tipoPagina = "Original";
            if (indexPdf == 1)
                tipoPagina = "Duplicado";
            else if (indexPdf == 2)
                tipoPagina = "Triplicado";

            if (imprimirEncabezado)
            {
                pdf.insertarMarcaDeAgua(pathMarcaAgua);

                pdf.EscribirXY(letra, 302, 50, 22, Alineado.Centro, NFPDFColor.WHITE);
                if (codigo != "")
                    pdf.EscribirXY("Cod. " + codigo, 302, 65, 10, Alineado.Centro, NFPDFColor.WHITE);
                pdf.EscribirXY(comp, 570, 45, 12, Alineado.Derecha);
                if (!esRecibo)
                {
                    pdf.EscribirXY(tipoPagina, 570, 60, 10, Alineado.Derecha);
                    if (esOrdenDeCompra)
                        pdf.EscribirXY("Nº: " + comprobante.NumeroComprobante.ToString().PadLeft(8, '0'), 570, 90, 15, Alineado.Derecha);
                    else
                        pdf.EscribirXY("Nº: " + comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0'), 570, 90, 15, Alineado.Derecha);
                }
                else
                {
                    pdf.EscribirXY("Documento no válido como factura", 570, 60, 10, Alineado.Derecha);//solo para recibo
                    string nro = comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                    if (comprobante.TipoComprobante == FETipoComprobante.PAGO)
                        nro = "0001-" + nro;
                    pdf.EscribirXY("Nº: " + nro, 570, 90, 15, Alineado.Derecha);
                }
                pdf.EscribirXY("Fecha: " + comprobante.Fecha.ToString("dd/MM/yyyy"), 570, 105, 12, Alineado.Derecha);
                if (comprobante.TipoComprobante == FETipoComprobante.ORDEN_COMPRAS && comprobante.FchVtoPago.HasValue)
                    pdf.EscribirXY("Fecha entrega: " + comprobante.FchVtoPago.Value.ToString("dd/MM/yyyy"), 570, 120, 10, Alineado.Derecha);
                else
                {
                    if (comprobante.FchVtoPago.HasValue)
                    {
                        if (!esPresupuesto)
                            pdf.EscribirXY("Vencimiento: " + comprobante.FchVtoPago.Value.ToString("dd/MM/yyyy"), 570, 120, 10, Alineado.Derecha);
                        else
                            pdf.EscribirXY("Validez: " + comprobante.FchVtoPago.Value.ToString("dd/MM/yyyy"), 570, 120, 10, Alineado.Derecha);
                    }
                }

                #region Datos de la empresa facturante

                if (!string.IsNullOrEmpty(pathLogo))
                {
                    pdf.InsertarImagenXYConTransparencia(pathLogo, 27, 750, 80);
                    pdf.EscribirXY(comprobante.RazonSocial, 27, 110, 10, Alineado.Izquierda);
                }
                else
                    pdf.EscribirXY(comprobante.RazonSocial, 27, 70, 22, Alineado.Izquierda);

                pdf.EscribirXY(comprobante.Domicilio, 27, 125, 10, Alineado.Izquierda);
                pdf.EscribirXY(comprobante.CiudadProvincia, 27, 140, 10, Alineado.Izquierda);
                pdf.EscribirXY("Tel.: " + comprobante.Telefono, 27, 155, 10, Alineado.Izquierda);
                pdf.EscribirXY(comprobante.CondicionIva, 27, 170, 10, Alineado.Izquierda);

                pdf.EscribirXY("CUIT: " + comprobante.Cuit, 570, 140, 10, Alineado.Derecha);
                pdf.EscribirXY("Ingresos brutos: " + comprobante.IIBB, 570, 155, 10, Alineado.Derecha);
                pdf.EscribirXY("Inicio de actividades: " + comprobante.FechaInicioActividades, 570, 170, 10, Alineado.Derecha);

                #endregion
            }
            else
            {
                //pdf.EscribirXY("Nº: " + comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0'), 570, 90, 15, Alineado.Derecha);
                pdf.EscribirXY("Fecha: " + comprobante.Fecha.ToString("dd/MM/yyyy"), 570, 105, 12, Alineado.Derecha);
            }

            #region Datos del cliente

            var tipoDoc = (comprobante.DocTipo == 96) ? "DNI" : "CUIT";
            var NroDeDoc = (comprobante.DocNro != 0) ? comprobante.DocNro.ToString() : "";
            pdf.EscribirXY("Razón social: " + comprobante.ClienteNombre, 27, 205, 10, Alineado.Izquierda);
            pdf.EscribirXY("Domicilio: " + comprobante.ClienteDomicilio, 27, 220, 10, Alineado.Izquierda);

            //Fix Localidad
            var localidad = comprobante.ClienteLocalidad.ReplaceAll("SIN IDENTIFICAR,", "");

            pdf.EscribirXY("Ubicación: " + localidad, 27, 235, 10, Alineado.Izquierda);
            if (comprobante.DocTipo != 99)
                pdf.EscribirXY(tipoDoc + ": " + NroDeDoc, 331, 235, 10, Alineado.Izquierda);
            pdf.EscribirXY("Condición de IVA: " + comprobante.ClienteCondiionIva, 331, 250, 10, Alineado.Izquierda);
            if (!esRecibo)
                pdf.EscribirXY("Condición de venta: " + comprobante.CondicionVenta, 27, 250, 10, Alineado.Izquierda);

            #endregion

        
        }
        public Stream GetStreamPDFFacturaElectonica(FEComprobante comprobante, string templateFc, string pathLogo, bool showUsuBonif, bool imprimirEncabezado, string pathMarcaAgua)
        {

            string letra = "";
            string comp = "";
            string codigo = "";
            bool esRecibo = comprobante.TipoComprobante == FETipoComprobante.RECIBO_A || comprobante.TipoComprobante == FETipoComprobante.RECIBO_B || comprobante.TipoComprobante == FETipoComprobante.RECIBO_C || comprobante.TipoComprobante == FETipoComprobante.SIN_DEFINIR_RECIBO || comprobante.TipoComprobante == FETipoComprobante.PAGO;
            bool esRemito = comprobante.TipoComprobante == FETipoComprobante.REMITO;
            bool esPresupuesto = comprobante.TipoComprobante == FETipoComprobante.PRESUPUESTO;
            bool esOrdenDeCompra = comprobante.TipoComprobante == FETipoComprobante.ORDEN_COMPRAS || comprobante.TipoComprobante == FETipoComprobante.ORDEN_VENTA;
            var template = (templateFc == string.Empty ? ConfigurationManager.AppSettings["FE.Template"] : templateFc);
            var templateFinal = template.Split(".pdf")[0] + "Final.pdf";
            template = templateFinal;
            NFPDFWriter pdf = new NFPDFWriter(MedidasDocumento.A4, template);

            if ((int)comprobante.TipoComprobante < 0)
                codigo = "";
            else if ((int)comprobante.TipoComprobante == 999)
                codigo = "P";
            else
                codigo = ((int)comprobante.TipoComprobante).ToString().PadLeft(2, '0');

            bool mostrarIva = true;
            int cantIva = 0;
            #region valores Inic
            switch (comprobante.TipoComprobante)
            {
                case FETipoComprobante.FACTURAS_A:
                    letra = "A";
                    comp = "FACTURA";
                    break;
                case FETipoComprobante.FACTURAS_B:
                    letra = "B";
                    comp = "FACTURA";
                    mostrarIva = false;
                    break;
                case FETipoComprobante.FACTURAS_C:
                    letra = "C";
                    comp = "FACTURA";
                    break;
                case FETipoComprobante.FACTURAS_E:
                    letra = "E";
                    comp = "FACTURA";
                    break;

                case FETipoComprobante.RECIBO_A:
                    letra = "A";
                    comp = "RECIBO";
                    break;
                case FETipoComprobante.RECIBO_B:
                    letra = "B";
                    comp = "RECIBO";
                    break;
                case FETipoComprobante.RECIBO_C:
                    letra = "C";
                    comp = "RECIBO";
                    break;

                case FETipoComprobante.NOTAS_DEBITO_A:
                    letra = "A";
                    comp = "NOTA DE DÉBITO";
                    break;
                case FETipoComprobante.NOTAS_DEBITO_B:
                    letra = "B";
                    comp = "NOTA DE DÉBITO";
                    mostrarIva = false;
                    break;
                case FETipoComprobante.NOTAS_DEBITO_C:
                    letra = "C";
                    comp = "NOTA DE DÉBITO";
                    break;
                case FETipoComprobante.NOTAS_DEBITO_E:
                    letra = "E";
                    comp = "NOTA DE DÉBITO";
                    break;

                case FETipoComprobante.NOTAS_CREDITO_A:
                    letra = "A";
                    comp = "NOTA DE CRÉDITO";
                    break;
                case FETipoComprobante.NOTAS_CREDITO_B:
                    letra = "B";
                    comp = "NOTA DE CRÉDITO";
                    mostrarIva = false;
                    break;
                case FETipoComprobante.NOTAS_CREDITO_C:
                    letra = "C";
                    comp = "NOTA DE CRÉDITO";
                    break;
                case FETipoComprobante.NOTAS_CREDITO_E:
                    letra = "E";
                    comp = "NOTA DE CRÉDITO";
                    break;
                case FETipoComprobante.SIN_DEFINIR_RECIBO:
                    letra = "X";
                    comp = "Recibo";
                    break;
                case FETipoComprobante.SIN_DEFINIR:
                    letra = "X";
                    comp = "Documento no válido como factura";
                    break;
                case FETipoComprobante.PRESUPUESTO:
                    letra = "X";
                    comp = "Presupuesto";
                    break;
                case FETipoComprobante.ORDEN_COMPRAS:
                    letra = "X";
                    comp = "Orden de compra";
                    codigo = "";
                    break;
                case FETipoComprobante.ORDEN_VENTA:
                    letra = "X";
                    comp = "Orden de venta";
                    codigo = "";
                    break;
                case FETipoComprobante.PAGO:
                    letra = "X";
                    comp = "Pago";
                    codigo = "";
                    break;
                case FETipoComprobante.REMITO:
                    letra = "R";
                    comp = "Remito";
                    break;
            }

            int cantPdf = 3;

            if (esRecibo || esRemito || esPresupuesto || esOrdenDeCompra)
                cantPdf = 1;
            #endregion
            for (int indexPdf = 0; indexPdf < cantPdf; indexPdf++)
            {

                InsertarHeader(comprobante, pdf, indexPdf, letra, codigo, pathLogo, imprimirEncabezado, comp, pathMarcaAgua);

                #region Datos generales
                int totalesY = 715;



                //Fix para presupuestos que no son con IVA
                if (esPresupuesto && cantIva == 0)
                    mostrarIva = false;
                /*int i = 0;
                double itemPrecio;*/
                int fontDetalleFE = 10;

                if (ConfigurationManager.AppSettings["FE.FontDetalle"] != null) fontDetalleFE = int.Parse(ConfigurationManager.AppSettings["FE.FontDetalle"]);

                List<FETipoComprobante> tipoRemitos = new List<FETipoComprobante>() { FETipoComprobante.RECIBO_A, FETipoComprobante.RECIBO_B, FETipoComprobante.RECIBO_C, FETipoComprobante.SIN_DEFINIR_RECIBO, FETipoComprobante.PAGO };

                #region detalle
                double subtotal = 0;
                List<FEItemDetalle> detalleAux = comprobante.ItemsDetalle.ToList();
              /*  var cantidadTotal = detalleAux.Count();
                bool altoFijo = (cantidadTotal <= 10 ? false : true);*/
                var i = 0;//mejorar redondeo
                var tam = int.Parse(detalleAux.Sum(x => ((x.Descripcion.Length / 49) > (x.Codigo.Length / 10)) ?
                                15f + (int.Parse((x.Descripcion.Length / 49).ToString()) * 10f) :
                                15f + (int.Parse((x.Codigo.Length / 10).ToString()) * 10f)).ToString());

                var totalHojas = int.Parse((tam / 335).ToString()) + 1;
                while(detalleAux.Any())
                {

                    if (i > 0)
                    {
                        //INSERTA 
                        pdf.NuevaPagina();
                        pdf.EscribirBoxXY("Trans: ", 478, 285, 10, 540);
                        pdf.EscribirBoxXY("$ " + subtotal.ToString("N2"), 520, 285, 10, 510);

                        //     pdf.EscribirBoxXY("$ " + subtotal.ToString("N2"), 550, 285, 10, 540);
                        //       pdf.EscribirBoxXY("$ " + subtotal.ToString("N2"), 520, 285, 10, 510);

                        var reader = new PdfReader(templateFinal);
                        PdfImportedPage page = pdf.writer.GetImportedPage(reader, 1);
                        PdfContentByte cb = pdf.writer.DirectContent;
                        cb.AddTemplate(page, 0, 0);
                        InsertarHeader(comprobante, pdf, indexPdf, letra, codigo, pathLogo, imprimirEncabezado, comp,pathMarcaAgua);

                    }

                  /*  var cant = 18;
                    if (detalleAux.Count() < 18)
                        cant = detalleAux.Count();

                    var list = detalleAux.GetRange(0, cant);
                    */
                    var cantInsertadas = pdf.InsertarTablaDetalle(detalleAux, mostrarIva, 335);
                    var list = detalleAux.GetRange(0, cantInsertadas);
                    detalleAux.RemoveRange(0, list.Count());

                    if (detalleAux.Any())//NO TERMINA
                    {
                        subtotal += list.Sum(x => x.Total);
                        pdf.EscribirBoxXY("Subtotal: " + subtotal.ToString("N2"), 480, 680, 10, 380);
                    }
                    pdf.EscribirXY(("Pag: " + (i + 1).ToString()  + " de " + totalHojas), 300, 770, 8, Alineado.Centro);
                    if (!esRemito && !esRecibo && !esPresupuesto && !esOrdenDeCompra)
                        InsertarFooterCAE(comprobante, pdf);

                    i++;

                }


                #endregion

                #region footer

                totalesY = 715;
                //int index = 0;
                if (!string.IsNullOrEmpty(comprobante.Observaciones))
                {
                    foreach (var obs in comprobante.Observaciones.Split('\n'))
                    {
                        pdf.EscribirBoxXY(obs, 27, totalesY, 7, 400);
                        totalesY += 15;// (i == 0 ? 30 : 15);
                        //index++;
                    }
                }

                //foreach(var obs in comprobante.Observaciones.Split('\n'))
                //{
                //    pdf.EscribirBoxXY(obs, 27, 715, 10, 400);
                //}

                totalesY = 680;

                if (!esRemito && comprobante.TotalBonificacion > 0 && showUsuBonif)
                {
                    pdf.EscribirXY("Bonificación: $" + Math.Abs(comprobante.TotalBonificacion).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                    totalesY += 15;
                }

                if ((letra == "A" || esPresupuesto || esOrdenDeCompra) && !esRecibo && !esRemito)
                {
                    if (comprobante.ImpNeto > 0)
                    {
                        var desc = (comprobante.CondicionIva != "Monotributo") ? "Importe Neto Gravado: $" : "Subtotal: $";
                        pdf.EscribirXY(desc + Math.Abs(comprobante.ImpNeto).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                        totalesY += 15;
                    }

                    /*if (comprobante.DescuentoPorcentaje != null && comprobante.DescuentoImporte != null)
                    {
                        pdf.EscribirXY("Descuento " + comprobante.DescuentoPorcentaje.Value.ToString("N2") + "%: $" + Math.Abs(comprobante.DescuentoImporte.Value).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                        totalesY += 15;
                    }*/
                    if (comprobante.CondicionIva != "Monotributo")
                    {

                        foreach (FERegistroIVA rIVA in comprobante.DetalleIva)
                        {
                            //if (rIVA.Importe != 0)
                            //{
                            switch (rIVA.TipoIva)
                            {
                                case FETipoIva.Iva0://Todo: es correcto esto?
                                    pdf.EscribirXY("No Gravado/Exento: $" + Math.Abs(rIVA.BaseImp).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                    totalesY += 15;
                                    break;

                                case FETipoIva.Iva10_5:
                                    pdf.EscribirXY("IVA 10.5%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                    totalesY += 15;
                                    cantIva++;
                                    break;

                                case FETipoIva.Iva21:
                                    pdf.EscribirXY("IVA 21%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                    totalesY += 15;
                                    cantIva++;
                                    break;

                                case FETipoIva.Iva27:
                                    pdf.EscribirXY("IVA 27%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                    totalesY += 15;
                                    cantIva++;
                                    break;

                                case FETipoIva.Iva5:
                                    pdf.EscribirXY("IVA 5%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                    totalesY += 15;
                                    cantIva++;
                                    break;
                                case FETipoIva.Iva2_5:
                                    pdf.EscribirXY("IVA 2.5%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                    totalesY += 15;
                                    cantIva++;
                                    break;
                            }
                        }
                    }
                }

                if (!esRemito)
                {
                    foreach (var item in comprobante.Tributos)
                    {
                        if (item.Importe != 0)
                        {
                            pdf.EscribirXY(item.Descripcion + " " + item.Alicuota + "%: $" + Math.Abs(item.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                            totalesY += 15;
                        }
                    }

                    //if (comprobante.ImpTotConc > 0)
                    //{
                    //    pdf.EscribirXY("No Gravado/Exento: $" + Math.Abs(comprobante.ImpTotConc).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                    //    totalesY += 15;
                    //}

                    pdf.EscribirXY("Importe Total: $" + Math.Abs(comprobante.ImpTotal).ToString("N2"), 570, 800, 15, Alineado.Derecha);
                }
                /*   if (!esRemito && !esRecibo && !esPresupuesto && !esOrdenDeCompra)
                   {
                       if (comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR &&
                           comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR_RECIBO &&
                                     comprobante.TipoComprobante != FETipoComprobante.PAGO &&
                           !string.IsNullOrEmpty(comprobante.CAE))
                       {
                           pdf.EscribirXY("CAE: " + comprobante.CAE, 27, 770, 8, Alineado.Izquierda);
                           pdf.EscribirXY("Vencimiento CAE: " + comprobante.FechaVencCAE.ToString("dd/MM/yyyy"), 150, 770, 8, Alineado.Izquierda);
                           pdf.InsertarImagenXY(ImagenCodigoBarras(comprobante), 27, 45);
                           pdf.EscribirXY(CodigoDeBarras(comprobante), 27, 808, 10, Alineado.Izquierda);
                       }
                   }*/
                #endregion

                if (!esRemito)
                {
                    pdf.EscribirBoxXY("Son Pesos " + NumeroALetrasMoneda(comprobante.ImpTotal) + ".", 27, 680, 10, 380);
                }
                else
                {
                    if (comprobante.CondicionIva == "Monotributo")
                        pdf.EscribirBoxXY("VPS: $" + Math.Abs(comprobante.ItemsDetalle.Sum(x => x.Total)).ToString("N2"), 27, 680, 10, 400);
                    else
                        pdf.EscribirBoxXY("VPS: $" + Math.Abs(comprobante.ImpNeto + comprobante.ImpTotConc).ToString("N2"), 27, 680, 10, 400);
                }



                #endregion

                if (cantPdf > 1)
                {
                    pdf.NuevaPagina();
                    if (indexPdf < 2)
                    {
                        var reader = new PdfReader(template);
                        //var writer = PdfWriter.GetInstance(pdf.document, pdf.mem);
                        PdfImportedPage page = pdf.writer.GetImportedPage(reader, 1);
                        PdfContentByte cb = pdf.writer.DirectContent;
                        cb.AddTemplate(page, 0, 0);
                    }
                }
            }

            return pdf.GenerarPDFStream();
        }
        public void InsertarFooterCAE(FEComprobante comprobante, NFPDFWriter pdf)
        {

            if (comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR &&
                comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR_RECIBO &&
                          comprobante.TipoComprobante != FETipoComprobante.PAGO &&
                !string.IsNullOrEmpty(comprobante.CAE))
            {
                pdf.EscribirXY("CAE: " + comprobante.CAE, 27, 770, 8, Alineado.Izquierda);
                pdf.EscribirXY("Vencimiento CAE: " + comprobante.FechaVencCAE.ToString("dd/MM/yyyy"), 150, 770, 8, Alineado.Izquierda);
                pdf.InsertarImagenXY(ImagenCodigoBarras(comprobante), 27, 45);
                pdf.EscribirXY(CodigoDeBarras(comprobante), 27, 808, 10, Alineado.Izquierda);
            }

        }
        public Stream GetStreamPDF(FEComprobante comprobante, string templateFc, string pathLogo, bool showUsuBonif, bool imprimirEncabezado,string pathMarcaAgua)
        {

            bool esRecibo = comprobante.TipoComprobante == FETipoComprobante.RECIBO_A || comprobante.TipoComprobante == FETipoComprobante.RECIBO_B || comprobante.TipoComprobante == FETipoComprobante.RECIBO_C || comprobante.TipoComprobante == FETipoComprobante.SIN_DEFINIR_RECIBO || comprobante.TipoComprobante == FETipoComprobante.PAGO;
            bool esRemito = comprobante.TipoComprobante == FETipoComprobante.REMITO;
            bool esPresupuesto = comprobante.TipoComprobante == FETipoComprobante.PRESUPUESTO;
            bool esOrdenDeCompra = comprobante.TipoComprobante == FETipoComprobante.ORDEN_COMPRAS || comprobante.TipoComprobante == FETipoComprobante.ORDEN_VENTA;
            if (!esRemito && !esRecibo && !esPresupuesto && !esOrdenDeCompra && comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR &&
                    comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR_RECIBO &&
                              comprobante.TipoComprobante != FETipoComprobante.PAGO &&
                    !string.IsNullOrEmpty(comprobante.CAE))
            {
            
                
                return GetStreamPDFFacturaElectonica(comprobante, templateFc, pathLogo, showUsuBonif, imprimirEncabezado,pathMarcaAgua);
             //  return null;
            }
            else
            {

                string letra = "";
                string comp = "";
                string codigo = "";

                var template = (templateFc == string.Empty ? ConfigurationManager.AppSettings["FE.Template"] : templateFc);
                var templateFinal = template.Split(".pdf")[0] + "Final.pdf";
                List<FETipoComprobante> tipoRemitos = new List<FETipoComprobante>() { FETipoComprobante.RECIBO_A, FETipoComprobante.RECIBO_B, FETipoComprobante.RECIBO_C, FETipoComprobante.SIN_DEFINIR_RECIBO, FETipoComprobante.PAGO };
                var tam = 0; 
                var tamRemito = 0;

                foreach (var item in comprobante.ItemsDetalle)
                {
                    if (item.Codigo == null)
                        item.Codigo = "";
                }

                if (comprobante.ItemsDetalle != null)
                {
                    tam = int.Parse(comprobante.ItemsDetalle.Sum(x => ((x.Descripcion.Length/49) > (x.Codigo.Length/10)) ?
                               15f + (int.Parse((x.Descripcion.Length / 49).ToString()) * 10f) :
                               15f + (int.Parse((x.Codigo.Length / 10).ToString()) * 10f)).ToString());
                    tamRemito = int.Parse(comprobante.ItemsDetalle.Sum(x => 15f + (int.Parse((x.Codigo.Length / 10).ToString()) * 10f)).ToString());
                }
                var tamCobranza=0;
                if(comprobante.ConceptoRecibo!=null)
                 tamCobranza = int.Parse(comprobante.ConceptoRecibo.Split('|').Sum(x => 15f).ToString());

                if (tam < (355+15f) && !esRemito && !(tipoRemitos.Contains(comprobante.TipoComprobante)))
                    template = templateFinal;
                else if ((tamRemito < (355+15f) && esRemito) || (tipoRemitos.Contains(comprobante.TipoComprobante) && tamCobranza < (355+15f)))
                    template = templateFinal;

                NFPDFWriter pdf = new NFPDFWriter(MedidasDocumento.A4, template);

                if ((int)comprobante.TipoComprobante < 0)
                    codigo = "";
                else if ((int)comprobante.TipoComprobante == 999)
                    codigo = "P";
                else
                    codigo = ((int)comprobante.TipoComprobante).ToString().PadLeft(2, '0');

                bool mostrarIva = true;
                int cantIva = 0;
                #region valores Inic
                switch (comprobante.TipoComprobante)
                {
                    case FETipoComprobante.FACTURAS_A:
                        letra = "A";
                        comp = "FACTURA";
                        break;
                    case FETipoComprobante.FACTURAS_B:
                        letra = "B";
                        comp = "FACTURA";
                        mostrarIva = false;
                        break;
                    case FETipoComprobante.FACTURAS_C:
                        letra = "C";
                        comp = "FACTURA";
                        break;
                    case FETipoComprobante.FACTURAS_E:
                        letra = "E";
                        comp = "FACTURA";
                        break;

                    case FETipoComprobante.RECIBO_A:
                        letra = "A";
                        comp = "RECIBO";
                        break;
                    case FETipoComprobante.RECIBO_B:
                        letra = "B";
                        comp = "RECIBO";
                        break;
                    case FETipoComprobante.RECIBO_C:
                        letra = "C";
                        comp = "RECIBO";
                        break;

                    case FETipoComprobante.NOTAS_DEBITO_A:
                        letra = "A";
                        comp = "NOTA DE DÉBITO";
                        break;
                    case FETipoComprobante.NOTAS_DEBITO_B:
                        letra = "B";
                        comp = "NOTA DE DÉBITO";
                        mostrarIva = false;
                        break;
                    case FETipoComprobante.NOTAS_DEBITO_C:
                        letra = "C";
                        comp = "NOTA DE DÉBITO";
                        break;
                    case FETipoComprobante.NOTAS_DEBITO_E:
                        letra = "E";
                        comp = "NOTA DE DÉBITO";
                        break;

                    case FETipoComprobante.NOTAS_CREDITO_A:
                        letra = "A";
                        comp = "NOTA DE CRÉDITO";
                        break;
                    case FETipoComprobante.NOTAS_CREDITO_B:
                        letra = "B";
                        comp = "NOTA DE CRÉDITO";
                        mostrarIva = false;
                        break;
                    case FETipoComprobante.NOTAS_CREDITO_C:
                        letra = "C";
                        comp = "NOTA DE CRÉDITO";
                        break;
                    case FETipoComprobante.NOTAS_CREDITO_E:
                        letra = "E";
                        comp = "NOTA DE CRÉDITO";
                        break;
                    case FETipoComprobante.SIN_DEFINIR_RECIBO:
                        letra = "X";
                        comp = "Recibo";
                        break;
                    case FETipoComprobante.SIN_DEFINIR:
                        letra = "X";
                        comp = "Documento no válido como factura";
                        break;
                    case FETipoComprobante.PRESUPUESTO:
                        letra = "X";
                        comp = "Presupuesto";
                        break;
                    case FETipoComprobante.ORDEN_COMPRAS:
                        letra = "X";
                        comp = "Orden de compra";
                        codigo = "";
                        break;
                    case FETipoComprobante.ORDEN_VENTA:
                        letra = "X";
                        comp = "Orden de venta";
                        codigo = "";
                        break;
                    case FETipoComprobante.PAGO:
                        letra = "X";
                        comp = "Pago";
                        codigo = "";
                        break;
                    case FETipoComprobante.REMITO:
                        letra = "R";
                        comp = "Remito";
                        break;
                }

                int cantPdf = 3;

                if (esRecibo || esRemito || esPresupuesto || esOrdenDeCompra)
                    cantPdf = 1;
                #endregion
                for (int indexPdf = 0; indexPdf < cantPdf; indexPdf++)
                {

                    InsertarHeader(comprobante, pdf, indexPdf, letra, codigo, pathLogo, imprimirEncabezado, comp,pathMarcaAgua);

                    #region Datos generales
                    int totalesY = 715;



                    //Fix para presupuestos que no son con IVA
                    //if (esPresupuesto)// && cantIva == 0)//despues del cambio de pdfs, quedó en desuso esto.
                    //    mostrarIva = false;
                    /*int i = 0;
                    double itemPrecio;*/
                    int fontDetalleFE = 10;

                    if (ConfigurationManager.AppSettings["FE.FontDetalle"] != null) fontDetalleFE = int.Parse(ConfigurationManager.AppSettings["FE.FontDetalle"]);

                    #region detalle
                    if (tipoRemitos.Contains(comprobante.TipoComprobante))
                    {
                        //pdf.EscribirBoxXY(comprobante.ConceptoRecibo, 27, 280, 10, 540);
                        // totalesY = 280;

                        #region detalle
                        List<string> detalleAux = comprobante.ConceptoRecibo.Split('|').ToList();

                        var cantidadTotal = detalleAux.Count();
                        bool altoFijo = (cantidadTotal <= 10 ? false : true);

                        var cantHojas = int.Parse((cantidadTotal / 25).ToString()) + 1;//mejorar redondeo
                        int i = 0;

                        var totalHojas = int.Parse((tamCobranza / 470).ToString()) + 1;
                        while (detalleAux.Any()&& detalleAux[0]!="")
                        {

                            if (i > 0)
                            {
                                //INSERTA 
                                pdf.NuevaPagina();
                                PdfReader reader;
                                var tamAux = int.Parse(detalleAux.Sum(x => 15f).ToString());
                                if (totalHojas <= i + 1 && tamAux < (355 + 15f))
                                    reader = new PdfReader(templateFinal);
                                else
                                    reader = new PdfReader(template);
                                PdfImportedPage page = pdf.writer.GetImportedPage(reader, 1);
                                PdfContentByte cb = pdf.writer.DirectContent;
                                cb.AddTemplate(page, 0, 0);
                                InsertarHeader(comprobante, pdf, indexPdf, letra, codigo, pathLogo, imprimirEncabezado, comp,pathMarcaAgua);

                            }
                            var alto = 470;

                            var tamAux2 = int.Parse(detalleAux.Sum(x => 15f).ToString());
                            if (totalHojas <= i || tamAux2 < (alto + 15f))
                                alto = 345;

                            int cant = 0;
                            int j = 0;
                            totalesY = 280;
                            float h = 0;
                            foreach (var concepto in detalleAux)
                            {
                                if (h < alto)
                                {
                                    pdf.EscribirBoxXY(concepto, 27, totalesY, 10, 540);
                                    totalesY += (j == 0 ? 30 : 15);
                                    j++;
                                    cant++;
                                    h += 15f;
                                }

                            }


                            detalleAux.RemoveRange(0, cant);
                            i++;
                        }




                        #endregion
                    }
                    else if (esRemito)
                    {
                        #region detalle
                        List<FEItemDetalle> detalleAux = comprobante.ItemsDetalle.ToList();

                        var cantidadTotal = detalleAux.Count();
                        bool altoFijo = (cantidadTotal <= 10 ? false : true);

                        var cantHojas = int.Parse((cantidadTotal / 25).ToString()) + 1;//mejorar redondeo
                        int i = 0;

                        var totalHojas = int.Parse((tamRemito / 470).ToString()) + 1;
                        while (detalleAux.Any())
                        {

                            if (i > 0)
                            {
                                //INSERTA 
                                pdf.NuevaPagina();
                                PdfReader reader;
                                var tamAux = int.Parse(detalleAux.Sum(x => 15f + (int.Parse((x.Codigo.Length / 10).ToString()) * 10f)).ToString());
                                if (totalHojas <= i + 1 && tamAux < (355 + 15f))
                                    reader = new PdfReader(templateFinal);
                                else
                                    reader = new PdfReader(template);
                                MemoryStream strm = new MemoryStream();
                                PdfStamper stamp = new PdfStamper(reader, strm);

                                PdfImportedPage page = pdf.writer.GetImportedPage(reader, 1);
                                PdfContentByte cb = pdf.writer.DirectContent;
                                cb.AddTemplate(page, 0, 0);  
                           //     pdf.insertarMarcaDeAgua();

                                InsertarHeader(comprobante, pdf, indexPdf, letra, codigo, pathLogo, imprimirEncabezado, comp,pathMarcaAgua);
                             

                            }

                            var alto = 470;

                            var tamAux2 = int.Parse(detalleAux.Sum(x => 15f + (int.Parse((x.Codigo.Length /10 ).ToString()) * 10f)).ToString());
                            if (totalHojas <= i || tamAux2 < (alto + 15f))
                                alto = 355;
                            int cant = pdf.InsertarTablaDetalleRemito(detalleAux, alto);

                            detalleAux.RemoveRange(0, cant);
                            i++;
                        }


                        #endregion

                    }
                    else
                    {
                        #region detalle
                        List<FEItemDetalle> detalleAux = comprobante.ItemsDetalle.ToList();

                        var cantidadTotal = detalleAux.Count();
                        bool altoFijo = (cantidadTotal <= 10 ? false : true);

                        var cantHojas = int.Parse((cantidadTotal / 25).ToString()) + 1;//mejorar redondeo
                        int i=0;
                  
                        var totalHojas = int.Parse((tam / 470).ToString()) + 1;
                        while (detalleAux.Any())
                        {

                            if (i > 0)
                            {
                                //INSERTA 
                                pdf.NuevaPagina();
                                PdfReader reader ;
                                var tamAux = int.Parse(detalleAux.Sum(x => ((x.Descripcion.Length / 49) > (x.Codigo.Length / 10)) ?
                                15f + (int.Parse((x.Descripcion.Length / 49).ToString()) * 10f) :
                                15f + (int.Parse((x.Codigo.Length / 10).ToString()) * 10f)).ToString());
                                if (totalHojas <= i + 1 && tamAux < (355 + 15f))
                                    reader = new PdfReader(templateFinal);
                                else
                                    reader = new PdfReader(template);
                                PdfImportedPage page = pdf.writer.GetImportedPage(reader, 1);
                                PdfContentByte cb = pdf.writer.DirectContent;
                                cb.AddTemplate(page, 0, 0);
                                InsertarHeader(comprobante, pdf, indexPdf, letra, codigo, pathLogo, imprimirEncabezado, comp,pathMarcaAgua);

                            }
                            var alto=470;

                            var tamAux2 = int.Parse(detalleAux.Sum(x => ((x.Descripcion.Length / 49) > (x.Codigo.Length / 10)) ?
                                15f + (int.Parse((x.Descripcion.Length / 49).ToString()) * 10f):
                                15f + (int.Parse((x.Codigo.Length / 10).ToString()) * 10f)
                                ).ToString());
                            if (totalHojas <= i || tamAux2 < (alto + 15f))
                                    alto = 355;
                            int cant = pdf.InsertarTablaDetalle(detalleAux, mostrarIva, alto);         

                            detalleAux.RemoveRange(0, cant);
                            i++;
                        }
                        #endregion


                    }

                    #endregion
                    #region footer

                    totalesY = 715;
                    //int index = 0;
                    if (!string.IsNullOrEmpty(comprobante.Observaciones))
                    {
                        foreach (var obs in comprobante.Observaciones.Split('\n'))
                        {
                            pdf.EscribirBoxXY(obs, 27, totalesY, 7, 400);
                            totalesY += 15;// (i == 0 ? 30 : 15);
                            //index++;
                        }
                    }

                    //foreach(var obs in comprobante.Observaciones.Split('\n'))
                    //{
                    //    pdf.EscribirBoxXY(obs, 27, 715, 10, 400);
                    //}

                    totalesY = 680;

                    if (!esRemito && comprobante.TotalBonificacion > 0 && showUsuBonif)
                    {
                        pdf.EscribirXY("Bonificación: $" + Math.Abs(comprobante.TotalBonificacion).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                        totalesY += 15;
                    }

                    if ((letra == "A" || esPresupuesto || esOrdenDeCompra) && !esRecibo && !esRemito)
                    {
                        if (comprobante.ImpNeto > 0)
                        {
                            var desc = (comprobante.CondicionIva != "Monotributo") ? "Importe Neto Gravado: $" : "Subtotal: $";
                            pdf.EscribirXY(desc + Math.Abs(comprobante.ImpNeto).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                            totalesY += 15;
                        }

                        /*if (comprobante.DescuentoPorcentaje != null && comprobante.DescuentoImporte != null)
                        {
                            pdf.EscribirXY("Descuento " + comprobante.DescuentoPorcentaje.Value.ToString("N2") + "%: $" + Math.Abs(comprobante.DescuentoImporte.Value).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                            totalesY += 15;
                        }*/
                        if (comprobante.CondicionIva != "Monotributo")
                        {

                            foreach (FERegistroIVA rIVA in comprobante.DetalleIva)
                            {
                                //if (rIVA.Importe != 0)
                                //{
                                switch (rIVA.TipoIva)
                                {
                                    case FETipoIva.Iva0://Todo: es correcto esto?
                                        pdf.EscribirXY("No Gravado/Exento: $" + Math.Abs(rIVA.BaseImp).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                        totalesY += 15;
                                        break;

                                    case FETipoIva.Iva10_5:
                                        pdf.EscribirXY("IVA 10.5%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                        totalesY += 15;
                                        cantIva++;
                                        break;

                                    case FETipoIva.Iva21:
                                        pdf.EscribirXY("IVA 21%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                        totalesY += 15;
                                        cantIva++;
                                        break;

                                    case FETipoIva.Iva27:
                                        pdf.EscribirXY("IVA 27%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                        totalesY += 15;
                                        cantIva++;
                                        break;

                                    case FETipoIva.Iva5:
                                        pdf.EscribirXY("IVA 5%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                        totalesY += 15;
                                        cantIva++;
                                        break;
                                    case FETipoIva.Iva2_5:
                                        pdf.EscribirXY("IVA 2.5%: $" + Math.Abs(rIVA.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                        totalesY += 15;
                                        cantIva++;
                                        break;
                                }
                            }
                        }
                    }

                    if (!esRemito)
                    {
                        foreach (var item in comprobante.Tributos)
                        {
                            if (item.Importe != 0)
                            {
                                pdf.EscribirXY(item.Descripcion + " " + item.Alicuota + "%: $" + Math.Abs(item.Importe).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                                totalesY += 15;
                            }
                        }

                        //if (comprobante.ImpTotConc > 0)
                        //{
                        //    pdf.EscribirXY("No Gravado/Exento: $" + Math.Abs(comprobante.ImpTotConc).ToString("N2"), 570, totalesY, 10, Alineado.Derecha);
                        //    totalesY += 15;
                        //}

                        pdf.EscribirXY("Importe Total: $" + Math.Abs(comprobante.ImpTotal).ToString("N2"), 570, 800, 15, Alineado.Derecha);
                    }
                    if (!esRemito && !esRecibo && !esPresupuesto && !esOrdenDeCompra)
                    {
                        if (comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR &&
                            comprobante.TipoComprobante != FETipoComprobante.SIN_DEFINIR_RECIBO &&
                                      comprobante.TipoComprobante != FETipoComprobante.PAGO &&
                            !string.IsNullOrEmpty(comprobante.CAE))
                        {
                            pdf.EscribirXY("CAE: " + comprobante.CAE, 27, 770, 8, Alineado.Izquierda);
                            pdf.EscribirXY("Vencimiento CAE: " + comprobante.FechaVencCAE.ToString("dd/MM/yyyy"), 150, 770, 8, Alineado.Izquierda);
                            pdf.InsertarImagenXY(ImagenCodigoBarras(comprobante), 27, 45);
                            pdf.EscribirXY(CodigoDeBarras(comprobante), 27, 808, 10, Alineado.Izquierda);
                        }
                    }
                    #endregion

                    if (!esRemito)
                    {
                        pdf.EscribirBoxXY("Son Pesos " + NumeroALetrasMoneda(comprobante.ImpTotal) + ".", 27, 680, 10, 380);
                    }
                    else
                    {
                        if (comprobante.CondicionIva == "Monotributo")
                            pdf.EscribirBoxXY("VPS: $" + Math.Abs(comprobante.ItemsDetalle.Sum(i => i.Total)).ToString("N2"), 27, 680, 10, 400);
                        else
                            pdf.EscribirBoxXY("VPS: $" + Math.Abs(comprobante.ImpNeto + comprobante.ImpTotConc).ToString("N2"), 27, 680, 10, 400);
                    }



                    #endregion

                    if (cantPdf > 1)
                    {
                        pdf.NuevaPagina();
                        if (indexPdf < 2)
                        {
                            var reader = new PdfReader(template);
                            //var writer = PdfWriter.GetInstance(pdf.document, pdf.mem);
                            PdfImportedPage page = pdf.writer.GetImportedPage(reader, 1);
                            PdfContentByte cb = pdf.writer.DirectContent;
                            cb.AddTemplate(page, 0, 0);
                        }
                    }
                }

                return pdf.GenerarPDFStream();
            }


        }










        public string CodigoDeBarras(FEComprobante comprobante)
        {
            /*
            El código de barras deberá contener los siguientes datos con su correspondiente orden:
            *Clave Unica de Identificación Tributaria (C.U.I.T.) del emisor de la factura (11 caracteres
            *Código de tipo de comprobante (2 caracteres)
            *Punto de venta (4 caracteres)
            *Código de Autorización de Impresión (C.A.I.) (14 caracteres)
            *Fecha de vencimiento (8 caracteres)
            *Dígito verificador (1 carácter)
            */

            string codigo = comprobante.Cuit.ToString();
            codigo += ((int)comprobante.TipoComprobante).ToString().PadLeft(2, '0');
            codigo += comprobante.PtoVta.ToString().PadLeft(4, '0');
            codigo += comprobante.CAE;
            codigo += comprobante.FechaVencCAE.ToString("yyyyMMdd");
            codigo += DigitoVerificador(codigo);
            codigo = codigo.Replace("-", "");

            return codigo;

        }

        private System.Drawing.Image ImagenCodigoBarras(FEComprobante comprobante)
        {
            BarcodeInter25 bar = new BarcodeInter25();

            bar.ChecksumText = true;
            bar.GenerateChecksum = false;
            bar.Code = CodigoDeBarras(comprobante);

            return bar.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White);
        }

        private static int DigitoVerificador(string codigoBarras)
        {
            int digito;
            int pares = 0;
            int impares = 0;

            var aux = codigoBarras.Replace("-", "");

            for (int i = 0; i < aux.Length; i++)
            {
                if (i % 2 == 0)
                    pares = pares + int.Parse(aux.Substring(i, 1));
                else
                    impares = impares + int.Parse(aux.Substring(i, 1));
            }

            digito = 10 - ((pares + (3 * impares)) % 10);

            if (digito == 10)
                digito = 0;

            return digito;
        }

        private static string NumeroALetrasMoneda(double value)
        {
            string[] total = value.ToString().Replace(',', '.').Split('.');
            double total_entero = double.Parse(total[0]);
            double total_decimales = 0;

            if (total.Length > 1)
            {
                total[1] = (total[1] + "0").Substring(0, 2);
                total_decimales = double.Parse(total[1]);
            }

            return NumeroALetras(total_entero) + " con " + NumeroALetras(total_decimales);
        }

        private static string NumeroALetras(double value)
        {
            string Num2Text = "";

            value = Math.Abs(Math.Truncate(value));

            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + NumeroALetras(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + NumeroALetras(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";


            else if (value < 100) Num2Text = NumeroALetras(Math.Truncate(value / 10) * 10) + " Y " + NumeroALetras(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + NumeroALetras(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800))
                Num2Text = NumeroALetras(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000)
                Num2Text = NumeroALetras(Math.Truncate(value / 100) * 100) + " " + NumeroALetras(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + NumeroALetras(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = NumeroALetras(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + NumeroALetras(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + NumeroALetras(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = NumeroALetras(Math.Truncate(value / 1000000)) + " MILLONES ";

                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0)
                    Num2Text = Num2Text + " " + NumeroALetras(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000)
                Num2Text = "UN BILLON " +
                           NumeroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            else
            {
                Num2Text = NumeroALetras(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0)
                    Num2Text = Num2Text + " " +
                               NumeroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }

            return Num2Text;
        }
    }
}

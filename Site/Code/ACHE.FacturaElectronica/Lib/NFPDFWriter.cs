﻿using System;
using System.Data;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Linq;
using Image = System.Drawing.Image;
using System.Collections.Generic;
using ACHE.FacturaElectronicaModelo;
using System.Configuration;

namespace ACHE.FacturaElectronica.Lib
{

    /// <summary>
    /// Crea documentos PDF y permite insertar contenido
    /// </summary>
    public class NFPDFWriter
    {
        private readonly BaseFont font = BaseFont.CreateFont(FontFactory.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

        private readonly BaseFont font_titulo = BaseFont.CreateFont(FontFactory.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

        private readonly Font font_sub_titulo = FontFactory.GetFont(FontFactory.HELVETICA, 10);
        private readonly Font font_cell_titulo = FontFactory.GetFont(FontFactory.HELVETICA, 8);
        private readonly Font font_cell_detalle = FontFactory.GetFont(FontFactory.HELVETICA, 8);
     /*   private readonly Font font_cell_detalle_fact = FontFactory.GetFont(FontFactory.HELVETICA,6);*/

        private readonly Font font_cell_totales = FontFactory.GetFont(FontFactory.HELVETICA_BOLDOBLIQUE, 8);
        private string fileName = "Documento.pdf";
        private Orientacion _orientacion = Orientacion.Vertical;
        private PdfContentByte cb;
        public Document document;
        public readonly MemoryStream mem = new MemoryStream();
        private string autor = "";
        private string titulo = "Generado por Contabilium.com";
        public PdfWriter writer;
        private readonly string _titulo = "";
        private readonly string _subtitulo = "";


        public NFPDFWriter(MedidasDocumento medidaDocumento)
        {
            Inicializar(medidaDocumento, null, Orientacion.Vertical);
        }

        public NFPDFWriter(MedidasDocumento medidaDocumento, string titulo, string subtitulo)
        {
            _titulo = titulo;
            _subtitulo = subtitulo;
            Inicializar(medidaDocumento, null, Orientacion.Vertical);
        }

        public NFPDFWriter(MedidasDocumento medidaDocumento, Orientacion orientacion)
        {
            Inicializar(medidaDocumento, null, orientacion);
        }

        public NFPDFWriter(float width, float height)
        {
            Rectangle documentSize;
            documentSize = new Rectangle(width, height);
            Inicializar(documentSize, null, Orientacion.Vertical);
        }

        public NFPDFWriter(MedidasDocumento medidaDocumento, string fileTemplate)
        {
            Inicializar(medidaDocumento, fileTemplate, Orientacion.Vertical);
        }

        private void AgregarEncabezadoPagina(ref Document document, PdfWriter writer)
        {
            PdfContentByte cb;

            cb = writer.DirectContent;
            cb.BeginText();

            //imprimo el titulo
            cb.SetFontAndSize(font_titulo, 13);
            if (this._orientacion == Orientacion.Vertical)
                cb.ShowTextAligned(Element.ALIGN_CENTER, this._titulo, PageSize.A4.Width / 2, PageSize.A4.Height - 60, 0);
            else
                cb.ShowTextAligned(Element.ALIGN_CENTER, this._titulo, PageSize.A4.Rotate().Width / 2, PageSize.A4.Rotate().Height - 40, 0);

            //imprimo el subtitulo
            cb.SetFontAndSize(font_titulo, font_sub_titulo.Size);
            if (this._orientacion == Orientacion.Vertical)
                cb.ShowTextAligned(Element.ALIGN_CENTER, this._subtitulo, PageSize.A4.Width / 2, PageSize.A4.Height - 92, 0);
            else
                cb.ShowTextAligned(Element.ALIGN_CENTER, this._subtitulo, PageSize.A4.Rotate().Width / 2, PageSize.A4.Rotate().Height - 72, 0);


            //imprimo la fecha y hora de impresion
            cb.SetFontAndSize(font_titulo, 7);
            if (this._orientacion == Orientacion.Vertical)
                cb.ShowTextAligned(Element.ALIGN_RIGHT, "Generado el día " + DateTime.Now.ToString("dd/MM/yyyy") + " a las " + DateTime.Now.ToString("HH:mm"), PageSize.A4.Width - document.RightMargin, PageSize.A4.Height - 92, 0);
            else
                cb.ShowTextAligned(Element.ALIGN_RIGHT, "Generado el día " + DateTime.Now.ToString("dd/MM/yyyy") + " a las " + DateTime.Now.ToString("HH:mm"), PageSize.A4.Rotate().Width - document.RightMargin, PageSize.A4.Rotate().Height - 72, 0);

            cb.EndText();

            cb.SetLineWidth(0.5f);
            if (this._orientacion == Orientacion.Vertical)
            {
                cb.MoveTo(document.LeftMargin, PageSize.A4.Height - 95);
                cb.LineTo(PageSize.A4.Width - document.RightMargin, PageSize.A4.Height - 95);
            }
            else
            {
                cb.MoveTo(document.LeftMargin, PageSize.A4.Rotate().Height - 75);
                cb.LineTo(PageSize.A4.Rotate().Width - document.RightMargin, PageSize.A4.Rotate().Height - 75);
            }

            cb.Stroke();
        }

        private void Inicializar(MedidasDocumento medidaDocumento, string fileTemplate, Orientacion orientacion)
        {
            Rectangle documentSize;

            switch (medidaDocumento)
            {
                case MedidasDocumento.A4:
                    documentSize = PageSize.A4;
                    break;

                case MedidasDocumento.Carta:
                    documentSize = PageSize.LETTER;
                    break;

                case MedidasDocumento.Oficio:
                    documentSize = PageSize.LEGAL;
                    break;

                case MedidasDocumento.RemitoPolydem:
                    documentSize = new Rectangle(482, 652);
                    break;

                case MedidasDocumento.Etiquetas108mmX3:
                    documentSize = new Rectangle(425, 83);
                    break;

                default:
                    throw new Exception("No se definio un tamaño");
            }

            Inicializar(documentSize, fileTemplate, orientacion);
        }

        private void Inicializar(Rectangle documentSize, string fileTemplate, Orientacion orientacion)
        {
            this._orientacion = orientacion;

            if (this._orientacion == Orientacion.Vertical)
                document = new Document(documentSize);
            else
                document = new Document(documentSize.Rotate());

            writer = PdfWriter.GetInstance(document, mem);

            document.AddAuthor(autor);
            document.AddCreationDate();
            document.AddCreator("NFPDFWriter");
            document.AddTitle(titulo);

            if (_titulo != "")
            {
                // create add the event handler
                MyPageEvents events = new MyPageEvents();
                writer.PageEvent = events;
            }

            document.Open();
            document.NewPage();
            cb = writer.DirectContent;

            if (_titulo != "")
                AgregarEncabezadoPagina(ref document, writer);

            if (fileTemplate != null)
            {
                PdfReader reader = new PdfReader(fileTemplate);
                PdfImportedPage page = writer.GetImportedPage(reader, 1);
                cb.AddTemplate(page, 0, 0);
            }
        }

        #region Propiedades

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public Orientacion Orientacion
        {
            get { return _orientacion; }
        }

        public string Autor
        {
            get { return autor; }
            set { autor = value; }
        }

        public float Width
        {
            get { return document.PageSize.Width; }
        }

        public float Height
        {
            get { return document.PageSize.Height; }
        }

        #endregion

        #region Metodos

        public void Escribir(string texto, Alineado alineado)
        {
            int align = Element.ALIGN_LEFT;

            switch (alineado)
            {
                case Alineado.Centro:
                    align = Element.ALIGN_CENTER;
                    break;

                case Alineado.Justificado:
                    align = Element.ALIGN_JUSTIFIED;
                    break;

                case Alineado.Izquierda:
                    align = Element.ALIGN_LEFT;
                    break;

                case Alineado.Derecha:
                    align = Element.ALIGN_RIGHT;
                    break;
            }

            Paragraph p1 = new Paragraph();
            p1.Alignment = align;
            p1.Add(texto);
            document.Add(p1);
        }

        private MemoryStream PrepararPDF()
        {
            document.Close();
            return new MemoryStream(mem.ToArray());
        }

        private int Align(Alineado alineado)
        {
            int align = Element.ALIGN_LEFT;

            switch (alineado)
            {
                case Alineado.Centro:
                    align = Element.ALIGN_CENTER;
                    break;

                case Alineado.Justificado:
                    align = Element.ALIGN_JUSTIFIED;
                    break;

                case Alineado.Izquierda:
                    align = Element.ALIGN_LEFT;
                    break;

                case Alineado.Derecha:
                    align = Element.ALIGN_RIGHT;
                    break;
            }

            return align;
        }
        public void InsertarTablaDetalleOrden(List<FEItemDetalle> ItemsDetalle)
        {
            PdfPTable table = new PdfPTable(4);

            foreach (FEItemDetalle item in ItemsDetalle)
            {
                PdfPCell cellCantidad = new PdfPCell(new Phrase(ToMoneyFormat(item.Cantidad), font_cell_detalle));
                PdfPCell cellCodigo = new PdfPCell(new Phrase(item.Codigo, font_cell_detalle));
                PdfPCell cellDescripcion = new PdfPCell(new Phrase(item.Descripcion, font_cell_detalle));
                PdfPCell cellPrecio = new PdfPCell(new Phrase("$ " + item.PrecioParaPDF.ToString("N2"), font_cell_detalle));

                cellCantidad.BorderWidth = 0;
                cellCantidad.PaddingBottom = 5;
                cellCantidad.PaddingRight = 5;
                cellCantidad.PaddingLeft = 5;
                cellCantidad.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                table.AddCell(cellCantidad);

                cellCodigo.BorderWidth = 0;
                cellCodigo.PaddingBottom = 5;
                cellCodigo.PaddingRight = 5;
                cellCodigo.PaddingLeft = 5;
                cellCodigo.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table.AddCell(cellCodigo);

                cellDescripcion.BorderWidth = 0;
                cellDescripcion.PaddingBottom = 5;
                cellDescripcion.PaddingRight = 5;
                cellDescripcion.PaddingLeft = 5;
                cellDescripcion.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                table.AddCell(cellDescripcion);
                cellPrecio.BorderWidth = 0;
                cellPrecio.PaddingBottom = 5;
                cellPrecio.PaddingRight = 5;
                cellPrecio.PaddingLeft = 5;
                cellPrecio.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                table.AddCell(cellPrecio);

            }

            float[] cellWidths = new float[] { 59, 120, 220, 120 };

            table.SetWidths(cellWidths);
            table.TotalWidth = cellWidths.Sum();

            table.DefaultCell.Border = Rectangle.NO_BORDER;
            cb.SetFontAndSize(font, 10);

            int filas = 555;

            table.WriteSelectedRows(0, ItemsDetalle.Count, 22, filas, cb);
        }

        public int InsertarTablaDetalle(List<FEItemDetalle> ItemsDetalle, bool mostrarIva,float alto)
        {
            float h = 0;
            int cantInsertadas = 0;
            PdfPTable table = new PdfPTable(7);
            foreach (FEItemDetalle item in ItemsDetalle)
            {
                if (h < alto)
                {
                    int cantRenglones = 1;
                    var cantRenglonesDescripcion = int.Parse((item.Descripcion.Length / 49).ToString());
                    var cantRenglonesCodigo = int.Parse((item.Codigo.Length / 10).ToString());
                    if (cantRenglonesDescripcion > cantRenglonesCodigo)
                        cantRenglones = cantRenglonesDescripcion;
                    else
                        cantRenglones = cantRenglonesCodigo;

                    cantInsertadas++;
                    PdfPCell cellCantidad = new PdfPCell(new Phrase(ToMoneyFormat(item.Cantidad), font_cell_detalle));
                    PdfPCell cellCodigo = new PdfPCell(new Phrase(item.Codigo, font_cell_detalle));
                    PdfPCell cellDescripcion = new PdfPCell(new Phrase(item.Descripcion, font_cell_detalle));
                    PdfPCell cellPrecio = new PdfPCell(new Phrase("$ " + item.PrecioParaPDF.ToString("N2"), font_cell_detalle));
                    PdfPCell cellIva = new PdfPCell(new Phrase(mostrarIva ? (item.Iva.ToString("N2") + " %") : "", font_cell_detalle));
                    PdfPCell cellBonificacion = new PdfPCell(new Phrase(item.Bonificacion.ToString("N2") + " %", font_cell_detalle));
                    PdfPCell cellTotal = new PdfPCell(new Phrase("$ " + item.TotalPDF.ToString("N2"), font_cell_detalle));
                
                    cellCantidad.FixedHeight =  15f + (cantRenglones * 10f);
                    h += 15f + (cantRenglones * 10f);
                  
                    cellCantidad.BorderWidth = 0;
                    cellCantidad.PaddingBottom = 5;
                    cellCantidad.PaddingRight = 5;
                    cellCantidad.PaddingLeft = 5;
                    cellCantidad.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellCantidad);

                    cellCodigo.FixedHeight = 15f + (cantRenglones * 10f);

                    cellCodigo.BorderWidth = 0;
                    cellCodigo.PaddingBottom = 5;
                    cellCodigo.PaddingRight = 5;
                    cellCodigo.PaddingLeft = 5;
                    cellCodigo.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellCodigo);

                    cellDescripcion.FixedHeight = 15f + (cantRenglones * 10f);

                    
                    cellDescripcion.BorderWidth = 0;
                    cellDescripcion.PaddingBottom = 5;
                    cellDescripcion.PaddingRight = 5;
                    cellDescripcion.PaddingLeft = 5;
                    cellDescripcion.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellDescripcion);

                    cellPrecio.FixedHeight = 15f + (cantRenglones * 10f);

                    
                    cellPrecio.BorderWidth = 0;
                    cellPrecio.PaddingBottom = 5;
                    cellPrecio.PaddingRight = 5;
                    cellPrecio.PaddingLeft = 5;
                    cellPrecio.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellPrecio);

                    cellIva.FixedHeight = 15f + (cantRenglones * 10f);

                    
                    cellIva.BorderWidth = 0;
                    cellIva.PaddingBottom = 5;
                    cellIva.PaddingRight = 5;
                    cellIva.PaddingLeft = 5;
                    cellIva.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellIva);

                    cellBonificacion.FixedHeight = 15f + (cantRenglones * 10f);

                    
                    cellBonificacion.BorderWidth = 0;
                    cellBonificacion.PaddingBottom = 5;
                    cellBonificacion.PaddingRight = 5;
                    cellBonificacion.PaddingLeft = 5;
                    cellBonificacion.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellBonificacion);

                    cellTotal.FixedHeight = 15f + (cantRenglones * 10f);

                    
                    cellTotal.BorderWidth = 0;
                    cellTotal.PaddingBottom = 5;
                    cellTotal.PaddingRight = 5;
                    cellTotal.PaddingLeft = 5;
                    cellTotal.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellTotal);
                }
                

            }

            float[] cellWidths = new float[] { 59, 64, 213, 78, 40, 40, 65 };

            table.SetWidths(cellWidths);
            table.TotalWidth = cellWidths.Sum();
            

            table.DefaultCell.Border = Rectangle.NO_BORDER;
            cb.SetFontAndSize(font, 10);


            table.WriteSelectedRows(0, ItemsDetalle.Count, 22, 555, cb);


            return cantInsertadas;


        }
        public int InsertarTablaDetalleRemito(List<FEItemDetalle> ItemsDetalle,float alto)
        {
            PdfPTable table = new PdfPTable(3);
         
        
            float h = 0;
            int cantInsertadas = 0;
            foreach (FEItemDetalle item in ItemsDetalle)
            {

                if (h < alto)
                {      
                    var cantRenglones = int.Parse((item.Codigo.Length / 10).ToString());

                    cantInsertadas++;
                    h += 15f + (cantRenglones * 10f); 
                    PdfPCell cellCantidad = new PdfPCell(new Phrase(ToMoneyFormat(item.Cantidad), font_cell_detalle));
                    PdfPCell cellCodigo = new PdfPCell(new Phrase(item.Codigo, font_cell_detalle));
                    PdfPCell cellDescripcion = new PdfPCell(new Phrase(item.Descripcion, font_cell_detalle));
                    cellCantidad.FixedHeight = 15f + (cantRenglones * 10f);
                    cellCantidad.BorderWidth = 0;
                    cellCantidad.PaddingBottom = 5;
                    cellCantidad.PaddingRight = 5;
                    cellCantidad.PaddingLeft = 5;
                    cellCantidad.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellCantidad);
                    cellCodigo.FixedHeight = 15f + (cantRenglones * 10f);

                    cellCodigo.BorderWidth = 0;
                    cellCodigo.PaddingBottom = 5;
                    cellCodigo.PaddingRight = 5;
                    cellCodigo.PaddingLeft = 5;
                    cellCodigo.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellCodigo);
                    cellDescripcion.FixedHeight = 15f + (cantRenglones * 10f);

                    cellDescripcion.BorderWidth = 0;
                    cellDescripcion.PaddingBottom = 5;
                    cellDescripcion.PaddingRight = 5;
                    cellDescripcion.PaddingLeft = 5;
                    cellDescripcion.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cellDescripcion);
                }
             
                int filas = 555;
                float[] cellWidths = new float[] { 49, 74, 400 };

                table.SetWidths(cellWidths);
                table.TotalWidth = cellWidths.Sum();

                table.DefaultCell.Border = Rectangle.NO_BORDER;
                cb.SetFontAndSize(font, 8);
                table.WriteSelectedRows(0, ItemsDetalle.Count, 22, filas, cb);
              
            }
            return cantInsertadas;
        }

        private BaseColor GetColor(NFPDFColor color)
        {
            switch (color)
            {
                case NFPDFColor.BLACK:
                    return BaseColor.BLACK;
                case NFPDFColor.BLUE:
                    return BaseColor.BLUE;
                case NFPDFColor.CYAN:
                    return BaseColor.CYAN;
                case NFPDFColor.DARK_GRAY:
                    return BaseColor.DARK_GRAY;
                case NFPDFColor.GRAY:
                    return BaseColor.GRAY;
                case NFPDFColor.GREEN:
                    return BaseColor.GREEN;
                case NFPDFColor.LIGHT_GRAY:
                    return BaseColor.LIGHT_GRAY;
                case NFPDFColor.MAGENTA:
                    return BaseColor.MAGENTA;
                case NFPDFColor.ORANGE:
                    return BaseColor.ORANGE;
                case NFPDFColor.PINK:
                    return BaseColor.PINK;
                case NFPDFColor.RED:
                    return BaseColor.RED;
                case NFPDFColor.WHITE:
                    return BaseColor.WHITE;
                case NFPDFColor.YELLOW:
                    return BaseColor.YELLOW;
                default:
                    return BaseColor.BLACK;
            }
        }

        public void EscribirXY(string texto, int x, int y, int size, Alineado alineado)
        {
            EscribirXY(texto, x, y, size, alineado, NFPDFColor.BLACK);
        }
        public void insertarMarcaDeAgua(string pathMarcaAgua)
        {
            ///si la imagen existe inserta si no no. en la ruta del logo contatenado con idusuario!!!
            if (File.Exists(pathMarcaAgua))
            {
                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(pathMarcaAgua);
                var size = cb.PdfDocument.PageSize;
                img.ScaleToFit(size.Width, size.Height);


                img.SetAbsolutePosition(0, 250);

                cb.AddImage(img);
                cb.Stroke();
            }

        
        }

        public void EscribirXY(string texto, int x, int y, int size, Alineado alineado, NFPDFColor color)
        {
            int align = Align(alineado);

            cb.BeginText();
            cb.SetColorFill(GetColor(color));
            cb.SetFontAndSize(font, size);
            cb.ShowTextAligned(align, texto, x, document.PageSize.Height - y, 0);
            cb.EndText();
        }

        public void EscribirBoxXY(string texto, int x, int y, int size, float width)
        {
            string[] palabras = texto.Split(' ');
            string oracion = "";
            int i = 0;

            cb.BeginText();
            cb.SetFontAndSize(font, size);
            foreach (string palabra in palabras)
            {
                if (this.cb.GetEffectiveStringWidth(oracion + " " + palabra, false) <= width)
                    oracion = oracion + " " + palabra;
                else
                {
                    cb.ShowTextAligned(Element.ALIGN_LEFT, oracion.Trim(), x, document.PageSize.Height - (y + i), 0);
                    i += size;
                    oracion = palabra;
                }
            }

            if (oracion.Trim() != "")
                cb.ShowTextAligned(Element.ALIGN_LEFT, oracion.Trim(), x, document.PageSize.Height - (y + i), 0);


            cb.EndText();
        }

      /*  public void EscribirBoxXY(string texto, int x, int y, int size, float width,int newSize)
        {
            string[] palabras = texto.Split(' ');
            string oracion = "";
            int i = 0;

            cb.BeginText();
            cb.SetFontAndSize(font, newSize);
            foreach (string palabra in palabras)
            {
                if (this.cb.GetEffectiveStringWidth(oracion + " " + palabra, false) <= width)
                    oracion = oracion + " " + palabra;
                else
                {
                    cb.ShowTextAligned(Element.ALIGN_LEFT, oracion.Trim(), x, document.PageSize.Height - (y + i), 0);
                    i += size;
                    oracion = palabra;
                }
            }

            if (oracion.Trim() != "")
                cb.ShowTextAligned(Element.ALIGN_LEFT, oracion.Trim(), x, document.PageSize.Height - (y + i), 0);


            cb.EndText();
        }*/
      
        public void InsertarImagenXY(iTextSharp.text.Image imagen, int x, int y)
        {
            InsertarImagenXY(imagen, x, y, 100);
        }

        public void InsertarImagenXY(iTextSharp.text.Image imagen, int x, int y, float porcentajeTamaño)
        {
            imagen.ScalePercent(porcentajeTamaño);
            imagen.SetAbsolutePosition(x, y);
            cb.AddImage(imagen);
            cb.Stroke();
        }

        public void InsertarImagenXY(Image imagen, int x, int y)
        {
            InsertarImagenXY(imagen, x, y, 100);
        }

        public void InsertarImagenXY(Image imagen, int x, int y, float porcentajeTamaño)
        {
            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(imagen, ImageFormat.Jpeg);

            img.ScalePercent(porcentajeTamaño);
            img.SetAbsolutePosition(x, y);
            cb.AddImage(img);
            cb.Stroke();
        }

        public void InsertarImagenXYConTransparencia(string pathImage, int x, int y, float porcentajeTamaño)
        {
            iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(pathImage);
            imagen.ScaleAbsolute(200f, 60f);

            //imagen.ScalePercent(porcentajeTamaño);
            imagen.SetAbsolutePosition(x, y);
            cb.AddImage(imagen);
            cb.Stroke();
        }

        /// <summary>
        /// Genera el PDF y lo graba en el disco
        /// </summary>
        /// <param name="Path">Ubicacion fisica donde se generara el PDF, por ejemplo "C:\TEMP"</param>
        public void GenerarPDFEnDisco(string Path)
        {
            FileStream outStream = File.OpenWrite(Path + @"\" + this.FileName);
            PrepararPDF().WriteTo(outStream);
            outStream.Flush();
            outStream.Close();
        }

        public void NuevaPagina()
        {
            document.NewPage();
        }

        /// <summary>
        /// Genera el PDF y lo envia al explorador como download "Guardar como...", debe usarse Helpers.AddPostbackEvent(btnReporte); para que fuerze un postback
        /// </summary>
        public void GenerarPDF()
        {
            MemoryStream mems;

            mems = PrepararPDF();

            Byte[] byteArray = mems.ToArray();
            mems.Flush();
            mems.Close();

            HttpContext.Current.Response.BufferOutput = true;
            // Clear all content output from the buffer stream
            HttpContext.Current.Response.Clear();

            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + this.FileName);

            // Set the HTTP MIME type of the output stream
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            // Write the data
            HttpContext.Current.Response.BinaryWrite(byteArray);
            HttpContext.Current.Response.End();
        }

        public Stream GenerarPDFStream()
        {
            return PrepararPDF();
        }

        public static void MergeFiles(string destinationFile, string[] sourceFiles)
        {
            try
            {
                int f = 0;
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(sourceFiles[f]);
                // we retrieve the total number of pages
                int n = reader.NumberOfPages;
                // Console.WriteLine("There are " + n + " pages in the original file.");
                // step 1: creation of a document-object
                Document document = new Document(reader.GetPageSizeWithRotation(1));
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));
                // step 3: we open the document
                document.Open();
                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;
                int rotation;
                // step 4: we add content
                while (f < sourceFiles.Length)
                {
                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(reader.GetPageSizeWithRotation(i));
                        document.NewPage();
                        page = writer.GetImportedPage(reader, i);
                        rotation = reader.GetPageRotation(i);


                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                        Console.WriteLine("Processed page " + i);
                    }
                    f++;
                    if (f < sourceFiles.Length)
                    {
                        reader = new PdfReader(sourceFiles[f]);
                        // we retrieve the total number of pages
                        n = reader.NumberOfPages;

                    }
                }
                // step 5: we close the document
                document.Close();
            }
            catch
            {
                throw;
            }
        }

        public static bool MergePDFs(IEnumerable<string> fileNames, string targetPdf, int maxPages)
        {
            bool merged = true;
            using (FileStream stream = new FileStream(targetPdf, FileMode.Create))
            {
                Document document = new Document();
                PdfCopy pdf = new PdfCopy(document, stream);
                PdfReader reader = null;
                try
                {
                    document.Open();
                    foreach (string file in fileNames)
                    {
                        reader = new PdfReader(file);
                        if (reader.NumberOfPages > maxPages)
                        {
                            //pdf.AddPage(pdf.GetImportedPage(reader, 1));
                            pdf.AddPage(pdf.GetImportedPage(reader, 2));
                        }
                        else
                        {
                            pdf.AddDocument(reader);
                            //pdf.AddPage(pdf.GetImportedPage(reader, 1));
                            //pdf.AddPage(pdf.GetImportedPage(reader, 2));
                        }
                            
                        //cleanup this page, keeps a big memory leak away
                        pdf.FreeReader(reader);
                        reader.Close();
                    }
                }
                catch (Exception)
                {
                    merged = false;
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
                finally
                {
                    if (document != null)
                    {
                        document.Close();
                    }
                }
            }
            return merged;
        }

        public static byte[] MergePDFsInMemory(IEnumerable<string> fileNames, int maxPages)
        {
            Document document = new Document();
            using (MemoryStream ms = new MemoryStream())
            {
                PdfCopy pdf = new PdfCopy(document, ms);
                PdfReader reader = null;
                try
                {
                    document.Open();
                    foreach (string file in fileNames)
                    {
                        reader = new PdfReader(file);
                        if (reader.NumberOfPages > maxPages)
                        {
                            //pdf.AddPage(pdf.GetImportedPage(reader, 1));
                            pdf.AddPage(pdf.GetImportedPage(reader, 2));
                        }
                        else
                        {
                            pdf.AddDocument(reader);
                            //pdf.AddPage(pdf.GetImportedPage(reader, 1));
                            //pdf.AddPage(pdf.GetImportedPage(reader, 2));
                        }

                        //cleanup this page, keeps a big memory leak away
                        pdf.FreeReader(reader);
                        reader.Close();
                    }
                }
                catch (Exception)
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
                finally
                {
                    if (document != null)
                    {
                        document.Close();
                    }
                }

                return ms.GetBuffer();
            }
        }

        public void ExtractPage(string sourcePdfPath, string outputPdfPath, int pageNumber, string password = "")
        {
            PdfReader reader = null;
            Document document = null;
            PdfCopy pdfCopyProvider = null;
            PdfImportedPage importedPage = null;

            try
            {
                // Intialize a new PdfReader instance with the contents of the source Pdf file:
                reader = new PdfReader(sourcePdfPath);

                // Capture the correct size and orientation for the page:
                document = new Document(reader.GetPageSizeWithRotation(pageNumber));

                // Initialize an instance of the PdfCopyClass with the source 
                // document and an output file stream:
                pdfCopyProvider = new PdfCopy(document,
                    new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));

                document.Open();

                // Extract the desired page number:
                importedPage = pdfCopyProvider.GetImportedPage(reader, pageNumber);
                pdfCopyProvider.AddPage(importedPage);
                document.Close();
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool EsNumerico(string cadena)
        {
            decimal numero;
            return decimal.TryParse(cadena, out numero);
        }

        private string ToMoneyFormat(decimal target)
        {
            string strValue = target.ToString("#,0.0000"); //Get the stock string

            //If there is a decimal point present
            if (strValue.Contains(","))
            {
                //Remove all trailing zeros
                strValue = strValue.TrimEnd('0');

                //If all we are left with is a decimal point
                if (strValue.EndsWith(",")) //then remove it
                    strValue = strValue.TrimEnd(',');
            }

            return strValue;
        }

        #endregion
    }
}
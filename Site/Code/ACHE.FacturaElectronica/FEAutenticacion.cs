﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using ACHE.FacturaElectronicaModelo;
using ACHE.Model;

namespace ACHE.FacturaElectronica
{
    public static class FEAutenticacion
    {
        private static readonly Hashtable tickets = new Hashtable();
        private static object bloqueo = new object();

        public static AccesoAfip GetTicket(long cuit, string servicio, /*string urlWsaaWsdl, string certificadoAfip,*/ string modo)
        {
            //FELog.AppendToFileInfo(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FELogError"]), "Inicio obtención de  FEticket",string.Format("Cuit: {0}, Servicio:{1}",cuit,servicio));
            AccesoAfip ticket = null;
            using (var dbContext = new ACHEEntities())
            {
                var aux= cuit.ToString();
                ticket = dbContext.AccesoAfip.Where(a => a.Cuit == aux && a.Servicio == servicio).OrderByDescending(a => a.FechaVencimiento).FirstOrDefault();

                if (ticket == null || ticket.FechaVencimiento < DateTime.Now)
                {

                    //if(ticket!=null)
                    //    FELog.AppendToFileInfo(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FELogError"]), "Ticket vencido ", string.Format("Fecha vencimiento:{0},Cuit: {1}, Servicio:{2}",ticket.FechaVencimiento.ToString("yyyy-MM-dd hh:mm:ss tt"), cuit, servicio));
                    FETicket feticket;

                    try
                    {
                        feticket = GenerarTicket(cuit, servicio, modo);
                    }
                    catch (Exception ex)
                    {
                        //2do intento ya que hay veces que la afip tira excepcion por sobrecarga
                        feticket = GenerarTicket(cuit, servicio, modo);
                    }

                    if (ticket == null)
                    {
                        ticket = new AccesoAfip
                        {
                            Cuit = cuit.ToString(),
                            FechaCreacion = DateTime.Now,
                            Servicio = servicio
                        };
                    }
                    ticket.FechaVencimiento = feticket.Vencimiento;
                    ticket.Token = feticket.Token;
                    ticket.Firma = feticket.Sign;
                    ticket.IdAfip = feticket.UniqueID.ToString();

                    if (ticket.IDAccesoAfip == 0)
                    {
                      //  FELog.AppendToFileInfo(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FELogError"]), "Nuevo FEticket", string.Format("Cuit: {0}, Servicio:{1}", cuit, servicio));
                        dbContext.AccesoAfip.Add(ticket);
                    }
                    else
                    {
                        //FELog.AppendToFileInfo(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FELogError"]),"Se renovó la fecha de vencimiento del FEticket Existente en BD", string.Format("IdAccesoAfip:{0},Cuit: {1}, Servicio:{2}",ticket.IDAccesoAfip , cuit, servicio));
                    }

                    dbContext.SaveChanges();

                }
                //else
                //    FELog.AppendToFileInfo(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FELogError"]), "FEticket Existente en BD y válido", string.Format("IdAccesoAfip:{0},Cuit: {1}, Servicio:{2}", ticket.IDAccesoAfip, cuit, servicio));
            }
            
            //FELog.AppendToFileInfo(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FELogError"]), "Fin obtención de  FEticket", string.Format("Cuit: {0}, Servicio:{1}", cuit, servicio));
            return ticket;
            
        }

        private static FETicket GenerarTicket(long cuit, string servicio, /*string urlWsaaWsdl, string certificadoAfip,*/ string modo)
        {
            FETicket ticket = new FETicket();
            LoginTicket objTicketRespuesta;
            string strTicketRespuesta;
            //string strUrlWsaaWsdl = (modo == "QA" ? ConfigurationManager.AppSettings["FE.QA.wsaa"] : ConfigurationManager.AppSettings["FE.PROD.wsaa"]);
            //string strRutaCertSigner = (modo == "QA" ?  HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.QA.CertificadoAFIP"]) :  HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.PROD.CertificadoAFIP"]));
            string strUrlWsaaWsdl = (modo == "QA" ? ConfigurationManager.AppSettings["FE.QA.wsaa"] : ConfigurationManager.AppSettings["FE.PROD.wsaa"]);
            string strRutaCertSigner = (modo == "QA" ? ConfigurationManager.AppSettings["FE.QA.CertificadoAFIP"] : ConfigurationManager.AppSettings["FE.PROD.CertificadoAFIP"]);

            try
            {
                if (!File.Exists(strRutaCertSigner))
                    throw new Exception("El certificado no existe en " + strRutaCertSigner);

                objTicketRespuesta = new LoginTicket();
                strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse(servicio, strUrlWsaaWsdl, strRutaCertSigner, false);

                ticket.Cuit = cuit;
                ticket.Creado = objTicketRespuesta.GenerationTime;
                ticket.Servicio = objTicketRespuesta.Service;
                ticket.Sign = objTicketRespuesta.Sign;
                ticket.Token = objTicketRespuesta.Token;
                ticket.UniqueID = objTicketRespuesta.UniqueId;
                ticket.Vencimiento = objTicketRespuesta.ExpirationTime;
                return ticket;
            }
            catch (Exception excepcionAlObtenerTicket)
            {
                throw excepcionAlObtenerTicket;
            }

           
        }
    }
}
﻿namespace ACHE.FacturaElectronicaModelo
{
    public enum FETipoComprobante
    {
        SIN_DEFINIR = -1,
        SIN_DEFINIR_RECIBO = -2,
        FACTURAS_A = 1,
        NOTAS_DEBITO_A = 2,
        NOTAS_CREDITO_A = 3,
        RECIBO_A = 4,
        FACTURAS_B = 6,
        NOTAS_DEBITO_B = 7,
        NOTAS_CREDITO_B = 8,

        FACTURAS_M = 51,
        NOTAS_DEBITO_M = 52,
        NOTAS_CREDITO_M = 53,

        RECIBO_B = 9,
        FACTURAS_C = 11,
        NOTAS_DEBITO_C = 12,
        NOTAS_CREDITO_C = 13,
        RECIBO_C = 15,
        FACTURAS_E = 19,
        NOTAS_DEBITO_E = 20,
        NOTAS_CREDITO_E = 21,

        PRESUPUESTO = 999,
        REMITO = 91,
        TIQUE = 83,
        OTROS = 99,
        ORDEN_COMPRAS = 100,
        PAGO=101,

        ORDEN_VENTA = 102
    }
}
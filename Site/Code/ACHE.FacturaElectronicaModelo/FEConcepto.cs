﻿namespace ACHE.FacturaElectronicaModelo
{
    public enum FEConcepto
    {
        Producto = 1,
        Servicio = 2,
        ProductoYServicio = 3,
        Otro = 4
    }
}

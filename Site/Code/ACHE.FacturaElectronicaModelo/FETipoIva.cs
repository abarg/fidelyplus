﻿namespace ACHE.FacturaElectronicaModelo
{
    public enum FETipoIva
    {
        Iva0 = 3,
        Iva10_5 = 4,
        Iva21 = 5,
        Iva27 = 6,
        Iva5 = 8,
        Iva2_5 = 9
    }
}

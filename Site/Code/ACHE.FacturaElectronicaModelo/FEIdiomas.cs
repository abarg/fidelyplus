﻿namespace ACHE.FacturaElectronicaModelo
{
    public enum FEIdioma
    {
        Español = 1,
        Ingles = 2,
        Portugues = 3
    }
}
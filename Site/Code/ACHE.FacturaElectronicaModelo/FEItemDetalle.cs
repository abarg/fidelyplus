﻿namespace ACHE.FacturaElectronicaModelo
{
    public class FEItemDetalle
    {
        public decimal Cantidad { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public double Precio { get; set; }
        public double PrecioParaPDF { get; set; }
        public decimal Bonificacion { get; set; }
        public decimal Iva { get; set; }
        public double Total
        {
            get
            {
                return double.Parse(Cantidad.ToString()) * Precio;
            }
        }

        public double TotalPDF
        {
            get
            {
                return (double.Parse(Cantidad.ToString()) * PrecioParaPDF)-this.ImporteBonificacion;
            }
        }

        public double ImporteBonificacion
        {
            get
            {
                if (Bonificacion > 0)
                {
                    return ((double.Parse(Cantidad.ToString()) * PrecioParaPDF * double.Parse(Bonificacion.ToString())) / 100);
                }
                else
                    return 0;
            }
        }
    }
}

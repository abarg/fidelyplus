﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ACHE.Admin
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            /*if (Roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }*/
            return true;

        }

        public CustomPrincipal(IIdentity identity)
        {
            this.Identity = identity;
        }

        public CustomPrincipal(string Username)
        {
            this.Identity = new GenericIdentity(Username);
        }

        public AdminWebUser CurrentUser { get; set; }
        //public string[] Roles { get; set; }
    }

    public class AdminWebUser
    {
        public int IDUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
    }


    public class CustomPrincipalSerializeModel
    {
        public int IDUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        //public string[] Roles { get; set; }

        public CustomPrincipalSerializeModel(string usuario, string nombre, string apellido, string email, int idUsuario)
        {
            this.IDUsuario = idUsuario;
            this.NombreUsuario = usuario;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Email = email;
        }
    }
}
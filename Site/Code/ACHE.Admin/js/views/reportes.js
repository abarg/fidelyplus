﻿var rptUsuarios = {
    filtrar: function () {

        $("#divError").hide();
        $("#resultsContainer").html("");
        var currentPage = parseInt($("#hdnPage").val());
        var info = "{ integraciones: '" + $("#ddlIntegraciones").val()
                   + "', periodo: '" + $("#ddlPeriodo").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "', metodoDePago: '" + $("#ddlMetodoDePago").val()
                   + "', formaDePago: '" + $("#ddlFormaDePago").val()
                   + "', tipoPlan: '" + $("#ddlTipoPlan").val()
                   + "', codigoPromo: '" + $("#txtCodigoPromo").val()
                   + "', estadoUsuario: '" + $("#ddlEstado").val()        
                   + "' , page: " + currentPage + ", pageSize: " + PAGE_SIZE
                   + "}";

        $.ajax({
            type: "POST",
            url: "/Reporte/ObtenerRptUsuarios",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (data.TotalPage > 0) {
                    $("#divPagination").show();

                    $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                    if (data.TotalPage == 1)
                        $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                    else if (currentPage == data.TotalPage)
                        $("#lnkNextPage").attr("disabled", "disabled");
                    else if (currentPage == 1)
                        $("#lnkPrevPage").attr("disabled", "disabled");

                    var aux = (currentPage * PAGE_SIZE);
                    if (aux > data.TotalItems)
                        aux = data.TotalItems;
                    $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.TotalItems);
                }
                else {
                    $("#divPagination").hide();
                    $("#msjResultados").html("");
                }

                // Render using the template
                if (data.Items.length > 0)
                    $("#resultTemplate").tmpl({ results: data.Items }).appendTo("#resultsContainer");
                else
                    $("#noResultTemplate").tmpl({ results: data.Items }).appendTo("#resultsContainer");
            }
        });
        rptUsuarios.resetearExportacion();
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    configFilters: function () {

        $(".select2").select2({ width: '100%', allowClear: true });

        $("#txtCondicion, #txtCodigoPromo").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                rptUsuarios.resetearPagina();
                rptUsuarios.filtrar();
                return false;
            }
        });

        // Date Picker
        Common.configDatePicker();
        //Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    },
    otroPeriodo: function () {
        if ($("#ddlPeriodo").val() == "-1")
            $('#divMasFiltros').toggle(600);
        else {
            if ($("#divMasFiltros").is(":visible"))
                $('#divMasFiltros').toggle(600);

            $("#txtFechaDesde,#txtFechaHasta").val("");
            rptUsuarios.resetearPagina();
            rptUsuarios.filtrar();
        }
    },
    exportar: function () {
        rptUsuarios.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();
        var info = "{ integraciones: '" + $("#ddlIntegraciones").val()
                   + "', periodo: '" + $("#ddlPeriodo").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "', metodoDePago: '" + $("#ddlMetodoDePago").val()
                   + "', formaDePago: '" + $("#ddlFormaDePago").val()
                   + "', tipoPlan: '" + $("#ddlTipoPlan").val()
                   + "', codigoPromo: '" + $("#txtCodigoPromo").val()
                   + "', estadoUsuario: '" + $("#ddlEstado").val()
                    + "'}";

        $.ajax({
            type: "POST",
            url: "/Reporte/exportRptUsuarios",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data);
                    $("#lnkDownload").attr("download", data);
                }
            }
        });
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        rptUsuarios.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        rptUsuarios.filtrar();
    },
    loginAutomatico: function (id) {
        window.open("https://app.contabilium.com/loginAut.aspx?Id=XX-" + id + "-985YY2344478678");
    },
    editar: function (id, idpadre) {
        //window.location.href = "/Usuario/Edicion/" + id;
        if (idpadre > 0)
            window.open("/Usuario/Edicion/" + idpadre);
        else
            window.open("/Usuario/Edicion/" + id);
    },

}
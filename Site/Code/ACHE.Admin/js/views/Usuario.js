﻿var listaParametrosAddin = new Array();
var contadorP = 0;

function sumacontadorP() {
    contadorP = contadorP + 1;
    return contadorP;
}

var usuario = {
    reenviarPwd: function (id, nombre) {
        bootbox.confirm("¿Está seguro que desea enviarle la contraseña por email a " + nombre + "?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: "/Usuario/RenviarPwd",
                    data: "{ id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data == true) {
                            alert("El pwd fue renviado.");
                        }
                        else {
                            alert("El pwd no pudo ser renviado.");
                        }
                    }
                });
            }
        });
    },
    grabarPlanPago: function (id, idUsuario) {
        if ($('#frmEdicion').valid()) {
            Common.mostrarProcesando("btnActualizarInfo");
            Common.mostrarProcesando("btnActualizarDatosInternos");
            $("#divError").hide();
            var info = "{ id: " + id
                + " ,idUsuario: " + idUsuario
                + " ,fechaInicioPlan: '" + $("#txtFechaInicioPlan").val()
                + "' , fechaVencimiento: '" + $("#txtFechaVencimiento").val()
                + "', fechaPago: '" + $("#txtFechaPago").val()
                + "', importe: '" + $("#txtImporte").val()
                + "', formaPago: '" + $("#ddlFormasDePago").val()
                + "', nroReferencia: '" + $("#txtNroReferencia").val()
                + "' , estado: '" + $("#ddlEstados").val()
                + "' , planes: '" + $("#ddlPlanes").val()
                + "' , pagoAnual: " + $('#chkPagoAnual').is(":checked")
                + " }";

            $.ajax({
                type: "POST",
                url: "/Usuario/GuardarPlanDePago",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    Common.ocultarProcesando("btnActualizarInfo", "Actualizar");
                    Common.ocultarProcesando("btnActualizarDatosInternos", "Actualizar");

                    if (data) {
                        $("#divOk").show();
                        window.location.href = "../Edicion/" + idUsuario;
                    } else {
                        $("#divError").show();
                        $("#msgError").html("no se guardaron los cambios");
                    }
                }
            });

        } else {
            return false;
        }

    },
    grabar: function (id) {

        if ($('#frmEdicion').valid()) {

            Common.mostrarProcesando("btnActualizarInfo");
            Common.mostrarProcesando("btnActualizarDatosInternos");
            $("#divError").hide();
            var info = "{ id: " + id
                    + " , condicionIva: '" + $("#ddlCondicionIva").val()
                    + "', celular: '" + $("#txtCelular").val()
                    + "', email: '" + $("#txtEmail").val()
                    + "', observaciones: '" + $("#txtObservaciones").val()
                    + "', factElectronica: " + $("#chkFactElectronica").is(':checked')
                    + " , usaProd: " + $("#chkUsaProd").is(':checked')
                    + " , cantEmpresasHabilitadas: " + $("#txtCantEmpresasHabilitadas").val()
                    + " , activo: " + $("#ddlActivo").val()
                    + " , fechaBaja: '" + $("#txtFechaBaja").val()
                    + "', motivoBaja: '" + $("#txtMotivoBaja").val()
                    + "', estaBloqueado: " + $("#ddlBloqueado").val()
                    + " , estaBloqueadoAd: " + $("#ddlBloqueadoAd").val()
                    + " }";

            $.ajax({
                type: "POST",
                url: "/Usuario/Guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    Common.ocultarProcesando("btnActualizarInfo", "Actualizar");
                    Common.ocultarProcesando("btnActualizarDatosInternos", "Actualizar");

                    if (data) {
                        $("#divError").hide();
                        $("#divOk").show();
                    }
                    else {
                        $("#divOk").hide();
                        $("#divError").show();
                        $("#msgError").html("El email ya se encuentra registrado");
                    }
                }
            });

        }
        else {
            return false;
        }
    },
    config: function (editMode) {
        $("#txtCantEmpresasHabilitadas, #txtDescuento,#txtCBU, #txtCantidadAddin, #txtBonificacionAddin").numericInput();
        $(".select2").select2({ width: '100%', allowClear: true });
        //Common.configTelefono("txtTelefono");
        if (!editMode) {
            $('#txtFechaPago').val('');
        }
        if ($('#txtFechaPago').val() != undefined)
            $('#txtFechaPago').val($('#txtFechaPago').val().split(' ')[0]);
        if ($('#txtFechaInicioPlan').val() != undefined)
            $('#txtFechaInicioPlan').val($('#txtFechaInicioPlan').val().split(' ')[0]);
        if ($('#txtFechaVencimiento').val() != undefined)
            $('#txtFechaVencimiento').val($('#txtFechaVencimiento').val().split(' ')[0]);
        if ($('#txtDesdeAddin').val() != undefined)
            $('#txtDesdeAddin').val($('#txtDesdeAddin').val().split(' ')[0]);
        if ($('#txtHastaAddin').val() != undefined)
            $('#txtHastaAddin').val($('#txtHastaAddin').val().split(' ')[0]);

        $('#txtFechaInicioPlan,#txtFechaPago,#txtFechaVencimiento,#txtDesdeAddin,#txtHastaAddin').datepicker();
        Common.configDatePicker();


        obtenerCantidadDecimales(function (serverData) {
            $("#txtImporte").maskMoney({
                thousands: '',
                decimal: '.',
                allowZero: true,
                precision: serverData.d
            });
        });
    },
    habilitarFCEmpresas: function (id, nombre, tipo) {
        var accion = "";
        if (tipo == true)
            accion = "habilitar";
        else
            accion = "inabilitar";

        bootbox.confirm("¿Está seguro que desea <b> " + accion + "</b> la FCE y usaProd a la empresa: " + nombre + "?", function (result) {
            if (result) {

                var info = "{ id: " + id + ", tipo:" + tipo + " }";
                $.ajax({
                    type: "POST",
                    url: "/Usuario/HabilitarFCEmpresas",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });
    },
    habilitarPuntoVenta: function (id) {
        var info = "{ id: " + id + " }";
        $.ajax({
            type: "POST",
            url: "/Usuario/HabilitarPuntoVenta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                location.reload();
            }
        });
    },
    aceptarPago: function (id, idUsuario, nombre) {
        bootbox.confirm("¿Está seguro que desea <b> Aceptar </b> el pago de la empresa: " + nombre + "?", function (result) {
            if (result) {

                var info = "{ id: " + id + " ,idUsuario:" + idUsuario + "}";
                $.ajax({
                    type: "POST",
                    url: "/Usuario/AceptarPago",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data.TieneError) {
                            alert(data.Mensaje)
                        }
                        else {
                            alert("Comprobante electronico y cobranza fueron generados correctamente.")
                            location.reload();
                        }

                    }
                });
            }
        });
    },
    eliminarPago: function (id, idUsuario, nombre) {
        bootbox.confirm("¿Está seguro que desea <b> Eliminar </b> el pago de la empresa: " + nombre + "?", function (result) {
            if (result) {

                var info = "{ id: " + id + " ,idUsuario:" + idUsuario + "}";
                $.ajax({
                    type: "POST",
                    url: "/Usuario/EliminarPlanDePago",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data) {
                            $("#divOk").show();
                            location.reload();
                        } else {
                            $("#divError").show();
                            $("#msgError").html("No se pudo eliminar el pago.");
                        }
                    }
                });
            }
        });
    },
    /*** SEARCH ***/
    
    editar: function (id, idpadre) {
        //window.location.href = "/Usuario/Edicion/" + id;
        if (idpadre > 0)
            window.open("/Usuario/Edicion/" + idpadre);
        else
            window.open("/Usuario/Edicion/" + id);
    },
    configFilters: function () {

        $(".select2").select2({ width: '100%', allowClear: true });

        $("#txtCondicion, #txtCodigoPromo").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                usuario.resetearPagina();
                usuario.filtrar();
                return false;
            }
        });

        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        usuario.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        usuario.filtrar();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    },
    filtrar: function () {

        $("#divError").hide();
        $("#resultsContainer").html("");
        var currentPage = parseInt($("#hdnPage").val());
        var info = "{ condicion: '" + $("#txtCondicion").val()
                   + "', periodo: '" + $("#ddlPeriodo").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "', tipoPlan: '" + $("#ddlTipoPlan").val()
                   + "', EstadoUsuario: '" + $("#ddlEstado").val()
                   + "', CodigoPromo: '" + $("#txtCodigoPromo").val()
                   + "' , page: " + currentPage + ", pageSize: " + PAGE_SIZE
                   + "}";

        $.ajax({
            type: "POST",
            url: "/Usuario/ObtenerUsuarios",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                if (data.TotalPage > 0) {
                    $("#divPagination").show();

                    $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                    if (data.TotalPage == 1)
                        $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                    else if (currentPage == data.TotalPage)
                        $("#lnkNextPage").attr("disabled", "disabled");
                    else if (currentPage == 1)
                        $("#lnkPrevPage").attr("disabled", "disabled");

                    var aux = (currentPage * PAGE_SIZE);
                    if (aux > data.TotalItems)
                        aux = data.TotalItems;
                    $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.TotalItems);
                }
                else {
                    $("#divPagination").hide();
                    $("#msjResultados").html("");
                }

                // Render using the template
                if (data.Items.length > 0)
                    $("#resultTemplate").tmpl({ results: data.Items }).appendTo("#resultsContainer");
                else
                    $("#noResultTemplate").tmpl({ results: data.Items }).appendTo("#resultsContainer");
            }
        });
        usuario.resetearExportacion();
    },
    exportar: function () {
        usuario.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();
        var info = "{ condicion: '" + $("#txtCondicion").val()
                    + "', periodo: '" + $("#ddlPeriodo").val()
                    + "', fechaDesde: '" + $("#txtFechaDesde").val()
                    + "', fechaHasta: '" + $("#txtFechaHasta").val()
                    + "', tipoPlan: '" + $("#ddlTipoPlan").val()
                    + "', EstadoUsuario: '" + $("#ddlEstado").val()
                    + "', CodigoPromo: '" + $("#txtCodigoPromo").val()
                    + "'}";

        $.ajax({
            type: "POST",
            url: "/Usuario/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data);
                    $("#lnkDownload").attr("download", data);
                }
            }
        });
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    otroPeriodo: function () {
        if ($("#ddlPeriodo").val() == "-1")
            $('#divMasFiltros').toggle(600);
        else {
            if ($("#divMasFiltros").is(":visible"))
                $('#divMasFiltros').toggle(600);

            $("#txtFechaDesde,#txtFechaHasta").val("");
            usuario.resetearPagina();
            usuario.filtrar();
        }
    },
    importaciones: function (id) {
        window.location.href = "/Importaciones/Index/" + id;
    },
    configurarPlanCorporativo: function (id) {
        $("#divError,#divOk").hide();
        var info = "{ idUsuario: " + id + " }";
        $.ajax({
            type: "POST",
            url: "/Usuario/ConfigurarPlanCorporativo",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.TieneError) {
                    $("#divError").show();
                    $("#msgError").html(data.Mensaje);
                    usuario.showBtnPlanCorporativo("False");
                }
                else {
                    $("#divOk").show();
                    usuario.showBtnPlanCorporativo("True");
                }
            }
        });
    },
    inhabililitarPlanCorporativo: function (id) {
        $("#divError,#divOk").hide();
        var info = "{ idUsuario: " + id + " }";
        $.ajax({
            type: "POST",
            url: "/Usuario/InhabililitarPlanCorporativo",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.TieneError) {
                    $("#divError").show();
                    $("#msgError").html(data.Mensaje);
                    usuario.showBtnPlanCorporativo("True");
                }
                else {
                    $("#divOk").show();
                    usuario.showBtnPlanCorporativo("False");
                }
            }
        });
    },
    showBtnPlanCorporativo: function (tienePlanDeCuentas) {
        if (tienePlanDeCuentas == "True") {
            $("#divbtnPlanCorporativo").hide();
            $("#divbtnPlanNoCorporativo").show();
            $("#divTextPlanCorporativo").show();
            $("#spnEstadoPlanCorporativo").text("Habilitado");
        }
        else {
            $("#divbtnPlanCorporativo").show();
            $("#divbtnPlanNoCorporativo").hide();
            $("#divTextPlanCorporativo").hide();
            $("#spnEstadoPlanCorporativo").text("Inhabilitado");

        }
    },
    abrirModal: function () {

        $("#txtDescuento").val($.trim($("#spnDto").html()));
        $("#txtPromo").val($.trim($("#spnPromo").html()));
        $('#modalEdicion').modal('show');
    },
    guardarCambios: function (id) {
        $("#divError,#divOk").hide();
        var descuento = 0
        if ($("#txtDescuento").val() != "")
            descuento = $("#txtDescuento").val();

        var info = "{ idUsuario: " + id
            + ", descuento: " + descuento
            + ", promo: '" + $("#txtPromo").val()
            + "' }";
        $.ajax({
            type: "POST",
            url: "/Usuario/GuardarDescuentoYPromo",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.TieneError) {
                    $("#divError").show();
                    $("#msgError").html(data.Mensaje);
                    $('#modalEdicion').modal('hide');
                }
                else {
                    $("#spnDto").html(descuento);
                    $("#spnPromo").html($("#txtPromo").val());
                    $('#modalEdicion').modal('hide');

                    $("#divOk").show();
                }
            }
        });

    },
    eliminarPunto: function (idUsuario, id) {
        usuario.ocultarMensajes();

        var info = "{idUsuario: " + idUsuario + " , punto: " + parseInt(id) + "}";

        $.ajax({
            type: "POST",
            url: "/Usuario/eliminarPunto",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                usuario.obtenerPuntos(idUsuario);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorPuntos").html(r.Message);
                $("#divErrorPuntos").show();
            }
        });
    },
    habilitarPuntoVentaDinamico: function (idUsuario, id) {
        var info = "{id: " + parseInt(id) + "}";
        $.ajax({
            type: "POST",
            url: "/Usuario/HabilitarPuntoVenta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                usuario.obtenerPuntos(idUsuario);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorPuntos").html(r.Message);
                $("#divErrorPuntos").show();
            }
        });
    },
    agregarPunto: function (idUsuario) {
        usuario.ocultarMensajes();

        if ($("#txtNuevoPunto").val() != "") {

            Common.mostrarProcesando("btnActualizarPunto");
            var info = "{idUsuario: " + idUsuario + " , punto: " + parseInt($("#txtNuevoPunto").val()) + "}";

            $.ajax({
                type: "POST",
                url: "/Usuario/agregarPunto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    usuario.obtenerPuntos(idUsuario);
                    Common.ocultarProcesando("btnActualizarPunto", "Agregar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorPuntos").html(r.Message);
                    $("#divErrorPuntos").show();
                    Common.ocultarProcesando("btnActualizarPunto", "Agregar");
                }
            });

        }
        else {
            $("#msgErrorPuntos").html("Debes ingresar un valor");
            $("#divErrorPuntos").show();
        }
    },
    obtenerPuntos: function (idUsuario) {
        var info = "{idUsuario: " + idUsuario + "}";

        $.ajax({
            type: "POST",
            url: "/Usuario/obtenerPuntos",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data);
                }
            }
        });
    },
    ponerPorDefecto: function (idUsuario, idPunto) {
        $.ajax({
            type: "POST",
            data: "{idUsuario:" + idUsuario + ", idPunto: " + idPunto + " }",
            url: "/Usuario/GuardarPorDefecto/",
            //data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    usuario.obtenerPuntos(idUsuario);
                }
            }
        });
    },
    ponerPorDefectoIntegraciones: function (idUsuario, idPunto) {
        $.ajax({
            type: "POST",
            data: "{ idUsuario:" + idUsuario + ", idPunto: " + idPunto + " }",
            url: "/Usuario/GuardarPorDefectoIntegraciones/",
            //data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    usuario.obtenerPuntos(idUsuario);
                }
            }
        });
    },
    ocultarMensajes: function () {
        $("#divError, #divOk, #divErrorPuntos").hide();
    },
    aceptarCambioEstadoDA: function (cambiarPor, nombre, idusuario) {
        bootbox.confirm("¿Está seguro que desea <b> " + (cambiarPor == true ? "activar" : "desactivar") + " </b> el débito automático de la empresa: " + nombre + "?", function (result) {
            if (result) {

                var info = "{ idUsuario: " + idusuario + " ,estadoDebitoAutomatico:" + cambiarPor + "}";
                $.ajax({
                    type: "POST",
                    url: "/Usuario/CambiarEstadoDebitoAutomatico",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data.TieneError) {
                            alert(data.Mensaje)
                        }
                        //else {
                        //alert("El estado del débito automático fue cambiado correctamente.")
                        //location.reload();
                        //}

                    }
                });
            } else {
                $('#chkDebitoAutomatico').prop('checked', !cambiarPor)
            }
        });
    },
    separarEmpresaHija: function (id, nombre) {
        bootbox.confirm("¿Está seguro que desea <b>separar</b> esta empresa: " + nombre + "?", function (result) {
            if (result) {
                var info = "{ id: " + id + " }";
                $.ajax({
                    type: "POST",
                    url: "/Usuario/SepararEmpresaHija",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

    }
}
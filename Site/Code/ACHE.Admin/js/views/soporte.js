﻿var soporte = {
    recuperarCAE: function () {
        $("#divError,#divOk").hide();
        if ($('#txtNroComprobante,#txtCuitUsuario,#txtPunto,#ddlTipoComprobante').valid()) {
            Common.mostrarProcesando("btnActualizar");
            $("#divError").hide();
            var info = "{ nroComprobante: '" + $("#txtNroComprobante").val()
                     + "', cuitUsuario: '" + $("#txtCuitUsuario").val()
                     + "', punto: " + $("#txtPunto").val()
                     + ", tipoComprobante: '" + $("#ddlTipoComprobante").val()
                     + "' }";

            $.ajax({
                type: "POST",
                url: "/Soporte/RecuperarCae",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    Common.ocultarProcesando("btnActualizar", "Actualizar");
                    if (data.TieneError) {
                        $("#msgError").html(data.Mensaje);
                        $("#divError").show();
                    }
                    else {
                        $("#divOk").show();
                    }
                }
            });
        }
        else {
            return false;
        }
    },
    regenerarPDF: function () {
        $("#divError,#divOk").hide();
        if ($('#txtNroComprobante,#txtCuitUsuario,#txtPunto,#ddlTipoComprobante').valid()) {
            Common.mostrarProcesando("btnActualizar");
            $("#divError").hide();
            var info = "{ nroComprobante: '" + $("#txtNroComprobante").val()
                     + "', cuitUsuario: '" + $("#txtCuitUsuario").val()
                     + "', punto: " + $("#txtPunto").val()
                     + ", tipoComprobante: '" + $("#ddlTipoComprobante").val()
                     + "' }";

            $.ajax({
                type: "POST",
                url: "/Soporte/RegenerarPdf",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    Common.ocultarProcesando("btnActualizar", "Actualizar");
                    if (data.TieneError) {
                        $("#msgError").html(data.Mensaje);
                        $("#divError").show();
                    }
                    else {
                        $("#divOk").show();
                    }
                }
            });
        }
        else {
            return false;
        }
    },
    configPDF: function () {
        $("#txtNroComprobante").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                soporte.regenerarPDF();
                return false;
            }
        });
    },
    configCAE: function () {
        $("#txtNroComprobante").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                soporte.recuperarCAE();
                return false;
            }
        });

    },
    
    config: function () {
        $("#txtCuitUsuario,#txtPunto").numericInput();

        $('#txtFechaDesde, #txtFechaHasta').datepicker();
        Common.configDatePicker();


        $(".select2").select2({ width: '100%', allowClear: true, });
        // Validation with select boxes
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    configBase: function () {
        $(".number").numericInput();
        $(".select2").select2({ width: '100%', allowClear: true, });
        // Validation with select boxes
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    configCuit: function () {
        $("#txtCuitUsuario").numericInput();

        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.validator.addMethod("validCuit", function (value, element) {
            var check = true;
            return CuitEsValido($("#txtCuitUsuario").val());
        }, "CUIT/CUIL Inválido");
    },



}
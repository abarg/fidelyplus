﻿/*var m1 = new Date();
var m2 = new Date().addMonths(-1);
var m3 = new Date().addMonths(-2);
var m4 = new Date().addMonths(-3);
var m5 = new Date().addMonths(-4);
var m6 = new Date().addMonths(-5);
var m7 = new Date().addMonths(-6);
var m8 = new Date().addMonths(-7);
var m9 = new Date().addMonths(-8);
var m10 = new Date().addMonths(-9);
var m11 = new Date().addMonths(-10);
var m12 = new Date().addMonths(-11);

var ddataBa = [];
var ddataPy = [];
var ddataPr = [];
var ddataEm = [];
var ddataIn = [];
var ddataCort = [];
var ddataCorp = [];*/

var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var timeFormat = 'DD/MM/YYYY HH:mm';
var monthFormat = 'MMM';

function newMonth(months) {
    return moment().add(months, 'M').toDate();
}

function newMonthString(months) {
    return moment().add(months, 'M').format(monthFormat);
}

function newDate(days) {
    return moment().add(days, 'd').toDate();
}

function newDateString(days) {
    return moment().add(days, 'd').format(timeFormat);
}

function newTimestamp(days) {
    return moment().add(days, 'd').unix();
}


var home = {
    alertas: function () {

        var ddata = [];
        total = 0;

        $.ajax({
            type: "POST",
            url: "/Home/Alertas",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    //$("#lblAlertaImpagos").html(data[0].data);
                    $("#lblAlertaAnual").html(data[0].data);
                    $("#lblAlertaPendientes").html(data[1].data);
                    $("#lblAlertaAnualInactivos").html(data[2].data);

                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
    },
    tipoUsuarios: function () {

        var ddata = [];
        total = 0;

        $.ajax({
            type: "POST",
            url: "/Home/TiposDeUsuarios",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {

                    for (i = 0; i < data.length; i++) {
                        ddata.push([data[i].data]);
                        total += data[i].data;
                    }

                    //$("#chartUsuariosRegistradosDiasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

        var myDoughnutChart = new Chart(document.getElementById("chartTiposUsuarios"), {
            type: 'doughnut',
            data: {
                labels: ["Productos", "Servicios", "Productos y Servicios", "Contador", "Otro", "Sin info"],
                datasets: [
                  {
                      label: "Cantidad",
                      backgroundColor: [
                        "#2ecc71",
                        "#3498db",
                        "#95a5a6",
                        "#9b59b6",
                        "#f1c40f",
                        "#ccc"
                      ],
                      data: ddata
                  }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'left',
                },
                title: {
                    display: true,
                    text: 'Total de usuarios: ' + total
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });

    },
    usuariosPorProvincia: function () {

        var ddata = [];
        total = 0;

        $.ajax({
            type: "POST",
            url: "/Home/UsuariosPorProvincia",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {

                    for (i = 0; i < data.length; i++) {
                        ddata.push([data[i].data]);
                        total += data[i].data;
                    }

                    //$("#chartUsuariosRegistradosDiasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

        var myDoughnutChart = new Chart(document.getElementById("chartTiposUsuarios"), {
            type: 'doughnut',
            data: {
                labels: ["Productos", "Servicios", "Productos y Servicios", "Contador", "Otro", "Sin info"],
                datasets: [
                  {
                      label: "Cantidad",
                      backgroundColor: [
                        "#2ecc71",
                        "#3498db",
                        "#95a5a6",
                        "#9b59b6",
                        "#f1c40f",
                        "#ccc"
                      ],
                      data: ddata
                  }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'left',
                },
                title: {
                    display: true,
                    text: 'Total de usuarios: ' + total
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });

    },
    usuariosVentaOnline: function () {

        var ddata = [];
        total = 0;

        $.ajax({
            type: "POST",
            url: "/Home/UsuariosVentaOnline",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {

                    for (i = 0; i < data.length; i++) {
                        ddata.push([data[i].data]);
                        total += data[i].data;
                    }

                    //$("#chartUsuariosRegistradosDiasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

        var myDoughnutChart = new Chart(document.getElementById("chartUsuariosEcommerce"), {
            type: 'doughnut',
            data: {
                labels: ["Venta online", "Otros"],
                datasets: [
                  {
                      label: "Cantidad",
                      backgroundColor: [
                        "#2ecc71",
                        "#3498db"
                      ],
                      data: ddata
                  }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'left',
                },
                title: {
                    display: true,
                    text: 'Total de usuarios: ' + total
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });

    },
    integracionesCant: function () {

        var ddata = [];
        total = 0;

        $.ajax({
            type: "POST",
            url: "/Home/UsuariosIntegrados",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    
                    for (i = 0; i < data.length; i++) {
                        ddata.push([data[i].data]);
                        total += data[i].data;
                    }

                    //$("#chartUsuariosRegistradosDiasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

        var myDoughnutChart = new Chart(document.getElementById("chartCantIntegraciones"), {
            type: 'doughnut',
            data: {
                labels: ["MercadoLibre", "MercadoPago", "MercadoShops", "PayU", "TiendaNube"],
                datasets: [
                  {
                      label: "Cantidad",
                      backgroundColor: [
                        "#2ecc71",
                        "#3498db",
                        "#95a5a6",
                        "#9b59b6",
                        "#f1c40f"
                      ],
                      data: ddata
                  }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'left',
                },
                title: {
                    display: true,
                    text: 'Total de cuentas integrados: '+ total
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });

    },
    integracionesComprobantes: function () {

        var ddata = [];
        total = 0;

        $.ajax({
            type: "POST",
            url: "/Home/ComprobantesPorIntegracion",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {

                    for (i = 0; i < data.length; i++) {
                        ddata.push([data[i].data]);
                        total += data[i].data;
                    }

                    //$("#chartUsuariosRegistradosDiasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

        var myDoughnutChart = new Chart(document.getElementById("chartIntegracionesComp"), {
            type: 'doughnut',
            data: {
                labels: ["MercadoLibre", "MercadoPago", "MercadoShops", "PayU", "TiendaNube"],
                datasets: [
                  {
                      label: "Cantidad",
                      backgroundColor: [
                        "#2ecc71",
                        "#3498db",
                        "#95a5a6",
                        "#9b59b6",
                        "#f1c40f"
                      ],
                      data: ddata
                  }
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'left',
                },
                title: {
                    display: true,
                    text: 'Total de comprobantes emitidos: ' + total.toLocaleString('es-AR')
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            var currentValue = dataset.data[tooltipItem.index];
                            return currentValue.toLocaleString('es-AR').replace(",",".");
                        }
                    }
                }
                
                //tooltipTemplate: function (v) { return v.value.toLocaleString('es-AR'); }
            }
        });

    },
    usuariosRegistrados: function () {

        var ddata = [];

        $.ajax({
            type: "POST",
            url: "/Home/UsuariosRegistrados",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    total = 0;
                    //valorAnt = 0;
                    //porc =0;
                    for (i = 0; i < data.length; i++) {
                        if (i > 0) {
                            ddata.push([data[i].data]);
                            total += data[i].data;

                            /*if (valorAnt > 0)
                            {
                                porc += ((data[i].data - valorAnt) / valorAnt);
                            }*/

                            //valorAnt = data[i].data;
                        }
                    }
                    //alert((porc / 12)*100);
                    $("#chartUsuariosRegistradosDiasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });


        var color = Chart.helpers.color;
        var config = {
            type: 'line',
            data: {
                labels: [
                    newMonthString(-12),
					newMonthString(-11),
					newMonthString(-10),
					newMonthString(-9),
					newMonthString(-8),
					newMonthString(-7),
					newMonthString(-6),
                    newMonthString(-5),
					newMonthString(-4),
					newMonthString(-3),
					newMonthString(-2),
					newMonthString(-1),
					newMonthString(0)
                ],
                datasets: [{
                    label: "",
                    backgroundColor: [
                        "rgb(54, 162, 235)",

                    ],
                    //borderColor: window.chartColors.red,
                    fill: true,
                    data: ddata
                }]
            },
            options: {
                spanGaps: false,
                responsive: true,
                legend: {
                    display: false
                },
                title: {
                    display: false,
                    text: 'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                elements: {
                    line: {
                        tension: 0.000001
                    }
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Cantidad'
                        }
                    }]
                }
            }
        };

        var ctx = document.getElementById("chartUsuariosRegistradosDias").getContext("2d");
        window.myLine = new Chart(ctx, config);
    },
    bajasRegistradas: function () {

        var ddata = [];

        $.ajax({
            type: "POST",
            url: "/Home/BajasRegistradas",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    total = 0;
                    for (i = 0; i < data.length; i++) {
                        //if (i > 0) {
                        ddata.push([data[i].data]);
                        total += data[i].data;
                        //}
                    }

                    $("#chartBajasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });


        var color = Chart.helpers.color;
        var config = {
            type: 'line',
            data: {
                labels: [
                    newMonthString(-12),
					newMonthString(-11),
					newMonthString(-10),
					newMonthString(-9),
					newMonthString(-8),
					newMonthString(-7),
					newMonthString(-6),
                    newMonthString(-5),
					newMonthString(-4),
					newMonthString(-3),
					newMonthString(-2),
					newMonthString(-1),
					newMonthString(0)
                ],
                datasets: [{
                    label: "",
                    backgroundColor: [
                        "#D9534F",

                    ],
                    //borderColor: window.chartColors.red,
                    fill: true,
                    data: ddata
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                title: {
                    display: false,
                    text: 'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },

                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Cantidad'
                        }
                    }]
                }
            }
        };

        var ctx = document.getElementById("chartBajas").getContext("2d");
        window.myLine = new Chart(ctx, config);
    },
    altasRegistradas: function () {

        var ddata = [];

        $.ajax({
            type: "POST",
            url: "/Home/AltasRegistradas",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    total = 0;
                    for (i = 0; i < data.length; i++) {
                        //if (i > 0) {
                        ddata.push([data[i].data]);
                        total += data[i].data;
                        //}
                    }

                    $("#chartAltasLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });


        var color = Chart.helpers.color;
        var config = {
            type: 'line',
            data: {
                labels: [
                    newMonthString(-12),
					newMonthString(-11),
					newMonthString(-10),
					newMonthString(-9),
					newMonthString(-8),
					newMonthString(-7),
					newMonthString(-6),
                    newMonthString(-5),
					newMonthString(-4),
					newMonthString(-3),
					newMonthString(-2),
					newMonthString(-1),
					newMonthString(0)
                ],
                datasets: [{
                    label: "",
                    backgroundColor: [
                        "#a9d86e",

                    ],
                    //borderColor: window.chartColors.red,
                    fill: true,
                    data: ddata
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                title: {
                    display: false,
                    text: 'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },

                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Cantidad'
                        }
                    }]
                }
            }
        };

        var ctx = document.getElementById("chartAltas").getContext("2d");
        window.myLine = new Chart(ctx, config);
    },
    pagosAnuales: function () {

        var ddata = [];

        $.ajax({
            type: "POST",
            url: "/Home/PagosAnuales",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    total = 0;
                    for (i = 0; i < data.length; i++) {
                        if (i > 0) {
                            ddata.push([data[i].data]);
                            total += data[i].data;
                        }
                    }

                    $("#chartPagosAnualesLbl").html((total / 12).toFixed(2));
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });


        var color = Chart.helpers.color;
        var config = {
            type: 'line',
            data: {
                labels: [
                    newMonthString(-12),
					newMonthString(-11),
					newMonthString(-10),
					newMonthString(-9),
					newMonthString(-8),
					newMonthString(-7),
					newMonthString(-6),
                    newMonthString(-5),
					newMonthString(-4),
					newMonthString(-3),
					newMonthString(-2),
					newMonthString(-1),
					newMonthString(0)
                ],
                datasets: [{
                    label: "",
                    backgroundColor: [
                        "#FCB322",

                    ],
                    //borderColor: window.chartColors.red,
                    fill: true,
                    data: ddata
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                title: {
                    display: false,
                    text: 'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },

                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mes'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Cantidad'
                        }
                    }]
                }
            }
        };

        var ctx = document.getElementById("chartPagosAnuales").getContext("2d");
        window.myLine = new Chart(ctx, config);
    },
    usuariosPorPlan: function () {

        var ddataBa = [];
        var ddataPr = [];
        var ddataPy = [];
        var ddataEm = [];
        var ddataCorp = [];
        var ddataFree = [];

        $.ajax({
            type: "POST",
            url: "/Home/UsuariosPorPlan",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    total = 0;
                    for (i = 0; i < data.length; i++) {
                        //if (i > 0) {
                        ddataBa.push([data[i].Basico]);
                        ddataPr.push([data[i].Profesional]);
                        ddataPy.push([data[i].Pyme]);
                        ddataEm.push([data[i].Empresa]);
                        ddataCorp.push([data[i].Corporativo]);
                        ddataFree.push([data[i].Free]);
                        //}

                        if (i == (data.length - 1)) {
                            total = data[i].Basico + data[i].Profesional + data[i].Pyme + data[i].Empresa + data[i].Corporativo + data[i].Free;

                            $("#divBarBasico").css("width", ((data[i].Basico * 100) / total) + "%");
                            $("#divBarProfesional").css("width", ((data[i].Profesional * 100) / total) + "%");
                            $("#divBarPyme").css("width", ((data[i].Pyme * 100) / total) + "%");
                            $("#divBarEmpresa").css("width", ((data[i].Empresa * 100) / total) + "%");
                            $("#divBarCorporativo").css("width", ((data[i].Corporativo * 100) / total) + "%");
                            $("#divBarFree").css("width", ((data[i].Free * 100) / total) + "%");

                            $("#divBarTotalPagos").html("Total pagos mes actual " + (total - data[i].Free));

                            $("#divBarBasicoLbl").html("Basico (" + data[i].Basico + "/" + total + ") " + ((data[i].Basico * 100) / total).toFixed(2) + "%");
                            $("#divBarProfesionalLbl").html("Profesional (" + data[i].Profesional + "/" + total + ") " + ((data[i].Profesional * 100) / total).toFixed(2) + "%");
                            $("#divBarPymeLbl").html("Pyme (" + data[i].Pyme + "/" + total + ") " + ((data[i].Pyme * 100) / total).toFixed(2) + "%");
                            $("#divBarEmpresaLbl").html("Empresa (" + data[i].Empresa + "/" + total + ") " + ((data[i].Empresa * 100) / total).toFixed(2) + "%");
                            $("#divBarCorporativoLbl").html("Corporativo (" + data[i].Corporativo + "/" + total + ") " + ((data[i].Corporativo * 100) / total).toFixed(2) + "%");
                            $("#divBarFreeLbl").html("Free (" + data[i].Free + "/" + total + ") " + ((data[i].Free * 100) / total).toFixed(2) + "%");
                        }
                    }

                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });


        var config = {
            labels: [
                    //newMonthString(-6),
                    newMonthString(-5),
					newMonthString(-4),
					newMonthString(-3),
					newMonthString(-2),
					newMonthString(-1),
					newMonthString(0)
            ],
            datasets: [
                {
                    label: "Basico",
                    backgroundColor: '#FAAC58',
                    //fillColor: "rgba(220,220,220,0.5)",
                    //strokeColor: "rgba(220,220,220,1)",
                    data: ddataBa,

                },
                {
                    label: 'Profesional',
                    backgroundColor: 'rgb(237,194,64)',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataPr

                }
                ,
                {
                    label: 'Pyme',
                    backgroundColor: '#58FAF4',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataPy

                }
                ,
                {
                    label: 'Empresa',
                    backgroundColor: '#AC58FA',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataEm

                }
                ,
                {
                    label: 'Corporativo',
                    backgroundColor: '#DF013A',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataCorp

                }
                ,
                {
                    label: 'Free',
                    backgroundColor: '#E6E6E6',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataFree

                }
            ]

            //lineColors: ['#D9534F', '#1CAF9A', '#1CAFFB', '#1CFF9A'],

        };

        var ctx = document.getElementById("chartUsuariosPorPlan").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: config,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },

                title: {
                    display: false,
                    text: 'Chart.js Bar Chart'
                }
            }
        });

    },
    formasDePagoUtilizadas: function () {

        var ddataTr = [];
        var ddataMP = [];
        var ddataDep = [];
        //var ddataOtr = [];

        $.ajax({
            type: "POST",
            url: "/Home/FormasDePagoUtilizadas",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    total = 0;
                    for (i = 0; i < data.length; i++) {
                        //if (i > 0) {
                        ddataTr.push([data[i].Transferencia]);
                        ddataMP.push([data[i].MercadoPago]);
                        ddataDep.push([data[i].Deposito]);
                        //ddataOtr.push([data[i].Otros]);

                        //}
                    }

                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });


        var config = {
            labels: [
                    //newMonthString(-6),
                    newMonthString(-5),
					newMonthString(-4),
					newMonthString(-3),
					newMonthString(-2),
					newMonthString(-1),
					newMonthString(0)
            ],
            datasets: [
                {
                    label: "Transferencia",
                    backgroundColor: '#FAAC58',
                    //fillColor: "rgba(220,220,220,0.5)",
                    //strokeColor: "rgba(220,220,220,1)",
                    data: ddataTr,

                },
                {
                    label: 'Mercado Pago',
                    backgroundColor: '#DF013A',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataMP

                }
                ,
                {
                    label: 'Depósito',
                    backgroundColor: '#58FAF4',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataDep

                }
                /*,
                {
                    label: 'Otros',
                    backgroundColor: '#AC58FA',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataOtr

                }*/

            ]

            //lineColors: ['#D9534F', '#1CAF9A', '#1CAFFB', '#1CFF9A'],

        };

        var ctx = document.getElementById("chartFormasDePago").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: config,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },

                title: {
                    display: false,
                    text: 'Chart.js Bar Chart'
                }
            }
        });

    },
    comprobantesPorTipo: function () {

        var ddataCOT = [];
        var ddataOTROS = [];
        //var ddataOtr = [];

        $.ajax({
            type: "POST",
            url: "/Home/ComprobantesPorTipo",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {
                    totalcot = 0;
                    totalotros = 0;

                    for (i = 0; i < data.length; i++) {
                        //if (i > 0) {
                        ddataCOT.push([data[i].COT]);
                        ddataOTROS.push([data[i].OTROS]);

                        totalcot += parseInt([data[i].COT]);
                        totalotros += parseInt([data[i].OTROS]);



                        //}
                    }
                    $("#chartComprobantesCOTLbl").html((totalcot / 12).toFixed(2));
                    $("#chartComprobantesOTROSLbl").html((totalotros / 12).toFixed(2));


                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });


        var config = {
            labels: [
					newMonthString(-11),
					newMonthString(-10),
					newMonthString(-9),
					newMonthString(-8),
					newMonthString(-7),
					newMonthString(-6),
                    newMonthString(-5),
					newMonthString(-4),
					newMonthString(-3),
					newMonthString(-2),
					newMonthString(-1),
					newMonthString(0)
            ],
            datasets: [
                {
                    label: "COT",
                    backgroundColor: 'black',
                    //fillColor: "rgba(220,220,220,0.5)",
                    //strokeColor: "rgba(220,220,220,1)",
                    data: ddataCOT,

                },
                {
                    label: 'OTROS',
                    backgroundColor: '#4C8470',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataOTROS

                }
                /*,
                {
                    label: 'Otros',
                    backgroundColor: '#AC58FA',
                    //fillColor: "rgba(151,187,205,0.5)",
                    //strokeColor: "rgba(151,187,205,1)",
                    data: ddataOtr

                }*/

            ]

            //lineColors: ['#D9534F', '#1CAF9A', '#1CAFFB', '#1CFF9A'],

        };

        var ctx = document.getElementById("chartComprobantesPorTipo").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: config,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },

                title: {
                    display: false,
                    text: 'Chart.js Bar Chart'
                }
            }
        });

    },
    verDetalleAnualesInactivos: function () {
        $("#divDetalle").show();
        home.anualesInactivosDetalle();

        $('#modalDetalle').modal('show');

    },
    anualesInactivosDetalle: function () {
        $("#resultsContainerDetalle").html("");
        $.ajax({
            type: "POST",
            url: "/Home/ObtenerDetalleAnualesInactivos",
            dataType: "json",
            success: function (data) {
              

                if (data.length > 0) {
                    $("#resultTemplateDet").tmpl({ results: data }).appendTo("#resultsContainerDetalle");                    
                }
                else
                    $("#divDetalle").hide();
            }
        });
    },
    /*
    
    ObtenerPlanesPorMeses: function () {
        var elem = $('#BarUsuariosRegistrados');
        //var Planes = home.ObtenerPlanes();
        home.ObtenerPlanes();
        var data = [
                        { label: "Prueba", data: ddataIn },
                        { label: "Basico", data: ddataBa },
                        { label: "Profesional", data: ddataPy },
                        { label: "Pyme", data: ddataPr },
                        { label: "Empresa", data: ddataEm },
                        { label: "Corporativo", data: ddataCorp },
                        { label: "Free", data: ddataCort }
        ];

        var options = {
            xaxis: {
                min: 0,
                max: 7,
                mode: null,
                ticks: [
                    //[1, MONTH_NAMES_SHORT[m12.getMonth()]],
                    //[2, MONTH_NAMES_SHORT[m11.getMonth()]],
                    //[3, MONTH_NAMES_SHORT[m10.getMonth()]],
                    //[4, MONTH_NAMES_SHORT[m9.getMonth()]],
                    //[5, MONTH_NAMES_SHORT[m8.getMonth()]],
                    //[6, MONTH_NAMES_SHORT[m7.getMonth()]],
                    //[7, MONTH_NAMES_SHORT[m6.getMonth()]],
                    //[8, MONTH_NAMES_SHORT[m5.getMonth()]],
                    //[9, MONTH_NAMES_SHORT[m4.getMonth()]],
                    //[10, MONTH_NAMES_SHORT[m3.getMonth()]],
                    //[11, MONTH_NAMES_SHORT[m2.getMonth()]],
                    //[12, MONTH_NAMES_SHORT[m1.getMonth()]]
                    [1, MONTH_NAMES_SHORT[m6.getMonth()]],
                    [2, MONTH_NAMES_SHORT[m5.getMonth()]],
                    [3, MONTH_NAMES_SHORT[m4.getMonth()]],
                    [4, MONTH_NAMES_SHORT[m3.getMonth()]],
                    [5, MONTH_NAMES_SHORT[m2.getMonth()]],
                    [6, MONTH_NAMES_SHORT[m1.getMonth()]]
                ],
                tickLength: 0
            }, grid: {
                borderColor: '#ddd',
                hoverable: true,
                clickable: false,
                borderWidth: 1,
                backgroundColor: '#fff'
            }, legend: {
                labelBoxBorderColor: "none",
                position: "nw"
            }, series: {
                shadowSize: 1,
                bars: {
                    show: true,
                    barWidth: 0.13,
                    //fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
                    order: 1,
                    align: "left"
                }
            }
        };

        $.plot(elem, data, options);

        elem.bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.datapoint) {
                    previousPoint = item.datapoint;
                    $("#flot-tooltip").remove();

                    y = item.datapoint[1];
                    z = item.series.color;

                    home.showTooltip(item.pageX, item.pageY,
                    "<b>" + item.series.label + "</b> = " + y,
                    //"<b>Importe = </b> $" + y,
                    z);
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

        //Click
        //$("#BarUsuariosRegistrados").bind("plotclick", function (event, pos, item) {
        //    if (item) {
        //         //verDetalle(item.series.label, (item.dataIndex));
        //    }
        //});
    },
    showTooltip: function (x, y, contents) {
        jQuery('<div id="flot-tooltip" class="tooltipflot">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5
        }).appendTo("body").fadeIn(200);
    },
    ObtenerPlanes: function () {
        var ddata = [];

        $.ajax({
            type: "POST",
            url: "/Home/ObtenerPlanes",
            //data: "{ tiempo: " + tiempo + "}",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                if (data) {

                    for (i = 0; i < data.length; i++) {
                        ddataIn.push([data[i].Fecha, data[i].Prueba]);
                        ddataBa.push([data[i].Fecha, data[i].Basico]);
                        ddataPy.push([data[i].Fecha, data[i].Profesional]);
                        ddataPr.push([data[i].Fecha, data[i].Pyme]);
                        ddataEm.push([data[i].Fecha, data[i].Empresa]);
                        ddataCort.push([data[i].Fecha, data[i].Cortesia]);
                        ddataCorp.push([data[i].Fecha, data[i].Corporativo]);
                    }
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
    },

    ObtenerPlanesPorDias: function () {
        var data = home.ObtenerPlanesDias();

        new Morris.Line({
            element: 'BarUsuariosRegistradosDias',
            data: data,
            xkey: 'Fecha',
            xLabels: 'day',
            ykeys: ['Total'],
            //ykeys: ['Prueba', 'Basico', 'Profesional', 'Pyme', 'Empresa', 'Corporativo', 'Cortesia'],
            //labels: ['Prueba', 'Basico', 'Profesional', 'Pyme', 'Empresa', 'Corporativo', 'Cortesia'],
            //lineColors: ['#D9534F', '#1CAF9A', '#1CAFFB', '#1CFF9A'],
            lineWidth: '2px',
            hideHover: true
        });
    },
    ObtenerPlanesDias: function () {
        var ddata = [];
        $.ajax({
            type: "POST",
            url: "/Home/ObtenerRegistradosPorDia",
            //data: "{periodo: '" + periodo + "'}",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;

                for (i = 0; i < data.length; i++) {

                    var obj = new Object();
                    obj.Fecha = data[i].label
                    obj.Total = data[i].data;
                    ddata.push(obj);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
        return ddata;
    },

    ObtenerFormasPago: function () {
        var data = home.ObtenerFormasDePago();

        new Morris.Line({
            element: 'BarFormasDePago',
            data: data,
            xkey: 'Fecha',
            xLabels: 'month',
            ykeys: ['MercadoPago', 'Transferencia', 'Deposito'],
            labels: ['Mercado Pago', 'Transferencia', 'Deposito'],
            //lineColors: ['#D9534F', '#1CAF9A'],
            lineWidth: '2px',
            hideHover: true
        });
    },
    ObtenerFormasDePago: function () {
        var ddata = [];
        $.ajax({
            type: "POST",
            url: "/Home/ObtenerFormasDePago",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                for (i = 0; i < data.length; i++) {
                    var obj = new Object();
                    obj.Fecha = data[i].Fecha
                    obj.MercadoPago = data[i].MercadoPago
                    obj.Transferencia = data[i].Transferencia
                    ddata.push(obj);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
        return ddata;
    },
    
    ObtenerFacturacion: function () {
        var data = home.ObtenerDatosFacturacion();

        new Morris.Line({
            element: 'BarFacturacion',
            data: data,
            xkey: 'Fecha',
            xLabels: 'month',
            ykeys: ['ImporteTotal'],
            labels: ['Importe Total'],
            lineColors: ['#1CAF9A'],
            lineWidth: '2px',
            hideHover: true
        });
    },
    ObtenerDatosFacturacion: function () {
        var ddata = [];
        $.ajax({
            type: "POST",
            url: "/Home/ObtenerFacturacion",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                for (i = 0; i < data.length; i++) {
                    var obj = new Object();
                    obj.Fecha = data[i].Fecha
                    obj.ImporteTotal = data[i].ImporteTotal
                    ddata.push(obj);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
        return ddata;
    },

    ObtenerFacturacionDias: function () {
        var data = home.ObtenerDatosFacturacionDias();

        new Morris.Line({
            element: 'BarFacturacionDias',
            data: data,
            xkey: 'Fecha',
            xLabels: 'day',
            ykeys: ['ImporteTotal'],
            labels: ['Importe Total'],
            lineColors: ['#1CAF9A'],
            lineWidth: '2px',
            hideHover: true
        });
    },
    ObtenerDatosFacturacionDias: function () {
        var ddata = [];
        $.ajax({
            type: "POST",
            url: "/Home/ObtenerFacturacionDias",
            async: false,//wait for result
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, text) {
                var data = msg;
                for (i = 0; i < data.length; i++) {
                    var obj = new Object();
                    obj.Fecha = data[i].Fecha
                    obj.ImporteTotal = data[i].ImporteTotal
                    ddata.push(obj);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
        return ddata;
    },

    consultarUsuariosSegunPlan: function (plan, opcion) {

        var url = "/Usuario/Index?idPlan=" + plan + "&estado=" + opcion;
        window.open(url, '_blank');
        window.open(url);
    }
    */
}
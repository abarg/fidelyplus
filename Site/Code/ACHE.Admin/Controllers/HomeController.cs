﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACHE.Model;
using ACHE.Extensions;
using ACHE.Admin.Models;
using System.Configuration;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Common;

namespace ACHE.Admin.Controllers
{
    [OutputCache(Duration = 60)]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var model = new DashboardAdminViewModel();

            using (var dbContext = new ACHEEntities())
            {
                //Fix 
                if (System.IO.File.Exists(Server.MapPath("~/usuario")))
                {
                    System.IO.File.Delete(Server.MapPath("~/usuario"));
                }
                if (System.IO.File.Exists(Server.MapPath("~/soporte")))
                {
                    System.IO.File.Delete(Server.MapPath("~/soporte"));
                }

                var results = dbContext.Database.SqlQuery<RptAltasMensuales>("exec Admin_RptAltasMensuales 2016", new object[] { }).ToList();
                model.TotalAltas2016 = results.Sum(x => x.Cantidad);
                model.TotalBajas2016 = results.Sum(x => x.Bajas);
                model.TotalRegistros2016 = results.Sum(x => x.Registros);
                model.TotalAnuales2016 = dbContext.PlanesPagos.Where(x => x.FechaDePago.Year == 2016 && x.ImportePagado > 0 && x.PagoAnual && x.IDPlan != 6).Count(); ;
                model.PromedioAltas2016 = (decimal.Parse(model.TotalAltas2016.ToString()) / 12).ToString("0.00");
                model.PromedioBajas2016 = (decimal.Parse(model.TotalBajas2016.ToString()) / 12).ToString("0.00");
                model.PromedioRegistros2016 = (decimal.Parse(model.TotalRegistros2016.ToString()) / 12).ToString("0.00");
                model.PromedioAnuales2016 = (decimal.Parse(model.TotalAnuales2016.ToString()) / 12).ToString("0.00");

                results = dbContext.Database.SqlQuery<RptAltasMensuales>("exec Admin_RptAltasMensuales 2017", new object[] { }).ToList();
                model.TotalAltas2017 = results.Sum(x => x.Cantidad);
                model.TotalBajas2017 = results.Sum(x => x.Bajas);
                model.TotalRegistros2017 = results.Sum(x => x.Registros);
                model.TotalAnuales2017 = dbContext.PlanesPagos.Where(x => x.FechaDePago.Year == 2017 && x.ImportePagado > 0 && x.PagoAnual && x.IDPlan != 6).Count(); ;
                model.PromedioAltas2017 = (decimal.Parse(model.TotalAltas2017.ToString()) / 12).ToString("0.00");
                model.PromedioBajas2017 = (decimal.Parse(model.TotalBajas2017.ToString()) / 12).ToString("0.00");
                model.PromedioRegistros2017 = (decimal.Parse(model.TotalRegistros2017.ToString()) / 12).ToString("0.00");
                model.PromedioAnuales2017 = (decimal.Parse(model.TotalAnuales2017.ToString()) / 12).ToString("0.00");

                results = dbContext.Database.SqlQuery<RptAltasMensuales>("exec Admin_RptAltasMensuales 2018", new object[] { }).ToList();
                model.TotalAltas2018 = results.Sum(x => x.Cantidad);
                model.TotalBajas2018 = results.Sum(x => x.Bajas);
                model.TotalRegistros2018 = results.Sum(x => x.Registros);
                model.TotalAnuales2018 = dbContext.PlanesPagos.Where(x => x.FechaDePago.Year == 2018 && x.ImportePagado > 0 && x.PagoAnual && x.IDPlan != 6).Count(); ;
                model.PromedioAltas2018 = (decimal.Parse(model.TotalAltas2018.ToString()) / 12).ToString("0.00");
                model.PromedioBajas2018 = (decimal.Parse(model.TotalBajas2018.ToString()) / 12).ToString("0.00");
                model.PromedioRegistros2018 = (decimal.Parse(model.TotalRegistros2018.ToString()) / 12).ToString("0.00");
                model.PromedioAnuales2018 = (decimal.Parse(model.TotalAnuales2018.ToString()) / 12).ToString("0.00");

                var comprobantesresults = dbContext.Comprobantes.Where(x => x.FechaAlta.Year == 2016 && x.Tipo == "COT").Count();
                model.TotalComprobantesCOT2016 = comprobantesresults;
                model.PromedioComprobantesCOT2016 = (decimal.Parse(model.TotalComprobantesCOT2016.ToString()) / 12).ToString("0.00");
                comprobantesresults = dbContext.Comprobantes.Where(x => x.FechaAlta.Year == 2016 && x.Tipo != "COT").Count();
                model.TotalComprobantesOTROS2016 = comprobantesresults;
                model.PromedioComprobantesOTROS2016 = (decimal.Parse(model.TotalComprobantesOTROS2016.ToString()) / 12).ToString("0.00");


                comprobantesresults = dbContext.Comprobantes.Where(x => x.FechaAlta.Year == 2017 && x.Tipo == "COT").Count();
                model.TotalComprobantesCOT2017 = comprobantesresults;
                model.PromedioComprobantesCOT2017 = (decimal.Parse(model.TotalComprobantesCOT2017.ToString()) / 12).ToString("0.00");
                comprobantesresults = dbContext.Comprobantes.Where(x => x.FechaAlta.Year == 2017 && x.Tipo != "COT").Count();
                model.TotalComprobantesOTROS2017 = comprobantesresults;
                model.PromedioComprobantesOTROS2017 = (decimal.Parse(model.TotalComprobantesOTROS2017.ToString()) / 12).ToString("0.00");


                comprobantesresults = dbContext.Comprobantes.Where(x => x.FechaAlta.Year == 2018 && x.Tipo == "COT").Count();
                model.TotalComprobantesCOT2018 = comprobantesresults;
                model.PromedioComprobantesCOT2018 = (decimal.Parse(model.TotalComprobantesCOT2018.ToString()) / 12).ToString("0.00");
                comprobantesresults = dbContext.Comprobantes.Where(x => x.FechaAlta.Year == 2018 && x.Tipo != "COT").Count();
                model.TotalComprobantesOTROS2018 = comprobantesresults;
                model.PromedioComprobantesOTROS2018 = (decimal.Parse(model.TotalComprobantesOTROS2018.ToString()) / 12).ToString("0.00");

                model.TotalImpagos = results.Sum(x => x.Impagos);
            }
            return View(model);

            //return View();
        }

        public ActionResult DashBoardPagos()
        {
            var model = new DashboardAdminViewModel();

            using (var dbContext = new ACHEEntities())
            {

                var results = dbContext.Database.SqlQuery<RptAltasMensuales>("exec Admin_RptAltasMensuales 2016", new object[] { }).ToList();
                model.TotalAltas2016 = results.Sum(x => x.Cantidad);
                model.TotalBajas2016 = results.Sum(x => x.Bajas);
                model.TotalRegistros2016 = results.Sum(x => x.Registros);
                model.TotalAnuales2016 = dbContext.PlanesPagos.Where(x => x.FechaDePago.Year == 2016 && x.ImportePagado > 0 && x.PagoAnual && x.IDPlan != 6).Count(); ;
                model.PromedioAltas2016 = (decimal.Parse(model.TotalAltas2016.ToString()) / 12).ToString("0.00");
                model.PromedioBajas2016 = (decimal.Parse(model.TotalBajas2016.ToString()) / 12).ToString("0.00");
                model.PromedioRegistros2016 = (decimal.Parse(model.TotalRegistros2016.ToString()) / 12).ToString("0.00");
                model.PromedioAnuales2016 = (decimal.Parse(model.TotalAnuales2016.ToString()) / 12).ToString("0.00");

                results = dbContext.Database.SqlQuery<RptAltasMensuales>("exec Admin_RptAltasMensuales 2017", new object[] { }).ToList();
                model.TotalAltas2017 = results.Sum(x => x.Cantidad);
                model.TotalBajas2017 = results.Sum(x => x.Bajas);
                model.TotalRegistros2017 = results.Sum(x => x.Registros);
                model.TotalAnuales2017 = dbContext.PlanesPagos.Where(x => x.FechaDePago.Year == 2017 && x.ImportePagado > 0 && x.PagoAnual && x.IDPlan != 6).Count(); ;
                model.PromedioAltas2017 = (decimal.Parse(model.TotalAltas2017.ToString()) / 12).ToString("0.00");
                model.PromedioBajas2017 = (decimal.Parse(model.TotalBajas2017.ToString()) / 12).ToString("0.00");
                model.PromedioRegistros2017 = (decimal.Parse(model.TotalRegistros2017.ToString()) / 12).ToString("0.00");
                model.PromedioAnuales2017 = (decimal.Parse(model.TotalAnuales2017.ToString()) / 12).ToString("0.00");

                results = dbContext.Database.SqlQuery<RptAltasMensuales>("exec Admin_RptAltasMensuales 2018", new object[] { }).ToList();
                model.TotalAltas2018 = results.Sum(x => x.Cantidad);
                model.TotalBajas2018 = results.Sum(x => x.Bajas);
                model.TotalRegistros2018 = results.Sum(x => x.Registros);
                model.TotalAnuales2018 = dbContext.PlanesPagos.Where(x => x.FechaDePago.Year == 2018 && x.ImportePagado > 0 && x.PagoAnual && x.IDPlan != 6).Count(); ;
                model.PromedioAltas2018 = (decimal.Parse(model.TotalAltas2018.ToString()) / 12).ToString("0.00");
                model.PromedioBajas2018 = (decimal.Parse(model.TotalBajas2018.ToString()) / 12).ToString("0.00");
                model.PromedioRegistros2018 = (decimal.Parse(model.TotalRegistros2018.ToString()) / 12).ToString("0.00");
                model.PromedioAnuales2018 = (decimal.Parse(model.TotalAnuales2018.ToString()) / 12).ToString("0.00");

                //public int TotalComprobantesCOT2018 { get; set; }
                //public int TotalComprobantesCOT2017 { get; set; }
                //public int TotalComprobantesCOT2016 { get; set; }
                //public string PromedioComprobantesCOT2018 { get; set; }
                //public string PromedioComprobantesCOT2017 { get; set; }
                //public string PromedioComprobantesCOT2016 { get; set; }


                //public int TotalComprobantesOTROS2018 { get; set; }
                //public int TotalComprobantesOTROS2017 { get; set; }
                //public int TotalComprobantesOTROS2016 { get; set; }
                //public string PromedioComprobantesOTROS2018 { get; set; }
                //public string PromedioComprobantesOTROS2017 { get; set; }
                //public string PromedioComprobantesOTROS2016 { get; set; }






                model.TotalImpagos = results.Sum(x => x.Impagos);



            }
            return View(model);

        }

        /*[HttpPost]
        public ActionResult ObtenerPlanes(string tiempo)
        {
            List<ChartXYZ> listaChart = new List<ChartXYZ>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var fecha = DateTime.Now.AddMonths(-6);
                    var listaPlanes = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.FechaDeAlta >= fecha && x.Usuarios.Activo).ToList();

                    var listaBasico = listaPlanes.Where(x => x.IDPlan == 1).GroupBy(x => new { x.FechaDeAlta.Month, x.IDPlan }).Select(x => new ChartXYZ()
                    {
                        Basico = x.Count(),
                        Fecha = Convert.ToInt32(x.Select(y => y.FechaDeAlta.ToString("MM")).FirstOrDefault())
                    }).ToList();

                    var listaProfesional = listaPlanes.Where(x => x.IDPlan == 2).GroupBy(x => new { x.FechaDeAlta.Month, x.IDPlan }).Select(x => new ChartXYZ()
                    {
                        Profesional = x.Count(),
                        Fecha = Convert.ToInt32(x.Select(y => y.FechaDeAlta.ToString("MM")).FirstOrDefault())
                    }).ToList();

                    var listaPyme = listaPlanes.Where(x => x.IDPlan == 3).GroupBy(x => new { x.FechaDeAlta.Month, x.IDPlan }).Select(x => new ChartXYZ()
                    {
                        Pyme = x.Count(),
                        Fecha = Convert.ToInt32(x.Select(y => y.FechaDeAlta.ToString("MM")).FirstOrDefault())
                    }).ToList();

                    var listaEmpresa = listaPlanes.Where(x => x.IDPlan == 4).GroupBy(x => new { x.FechaDeAlta.Month, x.IDPlan }).Select(x => new ChartXYZ()
                    {
                        Empresa = x.Count(),
                        Fecha = Convert.ToInt32(x.Select(y => y.FechaDeAlta.ToString("MM")).FirstOrDefault())
                    }).ToList();

                    var listaCorp = listaPlanes.Where(x => x.IDPlan == 5).GroupBy(x => new { x.FechaDeAlta.Month, x.IDPlan }).Select(x => new ChartXYZ()
                    {
                        Corporativo = x.Count(),
                        Fecha = Convert.ToInt32(x.Select(y => y.FechaDeAlta.ToString("MM")).FirstOrDefault())
                    }).ToList();

                    var listaPrueba = listaPlanes.Where(x => x.IDPlan == 6).GroupBy(x => new { x.FechaDeAlta.Month, x.IDPlan }).Select(x => new ChartXYZ()
                    {
                        Prueba = x.Count(),
                        Fecha = Convert.ToInt32(x.Select(y => y.FechaDeAlta.ToString("MM")).FirstOrDefault())
                    }).ToList();

                    var listaCortesia = listaPlanes.Where(x => x.IDPlan == 99).GroupBy(x => new { x.FechaDeAlta.Month, x.IDPlan }).Select(x => new ChartXYZ()
                    {
                        Free = x.Count(),
                        Fecha = Convert.ToInt32(x.Select(y => y.FechaDeAlta.ToString("MM")).FirstOrDefault())
                    }).ToList();

                    listaChart = listaChart.Union(listaBasico).Union(listaProfesional).Union(listaPyme)
                        .Union(listaEmpresa).Union(listaCorp).Union(listaCortesia).Union(listaPrueba).ToList();

                    var aux = new List<ChartXYZ>();
                    aux.Add(new ChartXYZ() { Fecha = 1 });
                    aux.Add(new ChartXYZ() { Fecha = 2 });
                    aux.Add(new ChartXYZ() { Fecha = 3 });
                    aux.Add(new ChartXYZ() { Fecha = 4 });
                    aux.Add(new ChartXYZ() { Fecha = 5 });
                    aux.Add(new ChartXYZ() { Fecha = 6 });
                    aux.Add(new ChartXYZ() { Fecha = 7 });
                    aux.Add(new ChartXYZ() { Fecha = 8 });
                    aux.Add(new ChartXYZ() { Fecha = 9 });
                    aux.Add(new ChartXYZ() { Fecha = 10 });
                    aux.Add(new ChartXYZ() { Fecha = 11 });
                    aux.Add(new ChartXYZ() { Fecha = 12 });

                    aux = aux.Union(listaChart).ToList();
                    aux = aux.GroupBy(x => x.Fecha).Select(x => new ChartXYZ()
                    {
                        Fecha = Convert.ToInt32(x.FirstOrDefault().Fecha),
                        Basico = x.Sum(y => y.Basico),
                        Profesional = x.Sum(y => y.Profesional),
                        Pyme = x.Sum(y => y.Pyme),
                        Empresa = x.Sum(y => y.Empresa),
                        Prueba = x.Sum(y => y.Prueba),
                        Corporativo = x.Sum(y => y.Corporativo),
                        Free = x.Sum(y => y.Free),
                    }).OrderByDescending(x => x.Fecha).ToList();

                    int cont = 11;
                    foreach (var item in aux)
                    {
                        item.Fecha = cont;
                        cont--;
                    }

                    listaChart = aux;
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }
        */

        [HttpPost]
        public ActionResult Alertas()
        {
            List<Chart> listaChart = new List<Chart>();
            Chart result;
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fechaHoy = DateTime.Now;
                    var fechaDesde = DateTime.Now.AddMonths(-1);
                    var fechaHasta = DateTime.Now.AddMonths(3);

                    //pagos impagos
                    /*var impagos = 0;// dbContext.PlanesPagos.Count(x => x.PagoAnual && x.FechaFinPlan > fechaDesde && x.FechaFinPlan < fechaHasta);
                    result = new Chart()
                    {
                        //label = fechaDesde.ToString("MMM"),
                        data = impagos
                    };
                    listaChart.Add(result);*/

                    //pagos anuales prox a vencer
                    var planes = dbContext.PlanesPagos.Count(x => x.PagoAnual && x.FechaFinPlan > fechaDesde && x.FechaFinPlan < fechaHasta);
                    result = new Chart()
                    {
                        //label = fechaDesde.ToString("MMM"),
                        data = planes
                    };
                    listaChart.Add(result);

                    //pagos pendientes
                    var pendientes = dbContext.PlanesPagos.Count(x => x.Estado == "Pendiente");
                    result = new Chart()
                    {
                        //label = fechaDesde.ToString("MMM"),
                        data = pendientes
                    };
                    listaChart.Add(result);

                    var anualesinactivos = dbContext.PlanesPagos.Count(x => x.PagoAnual && x.Usuarios.FechaUltLogin < fechaDesde && x.FechaFinPlan > fechaHoy && x.FechaInicioPlan < fechaHoy);
                    result = new Chart() {
                        //label = fechaDesde.ToString("MMM"),
                        data = anualesinactivos
                    };
                    listaChart.Add(result);
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UsuariosRegistrados()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddMonths(-13);
                    listaChart = dbContext.Usuarios.Where(x => x.FechaAlta >= fecha && !x.IDUsuarioPadre.HasValue)
                        .GroupBy(x => new { x.FechaAlta.Year, x.FechaAlta.Month }).ToList()
                        .Select(x => new Chart()
                        {
                            label = x.FirstOrDefault().FechaAlta.ToString("MMM"),
                            data = x.Count()
                        }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UsuariosIntegrados()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddMonths(-1);
                    listaChart = dbContext.Integraciones.Include("Usuarios").Include("Usuarios.PlanesPago").Where(x => !x.FechaEliminacion.HasValue

                        && x.Usuarios.PlanesPagos.Any(y => y.FechaFinPlan > fecha))
                        .GroupBy(x => new { x.Tipo }).ToList().OrderBy(x => x.First().Tipo)
                        .Select(x => new Chart()
                        {
                            label = x.FirstOrDefault().Tipo,
                            data = x.Count()
                        }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TiposDeUsuarios()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddMonths(-1);
                    listaChart = dbContext.Usuarios.Include("PlanesPago").Where(x => !x.FechaBaja.HasValue
                        && !x.IDUsuarioPadre.HasValue
                        && x.PlanesPagos.Any(y => y.FechaFinPlan > fecha && y.IDPlan != 6))
                        .GroupBy(x => new { x.Actividad }).ToList().OrderBy(x => x.First().Actividad)
                        .Select(x => new Chart()
                        {
                            label = x.FirstOrDefault().Actividad ?? "Sin info",
                            data = x.Count()
                        }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UsuariosPlanesMensuales()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now;
                    listaChart = dbContext.PlanesPagos.Include("Usuarios").Where(x => x.FechaFinPlan > fecha && x.IDPlan != 6 && !x.PagoAnual &&
                        !x.Usuarios.FechaBaja.HasValue && !x.Usuarios.IDUsuarioPadre.HasValue && x.Estado == "Aceptado")
                    .GroupBy(x => new { x.IDPlan }).ToList().OrderBy(x => x.First().IDPlan)
                    .Select(x => new Chart()
                    {
                        label = x.FirstOrDefault().Planes.Nombre,
                        data = x.Count() != 0 ? x.Count() : 0
                    }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UsuariosPlanesAnuales()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now;
                    listaChart = dbContext.PlanesPagos.Include("Usuarios").Where(x => x.FechaFinPlan > fecha && x.IDPlan != 6 && x.PagoAnual &&
                        !x.Usuarios.FechaBaja.HasValue && !x.Usuarios.IDUsuarioPadre.HasValue && x.Estado=="Aceptado")
                    .GroupBy(x => new { x.IDPlan }).ToList().OrderBy(x => x.First().IDPlan)
                    .Select(x => new Chart()
                    {
                        label = x.FirstOrDefault().Planes.Nombre,
                        data = x.Count() != 0 ? x.Count() : 0
                    }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerDetalleChart(string label, string anual, int page, int pageSize) {
            ResultadosRptUsuariosPlanes resultado = new ResultadosRptUsuariosPlanes();
            try {
                using (var dbContext = new ACHEEntities()) {
                    List<RptUsuariosPlanes> results = new List<RptUsuariosPlanes>();
                    var fecha = DateTime.Now;
                    var filtrarAnual = false;

                    if (int.Parse(anual) == 1)
                        filtrarAnual = true;

                    results = dbContext.PlanesPagos.Include("Usuarios").Where(x => x.FechaFinPlan > fecha && x.IDPlan != 6 && x.PagoAnual == filtrarAnual &&
                        !x.Usuarios.FechaBaja.HasValue && !x.Usuarios.IDUsuarioPadre.HasValue && x.Planes.Nombre == label && x.Estado == "Aceptado").ToList().Select(x => new RptUsuariosPlanes() {
                            IDUsuario = x.IDUsuario,
                            RazonSocial = x.Usuarios.RazonSocial,
                            CUIT = x.Usuarios.CUIT,
                            Email = x.Usuarios.Email,
                            FechaAlta = x.Usuarios.FechaAlta,
                            FormaDePago = x.FormaDePago,
                            FechaFinPlan = x.FechaFinPlan.Value

                        }).ToList();

                    page--;

                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.Skip(page * pageSize).Take(pageSize).ToList();


                    resultado.Items = list.ToList();


                    foreach (var item in resultado.Items) {

                        var planespagados = dbContext.PlanesPagos.Where(x => x.IDUsuario == item.IDUsuario && x.ImportePagado > 0 && x.Estado == "Aceptado").ToList();
                        var total = 0;
                        foreach (var planes in planespagados) {
                            var pago = (planes.FechaInicioPlan.Value.Month - planes.FechaFinPlan.Value.Month) + 12 * (planes.FechaInicioPlan.Value.Year - planes.FechaFinPlan.Value.Year);
                            total -= pago;


                        }
                        item.MesesPagados = total;

                    }

                }
            }
            catch (Exception ex) {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UsuariosVentaOnline()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddMonths(-1);
                    listaChart = dbContext.Usuarios.Include("PlanesPago").Where(x => !x.FechaBaja.HasValue
                        && !x.IDUsuarioPadre.HasValue
                        && x.Integraciones.Any()
                        && x.PlanesPagos.Any(y => y.FechaFinPlan > fecha && y.IDPlan != 6))
                        .GroupBy(x => new { VentaOnline = (x.Integraciones.Any() ? "Si" : "No") }).ToList()
                        .Select(x => new Chart()
                        {
                            label = x.FirstOrDefault().Integraciones.Any() ? "Si" : "No",
                            data = x.Count()
                        }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ComprobantesPorIntegracion()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    //var fecha = DateTime.Now.AddMonths(-1);
                    string sql = "select integraciones.Tipo as label, cast(count(Comprobantes.IDIntegracion) as int) as data from integraciones inner join Comprobantes on Integraciones.IDIntegracion= Comprobantes.IDIntegracion group by integraciones.Tipo order by integraciones.Tipo";

                    listaChart = dbContext.Database.SqlQuery<Chart>(sql, new object[] { }).ToList();

                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BajasRegistradas()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddMonths(-12);
                    listaChart = dbContext.Usuarios.Where(x => x.FechaBaja.HasValue && x.FechaBaja >= fecha && x.PlanesPagos.Any(y => y.IDPlan != 6 && y.IDPlan != 99))
                        .GroupBy(x => new { x.FechaBaja.Value.Year, x.FechaBaja.Value.Month }).ToList()
                        .Select(x => new Chart()
                        {
                            label = x.FirstOrDefault().FechaBaja.Value.ToString("MMM"),
                            data = x.Count()
                        }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PagosAnuales()
        {
            List<Chart> listaChart = new List<Chart>();
            List<Chart> data;
            Chart result;
            try
            {
                DateTime fechaDesde;
                DateTime fechaHasta;

                using (var dbContext = new ACHEEntities())
                {

                    int index = -12;
                    while (index <= 0)
                    {
                        fechaDesde = DateTime.Now.AddMonths(index).GetFirstDayOfMonth();
                        fechaHasta = DateTime.Now.AddMonths(index).GetLastDayOfMonth();
                        data = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado"
                            && x.FechaDePago >= fechaDesde && x.FechaDePago <= fechaHasta
                            && x.PagoAnual && x.IDPlan != 6 && x.IDPlan != 99)
                            .GroupBy(x => new { x.FechaDePago.Year, x.FechaDePago.Month }).ToList()
                            .Select(x => new Chart()
                            {
                                label = x.FirstOrDefault().FechaDePago.ToString("MMM"),
                                data = x.Count()
                            }).ToList();

                        result = new Chart()
                        {
                            label = fechaDesde.ToString("MMM"),
                            data = data.Any() ? data.First().data : 0
                        };
                        listaChart.Add(result);

                        index++;
                    }
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AltasRegistradas()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddMonths(-12);
                    listaChart = dbContext.RptFechaPrimerPagoView.Where(x => x.Fecha >= fecha)
                        .GroupBy(x => new { x.Fecha.Value.Year, x.Fecha.Value.Month }).ToList()
                        .Select(x => new Chart()
                        {
                            label = x.FirstOrDefault().Fecha.Value.ToString("MMM"),
                            data = x.Count()
                        }).ToList();
                }
            }
            catch (Exception es)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UsuariosPorPlan()
        {
            List<CharPlanes> listaChart = new List<CharPlanes>();
            List<Chart> data;
            CharPlanes result;
            try
            {
                string fechaDesde = "";
                string fechaHasta = "";
                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];

                using (var dbContext = new ACHEEntities())
                {
                    //basico
                    fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptUsuariosPorPlan '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptUsuariosPorPlan '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptUsuariosPorPlan '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptUsuariosPorPlan '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptUsuariosPorPlan '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptUsuariosPorPlan '" + fechaDesde + "','" + fechaHasta + "'", new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);
                }
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FormasDePagoUtilizadas()
        {

            List<CharFormasDePago> listaChart = new List<CharFormasDePago>();
            List<Chart> data;
            CharFormasDePago result;
            try
            {
                DateTime fechaDesde;
                DateTime fechaHasta;

                using (var dbContext = new ACHEEntities())
                {
                    //basico
                    fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth();
                    data = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.IDPlan != 6 && x.IDPlan != 99 //excluyo los free y prueba
                        && x.FechaDePago >= fechaDesde && x.FechaDePago <= fechaHasta)
                        .GroupBy(x => new { x.FormaDePago, x.FechaDePago.Year, x.FechaDePago.Month }).ToList()
                        .Select(x => new Chart()
                        {
                            label = x.FirstOrDefault().FormaDePago,
                            data = x.Count()
                        }).ToList();

                    result = new CharFormasDePago()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        MercadoPago = data.Any(y => y.label == "Mercado Pago") ? data.Where(y => y.label == "Mercado Pago").First().data : 0,
                        Transferencia = data.Any(y => y.label == "Transferencia") ? data.Where(y => y.label == "Transferencia").First().data : 0,
                        Deposito = data.Any(y => y.label == "Depósito") ? data.Where(y => y.label == "Depósito").First().data : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth();
                    data = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.IDPlan != 6 && x.IDPlan != 99 //excluyo los free y prueba
                       && x.FechaDePago >= fechaDesde && x.FechaDePago <= fechaHasta)
                       .GroupBy(x => new { x.FormaDePago, x.FechaDePago.Year, x.FechaDePago.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().FormaDePago,
                           data = x.Count()
                       }).ToList();

                    result = new CharFormasDePago()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        MercadoPago = data.Any(y => y.label == "Mercado Pago") ? data.Where(y => y.label == "Mercado Pago").First().data : 0,
                        Transferencia = data.Any(y => y.label == "Transferencia") ? data.Where(y => y.label == "Transferencia").First().data : 0,
                        Deposito = data.Any(y => y.label == "Depósito") ? data.Where(y => y.label == "Depósito").First().data : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth();
                    data = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.IDPlan != 6 && x.IDPlan != 99 //excluyo los free y prueba
                       && x.FechaDePago >= fechaDesde && x.FechaDePago <= fechaHasta)
                       .GroupBy(x => new { x.FormaDePago, x.FechaDePago.Year, x.FechaDePago.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().FormaDePago,
                           data = x.Count()
                       }).ToList();

                    result = new CharFormasDePago()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        MercadoPago = data.Any(y => y.label == "Mercado Pago") ? data.Where(y => y.label == "Mercado Pago").First().data : 0,
                        Transferencia = data.Any(y => y.label == "Transferencia") ? data.Where(y => y.label == "Transferencia").First().data : 0,
                        Deposito = data.Any(y => y.label == "Depósito") ? data.Where(y => y.label == "Depósito").First().data : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth();
                    data = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.IDPlan != 6 && x.IDPlan != 99 //excluyo los free y prueba
                       && x.FechaDePago >= fechaDesde && x.FechaDePago <= fechaHasta)
                       .GroupBy(x => new { x.FormaDePago, x.FechaDePago.Year, x.FechaDePago.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().FormaDePago,
                           data = x.Count()
                       }).ToList();

                    result = new CharFormasDePago()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        MercadoPago = data.Any(y => y.label == "Mercado Pago") ? data.Where(y => y.label == "Mercado Pago").First().data : 0,
                        Transferencia = data.Any(y => y.label == "Transferencia") ? data.Where(y => y.label == "Transferencia").First().data : 0,
                        Deposito = data.Any(y => y.label == "Depósito") ? data.Where(y => y.label == "Depósito").First().data : 0,
                        ////Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth();
                    data = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.IDPlan != 6 && x.IDPlan != 99 //excluyo los free y prueba
                       && x.FechaDePago >= fechaDesde && x.FechaDePago <= fechaHasta)
                       .GroupBy(x => new { x.FormaDePago, x.FechaDePago.Year, x.FechaDePago.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().FormaDePago,
                           data = x.Count()
                       }).ToList();

                    result = new CharFormasDePago()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        MercadoPago = data.Any(y => y.label == "Mercado Pago") ? data.Where(y => y.label == "Mercado Pago").First().data : 0,
                        Transferencia = data.Any(y => y.label == "Transferencia") ? data.Where(y => y.label == "Transferencia").First().data : 0,
                        Deposito = data.Any(y => y.label == "Depósito") ? data.Where(y => y.label == "Depósito").First().data : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.GetLastDayOfMonth();
                    data = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.IDPlan != 6 && x.IDPlan != 99 //excluyo los free y prueba
                       && x.FechaDePago >= fechaDesde && x.FechaDePago <= fechaHasta)
                       .GroupBy(x => new { x.FormaDePago, x.FechaDePago.Year, x.FechaDePago.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().FormaDePago,
                           data = x.Count()
                       }).ToList();

                    result = new CharFormasDePago()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        MercadoPago = data.Any(y => y.label == "Mercado Pago") ? data.Where(y => y.label == "Mercado Pago").First().data : 0,
                        Transferencia = data.Any(y => y.label == "Transferencia") ? data.Where(y => y.label == "Transferencia").First().data : 0,
                        Deposito = data.Any(y => y.label == "Depósito") ? data.Where(y => y.label == "Depósito").First().data : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);
                }
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ComprobantesPorTipo()
        {
            List<CharComprobantesTipo> listaChart = new List<CharComprobantesTipo>();

        /*
            List<CharComprobantesTipo> listaChart = new List<CharComprobantesTipo>();
            /*
            List<Chart> data;
            CharComprobantesTipo result;
            try
            {
                DateTime fechaDesde;
                DateTime fechaHasta;

                using (var dbContext = new ACHEEntities())
                {
                    //basico
                    //string sql = "select count comprobantes.Tipo as label, from comprobantes group by comprobantes.Tipo order by comprobantes.Tipo";

                    //listaChart = dbContext.Database.SqlQuery<CharComprobantesTipo>(sql, new object[] { }).ToList();

                    fechaDesde = DateTime.Now.AddMonths(-11).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-6).GetLastDayOfMonth();
                    var listacomprobantes = dbContext.Comprobantes.Where(x => x.FechaAlta >= fechaDesde && x.FechaAlta <= fechaHasta).Select(x => new { label = x.Tipo, fecha = x.FechaAlta }).ToList();

                    fechaDesde = DateTime.Now.AddMonths(-11).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-11).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);


                    fechaDesde = DateTime.Now.AddMonths(-10).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-10).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);



                    fechaDesde = DateTime.Now.AddMonths(-9).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-9).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);



                    fechaDesde = DateTime.Now.AddMonths(-8).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-8).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);



                    fechaDesde = DateTime.Now.AddMonths(-7).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-7).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);



                    fechaDesde = DateTime.Now.AddMonths(-6).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-6).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth();
                    listacomprobantes = dbContext.Comprobantes.Where(x => x.FechaAlta >= fechaDesde).Select(x => new { label = x.Tipo, fecha = x.FechaAlta }).ToList();



                    fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);


                    fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);


                    fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);



                    fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);


                    fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);




                    fechaDesde = DateTime.Now.GetFirstDayOfMonth();
                    fechaHasta = DateTime.Now.GetLastDayOfMonth();
                    data = listacomprobantes.Where(x =>
                       x.fecha >= fechaDesde && x.fecha <= fechaHasta)
                       .GroupBy(x => new { x.fecha, x.fecha.Year, x.fecha.Month }).ToList()
                       .Select(x => new Chart()
                       {
                           label = x.FirstOrDefault().label,
                           data = x.Count()
                       }).ToList();

                    result = new CharComprobantesTipo()
                    {
                        Fecha = fechaDesde.ToString("MMM"),
                        COT = data.Any(y => y.label == "COT") ? data.Count(y => y.label == "COT") : 0,
                        OTROS = data.Any(y => y.label != "COT") ? data.Count(y => y.label != "COT") : 0,
                        //Otros = data.Where(y => y.label == "-").First().data

                    };

                    listaChart.Add(result);
                }
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }*/
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AltasPagosAnuales()
        {
            List<CharPlanes> listaChart = new List<CharPlanes>();
            List<Chart> data;
            CharPlanes result;
            try
            {
                string fechaDesde = "";
                string fechaHasta = "";
                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];

                using (var dbContext = new ACHEEntities())
                {
                    //basico
                    fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 1, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 1, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 1, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 1, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 1, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 1, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);
                }
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AltasPagosMensuales()
        {
            List<CharPlanes> listaChart = new List<CharPlanes>();
            List<Chart> data;
            CharPlanes result;
            try
            {
                string fechaDesde = "";
                string fechaHasta = "";
                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];

                using (var dbContext = new ACHEEntities())
                {
                    //basico
                    fechaDesde = DateTime.Now.AddMonths(-5).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-5).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 0, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-4).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-4).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 0, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-3).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-3).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 0, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-2).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-2).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 0, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.AddMonths(-1).GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 0, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);

                    fechaDesde = DateTime.Now.GetFirstDayOfMonth().ToString(formato);
                    fechaHasta = DateTime.Now.GetLastDayOfMonth().ToString(formato) + " 11:59:59 pm";
                    data = dbContext.Database.SqlQuery<Chart>("exec Admin_RptPagosAnualesPorPlan '" + fechaDesde + "','" + fechaHasta + "'," + 0, new object[] { }).ToList();
                    result = new CharPlanes()
                    {
                        Fecha = fechaDesde,
                        Basico = data.Where(y => y.label == "1").First().data,
                        Profesional = data.Where(y => y.label == "2").First().data,
                        Pyme = data.Where(y => y.label == "3").First().data,
                        Empresa = data.Where(y => y.label == "4").First().data,
                        Corporativo = data.Where(y => y.label == "5").First().data,
                        Free = data.Where(y => y.label == "99").First().data,
                    };
                    listaChart.Add(result);
                }
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult ObtenerDetalleAnualesInactivos() {
            /*List<AnualesInactivosDetallesViewModel> results = new List<AnualesInactivosDetallesViewModel>();
            try {
                using (var dbContext = new ACHEEntities()) {

                    var fechaHoy = DateTime.Now;
                    var fechaDesde = DateTime.Now.AddMonths(-1);
                    results = dbContext.PlanesPagos.Include("Usuarios").Include("Planes").Where(x => x.PagoAnual && x.Usuarios.FechaUltLogin < fechaDesde && x.FechaFinPlan > fechaHoy && x.FechaInicioPlan < fechaHoy).Select(x => new AnualesInactivosDetallesViewModel {
                    IDUsuario = x.Usuarios.IDUsuario,
                    RazonSocial = x.Usuarios.RazonSocial,
                    CUIT= x.Usuarios.CUIT,
                    Email= x.Usuarios.Email,
                    FechaDeAlta = x.Usuarios.FechaAlta,
                    FechaDePago = x.FechaDePago,
                    NombrePlan= x.Planes.Nombre,
                    ImportePagado= x.ImportePagado,
                    CodigoPromo= x.Usuarios.CodigoPromo,
                    FechaUltimoLogin = x.Usuarios.FechaUltLogin,
                    FechaVencimiento = x.FechaFinPlan.Value                                              
                    
                    
                    }).ToList();

                    
                }
            }
            catch (Exception ex) {
                return Json(false, JsonRequestBehavior.AllowGet);
            }*/

            return Json("", JsonRequestBehavior.AllowGet);
        }

        /*
        [HttpPost]
        public ActionResult ObtenerRegistradosPorDia()
        {
            List<Chart> listaChart = new List<Chart>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddDays(-30);
                    var listaAux = dbContext.Usuarios.Where(x => x.FechaAlta >= fecha && x.Activo).Select(x => new
                    {
                        FechaAlta = x.FechaAlta,
                        Cantidad = 1
                    }).ToList();

                    var listaUsuarios = listaAux.Select(x => new
                    {
                        Cantidad = x.Cantidad,
                        Fecha = x.FechaAlta.ToString("yyyy-MM-dd")
                    }).ToList();


                    listaChart = listaUsuarios.GroupBy(x => x.Fecha).Select(x => new Chart()
                    {
                        label = x.FirstOrDefault().Fecha,
                        data = x.Sum(y => y.Cantidad)
                    }).ToList();

                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerPlanesDias()
        {
            List<CharPlanes> listaChart = new List<CharPlanes>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddDays(-30);
                    var listaPlanes = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.FechaInicioPlan >= fecha && x.Usuarios.Activo).ToList();

                    var listaBasico = listaPlanes.Where(x => x.IDPlan == 1).GroupBy(x => new { x.FechaInicioPlan.Value.Day, x.IDPlan }).Select(x => new CharPlanes()
                    {
                        Basico = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();

                    var listaProfesional = listaPlanes.Where(x => x.IDPlan == 2).GroupBy(x => new { x.FechaInicioPlan.Value.Day, x.IDPlan }).Select(x => new CharPlanes()
                    {
                        Profesional = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();

                    var listaPyme = listaPlanes.Where(x => x.IDPlan == 3).GroupBy(x => new { x.FechaInicioPlan.Value.Day, x.IDPlan }).Select(x => new CharPlanes()
                    {
                        Pyme = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();

                    var listaEmpresa = listaPlanes.Where(x => x.IDPlan == 4).GroupBy(x => new { x.FechaInicioPlan.Value.Day, x.IDPlan }).Select(x => new CharPlanes()
                    {
                        Empresa = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();

                    var listaCorporativo = listaPlanes.Where(x => x.IDPlan == 5).GroupBy(x => new { x.FechaInicioPlan.Value.Day, x.IDPlan }).Select(x => new CharPlanes()
                    {
                        Corporativo = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();

                    var listaPrueba = listaPlanes.Where(x => x.IDPlan == 6).GroupBy(x => new { x.FechaInicioPlan.Value.Day, x.IDPlan }).Select(x => new CharPlanes()
                    {
                        Prueba = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();

                    var listaCortesia = listaPlanes.Where(x => x.IDPlan == 99).GroupBy(x => new { x.FechaInicioPlan.Value.Day, x.IDPlan }).Select(x => new CharPlanes()
                    {
                        Free = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();

                    listaChart = listaChart.Union(listaBasico).Union(listaProfesional).Union(listaPyme)
                        .Union(listaEmpresa).Union(listaPrueba)
                        .Union(listaCorporativo).Union(listaCortesia).ToList();

                    listaChart = listaChart.GroupBy(x => x.Fecha).Select(x => new CharPlanes()
                    {
                        Fecha = x.FirstOrDefault().Fecha,
                        Prueba = x.Sum(y => y.Prueba),
                        Basico = x.Sum(y => y.Basico),
                        Profesional = x.Sum(y => y.Profesional),
                        Pyme = x.Sum(y => y.Pyme),
                        Empresa = x.Sum(y => y.Empresa),
                        Corporativo = x.Sum(y => y.Corporativo),
                        Free = x.Sum(y => y.Free)
                    }).ToList();

                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerFormasDePago()
        {
            List<CharFormas> listaChart = new List<CharFormas>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var fecha = DateTime.Now.AddMonths(-6);
                    var listaPlanes = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.FechaInicioPlan.Value >= fecha).ToList();

                    var listaMP = listaPlanes.Where(x => x.FormaDePago == "Mercado Pago").GroupBy(x => new { x.FechaInicioPlan.Value.Month }).Select(x => new CharFormas()
                    {
                        MercadoPago = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM")).FirstOrDefault()
                    }).ToList();

                    var listaTrans = listaPlanes.Where(x => x.FormaDePago == "Transferencia").GroupBy(x => new { x.FechaInicioPlan.Value.Month }).Select(x => new CharFormas()
                    {
                        Transferencia = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM")).FirstOrDefault()
                    }).ToList();

                    var listaDep = listaPlanes.Where(x => x.FormaDePago == "Deposito").GroupBy(x => new { x.FechaInicioPlan.Value.Month }).Select(x => new CharFormas()
                    {
                        Deposito = x.Count(),
                        Fecha = x.Select(y => y.FechaInicioPlan.Value.ToString("yyyy-MM")).FirstOrDefault()
                    }).ToList();

                    listaChart = listaChart.Union(listaMP).Union(listaTrans).Union(listaDep).ToList();
                    listaChart = listaChart.GroupBy(x => x.Fecha).Select(x => new CharFormas()
                    {
                        Fecha = x.FirstOrDefault().Fecha,
                        MercadoPago = x.Sum(y => y.MercadoPago),
                        Transferencia = x.Sum(y => y.Transferencia),
                        Deposito = x.Sum(y => y.Deposito),
                    }).ToList();

                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerFacturacion()
        {
            List<CharFacturacion> listaChart = new List<CharFacturacion>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var fecha = DateTime.Now.AddMonths(-6);
                    var listaPlanes = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.FechaDePago >= fecha).ToList();

                    listaChart = listaPlanes.GroupBy(x => new { x.FechaDePago.Month }).Select(x => new CharFacturacion()
                    {
                        ImporteTotal = x.Sum(y => y.ImportePagado),
                        Fecha = x.Select(y => y.FechaDePago.ToString("yyyy-MM")).FirstOrDefault()
                    }).ToList();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerFacturacionDias()
        {
            List<CharFacturacion> listaChart = new List<CharFacturacion>();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var fecha = DateTime.Now.AddDays(-31);
                    var listaPlanes = dbContext.PlanesPagos.Where(x => x.Estado == "Aceptado" && x.FechaDePago >= fecha).ToList();

                    listaChart = listaPlanes.GroupBy(x => new { x.FechaDePago.Day }).Select(x => new CharFacturacion()
                    {
                        ImporteTotal = x.Sum(y => y.ImportePagado),
                        Fecha = x.Select(y => y.FechaDePago.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(listaChart, JsonRequestBehavior.AllowGet);
        }*/
    }
}
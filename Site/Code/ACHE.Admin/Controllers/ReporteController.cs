﻿using ACHE.Admin.Models;
using ACHE.Model;
using ACHE.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ACHE.Admin.Controllers
{
    public class ReporteController : Controller
    {
        //
        // GET: /Reporte/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RptUsuarios(int idPlan = 0, string estado = "")
        {
            if (idPlan > 0 && !string.IsNullOrWhiteSpace(estado))
            {
                var model = new PlanesViewModel();
                model.Estado = estado;
                switch (idPlan)
                {
                    case 1:
                        model.Plan = "Basico";
                        break;
                    case 2:
                        model.Plan = "Profesional";
                        break;
                    case 3:
                        model.Plan = "Pyme";
                        break;
                    case 4:
                        model.Plan = "Empresa";
                        break;
                    case 5:
                        model.Plan = "Corporativo";
                        break;
                    case 6:
                        model.Plan = "Prueba";
                        break;
                    case 99:
                        model.Plan = "Free";
                        break;
                }
                return View(model);
            }
            else
                return View();
        }

        [HttpPost]
        public ActionResult ObtenerRptUsuarios(string integraciones, string periodo,
        string fechaDesde, string fechaHasta, string metodoDePago, string formaDePago, string tipoPlan, string codigoPromo, string estadoUsuario, int page, int pageSize)
        {
            ResultadosUsuarioViewModel resultado = new ResultadosUsuarioViewModel();
            try
            {

                var fecha = DateTime.Now.Date;

                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.AdminRptUsuariosView.Where(x => x.TipoUsuario != "Adicional")
                        .OrderByDescending(x => x.FechaAlta).AsQueryable();

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaUltLogin >= dtDesde).AsQueryable();
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaUltLogin <= dtHasta).AsQueryable();
                    }

                    if (integraciones != string.Empty)
                        results = results.Where(x => x.TieneIntegracion.ToUpper() == integraciones.ToUpper()).AsQueryable();

                    if (metodoDePago != string.Empty)
                        results = results.Where(x => x.FormaDePago.ToUpper() == metodoDePago.ToUpper()).AsQueryable();

                    if (formaDePago != string.Empty)
                        results = results.Where(x => x.PagoAnual == (formaDePago == "0" ? false : true)).AsQueryable();


                    if (!string.IsNullOrWhiteSpace(tipoPlan))
                    {
                        results = results.Where(x => x.PlanActual.ToUpper() == tipoPlan.ToUpper()).AsQueryable();
                    }

                    if (!string.IsNullOrWhiteSpace(codigoPromo))
                    {
                        results = results.Where(x => x.CodigoPromo.ToUpper().Contains(codigoPromo.ToUpper())).AsQueryable();
                    }

                    switch (estadoUsuario)
                    {
                        case "Setup OK":
                        case "Total Activos":
                            results = results.Where(x => x.Activo == true && x.Estado == "Aceptado" && x.FechaFinPlan >= fecha).AsQueryable();
                            break;
                        case "Dados de baja":
                            results = results.Where(x => x.Activo == false).AsQueryable();
                            break;
                        case "Total Usuarios":
                        default:
                            results = results.AsQueryable();
                            break;
                    }


                    page--;

                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new UsuarioViewModel()
                        {
                            ID = x.IDUsuario,
                            IDPadre = x.IDUsuarioPadre ?? 0,
                            RazonSocial = x.RazonSocial,
                            AntiguedadMeses = x.AntiguedadMeses ?? 0,
                            CodigoPromo = x.CodigoPromo ?? "",
                            PlanActual = x.PlanActual,
                            Email = x.Email,
                            FechaDeBaja = x.FechaBaja.HasValue ? x.FechaBaja.Value.ToString("dd/MM/yyyy") : ""
                        }).ToList();
                    resultado.Items = list.ToList();
                }
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult exportRptUsuarios(string integraciones, string periodo,
        string fechaDesde, string fechaHasta, string metodoDePago, string formaDePago, string tipoPlan, string codigoPromo, string estadoUsuario)
        {
            string fileName = "Usuarios";
            string path = "~/tmp/";

            var fecha = DateTime.Now.Date;


            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.AdminRptUsuariosView.OrderByDescending(x => x.FechaAlta).AsQueryable();//Obtengo todos los usuarios padres e hijos.
                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    //Filtro los usuarios padres e hijos que cumplan las condiciones
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaUltLogin >= dtDesde).AsQueryable();
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaUltLogin <= dtHasta).AsQueryable();
                    }

                    if (integraciones != string.Empty)
                        results = results.Where(x => x.TieneIntegracion.ToUpper() == integraciones.ToUpper()).AsQueryable();

                    if (metodoDePago != string.Empty)
                        results = results.Where(x => x.FormaDePago.ToUpper() == metodoDePago.ToUpper()).AsQueryable();

                    if (formaDePago != string.Empty)
                        results = results.Where(x => x.PagoAnual == (formaDePago == "0" ? false : true)).AsQueryable();

                    if (!string.IsNullOrWhiteSpace(tipoPlan))
                    {
                        results = results.Where(x => x.PlanActual.ToUpper() == tipoPlan.ToUpper()).AsQueryable();
                    }

                    if (!string.IsNullOrWhiteSpace(codigoPromo))
                    {
                        results = results.Where(x => x.CodigoPromo.ToUpper().Contains(codigoPromo.ToUpper())).AsQueryable();
                    }

                    switch (estadoUsuario)
                    {
                        case "Setup OK":
                        case "Total Activos":
                            results = results.Where(x => x.Activo == true && x.Estado == "Aceptado" && x.FechaFinPlan >= fecha).AsQueryable();
                            break;
                        case "Dados de baja":
                            results = results.Where(x => x.Activo == false).AsQueryable();
                            break;
                        case "Total Usuarios":
                        default:
                            results = results.AsQueryable();
                            break;
                    }


                    //var asd = results.Where(x => x.TipoUsuario != "Adicional").ToList();

                    var aux = results.Where(x => x.TipoUsuario != "Adicional").ToList().Select(x => new RptUsuariosViewModel
                    {
                        ID = x.IDUsuario,
                        IDPadre = x.IDUsuarioPadre ?? 0,
                        RazonSocial = x.RazonSocial,
                        Email = x.Email,
                        AntiguedadMeses = x.AntiguedadMeses ?? 0,
                        CodigoPromo = x.CodigoPromo ?? "",
                        PlanActual = x.PlanActual,
                        FechaBaja = x.FechaBaja != null ? x.FechaBaja.Value.ToString("dd/MM/yyyy") : "",
                        MotivoBaja = x.MotivoBaja,
                        IntegracionesActivas = x.IntegracionesActivas,
                        TipoUsuario = x.TipoUsuario
                    }).OrderBy(x => x.ID).ThenBy(x => x.IDPadre).ToList();

                    var padresAdicionales = aux.Select(x => x.ID).ToList();

                    //Creo una lista auxiliar.
                    var aux2 = new List<RptUsuariosViewModel>();

                    aux2.AddRange(aux);
                    aux2.AddRange(results.Where(x => x.TipoUsuario == "Adicional" && padresAdicionales.Contains(x.IDUsuario)).ToList().Select(x => new RptUsuariosViewModel
                    {
                        ID = x.IDUsuarioAdicional,
                        IDPadre = x.IDUsuario,
                        Email = x.Email,
                        TipoUsuario = x.TipoUsuario
                    }));

                    //foreach (var item in aux) {
                    //    aux2.Add(item);//Agrego el usuario
                    //    var usuariosAdic = results.Where(x=>x.TipoUsuario == "Adicional" && x.IDUsuario == item.ID).Select(x => new RptUsuariosViewModel {
                    //        ID = x.IDUsuarioAdicional,
                    //        IDPadre = x.IDUsuario,
                    //        Email = x.Email,
                    //        TipoUsuario = x.TipoUsuario
                    //    }).ToList();
                    //    if (usuariosAdic.Count > 0)
                    //        aux2.AddRange(usuariosAdic);//Agrego los usuarios adicionales
                    //}


                    dt = aux2.Select(x => new
                    {
                        ID = x.ID,
                        IDPadre = x.IDPadre,
                        RazonSocial = x.RazonSocial,
                        Email = x.Email,
                        AntiguedadMeses = Convert.ToInt32(x.AntiguedadMeses),
                        CodigoPromo = x.CodigoPromo,
                        PlanActual = x.PlanActual,
                        FechaBaja = x.FechaBaja,
                        MotivoBaja = x.MotivoBaja,
                        IntegracionesActivas = x.IntegracionesActivas ?? "",
                        TipoUsuario = x.TipoUsuario
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    CommonModel.GenerarArchivo(dt, Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                var archivo = (path + fileName + "_" + DateTime.Now.ToString("yyyMMdd") + ".xlsx").Replace("~", "");
                return Json(archivo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
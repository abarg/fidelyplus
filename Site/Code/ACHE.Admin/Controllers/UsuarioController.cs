﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACHE.Model;
using ACHE.Extensions;
using ACHE.Admin.Models;
using System.Configuration;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;
using ACHE.Negocio.Contabilidad;
using ACHE.Model.ViewModels;

namespace ACHE.Admin.Controllers
{
    public class UsuarioController : BaseController
    {
        public ActionResult Index(int idPlan = 0, string estado = "")
        {
            if (idPlan > 0 && !string.IsNullOrWhiteSpace(estado))
            {
                var model = new PlanesViewModel();
                model.Estado = estado;
                switch (idPlan)
                {
                    case 1:
                        model.Plan = "Basico";
                        break;
                    case 2:
                        model.Plan = "Profesional";
                        break;
                    case 3:
                        model.Plan = "Pyme";
                        break;
                    case 4:
                        model.Plan = "Empresa";
                        break;
                    case 5:
                        model.Plan = "Corporativo";
                        break;
                    case 6:
                        model.Plan = "Prueba";
                        break;
                    case 99:
                        model.Plan = "Free";
                        break;
                }
                return View(model);
            }
            else
                return View();
        }

        public ActionResult Edicion(int id)
        {
            var model = new UsuarioViewModel();
            using (var dbContext = new ACHEEntities())
            {
                model = dbContext.Usuarios.Include("PlanesPagos").Where(x => x.IDUsuario == id).Select(x => new UsuarioViewModel()
                {
                    ID = x.IDUsuario,
                    RazonSocial = x.RazonSocial,
                    CondicionIva = x.CondicionIva,
                    CUIT = x.CUIT,
                    Telefono = x.Telefono,
                    Email = x.Email,
                    EsAgentePercepcionIIBB = (x.EsAgentePercepcionIIBB == null) ? false : (bool)x.EsAgentePercepcionIIBB,
                    EsAgentePercepcionIVA = (x.EsAgentePercepcionIVA == null) ? false : (bool)x.EsAgentePercepcionIVA,
                    EsAgenteRetencion = (x.EsAgenteRetencion == null) ? false : (bool)x.EsAgenteRetencion,
                    MercadoPagoClientSecret = x.MercadoPagoClientSecret,
                    MercadoPagoClientID = x.MercadoPagoClientID,
                    SetupRealizado = x.SetupRealizado ? "SI" : "NO",
                    ApiKey = x.ApiKey,
                    TemplateFc = x.TemplateFc,
                    Celular = x.Celular,
                    IIBB = x.IIBB,
                    Activo = x.Activo,
                    EmailAlertas = x.EmailAlertas,
                    TieneFacturaElectronica = x.TieneFacturaElectronica,
                    FechaAlta = x.FechaAlta,
                    FechaInicioActividades = (x.FechaInicioActividades == null) ? DateTime.Now : (DateTime)x.FechaInicioActividades,
                    CodigoPostal = x.CodigoPostal,
                    CodigoPromo = x.CodigoPromo,
                    Domicilio = x.Domicilio,
                    Contacto = x.Contacto,
                    CorreoPortal = x.CorreoPortal,
                    FechaFinPlan = x.FechaFinPlan,
                    IDCiudad = x.IDCiudad,
                    Personeria = x.Personeria,
                    PisoDepto = x.PisoDepto,
                    Logo = x.Logo,
                    //FechaUltLogin = x.FechaUltLogin.ToString("dd/MM/yyyy"),
                    UsaFechaFinPlan = x.UsaFechaFinPlan,
                    IDProvincia = x.IDProvincia,
                    IDUsuarioPadre = (x.IDUsuarioPadre == null) ? 0 : (Int32)x.IDUsuarioPadre,
                    PortalClientes = x.PortalClientes,
                    UsaProd = x.UsaProd,
                    listaPuntos = x.PuntosDeVenta.ToList(),
                    Observaciones = x.Observaciones,
                    ProvinciaNombre = x.Provincias.Nombre,
                    CiudadNombre = (x.Ciudades == null) ? "" : x.Ciudades.Nombre,
                    CantEmpresasHabilitadas = x.CantidadEmpresas,
                    ExentoIIBB = (x.ExentoIIBB != null) ? false : x.ExentoIIBB,
                    Pwd = x.IDUsuario != 2 ? x.Pwd : "",
                    UsaPrecioFinalConIVA = x.UsaPrecioFinalConIVA,
                    FechaBaja = x.FechaBaja,
                    MotivoBaja = (x.MotivoBaja == null) ? "" : x.MotivoBaja,
                    EstaBloqueado = x.EstaBloqueado,
                    EstaBloqueadoAd = x.UsuariosAdicionales.Any(y => y.EstaBloqueado),
                    EsContador = x.EsContador,
                    UsaPlanCorporativo = x.UsaPlanCorporativo,
                    Descuento = x.PorcentajeDescuento,
                    Actividad = x.Actividad,
                    TieneDebitoAutomatico = x.TieneDebitoAutomatico
                }).FirstOrDefault();

                var planActual = dbContext.PlanesPagos.Include("Planes").Where(x => x.IDUsuario == id && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();

                model.PlanActual = (planActual == null) ? "SIN Plan Actual" : planActual.Planes.Nombre;
                model.PlanEstado = (planActual == null) ? "SIN Plan Actual" : planActual.Estado;
                model.FechaFinPlanActual = (planActual == null) ? "SIN Plan Actual" : Convert.ToDateTime(planActual.FechaFinPlan).ToString("dd/MM/yyyy");
                model.TienePlanDeCuentas = dbContext.PlanDeCuentas.Any(x => x.IDUsuario == id);
                model.ListaUsuariosAd = dbContext.UsuariosAdicionales.Where(x => x.IDUsuario == id).Select(x => new UsuariosAdViewModel()
                {
                    Activo = (x.Activo) ? "SI" : "NO",
                    Correo = x.Email,
                    Pwd = x.Pwd,
                    NivelSeguridad = (x.Tipo == "A") ? "Administrador" : "Backoffice"
                }).ToList();


                model.ListaEmpresas = dbContext.Usuarios.Where(x => x.IDUsuarioPadre == id).ToList().Select(x => new EmpresasViewModel()
                {
                    ID = x.IDUsuario,
                    CondicionIva = x.CondicionIva,
                    CUIT = x.CUIT,
                    Email = x.Email,
                    Pwd = dbContext.UsuariosAdicionales.Any(y => y.Email == x.Email) ? dbContext.UsuariosAdicionales.Where(y => y.Email == x.Email).First().Pwd : "",
                    RazonSocial = x.RazonSocial,
                    Domicilio = x.Domicilio + ", " + x.PisoDepto,
                    TieneFacturaElectronica = (x.TieneFacturaElectronica) ? "SI" : "NO",
                    UsaProd = (x.UsaProd) ? "SI" : "NO"
                }).ToList();

                model.listaPlanesPagos = dbContext.PlanesPagos
                                                  .Include("Planes")
                                                  .Include("Usuarios")
                                                  .Include("Comprobantes")
                                                  .Include("Comprobantes.PuntosDeVenta")
                                                  .Where(x => x.IDUsuario == id)
                                                  .OrderBy(x => x.FechaInicioPlan).ToList();

                model.listaIntegraciones = dbContext.Integraciones.Include("ParametrosIntegracion")
                    .Where(x => x.IDUsuario == id).ToList().Select(x => new IntegracionesViewModel
                    {
                        ID = x.IDIntegracion,
                        Tipo = x.Tipo,
                        Descripcion = x.ParametrosIntegracion.Any(y => y.Clave == "Email") ? x.ParametrosIntegracion.Where(y => y.Clave == "Email").FirstOrDefault().Valor : "",
                        FechaAlta = x.FechaAlta,
                        FechaBaja = x.FechaEliminacion.HasValue ? x.FechaEliminacion.Value.ToString("dd/MM/yyyy") : ""
                    }).ToList();



                model.listaLoginUsuarios = dbContext.LoginUsuarios.Include("Usuarios")
                                                                  .Include("UsuariosAdicionales")
                                                                  .Where(x => x.IDUsuario == id && x.IDUsuarioAdicional == null).OrderByDescending(x => x.IDLogin).Take(10)
                                                                  .Select(x => new LoginUsuarioViewModel()
                                                                  {
                                                                      IDLoginUsuario = x.IDLogin,
                                                                      Email = x.EmailLogin,
                                                                      Fecha = x.FechaLogin,
                                                                      Observaciones = x.Observacion
                                                                  }).ToList();

                model.listaUsuarioAddins = dbContext.UsuarioAddins.Include("Addins").Include("ParametrosAddin").Where(x => x.IDUsuario == id).ToList();

                model.listaAddins = dbContext.Addins.ToList();

                model.listaLoginUsuariosAdicionales = dbContext.LoginUsuarios.Include("Usuarios")
                                                                  .Include("UsuariosAdicionales")
                                                                  .Where(x => x.IDUsuario == id && x.IDUsuarioAdicional != null).OrderByDescending(x => x.IDLogin).Take(10)
                                                                  .Select(x => new LoginUsuarioViewModel()
                                                                  {
                                                                      IDLoginUsuario = x.IDLogin,
                                                                      RazonSocial = x.Usuarios.RazonSocial,
                                                                      Email = x.EmailLogin,
                                                                      Fecha = x.FechaLogin,
                                                                      Observaciones = x.Observacion
                                                                  }).ToList();

                var fecha = DateTime.Now.Date.GetFirstDayOfMonth();
                var fechaFin = DateTime.Now.Date.GetLastDayOfMonth();
                model.Estadisticas = new EstadisticasViewModel();

                model.Estadisticas.CantClientes = dbContext.Personas.Count(x => x.IDUsuario == id && x.Tipo == "C");
                model.Estadisticas.CantProveedores = dbContext.Personas.Count(x => x.IDUsuario == id && x.Tipo == "P");

                model.Estadisticas.CantVentasTotal = dbContext.Comprobantes.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantVentasMes = dbContext.Comprobantes.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);

                model.Estadisticas.CantAbonosTotal = dbContext.Abonos.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantAbonosMes = dbContext.Abonos.Count(x => x.IDUsuario == id && x.FechaFin >= fecha && x.FechaFin <= fechaFin);

                model.Estadisticas.CantProductosTotal = dbContext.Conceptos.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantProductosMes = dbContext.Conceptos.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);

                model.Estadisticas.CantComprasTotal = dbContext.Compras.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantComprasMes = dbContext.Compras.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);

                model.Estadisticas.CantPagosTotal = dbContext.Pagos.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantPagosMes = dbContext.Pagos.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);

                model.Estadisticas.CantCobranzasTotal = dbContext.Cobranzas.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantCobranzasMes = dbContext.Cobranzas.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);

                model.Estadisticas.CantPresupuestosTotal = dbContext.Presupuestos.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantPresupuestosMes = dbContext.Presupuestos.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);

                model.Estadisticas.CantMovcajaTotal = dbContext.Caja.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantMovcajaMes = dbContext.Caja.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);

                model.Estadisticas.CantChequesTotal = dbContext.Cheques.Count(x => x.IDUsuario == id);
                model.Estadisticas.CantChequesMes = dbContext.Cheques.Count(x => x.IDUsuario == id && x.FechaAlta >= fecha && x.FechaAlta <= fechaFin);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult RenviarPwd(int id)
        {
            bool send = false;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == id).FirstOrDefault();
                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<PASSWORD>", usu.Pwd);
                    replacements.Add("<USUARIO>", usu.RazonSocial);
                    send = EmailCommon.SendMessage(EmailTemplate.RecuperoPwd.ToString(), replacements, usu.Email, "Contabilium: Recuperar contraseña");
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(send, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Guardar(int id, string condicionIva, string celular, string email, string observaciones,
            bool factElectronica, bool usaProd, int cantEmpresasHabilitadas, bool activo, string fechaBaja, string motivoBaja, bool estaBloqueado, bool estaBloqueadoAd)
        {
            bool resultado = true;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == id).FirstOrDefault();
                    //if (!email.IsValidEmailAddress())
                    //    resultado = false;
                    if (dbContext.Usuarios.Any(x => x.IDUsuario != id && x.Email == email))
                        resultado = false;

                    if (resultado)
                    {
                        usu.CondicionIva = condicionIva;
                        usu.Celular = celular;
                        usu.Email = email;
                        usu.Observaciones = observaciones;
                        usu.TieneFacturaElectronica = factElectronica;
                        usu.UsaProd = usaProd;
                        usu.CantidadEmpresas = cantEmpresasHabilitadas;
                        usu.MotivoBaja = motivoBaja;
                        usu.Activo = activo;

                        usu.EstaBloqueado = estaBloqueado;
                        usu.CantIntentos = (estaBloqueado) ? 3 : 0;

                        foreach (var item in usu.UsuariosAdicionales)
                        {
                            item.EstaBloqueado = estaBloqueadoAd;
                            item.CantIntentos = (estaBloqueadoAd) ? 3 : 0;
                        }

                        if (activo)
                            usu.FechaBaja = null;
                        else if (!string.IsNullOrEmpty(fechaBaja))
                            usu.FechaBaja = DateTime.Parse(fechaBaja);
                        else
                            usu.FechaBaja = DateTime.Now;

                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ObtenerUsuarios(string condicion, string periodo,
        string fechaDesde, string fechaHasta, string tipoPlan, string EstadoUsuario, string CodigoPromo, int page, int pageSize)
        {
            ResultadosUsuarioViewModel resultado = new ResultadosUsuarioViewModel();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.AdminUsuariosPlanesView.OrderByDescending(x => x.FechaAlta).AsQueryable();
                    var fecha = DateTime.Now.Date;

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    switch (EstadoUsuario)
                    {
                        case "Setup OK":
                        case "Total Activos":
                            results = results.Where(x => x.Activo == true && x.SetupRealizado == true && x.Estado == "Aceptado" && x.FechaFinPlan >= fecha).AsQueryable();
                            break;
                        case "Total Pendientes de pago":
                            results = results.Where(x => x.Activo == true && x.Estado == "Pendiente" && x.FechaFinPlan > fecha).AsQueryable();
                            break;
                        case "Total Inactivos":
                            results = results.Where(x => x.Activo == true && (x.SetupRealizado == false || x.FechaFinPlan < fecha || (x.Estado == "Aceptado" && x.FechaFinPlan < fecha))).AsQueryable();
                            break;
                        case "Dados de baja":
                            results = results.Where(x => x.Activo == false).AsQueryable();
                            break;
                        case "Total Usuarios":
                        default:
                            results = results.AsQueryable();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(tipoPlan))
                    {
                        results = results.Where(x => x.PlanActual.ToLower() == tipoPlan.ToLower()).AsQueryable();
                    }

                    //switch (tipoPlan)
                    //{
                    //    case "Basico":
                    //        results = results.Where(x => x.PlanActual == tipoPlan).ToList();
                    //        break;
                    //    case "Profesional":
                    //        results = results.Where(x => x.PlanActual == tipoPlan).ToList();
                    //        break;
                    //    case "Pyme":
                    //        results = results.Where(x => x.PlanActual == tipoPlan).ToList();
                    //        break;
                    //    case "Empresa":
                    //        results = results.Where(x => x.PlanActual == tipoPlan).ToList();
                    //        break;
                    //    case "Corporativo":
                    //        results = results.Where(x => x.PlanActual == tipoPlan).ToList();
                    //        break;
                    //    case "Prueba":
                    //        results = results.Where(x => x.PlanActual == tipoPlan).ToList();
                    //        break;
                    //}

                    //if (!string.IsNullOrWhiteSpace(tipoPlan))
                    //    results = results.Where(x => x.PlanActual.Contains(tipoPlan)).ToList();
                    if (condicion != "")
                    {
                        int idUsuario = 0;
                        bool esId = Int32.TryParse(condicion.Trim(), out idUsuario);

                        if (esId)
                        {
                            results = results.Where(x => x.IDUsuario == idUsuario).AsQueryable();
                        }
                        else
                        {
                            var ids = dbContext.Usuarios.Where(x => x.IDUsuarioPadre != null && x.RazonSocial.ToUpper().Contains(condicion.ToUpper()) || x.CUIT.Contains(condicion) || x.Email.ToUpper().Contains(condicion.ToUpper())
                            ).Select(x => x.IDUsuarioPadre).ToList();

                            results = results.Where(x => x.RazonSocial.ToUpper().Contains(condicion.ToUpper()) || x.CUIT.Contains(condicion) || x.Email.ToUpper().Contains(condicion.ToUpper())
                                || ids.Contains(x.IDUsuario)
                                ).AsQueryable();
                        }
                    }
                    else
                    {
                        results = results.Where(x => !(x.IDUsuarioPadre.HasValue)).AsQueryable();
                    }
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaAlta >= dtDesde).AsQueryable();
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaAlta <= dtHasta).AsQueryable();
                    }
                    if (CodigoPromo != "")
                        results = results.Where(x => x.CodigoPromo.ToUpper().Contains(CodigoPromo.ToUpper())).AsQueryable();

                    page--;

                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.Skip(page * pageSize).Take(pageSize).GroupBy(x => x.IDUsuario).ToList()
                        .Select(x => new UsuarioViewModel()
                        {
                            ID = x.FirstOrDefault().IDUsuario,
                            IDPadre = x.FirstOrDefault().IDUsuarioPadre ?? 0,
                            RazonSocial = x.FirstOrDefault().RazonSocial,
                            //(x.FirstOrDefault().Empresa != "") ? (x.FirstOrDefault().RazonSocial + " (" + x.FirstOrDefault().Empresa + ")") : x.FirstOrDefault().RazonSocial,
                            CUIT = x.FirstOrDefault().CUIT,
                            Telefono = x.FirstOrDefault().Telefono,
                            Email = x.FirstOrDefault().Email,
                            SetupRealizado = (x.FirstOrDefault().SetupRealizado) ? "SI" : "<span style='color: #fff;background-color: red;padding: 5px;'>NO</span>",
                            PlanActual = x.FirstOrDefault().PlanActual,
                            //AntiguedadMeses = Convert.ToInt32(x.FirstOrDefault().AntiguedadMeses),
                            CondicionIva = x.FirstOrDefault().CondicionIva,
                            FechaUltLogin = x.FirstOrDefault().FechaUltLogin.ToString("dd/MM/yyyy"),
                            //Baja = x.FirstOrDefault().Activo ? "NO" : "SI",
                            FechaAltaDesc = x.FirstOrDefault().FechaAlta.ToString("dd/MM/yyyy"),
                            CodigoPromo = x.FirstOrDefault().CodigoPromo,
                            Actividad = x.FirstOrDefault().Actividad
                        });
                    resultado.Items = list.ToList();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult export(string condicion, string periodo, string fechaDesde, string tipoPlan, string fechaHasta, string EstadoUsuario, string CodigoPromo)
        {
            string fileName = "Usuarios";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.Usuarios.AsQueryable();
                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }


                    if (EstadoUsuario != "")
                    {
                        var estadoUsuario = dbContext.AdminUsuariosPlanesView.Where(x => x.IDUsuario != 2).OrderBy(x => x.FechaAlta).ToList();
                        var fecha = DateTime.Now.Date;
                        switch (EstadoUsuario)
                        {
                            case "Setup OK":
                            case "Total Activos":
                                estadoUsuario = estadoUsuario.Where(x => x.Activo == true && x.SetupRealizado == true && x.Estado == "Aceptado" && x.FechaFinPlan >= fecha).ToList();
                                break;
                            case "Total Pendientes de pago":
                                estadoUsuario = estadoUsuario.Where(x => x.Activo == true && x.Estado == "Pendiente" && x.FechaFinPlan > fecha).ToList();
                                break;
                            case "Total Inactivos":
                                estadoUsuario = estadoUsuario.Where(x => x.Activo == true && (x.SetupRealizado == false || x.FechaFinPlan < fecha || (x.Estado == "Aceptado" && x.FechaFinPlan < fecha))).ToList();
                                break;
                            case "Dados de baja":
                                estadoUsuario = estadoUsuario.Where(x => x.Activo == false).ToList();
                                break;
                            case "Total Usuarios":
                            default:
                                estadoUsuario = estadoUsuario.Where(x => x.Activo == true).ToList();
                                break;
                        }

                        //if (!string.IsNullOrWhiteSpace(tipoPlan))
                        //{
                        //    estadoUsuario = estadoUsuario.Where(x => x.PlanActual.ToLower() == tipoPlan.ToLower()).ToList();
                        //}

                        var idUsuarios = estadoUsuario.Select(x => x.IDUsuario).ToList();
                        if (idUsuarios.Count() > 0)
                            results = results.Where(x => idUsuarios.Contains(x.IDUsuario));
                    }


                    if (!string.IsNullOrWhiteSpace(tipoPlan))
                    {
                        var listaIdUsuarios = dbContext.UsuariosPlanesView.Where(x => x.PlanActual.Contains(tipoPlan)).ToList().Select(x => x.IDUsuario).ToList();
                        results = results.Where(x => listaIdUsuarios.Contains(x.IDUsuario));
                    }



                    if (condicion != "")
                    {

                        var ids = dbContext.Usuarios.Where(x => x.IDUsuarioPadre != null && x.RazonSocial.ToUpper().Contains(condicion.ToUpper()) || x.CUIT.Contains(condicion) || x.Email.ToUpper().Contains(condicion.ToUpper())
                            ).Select(x => x.IDUsuarioPadre);


                        results = results.Where(x => x.RazonSocial.ToUpper().Contains(condicion.ToUpper()) || x.CUIT.Contains(condicion) || x.Email.ToUpper().Contains(condicion.ToUpper())
                            || ids.Contains(x.IDUsuario));

                    }
                    else
                        results = results.Where(x => x.IDUsuarioPadre == null);

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaAlta >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaAlta <= dtHasta);
                    }
                    if (CodigoPromo != "")
                        results = results.Where(x => x.CodigoPromo.ToUpper().Contains(CodigoPromo.ToUpper()));

                    dt = results.ToList()
                        .Select(x => new
                        {
                            ID = x.IDUsuario,
                            IDEmpresa = x.IDUsuarioPadre.HasValue ? x.IDUsuarioPadre.ToString() : "",
                            RazonSocial = x.RazonSocial,
                            CondicionIva = x.CondicionIva,
                            CUIT = x.CUIT,
                            Telefono = x.Telefono,
                            Email = x.Email,
                            EsAgentePercepcionIIBB = (x.EsAgentePercepcionIIBB == null) ? false : (bool)x.EsAgentePercepcionIIBB,
                            EsAgentePercepcionIVA = (x.EsAgentePercepcionIVA == null) ? false : (bool)x.EsAgentePercepcionIVA,
                            EsAgenteRetencion = (x.EsAgenteRetencion == null) ? false : (bool)x.EsAgenteRetencion,
                            MercadoPagoClientSecret = (x.MercadoPagoClientSecret == null) ? "" : x.MercadoPagoClientSecret,
                            MercadoPagoClientID = (x.MercadoPagoClientID == null) ? "" : x.MercadoPagoClientID,
                            SetupRealizado = x.SetupRealizado ? "SI" : "NO",
                            ApiKey = x.ApiKey,
                            TemplateFc = x.TemplateFc,
                            Celular = x.Celular,
                            IIBB = x.IIBB,
                            Activo = x.Activo,
                            EmailAlertas = x.EmailAlertas,
                            TieneFacturaElectronica = x.TieneFacturaElectronica,
                            FechaAlta = x.FechaAlta,
                            FechaInicioActividades = (x.FechaInicioActividades == null) ? DateTime.Now : (DateTime)x.FechaInicioActividades,
                            CodigoPostal = x.CodigoPostal,
                            Domicilio = x.Domicilio,
                            Contacto = x.Contacto,
                            CorreoPortal = x.CorreoPortal,
                            FechaFinPlan = x.FechaFinPlan,
                            Personeria = x.Personeria,
                            PisoDepto = x.PisoDepto,
                            Logo = (x.Logo == null) ? "" : x.Logo,
                            FechaUltLogin = x.FechaUltLogin,
                            PortalClientes = x.PortalClientes,
                            UsaProd = x.UsaProd,
                            Observaciones = (x.Observaciones == null) ? "" : x.Observaciones,
                            ProvinciaNombre = (x.Provincias == null) ? "" : x.Provincias.Nombre,
                            CiudadNombre = (x.Ciudades == null) ? "" : x.Ciudades.Nombre,
                            ExentoIIBB = (x.ExentoIIBB == null) ? false : (bool)x.ExentoIIBB,
                            CodigoPromo = (string.IsNullOrWhiteSpace(x.CodigoPromo)) ? "" : x.CodigoPromo,
                            MotivoBaja = (x.MotivoBaja == null) ? "" : x.MotivoBaja,
                            EsContador = x.EsContador ? "SI" : "NO",
                            UsaPlanCorporativo = x.UsaPlanCorporativo ? "SI" : "NO",
                            Actividad = x.Actividad
                        }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    CommonModel.GenerarArchivo(dt, Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                var archivo = (path + fileName + "_" + DateTime.Now.ToString("yyyMMdd") + ".xlsx").Replace("~", "");
                return Json(archivo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult HabilitarFCEmpresas(int id, bool tipo)
        {
            bool resultado = true;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == id).FirstOrDefault();

                    if (usu != null)
                    {
                        usu.TieneFacturaElectronica = tipo;
                        usu.UsaProd = tipo;
                        dbContext.SaveChanges();
                    }
                }
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SepararEmpresaHija(int id)
        {
            bool resultado = true;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == id).FirstOrDefault();

                    if (usu != null)
                    {
                        usu.IDUsuarioPadre = null;
                        usu.TieneDebitoAutomatico = false;//POR SI TENIA DEBITO AUTOMATICO HEREDADO DE EMPRESA PADRE
                        dbContext.SaveChanges();
                    }
                }
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult HabilitarPuntoVenta(int id)
        {
            bool resultado = true;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == id).FirstOrDefault();
                    if (punto != null)
                    {
                        punto.FechaBaja = null;
                        dbContext.SaveChanges();
                    }
                }

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        
        [HttpPost]
        public ActionResult AceptarPago(int id, int idUsuario)
        {
            var dto = new ErrorViewModel();

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var path = HttpContext.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["Admin.BasicLogError"]);
                    //BasicLog.AppendToFile(path, "AceptarPago ", "Empiza la funcion AceptarPago");

                    var planesPagos = dbContext.PlanesPagos.Where(x => x.IDPlanesPagos == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    var nr = planesPagos.NroReferencia;
                    var pathBase = ConfigurationManager.AppSettings["PathBaseWeb"];

                    var comprobanteViewModel = ComprobantesCommon.CrearDatosParaContabilium(nr, idUsuario, dbContext, planesPagos, pathBase, "Transferencia");
                    planesPagos.Estado = "Aceptado";
                    planesPagos.IDComprobante = comprobanteViewModel.comprobante.IDComprobante;

                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();

                    if (planesPagos.IDPlan == 5)// Plan Corporativo
                        usu.UsaPlanCorporativo = true;
                    else
                        usu.UsaPlanCorporativo = false;

                    dbContext.SaveChanges();
                    //BasicLog.AppendToFile(path, "AceptarPago ", "termina de generar los datos para contabilium");
                    //BasicLog.AppendToFile(path, "AceptarPago ", "Manda el mail");
                    //var send = EnviarComprobantePorEmail(comprobanteViewModel, pathBase);
                    //if (!send)
                    //    BasicLog.AppendToFile(path, "AceptarPago ", "la factura electronica no fue enviada");
                    PersonasCommon.CrearDatosParaElCliente(nr, idUsuario, dbContext, planesPagos, comprobanteViewModel.comprobante, comprobanteViewModel.nroComprobanteElectronico);
                    //BasicLog.AppendToFile(path, "AceptarPago ", "termina de dar crear los datos para el cliente");
                }
                dto.TieneError = false;
                return Json(dto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                dto.TieneError = true;
                dto.Mensaje = ex.Message;
                var path = HttpContext.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["Admin.BasicLogError"]);
                BasicLog.AppendToFile(path, "AceptarPago Error:", ex.Message);
                return Json(dto, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult CambiarEstadoDebitoAutomatico(int idUsuario, bool estadoDebitoAutomatico)
        {
            var dto = new ErrorViewModel();

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault().TieneDebitoAutomatico = estadoDebitoAutomatico;
                    dbContext.Usuarios.Where(x => x.IDUsuarioPadre == idUsuario).ToList().ForEach(x=>x.TieneDebitoAutomatico = estadoDebitoAutomatico);
                    dbContext.SaveChanges();
                }
                dto.TieneError = false;
                return Json(dto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                dto.TieneError = true;
                dto.Mensaje = ex.Message;
                var path = HttpContext.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["Admin.BasicLogError"]);
                BasicLog.AppendToFile(path, "AceptarPago Error:", ex.Message);
                return Json(dto, JsonRequestBehavior.AllowGet);
            }

        }

        /*private static bool EnviarComprobantePorEmail(ComprobanteNroViewModel comprobante, string pathBase)
        {

            System.Net.Mail.MailAddressCollection listTo = new System.Net.Mail.MailAddressCollection();
            listTo.Add(new System.Net.Mail.MailAddress(comprobante.comprobante.Personas.Email));

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<NOTIFICACION>", "Comprobante de contratación de plan");
            replacements.Add("<USUARIO>", comprobante.comprobante.Personas.RazonSocial);


            var nroComprobane = comprobante.comprobante.Personas.RazonSocial.Replace(" ", "_").Replace(",", "") + "_" + comprobante.comprobante.Tipo + "-" + comprobante.nroComprobanteElectronico + ".pdf";
            List<string> attachments = new List<string>();
            attachments.Add(Path.Combine(pathBase + "\\files\\explorer\\" + comprobante.comprobante.Usuarios.IDUsuario + "\\comprobantes\\" + DateTime.Now.Year.ToString() + "\\" + nroComprobane));
            bool send = EmailCommon.SendMessage(comprobante.comprobante.IDUsuario, EmailTemplate.EnvioComprobante.ToString(), replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], comprobante.comprobante.Personas.Email, "Comprobante electrónico", attachments, comprobante.comprobante.Usuarios.RazonSocial);

            return send;
        }*/

        [HttpPost]
        public ActionResult ConfigurarPlanCorporativo(int idUsuario)
        {
            var dto = new ErrorViewModel();
            Usuarios usuario;
            dto.TieneError = false;
            try
            {
                List<Bancos> listaBancos = new List<Bancos>();
                using (var dbContext = new ACHEEntities())
                {
                    usuario = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    if (!usuario.UsaPlanCorporativo)
                    {
                        usuario.UsaPlanCorporativo = true;
                        dbContext.SaveChanges();
                    }

                    if (usuario.UsaPlanCorporativo && usuario.CondicionIva == "RI") //Plan Corporativo
                    {
                        if (usuario != null)
                        {
                            var planDeCta = dbContext.PlanDeCuentas.Any(x => x.IDUsuario == idUsuario);
                            if (!planDeCta)
                            {
                                dbContext.ConfigurarPlanCorporativo(idUsuario);
                                var usuEmpresa = TokenCommon.ObtenerWebUser(idUsuario);
                                foreach (var item in dbContext.Bancos.Where(x => x.IDUsuario == idUsuario).ToList())
                                    ContabilidadCommon.CrearCuentaBancos(item.IDBanco, null, usuEmpresa);

                                var listaCajas = dbContext.Cajas.Where(x => x.IDUsuario == idUsuario).ToList();
                                foreach (var item in listaCajas)
                                    ContabilidadCommon.CrearCuentaCaja(item.IDCaja, usuEmpresa);

                            }
                            else
                            {
                                dto.TieneError = true;
                                dto.Mensaje = "El usuario seleccionado ya tiene un plan de cuentas cargado.";
                            }
                        }
                        else
                        {
                            dto.TieneError = true;
                            dto.Mensaje = "IDUsuario incorrecto o invalido.";
                        }
                    }
                    else
                    {
                        dto.TieneError = true;
                        dto.Mensaje = "El usuario NO tiene el plan Corporativo activo.";
                    }
                }

                if (!dto.TieneError)
                {
                    ConfigurarBancos(idUsuario, listaBancos, usuario.CondicionIva);
                }
                return Json(dto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                dto.TieneError = true;
                dto.Mensaje = ex.Message;
                return Json(dto, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult InhabililitarPlanCorporativo(int idUsuario)
        {
            var dto = new ErrorViewModel();
            Usuarios usuario;
            dto.TieneError = false;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    usuario = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    if (usuario.UsaPlanCorporativo)
                    {
                        usuario.UsaPlanCorporativo = false;
                        dbContext.SaveChanges();
                    }

                }

                return Json(dto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                dto.TieneError = true;
                dto.Mensaje = ex.Message;
                return Json(dto, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EdicionPlanPago(int id, int? idPlanPago)
        {
            PlanesPagos planDePago;

            ViewBag.EstadosList = new[] { new { Nombre = "Aceptado" }, new { Nombre = "Pendiente" }, new { Nombre = "Cancelled" }, new { Nombre = "Rejected" } };
            ViewBag.FormaDePagosList = new[] { new { Nombre = "Mercado Pago" }, new { Nombre = "Transferencia" }, new { Nombre = "Depósito" }, new { Nombre = "Débito" }, new { Nombre = "PayU" } };

            using (var dbContext = new ACHEEntities())
            {
                ViewBag.PlanesList = dbContext.Planes.ToList();

                if (idPlanPago.HasValue)
                {
                    planDePago = dbContext.PlanesPagos.Include("Planes").Include("Comprobantes")
                        .FirstOrDefault(x => x.IDPlanesPagos == idPlanPago.Value);
                }
                else
                {

                    planDePago = new PlanesPagos { IDUsuario = id };
                }
                return View(planDePago);
            }
            return null;
        }

        //public ActionResult EdicionDebitoAutomatico(int id, int? idPlanPago) {
        //    DebitoAutomaticoViewModel debitoAutomatico;
        //    using (var dbContext = new ACHEEntities()) {
        //        ViewBag.PlanesList = dbContext.Planes.ToList();

        //        debitoAutomatico = new DebitoAutomaticoViewModel { IDUsuario = id };

        //        return View(debitoAutomatico);
        //    }
        //}

        private void ConfigurarBancos(int idUsuario, List<Bancos> listaBancos, string condicionIVA)
        {
            var usu = TokenCommon.ObtenerWebUser(idUsuario);
            foreach (var item in listaBancos)
                ContabilidadCommon.CrearCuentaBancos(item.IDBanco, null, usu);
        }

        [HttpPost]
        public ActionResult GuardarPlanDePago(int id, int idUsuario, string fechaInicioPlan, string fechaVencimiento, string fechaPago, string importe,
            string formaPago, string nroReferencia, string estado, string planes, bool pagoAnual)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    PlanesPagos planPago = null;
                    if (id > 0)
                        planPago = dbContext.PlanesPagos.Where(x => x.IDPlanesPagos == id).FirstOrDefault();
                    else
                        planPago = new PlanesPagos();

                    if (!string.IsNullOrEmpty(fechaInicioPlan))
                        planPago.FechaInicioPlan = DateTime.Parse(fechaInicioPlan);
                    if (!string.IsNullOrEmpty(fechaVencimiento))
                        planPago.FechaFinPlan = DateTime.Parse(fechaVencimiento);

                    if (planPago.FechaFinPlan.Value.Date <= planPago.FechaInicioPlan.Value.Date)
                        throw new Exception("Las fechas de inicio/fin son inválidas");

                    planPago.Estado = estado;

                    if (id == 0)
                    {
                        planPago.IDUsuario = idUsuario;
                        planPago.FechaDePago = DateTime.Parse(fechaPago);
                        planPago.ImportePagado = decimal.Parse(importe.Replace(".", ","));
                        planPago.FormaDePago = formaPago;
                        planPago.NroReferencia = nroReferencia;
                        planPago.IDPlan = int.Parse(planes);
                        planPago.PagoAnual = pagoAnual;
                        planPago.FechaDeAlta = DateTime.Now;
                        dbContext.PlanesPagos.Add(planPago);
                    }
                    else
                        planPago.IDPlan = int.Parse(planes);

                    if (!dbContext.PlanesPagos.Any(x => x.IDUsuario == idUsuario && x.IDPlan != 6 && x.Estado == "Aceptado") && planPago.IDPlan == 5)
                        dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault().CantidadEmpresas = 4;



                    dbContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EliminarPlanDePago(int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    PlanesPagos planPago = null;
                    if (id > 0)
                    {
                        planPago = dbContext.PlanesPagos.FirstOrDefault(x => x.IDPlanesPagos == id);
                        dbContext.PlanesPagos.Remove(planPago);
                    }

                    dbContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GuardarDescuentoYPromo(int idUsuario, int descuento, string promo)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    usu.PorcentajeDescuento = descuento;
                    usu.CodigoPromo = promo.Trim();

                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult agregarPunto(int idUsuario, int punto)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    PuntoDeVentaCommon.GuardarPuntoDeVenta(punto, idUsuario);
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult eliminarPunto(int idUsuario, int punto)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    PuntoDeVentaCommon.EliminarPuntoDeVenta(punto, idUsuario);
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult obtenerPuntos(int idUsuario)
        {
            var html = string.Empty;
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario).ToList();
                    //var list = UsuarioCommon.ObtenerPuntosDeVenta(usu, true);
                    if (list.Any())
                    {
                        int index = 1;
                        foreach (var punto in list)
                        {
                            html += "<tr>";
                            html += "<td>" + index + "</td>";
                            html += "<td>" + punto.Punto.ToString("#0000") + "</td>";
                            html += "<td>" + punto.FechaAlta.ToString("dd/MM/yyyy") + "</td>";
                            if (punto.FechaBaja.HasValue)
                                html += "<td>" + punto.FechaBaja.Value.ToString("dd/MM/yyyy") + "</td>";
                            else
                                html += "<td></td>";

                            if (!punto.FechaBaja.HasValue)
                                html += "<td>" + (!punto.PorDefecto ? "<a title='Poner por defecto' style='font-size: 14px;color:lightgray;' href='javascript:usuario.ponerPorDefecto(" + idUsuario + "," + punto.IDPuntoVenta + ");'><i class='fa fa-circle-o''></i></a>" : "<a href='#' title='Punto de venta por defecto' style='font-size: 16px;' );'><i class='fa fa-check' style='color: green;'></i></a>") + "</td>";
                            else
                                html += "<td></td>";
                            if (!punto.FechaBaja.HasValue)
                                html += "<td>" + (!punto.PorDefectoIntegraciones ? "<a title='Poner por defecto para integraciones' style='font-size: 14px;color:lightgray;' href='javascript:usuario.ponerPorDefectoIntegraciones(" + idUsuario + "," + punto.IDPuntoVenta + ");'><i class='fa fa-circle-o''></i></a>" : "<a href='#' title='Punto de venta por defecto para integraciones' style='font-size: 16px;' );'><i class='fa fa-check' style='color: green;'></i></a>") + "</td>";
                            else
                                html += "<td></td>";

                            if (!punto.PorDefecto && !punto.FechaBaja.HasValue)
                                html += "<td><a title='Dar de baja' style='font-size: 16px;' href='javascript:usuario.eliminarPunto(" + idUsuario + "," + punto.IDPuntoVenta + ");'><i class='fa fa-times'></i></a></td>";
                            else if (punto.FechaBaja.HasValue)
                            {
                                html += "<td><a title='Habilitar punto de venta' style='font-size: 16px;' href='javascript:usuario.habilitarPuntoVentaDinamico(" + idUsuario + "," + punto.IDPuntoVenta + ");'><i class='fa fa-hand-o-up' style='color: green;'></i></a></td>";
                            }
                            else
                                html += "<td><a title='Dar de baja' style='font-size: 16px;' href='javascript:usuario.eliminarPunto(" + idUsuario + "," + punto.IDPuntoVenta + ");'><i class='fa fa-times'></i></a>";
                            index++;
                            html += "</tr>";
                        }
                    }
                }
                if (html == "")
                    html = "<tr><td colspan='5' style='text-align:center'>No tienes puntos de venta registrados</td></tr>";

            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            //return html;
            return Json(html, JsonRequestBehavior.AllowGet);

        }

        /*
                [HttpPost]
                public ActionResult HabilitarPuntoVenta(int idUsuario,int id)
                {
                    try
                    {
                        using (var dbContext = new ACHEEntities())
                        {
                            PuntosDeVenta entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == id && x.IDUsuario == idUsuario).FirstOrDefault();
                            if (entity != null)
                                entity.FechaBaja = null;
                            dbContext.SaveChanges();
                        }
                    }
                    catch (Exception)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                    //return html;
                    return Json(true, JsonRequestBehavior.AllowGet);
                }*/

        [HttpPost]
        public ActionResult GuardarPorDefecto(int idUsuario, int idPunto)
        {
            try
            {


                using (var dbContext = new ACHEEntities())
                {
                    var ListaPuntos = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario);

                    foreach (var item in ListaPuntos)
                    {
                        if (item.IDPuntoVenta == idPunto)
                            item.PorDefecto = true;
                        else
                            item.PorDefecto = false;
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            //return html;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GuardarPorDefectoIntegraciones(int idUsuario, int idPunto)
        {
            try
            {


                using (var dbContext = new ACHEEntities())
                {
                    var ListaPuntos = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario);

                    foreach (var item in ListaPuntos)
                    {
                        if (item.IDPuntoVenta == idPunto)
                            item.PorDefectoIntegraciones = true;
                        else
                            item.PorDefectoIntegraciones = false;
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);

        }
    }
}
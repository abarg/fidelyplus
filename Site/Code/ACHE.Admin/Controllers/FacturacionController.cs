﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACHE.Model;
using ACHE.Extensions;
using ACHE.Admin.Models;
using System.Configuration;
using System.Collections.Specialized;
using System.Data;
using System.IO;

namespace ACHE.Admin.Controllers
{
    public class FacturacionController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ObtenerUsuarios(string condicion, string periodo, string fechaDesde, string fechaHasta,string anualMensual, bool modoPrueba, int page, int pageSize)
        {
            ResultadosUsuarioViewModel resultado = new ResultadosUsuarioViewModel();
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    IQueryable<UsuariosPlanesView> results = null;// = new IQueryable<UsuariosPlanesView>();
                    var resultsFinal = new List<UsuariosPlanesView>();
                    var fecha = DateTime.Now.Date;

                    switch (periodo)
                    {
                        case "-1": //Otro Período
                            results = dbContext.UsuariosPlanesView.OrderByDescending(x => x.FechaFinPlan).AsQueryable();
                            break;
                        case "1": //Planes vencidos
                            results = dbContext.UsuariosPlanesView.Where(x => x.FechaFinPlan <= fecha).OrderByDescending(x => x.FechaFinPlan).AsQueryable();
                            if (fechaDesde != string.Empty)
                            {
                                DateTime dtDesde = DateTime.Parse(fechaDesde);
                                results = results.Where(x => x.FechaFinPlan >= dtDesde).AsQueryable();
                            }
                            if (fechaHasta != string.Empty)
                            {
                                DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                                results = results.Where(x => x.FechaFinPlan <= dtHasta).AsQueryable();
                            }
                            break;
                        case "2": // Planes a vencer
                            results = dbContext.UsuariosPlanesView.Where(x => x.FechaFinPlan > fecha).OrderByDescending(x => x.FechaFinPlan).AsQueryable();
                            if (fechaDesde != string.Empty)
                            {
                                DateTime dtDesde = DateTime.Parse(fechaDesde);
                                results = results.Where(x => x.FechaFinPlan >= dtDesde).AsQueryable();
                            }
                            if (fechaHasta != string.Empty)
                            {
                                DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                                results = results.Where(x => x.FechaFinPlan <= dtHasta).AsQueryable();
                            }
                            break;
                    }

                    switch (anualMensual) {
                        case "1": //Planes mensuales
                            results = results.Where(x => x.PagoAnual == false).AsQueryable();
                            break;
                        case "2": // Planes anuales
                            results = results.Where(x => x.PagoAnual == true).AsQueryable();
                            break;
                    }

                    if (modoPrueba)
                        results = results.Where(x => x.IDPlan != 6).AsQueryable();
                    resultsFinal = results.ToList();

                    page--;

                    resultado.TotalPage = ((resultsFinal.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = resultsFinal.Count();

                    var list = resultsFinal.GroupBy(x => x.IDUsuario).OrderBy(x => x.FirstOrDefault().FechaFinPlan.Value).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new UsuarioViewModel()
                        {
                            ID = x.FirstOrDefault().IDUsuario,
                            RazonSocial = x.FirstOrDefault().RazonSocial,
                            CUIT = x.FirstOrDefault().CUIT,
                            Telefono = x.FirstOrDefault().Telefono,
                            Email = x.FirstOrDefault().Email,
                            SetupRealizado = (x.FirstOrDefault().SetupRealizado) ? "SI" : "<span style='color: #fff;background-color: red;padding: 5px;'>NO</span>",
                            PlanActual = x.FirstOrDefault().PlanActual,
                            AntiguedadMeses = Convert.ToInt32(x.FirstOrDefault().AntiguedadMeses),
                            CondicionIva = x.FirstOrDefault().CondicionIva,
                            FechaUltLogin = x.FirstOrDefault().FechaUltLogin.ToString("dd/MM/yyyy"),
                            Baja = x.FirstOrDefault().Activo ? "NO" : "SI",
                            FechaAltaDesc = x.FirstOrDefault().FechaAlta.ToString("dd/MM/yyyy"),
                            CodigoPromo = x.FirstOrDefault().CodigoPromo,
                            VencimientoPago = x.FirstOrDefault().FechaFinPlan.Value.ToString("dd/MM/yyyy"),

                        });
                    resultado.Items = list.ToList();
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult export(string condicion, string periodo, string fechaDesde, string fechaHasta, bool modoPrueba)
        {
            string fileName = "Facturacion";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {

                    IQueryable<UsuariosPlanesView> results = null;// = new IQueryable<UsuariosPlanesView>();
                    var resultsFinal = new List<UsuariosPlanesView>();
                    var fecha = DateTime.Now.Date;

                    switch (periodo)
                    {
                        case "-1": //Otro Período
                            results = dbContext.UsuariosPlanesView.OrderByDescending(x => x.FechaFinPlan).AsQueryable();
                            break;
                        case "1": //Planes vencidos
                            results = dbContext.UsuariosPlanesView.Where(x => x.FechaFinPlan <= fecha).OrderByDescending(x => x.FechaFinPlan).AsQueryable();
                            if (fechaDesde != string.Empty)
                            {
                                DateTime dtDesde = DateTime.Parse(fechaDesde);
                                results = results.Where(x => x.FechaFinPlan >= dtDesde).AsQueryable();
                            }
                            if (fechaHasta != string.Empty)
                            {
                                DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                                results = results.Where(x => x.FechaFinPlan <= dtHasta).AsQueryable();
                            }
                            break;
                        case "2": // Planes a vencer
                            results = dbContext.UsuariosPlanesView.Where(x => x.FechaFinPlan > fecha).OrderByDescending(x => x.FechaFinPlan).AsQueryable();
                            if (fechaDesde != string.Empty)
                            {
                                DateTime dtDesde = DateTime.Parse(fechaDesde);
                                results = results.Where(x => x.FechaFinPlan >= dtDesde).AsQueryable();
                            }
                            if (fechaHasta != string.Empty)
                            {
                                DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                                results = results.Where(x => x.FechaFinPlan <= dtHasta).AsQueryable();
                            }
                            break;
                    }

                    if (modoPrueba)
                        results = results.Where(x => x.IDPlan != 6).AsQueryable();
                    resultsFinal = results.ToList();

                    dt = resultsFinal.GroupBy(x => x.IDUsuario)
                        .Select(x => new
                        {
                            ID = x.FirstOrDefault().IDUsuario,
                            RazonSocial = x.FirstOrDefault().RazonSocial,
                            CUIT = x.FirstOrDefault().CUIT,
                            Telefono = x.FirstOrDefault().Telefono,
                            Email = x.FirstOrDefault().Email,
                            CodigoPromo = x.FirstOrDefault().CodigoPromo,
                            SetupRealizado = (x.FirstOrDefault().SetupRealizado) ? "SI" : "NO",
                            PlanActual = x.FirstOrDefault().PlanActual,
                            AntiguedadMeses = Convert.ToInt32(x.FirstOrDefault().AntiguedadMeses),
                            CondicionIva = x.FirstOrDefault().CondicionIva,
                            VencimientoPago = x.FirstOrDefault().FechaFinPlan.Value.ToString("dd/MM/yyyy"),
                            FechaUltLogin = x.FirstOrDefault().FechaUltLogin.ToString("dd/MM/yyyy"),
                            Baja = x.FirstOrDefault().Activo ? "NO" : "SI",
                            FechaAltaDesc = x.FirstOrDefault().FechaAlta.ToString("dd/MM/yyyy")
                        }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                var archivo = (path + fileName + "_" + DateTime.Now.ToString("yyyMMdd") + ".xlsx").Replace("~", "");
                return Json(archivo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
﻿using ACHE.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACHE.Model;
using ACHE.Extensions;
using System.Web.Security;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Configuration;
using ACHE.Negocio.Common;
using System.Web.Script.Serialization;

namespace ACHE.Admin.Controllers
{
    public class AccountController : BaseController
    {
        // GET: /Account/
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var Usuario = model.Usuario;
                    var pass = model.Password;
                    var usuAdmin = ConfigurationManager.AppSettings["Admin.usu"];
                    var usuPwd = ConfigurationManager.AppSettings["Admin.pwd"];

                    if (usuAdmin != Usuario || usuPwd != pass)
                    {
                        ModelState.AddModelError("", "Usuario y/o contraseña incorrecto");
                        return View(model);
                    }
                    else
                    {
                        //CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel("Admin", "", 1, "", "", "");
                        ////Se deberia hacer el  Response.SetAuthCookie, sin enviar el mail. Sino cualquiera puede acceder a ese metodo desde la URL. NO es seguro
                        //Response.SetAuthCookie("1", false, serializeModel);

                        Session["Contabilium.Admin"] = Usuario;

                        HttpCookie cookieUserName = new HttpCookie("Contabilium.Usuario");
                        cookieUserName.Value = HttpContext.Server.UrlEncode(model.Usuario);
                        cookieUserName.Expires = DateTime.Now.AddDays(14);
                        HttpContext.Response.Cookies.Add(cookieUserName);

                        HttpCookie cookiePwd = new HttpCookie("Contabilium.Pwd");
                        cookiePwd.Value = HttpContext.Server.UrlEncode(model.Password);
                        cookiePwd.Expires = DateTime.Now.AddDays(14);
                        HttpContext.Response.Cookies.Add(cookiePwd);

                        var serializer = new JavaScriptSerializer();

                        AdminWebUser webUser = new AdminWebUser();
                        webUser.IDUsuario = 0;
                        webUser.NombreUsuario = "admin";
                        
                        string userData = serializer.Serialize(webUser);

                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                                "admin",
                                DateTime.Now,
                                DateTime.Now.AddDays(30),
                                true,
                                userData);

                        // Encrypt the ticket.
                        string encTicket = FormsAuthentication.Encrypt(ticket);
                        // Create the cookie.
                        Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
                        
                        return RedirectToAction("Index", "Usuario");
                    }
                }
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {

            Session.Abandon();
            Session["Contabilium.Admin"] = null;
            FormsAuthentication.SignOut();

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(cookie);

            HttpCookie cookieUserName = new HttpCookie("Contabilium.Usuario");
            cookieUserName.Expires = DateTime.Now.AddDays(-1);
            cookieUserName.Value = "";
            HttpContext.Response.Cookies.Add(cookieUserName);


            HttpContext.Response.Cookies.Add(cookieUserName);

            HttpCookie cookiePwd = new HttpCookie("Contabilium.Pwd");
            cookiePwd.Expires = DateTime.Now.AddDays(-1);
            cookiePwd.Value = "";
            HttpContext.Response.Cookies.Add(cookiePwd);
    
            return RedirectToAction("Login", "Account", null);
        }
        
        protected string WindowStatusText = "";

        [HttpPost]
        public void KeepSession(string pwdOld, string pwd) {
            //string RefreshValue = Convert.ToString((Session.Timeout * 60) - 90);

            // Refresh this page 60 seconds before session timeout, effectively resetting the session timeout counter.
            //MetaRefresh.Attributes["content"] = RefreshValue + ";url=KeepSessionAlive.aspx?q=" + DateTime.Now.Ticks;
            //MetaRefresh.Attributes["content"] = "30;URL=/KeepSession.aspx?q=" + DateTime.Now.Ticks;

            WindowStatusText = "Last refresh " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
        }
    }
}
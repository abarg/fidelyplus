﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACHE.Admin.Models;
using ACHE.Model;
using ACHE.Extensions;

namespace ACHE.Admin.Models
{
    public class CustomFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // This method is executed before calling the action
            // and here you have access to the route data:
            //var foo = filterContext.RouteData.Values["foo"];
            // TODO: use the foo route value to perform some action

            /*if (filterContext.HttpContext.User == null || !filterContext.HttpContext.User.Identity.IsAuthenticated)
            {

                if (filterContext.HttpContext.Request.Cookies["Contabilium.Usuario"] != null && filterContext.HttpContext.Request.Cookies["Contabilium.Pwd"] != null)
                {
                    var userCookie = filterContext.HttpContext.Server.UrlDecode(filterContext.HttpContext.Request.Cookies["Contabilium.Usuario"].Value);
                    var passCookie = filterContext.HttpContext.Server.UrlDecode(filterContext.HttpContext.Request.Cookies["Contabilium.Pwd"].Value);

                    if (userCookie != string.Empty && passCookie != string.Empty)
                    {
                        ////Chequeo usuariocookie
                        //using (var dbContext = new ACHEEntities())
                        //{
                        //    var user = dbContext.Usuarios.Where(x => x.NombreUsuario.ToLower() == userCookie.Trim().ToLower()
                        //        && x.Pwd == passCookie && x.Estado.ToLower() == "aceptado" && x.FechaActivacion.HasValue && x.Activo).FirstOrDefault();
                        //    if (user != null)
                        //    {
                        //actualizo cookies
                        HttpCookie cookieUserName = new HttpCookie("Contabilium.Usuario");
                        cookieUserName.Expires = DateTime.Now.AddDays(14);
                        cookieUserName.Value = filterContext.HttpContext.Server.UrlEncode(userCookie.Trim().ToLower());
                        filterContext.HttpContext.Response.Cookies.Add(cookieUserName);

                        HttpCookie cookiePwd = new HttpCookie("Contabilium.Pwd");
                        cookiePwd.Value = filterContext.HttpContext.Server.UrlEncode(passCookie);
                        cookiePwd.Expires = DateTime.Now.AddDays(14);
                        filterContext.HttpContext.Response.Cookies.Add(cookiePwd);

                        var nombreUsuario = "admin";
                        CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel("ädmin", "", "", "", 0);
                        filterContext.HttpContext.Response.SetAuthCookie(nombreUsuario, true, serializeModel);
                        filterContext.Result = new RedirectResult("~/Home/Index");
                        //    }
                        //}
                    }
                }
            }*/

            base.OnActionExecuting(filterContext);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.EnvioMailsBatch
{
    public class EnvioMailDTO
    {
        public int IdUsuario { get; set; }
        public string Template { get; set; }
        public ListDictionary Reemplazos { get; set; }
        public MailAddressCollection To { get; set; }
        public string From { get; set; }
        public string FromNombre { get; set; }
        public string Cc { get; set; }
        public string Asunto { get; set; }
        public List<string> Adjuntos { get; set; }
        public string Error { get; set; }

    }
}

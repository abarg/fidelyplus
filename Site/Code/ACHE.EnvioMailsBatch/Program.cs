﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;

namespace ACHE.EnvioMailsBatch
{
    class Program
    {
        private static Dictionary<int, SmtpSettings> SmtpSettings { get; set; }

        static void Main(string[] args)
        {
            var path = ConfigurationManager.AppSettings["EnvioMailsBatchLogError"];
            var cantProcesados = 0;
            var cantErrores = 0;

            using (var dbContext = new ACHEEntities())
            {
                //BasicLog.AppendToFileInfo(path, "Antes de obtener mails a enviar " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                //Obtener los mails que no se enviaron o que se enviaron con error
                IEnumerable<EnvioMails> mailsEnviar = ObtenerEnviosPendientes(dbContext);
                //BasicLog.AppendToFileInfo(path, "Despues de obtener mails a enviar " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");

                if (mailsEnviar.Count() > 0)
                {

                    //BasicLog.AppendToFileInfo(path, "****** Inicia proceso EnvioMailsBatch a enviar " + mailsEnviar.Count() + " - " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " ******", "");

                    //Obtener los datos de smtp de cada usuario
                    //BasicLog.AppendToFileInfo(path, "Antes de obtener settings" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                    var listaIds = mailsEnviar.Select(x => x.IDUsuario).ToList();
                    SmtpSettings = AddinsCommon.ObtenerSmtpSettings(ConfigurationManager.AppSettings["Addins.MailsCustomizable"], listaIds);
                    //BasicLog.AppendToFileInfo(path, "Despues de obtener settings" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");

                    foreach (EnvioMails mail in mailsEnviar)
                    {
                        //enviar el mail
                        try
                        {
                            //BasicLog.AppendToFileInfo(path, "Antes de obtener el maildto y las setting para usuario" + mail.IDUsuario + ". " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                            var mailDto = ObtenerMailDTO(mail);

                            var settings = (SmtpSettings != null && SmtpSettings.ContainsKey(mail.IDUsuario)) ? SmtpSettings[mail.IDUsuario] : null;
                            if (settings != null)
                            {
                                mailDto.From = settings.EmailFrom;
                            }
                            //BasicLog.AppendToFileInfo(path, "Despues de obtener el maildto y las setting para usuario " + mail.IDUsuario + ". " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");

                            //BasicLog.AppendToFileInfo(path, "Antes de Envio de mail para usuario " + mail.IDUsuario + ". " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");
                            var send = EmailCommon.SendMessageBatch(mailDto.IdUsuario,
                                (EmailTemplate)Enum.Parse(typeof(EmailTemplate), mailDto.Template), mailDto.Reemplazos,
                                mailDto.To, mailDto.From, mailDto.Cc, mailDto.Asunto, mailDto.Adjuntos, mailDto.FromNombre,
                                settings);
                            //BasicLog.AppendToFileInfo(path, "Despues de Envio de mail para usuario " + mail.IDUsuario + ". " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "");

                            if (send)
                                mail.Error = string.Empty;
                            else
                                throw new Exception("El correo no fue enviado");

                        }
                        catch (Exception ex)
                        {
                            cantErrores++;
                            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            mail.Error = msg;
                            mail.Intentos = mail.Intentos + 1;
                            BasicLog.AppendToFile(path, msg, ex.ToString());
                        }
                        cantProcesados++;

                        //Actualizacion registro
                        mail.FechaEnvio = DateTime.Now;
                        dbContext.SaveChanges();
                    }
                    BasicLog.AppendToFileInfo(path, string.Format("****** Termina proceso EnvioMailsBatch " + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + ", cantidad procesados: {0}, cantidad errores: {1} ******", cantProcesados, cantErrores), "");
                }
            }
        }

        private static EnvioMailDTO ObtenerMailDTO(EnvioMails m)
        {
            ListDictionary reempDictionary = new ListDictionary();
            MailAddressCollection listTo = new MailAddressCollection();

            var aux = m.Reemplazos.Split('^');
            foreach (var item in m.Reemplazos.Split('^'))
            {
                var kv = item.Split('=');
                if (kv.Length == 2)
                {
                    reempDictionary.Add(kv[0], kv[1]);
                }
                else
                {//si viene por aca es porque hay html en el valor del reemplazo
                    string html = "";
                    int index = 1;
                    while (index < kv.Length)
                    {
                        html += kv[index] + "=";
                        index++;
                    }
                    html = html.Substring(0, html.Length - 1);

                    reempDictionary.Add(kv[0], html);
                }
            }

            //.ToList()
            //.ForEach(kv => reempDictionary.Add(kv.Split('=')[0], kv.Split('=')[1]));
            m.Receptor.Split(',')
                         .ToList()
                         .ForEach(x => listTo.Add(x));

            if (m.Adjuntos != null)
            {

                return new EnvioMailDTO
                {
                    IdUsuario = m.IDUsuario,
                    Template = m.Template,
                    Adjuntos = m.Adjuntos.Split(',').ToList(),
                    Asunto = m.Asunto,
                    Cc = m.Cc,
                    Error = m.Error,
                    From = m.Emisor,
                    To = listTo,
                    FromNombre = m.EmisorNombre,
                    Reemplazos = reempDictionary
                };
            }
            else
            {
                return new EnvioMailDTO
                {
                    IdUsuario = m.IDUsuario,
                    Template = m.Template,
                    Adjuntos = new List<string>(),//  m.Adjuntos.Any() ? m.Adjuntos.Split(',').ToList() : (new List<string>()),
                    Asunto = m.Asunto,
                    Cc = m.Cc,
                    Error = m.Error,
                    From = m.Emisor,
                    To = listTo,
                    FromNombre = m.EmisorNombre,
                    Reemplazos = reempDictionary
                };
            }
        }

        private static IEnumerable<EnvioMails> ObtenerEnviosPendientes(ACHEEntities dbContext)
        {
            try
            {
                return dbContext.EnvioMails.Where(e => e.FechaEnvio == null || (!string.IsNullOrEmpty(e.Error) && e.Intentos < 3)).ToList();
            }
            catch (Exception e)
            {
                var path = ConfigurationManager.AppSettings["EnvioMailsBatchLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
                return null;
            }
        }
    }
}

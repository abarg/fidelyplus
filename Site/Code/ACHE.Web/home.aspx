﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .row-centered {
            text-align: center;
        }

        .col-centered {
            display: inline-block;
            float: none;
            /* reset the text-align */
            text-align: left;
            /* inline-block space fix */
            margin-right: -4px;
        }

        .panel-stat h1 {
            font-size: 30px !important;
        }

        .panel-stat h1.small {
            font-size: 25px !important;
        }

        .panel-stat .stat {
            max-width: 500px;
        }

        .accesos-cc {
            font-size: 15px;
            font-family: "Roboto",sans-serif;
            line-height: normal;
            margin: 0px;
            color: rgb(99, 110, 123);
        }

    </style>
    <link href="css/morris.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="contentpanel">
        
        <%--<div class="row mb15">
	        <div class="alert alert-warning fade in nomargin">
		        El día Sábado 31/03 a las <b>20:00 hrs</b> se realizarán tareas de mantenimiento en el servidor y la aplicación NO estará disponible por el lapso de 1 hora.
	        </div>
	    </div>--%>
        <div class="panel" id="divMsjeCompletarPerfil" runat="server">
            <div runat="server" id="divPerc" class="progress-circle wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.2s" data-duration="800" data-color="#e4e7ea,#2b9b8f"></div>
            <div class="head">
                <h4 class="wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.25s">Bienvenido a Contabilium! Ya falta muy poco para completar tu perfil.</h4>
            </div>
            <div class="content">
                <ul>
                    <li runat="server" id="liProf1" class="checked wow fadeIn animated" data-wow-duration="1s" data-wow-delay="0.2s"><a href="modulos/seguridad/mis-datos.aspx">Completa tus datos personales</a></li>
                    <li runat="server" id="liProf2" class="checked wow fadeIn animated" data-wow-duration="1s" data-wow-delay="0.4s"><a href="modulos/seguridad/mis-datos.aspx">Completa tu domicilio</a></li>
                    <li runat="server" id="liProf3" class="checked wow fadeIn animated" data-wow-duration="1s" data-wow-delay="0.6s"><a target="_blank" href="http://www.contabilium.com/ayuda/tutoriales/como-habilito-la-facturacion-electronica-y-el-punto-de-venta-online-en-contabilium">Habilita la facturación electrónica</a></li>
                </ul>
            </div>
           
            <div style="clear:both"></div>

            <%--<div class="alert alert-info fade in">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4>Hemos detectado que aún no tiene habilitado en Contabilium la facturación electrónica</h4>

                <p>
                    Puedes realizarlo siguiendo estos <a target="_blank" href="http://www.contabilium.com/ayuda/tutoriales/como-habilito-la-facturacion-electronica-y-el-punto-de-venta-online-en-contabilium/">sencillos pasos</a>
                </p>

                <p>
                    Este procedimiento habilita a Contabilium a poder comunicarse con AFIP para que usted pueda emitir Facturas Electrónicas.
                    <br />
                    Una vez realizado este procedimiento, por favor envie un mail a <b>atencioncliente@contabilium.com </b>ya que debemos Aceptar la designación, o bién comuniquese al (011) 5263-2062.
                </p>
            </div>--%>
        </div>

        <div id="divPagarPlan" runat="server">
            <div class="alert alert-info fade in">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4 style="margin-bottom: 0px;">Su plan actual es <asp:Literal ID="linombrePlan" runat="server"></asp:Literal> , y vence en <asp:Literal ID="liCantDias" runat="server"></asp:Literal>
                <a href="#" onclick="dashBoard.pagarPlanActual()"> Pagar/Renovar</a></h4>
            </div>
             <input id="hdnNombrePlanActual" runat="server" type="hidden"/>
        </div>

        <div id="divPlanPendiente" runat="server">
            <div class="alert alert-info fade in">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4>Su plan actual es <asp:Literal ID="linombrePlanPendiente" runat="server"></asp:Literal> y se encuentra pendiente de pago </h4>
            </div>
        </div>

        <div id="PanelDeControl" style="display:none">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-default" style="min-height: 183px">
                        <div class="panel-body" style="padding-top: 40px;">

                            <div class="col-xs-6 col-sm-2 col-md-2 action-icon">
                                <a href="/comprobantese.aspx" class="accesos-cc">
                                    <i class="fa fa-file-text fa-4x"></i>
                                    <span style="display: block">Nueva venta</span>
                                </a>
                            </div>

                            <div class="col-xs-6 col-sm-2 col-md-2 action-icon">
                                <a href="/modulos/compras/comprase.aspx" class="accesos-cc">
                                    <i class="fa fa-tasks fa-4x"></i>
                                    <span style="display: block">Nueva compra</span>
                                </a>
                            </div>

                            <div class="col-xs-6 col-sm-2 col-md-2 action-icon">
                                <a href="/personas.aspx?tipo=c" class="accesos-cc">
                                    <i class="fa fa-briefcase fa-4x"></i>
                                    <span style="display: block">Clientes</span>
                                </a>
                            </div>

                            <div class="col-xs-6 col-sm-2 col-md-2 action-icon">
                                <a href="/personas.aspx?tipo=p" class="accesos-cc">
                                    <i class="fa fa-users fa-4x"></i>
                                    <span style="display: block">Proveedores</span>
                                </a>
                            </div>

                            <div class="col-xs-6 col-sm-2 col-md-2 action-icon">
                                <a href="/modulos/reportes/cc.aspx" class="accesos-cc">
                                    <i class="fa fa-bar-chart-o fa-4x"></i>
                                    <span style="display: block">Cuenta corriente</span>
                                </a>
                            </div>
                           <%-- <div class="col-sm-1 col-md-1"></div>--%>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="panel panel-success panel-stat">
                        <div class="panel-heading" style="min-height: 183px">
                            <div class="stat">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="fa fa-thumbs-up" style="font-size: 50px; opacity: 1; border: none; padding: 0"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <small class="stat-label">Ventas del mes sin IVA</small>
                                        <h1 id="litIngresosMes"><%--$<asp:Literal runat="server" ID="litIngresosMes"></asp:Literal>--%></h1>
                                    </div>
                                </div>

                                <div class="mb30">
                                    <br />
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-label">Mes anterior</small>
                                        <h4 id="litIngresosMesAnterior"><%--$<asp:Literal runat="server" ID="litIngresosMesAnterior"></asp:Literal>--%></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Este año</small>
                                        <h4 id="litIngresosAnio"><%--$<asp:Literal runat="server" ID="litIngresosAnio"></asp:Literal>--%></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="panel panel-danger panel-stat">
                        <div class="panel-heading" style="min-height: 183px">
                            <div class="stat">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="glyphicon glyphicon-fire" style="font-size: 50px; opacity: 1; border: none; padding: 0"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <small class="stat-label">Pendiente de cobro</small>
                                        <h1 id="litPorCobrar"><%--$<asp:Literal runat="server" ID="litPorCobrar"></asp:Literal>--%></h1>
                                    </div>
                                </div>

                                <div class="mb30">
                                    <br />
                                </div>

                                <div class="row">
                                    <div class="col-xs-7">
                                        <small class="stat-label">Con atraso mayor a 60 días</small>
                                        <h4 id="litPorCobrarUrgente"><%--$<asp:Literal runat="server" ID="litPorCobrarUrgente"></asp:Literal>--%></h4>
                                    </div>
                                    <div class="col-xs-5 text-right">
                                        <a href="/modulos/reportes/saldosClientes.aspx" class="btn btn-sm" style="border: 1px solid #ccc; color: #fff;">Ver detalle</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-warning panel-alt widget-messaging">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ventas vs Compras - Evolución últimos 12 meses <%--<small style="font-size: 10px; color: #fff">No se incluyen las cotizaciones.</small>--%></h3>
                        </div>
                        <div class="panel-body" style="height: 346px; background-color: #fff">
                            <div id="line-chart" style="height: 300px;">
                                <div align="center" style="margin-top: 50px;"> 
                                    <%--Momentaneamente deshabilitado--%>
                                    <img src="/images/loaders/home.gif" alt="generando" />
                                    <%--<i class="fa fa-spinner faa-spin animated"></i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-info panel-alt widget-messaging">
                        <div class="panel-heading">


                            <div class="panel-btns">
                                <%--<a href="/modulos/reportes/cuentasPagar.aspx"  class="btn btn-white btn-sm pull-right">Ver todas</a>--%>
                                <a href="/modulos/reportes/cuentasPagar.aspx" class="panel-edit pull-right" style="color: #fff; opacity: inherit; font-size: 12px; font-weight: normal;">
                                    <%--<i class="fa fa-file-text"></i>--%>
                        Ver todas
                                </a>
                                <%--<a href="/modulos/reportes/cuentasPagar.aspx" class="pull-right" style="border: 1px solid #ccc;color: #fff;">Ver todas</a>--%>
                            </div>
                            <h3 class="panel-title">Cuentas a pagar (<span id="TotalComprobantes"></span>)</h3>
                        </div>
                        <div class="panel-body" style="height: 346px">
                            <ul id="ulTemplateContainer">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" id="hdnPanelDeControl" runat="server" value="0"/>
        </div>
        <script id="templateFacturasPendientes" type="text/x-jQuery-tmpl">
            {{each results}}
                <li>
                    <small class="pull-right">
                        <a onclick="dashBoard.verFacturasPendientes(${ID})" class="btn btn-white btn-sm">Ver factura</a>
                        <a onclick="dashBoard.pagarFacturasPendientes(${ID})" class="btn btn-white btn-sm">Pagar</a>
                    </small>
                    <h4 class="sender">${Fecha} - ${RazonSocial}</h4>
                    <small>${Numero} por $ ${ImporteTotalNeto}</small>
                </li>
            {{/each}}
        </script>

        <script id="NotemplateFacturasPendientes" type="text/x-jQuery-tmpl">
            <li>
                <h4 class="sender">No se han registrado cuentas a pagar</h4>
            </li>
        </script>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <%--<script src="/js/flot/jquery.flot.js"></script>
    <script src="/js/flot/jquery.flot.resize.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/flot/flot.pie.min.js"></script>
    <script src="/js/flot/jquery.flot.orderBars.min.js"></script>
    <script src="/js/flot/jquery.flot.time.js"></script>
    <script src="/js/flot/jquery.flot.categories.js"></script>--%>
    <%--<script src="js/jquery.sparkline.min.js"></script>--%>
    <script src="/js/morris.min.js"></script>
    <script src="/js/raphael-2.1.0.min.js"></script>
    <script src="/js/views/home.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script src="/js/progress-circle.js"></script>
    <%--<script src="/js/wow.js"></script>--%>

    <script type="text/javascript">
        $(".progress-circle").loading();
        //new WOW().init();
    </script>
</asp:Content>

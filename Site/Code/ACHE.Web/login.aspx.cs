﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Security.Cryptography;
using ACHE.Negocio.Common;
using System.Configuration;


public partial class login : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            /*HttpContext.Current.Session.Remove("CurrentUser");
            HttpContext.Current.Session.Remove("ASPNETPlanActual");
            HttpContext.Current.Session.Remove("ASPNETListaFormularios");
            HttpContext.Current.Session.Remove("ASPNETCobranzaCart");
            HttpContext.Current.Session.Remove("ASPNETComprobanteCart");
            HttpContext.Current.Session.Remove("ASPNETPagosCart");
            HttpContext.Current.Session.Remove("ASPNETListaRoles");*/
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();

            //LogOut
            if (!String.IsNullOrEmpty(Request.QueryString["logOut"]))
            {
                if (Request.QueryString["logOut"].Equals("true"))
                {

                    HttpCookie myCookie = new HttpCookie("ContabiliumRecordarme");
                    myCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie);

                    myCookie = new HttpCookie("ContabiliumUsuario");
                    myCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie);

                    myCookie = new HttpCookie("ContabiliumPwd");
                    myCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie);
                }
            }
            else
            {
                if (Request.Cookies["ContabiliumRecordarme"] != null)
                {
                    chkRecordarme.Checked = (Request.Cookies["ContabiliumRecordarme"].Value == "T");
                    if ((chkRecordarme.Checked) && (Request.Cookies["ContabiliumUsuario"] != null))
                    {
                        txtUsuario.Value = Server.UrlDecode(Request.Cookies["ContabiliumUsuario"].Value);
                        if (Request.Cookies["ContabiliumPwd"] != null)
                        {
                            var pass = Server.UrlDecode(Request.Cookies["ContabiliumPwd"].Value);

                            try
                            {
                                txtPwd.Value = Encriptar.DesencriptarCadena(pass).Replace("\0", "");
                                tieneDatos.Value = "1";
                            }
                            catch (Exception)
                            {
                                txtPwd.Value = "";
                            }
                        }
                    }
                    else
                    {
                        txtUsuario.Value = "";
                        txtPwd.Value = "";
                    }
                }
            }
            txtPwd.Attributes["type"] = "password";

           if (ConfigurationManager.AppSettings["Config.MarcaBlanca"] != "")
               imgLogo.Src = "/images/" + ConfigurationManager.AppSettings["Config.MarcaBlanca"] + "/logo-login.png";
        }
    }

    [WebMethod(true)]
    public static void recuperar(string email)
    {
        UsuarioCommon.RecuperarPassword(email);
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="aplicarPagosACuenta.aspx.cs" Inherits="aplicarPagosACuenta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-tasks"></i>Aplicar pagos a cuenta <span>Administración</span></h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li class="active">Compras</li>
            </ol>
        </div>
    </div>
    <div id="divConDatos" runat="server">
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12 col-md-12 table-results">
                    <div class="alert alert-warning mb15" id="divAlert" runat="server" visible="false">
		                <asp:Literal runat="server" ID="litMsj"></asp:Literal>
	                </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <form runat="server" id="frmSearch">
                                <input type="hidden" id="hdnPage" runat="server" value="1" />
                                <div class="col-sm-12" style="padding-left:inherit">
                                    <div class="col-sm-5 col-md-6">
                                           <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                            <option value=""></option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </form>

                            <div class="col-sm-12"><hr /></div>

                            <div class="row">
                                <div class="pull-right">
                                   
                                    <div class="btn-group mr10" id="divPagination" style="display: none">
                                        <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="AplicarPagosACuenta.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                        <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="AplicarPagosACuenta.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                    </div>
                                </div>


                                <h4 class="panel-title" style="clear:left;padding-left:20px">Resultados</h4>
                                <p id="msjResultados" style="padding-left:20px"></p>

                            </div>
                        </div>
                        <!-- panel-heading -->
                        <div class="panel-body">

                            <div class="alert alert-danger" id="divError" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Lo sentimos!</strong> <span id="msgError"></span>
                            </div>
                            
                                  <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                           <div class="table-responsive">
                                <table class="table mb30">
                                    <thead>
                                        <tr>
                                            <!--th>#</!--th-->
                                            <th>Fecha</th>
                                            <th>Proveedor/Cliente</th>
                                            <!--th>Tipo</th-->
                                            <!-- <th>Nro Factura</th>-->
                                            <th>Imp. Neto Grav.</th>
                                            <th>Iva</th>
                                            <th>No Grav.</th>
                                            <th>Ret</th>
                                            <th>Total</th>
                                            <th class="columnIcons"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="resultsContainer">
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        
                         <div class="panel-footer" runat="server" id="divFooter">
                            <a class="btn btn-success" onclick="AplicarPagosACuenta.aplicar();" id="lnkAceptar">Aplicar</a>
                            <a href="#" onclick="AplicarPagosACuenta.cancelar();" style="margin-left: 20px">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script id="resultTemplate" type="text/x-jQuery-tmpl">
            {{each results}}
        <tr>           
            <td>${Fecha}</td>
            <td>${RazonSocial}</td>          
            <td>${ImporteNeto}</td>
            <td>${Iva}</td>
            <td>${NoGravado}</td>
            <td>${Retenciones}</td>
            <td>${Total}</td>
            <td >
                
                 <select class="form-control input-sm chosen-select ddlComprobantes" id="ddlPagos${ID}" >
                    <option value="0" >Mantener a cuenta</option>
                 </select>
                    
            </td>
        </tr>
            {{/each}}
        </script>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/AplicarPagosACuenta.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            if ($('#divConDatos').is(":visible")) {
                AplicarPagosACuenta.configFilters();
            }
        });
    </script>
</asp:Content>

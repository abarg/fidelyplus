﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="personase.aspx.cs" Inherits="personase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .modal.modal-wide .modal-dialog {
            width: 90%;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2>
            <asp:Literal runat="server" ID="litTitulo"></asp:Literal>
        </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li>
                    <asp:Literal runat="server" ID="litPathPadre"></asp:Literal></li>
                <li class="active">
                    <asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="row hide" id="divStats">
                            <div class="col-sm-3 col-md-3">
                                <div class="panel panel-success panel-stat">
                                    <div class="panel-heading">

                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="fa fa-thumbs-up" style="font-size: 30px; opacity: 1; border: none; padding: 0"></i>
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label">Importe facturado</small>
                                                    <h4>$
                                                        <asp:Literal runat="server" ID="litImporteFacturado"></asp:Literal></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div class="panel panel-info panel-stat">
                                    <div class="panel-heading">
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="fa fa-thumbs-down" style="font-size: 30px; opacity: 1; border: none; padding: 0"></i>
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label">Importe pagado</small>
                                                    <h4>$
                                                        <asp:Literal runat="server" ID="litImportePagado"></asp:Literal></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div class="panel panel-danger panel-stat">
                                    <div class="panel-heading">
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="glyphicon glyphicon-fire" style="font-size: 30px; opacity: 1; border: none; padding: 0"></i>
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label">Saldo pendiente</small>
                                                    <h4>$
                                                        <asp:Literal runat="server" ID="litSaldoPendiente"></asp:Literal></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 col-md-3"></div>
                        </div>

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                        <div class="row mb15 hide">

                            <div class="col-sm-6 hide">
                                <img id="imgFoto" src="/files/usuarios/no-cheque.png" class="thumbnail img-responsive" alt="" runat="server" />
                                <div class="mb30"></div>

                                <div id="divLogo" style="display: none">
                                    <%--<p class="mb30">Formato JPG, PNG o GIF. Tamaño máximo recomendado: 100x70px</p>--%>
                                    <input type="hidden" value="" name="" /><input type="file" id="flpArchivo" />
                                    <div class="mb20"></div>
                                </div>

                                <div class="col-sm-6" id="divAdjuntarFoto">
                                    <a class="btn btn-white btn-block" onclick="fotos.showInputFoto();">Adjuntar foto</a>
                                </div>
                                <div class="col-sm-6" id="divEliminarFoto">
                                    <a class="btn btn-white btn-block" onclick="fotos.eliminarFoto();">Eliminar foto</a>
                                </div>
                            </div>
                        </div>
                       
                        <%-- Datos principales--%>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Datos principales </h3>
                                <label class="control-label">Estos son los datos básicos que el sistema necesita para poder realizar operaciones con el <asp:Literal ID="liDatosGenerales" runat="server" ></asp:Literal>.</label>
                            </div>
                        </div>
                       
                        <div class="row mb15">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Razón Social</label>
                                    <asp:TextBox runat="server" ID="txtRazonSocial" CssClass="form-control  required" MaxLength="128" placeholder="Pepas SRL o José Suárez" onkeyup="remplazarCaracteresEspeciales2('txtRazonSocial');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15" id="divFantasia">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Nombre de Fantansía</label>
                                    <asp:TextBox runat="server" ID="txtNombreFantasia" CssClass="form-control" MaxLength="128" placeholder="Ej: Pepas"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Código</label>
                                    <asp:TextBox runat="server" ID="txtCodigo" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Categoría Impositiva</label>
                                    <asp:DropDownList runat="server" ID="ddlCondicionIva" CssClass="form-control required" onchange="changeCondicionIva();">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Responsable Inscripto" Value="RI"></asp:ListItem>
                                        <asp:ListItem Text="Consumidor Final" Value="CF"></asp:ListItem>
                                        <asp:ListItem Text="Monotributista" Value="MO"></asp:ListItem>
                                        <asp:ListItem Text="Exento" Value="EX"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3" id="divPersoneria">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Personería</label>
                                    <asp:DropDownList runat="server" ID="ddlPersoneria" CssClass="form-control">
                                        <asp:ListItem Text="Física" Value="F"></asp:ListItem>
                                        <asp:ListItem Text="Jurídica" Value="J"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div id="divNumero">
                             <div class="row">
                                <div class="col-sm-6">
                                    <label class="control-label"><span class="asterisk" id="spIdentificacionObligatoria">*</span> Indicanos como identificarlo</label>
                                </div>
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <asp:DropDownList runat="server" ID="ddlTipoDoc" CssClass="form-control required">
                                            <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="DNI" Value="DNI"></asp:ListItem>
                                            <asp:ListItem Text="CUIT/CUIL" Value="CUIT"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <asp:TextBox runat="server" ID="txtNroDocumento" CssClass="form-control required number validCuit" MaxLength="13" placeholder="Número"></asp:TextBox>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control email" MaxLength="128" placeholder="ejemplo@ejemplo.com"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Email (para poder enviarle la factura electrónica, recibo o avisos de vencimiento)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <asp:TextBox runat="server" ID="txtEmailFc" CssClass="form-control multiemails" MaxLength="128" placeholder="ejemplo@ejemplo.com"></asp:TextBox>
                                    </div>
                                    <small>Si deseas ingresar más de 1 dirección, separa las direcciones mediante una coma.</small>
                                </div>
                            </div>
                        </div>

                        <%-- Datos complementarios--%>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <hr />
                                <h3>Datos complementarios </h3>
                                <label class="control-label">Estos datos no son necesarios para facturar, pero podrían serte de utilidad ;-)</label>
                            </div>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Teléfono</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                        <asp:TextBox runat="server" ID="txtTelefono" TextMode="Phone" CssClass="form-control" MaxLength="50" placeholder="Fijo y/o Móvil"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Apodo de mercado libre</label>
                                    <asp:TextBox runat="server" ID="txtApodoML" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15" id="divDomicilio">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Domicilio</label>
                                    <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control" MaxLength="100" placeholder=" Ej: Av. Corrientes"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Piso/Depto</label>
                                    <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10" placeholder=" Ej: 9B"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Código Postal</label>
                                    <asp:TextBox runat="server" ID="txtCp" CssClass="form-control" MaxLength="10" placeholder=" Ej: 1414"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Seleccioná el país, provincia y localidad o barrio (para CABA)</label>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <select class="select2 required" data-placeholder="Selecciona un país..." id="ddlPais" onchange="changePais();">
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <select class="select2 required" data-placeholder="Selecciona una provincia..." id="ddlProvincia" onchange="changeProvincia();">
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <asp:DropDownList runat="server" ID="ddlCiudad" CssClass="select2" data-placeholder="Selecciona una Ciudad/Localidad...">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Observaciones</label>
                                    <asp:TextBox runat="server" TextMode="MultiLine" Rows="5" ID="txtObservaciones" CssClass="form-control" placeholder="Ej: Datos bancarios, tipo de comprobante que necesita el cliente, nombres de contactos,etc."></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Saldo inicial</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox runat="server" ID="txtSaldo" CssClass="form-control" MaxLength="10" placeholder="Saldo inicial"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3" id="divListaClientes">
                                <div class="form-group">
                                    <label class="control-label">Lista de precios</label>
                                    <asp:DropDownList runat="server" ID="ddlListaPrecios" CssClass="form-control">
                                        <asp:ListItem Text="Default" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15" runat="server" id="divUsuarioVendedor">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Pwd acceso clientes</label>
                                    <asp:TextBox runat="server" ID="txtPwd" Enabled="false" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Vendedor asignado</label>
                                    <asp:DropDownList runat="server" ID="ddlIDUsuarioAdicional" CssClass="select2" data-placeholder="Selecciona un usuario adicional...">
                                    </asp:DropDownList>
                                </div>
                            </div>
                       </div>

                        <div class="row mb15">
                            <div class="col-sm-6 hide">
                                <h5 class="control-label">Ubicación de Contacto</h5>
                                <div id="gmap-marker" style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-success" id="btnActualizar" onclick="grabar();"> <asp:Literal ID="libtnGuardar" runat="server"></asp:Literal></a>
                        <a href="#" onclick="cancelar();" style="margin-left:20px">No quiero guardarlo</a>

                        <div class="btn-group dropup" style="margin-bottom: 0; float:right" id="btnAcciones">
                            <button type="button" class="btn btn-warning">Más opciones</button>
                            <button class="btn btn-warning dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:CambiarReporte()">Ver cuenta corriente</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <asp:HiddenField runat="server" ID="hdnTieneFoto" Value="0" />
                <asp:HiddenField runat="server" ID="hdnSinCombioDeFoto" Value="0" />
                <asp:HiddenField runat="server" ID="hdnFileName" Value="" />

                <asp:HiddenField runat="server" ID="hdnDireccion" Value="" />
                <asp:HiddenField runat="server" ID="hdnPais" Value="10" />
                <asp:HiddenField runat="server" ID="hdnProvincia" Value="2" />
                <asp:HiddenField runat="server" ID="hdnCiudad" Value="0" />

                <asp:HiddenField runat="server" ID="hdnIDListaPrecio" Value="0" />
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDPersona" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTipo" />
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="titDetalle">Detalle de cuenta corriente</h4>
                    <%--style="display: inline-table;"--%>
                </div>
                <div class="modal-body">
                    <div class="pull-left">   
                        <input type="radio" name="rCtaCte" id="rCtaCteCliente" value="1" onchange="CambiarReporte();" />
                        <label>Ver como Cliente</label>
                        <input type="radio" name="rCtaCte" id="rCtaCteProv" value="0" onchange="CambiarReporte();" />
                        <label>Ver como Proveedor</label>
                    </div>
                    <div class="pull-right" id="divExportar">   
                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoadingModal" style="display:none" />
                        <a href="" id="lnkDownloadModal" onclick="ExportarCC.resetearExportacion();" download="CuentaCorriente" style="display:none">Descargar</a>
                        <div class="btn-group mr10  dropdown" id="divExportarModal" runat="server">
                            <button type="button" class="btn btn-btn-default"><i class="fa fa-tasks"></i> Exportar</button>
                            <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                            <ul class="dropdown-menu" style="min-width: 130px;">
                                <li><a href="javascript:ExportarCC.exportar();" >A Excel</a></li>
                                <li><a href="javascript:ExportarCC.exportarPDF();">A PDF</a></li>      
                            </ul>
                        </div>  
                        <input hidden id="hdnTipoPersonaExportar" value="0" />
                    </div>
                    <div class="table-responsive">
                        <table class="table table-success mb30 smallTable">
                            <thead>
                                <tr>
                                    <th>Comprobante</th>
                                    <th>Fecha</th>
                                    <th>Comprobante aplicado</th>
                                    <th>Fecha Cobro</th>
                                    <th>Importe</th>
                                    <th>Cobrado</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody id="bodyDetalle">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left:20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplateDetalle" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            {{if $value.Cobrado == "SaldoAnterior"}}
                <td class="bgTotal text-danger">
                    Saldo a la fecha
                </td>
                <td class="bgTotal" colspan="5">&nbsp;</td>
                <td class="bgTotal text-danger">
                    ${Total}
                </td>
            {{else}}

                {{if $value.Cobrado != "Saldo"}}//Si no es la ultima columna, muestro todo normal. Solo por cuestiones estéticas.
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Comprobante}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Fecha}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${ComprobanteAplicado}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${FechaCobro}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Importe}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Cobrado}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Total}
                    </td>
                {{else}}
                    <td class="bgTotal" colspan="5">&nbsp;</td>
                    <td class="bgTotal text-danger">
                        ${Cobrado}
                    </td>
                    <td class="bgTotal text-danger">
                        <span id="spnTotal">${Total}</span>
                    </td>
                {{/if}}
            {{/if}}
        </tr>
        {{/each}}
    </script>

    <script id="noResultTemplateDetalle" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="7">No se han encontrado resultados
            </td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/personas.js?v=1.1"></script>
    <script src="/js/jquery.fileupload.js" type="text/javascript"></script>
    
   <<script>
        jQuery(document).ready(function () {
            configForm();
        });
    </script>

</asp:Content>

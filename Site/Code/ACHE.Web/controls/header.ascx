﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header.ascx.cs" Inherits="controls_header" %>

<div class="headerbar">
    <div class="header-left">
        <div class="headerbar">
            <div class="header-left">
                <div class="cd-main-header fixed">
                    <div class="top-nav">
                        <nav class="">
                            <ul class="cd-primary-nav">
                                <!-- empresas -->
                                <li class="has-children dropdown-settings dropdown-default open-on-hover" runat="server" id="divUsuariosMultiempresas">
                                    <a href="#">
                                        <span>Empresa actual: <strong><asp:Literal ID="litEmpresaActual" runat="server"></asp:Literal></strong></span> 
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                        <li class="go-back"><a href="#0">Volver</a></li>
                                        <li class="title">
                                            <h5>Seleccioná una empresa</h5>
                                        </li>
                                        <asp:Literal ID="liEmpresasHeader" runat="server"></asp:Literal>
                                    </ul>
                                </li>
                                
                                <!-- cuenta -->
                                <li class="has-children dropdown-settings dropdown-default open-on-hover" style="margin-left:10px">
                                    <a href="#"><span class="">Mi cuenta</span> <span class="caret"></span></a>
                                    <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                        <li class="go-back"><a href="#0">Volver</a></li>
                                        <li class="title">
                                            <h5>Mi cuenta</h5>
                                        </li>
                                        <li runat="server" id="liEmpresa"><a href="/modulos/seguridad/mis-datos.aspx"><i class="fa fa-user"></i> Datos de mi empresa</span></a></li>
                                        <li><a href="/modulos/seguridad/cambiar-pwd.aspx"><i class="fa fa-key"></i> Cambiar contraseña</span></a></li>
                                        <li runat="server" id="liUsuarios"><a href="/modulos/seguridad/usuarios.aspx"><i class="fa fa-users"></i> Administrar usuarios</span></a></li>
                                        <li runat="server" id="liPagar"><a href="/modulos/seguridad/elegir-plan.aspx"><i class="fa fa-comment"></i> Pagar mi plan</span></a></li>
                                        <li><a href="/login.aspx?logOut=true"><i class="fa fa-sign-out"></i> Cerrar sesión</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="nav-content">
                        <div class="cd-nav-header">
                            <a href="/home.aspx" class="cd-logo"><img src="/images/logo-inside.png" alt="logoContabilium" runat="server" id="imgLogo" style="height:40px;width: auto;"></a>
                            <ul class="cd-header-buttons">
                                <li><a class="cd-search-trigger" href="#cd-search">Buscar...<span></span></a></li>
                                <li><a class="cd-nav-trigger" href="#cd-primary-nav">Menu<span></span></a></li>
                            </ul>
                            <!-- cd-header-buttons -->
                        </div>
                        <div class="cd-menu">
                            <nav class="cd-nav">
                                <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
                                    <li class="has-children open-on-hover" id="liVentas" runat="server">

                                        <a href="#"><i class="fa fa-file-text"></i>Ventas <span class="caret"></span></a>
                                        <ul class="cd-secondary-nav is-hidden">
                                            <li class="go-back"><a href="#0">Menú</a></li>
                                            <li><a id="liVentasComprobantes" data-formulario="comprobantes" runat="server" href="/comprobantes.aspx"><span>Facturación</span></a></li>
                                            <li><a id="liVentasCobranzas" data-formulario="cobranzas" runat="server" href="/cobranzas.aspx"><span>Cobranzas</span></a></li>
                                            <li><a id="liVentasPresupuestos" data-formulario="presupuestos" runat="server" href="/presupuestos.aspx"><span>Presupuestos</span></a></li>
                                            <li><a id="liVentasAbonos" data-formulario="Abonos" runat="server" href="/Abonos.aspx"><span>Abonos</span></a></li>
                                            <li><a id="liVentasProductosyServicios" data-formulario="conceptos" runat="server" href="/conceptos.aspx"><span>Productos y servicios</span></a></li>
                                            <li><a id="liVentasClientes" data-formulario="personas" runat="server" href="/personas.aspx?tipo=c"><span>Clientes</span></a></li>
										    
                                        </ul>
                                    </li>

                                    <li class="has-children open-on-hover" id="liCompras" runat="server">
                                        <a href="#"><i class="fa fa-tasks"></i>Compras <span class="caret"></span></a>
                                        <ul class="cd-secondary-nav is-hidden">
                                            <li class="go-back"><a href="#0">Menú</a></li>
                                            <li><a id="liComprasComprobantes" data-formulario="compras" runat="server" href="/modulos/compras/compras.aspx"><span>Comprobantes</span></a></li>
                                            <li><a id="liComprasPagos" data-formulario="pagos" runat="server" href="/modulos/compras/pagos.aspx"><span>Pagos</span></a></li>
                                            <li><a id="liComprasProveedores" data-formulario="personas" runat="server" href="/personas.aspx?tipo=p"><span>Proveedores</span></a></li>
                                           
                                        </ul>
                                    </li>
                                    <li class="has-children open-on-hover" id="litesoreria" runat="server">
                                        <a href="#"><i class="fa fa-money"></i>Tesorería <span class="caret"></span></a>
                                        <ul class="cd-secondary-nav is-hidden">
                                            <li class="go-back"><a href="#0">Menú</a></li>
                                            <li><a id="liTesoreriaBancos" data-formulario="bancos" runat="server" href="/modulos/tesoreria/bancos.aspx"><span>Bancos</span></a></li>
                                            <li><a id="liTesoreriaCaja" data-formulario="caja" runat="server" href="/modulos/tesoreria/caja.aspx"><span>Cajas</span></a></li>
                                            <li><a id="liTesoreriaCheques" data-formulario="cheques" runat="server" href="/modulos/tesoreria/cheques.aspx"><span>Cheques</span></a></li>
                                            <li><a id="liTesoreriaDetalleBancario" data-formulario="DetalleBancario" runat="server" href="/modulos/reportes/DetalleBancario.aspx">Movimientos bancarios</a></li>
                                            <li><a id="liTesoreriaMovimientosFondos" data-formulario="MovimientoDeFondos" runat="server" href="/modulos/tesoreria/MovimientoDeFondos.aspx"><span>Movimiento de fondos</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="has-children open-on-hover" id="liInventarios" runat="server">
                                        <a href="#"><i class="fa fa-archive"></i>Inventarios <span class="caret"></span></a>
                                        <ul class="cd-secondary-nav is-hidden">
                                            <li class="go-back"><a href="#0">Menú</a></li>
                                            <li><a id="liModificacionStock" runat="server" data-formulario="modificacionStock" href="/modulos/inventarios/modificacionStock.aspx"><span>Modificación de stock</span></a></li>
                                            <li><a id="liMovimientosDepositos" runat="server" data-formulario="movimientosDeStock" href="/modulos/inventarios/movimientosDeStock.aspx"><span>Movimientos entre depósitos</span></a></li>
                                            <li><a id="liImportacionMasivaProductos" runat="server" data-formulario="importar" href="/importar.aspx?tipo=Productos"><span>Importación masiva de productos</span></a></li>
                                            <li><a id="liActualizacionMasivaStock" runat="server" data-formulario="importar" href="/importar.aspx?tipo=Stock"><span>Actualización masiva de stock</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="has-children open-on-hover" id="liContabilidad" runat="server">
                                        <a href="#"><i class="fa fa-money"></i>Contabilidad <span class="caret"></span></a>
                                        <ul class="cd-secondary-nav is-hidden">
                                            <li class="go-back"><a href="#0">Menú</a></li>
                                            <li><a id="liContabilidadPlanDeCuentas" data-formulario="plandecuentas" runat="server" href="/modulos/contabilidad/plandecuentas.aspx"><span>Plan de cuentas</span></a></li>
                                            <li><a id="liContabilidadLibroDiario" data-formulario="LibroDiario" runat="server" href="/modulos/reportes/LibroDiario.aspx"><span>Libro diario</span></a></li>
                                            <li><a id="liContabilidadLibroMayor" data-formulario="LibroMayor" runat="server" href="/modulos/reportes/LibroMayor.aspx"><span>Libro mayor</span></a></li>
                                            <li><a id="liContabilidadBalanceGeneral" data-formulario="BalanceGeneral" runat="server" href="/modulos/reportes/BalanceGeneral.aspx"><span>Balance general</span></a></li>
                                            <li><a id="liContabilidadAsientosManuales" data-formulario="asientosManuales" runat="server" href="/modulos/contabilidad/asientosManuales.aspx"><span>Asientos manuales</span></a></li>
                                            <li><a id="liContabilidadEstadoResultado" data-formulario="EstadoResultado" runat="server" href="/modulos/reportes/EstadoResultado.aspx"><span>Estado de resultado</span></a></li>
                                            
                                        </ul>
                                    </li>
                                    <li class="has-children col-x-4 open-on-hover" id="liReportes" runat="server">
                                        <a href="#"><i class="fa fa-bar-chart-o"></i>Reportes <span class="caret"></span></a>
                                        <ul class="cd-secondary-nav is-hidden">
                                            <li class="go-back"><a href="#0">Menu</a></li>
                                            <li class="has-children" runat="server" id="liFinancierosEconomicos">
                                                <a href="#" class="title"><h5>Financieros y Económicos</h5></a>
                                                <ul class="is-hidden">
                                                    <li class="go-back"><a href="#0">Reportes</a></li>
                                                    <li><a id="liReportesVentasVsCompras" data-formulario="ventas-vs-compras" runat="server" href="/modulos/reportes/ventas-vs-compras.aspx">Ventas vs Compras</a></li>
                                                    <li><a id="liReportesCobradoVsPagado" data-formulario="cobrado-vs-pagado" runat="server" href="/modulos/reportes/cobrado-vs-pagado.aspx">Cobrado vs Pagado</a></li>
                                                    <li><a id="liReportesVentasVsPagado" data-formulario="ventas-vs-pagado" runat="server" href="/modulos/reportes/ventas-vs-pagado.aspx">Ventas vs Pagado</a></li>
                                                    <li><a id="liReportesComprasPorCategoria" data-formulario="compras-por-categoria" runat="server" href="/modulos/reportes/compras-por-categoria.aspx">Compras por categoría</a></li>
                                                    <li><a id="liReportesComprasPorCategoriaMensual" data-formulario="compras-por-categoria-mensual" runat="server" href="/modulos/reportes/compras-por-categoria-mensual.aspx">Compras por categorias mensuales</a></li>
                                                    
                                                </ul>
                                            </li>
                                            <li class="has-children" runat="server" id="liImpositivos">
                                                <a href="#" class="title"><h5>Impositivos</h5></a>
                                                <ul class="is-hidden">
                                                    <li class="go-back"><a href="#0">Reportes</a></li>
                                                    <li runat="server" id="liIva1" data-formulario="iva-ventas"><a href="/modulos/reportes/iva-ventas.aspx">IVA Ventas</a></li>
                                                    <li runat="server" id="liIva2" data-formulario="iva-compras"><a href="/modulos/reportes/iva-compras.aspx">IVA Compras</a></li>
                                                    <li runat="server" id="liIva3" data-formulario="iva-saldo"><a href="/modulos/reportes/iva-saldo.aspx">IVA Saldo</a></li>
                                                    <li><a id="liReportesRetenciones" data-formulario="retenciones" runat="server" href="/modulos/reportes/retenciones.aspx">Retenciones</a></li>
                                                    <li><a id="liReportesPercepciones" data-formulario="percepciones" runat="server" href="/modulos/reportes/percepciones.aspx">Percepciones</a></li>
                                                    <li><a id="liReportesCitiVentas" data-formulario="citiVentas" runat="server" href="/modulos/reportes/citiVentas.aspx">C.I.T.I: compras y ventas</a></li>
                                                    <li><a id="liReportesSicore" data-formulario="SicoreRetEmitidas" runat="server" href="/modulos/reportes/SicoreRetEmitidas.aspx">Sicore y e-Arciba</a></li>
                                                </ul>
                                            </li>
                                            <li class="has-children" runat="server" id="liProductos">
                                                <a href="#" class="title"><h5>Productos</h5></a>
                                                <ul class="is-hidden">
                                                    <li><a id="liReportesStock" data-formulario="stock" runat="server" href="/modulos/reportes/stock.aspx">Stock</a></li>
                                                    <li><a id="liReportesHistoricoStock" data-formulario="historicoStock" runat="server" href="/modulos/reportes/historicoStock.aspx">Entradas y salidas</a></li>
                                                    <li><a id="liReportesRankingPorProducto" data-formulario="rnk-conceptos" runat="server" href="/modulos/reportes/rnk-conceptos.aspx">Ranking</a></li>
                                                    <li><a id="liReportesProductosPorCliente" data-formulario="productosPorCliente" runat="server" href="/modulos/reportes/productosPorCliente.aspx">Ventas por cliente</a></li>
                                                </ul>
                                            </li>
                                            <li class="has-children" runat="server" id="liGestion">
                                                <a href="#" class="title"><h5>Gestión</h5></a>
                                                <ul class="is-hidden">
                                                    <li class="go-back"><a href="#0">Reportes</a></li>
                                                    <li><a id="liReportesCuentaCorriente" data-formulario="cc" runat="server" href="/modulos/reportes/cc.aspx">Cuenta corriente</a></li>
                                                    <li><a id="liReportesSaldosClientes" data-formulario="saldosClientes" runat="server" href="/modulos/reportes/saldosClientes.aspx">Saldos de clientes</a></li>
                                                    <li><a id="liReportesCobranzasPendientes" data-formulario="cobranzasPendientes" runat="server" href="/modulos/reportes/cobranzasPendientes.aspx">Cobranzas pendientes</a></li>
                                                    <li><a id="liReportesSaldosProveedores" data-formulario="saldosProveedores" runat="server" href="/modulos/reportes/saldosProveedores.aspx">Saldos de proveedores</a></li>
                                                    <li><a id="liReportesCuentasAPagar" data-formulario="cuentasPagar" runat="server" href="/modulos/reportes/cuentasPagar.aspx">Cuentas a pagar</a></li>
                                                    <li><a id="liReportesPagoAProveedores" data-formulario="pagoprov" runat="server" href="/modulos/reportes/pagoprov.aspx">Pagos a proveedores</a></li>
                                                    <li><a id="liReportesRankingPorCliente" data-formulario="rnk-clientes" runat="server" href="/modulos/reportes/rnk-clientes.aspx">Ranking por cliente</a></li>
                                                    <li><a id="liReportesComprasPorCliente" data-formulario="comprasPorCliente" runat="server" href="/modulos/reportes/comprasPorCliente.aspx">Compras por cliente</a></li>
                                                    <li><a id="liReportesTrackingHoras" data-formulario="trackingHora" runat="server" href="/modulos/reportes/trackingHora.aspx">Tracking horas</a></li>
                                                    
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <!-- mobile only -->
                                    <!-- cuenta -->
                                    <!-- herramientas -->
                                    <li class="has-children dropdown-settings dropdown-default mobile-only" id="liHerramientas" runat="server">
                                        <a href="#" data-placement="bottom" data-original-title="Herramientas" class="tooltips"><i class="glyphicon glyphicon-cog"></i><span class="link-text">Herramientas</span></a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li><a id="liHerramientasExploradorArchivos" data-formulario="file-explorer" runat="server" href="/file-explorer.aspx">Explorador de archivos</a></li>
                                            <li class="has-children">
                                                <a href="#0">Importación masiva <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                <ul class="is-hidden">
                                                    <li class="go-back"><a href="#0"><i class="fa fa-caret-left" aria-hidden="true"></i> Volver</a></li>
                                                    <li><a id="liHerramientasImportarClientes" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Clientes">Importación masiva de clientes</a></li>
                                                    <li><a id="liHerramientasImportarProveedores" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Proveedores">Importación masiva de proveedores</a></li>
                                                    <li><a id="liHerramientasImportarProductos" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Productos">Importación masiva de productos</a></li>
                                                    <li><a id="liHerramientasImportarServicios" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Servicios">Importación masiva de servicios</a></li>
                                                    <li><a id="liHerramientasImportarFacturas" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Facturas">Importación masiva de facturas de venta simple</a></li>
                                                    <li><a id="liHerramientasImportarFacturasCompras" data-formulario="importar" runat="server" href="/importar.aspx?tipo=FacturasCompras">Importación masiva de facturas de compras</a></li>
                                                    <li runat="server" id="liImportarPlanDeCuentas" data-formulario="importar"><a href="/importar.aspx?tipo=PlanDeCuentas">Importación masiva de cuentas contables</a></li>
                                                </ul>
                                            </li>
                                            <li><a id="liHerramientasTrackingHoras" data-formulario="trackingHoras" runat="server" href="/modulos/ventas/trackingHoras.aspx">Tracking de horas</a></li>
                                            <li><a id="liHerramientasAlertas" data-formulario="alertas" runat="server" href="/alertas.aspx">Configurar alertas</a></li>
                                           
                                        </ul>
                                    </li>
                                    <li class="has-children dropdown-settings dropdown-default mobile-only">
                                        <a href="#" data-placement="bottom" data-original-title="Mi cuenta" class="tooltips"><i class="fa fa-user"></i><span class="link-text">Mi cuenta</span></a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li class="title">
                                                <h5>Mi cuenta</h5>
                                            </li>
                                            <li runat="server" id="liEmpresa2"><a href="/modulos/seguridad/mis-datos.aspx">Datos de mi empresa</span></a></li>
                                            <li><a href="/modulos/seguridad/cambiar-pwd.aspx">Cambiar contraseña</span></a></li>
                                            <li runat="server" id="liUsuarios2"><a href="/modulos/seguridad/usuarios.aspx">Administrar usuarios</span></a></li>
                                            <li runat="server" id="liPagar2"><a href="/modulos/seguridad/elegir-plan.aspx">Pagar mi plan</span></a></li>
                                            <li><a href="/login.aspx?logOut=true">Cerrar sesión</a></li>
                                        </ul>
                                    </li>
                                    <!-- empresa -->
                                    <li class="has-children dropdown-settings dropdown-default mobile-only" id="divUsuariosMultiempresas2" runat="server">
                                        <a href="#" data-placement="bottom" data-original-title="Mi cuenta" class="tooltips">
                                            <i class="fa fa-industry"></i><span class="link-text">Empresa actual:</span> 
                                            <strong><asp:Literal ID="litEmpresaActual2" runat="server"></asp:Literal></strong>
                                        </a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li class="title">
                                                <h5>Mi cuenta</h5>
                                            </li>
                                            <asp:Literal ID="liEmpresasHeader2" runat="server"></asp:Literal>
                                        </ul>
                                    </li>
                                    
                                    <!-- ayuda -->
                                    <li class="has-children dropdown-settings dropdown-default mobile-only">
                                        <a href="#" data-placement="bottom" data-original-title="Ayuda" class="tooltips"><i class="fa fa-question-circle"></i><span class="link-text">Ayuda</span></a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li class="title">
                                                <h5>Centro de ayuda</h5>
                                            </li>
                                            <%--<li><a href="/modulos/seguridad/referidos.aspx">Recomendá Contabilium y ganá $$<span class="subinfo">No te pierdas la oportunidad</span></a></li>--%>
                                            <li><a href="http://www.contabilium.com/ayuda/" target="_blank">Capacitación y tutoriales <span class="subinfo">Accedé a un listado de tutoriales donde podrás aprender cómo usar las funcionalidades del sistema</span></a></li>
                                            <li><a href="http://www.contabilium.com/ayuda/faqs/" target="_blank">Preguntas frecuentes <span class="subinfo">Despejá todas tus dudas. Qué estás esperando?</span></a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#modalAyuda">Soporte <span class="subinfo">Escribinos y te responderá un especialista.</span></a></li>
                                        </ul>
                                    </li>
                                    <!-- mensajes -->
                                    <li class="has-children dropdown-settings dropdown-default mobile-only">
                                        <a href="#" data-placement="bottom" data-original-title="Mensajes" class="tooltips"><i class="fa fa-envelope"></i><span class="link-text">Mensajes</span> <span class="notify"><asp:Literal runat="server" ID="litMsjCant"></asp:Literal></span></a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li class="title">
                                                <h5>Mensajes</h5>
                                            </li>
                                            <li runat="server" id="liSinMsj">
                                                <div class="no-msg">No hay mensajes nuevos</div>
                                            </li>
                                            <li runat="server" id="liFE">
                                                <a href="#" data-toggle="modal" data-target="#modalAyuda">
                                                    Facturación electrónica <span class="subinfo">Aún no configuraste tu cuenta para poder emitir comprobantes de forma electrónica
                                                </a>
                                            </li>
                                            <asp:Literal runat="server" ID="liPagarPlan"></asp:Literal>
                                            <asp:Literal runat="server" ID="litAlertas"></asp:Literal>
                                        </ul>
                                    </li>

                                </ul>
                                <!-- primary-nav -->
                                <ul id="cd-secondary-nav" class="cd-primary-nav is-fixed">
                                    <!-- herramientas -->
                                    <li class="has-children dropdown-settings button-config" id="liHerramientas2" runat="server">
                                        <a href="#" data-placement="bottom" data-original-title="Herramientas" class="tooltips"><i class="glyphicon glyphicon-cog"></i><span class="link-text">Herramientas</span></a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li><a id="liHerramientasExploradorArchivos2" data-formulario="file-explorer" runat="server" href="/file-explorer.aspx">Explorador de archivos</a></li>
                                            <li class="has-children">
                                                <a href="#0">Importación masiva <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                <ul class="is-hidden">
                                                    <li class="go-back"><a href="#0"><i class="fa fa-caret-left" aria-hidden="true"></i> Volver</a></li>
                                                    <li><a id="liHerramientasImportarClientes2" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Clientes">Importación masiva de clientes</a></li>
                                                    <li><a id="liHerramientasImportarProveedores2" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Proveedores">Importación masiva de proveedores</a></li>
                                                    <li><a id="liHerramientasImportarProductos2" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Productos">Importación masiva de productos</a></li>
                                                    <li><a id="liHerramientasImportarServicios2" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Servicios">Importación masiva de servicios</a></li>
                                                    <li><a id="liHerramientasImportarFacturas2" data-formulario="importar" runat="server" href="/importar.aspx?tipo=Facturas">Importación masiva de facturas de venta simple</a></li>
                                                    <li><a id="liHerramientasImportarComprovantes2" data-formulario="importar" runat="server" href="/importar.aspx?tipo=FacturasDetalle">Importación masiva de facturas de venta con items</a></li>
                                                    <li><a id="liHerramientasImportarFacturasCompras2" data-formulario="importar" runat="server" href="/importar.aspx?tipo=FacturasCompras">Importación masiva de facturas de compras</a></li>
                                                    <li runat="server" id="liImportarPlanDeCuentas2" data-formulario="importar"><a href="/importar.aspx?tipo=PlanDeCuentas">Importación masiva de cuentas contables</a></li>
                                                </ul>
                                            </li>
                                            <li><a id="liHerramientasTrackingHoras2" data-formulario="trackingHoras" runat="server" href="/modulos/ventas/trackingHoras.aspx">Tracking de horas</a></li>
                                            <li><a id="liHerramientasAlertas2" data-formulario="alertas" runat="server" href="/alertas.aspx">Configurar alertas</a></li>
                                            
                                        </ul>
                                    </li>
                                    <!-- search li -->
                                    <li class="search-desktop">
                                        <!-- buscador -->
                                        <div class="search-input">
                                            <div class="input-group">
                                                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                                                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                                                <input type="text" id="autocompleteHeader" class="form-control ui-autocomplete-input" placeholder="Buscar ..." autocomplete="off" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- buscar -->
                                    <li class="dropdown-settings search-desktop-icon">
                                        <a class="cd-search-trigger-desktop tooltips" href="#cd-search" data-placement="bottom" data-original-title="Buscar"><i class="fa fa-search"></i></a>
                                    </li>
                                    <!-- tienda apps -->
                                    <li class="dropdown-settings" runat="server" id="liApps2">
                                        <a href="/shopping.aspx" data-placement="bottom" data-original-title="Potenciá tu plan!" class="tooltips"><i class="fa fa-shopping-cart"></i><span class="link-text">Potenciá tu plan!</span></a>
                                    </li>
                                    <!-- ayuda -->
                                    <li class="has-children dropdown-settings dropdown-default">
                                        <a href="#" data-placement="bottom" data-original-title="Ayuda" class="tooltips"><i class="fa fa-question-circle"></i><span class="link-text">Ayuda</span></a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li class="title">
                                                <h5>Centro de ayuda</h5>
                                            </li>
                                            <%--<li><a href="/modulos/seguridad/referidos.aspx">Recomendá Contabilium y ganá $$<span class="subinfo">No te pierdas la oportunidad</span></a></li>--%>
                                            <li><a href="http://www.contabilium.com/ayuda/" target="_blank">Capacitación y tutoriales <span class="subinfo">Accedé a un listado de tutoriales donde podrás aprender cómo usar las funcionalidades del sistema</span></a></li>
                                            <li><a href="http://www.contabilium.com/ayuda/faqs/" target="_blank">Preguntas frecuentes <span class="subinfo">Despejá todas tus dudas. Qué estás esperando?</span></a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#modalAyuda">Soporte <span class="subinfo">Escribinos y te responderá un especialista.</span></a></li>
                                        </ul>
                                    </li>
                                    <!-- mensajes -->
                                    <li class="has-children dropdown-settings dropdown-default">
                                        <a href="#" data-placement="bottom" data-original-title="Mensajes" class="tooltips">
                                            <i class="fa fa-envelope"></i><span class="link-text">Mensajes</span> <span class="notify"><asp:Literal runat="server" ID="litMsjCant2"></asp:Literal></span></a>
                                        <ul class="cd-secondary-nav is-hidden dd-first-lvl">
                                            <li class="go-back"><a href="#0">Volver</a></li>
                                            <li class="title">
                                                <h5>Mensajes</h5>
                                            </li>
                                            <li runat="server" id="liSinMsj2">
                                                <div class="no-msg">No hay mensajes nuevos</div>
                                            </li>
                                            <li runat="server" id="liFE2">
                                                <a href="#" data-toggle="modal" data-target="#modalAyuda">
                                                    Facturación electrónica <span class="subinfo">Aún no configuraste tu cuenta para poder emitir comprobantes de forma electrónica
                                                </a>
                                            </li>
                                            <%--<li class="new" runat="server" id="liPuntosDeVenta">
                                                <a href="/modulos/seguridad/mis-datos.aspx">
                                                    <span class="desc" style="margin-left: 0px">
                                                        <span class="name">Puntos de venta <span class="badge badge-success">configurar</span></span>
                                                        <span class="msg">Aún no configuraste tus puntos de venta.<br />
                                                            ¿Qué estás esperando?
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>--%>

                                            <asp:Literal runat="server" ID="liPagarPlan2"></asp:Literal>
                                            <asp:Literal runat="server" ID="litAlertas2"></asp:Literal>

                                        </ul>
                                    </li>
                                </ul>

                            </nav>
                        </div>
                        <div id="cd-search" class="cd-search">
                            <form>
                                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                                <input type="text" id="autocompleteHeader2" class="form-control ui-autocomplete-input" placeholder="Buscar ..." autocomplete="off" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="cd-overlay"></div>
<%--<asp:Literal runat="server" ID="litScript"></asp:Literal>--%>

﻿using ACHE.Model;
using ACHE.Negocio.Common;
using ACHE.Negocio.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class controls_header : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentUser"] != null)
        {
            var user = (WebUser)Session["CurrentUser"];

            int cantAvisos = 0;

            //Valido qué avisos se muestran
            if (!user.TieneFE && user.CondicionIVA == "RI" && user.SetupFinalizado)
            {
                cantAvisos++;
                liFE.Visible = liFE2.Visible = true;
            }
            else
                liFE.Visible = liFE2.Visible = false;

            //liIva1.Visible = liIva2.Visible = liIva3.Visible = (user.CondicionIVA == "RI");

            if (user.TipoUsuario == "B")
                mostrarHeaderSegunPermiso();
            else if (user.TipoUsuario == "C")
                mostrarHeaderSegunPermisoPersonalizado();

            if (user.TipoUsuario == "B" || user.TipoUsuario == "C")
            {
                liEmpresa.Visible = liEmpresa2.Visible = false;
                liUsuarios.Visible = liUsuarios2.Visible = false;
                liPagar.Visible = liPagar2.Visible = false;
            }

            using (var dbContext = new ACHEEntities())
            {
                //LH: var puntosDeVenta = UsuarioCommon.ObtenerPuntosDeVenta(user);
                ////bool puntos = dbContext.PuntosDeVenta.Any(x => x.IDUsuario == user.IDUsuario);
                //bool puntos = puntosDeVenta.Count > 0;

                //if (!puntos)
                //{
                //    cantAvisos++;
                //    liPuntosDeVenta.Visible = true;
                //}
                //else
                //    liPuntosDeVenta.Visible = false;

                cantAvisos += verificarAlertas(dbContext);

                //oculta header
                //LH:setupFinalizado.Visible = user.SetupFinalizado;
                //LH:lnkMiCuenta.Visible = user.SetupFinalizado;
                //LH:lnkTiendaApps.Visible = user.SetupFinalizado;


                if (user.UsaPlanCorporativo) //Plan Corporativo
                {
                    if (user.TipoUsuario == "B" || user.TipoUsuario == "C")
                    {

                    }
                    else
                    {
                        liContabilidad.Visible = true;
                        liImportarPlanDeCuentas.Visible = liImportarPlanDeCuentas2.Visible = true;
                    }
                }
                else
                {
                    liContabilidad.Visible = false;
                    liImportarPlanDeCuentas.Visible = liImportarPlanDeCuentas2.Visible = false;
                }

                var nombrePlan = string.Empty;
                var cantDias = 0;

                if (Common.AlertaPlan(dbContext, user.IDUsuario, ref nombrePlan, ref cantDias))
                {
                    var txtDias = string.Empty;
                    if (cantDias == 1)
                        txtDias = cantDias.ToString() + " día.";
                    else
                        txtDias = cantDias.ToString() + " días.";

                    cantAvisos++;
                    liPagarPlan.Text = liPagarPlan2.Text = "<li><a href='#' data-toggle='modal' onclick=Common.pagarPlanActual('" + nombrePlan.ToString() + "');>Pagar/Renovar plan " + nombrePlan + " <span class='subinfo'>Su plan vence en " + txtDias + "</span></a></li>";
                }
                ////Muestro el menu rapido de acceso a ML
                //if (!user.MostrarMenuML.HasValue) {
                //    bool conectado = IntegracionCommon.Conectado(AplicacionIntegracion.MercadoLibre, user.IDUsuario);
                //    if (conectado) {
                //        user.MostrarMenuML = true;
                //        litScript.Text = "<script>document.getElementById('liMercadoLibre').style.display='block';</script>";
                //    }
                //    else
                //        user.MostrarMenuML = false;

                //Session["CurrentUser"] = user;
                //}
                //else {
                //    if (user.MostrarMenuML.Value)
                //        litScript.Text = "<script>document.getElementById('liMercadoLibre').style.display='block';</script>";
                //}
                //hdnMostrarMenuML.Value = conectado ? "1" : "0";
            }

            //btnMensaje.Visible = user.SetupFinalizado;

            //LH: if (user.SetupFinalizado)
            //    LitCant.Text = (cantAvisos > 0) ? "<i class='glyphicon glyphicon-envelope'></i><strong><span id='spamMensajes'> Mensajes (" + cantAvisos.ToString() + ")</span></strong>" : "<i class='glyphicon glyphicon-envelope'></i><span id='spamMensajes'> Mensajes</span>";

            //Muestro avisos  
            litMsjCant.Text = litMsjCant2.Text = cantAvisos.ToString();

            if (cantAvisos > 0)
                liSinMsj.Visible = liSinMsj2.Visible = false;

            if (PermisosModulos.tienePlan("Empresas"))
                mostrarMultiEmpresas();
            else
                divUsuariosMultiempresas.Visible = divUsuariosMultiempresas2.Visible = false;

            //TODO: MEJORAR ESTO CUANDO HAYA MAS DE 1 USUARIO
            //liCustom.Visible = user.IDUsuario == 404;

            
        }
    }

    private void mostrarMultiEmpresas()
    {
        var user = (WebUser)Session["CurrentUser"];
        using (var dbContext = new ACHEEntities())
        {
            if (OcultarDatosUsuarioEmpresas(dbContext))
            {
                List<EmpresasViewModel> list = new List<EmpresasViewModel>();
                list = UsuarioCommon.ListaEmpresasDisponibles(user, dbContext);
                EmpresasViewModel empresaActual = null;
                if (list.Count > 0)
                {
                    empresaActual = list.FirstOrDefault(item => item.ID == user.IDUsuario);
                    if (empresaActual == null)
                    {
                        empresaActual = list.FirstOrDefault();
                        UsuarioCommon.cambiarEmpresa(empresaActual.ID);
                    }
                }
                if (list.Count <= 1)
                    divUsuariosMultiempresas.Visible = divUsuariosMultiempresas2.Visible = false;
                else
                {
                    litEmpresaActual.Text = litEmpresaActual2.Text = empresaActual != null ? empresaActual.RazonSocial : "";
                    var empresaId = empresaActual != null ? empresaActual.ID : 0;

                    foreach (var item in list.Where(x => x.ID != empresaId).OrderBy(x => x.RazonSocial))
                    {
                        //var direccion = item.Domicilio + ", " + item.Ciudad + ", " + item.Provincia;
                        //var datosInpositivos = item.CUIT + ", " + item.CondicionIva;
                        liEmpresasHeader.Text += "<li><a href='#' onclick=\"Common.cambiarSesion(" + item.ID + ",'" + item.RazonSocial + "');\">" + item.RazonSocial + " <span class='subinfo'>" + item.CUIT + "</span></a></li>";
                        liEmpresasHeader2.Text += "<li><a href='#' onclick=\"Common.cambiarSesion(" + item.ID + ",'" + item.RazonSocial + "');\">" + item.RazonSocial + " <span class='subinfo'>" + item.CUIT + "</span></a></li>";
                    }
                }
            }
        }
    }

    private bool OcultarDatosUsuarioEmpresas(ACHEEntities dbContext)
    {
        var CurrentUser = (WebUser)Session["CurrentUser"];
        var tieneAcceso = true;
        if (CurrentUser.IDUsuarioPadre > 0 && CurrentUser.IDUsuarioAdicional > 1)
        {
            var usuAdic = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == CurrentUser.IDUsuarioAdicional).FirstOrDefault();
            if (usuAdic.IDUsuario != CurrentUser.IDUsuarioPadre)
            {
                divUsuariosMultiempresas.Visible = divUsuariosMultiempresas2.Visible = false;
                var list = UsuarioCommon.ListaEmpresasDisponibles(CurrentUser, dbContext);
                if (list.Count > 0)
                {
                    if (list.Any(x => x.ID == CurrentUser.IDUsuario))
                        litEmpresaActual.Text = litEmpresaActual2.Text = CurrentUser.RazonSocial;
                    //litSinMultiEmpresa.Text = "Empresa Actual: " + CurrentUser.RazonSocial;
                    else
                    {
                        var empresaActual = list.FirstOrDefault();
                        litEmpresaActual.Text = litEmpresaActual2.Text = empresaActual.RazonSocial;
                        //litSinMultiEmpresa.Text = "Empresa Actual: " + empresaActual.RazonSocial;
                        UsuarioCommon.cambiarEmpresa(empresaActual.ID);
                    }
                }

                tieneAcceso = false;
            }
        }
        return tieneAcceso;
    }

    private void mostrarHeaderSegunPermiso()
    {
        //Ventas
        if (!PermisosModulos.ocultarHeader(2))
            liVentas.Visible = true;
        else
            liVentas.Visible = false;

        //Compras
        if (!PermisosModulos.ocultarHeader(1))
            liCompras.Visible = true;
        else
            liCompras.Visible = false;

        //Tesoreria
        if (!PermisosModulos.ocultarHeader(3))
            litesoreria.Visible = true;
        else
            litesoreria.Visible = false;

        //Reportes
        if (!PermisosModulos.ocultarHeader(4))
            liReportes.Visible = true;
        else
            liReportes.Visible = false;

        //Herramientas
        if (!PermisosModulos.ocultarHeader(5))
            liHerramientas.Visible = true;
        else
            liHerramientas.Visible = false;

        //Contabilidad
        if (!PermisosModulos.ocultarHeader(7))
            liContabilidad.Visible = liImportarPlanDeCuentas.Visible = liImportarPlanDeCuentas2.Visible = true;
        else
            liContabilidad.Visible = liImportarPlanDeCuentas.Visible = liImportarPlanDeCuentas2.Visible = false;

        //Inventarios
        if (!PermisosModulos.ocultarHeader(8))
            liInventarios.Visible = true;
        else
            liInventarios.Visible = false;

        //Recursos Humanos
        //if (PermisosModulos.ocultarHeader(7))
        //    liRRHH.Visible = true;
        //else
        //    liRRHH.Visible = false;

    }

    private void mostrarHeaderSegunPermisoPersonalizado()
    {
        //Ventas
        if (!PermisosModulos.ocultarHeaderPersonalizado(2))
        {
            liVentas.Visible = true;
            liVentasComprobantes.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liVentasComprobantes.Attributes["data-formulario"]);
            liVentasCobranzas.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liVentasCobranzas.Attributes["data-formulario"]);
            liVentasPresupuestos.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liVentasPresupuestos.Attributes["data-formulario"]);
            liVentasAbonos.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liVentasAbonos.Attributes["data-formulario"]);
            //liVentasSuscripciones.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liVentasSuscripciones.Attributes["data-formulario"]);
            liVentasProductosyServicios.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liVentasProductosyServicios.Attributes["data-formulario"]);
            liVentasClientes.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liVentasClientes.Attributes["data-formulario"]);
            
        }
        else
            liVentas.Visible = false;

        //Inventarios
        liModificacionStock.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(8, liModificacionStock.Attributes["data-formulario"]);
        liMovimientosDepositos.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(8, liMovimientosDepositos.Attributes["data-formulario"]);
        if (!PermisosModulos.ocultarHeaderPersonalizado(8))
        {
            liInventarios.Visible = true;
        }
        else
            liInventarios.Visible = false;

        //Inventarios Importacion
        var mostrarIMProductos = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liImportacionMasivaProductos.Attributes["data-formulario"]);
        var mostrarIMStock = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liActualizacionMasivaStock.Attributes["data-formulario"]);
        liImportacionMasivaProductos.Visible = mostrarIMProductos;
        liActualizacionMasivaStock.Visible = mostrarIMStock;
        if (mostrarIMProductos || mostrarIMStock)// si alguna de la opcion de importacion de inventarios esta activa muestro el modulo
            liInventarios.Visible = true;


        //Compras
        if (!PermisosModulos.ocultarHeaderPersonalizado(1))
        {
            liCompras.Visible = true;
            liComprasComprobantes.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(1, liComprasComprobantes.Attributes["data-formulario"]);
            liComprasPagos.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(1, liComprasPagos.Attributes["data-formulario"]);
            liComprasProveedores.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(1, liComprasProveedores.Attributes["data-formulario"]);
        }
        else
            liCompras.Visible = false;

        //Tesoreria
        if (!PermisosModulos.ocultarHeaderPersonalizado(3))
        {
            litesoreria.Visible = true;
            liTesoreriaBancos.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(3, liTesoreriaBancos.Attributes["data-formulario"]);
            //liTesoreriaGastosBancarios.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(3, liTesoreriaGastosBancarios.Attributes["data-formulario"]);
            liTesoreriaCheques.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(3, liTesoreriaCheques.Attributes["data-formulario"]);
            liTesoreriaCaja.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(3, liTesoreriaCaja.Attributes["data-formulario"]);
            liTesoreriaMovimientosFondos.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(3, liTesoreriaMovimientosFondos.Attributes["data-formulario"]);
            liTesoreriaDetalleBancario.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(3, liTesoreriaDetalleBancario.Attributes["data-formulario"]);
        }
        else
            litesoreria.Visible = false;

        //Reportes
        if (!PermisosModulos.ocultarHeaderPersonalizado(4))
        {
            liReportes.Visible = true;


            #region subMenu FinancierosEconomicos
            liReportesVentasVsCompras.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesVentasVsCompras.Attributes["data-formulario"]);
            liReportesCobradoVsPagado.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesCobradoVsPagado.Attributes["data-formulario"]);
            liReportesVentasVsPagado.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesVentasVsPagado.Attributes["data-formulario"]);
            liReportesComprasPorCategoria.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesComprasPorCategoria.Attributes["data-formulario"]);
            liReportesComprasPorCategoriaMensual.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesComprasPorCategoriaMensual.Attributes["data-formulario"]);
            
            bool financierosEconomicos = false;

            if (liReportesVentasVsCompras.Visible
            || liReportesCobradoVsPagado.Visible
            || liReportesVentasVsPagado.Visible
            || liReportesComprasPorCategoria.Visible
            || liReportesComprasPorCategoriaMensual.Visible
            
            )
                financierosEconomicos = true;

            liFinancierosEconomicos.Visible = financierosEconomicos;
            #endregion

            #region subMenu Impositivos
            liIva1.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liIva1.Attributes["data-formulario"]);
            liIva2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liIva2.Attributes["data-formulario"]);
            liIva3.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liIva3.Attributes["data-formulario"]);
            liReportesRetenciones.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesRetenciones.Attributes["data-formulario"]);
            liReportesPercepciones.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesPercepciones.Attributes["data-formulario"]);
            liReportesCitiVentas.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesCitiVentas.Attributes["data-formulario"]);
            liReportesSicore.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesSicore.Attributes["data-formulario"]);

            bool impositivos = false;

            if (liIva1.Visible
            || liIva2.Visible
            || liIva3.Visible
            || liReportesRetenciones.Visible
            || liReportesPercepciones.Visible
            || liReportesCitiVentas.Visible
            || liReportesSicore.Visible)
                impositivos = true;

            liImpositivos.Visible = impositivos;
            #endregion

            #region subMenu Productos
            liReportesStock.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesStock.Attributes["data-formulario"]);
            liReportesHistoricoStock.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesHistoricoStock.Attributes["data-formulario"]);
            liReportesRankingPorProducto.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesRankingPorProducto.Attributes["data-formulario"]);
            liReportesProductosPorCliente.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesProductosPorCliente.Attributes["data-formulario"]);

            bool productos = false;

            if (liReportesStock.Visible
            || liReportesHistoricoStock.Visible
            || liReportesRankingPorProducto.Visible
            || liReportesProductosPorCliente.Visible
            )
                productos = true;

            liProductos.Visible = productos;
            #endregion

            #region subMenu Gestion
            liReportesCuentaCorriente.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesCuentaCorriente.Attributes["data-formulario"]);
            liReportesCobranzasPendientes.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesCobranzasPendientes.Attributes["data-formulario"]);
            liReportesSaldosClientes.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesSaldosClientes.Attributes["data-formulario"]);
            liReportesSaldosProveedores.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesSaldosProveedores.Attributes["data-formulario"]);
            liReportesPagoAProveedores.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesPagoAProveedores.Attributes["data-formulario"]);
            liReportesCuentasAPagar.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesCuentasAPagar.Attributes["data-formulario"]);
            liReportesRankingPorCliente.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesRankingPorCliente.Attributes["data-formulario"]);
            liReportesTrackingHoras.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesTrackingHoras.Attributes["data-formulario"]);
            liReportesComprasPorCliente.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(4, liReportesComprasPorCliente.Attributes["data-formulario"]);
            
            bool gestion = false;

            if (liReportesCuentaCorriente.Visible
            || liReportesCobranzasPendientes.Visible
            || liReportesSaldosClientes.Visible
            || liReportesSaldosProveedores.Visible
            || liReportesPagoAProveedores.Visible
                       || liReportesCuentasAPagar.Visible
            || liReportesRankingPorCliente.Visible
            || liReportesTrackingHoras.Visible
            || liReportesComprasPorCliente.Visible
            )
                gestion = true;

            liGestion.Visible = gestion;
            #endregion
        }
        else
            liReportes.Visible = false;

        //Herramientas
        if (!PermisosModulos.ocultarHeaderPersonalizado(5))
        {
            liHerramientas.Visible = liHerramientas2.Visible = true;
            liHerramientasExploradorArchivos.Visible = liHerramientasExploradorArchivos2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasExploradorArchivos.Attributes["data-formulario"]);
            liHerramientasImportarClientes.Visible = liHerramientasImportarClientes2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasImportarClientes.Attributes["data-formulario"]);
            liHerramientasImportarProveedores.Visible = liHerramientasImportarProveedores2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasImportarProveedores.Attributes["data-formulario"]);
            liHerramientasImportarProductos.Visible = liHerramientasImportarProductos2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasImportarProductos.Attributes["data-formulario"]);
            liHerramientasImportarServicios.Visible = liHerramientasImportarServicios2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasImportarServicios.Attributes["data-formulario"]);
            liHerramientasImportarFacturas.Visible = liHerramientasImportarFacturas2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasImportarFacturas.Attributes["data-formulario"]);
            liHerramientasImportarComprovantes2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasImportarComprovantes2.Attributes["data-formulario"]);
            liHerramientasImportarFacturasCompras.Visible = liHerramientasImportarFacturasCompras2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasImportarFacturasCompras.Attributes["data-formulario"]);
            liHerramientasTrackingHoras.Visible = liHerramientasTrackingHoras2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasTrackingHoras.Attributes["data-formulario"]);
            liHerramientasAlertas.Visible = liHerramientasAlertas2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liHerramientasAlertas.Attributes["data-formulario"]);
            liImportarPlanDeCuentas.Visible = liImportarPlanDeCuentas2.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(5, liImportarPlanDeCuentas.Attributes["data-formulario"]);
        }
        else
            liHerramientas.Visible = liHerramientas2.Visible = false;

        //Contabilidad
        if (!PermisosModulos.ocultarHeaderPersonalizado(7))
        {
            liContabilidad.Visible = liImportarPlanDeCuentas.Visible = true;
            liContabilidadPlanDeCuentas.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(7, liContabilidadPlanDeCuentas.Attributes["data-formulario"]);
            liContabilidadLibroDiario.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(7, liContabilidadLibroDiario.Attributes["data-formulario"]);
            liContabilidadLibroMayor.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(7, liContabilidadLibroMayor.Attributes["data-formulario"]);
            liContabilidadBalanceGeneral.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(7, liContabilidadBalanceGeneral.Attributes["data-formulario"]);
            liContabilidadAsientosManuales.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(7, liContabilidadAsientosManuales.Attributes["data-formulario"]);
            liContabilidadEstadoResultado.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(7, liContabilidadEstadoResultado.Attributes["data-formulario"]);
            
        }
        else
            liContabilidad.Visible = liImportarPlanDeCuentas.Visible = liImportarPlanDeCuentas2.Visible = false;

    }

    private int verificarAlertas(ACHEEntities dbContext)
    {
        var user = (WebUser)Session["CurrentUser"];
        var listaAlertas = dbContext.AlertasGeneradas.Include("Personas").Where(x => x.IDUsuario == user.IDUsuario && x.Visible).ToList();

        foreach (var alertas in listaAlertas)
        {

            var tipo = (alertas.Alertas.AvisoAlerta == "El pago a un proveedor es") ? "1" : "0";
            var id = (tipo == "1") ? alertas.IDPagos : alertas.IDCobranzas;
            var persona = (tipo == "1") ? " Al Proveedor: " : " Al Cliente: ";

            var mensaje = alertas.NroComprobante + persona + alertas.Personas.RazonSocial;
            litAlertas.Text += "<li id='alertaGenerada_" + alertas.IDAlertasGeneradas + "'><a href='#' onclick='alertas.abrirComprobante(" + id + "," + tipo + ");'>" + mensaje + " <span class='subinfo' id='msgAlertasPagos' onclick='alertas.esconderAlertaGenerada(" + alertas.IDAlertasGeneradas + ");'>Haga click aqui para ocultar las alertas.</span></a></li>";
            litAlertas2.Text += "<li id='alertaGenerada_" + alertas.IDAlertasGeneradas + "'><a href='#' onclick='alertas.abrirComprobante(" + id + "," + tipo + ");'>" + mensaje + " <span class='subinfo' id='msgAlertasPagos' onclick='alertas.esconderAlertaGenerada(" + alertas.IDAlertasGeneradas + ");'>Haga click aqui para ocultar las alertas.</span></a></li>";
        }
        return listaAlertas.Count();
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="nuevoConcepto.ascx.cs" Inherits="controls_nuevoConcepto" %>

<%--Por ahora solo se usa en la creacion de un combo en conceptose--%>
<div id="divDetalle" runat="server" class="col-sm-12">
    <div class="well">
        <div class="alert alert-danger" id="divErrorDetalle" style="display: none">
            <strong>Lo siento! </strong><span id="msgErrorDetalle"></span>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                <span class="asterisk">*</span> Cantidad
                <input type="text" class="form-control" maxlength="10" id="txtCantidad" />
            </div>
             <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                <span class="asterisk">*</span> Seleccione un Producto/Servicio
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <select class="select2" data-placeholder="Seleccione un Producto/Servicio" id="ddlProductos" onchange="changeConcepto();">
                        </select>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                Descripción
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <input type="text" class="form-control" maxlength="500" id="txtConcepto" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12 form-group">
                <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="agregarItem();">Agregar</a>
                <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="cancelarItem();">Cancelar</a>
                <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                <input type="hidden" runat="server" ID="hdnCodigo" Value="" />
                <input type="hidden" runat="server" ID="hdnPersona" Value="" />
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-invoice">
            <thead>
                <tr>
                    <th>#</th>
                    <th style="text-align: right">Cantidad</th>
                    <th style="text-align: left">Código</th>
                    <th style="text-align: left">Descripción</th>
                    <th style="text-align: right">Costo interno s/IVA</th>
                    <th style="text-align: right">Precio unit. sin IVA</th>
                </tr>
            </thead>
            <tbody id="bodyDetalle">
            </tbody>
        </table>
     
    </div>
</div>
<asp:HiddenField runat="server" ID="hdnUsaPlanCorporativo" Value="0" />

<%--<script src="/js/jquery-1.11.1.min.js"></script>--%>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="nuevaPersona.ascx.cs" Inherits="controls_nuevaPersona" %>

<div class="modal modal-wide fade" id="modalNuevoCliente" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="contentpanel">
            <form id="frmNuevaPersona" class="col-sm-12" onsubmit="return false;">
                <div class="row mb15">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="alert alert-danger" id="divAyudaCliente" style="display: none">
                                <strong>Lo sentimos! </strong><span id="msgAyudaCliente"></span>
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Razón Social</label>
                                        <input type="text" id="txtRazonSocial" name="txtRazonSocial" class="form-control required" maxlength="128" tabindex="1" onkeyup="remplazarCaracteresEspeciales2('txtRazonSocial');"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Categoría Impositiva</label>
                                        <select id="ddlCondicionIva" class="form-control required" tabindex="2" onchange="changeCondicionIvaNuevaPersona();">
                                            <option value=""></option>
                                            <option value="RI">Responsable Inscripto</option>
                                            <option value="CF">Consumidor Final</option>
                                            <option value="MO">Monotributista</option>
                                            <option value="EX">Exento</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb15" >
                                <div class="col-sm-6" id="divPersoneriaNuevaPersona">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Personería</label>
                                        <select id="ddlPersoneria" class="form-control" tabindex="3">
                                            <option value="F">Física</option>
                                            <option value="J">Jurídica</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="divFantasia">
                                        <label class="control-label">Nombre de Fantasía</label>
                                        <input id="txtNombreFantasia" type="text" class="form-control required" maxlength="128" tabindex="4" />
                                    </div>
                                </div>
                            </div>
                            <div class="row mb15" id="divNumeroNuevaPersona">
                                <div class="col-sm-6" >
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk" id="spIdentificacionObligatoria">*</span> Tipo Doc</label>
                                        <select id="ddlTipoDoc" class="form-control required" tabindex="5">
                                            <option value=""></option>
                                            <option value="DNI">DNI</option>
                                            <option value="CUIT" selected="selected">CUIT/CUIL</option>
                                        </select>
                                    </div>
                                </div>
                                  
                                       
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Número </label>
                                        <input id="txtNroDocumento" type="text" class="form-control required number validCuit" maxlength="13" tabindex="6" />
                                           
                                    </div>
                                </div>
                                
                                     
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"> País</label>
                                        <select id="ddlPais" class="select2" data-placeholder="Selecciona un país..." tabindex="7" onchange="changePais();">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"> Provincia</label>
                                        <select id="ddlProvincia" class="select2" data-placeholder="Selecciona una provincia..." tabindex="7" onchange="changeProvincia();">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label"> Ciudad/Localidad</label>
                                        <select id="ddlCiudadCliente" class="select2" data-placeholder="Selecciona una ciudad..." tabindex="8">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb15" id="divDomicilioNuevaPersona">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"> Domicilio</label>
                                        <input type="text" id="txtDomicilio" class="form-control" maxlength="100"  tabindex="9"/>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Piso/Depto</label>
                                        <input id="txtPisoDepto" type="text" class="form-control" maxlength="10"  tabindex="10"/>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Código Postal</label>
                                        <input id="txtCp" type="text" class="form-control" maxlength="10"  tabindex="11"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">Código</label>
                                        <input id="txtCodigoPersonas" type="text" class="form-control" maxlength="10"  tabindex="12"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"> Email</label>
                                          <input id="txtEmailNuevaPersona" type="text" class="form-control" maxlength="128" tabindex="13" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                             
                            <input type="button" id="btnCrear" class="btn btn-primary" value="Crear" onclick="crearPersona();"  tabindex="14"/>
                            <a style="margin-left:20px" href="#" onclick="$('#modalNuevoCliente').modal('toggle');" tabindex="15">Cancelar</a>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="hdnNuevaPersonaTipo" value="C" />

                <input type="hidden" id="hdnPais" value="10" />
                <input type="hidden" id="hdnProvincia" value="2" />
                
            </form>
        </div>
    </div>
</div>

<script src="/js/views/nuevaPersona.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class controls_nuevoConcepto : System.Web.UI.UserControl
{
    private IList<AbonosDetalleViewModel> items;
    private string precioFinalIva=String.Empty;
    private WebUser currentUser = null;
    private string idPersona = String.Empty;
    private bool mostrarPercepciones = true;

    public IList<AbonosDetalleViewModel> Items
    {
        get { return items; }
        set { items = value; }
    }

    public WebUser CurrentUser
    {
        get { return currentUser; }
        set { currentUser = value; }
    }
    
    public String PrecioFinalIva
    {
        get { return precioFinalIva; }
        set { precioFinalIva = value;}
    }

    public String IdPersona
    {
        get { return idPersona; }
        set { idPersona = value; }
    }

    public bool MostrarPercepciones
    {
        get { return mostrarPercepciones; }
        set { mostrarPercepciones = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            if (!String.IsNullOrEmpty(IdPersona))
                hdnPersona.Value = IdPersona;
            obtenerItems();
        }
    }

    public List<DetalleViewModel> GetItems()
    {
        return DetalleCart.Retrieve().Items;
    }

    [WebMethod(true)]
    public static string obtenerItems()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var list = DetalleCart.Retrieve().Items.OrderBy(x => x.Concepto).ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
                        
                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='6' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }
}

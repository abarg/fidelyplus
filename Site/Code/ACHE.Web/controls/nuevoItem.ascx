﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="nuevoItem.ascx.cs" Inherits="controls_nuevoItem" %>

<%--Por ahora solo se usa en compras detalladas--%>
<div id="divDetalle" runat="server" class="col-sm-12">
    <div class="well">
        <div class="alert alert-danger" id="divErrorDetalle" style="display: none">
            <strong>Lo siento! </strong><span id="msgErrorDetalle"></span>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                <span class="asterisk">*</span> Cantidad
                <input type="text" class="form-control" maxlength="10" id="txtCantidad" />
            </div>
             <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                <span class="asterisk">*</span> Seleccione un Producto/Servicio
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <select class="select4" data-placeholder="Seleccione un Producto/Servicio" id="ddlProductos" onchange="changeConcepto();">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                o ingrese una descripción libre
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <input type="text" class="form-control" maxlength="500" id="txtConcepto" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                <span class="asterisk">*</span> Costo interno s/IVA
                <%--<asp:Literal ID="liPrecioUnitario" runat="server"></asp:Literal>--%>
                <input type="text" class="form-control" maxlength="10" id="txtPrecioDet" />
            </div>
            <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                Bonif %
                <input type="text" class="form-control" maxlength="6" id="txtBonificacion" />
            </div>
            <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                <span class="asterisk">*</span> IVA %
                <select class="form-control" id="ddlIvaDet">
                    <option value="0,00">0</option>
                    <option value="2,50">2,5</option>
                    <option value="5,00">5</option>
                    <option value="10,50">10,5</option>
                    <option value="21,00">21</option>
                    <option value="27,00">27</option>
                </select>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-2 form-group divPlanDeCuentas" style="clear:left">
                <div class="form-group">
                    <span class="asterisk">*</span> Cuenta contable
                    <asp:DropDownList runat="server" ID="ddlPlanDeCuentas2" CssClass="select3" data-placeholder="Seleccione una cuenta...">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12 form-group">
                <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="agregarItem();">Agregar</a>
                <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="cancelarItem();">Cancelar</a>
                <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                <input type="hidden" runat="server" ID="hdnCodigo" Value="" />
                <input type="hidden" runat="server" ID="hdnPersona" Value="" />
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-invoice">
            <thead>
                <tr>
                    <th>#</th>
                    <th style="text-align: right">Cantidad</th>
                    <th style="text-align: left">Código</th>
                    <th style="text-align: left">Descripción</th>
                    <th style="text-align: right" id="tdPrecio">Costo interno s/IVA</th>
                    <th style="text-align: right">Bonif. %</th>
                    <th style="text-align: right">IVA %</th>
                    <th class="divPlanDeCuentas" style="text-align: left">Cuenta</th>
                    <th style="text-align: right">Subtotal</th>
                </tr>
            </thead>
            <tbody id="bodyDetalle">
            </tbody>
        </table>
        <div class="col-sm-12">
            <table class="table table-total">
                <tbody>
                    <tr id="trImpNoGrabado" class="hide">
                        <td><strong>Neto no gravado :</strong></td>
                        <td id="Td1">
                            <asp:TextBox runat="server" ID="txtImporteNoGrabado" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static" Text="0" onBlur="obtenerTotales();"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Neto gravado :</strong></td>
                        <td id="divSubtotal">$ 0</td>
                    </tr>
                    <tr>
                        <td><strong>IVA :</strong></td>
                        <td id="divIVA">$ 0</td>
                    </tr>

                    <tr id="trIVA" runat="server">
                        <td><strong>Percepción IVA :</strong></td>
                        <td id="trIVATotal">$ 0</td>
                    </tr>
                    <tr id="trIIBB" runat="server">
                        <td><strong>Percepción IIBB :</strong></td>
                        <td id="trIIBBTotal">$ 0</td>
                    </tr>

                    <tr>
                        <td><strong>TOTAL :</strong></td>
                        <td id="divTotalItems" style="color: green">$ 0</td>
                        <asp:HiddenField runat="server" ID="hdnTotal" Value="0" />
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<asp:HiddenField runat="server" ID="hdnUsaPlanCorporativo" Value="0" />

<%--<script src="/js/jquery-1.11.1.min.js"></script>--%>

<%--<script>
    jQuery(document).ready(function () {
        configForm();
    });
</script>--%>


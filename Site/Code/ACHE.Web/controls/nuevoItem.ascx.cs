﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class controls_nuevoItem : System.Web.UI.UserControl
{
    private IList<AbonosDetalleViewModel> items;
    private string precioFinalIva=String.Empty;
    private WebUser currentUser = null;
    private string idPersona = String.Empty;
    private bool mostrarPercepciones = true;

    public IList<AbonosDetalleViewModel> Items
    {
        get { return items; }
        set { items = value; }
    }

    public WebUser CurrentUser
    {
        get { return currentUser; }
        set { currentUser = value; }
    }
    
    public String PrecioFinalIva
    {
        get { return precioFinalIva; }
        set { precioFinalIva = value;}
    }

    public String IdPersona
    {
        get { return idPersona; }
        set { idPersona = value; }
    }

    public bool MostrarPercepciones
    {
        get { return mostrarPercepciones; }
        set { mostrarPercepciones = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!MostrarPercepciones)
            {
                trIVA.Visible = false;
                trIIBB.Visible = false;
            }
            if (CurrentUser.UsaPlanCorporativo)
            {
                CargarCuentasActivo();
            }
            //if (CurrentUser.UsaPrecioFinalConIVA)
            //    liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Costo interno c/IVA</span> ";
            //else
            //liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Costo interno s/IVA</span> ";

            if (!String.IsNullOrEmpty(IdPersona))
                hdnPersona.Value = IdPersona;
            obtenerItems();
        }
    }

    private void CargarCuentasActivo()
    {
        using (var dbContext = new ACHEEntities())
        {
            if (CurrentUser.UsaPlanCorporativo) //Plan Corporativo
            {
                if (dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == CurrentUser.IDUsuario))
                {
                    //TODO: Puede ser de ventas o compras.
                    var idctas = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault().CtasFiltroCompras.Split(',');
                    List<Int32> cuentas = new List<Int32>();
                    for (int i = 0; i < idctas.Length; i++)
                    {
                        if (idctas[i] != string.Empty)
                            cuentas.Add(Convert.ToInt32(idctas[i]));
                    }

                    var listaAux = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && cuentas.Contains(x.IDPlanDeCuenta)).OrderBy(x => x.Codigo).ToList();
                    ddlPlanDeCuentas2.Items.Add(new ListItem("", ""));

                    foreach (var item in listaAux)
                    {
                        ddlPlanDeCuentas2.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));
                    }
                }
                hdnUsaPlanCorporativo.Value = "1";
            }
        }
    }

    public List<DetalleViewModel> GetItems()
    {
        return DetalleCart.Retrieve().Items;
    }

    [WebMethod(true)]
    public static string obtenerItems()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var list = DetalleCart.Retrieve().Items.ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
                        html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Bonificacion.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Iva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        if (usu.UsaPlanCorporativo)
                            html += "<td class='divPlanDeCuentas' style='text-align:left'>" + detalle.CodigoPlanCta + "</td>";
                        else
                            html += "<td class='divPlanDeCuentas' style='text-align:left; display:none'></div>";

                        html += "<td style='text-align:right'>" + detalle.TotalConIva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";

                        decimal preciFinal = 0;

                        //if (usu.UsaPrecioFinalConIVA)
                        //    preciFinal = detalle.PrecioUnitarioConIvaSinBonificacion;
                        //else
                        preciFinal = detalle.PrecioUnitario;

                        //html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        //html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem(" 
                        //    + detalle.ID 
                        //    + ", '" + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
                        //    + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(ConfiguracionHelper.ObtenerCantidadDecimales()) 
                        //    + "' ,'" + detalle.Codigo 
                        //    + "','" + detalle.Concepto
                        //    + "','" + preciFinal.ToMoneyFormatForEdit(ConfiguracionHelper.ObtenerCantidadDecimales()) 
                        //    + "','" + detalle.Iva.ToString("#0.00")
                        //    + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(ConfiguracionHelper.ObtenerCantidadDecimales()) 
                        //    + "','" + detalle.IDPlanDeCuenta.ToString() 
                        //    + "');\"><i class='fa fa-edit'></i></a></td>";
                        //html += "</tr>";
                        
                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='9' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }
}

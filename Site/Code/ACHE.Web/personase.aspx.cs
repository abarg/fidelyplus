﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Negocio.Common;

public partial class personase : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            var tipo = Request.QueryString["tipo"];
            hdnTipo.Value = tipo;
            divUsuarioVendedor.Visible = false;

            if (CurrentUser.TipoUsuario == "B") {
                if (!PermisosModulos.mostrarPersonaSegunPermiso(tipo))
                    Response.Redirect("home.aspx");
            }
            if (tipo == "c") {
                litTitulo.Text = "<i class='fa fa-suitcase'></i> Clientes";
                litPathPadre.Text = "<a href='/personas.aspx?tipo=c'>Clientes</a>";
                liDatosGenerales.Text = "cliente";
                libtnGuardar.Text = "Guardar cliente";

                divUsuarioVendedor.Visible = true;
                getUsuariosAdicionales();

            }
            else if (tipo == "p") {
                litTitulo.Text = "<i class='fa fa-users'></i> Proveedores";
                litPathPadre.Text = "<a href='/personas.aspx?tipo=p'>Proveedores</a>";
                liDatosGenerales.Text = "proveedor";
                libtnGuardar.Text = "Guardar proveedor";
            }
            else
                Response.Redirect("home.aspx");

            litPath.Text = "Alta";
            litImporteFacturado.Text = litSaldoPendiente.Text = litImportePagado.Text = "0,00";

            if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];
                hdnIDPersona.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0") {
                    cargarEntidad(int.Parse(hdnID.Value));
                    litPath.Text = "Edición";
                }
            }
        }
    }

    private void getUsuariosAdicionales() {
        using (var dbContext = new ACHEEntities()) {
            ddlIDUsuarioAdicional.Items.Add(new ListItem("Seleccione un vendedor", ""));

            foreach (var item in dbContext.UsuariosAdicionales.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.Activo && x.Tipo != "T").ToList()) {
                ddlIDUsuarioAdicional.Items.Add(new ListItem(item.Email, item.IDUsuarioAdicional.ToString()));
            }

        }
    }

    private void cargarEntidad(int id) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Personas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPersona == id).FirstOrDefault();
            if (entity != null) {
                txtRazonSocial.Text = entity.RazonSocial.ToUpper();
                txtNombreFantasia.Text = entity.NombreFantansia.ToUpper();
                ddlCondicionIva.SelectedValue = entity.CondicionIva;
                ddlTipoDoc.SelectedValue = entity.TipoDocumento;
                txtNroDocumento.Text = entity.NroDocumento;
                ddlPersoneria.SelectedValue = entity.Personeria;
                txtEmail.Text = entity.Email != null ? entity.Email.ToLower() : "";
                txtEmailFc.Text = entity.Email != null ? (entity.EmailsEnvioFc ?? "") : "";
                txtTelefono.Text = entity.Telefono;
                //txtCelular.Text = entity.Celular;
                //txtWeb.Text = entity.Web.ToLower();
                txtObservaciones.Text = entity.Observaciones;
                txtCodigo.Text = entity.Codigo;
                txtSaldo.Text = Convert.ToDecimal(entity.SaldoInicial).ToString("").Replace(",", ".");
                if (entity.IDUsuarioAdicional.HasValue)
                    ddlIDUsuarioAdicional.SelectedValue = entity.IDUsuarioAdicional.Value.ToString();

                //Domicilio
                hdnPais.Value = entity.IDPais.ToString();
                hdnProvincia.Value = entity.IDProvincia.ToString();
                hdnCiudad.Value = entity.IDCiudad.ToString();
                txtDomicilio.Text = entity.Domicilio.ToUpper();
                hdnDireccion.Value = entity.Domicilio.ToLower() + ", " + entity.Provincias.Nombre;
                txtPisoDepto.Text = entity.PisoDepto;
                txtCp.Text = entity.CodigoPostal;

                //txtBanco.Text = entity.Banco;
                //txtCbu.Text = entity.CBU;
                //txtContacto.Text = entity.Contacto;

                //Lista de Precios
                if (entity.ListaPrecios != null)
                    hdnIDListaPrecio.Value = entity.ListaPrecios.IDListaPrecio.ToString();

                litImporteFacturado.Text = dbContext.Comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPersona == id)
                   .Select(x => x.ImporteTotalNeto).DefaultIfEmpty(0).Sum().ToMoneyFormat(2);

                var aFavor = dbContext.Comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPersona == id && x.Saldo > 0)
                   .Select(x => x.Saldo).DefaultIfEmpty(0).Sum();
                var enContra = dbContext.Compras.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPersona == id && x.Saldo > 0)
                   .Select(x => x.Saldo).DefaultIfEmpty(0).Sum();
                litSaldoPendiente.Text = (aFavor - enContra).ToMoneyFormat(2);

                litImportePagado.Text = dbContext.Compras.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPersona == id)
                   .Select(x => x.Total.Value).DefaultIfEmpty(0).Sum().ToMoneyFormat(2);

                if (!string.IsNullOrWhiteSpace(entity.Foto)) {
                    imgFoto.Src = "/files/explorer/" + CurrentUser.IDUsuario.ToString() + "/Contactos/" + entity.Foto;
                    hdnTieneFoto.Value = "1";
                }

                var accesoPwd = dbContext.PersonasPwd.Where(x => x.NroDocumento == entity.NroDocumento && x.TipoDocumento == entity.TipoDocumento).FirstOrDefault();
                if (accesoPwd != null)
                    txtPwd.Text = accesoPwd.Pwd;
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static int guardar(int id, string razonSocial, string nombreFantasia, string condicionIva, string personeria, string tipoDoc, string nroDoc,
        string telefono, string email, string emailFc, string tipo,
        int idPais, int idProvincia, int idCiudad, string domicilio, string pisoDepto, string cp, string obs, int listaPrecio, string codigo, decimal saldoInicial, string idUsuarioAdicional) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        var idPersona = PersonasCommon.GuardarPersonas(id, razonSocial, nombreFantasia, condicionIva, personeria, tipoDoc, nroDoc, telefono,
            email, emailFc, tipo, idPais, idProvincia, idCiudad, domicilio, pisoDepto, cp, obs, listaPrecio, codigo, saldoInicial, usu.IDUsuario, idUsuarioAdicional);

        return idPersona;

    }

    [WebMethod(true)]
    public static PersonasViewModel obtenerDatos(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return PersonasCommon.ObtenerDatos(id, usu.IDUsuario);
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerComprobantes(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return PersonasCommon.ObtenerComprobantes(id, usu.IDUsuario);
    }

    [WebMethod(true)]
    public static void eliminarFoto(int idPersona) {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        PersonasCommon.EliminarFoto(idPersona, usu.IDUsuario);

    }
}
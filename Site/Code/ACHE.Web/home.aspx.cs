﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;
using System.Globalization;
using ACHE.Negocio.Common;
using ACHE.Negocio.Contabilidad;
using ACHE.Model.ViewModels;
using System.Data.Entity;

public partial class home : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {

            var nombrePlan = string.Empty;
            var cantDias = 0;

            //ACHE.Negocio.Productos.ConceptosCommon.ActualizarCostosInternosDeCombos(3806, new List<string> { "2945", "2948", "2947", "2946", "2944", "2934", "2933", "2932", "2929", "2923", "2928", "2931", "2922", "2930", "2937", "2936", "2941", "2940", "2967", "2464", "2893", "2502", "2501", "2841", "2718", "2840", "2842", "2843" });
            //ACHE.Negocio.Integraciones.IntegracionCommon.NotificarCambioConcepto(2675, 102252, "P", false);
            //ACHE.Negocio.Integraciones.IntegracionCommon.NotificarCambioCombos(1788, 581389, "P");

            /*if (!CurrentUser.TieneFE && CurrentUser.ModoQA)// && CurrentUser.CondicionIVA == "RI")
                divMensajeFCE.Visible = true;
            else
                divMensajeFCE.Visible = false;
            */
            bool showProfile = false;
            decimal perce = 100;
            if (CurrentUser.CUIT == string.Empty) {
                showProfile = true;
                liProf1.Attributes.Add("class", "no-checked wow fadeIn animated");
                perce -= 33;
            }
            if (CurrentUser.Domicilio == null || CurrentUser.Domicilio.Trim() == string.Empty) {
                showProfile = true;
                liProf2.Attributes.Add("class", "no-checked wow fadeIn animated");
                perce -= 33;
            }
            if (!CurrentUser.TieneFE) {
                showProfile = true;
                liProf3.Attributes.Add("class", "no-checked wow fadeIn animated");
                perce -= 33;
            }

            if (perce == 1)
                perce = 25;

            divPerc.Attributes.Add("data-percent", perce.ToString());
            //divGraph.Attributes.Add("class", "c100 p" + perce + " small left green");
            divMsjeCompletarPerfil.Visible = showProfile;

            //Permiso de Venta y Compra 
            if (CurrentUser.TipoUsuario == "B" && !PermisosModulos.ocultarHeader(1) && !PermisosModulos.ocultarHeader(2))
                hdnPanelDeControl.Value = "1";
            else if (CurrentUser.TipoUsuario == "A")
                hdnPanelDeControl.Value = "1";
            else
                hdnPanelDeControl.Value = "0";

            string basePath = Server.MapPath("~/files/explorer");
            UsuarioCommon.CreateFolders(CurrentUser.IDUsuario, basePath);

            if (CurrentUser.CondicionIVA != "MO" && !Directory.Exists(basePath + "//" + CurrentUser.IDUsuario + "//Balances"))
                UsuarioCommon.CreateFoldersRI(CurrentUser.IDUsuario, basePath);

            basePath = Server.MapPath("~/files/explorer/" + CurrentUser.IDUsuario + "/Comprobantes/" + DateTime.Now.Year.ToString());
            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);

            basePath = Server.MapPath("~/files/explorer/" + CurrentUser.IDUsuario + "/Pagos/" + DateTime.Now.Year.ToString());
            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);

            basePath = Server.MapPath("~/files/explorer/" + CurrentUser.IDUsuario + "/Compras/" + DateTime.Now.Year.ToString());
            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);

            basePath = Server.MapPath("~/files/explorer/" + CurrentUser.IDUsuario + "/OrdenCompra/");
            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);
            basePath = Server.MapPath("~/files/pagos");
            if (!Directory.Exists(basePath + "//" + CurrentUser.IDUsuario))
                Directory.CreateDirectory(basePath + "//" + CurrentUser.IDUsuario);

            basePath = Server.MapPath("~/files/explorer/" + CurrentUser.IDUsuario + "/OrdenVenta/");
            if (!Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);

            //basePath = Server.MapPath("~/files/comprobantes");
            //if (!Directory.Exists(basePath + "//" + CurrentUser.IDUsuario))
            //    Directory.CreateDirectory(basePath + "//" + CurrentUser.IDUsuario);

            //basePath = Server.MapPath("~/files/remitos");
            //if (!Directory.Exists(basePath + "//" + CurrentUser.IDUsuario))
            //    Directory.CreateDirectory(basePath + "//" + CurrentUser.IDUsuario);

            //basePath = Server.MapPath("~/files/Presupuestos");
            //if (!Directory.Exists(basePath + "//" + CurrentUser.IDUsuario))
            //    Directory.CreateDirectory(basePath + "//" + CurrentUser.IDUsuario);

            using (var dbContext = new ACHEEntities()) {
                //var tieneDatosOK = dbContext.Usuarios.Any(x => x.IDUsuario == CurrentUser.IDUsuario && x.Domicilio != "" && x.Personeria != "" && x.CondicionIva != "");
                //if (!tieneDatosOK)
                //    Response.Redirect("~/finRegistro.aspx");
                if (!CurrentUser.TieneDebitoAutomatico) {
                    if (Common.AlertaPlan(dbContext, CurrentUser.IDUsuario, ref nombrePlan, ref cantDias)) {
                        var txtDias = string.Empty;
                        if (cantDias == 1)
                            txtDias = cantDias.ToString() + " dia.";
                        else
                            txtDias = cantDias.ToString() + " dias.";

                        linombrePlan.Text = nombrePlan;
                        liCantDias.Text = txtDias;
                        hdnNombrePlanActual.Value = nombrePlan;
                        divPagarPlan.Visible = true;
                    }
                    else
                        divPagarPlan.Visible = false;
                }
                else
                    divPagarPlan.Visible = false;

                ////Fix cajas y bancos PLAN Corporativo
                //if (CurrentUser.UsaPlanCorporativo)
                //{
                //    foreach (var banco in dbContext.Bancos.Where(x => x.IDUsuario == CurrentUser.IDUsuario).ToList())
                //        ContabilidadCommon.CrearCuentaBancos(banco.IDBanco, CurrentUser);

                //    foreach (var caja in dbContext.Cajas.Where(x => x.IDUsuario == CurrentUser.IDUsuario).ToList())
                //        ContabilidadCommon.CrearCuentaCaja(caja.IDCaja, CurrentUser);
                //}
            }


            if (PermisosModulosCommon.AlertaPlanPendiente(CurrentUser.IDUsuario, ref nombrePlan)) {
                divPlanPendiente.Visible = true;
                linombrePlanPendiente.Text = nombrePlan;
            }
            else
                divPlanPendiente.Visible = false;

            //obtenerDashBoard();
        }
    }

    /*private void obtenerDashBoard()
    {
        var mesDesde = DateTime.Now.GetFirstDayOfMonth();
        var mesHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1);

        var mesDesdeAnt = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth();
        var mesHastaAnt = DateTime.Now.AddMonths(-1).GetLastDayOfMonth();

        var anioDesde = DateTime.Now.GetFirstDayOfYear();
        var anioHasta = DateTime.Now.GetLastDayOfYear();

        using (var dbContext = new ACHEEntities())
        {
            var comprobantes = dbContext.Comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE").ToList();
            var notas = dbContext.Comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario && (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE")).ToList();
            //quitarNC(dbContext, Comprobantes);

            //Ingresos
            decimal ingresosMesAnt = comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario
                && x.FechaComprobante >= mesDesdeAnt && x.FechaComprobante <= mesHastaAnt)
                .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

            decimal ingresosMesAntNotas = notas.Where(x => x.IDUsuario == CurrentUser.IDUsuario
                && x.FechaComprobante >= mesDesdeAnt && x.FechaComprobante <= mesHastaAnt)
                .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

            decimal ingresosMes = comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario
                && x.FechaComprobante >= mesDesde && x.FechaComprobante <= mesHasta)
                .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

            decimal ingresosMesNotas = notas.Where(x => x.IDUsuario == CurrentUser.IDUsuario
                && x.FechaComprobante >= mesDesde && x.FechaComprobante <= mesHasta)
                .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

            decimal ingresosAnio = comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario
                && x.FechaComprobante >= anioDesde && x.FechaComprobante <= anioHasta)
                .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

            decimal ingresosAnioNotas = notas.Where(x => x.IDUsuario == CurrentUser.IDUsuario
                && x.FechaComprobante >= anioDesde && x.FechaComprobante <= anioHasta)
                .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

            litIngresosMesAnterior.Text = (ingresosMesAnt - ingresosMesAntNotas).ToMoneyFormat(2);
            litIngresosMes.Text = (ingresosMes - ingresosMesNotas).ToMoneyFormat(2);
            litIngresosAnio.Text = (ingresosAnio - ingresosAnioNotas).ToMoneyFormat(2);

            //Saldos
            var fechaVenc = DateTime.Now.AddDays(-60);
            //if (CurrentUser.IDUsuario != 2457)//DAMARFU
            //{
            var porCobrar = dbContext.Database.SqlQuery<RptSaldosViewModel>("exec Rpt_SaldosClientes " + CurrentUser.IDUsuario + ",0", new object[] { }).ToList();

            litPorCobrar.Text = porCobrar.Sum(x => (x.SaldoInicial + x.Saldo + x.CotNC - x.CobranzasACuenta + x.NCSinAplicar)).ToMoneyFormat(2);
            //}
            //.AsEnumerable().Sum(x => x.Saldo).ToMoneyFormat();
            litPorCobrarUrgente.Text = comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.Saldo > 0 && x.FechaVencimiento <= fechaVenc)
                .Select(x => x.Saldo).DefaultIfEmpty(0).Sum().ToMoneyFormat(2);
        }
    }*/

    [WebMethod(true, CacheDuration = 60)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public static ResultadosComprobantesViewModel obtenerFacturasPendientes() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];


        using (var dbContext = new ACHEEntities()) {
            var results = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.Saldo != 0 && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE").AsQueryable();

            ResultadosComprobantesViewModel resultado = new ResultadosComprobantesViewModel();
            resultado.TotalItems = results.Count();

            var list = results.OrderByDescending(x => x.Fecha).Take(5).ToList()
                .Select(x => new ComprobantesViewModel() {
                    ID = x.IDCompra,
                    Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                    RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                    Numero = x.Tipo + " " + x.NroFactura,
                    ImporteTotalNeto = x.Saldo.ToMoneyFormat(2)
                });
            resultado.Items = list.ToList();

            return resultado;
        }

    }

    /*public static ResultadosComprobantesViewModel obtenerVentasPendientes()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var listaComprobantesPagados = new List<Comprobantes>();
                var listaCobranzas = dbContext.CobranzasDetalle
                                .Where(x => x.Comprobantes.IDUsuario == usu.IDUsuario && x.Comprobantes.Tipo != "COT" && x.Comprobantes.Tipo != "NCA" && x.Comprobantes.Tipo != "NCB" && x.Comprobantes.Tipo != "NCC" && x.Comprobantes.Tipo != "NCM" && x.Comprobantes.Tipo != "NCE")
                                .OrderBy(x => x.Comprobantes.FechaVencimiento).ToList();
                foreach (var item in listaCobranzas)
                    listaComprobantesPagados.Add(item.Comprobantes);

                var listaComprobantesNoPagados = dbContext.Comprobantes
                                                .Where(x => x.IDUsuario == usu.IDUsuario && x.Tipo != "COT" && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE")
                                                .OrderBy(x => x.FechaVencimiento).ToList()
                                                .Except(listaComprobantesPagados).ToList();

                //var results = dbContext.Comprobantes.Include("Personas").Include("PuntosDeVenta").Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                ResultadosComprobantesViewModel resultado = new ResultadosComprobantesViewModel();
                resultado.TotalItems = listaComprobantesNoPagados.Count();

                var list = listaComprobantesNoPagados.OrderBy(x => x.FechaComprobante).Take(5).ToList()
                    .Select(x => new ComprobantesViewModel()
                    {
                        ID = x.IDComprobante,
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                        Numero = x.Tipo + " " + x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }*/

    [WebMethod(true, CacheDuration = 30)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static TableHtml obtenerVentas() {
        var ventas = new TableHtml();

        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities()) {
                var result = dbContext.Database.SqlQuery<ChartDecimal>("exec Dashboard_TotalVentas " + usu.IDUsuario, new object[] { }).ToList();
                ventas.uno = result.First(x => x.label == "mesanterior").data.ToMoneyFormat(2);
                ventas.dos = result.First(x => x.label == "mesactual").data.ToMoneyFormat(2);
                ventas.tres = result.First(x => x.label == "full").data.ToMoneyFormat(2);
            }


            /*var mesDesde = DateTime.Now.GetFirstDayOfMonth();
            var mesHasta = DateTime.Now.GetLastDayOfMonth().AddDays(1);

            var mesDesdeAnt = DateTime.Now.AddMonths(-1).GetFirstDayOfMonth();
            var mesHastaAnt = DateTime.Now.AddMonths(-1).GetLastDayOfMonth();

            var anioDesde = DateTime.Now.GetFirstDayOfYear();
            var anioHasta = DateTime.Now.GetLastDayOfYear();

            using (var dbContext = new ACHEEntities())
            {
                var comprobantes = dbContext.Comprobantes.Where(x => x.IDUsuario == usu.IDUsuario && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE").ToList();
                var notas = dbContext.Comprobantes.Where(x => x.IDUsuario == usu.IDUsuario && (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE")).ToList();
                //quitarNC(dbContext, Comprobantes);

                //Ingresos
                decimal ingresosMesAnt = comprobantes.Where(x => x.IDUsuario == usu.IDUsuario
                    && x.FechaComprobante >= mesDesdeAnt && x.FechaComprobante <= mesHastaAnt)
                    .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

                decimal ingresosMesAntNotas = notas.Where(x => x.IDUsuario == usu.IDUsuario
                    && x.FechaComprobante >= mesDesdeAnt && x.FechaComprobante <= mesHastaAnt)
                    .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

                decimal ingresosMes = comprobantes.Where(x => x.IDUsuario == usu.IDUsuario
                    && x.FechaComprobante >= mesDesde && x.FechaComprobante <= mesHasta)
                    .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

                decimal ingresosMesNotas = notas.Where(x => x.IDUsuario == usu.IDUsuario
                    && x.FechaComprobante >= mesDesde && x.FechaComprobante <= mesHasta)
                    .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

                decimal ingresosAnio = comprobantes.Where(x => x.IDUsuario == usu.IDUsuario
                    && x.FechaComprobante >= anioDesde && x.FechaComprobante <= anioHasta)
                    .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

                decimal ingresosAnioNotas = notas.Where(x => x.IDUsuario == usu.IDUsuario
                    && x.FechaComprobante >= anioDesde && x.FechaComprobante <= anioHasta)
                    .Select(x => x.ImporteTotalBruto).DefaultIfEmpty(0).Sum();

                ventas.uno = (ingresosMesAnt - ingresosMesAntNotas).ToMoneyFormat(2);
                ventas.dos = (ingresosMes - ingresosMesNotas).ToMoneyFormat(2);
                ventas.tres = (ingresosAnio - ingresosAnioNotas).ToMoneyFormat(2);
            }*/
        }

        return ventas;
    }

    [WebMethod(true, CacheDuration = 30)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static TableHtml obtenerPendienteCobro() {
        var ventas = new TableHtml();

        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities()) {

                //Saldos
                var fechaVenc = DateTime.Now.AddDays(-60);
                //if (CurrentUser.IDUsuario != 2457)//DAMARFU
                //{
                var porCobrar = dbContext.Database.SqlQuery<RptSaldosViewModel>("exec Rpt_SaldosClientes " + usu.IDUsuario + ",0", new object[] { }).ToList();

                ventas.uno = porCobrar.Sum(x => (x.SaldoInicial + x.Saldo + x.CotNC - x.CobranzasACuenta + x.NCSinAplicar)).ToMoneyFormat(2);
                //}
                //.AsEnumerable().Sum(x => x.Saldo).ToMoneyFormat();
                ventas.dos = dbContext.Comprobantes.Where(x => x.IDUsuario == usu.IDUsuario
                        && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE"
                        && x.Saldo > 0 && x.FechaVencimiento <= fechaVenc)
                        .Select(x => x.Saldo).DefaultIfEmpty(0).Sum().ToMoneyFormat(2);
            }
        }

        return ventas;
    }

    /*** VENTAS VS COMPRAS ***/
    #region

    [WebMethod(true, CacheDuration = 60)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static DashboardViewModel obtenerVentasVsCompras() {
        /*
        List<ChartYXZ> listaCompras = new List<ChartYXZ>();
        List<ChartYXZ> listaComprobantes = new List<ChartYXZ>();

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            DateTime fechaDesde = new DateTime();
            DateTime fechaHasta = new DateTime();

            using (var dbContext = new ACHEEntities())
            {
                fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth();
                fechaHasta = DateTime.Now.GetLastDayOfYear();

                var fechaMax = DateTime.Now.AddMonths(-3);

                var tipo = "DIA";
                if (dbContext.Comprobantes.Any(x => x.IDUsuario == usu.IDUsuario && x.FechaComprobante < fechaMax))
                    tipo = "MES";

                var lista = dbContext.Comprobantes.Where(x => x.IDUsuario == usu.IDUsuario
                                                                && x.FechaComprobante >= fechaDesde && DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(fechaHasta))
                                                         .OrderBy(x => x.FechaComprobante).AsQueryable();

                var aux = lista.ToList();
                var aux2 = lista.Where(x => x.FechaComprobante.Year == 2018 && x.FechaComprobante.Month == 2).Select(x=>x.IDComprobante).ToList();
                var listafechas = new List<CharFacturacion>();
                if (tipo == "DIA")
                {
                    listafechas = lista.GroupBy(x => new { x.FechaComprobante.Month, x.FechaComprobante.Year, x.FechaComprobante.Day }).ToList().Select(x => new CharFacturacion()
                    {
                        ImporteTotal = x.Sum(y => (y.Tipo != "NCA" && y.Tipo != "NCB" && y.Tipo != "NCC" && y.Tipo != "NCM" && y.Tipo != "NCE" ? y.ImporteTotalBruto : y.ImporteTotalBruto * -1)),
                        Fecha = x.Select(y => y.FechaComprobante.ToString("yyyy-MM-dd")).FirstOrDefault()
                    }).ToList();
                }
                else
                {
                    listafechas = lista.GroupBy(x => new { x.FechaComprobante.Month, x.FechaComprobante.Year }).ToList().Select(x => new CharFacturacion()
                    {
                        ImporteTotal = x.Sum(y => (y.Tipo != "NCA" && y.Tipo != "NCB" && y.Tipo != "NCC" && y.Tipo != "NCM" && y.Tipo != "NCE" ? y.ImporteTotalBruto : y.ImporteTotalBruto * -1)),
                        Fecha = x.Select(y => y.FechaComprobante.ToString("yyyy-MM")).FirstOrDefault()
                    }).ToList();
                }


                foreach (var item in listafechas)
                {
                    ChartYXZ chart = new ChartYXZ();
                    chart.Fecha = item.Fecha;
                    chart.Uno = item.ImporteTotal;
                    listaComprobantes.Add(chart);
                }

                //quitarNC(dbContext, Comprobantes, fechaDesde, fechaHasta);

                //var tipo = (Comprobantes.Any(x => x.FechaComprobante.Date < DateTime.Now.Date.AddMonths(-3))) ? "MES" : "DIA";

                //AgruparComprobantes(listaComprobantes, Comprobantes, tipo);
                var Compras = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.Fecha >= fechaDesde 
                    && DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(fechaHasta)).OrderBy(x => x.Fecha).ToList();
                AgruparCompras(listaCompras, Compras, tipo);
            }     //   d.Items = JoinResultados(listaCompras, listaComprobantes);

        }  
         * */
        DashboardViewModel d = new DashboardViewModel();

        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];


            var fechaDesde = DateTime.Now.AddMonths(-12).GetFirstDayOfMonth().ToString(formato);
            var fechaHasta = DateTime.Now.GetLastDayOfYear().ToString(formato) + " 23:59:59";
            var fechaMax = DateTime.Now.AddMonths(-3);
            var tipo = "DIA";

            using (var dbContext = new ACHEEntities()) {
                if (dbContext.Comprobantes.Any(x => x.IDUsuario == usu.IDUsuario && x.FechaComprobante < fechaMax))
                    tipo = "MES";
                try {

                    d.Items = dbContext.Database.SqlQuery<ChartYXZ>("exec RptVentasVSCompras '" + tipo + "','" + fechaDesde + "','" + fechaHasta + "'," + usu.IDUsuario, new object[] { }).ToList();
                }
                catch (Exception e) {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
        }
        return d;

    }

    #endregion
}
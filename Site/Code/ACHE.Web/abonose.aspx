﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="abonose.aspx.cs" Inherits="abonose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-tasks"></i>Abonos </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/abonos.aspx">Abonos</a></li>
                <li class="active">
                    <asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Tipo de Abono</h3>
                                <label class="control-label">Ingrese el tipo de abono que desea crear.</label>
                            </div>
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Tipo de Abono</label>
                                    <asp:DropDownList runat="server" ID="ddlTipoAbono" CssClass="form-control required">
                                        <asp:ListItem Value="0" Text="Único - Un sólo concepto"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Detallado - Con diferentes conceptos"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Datos generales</h3>
                                <label class="control-label">Ingrese los datos más generales para crear un abono.</label>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Frecuencia</label>
                                    <asp:DropDownList runat="server" ID="ddlFrecuencia" CssClass="form-control" onchange="changeTipo(true);">
                                        <asp:ListItem Value="A">Anual</asp:ListItem>
                                        <asp:ListItem Value="S">Semestral</asp:ListItem>
                                        <asp:ListItem Value="T">Trimestral</asp:ListItem>
                                        <asp:ListItem Value="M">Mensual</asp:ListItem>
                                        <asp:ListItem Value="Q">Quincenal</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Nombre</label>
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="100"></asp:TextBox>

                                </div>
                            </div>

                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Producto o Servicio</label>
                                    <asp:DropDownList runat="server" ID="ddlProducto" CssClass="form-control required">
                                        <asp:ListItem Value="1" Text="Producto"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Servicio"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Producto y servicio"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Condición de Venta</label>
                                    <select class="form-control required" id="ddlCondicionVenta">
                                          <!--      <option value="Efectivo">Efectivo</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="Cuenta corriente">Cuenta corriente</option>
                                                <option value="MercadoPago">MercadoPago</option>
                                                <option value="Tarjeta de debito">Tarjeta de debito</option>
                                                <option value="Tarjeta de credito">Tarjeta de credito</option>
                                                <option value="Ticket">Ticket</option>
                                                 <option value="PayU">PayU</option>
                                                <option value="Otro">Otro</option>-->
                                            </select>
                                </div>
                            </div>
                        </div>

                        <div id="divPlanDeCuentas" class="row mb15">
                            <div class="col-sm-4 col-md-2 divPlanDeCuentas">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Cuenta contable</label>
                                    <asp:DropDownList runat="server" ID="ddlPlanDeCuentas" CssClass="form-control required" data-placeholder="Seleccione una cuenta...">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Período del abono</h3>
                                <label class="control-label">Ingrese el período del abono.</label>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Fecha Inicio</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        <asp:TextBox runat="server" ID="txtFechaInicio" CssClass="form-control required validDate greaterThan validFechaActual" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Fecha Fin</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        <asp:TextBox runat="server" ID="txtFechaFin" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Estado</label>
                                    <asp:DropDownList runat="server" ID="ddlEstado" CssClass="form-control required">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Activo" Value="A" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Inactivo" Value="I"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Items a facturar</h3>
                                <label class="control-label">Agrega a continuación los productos o servicios que vas a incluir en la factura.</label>
                            </div>
                        </div>

                        <div class="row mb15" runat="server" id="divAbonoUnico">
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Precio unitario</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <asp:TextBox runat="server" ID="txtPrecio" CssClass="form-control required number" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> IVA %</label>
                                    <asp:DropDownList runat="server" ID="ddlIva" CssClass="form-control">
                                        <asp:ListItem Value="0,00">0</asp:ListItem>
                                        <asp:ListItem Value="2,50">2,5</asp:ListItem>
                                        <asp:ListItem Value="5,00">5</asp:ListItem>
                                        <asp:ListItem Value="10,50">10,5</asp:ListItem>
                                        <asp:ListItem Value="21,00">21</asp:ListItem>
                                        <asp:ListItem Value="27,00">27</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-2">
                                <h5 class="subtitle mb10">PRECIO CON IVA</h5>
                                <h4 class="text-primary" style="color: green" id="divPrecioIVA">$ <asp:Literal runat="server" ID="litTotal"></asp:Literal></h4>
                            </div>
                        </div>

                        <div id="divAbonoDetalle" runat="server">
                            <div class="well">
                                <div class="alert alert-danger" id="divErrorDetalle" style="display: none">
                                    <strong>Lo siento! </strong><span id="msgErrorDetalle"></span>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                        <span class="asterisk">*</span> Cantidad
                                    <input type="text" class="form-control" maxlength="10" id="txtCantidad" />
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                                        <span class="asterisk">*</span> Seleccione un Producto/Servicio
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <select class="select3" data-placeholder="Seleccione un Producto/Servicio/Concepto" id="ddlProductos" onchange="changeConcepto();">
                                            </select>
                                        </div>
                                        <%--<div class="col-lg-6 col-lg-6">
                                            <input type="text" class="form-control" maxlength="200" id="txtConcepto" />
                                        </div>--%>
                                    </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                                        o ingrese una descripción libre
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <input type="text" class="form-control" maxlength="200" id="txtConcepto" />
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                        <span class="asterisk">*</span>
                                        <asp:Literal ID="liPrecioUnitario" runat="server"></asp:Literal>
                                        <input type="text" class="form-control" maxlength="10" id="txtPrecioDet" />
                                    </div>
                                    <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                        Bonif %
                                    <input type="text" class="form-control" maxlength="6" id="txtBonificacion" />
                                    </div>
                                    <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                        <span class="asterisk">*</span> IVA %
                                    <select class="select2" id="ddlIvaDet">
                                        <option value="0,00">0</option>
                                        <option value="2,50">2,5</option>
                                        <option value="5,00">5</option>
                                        <option value="10,50">10,5</option>
                                        <option value="21,00">21</option>
                                        <option value="27,00">27</option>
                                    </select>
                                    </div>
                                    <div class="col-xs-12 col-md-4 col-lg-2 form-group divPlanDeCuentas">
                                        <div class="form-group">
                                            <span class="asterisk">*</span> Cuenta contable
                                        <asp:DropDownList runat="server" ID="ddlPlanDeCuentas2" CssClass="select3" data-placeholder="Seleccione una cuenta...">
                                        </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-12 col-lg-12 form-group">
                                        <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="agregarItem();">Agregar</a>
                                        <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="cancelarItem();">Cancelar</a>
                                        <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-invoice">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th style="text-align: right">Cantidad</th>
                                            <th style="text-align: left">Código del Producto</th>
                                            <th style="text-align: left">Concepto</th>
                                            <th style="text-align: right" id="tdPrecio">Precio unitario sin IVA</th>
                                            <th style="text-align: right">Bonificación %</th>
                                            <th style="text-align: right">IVA %</th>
                                            <th class="divPlanDeCuentas" style="text-align: left">Cuenta</th>
                                            <th style="text-align: right">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodyDetalle">
                                    </tbody>
                                </table>
                                <div class="col-xs-12 col-sm-5"></div>
                                <div class="col-sm-12 col-sm-7">
                                    <table class="table table-total">
                                        <tbody>
                                            <tr id="trImpNoGrabado" class="hide">
                                                <td><strong>Neto no gravado :</strong></td>
                                                <%--<td id="trImpNoGrabadoTotal">$ 0</td>--%>
                                                <td id="Td1">
                                                    <asp:TextBox runat="server" ID="txtImporteNoGrabado" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static" Text="0" onBlur="obtenerTotales();"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Neto gravado :</strong></td>
                                                <td id="divSubtotal">$ 0</td>
                                            </tr>
                                            <tr>
                                                <td><strong>IVA :</strong></td>
                                                <td id="divIVA">$ 0</td>
                                            </tr>

                                            <tr id="trIVA">
                                                <td><strong>Percepción IVA :</strong></td>
                                                <td id="trIVATotal">$ 0</td>
                                                <%--<asp:TextBox runat="server" ID="txtPercepcionIVA" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static"></asp:TextBox></td>--%>
                                            </tr>
                                            <tr id="trIIBB">
                                                <td><strong>Percepción IIBB :</strong></td>
                                                <td id="trIIBBTotal">$ 0</td>
                                                <%--<asp:TextBox runat="server" ID="txtPercepcionIIBB" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static"></asp:TextBox></td>--%>
                                            </tr>

                                            <tr>
                                                <td><strong>TOTAL :</strong></td>
                                                <td id="divTotal" style="color: green">$ 0</td>
                                                <asp:HiddenField runat="server" ID="hdnTotal" Value="0" />
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Clientes</h3>
                                <label class="control-label">Seleccione los clientes a los cuales le va a facturar el abono.</label>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">&nbsp;</label>
                                    <select class="select2" data-placeholder="Seleccione 1 o más clientes/proveedores..." id="ddlPersona">
                                    </select>
                                    <asp:HiddenField runat="server" ID="hdnPersonasID" ClientIDMode="Static" />

                                    <div class="" id="divErrorClientes" style="display: none; margin-top: 20px; color: #a94442;">
                                        <strong>Lo sentimos! </strong><span id="msgErrorClientes"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#modalNuevoCliente" style="margin-top: 29px; padding: 7px 15px">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                    <a class="btn btn-success" style="margin-top: 29px; margin-left: 10px; padding: 7px 15px" onclick="cargarPersona()">agregar al abono</a>
                                </div>
                            </div>

                        </div>

                        <div class="row mb15" style="margin-left: -30px">
                            <div class="col-sm-6">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table mb30">
                                            <thead>
                                                <tr>
                                                    <th>Razón social</th>
                                                    <th>Cantidad</th>
                                                    <th>Total</th>
                                                    <th class="columnIcons"></th>
                                                </tr>
                                            </thead>
                                            <tbody id="resultsContainer">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <hr />
                                <h3>Datos complementarios </h3>
                                <label class="control-label">Estos datos no son necesarios para crear un abono, pero podrían serte de utilidad ;-)</label>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Observaciones</label>
                                    <asp:TextBox runat="server" TextMode="MultiLine" Rows="5" ID="txtObservaciones" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-success" id="btnActualizar" onclick="confirmarGrabar();">Aceptar</a>
                        <a href="#" onclick="cancelar();" style="margin-left: 20px">Cancelar</a>
                    </div>
                </div>
                                    <asp:HiddenField runat="server" ID="hdnIDCondicionVenta" Value="0" />

                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnDuplicar" Value="0" />
                <asp:HiddenField runat="server" ID="hdnUsaPlanCorporativo" Value="0" />
                <asp:HiddenField runat="server" ID="hdnCodigo" Value="" />
                <asp:HiddenField runat="server" ID="hdnTieneSuscripcionesVinculadas" Value="0" />

            </form>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
            <tr>
                <td>${RazonSocial}</td>
                <td>
                    <input runat="server" class="form-control ListaClientes" maxlength="4" clientidmode="Static" idpersonascantidad="${IDPersona}" value="${Cantidad}" onchange="recalcularTotal(${IDPersona},this.value);" />
                </td>
                <td>${Total}</td>
                <td class="table-action">
                    <a onclick="eliminarCliente(${IDPersona});" style="cursor: pointer; font-size: 16px" class="delete-row" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                </td>

            </tr>
        {{/each}}
    </script>
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="3">No se han encontrado resultados</td>
        </tr>
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="js/numeral.min.js"></script>
    <script src="/js/views/abonos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <UC:NuevaPersona runat="server" ID="ucCliente" />
    <script>
        jQuery(document).ready(function () {
            configForm();
        });
    </script>
</asp:Content>

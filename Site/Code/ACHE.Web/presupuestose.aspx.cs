﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Negocio.Productos;
using ACHE.Model.Negocio;
using ACHE.Negocio.Presupuesto;
using ACHE.FacturaElectronica;
using ACHE.FacturaElectronicaModelo;
using ACHE.Negocio.Common;
using ACHE.Negocio.Ventas;

public partial class presupuestose : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            litPath.Text = "Alta";
            ComprobanteCart.Retrieve().Items.Clear();

            txtFechaEmision.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFechaValidez.Text = DateTime.Now.AddDays(30).ToString("dd/MM/yyyy");

            if (CurrentUser.UsaPrecioFinalConIVA)
                liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Precio Unit. con IVA</span> ";
            else
                liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Precio Unit. sin IVA</span> ";

            hdnUsaPrecioConIVA.Value = (CurrentUser.UsaPrecioFinalConIVA) ? "1" : "0";

            litUsuarioAlta.Text = CurrentUser.Email;
            bool duplicar = false;
            if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];

                //Duplicar presupuesto
                if (!String.IsNullOrEmpty(Request.QueryString["Duplicar"])) {
                    duplicar = true;
                }


                if (hdnID.Value != "0") {
                    cargarEntidad(int.Parse(hdnID.Value), duplicar);
                    if (duplicar) {
                        litPath.Text = "Alta";
                        hdnID.Value = "0";
                    }
                    else {
                        litPath.Text = "Edición";
                    }
                }
            }
            else
                txtObservaciones.Text = CurrentUser.ObservacionesPresupuestos;

        }
    }

    private void cargarEntidad(int id, bool duplicar) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Presupuestos.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPresupuesto == id).FirstOrDefault();
            if (entity != null) {
                hdnIDPersona.Value = entity.IDPersona.ToString();

                txtFechaValidez.Text = entity.FechaValidez.ToString("dd/MM/yyyy");
                txtFechaEmision.Text = entity.FechaAlta.ToString("dd/MM/yyyy");
                ddlEstado.SelectedValue = entity.Estado;
                txtNombre.Text = entity.Nombre.ToUpper();
                // ddlFormaPago.SelectedValue = entity.FormaDePago.ToString();
                hdnIDCondicionVenta.Value = entity.IDCondicionVenta.ToString();
                txtObservaciones.Text = entity.Observaciones;
                ddlProducto.SelectedValue = entity.Tipo.ToString();

                litUsuarioAlta.Text = entity.UsuarioAlta;
                litUsuarioModifica.Text = entity.UsuarioModifica;

                if (duplicar) {
                    int nro = dbContext.Presupuestos.Where(x => x.IDUsuario == CurrentUser.IDUsuario).Max(x => x.Numero) + 1;
                    txtNombre.Text = nro.ToString("#00000000");
                }
                else
                    txtNumero.Text = entity.Numero.ToString("#00000000");


                foreach (var det in entity.PresupuestoDetalle) {
                    var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                    tra.ID = ComprobanteCart.Retrieve().Items.Count() + 1;
                    tra.Codigo = (det.Conceptos != null) ? det.Conceptos.Codigo : "";
                    tra.Concepto = det.Concepto;
                    tra.Iva = det.Iva;

                    if (CurrentUser.UsaPrecioFinalConIVA)
                        tra.PrecioUnitario = ConceptosCommon.ObtenerPrecioFinal(det.PrecioUnitario, det.Iva.ToString(), ConfiguracionHelper.ObtenerCantidadDecimales());
                    else
                        tra.PrecioUnitario = det.PrecioUnitario;

                    tra.Bonificacion = det.Bonificacion;
                    tra.Cantidad = det.Cantidad;
                    tra.IDConcepto = det.IDConcepto;

                    ComprobanteCart.Retrieve().Items.Add(tra);
                }

            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static int guardar(int id, int idPersona, string fechaAlta, string fecha, string nombre, int numero, int tipo, string condicionesPago, string obs, string estado) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        PresupuestoCartDto comprobanteCart = new PresupuestoCartDto();
        comprobanteCart.Items = ComprobanteCart.Retrieve().Items.ToList();

        comprobanteCart.IDPresupuesto = id;
        comprobanteCart.IDPersona = idPersona;
        comprobanteCart.Fecha = fechaAlta;
        comprobanteCart.FechaValidez = fecha;
        comprobanteCart.Nombre = nombre;
        comprobanteCart.Numero = numero;
        comprobanteCart.Tipo = tipo;
        comprobanteCart.CondicionesPago = "";
        comprobanteCart.IDCondicionVenta = int.Parse(condicionesPago);
        comprobanteCart.Observaciones = obs;
        comprobanteCart.Estado = estado;

        return PresupuestosCommon.GuardarPresupuesto(comprobanteCart, usu.IDUsuario, usu.Email);
    }

    /*** ITEM ***/
    [WebMethod(true)]
    public static void agregarItem(int id, string idConcepto, string concepto, string iva, string precio, string bonif, string cantidad, int idPersona, string codigo) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        PresupuestoItemDto itemDto = new PresupuestoItemDto();
        itemDto.ID = id;
        itemDto.IDConcepto = idConcepto;
        itemDto.Concepto = concepto;
        itemDto.Iva = iva;
        itemDto.Precio = precio;
        itemDto.Bonificacion = bonif;
        itemDto.Cantidad = cantidad;
        itemDto.IDPersona = idPersona;

        PresupuestosCommon.AgregarItem(itemDto, usu, ConfiguracionHelper.ObtenerCantidadDecimales());

    }

    [WebMethod(true)]
    public static void eliminarItem(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        PresupuestosCommon.EliminarItem(id);

    }

    [WebMethod(true)]
    public static TotalesViewModel obtenerTotales() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        return PresupuestosCommon.ObtenerTotales(ConfiguracionHelper.ObtenerCantidadDecimales());

    }

    [WebMethod(true)]
    public static string obtenerItems(int idPersona) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return PresupuestosCommon.ObtenerItems(ConfiguracionHelper.ObtenerCantidadDecimales(), usu.UsaPrecioFinalConIVA);

    }

    [WebMethod(true)]
    public static string previsualizar(int id, int idPersona, string fecha, string fechaVencimiento, string nombre, int numero, string condicionesPago, string obs) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        PresupuestoPrevisualizarDto pre = new PresupuestoPrevisualizarDto();
        pre.ID = id;
        pre.IDPersona = idPersona;
        pre.Fecha = fecha;
        pre.FechaVencimiento = fechaVencimiento;
        pre.Nombre = nombre;
        pre.Numero = numero;
        pre.CondicionesPago = condicionesPago;
        pre.Observaciones = obs;

        return PresupuestosCommon.Previsualizar(pre, usu);

    }

    [WebMethod(true)]
    public static string GenerarRemito(int id)//Comprobantes entity, string fileNameRemito
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return PresupuestosCommon.GenerarRemito(usu, id);

    }

    [WebMethod(true)]
    public static string generarPresupuesto(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return PresupuestosCommon.GenerarPresupuesto(id, usu);

    }


}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Abono;
using ACHE.Negocio.Common;

public partial class abonos : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            using (var dbContext = new ACHEEntities()) {
                var TieneDatos = dbContext.Abonos.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos) {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
             CurrentUser.TipoUsuario);
        }
    }


    [System.Web.Services.WebMethod(true)]
    public static void delete(int id) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            AbonosCommon.BorrarAbono(usu, id);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosAbonosViewModel getResults(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return AbonosCommon.ObtenerAbonos(usu, condicion, periodo, fechaDesde, fechaHasta, page, pageSize);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(string condicion, string periodo, string fechaDesde, string fechaHasta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        try {
            return AbonosCommon.Exportar(usu, condicion, periodo, fechaDesde, fechaHasta);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }

    }
}
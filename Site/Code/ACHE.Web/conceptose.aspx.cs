﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Common;

public partial class conceptose : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            DetalleCart.Retrieve().Items.Clear();
            litPath.Text = "Alta";
            hdnIdUsuario.Value = CurrentUser.IDUsuario.ToString();
            hdnUsaPrecioFinalConIVA.Value = (CurrentUser.UsaPrecioFinalConIVA) ? "1" : "0";

            if (CurrentUser.CondicionIVA == "MO") {
                ddlIva.Enabled = false;
                ddlIva.SelectedValue = "0,00";
            }
            else if (CurrentUser.CondicionIVA == "RI")
                ddlIva.SelectedValue = "21,00";

            if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0") {
                    var duplicado = (!String.IsNullOrEmpty(Request.QueryString["Duplicar"])) ? Request.QueryString["Duplicar"] : "0";
                    cargarEntidad(int.Parse(hdnID.Value), duplicado);
                    if (duplicado == "1") {
                        litPath.Text = "Alta";
                        hdnID.Value = "0";
                    }
                    else {
                        litPath.Text = "Edición";
                    }
                }
            }

            if (CurrentUser.UsaPrecioFinalConIVA) {
                liPrecioUnitario.Text = " Precio unit. con IVA";
                liPrecioTotal.Text = " PRECIO SIN IVA";
            }
            else {
                liPrecioUnitario.Text = " Precio unit. sin IVA";
                liPrecioTotal.Text = " PRECIO CON IVA";
            }
            if (CurrentUser.UsaPlanCorporativo) {
                CargarCuentasActivo();
            }
        }
    }

    private void CargarCuentasActivo() {
        using (var dbContext = new ACHEEntities()) {
            if (CurrentUser.UsaPlanCorporativo) //Plan Corporativo
            {
                if (dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == CurrentUser.IDUsuario)) {
                    //TODO: Puede ser de ventas o compras.
                    var idctas = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault().CtasFiltroCompras.Split(',');
                    List<Int32> cuentascompras = new List<Int32>();
                    for (int i = 0; i < idctas.Length; i++) {
                        if (idctas[i] != string.Empty)
                            cuentascompras.Add(Convert.ToInt32(idctas[i]));
                    }

                    var listaAuxCompras = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && cuentascompras.Contains(x.IDPlanDeCuenta)).OrderBy(x => x.Codigo).ToList();
                    ddlPlanDeCuentasCompra.Items.Add(new ListItem("", ""));

                    foreach (var item in listaAuxCompras) {
                        ddlPlanDeCuentasCompra.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));
                    }


                    //TODO: Puede ser de ventas o compras.
                    var idvtas = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault().CtasFiltroVentas.Split(',');
                    List<Int32> cuentasventas = new List<Int32>();
                    for (int i = 0; i < idvtas.Length; i++) {
                        if (idvtas[i] != string.Empty)
                            cuentasventas.Add(Convert.ToInt32(idvtas[i]));
                    }

                    var listaAuxVentas = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && cuentasventas.Contains(x.IDPlanDeCuenta)).OrderBy(x => x.Codigo).ToList();
                    ddlPlanDeCuentasVentas.Items.Add(new ListItem("", ""));

                    foreach (var item in listaAuxVentas) {
                        ddlPlanDeCuentasVentas.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));
                    }
                }
                hdnPlanCorporativo.Value = "1";
            }
        }
    }

    private void cargarEntidad(int id, string duplicado) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Conceptos.Include("ComboConceptos").Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDConcepto == id).FirstOrDefault();
            if (entity != null) {
                ddlTipo.SelectedValue = entity.Tipo;
                txtNombre.Text = entity.Nombre.ToUpper();
                txtCodigo.Text = (duplicado == "0") ? entity.Codigo.ToUpper() : "";
                txtCodigoOem.Text = entity.CodigoOEM ?? "";
                ddlEstado.SelectedValue = entity.Estado;
                txtDescripcion.Text = entity.Descripcion;
                //txtStock.Text = entity.Stock.ToString().Replace(",", ".");
                txtRentabilidad.Text = entity.Rentabilidad.ToString().Replace(",", ".");
                txtPrecio.Text = entity.PrecioUnitario.ToString().Replace(",", ".");
                txtCodigoDeBarra.Text = entity.CodigoBarras;
                ddlIva.SelectedValue = entity.Iva.ToString("#0.00");
                txtObservaciones.Text = entity.Observaciones;
                txtCostoInterno.Text = entity.CostoInterno.HasValue ? entity.CostoInterno.ToString().Replace(",", ".") : "";
                hdnIDPersona.Value = Convert.ToInt32(entity.IDPersona).ToString();
                hdnIDSubRubro.Value = entity.IDSubRubro.HasValue ? entity.IDSubRubro.Value.ToString() : "0";
                hdnIDRubro.Value = entity.IDRubro.ToString();
                hdnIDCuentaCompras.Value = entity.IDPlanDeCuentaCompras.ToString();
                hdnIDCuentaVentas.Value = entity.IDPlanDeCuentaVentas.ToString();
                ddlPrecioAutomatico.SelectedValue = entity.PrecioAutomatico ? "Si" : "No";
                //var aux = ConceptosCommon.ObtenerStockTotal(entity);
                //items combo
                
                var total = entity.PrecioUnitario;
                if (CurrentUser.UsaPrecioFinalConIVA) {
                    litTotal.Text = Math.Round(ConceptosCommon.ObtenerPrecioFinal(total, entity.Iva.ToString("00.00"), ConfiguracionHelper.ObtenerCantidadDecimales()), ConfiguracionHelper.ObtenerCantidadDecimales()).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    hdnTotal.Value = litTotal.Text.Replace(",", ".");
                }
                else {
                    litTotal.Text = (total + ((total * entity.Iva) / 100)).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    hdnTotal.Value = txtPrecio.Text.Replace(",", ".");
                }

                if (entity.StockMinimo.HasValue)
                    txtStockMinimo.Text = entity.StockMinimo.Value.ToString();

                if (!string.IsNullOrWhiteSpace(entity.Foto)) {
                    imgFoto.Src = "/files/explorer/" + CurrentUser.IDUsuario.ToString() + "/Productos-Servicios/" + entity.Foto;
                    hdnTieneFoto.Value = "1";
                }
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static int guardar(int id, string nombre, string codigo, string tipo, string descripcion, string estado, string precio, 
        string iva, string stock, string obs, string constoInterno, string stockMinimo, string codigoBarra, string codigoOem, int idPersona, 
        string rentabilidad, string rubro, string subRubro, string idPlanDeCuentaCompra, string idPlanDeCuentaVenta,string precioAutomatico) {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        if (tipo == "C" && DetalleCart.Retrieve().Items.Count == 0)
            throw new CustomException("No puede generar un combo sin productos/servicios que lo compongan.");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        bool precioAutomaticoBool = (precioAutomatico == "Si" && tipo == "C") ? true : false;
        return ConceptosCommon.GuardarConcepto(id, nombre, codigo, tipo, descripcion, estado, precio, iva, stock, obs, constoInterno, stockMinimo, codigoOem, idPersona, rentabilidad, usu.IDUsuario, codigoBarra, rubro, subRubro, idPlanDeCuentaCompra, idPlanDeCuentaVenta,precioAutomaticoBool);

    }

    [WebMethod(true)]
    public static ConceptosViewModel obtenerDatos(int id, int idPersona) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return ConceptosCommon.ObtenerDatos(usu.IDUsuario, id, idPersona);

    }

    [WebMethod(true)]
    public static ConceptosViewModel obtenerDatosParaCompras(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return ConceptosCommon.ObtenerDatosParaCompras(usu.IDUsuario, id);

    }

    [WebMethod(true)]
    public static void eliminarFoto(int idConcepto) {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        ConceptosCommon.EliminarFoto(usu, idConcepto);


    }
}
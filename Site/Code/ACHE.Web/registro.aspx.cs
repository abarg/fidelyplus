﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Specialized;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Common;
using System.Collections.Generic;
using ACHE.Model.Negocio;

public partial class registro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request.QueryString["from"]))
            txtCodigoPromocion.Text = Request.QueryString["from"];

        if (ConfigurationManager.AppSettings["Config.MarcaBlanca"] != "")
            imgLogo.Src = "/images/" + ConfigurationManager.AppSettings["Config.MarcaBlanca"] + "/logo-login.png";

        //var email = Request.QueryString["email"];
        //var empresa = Request.QueryString["empresa"];
        //var cuit = Request.QueryString["cuit"];

        //if (!string.IsNullOrEmpty(nombre) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(empresa) && !string.IsNullOrEmpty(cuit))
        //{
        //    //Response.Redirect("http://www.google.com.ar");
        //    HttpContext.Current.Response.Status = "301 Moved Permanently";
        //    HttpContext.Current.Response.AddHeader("Location", "http://www.google.com.ar");
        //}
    }

    [WebMethod(true)]
    public static void guardar(string nombre, string email, string pwd, string telefono, string codigoPromocion, string actividad)
    {
        UsuarioDto usu = new UsuarioDto();
        usu.RazonSocial = nombre;
        usu.Email = email;
        usu.Pwd = pwd;
        usu.Telefono = telefono;
        usu.CodigoPromocion = codigoPromocion;
        usu.Actividad = actividad;
        if (codigoPromocion.ToLower() == "egoforum")
            usu.Descuento = "15";
        UsuarioCommon.Registrar(usu);
        /*
                using (var dbContext = new ACHEEntities())
                {
                    if (esMailFalso(email.ToLower()))//mails temporarios
                        throw new Exception("El E-mail ingresado es inválido.");
                    if (dbContext.Usuarios.Any(x => x.Email == email))
                        throw new Exception("El E-mail ingresado ya se encuentra registrado.");
                    //else if (dbContext.Usuarios.Any(x => x.CUIT == cuit))
                    //    throw new Exception("El CUIT ingresado ya se encuentra registrado.");
                    //if (!cuit.IsValidCUIT())
                    //    throw new Exception("El CUIT es inválido.");
                    else
                    {
                        Usuarios entity = new Usuarios();

                        entity.RazonSocial = nombre; //razonSocial;
                        entity.EmailFrom = nombre;
                        entity.FechaInicioActividades = null;
                        entity.CondicionIva = ""; //condicionIva;
                        entity.CUIT = "";// cuit;
                        entity.IIBB = string.Empty;
                        entity.Personeria = ""; //personeria;
                        entity.Email = email;
                        entity.EmailAlertas = email;
                        entity.EmailStock = email;
                        entity.Pwd = pwd;
                        entity.Telefono = telefono;
                        entity.Celular = string.Empty;
                        entity.Contacto = string.Empty;
                        entity.Actividad = actividad;

                        //Domicilio
                        entity.Pais = "Argentina";
                        entity.IDProvincia = 2;//Ciudad de Buenos Aires
                        entity.IDCiudad = 5003;//SIN IDENTIFICAR
                        entity.Domicilio = string.Empty;
                        entity.PisoDepto = string.Empty;
                        entity.CodigoPostal = string.Empty;

                        entity.Theme = "default";
                        entity.TemplateFc = "default";
                        entity.FechaAlta = DateTime.Now;
                        entity.FechaUltLogin = DateTime.Now;
                        entity.TieneFacturaElectronica = false;
                        entity.Logo = null;
                        entity.Activo = true;
                        entity.IDPlan = 4;
                        entity.FechaFinPlan = DateTime.Now.AddDays(8);
                        entity.UsaFechaFinPlan = true;
                        entity.SetupRealizado = false;
                        entity.UsaProd = false;

                        entity.IDUsuarioPadre = null;
                        entity.CodigoPromo = codigoPromocion;
                        entity.PorcentajeDescuento = 0;

                        entity.ApiKey = Guid.NewGuid().ToString().Replace("-", "");

                        entity.EsAgentePercepcionIVA = false;
                        entity.EsAgentePercepcionIIBB = false;
                        entity.IDJurisdiccion = entity.IDProvincia.ToString();
                        entity.EsAgenteRetencion = false;
                        entity.CantidadEmpresas = 2;
                        entity.ExentoIIBB = false;
                        entity.UsaPrecioFinalConIVA = true;
                        entity.EnvioAutomaticoComprobante = false;
                        entity.EnvioAutomaticoRecibo = false;
                        entity.EsContador = false;
                        entity.UsaPlanCorporativo = false;
                        entity.CantidadDecimales = 2;
                        entity.MostrarBonificacionFc = true;
                        entity.VenderSinStock = true;

                        try
                        {
                            PlanesPagos p = new PlanesPagos();
                            p.IDUsuario = entity.IDUsuario;
                            p.IDPlan = 6;
                            p.ImportePagado = 0;
                            p.PagoAnual = false;
                            p.FormaDePago = "-";
                            p.NroReferencia = "";
                            p.Estado = "Aceptado";
                            p.FechaDeAlta = DateTime.Now.Date;
                            p.FechaDePago = DateTime.Now.Date;
                            p.FechaInicioPlan = p.FechaDePago;
                            p.FechaFinPlan = DateTime.Now.AddDays(int.Parse(ConfigurationManager.AppSettings["Registro.DiasPrueba"]));

                            dbContext.PlanesPagos.Add(p);

                            PuntosDeVenta punto = new PuntosDeVenta();
                            punto.FechaAlta = DateTime.Now;
                            punto.Punto = 1;
                            punto.PorDefecto = true;
                            entity.PuntosDeVenta.Add(punto);

                            Inventarios inventario = new Inventarios();
                            inventario.IDPuntoVenta = punto.IDPuntoVenta;
                            inventario.FechaAlta = DateTime.Now;
                            inventario.Nombre = "Principal";
                            inventario.IDProvincia = 2;
                            inventario.IDCiudad = 5003;
                            inventario.Direccion = string.Empty;
                            inventario.Telefono = telefono;
                            inventario.FechaUltimoInventarioFisico = DateTime.Now;
                            inventario.Activo = true;
                            entity.Inventarios.Add(inventario);

                            dbContext.Inventarios.Add(inventario);

                    

                            //if (cuit.StartsWith("30"))
                            //{
                            //    punto = new PuntosDeVenta();
                            //    punto.FechaAlta = DateTime.Now;
                            //    punto.Punto = 2;
                            //    punto.PorDefecto = false;
                            //    entity.PuntosDeVenta.Add(punto);
                            //}

                            Categorias cat = new Categorias();
                            cat.Nombre = "General";
                            entity.Categorias.Add(cat);

                            Categorias cat2 = new Categorias();
                            cat2.Nombre = "Honorarios";
                            entity.Categorias.Add(cat2);

                            Categorias cat3 = new Categorias();
                            cat3.Nombre = "Gastos varios";
                            entity.Categorias.Add(cat3);

                            Categorias cat4 = new Categorias();
                            cat4.Nombre = "Nafta";
                            entity.Categorias.Add(cat4);

                            Categorias cat5 = new Categorias();
                            cat5.Nombre = "Equipamiento";
                            entity.Categorias.Add(cat5);

                            //Rubros rub = new Rubros();
                            //rub.Nombre = "General";
                            //entity.Rubros.Add(rub);

                            Bancos banco = new Bancos();
                            banco.Moneda = "Pesos Argentinos";
                            banco.SaldoInicial = 0;
                            banco.NroCuenta = "";
                            banco.IDBancoBase = dbContext.BancosBase.Where(x => x.Nombre == "Default").FirstOrDefault().IDBancoBase;
                            banco.FechaAlta = DateTime.Now;
                            banco.Activo = true;
                            entity.Bancos.Add(banco);

                


                            entity.Cajas = new List<Cajas> { new Cajas { Nombre = "Caja en pesos" } };

                            dbContext.Usuarios.Add(entity);

                            /*Personas persona = new Personas();
                            persona.CondicionIva = "CF";
                            persona.Tipo = "C";
                            persona.IDUsuario = entity.IDUsuario;
                            persona.RazonSocial = "Cliente final";*/

        //ListDictionary replacements = new ListDictionary();
        //bool send = EmailHelper.SendMessage(EmailTemplate.Bienvenido, replacements, entity.Email, "Contabilium: Bienvenido");
        /*        if (actividad == "Soy contador")
                    entity.UsaPlanCorporativo = true;

                dbContext.SaveChanges();
                int idCaja = dbContext.Cajas.Where(x => x.IDUsuario == entity.IDUsuario).First().IDCaja;

                PermisosModulos.ObtenerTodosLosFormularios(dbContext, entity.IDUsuario);

                //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, entity.IDUsuario, entity.CondicionIva);

                HttpContext.Current.Session["CurrentUser"] = new WebUser(entity.IDUsuario, 0, "A", entity.RazonSocial, entity.CUIT, entity.CondicionIva,
                    entity.Email, entity.EmailFrom, "", entity.Domicilio + " " + entity.PisoDepto, entity.Pais, entity.IDProvincia, entity.IDCiudad, entity.Telefono,
                    entity.TieneFacturaElectronica, entity.IIBB, entity.FechaInicioActividades, "", entity.TemplateFc, entity.IDUsuarioPadre, entity.SetupRealizado, false,
                    !entity.UsaProd, entity.IDPlan, entity.EmailAlertas, "", "", entity.EsAgentePercepcionIVA, entity.EsAgentePercepcionIIBB,
                    entity.EsAgenteRetencion, true, entity.UsaFechaFinPlan, entity.ApiKey, entity.ExentoIIBB, entity.UsaPrecioFinalConIVA,
                    entity.FechaAlta, entity.EnvioAutomaticoComprobante, entity.EnvioAutomaticoRecibo, entity.IDJurisdiccion, entity.UsaPlanCorporativo,
                    entity.ObservacionesFc, entity.ObservacionesRemito, entity.ObservacionesPresupuestos, entity.ObservacionesOrdenesPago, entity.MostrarBonificacionFc, entity.PorcentajeDescuento, false);

                if (actividad == "Soy contador")
                {
                    dbContext.ConfigurarPlanCorporativo(entity.IDUsuario);
                    var listaBancos = dbContext.Bancos.Where(x => x.IDUsuario == entity.IDUsuario).ToList();
                    foreach (var item in listaBancos)
                        ContabilidadCommon.CrearCuentaBancos(item.IDBanco, (WebUser)HttpContext.Current.Session["CurrentUser"]);

                    ContabilidadCommon.CrearCuentaCaja(idCaja, (WebUser)HttpContext.Current.Session["CurrentUser"]);
                }

                CrearDatosPrincipales(idCaja);

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<USUARIO>", entity.RazonSocial);
                try
                {
                    bool send = EmailCommon.SendMessage(entity.IDUsuario, EmailTemplate.Bienvenido, replacements, entity.Email, "¡Bienvenido a Contabilium!");
                }
                catch (Exception e)
                {
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "Fallo en envío de mails", e.ToString());
                }
                try
                {
                    bool send2 = EmailCommon.SendMessage(entity.IDUsuario, EmailTemplate.Notificacion, replacements, "roni@inlat.biz", "Contabilium: Nuevo registro " + entity.CodigoPromo + " " + entity.Email);
                }
                catch (Exception e)
                {
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "Fallo en envío de mails", e.ToString());
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }*/
    }

    [WebMethod(true)]
    public static void guardarConDescuento(string nombre, string email, string pwd, string telefono, string codigoPromocion, string actividad, string descuento)
    {
        UsuarioDto usu = new UsuarioDto();
        usu.RazonSocial = nombre;
        usu.Email = email;
        usu.Pwd = pwd;
        usu.Telefono = telefono;
        usu.CodigoPromocion = codigoPromocion;
        usu.Actividad = actividad;
        usu.Descuento = descuento;
        if (codigoPromocion.ToLower() == "egoforum")
            usu.Descuento = "15";
        UsuarioCommon.Registrar(usu);
        /*
                /*
                using (var dbContext = new ACHEEntities())
                {
                    if (esMailFalso(email.ToLower()))//mails temporarios
                        throw new Exception("El E-mail ingresado es inválido.");
                    if (dbContext.Usuarios.Any(x => x.Email == email))
                        throw new Exception("El E-mail ingresado ya se encuentra registrado.");
                    //else if (dbContext.Usuarios.Any(x => x.CUIT == cuit))
                    //    throw new Exception("El CUIT ingresado ya se encuentra registrado.");
                    //if (!cuit.IsValidCUIT())
                    //    throw new Exception("El CUIT es inválido.");
                    else
                    {
                        Usuarios entity = new Usuarios();

                        entity.RazonSocial = nombre; //razonSocial;
                        entity.EmailFrom = nombre;
                        entity.FechaInicioActividades = null;
                        entity.CondicionIva = ""; //condicionIva;
                        entity.CUIT = "";// cuit;
                        entity.IIBB = string.Empty;
                        entity.Personeria = ""; //personeria;
                        entity.Email = email;
                        entity.EmailAlertas = email;
                        entity.EmailStock = email;
                        entity.Pwd = pwd;
                        entity.Telefono = telefono;
                        entity.Celular = string.Empty;
                        entity.Contacto = string.Empty;
                        entity.Actividad = actividad;

                        //Domicilio
                        entity.Pais = "Argentina";
                        entity.IDProvincia = 2;//Ciudad de Buenos Aires
                        entity.IDCiudad = 5003;//SIN IDENTIFICAR
                        entity.Domicilio = string.Empty;
                        entity.PisoDepto = string.Empty;
                        entity.CodigoPostal = string.Empty;

                        entity.Theme = "default";
                        entity.TemplateFc = "default";
                        entity.FechaAlta = DateTime.Now;
                        entity.FechaUltLogin = DateTime.Now;
                        entity.TieneFacturaElectronica = false;
                        entity.Logo = null;
                        entity.Activo = true;
                        entity.IDPlan = 4;
                        entity.FechaFinPlan = DateTime.Now.AddDays(8);
                        entity.UsaFechaFinPlan = true;
                        entity.SetupRealizado = false;
                        entity.UsaProd = false;

                        entity.IDUsuarioPadre = null;
                        entity.CodigoPromo = codigoPromocion;

                        if (descuento != string.Empty)
                            entity.PorcentajeDescuento = decimal.Parse(descuento);
                        else
                            entity.PorcentajeDescuento = 0;

                        entity.ApiKey = Guid.NewGuid().ToString().Replace("-", "");

                        entity.EsAgentePercepcionIVA = false;
                        entity.EsAgentePercepcionIIBB = false;
                        entity.IDJurisdiccion = entity.IDProvincia.ToString();
                        entity.EsAgenteRetencion = false;
                        entity.CantidadEmpresas = 4;
                        entity.ExentoIIBB = false;
                        entity.UsaPrecioFinalConIVA = true;
                        entity.EnvioAutomaticoComprobante = false;
                        entity.EnvioAutomaticoRecibo = false;
                        entity.EsContador = false;
                        entity.UsaPlanCorporativo = false;
                        entity.CantidadDecimales = 2;
                        entity.MostrarBonificacionFc = true;
                        entity.VenderSinStock = true;

                        try
                        {
                            PlanesPagos p = new PlanesPagos();
                            p.IDUsuario = entity.IDUsuario;
                            p.IDPlan = 6;
                            p.ImportePagado = 0;
                            p.PagoAnual = false;
                            p.FormaDePago = "-";
                            p.NroReferencia = "";
                            p.Estado = "Aceptado";
                            p.FechaDeAlta = DateTime.Now.Date;
                            p.FechaDePago = DateTime.Now.Date;
                            p.FechaInicioPlan = p.FechaDePago;
                            p.FechaFinPlan = DateTime.Now.AddDays(int.Parse(ConfigurationManager.AppSettings["Registro.DiasPrueba"]));

                            dbContext.PlanesPagos.Add(p);

                            PuntosDeVenta punto = new PuntosDeVenta();
                            punto.FechaAlta = DateTime.Now;
                            punto.Punto = 1;
                            punto.PorDefecto = true;
                            entity.PuntosDeVenta.Add(punto);

                            Inventarios inventario = new Inventarios();
                            inventario.IDPuntoVenta = punto.IDPuntoVenta;
                            inventario.FechaAlta = DateTime.Now;
                            inventario.Nombre = "Principal";
                            inventario.IDProvincia = 2;
                            inventario.IDCiudad = 5003;
                            inventario.Direccion = string.Empty;
                            inventario.Telefono = telefono;
                            inventario.FechaUltimoInventarioFisico = DateTime.Now;
                            inventario.Activo = true;
                            entity.Inventarios.Add(inventario);

                            //if (cuit.StartsWith("30"))
                            //{
                            //    punto = new PuntosDeVenta();
                            //    punto.FechaAlta = DateTime.Now;
                            //    punto.Punto = 2;
                            //    punto.PorDefecto = false;
                            //    entity.PuntosDeVenta.Add(punto);
                            //}

                            Categorias cat = new Categorias();
                            cat.Nombre = "General";
                            entity.Categorias.Add(cat);

                            Categorias cat2 = new Categorias();
                            cat2.Nombre = "Honorarios";
                            entity.Categorias.Add(cat2);

                            Categorias cat3 = new Categorias();
                            cat3.Nombre = "Gastos varios";
                            entity.Categorias.Add(cat3);

                            Categorias cat4 = new Categorias();
                            cat4.Nombre = "Nafta";
                            entity.Categorias.Add(cat4);

                            Categorias cat5 = new Categorias();
                            cat5.Nombre = "Equipamiento";
                            entity.Categorias.Add(cat5);

                            //Rubros rub = new Rubros();
                            //rub.Nombre = "General";
                            //entity.Rubros.Add(rub);

                            Bancos banco = new Bancos();
                            banco.Moneda = "Pesos Argentinos";
                            banco.SaldoInicial = 0;
                            banco.NroCuenta = "";
                            banco.IDBancoBase = dbContext.BancosBase.Where(x => x.Nombre == "Default").FirstOrDefault().IDBancoBase;
                            banco.FechaAlta = DateTime.Now;
                            banco.Activo = true;
                            entity.Bancos.Add(banco);

                            entity.Cajas = new List<Cajas> { new Cajas { Nombre = "Caja en pesos" } };

                            dbContext.Usuarios.Add(entity);

                            /*Personas persona = new Personas();
                            persona.CondicionIva = "CF";
                            persona.Tipo = "C";
                            persona.IDUsuario = entity.IDUsuario;
                            persona.RazonSocial = "Cliente final";*/

        //ListDictionary replacements = new ListDictionary();
        //bool send = EmailHelper.SendMessage(EmailTemplate.Bienvenido, replacements, entity.Email, "Contabilium: Bienvenido");

        /*    dbContext.SaveChanges();
        
            int idCaja = dbContext.Cajas.Where(x => x.IDUsuario == entity.IDUsuario).First().IDCaja;

            PermisosModulos.ObtenerTodosLosFormularios(dbContext, entity.IDUsuario);

            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, entity.IDUsuario, entity.CondicionIva);

            HttpContext.Current.Session["CurrentUser"] = new WebUser(entity.IDUsuario, 0, "A", entity.RazonSocial, entity.CUIT, entity.CondicionIva,
                entity.Email, entity.EmailFrom, "", entity.Domicilio + " " + entity.PisoDepto, entity.Pais, entity.IDProvincia, entity.IDCiudad, entity.Telefono,
                entity.TieneFacturaElectronica, entity.IIBB, entity.FechaInicioActividades, "", entity.TemplateFc, entity.IDUsuarioPadre, entity.SetupRealizado, false,
                !entity.UsaProd, entity.IDPlan, entity.EmailAlertas, "", "", entity.EsAgentePercepcionIVA, entity.EsAgentePercepcionIIBB,
                entity.EsAgenteRetencion, true, entity.UsaFechaFinPlan, entity.ApiKey, entity.ExentoIIBB, entity.UsaPrecioFinalConIVA,
                entity.FechaAlta, entity.EnvioAutomaticoComprobante, entity.EnvioAutomaticoRecibo, entity.IDJurisdiccion, entity.UsaPlanCorporativo,
                entity.ObservacionesFc, entity.ObservacionesRemito, entity.ObservacionesPresupuestos, entity.ObservacionesOrdenesPago, entity.MostrarBonificacionFc, entity.PorcentajeDescuento, false);

            if (actividad == "Soy contador")
            {
                dbContext.ConfigurarPlanCorporativo(entity.IDUsuario);
                var listaBancos = dbContext.Bancos.Where(x => x.IDUsuario == entity.IDUsuario).ToList();
                foreach (var item in listaBancos)
                    ContabilidadCommon.CrearCuentaBancos(item.IDBanco, (WebUser)HttpContext.Current.Session["CurrentUser"]);

                ContabilidadCommon.CrearCuentaCaja(idCaja, (WebUser)HttpContext.Current.Session["CurrentUser"]);
            }

            CrearDatosPrincipales(idCaja);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}*/
    }

    /*    private static bool esMailFalso(string email)
        {
            if (email.Contains("mvrht.com") || email.Contains("mvrht.net") || email.Contains("trbvn.com") || email.Contains("my10minutemail.com")
                || email.Contains("20email.eu") || email.Contains("20mail.it") || email.Contains("yopmail.com")
                || email.Contains("mailinator.com") || email.Contains("haribu.com") || email.Contains("nowmymail.com")
                || email.Contains("clrmail.com") || email.Contains("@guerrillamail") || email.Contains("throwam.com")
                || email.Contains("stexsy.com") || email.Contains("lackmail.ru") || email.Contains("dispostable.com")
                || email.Contains("spamgourmet.com") || email.Contains("@trashmail") || email.Contains("inboxproxy.com")
                || email.Contains("instantemailaddress.com") || email.Contains("boun.cr") || email.Contains("mailforspam.com")
                || email.Contains("mailnull.com") || email.Contains("mailnesia.com") || email.Contains("maildrop.cc")
                || email.Contains("33mail.com") || email.Contains("zasod.com") || email.Contains("dayrep.com")
                || email.Contains("posdz.com") || email.Contains("tiapz.com") || email.Contains("isosq.com")
                || email.Contains("yuiop.com")
                )

                return true;
            else
                return false;
        }*/

    [WebMethod(true)]
    public static void enviarContacto(string nombre, string email, string telefono, string actividad, string mensaje)
    {
        var html = "<p>Nombre: " + nombre + "</p>";
        html += "<p>Email: " + email + "</p>";
        html += "<p>Telefono: " + telefono + "</p>";
        html += "<p>Actividad: " + actividad + "</p>";
        html += "<p>Mensaje: " + mensaje + "</p>";

        ListDictionary replacements = new ListDictionary();
        replacements.Add("<NOTIFICACION>", html);

        UsuarioCommon.EnviarContacto(replacements, email);

    }

    /*  private static void CrearDatosPrincipales(int idCaja)
      {
          try
          {
              if (HttpContext.Current.Session["CurrentUser"] != null)
              {
                  var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                  List<AbonosPersonasViewModel> personas = new List<AbonosPersonasViewModel>();
                  var idClientes = DatosInicialesCommon.CrearDatosClientes(usu);
                  var idProveedor = DatosInicialesCommon.CrearDatosProveedores(usu);
                  var comprobante = DatosInicialesCommon.CrearDatosVentas(usu, idClientes);
                  var compra = DatosInicialesCommon.CrearDatosCompras(usu, idProveedor);

                  personas.Add(new AbonosPersonasViewModel() { IDPersona = idClientes, Cantidad = "1" });
                  personas.Add(new AbonosPersonasViewModel() { IDPersona = idProveedor, Cantidad = "1" });
                  var idRubro = DatosInicialesCommon.CrearDatosRubros(usu.IDUsuario);
                  DatosInicialesCommon.CrearDatosConceptos(usu.IDUsuario, idRubro);
                  var idPago = DatosInicialesCommon.CrearDatosPagos(usu, compra, idCaja);
                  var idCobranza = DatosInicialesCommon.CrearDatosCobranzas(usu, comprobante, idCaja);
                  DatosInicialesCommon.CrearDatosPresupuestos(usu, idClientes);
                  //DatosInicialesCommon.CrearDatosAbonos(usu.IDUsuario, personas);

                  if (usu.UsaPlanCorporativo && usu.CondicionIVA == "RI")
                  {
                      ContabilidadCommon.AgregarAsientoDeCompra(compra.IDCompra, usu);
                      ContabilidadCommon.AgregarAsientoDeVentas(usu, comprobante.IDComprobante);
                      ContabilidadCommon.AgregarAsientoDePago(usu, idPago);
                      ContabilidadCommon.AgregarAsientoDeCobranza(usu, idCobranza);
                  }
              }
              else
                  throw new Exception("Por favor, vuelva a iniciar sesión");
          }
          catch (Exception ex)
          {
              //throw new Exception(ex.Message);
          }
      }*/
}
﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using ACHE.Negocio.Contabilidad;
using System.Web.Services;
using ACHE.Negocio.Common;
using ACHE.Negocio.Facturacion;
//using ACHE.FacturaElectronica.Lib;
using System.Collections.Generic;
//using iTextSharp.text.pdf;
//using iTextSharp.text;
using iText.Kernel.Pdf;
using System.Data.Entity;


public partial class comprobantes : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            //txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            //txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");

            //ddlModo.Items.Add(new ListItem("Otro", "O"));
            //ddlModo.Items.Add(new ListItem("Talonario preimpreso", "T"));
            //if (CurrentUser.TieneFE)
            //    ddlModo.Items.Add(new ListItem("Electrónica", "E"));
            // ddlModo.Items.Insert(0, new ListItem("Todos", ""));
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
                CurrentUser.TipoUsuario);

            if (CurrentUser.UsaPrecioFinalConIVA)
                hdnPrecioConIva.Value = "1";
            else
                hdnPrecioConIva.Value = "0";

            lnkGenerarCAE.Visible = CurrentUser.TieneFE;

            using (var dbContext = new ACHEEntities()) {
                var TieneDatos = dbContext.Comprobantes.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos) {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }
        }
    }

    [WebMethod(true)]
    public static void delete(int id) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ComprobantesCommon.EliminarComprobante(id, usu.IDUsuario);

        }
        catch (CustomException e) {
            throw new CustomException(e.Message);
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static int generarAbono(int id) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ComprobantesCommon.ConvertirFacturaEnAbono(id, usu.IDUsuario);

        }
        catch (CustomException e) {
            throw new CustomException(e.Message);
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosComprobantesViewModel getResults(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ComprobantesCommon.ObtenerComprobantes(condicion, periodo, fechaDesde, fechaHasta, page, pageSize, usu);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(string condicion, string periodo, string fechaDesde, string fechaHasta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return ComprobantesCommon.Exportar(condicion, periodo, fechaDesde, fechaHasta, usu);
    }


    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string imprimirMasivamente(string fechaDesde, string fechaHasta, string tipoComprobante, string puntos, string hojas) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return ComprobantesCommon.ImprimirMasivamente(fechaDesde, fechaHasta, tipoComprobante, puntos, hojas, usu);

    }


}
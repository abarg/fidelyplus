﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Abono;
using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Productos;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;

public partial class abonose : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            litPath.Text = "Alta";
            AbonoCart.Retrieve().Items.Clear();

            if (CurrentUser.CondicionIVA == "MO") {
                ddlIva.Enabled = false;
                ddlIva.SelectedValue = "0,00";
            }
            else if (CurrentUser.CondicionIVA == "RI")
                ddlIva.SelectedValue = "21,00";

            if (CurrentUser.UsaPlanCorporativo)
                CargarCuentasActivo();

            if (CurrentUser.UsaPrecioFinalConIVA)
                liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Precio Unit. con IVA</span> ";
            else
                liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Precio Unit. sin IVA</span> ";

            //Edicion
            if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];
                if (!String.IsNullOrEmpty(Request.QueryString["Duplicar"]))
                    hdnDuplicar.Value = Request.QueryString["ID"];

                if (hdnID.Value != "0") {
                    var duplicado = (!String.IsNullOrEmpty(Request.QueryString["Duplicar"])) ? Request.QueryString["Duplicar"] : "0";
                    cargarEntidad(int.Parse(hdnID.Value), duplicado);
                    if (duplicado == "1") {
                        litPath.Text = "Alta";
                        hdnID.Value = "0";
                    }
                    else
                        litPath.Text = "Edición";
                }
            }
        }
    }

    private void CargarCuentasActivo() {
        using (var dbContext = new ACHEEntities()) {
            if (CurrentUser.UsaPlanCorporativo) //Plan Corporativo
            {
                if (dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == CurrentUser.IDUsuario)) {
                    var listaAux = AbonosCommon.ObtenerCuentasActivo(CurrentUser.IDUsuario, dbContext);

                    ddlPlanDeCuentas.Items.Add(new ListItem("", ""));
                    ddlPlanDeCuentas2.Items.Add(new ListItem("", ""));

                    foreach (var item in listaAux) {
                        ddlPlanDeCuentas.Items.Add(new ListItem(item.Nombre, item.IDPlanDeCuenta.ToString()));
                        ddlPlanDeCuentas2.Items.Add(new ListItem(item.Nombre, item.IDPlanDeCuenta.ToString()));
                    }
                }
                hdnUsaPlanCorporativo.Value = "1";
            }
        }
    }

    private void cargarEntidad(int id, string duplicado) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Abonos.Include("AbonosPersona").Include("AbonosDetalle").Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDAbono == id).FirstOrDefault();
            if (entity != null) {
                ddlFrecuencia.SelectedValue = entity.Frecuencia;
                txtNombre.Text = entity.Nombre.ToUpper();
                txtFechaInicio.Text = entity.FechaInicio.ToString("dd/MM/yyyy");
                if (entity.FechaFin.HasValue)
                    txtFechaFin.Text = entity.FechaFin.Value.ToString("dd/MM/yyyy");
                ddlEstado.SelectedValue = entity.Estado;
                txtPrecio.Text = entity.PrecioUnitario.ToString("").Replace(",", ".");
                ddlIva.SelectedValue = entity.Iva.ToString("#0.00");
                txtObservaciones.Text = entity.Observaciones;
                hdnIDCondicionVenta.Value = entity.IDCondicionVenta.ToString();
                hdnTieneSuscripcionesVinculadas.Value = dbContext.Suscripciones.Any(x => x.IDAbono == id) ? "1" : "0";

                ddlProducto.SelectedValue = entity.Tipo.ToString();
                if (entity.IDPlanDeCuenta != null)
                    ddlPlanDeCuentas.SelectedValue = entity.IDPlanDeCuenta.ToString();

                foreach (var per in entity.AbonosPersona)
                    hdnPersonasID.Value += per.IDPersona + ",";

                if (hdnPersonasID.Value.Length > 0)
                    hdnPersonasID.Value = hdnPersonasID.Value.Substring(0, hdnPersonasID.Value.Length - 1);

                litTotal.Text = (entity.PrecioUnitario + ((entity.PrecioUnitario * entity.Iva) / 100)).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static AbonoCartDto obtenerDatos(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return AbonosCommon.ObtenerDatos(id, usu.IDUsuario);

    }

    [WebMethod(true)]
    public static void guardar(int id, string nombre, string frecuencia, string fechaInicio, string fechaFin, string estado, string precio, string iva, string obs, List<AbonosPersonasViewModel> personas, int tipo, int idPlanDeCuenta, string detallado, string condicionVenta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        if (detallado == "1" && AbonoCart.Retrieve().Items.Count == 0)
            throw new CustomException("No puede generar un abono detallado sin items.");

        using (var dbContext = new ACHEEntities()) {
            AbonoCartDto compCart = new AbonoCartDto();
            compCart.IDAbono = id;
            compCart.Nombre = nombre;
            compCart.Frecuencia = frecuencia;
            compCart.FechaInicio = fechaInicio;
            compCart.FechaFin = fechaFin;
            compCart.IDUsuario = usu.IDUsuario;
            compCart.Estado = estado;
            compCart.Precio = precio;
            compCart.Iva = iva;
            compCart.Observaciones = obs;
            compCart.Tipo = tipo;
            compCart.IDPlanDeCuenta = idPlanDeCuenta;
            compCart.Items = new List<AbonosDetalleViewModel>();
            compCart.Items = AbonoCart.Retrieve().Items;
            compCart.Personas = new List<AbonosPersonasViewModel>();
            compCart.Personas = personas;
            compCart.Detallado = detallado == "1";
            compCart.CondicionVenta = "";//DESPUES SACAR
            compCart.IDCondicionVenta = int.Parse(condicionVenta);
            if (compCart.Detallado) {

                compCart.ImporteNoGrabado = AbonoCart.Retrieve().GetSubTotal();
                compCart.PercepcionIVA = (AbonoCart.Retrieve().GetIva() / AbonoCart.Retrieve().GetSubTotal()) * 100;
            }

            var entity = AbonosCommon.Guardar(dbContext, compCart);
            if (detallado == "1") {
                if (dbContext.Suscripciones.Any(x => x.IDAbono == id)) {
                    var suscripcionesVinculadas = dbContext.Suscripciones.Where(x => x.IDAbono == id).ToList();
                    if (suscripcionesVinculadas.Count() > 0) {
                        foreach (var s in suscripcionesVinculadas) {
                            s.PrecioUnitario = compCart.ImporteNoGrabado;
                            s.Iva = compCart.PercepcionIVA;
                        }
                        dbContext.SaveChanges();
                    }
                }
            }
        }

    }

    [WebMethod(true)]
    public static List<AbonosPersonasViewModel> ObtenerClientes(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return AbonosCommon.ObtenerClientes(id, usu.IDUsuario);


    }

    [WebMethod(true)]
    public static void agregarItem(int id, string idConcepto, string concepto, string iva, string precio, string bonif, string cantidad, string codigo, string idPlanDeCuenta, string codigoCta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        AbonoCartItemDto item = new AbonoCartItemDto();
        item.ID = id;
        item.IDConcepto = idConcepto;
        item.Concepto = concepto;
        item.Iva = iva;
        item.Precio = precio;
        item.Bonificacion = bonif;
        item.Cantidad = cantidad;
        item.Codigo = codigo;
        item.IDPlanDeCuenta = idPlanDeCuenta;
        item.CodigoCuenta = codigoCta;

        AbonosCommon.AgregarItem(item, ConfiguracionHelper.ObtenerCantidadDecimales());

    }

    [WebMethod(true)]
    public static string obtenerItems() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return AbonosCommon.ObtenerItems(usu.IDUsuario);


    }

    [WebMethod(true)]
    public static void eliminarItem(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        AbonosCommon.EliminarItem(id);
    }

    [WebMethod(true)]
    public static TotalesViewModel obtenerTotales(string percepcionesIIBB, string percepcionesIVA, string importeNoGrabado) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        return AbonosCommon.ObtenerTotales(percepcionesIIBB, percepcionesIVA, importeNoGrabado, ConfiguracionHelper.ObtenerCantidadDecimales());
    }

    #region Helpers
    private static decimal ObtenerPrecioFinal(decimal PrecioUnitario, string iva) {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        using (var dbContext = new ACHEEntities()) {
            if (usu.UsaPrecioFinalConIVA)
                return ConceptosCommon.ObtenerPrecioFinal(PrecioUnitario, iva, ConfiguracionHelper.ObtenerCantidadDecimales());
            else
                return PrecioUnitario;
        }
    }
    #endregion
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Model;
using System.Web.Services;
using ACHE.Negocio.Inventario;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Common;

public partial class conceptos : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                var TieneDatos = dbContext.Conceptos.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos)
                {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else
                {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }

                //verifico si tiene acceso a lista de precios
                if (CurrentUser.TipoUsuario == "C")
                    liListaPreciosConceptos.Visible = PermisosModulos.mostrarHeaderFormularioPersonalizado(2, liListaPreciosConceptos.Attributes["data-formulario"]);
            }
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);

        }
    }

    [WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var archivo = string.Empty;
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Conceptos.Where(x => x.IDConcepto == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (string.IsNullOrWhiteSpace(entity.Foto))
                    archivo = entity.Foto;
            }
            ConceptosCommon.EliminarConcepto(id, usu.IDUsuario);

        }
        catch (CustomException e)
        {
            throw new CustomException(e.Message);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void deleteAll()
    {
        try
        {

            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");


            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ConceptosCommon.EliminarTodos(usu.IDUsuario);

        }
        catch (CustomException e)
        {
            throw new CustomException(e.Message);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosProductosViewModel getResults(string tipo, string codigo, string condicion, int page, int pageSize)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ConceptosCommon.ObtenerConceptos(tipo, codigo, condicion, page, pageSize, usu.IDUsuario);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(string tipo, string codigo, string condicion)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return ConceptosCommon.ExportarConceptos(usu.IDUsuario, tipo, codigo, condicion);
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string getHistorial(int id)
    {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return ConceptosCommon.ObtenerHistorial(id, usu.IDUsuario);

    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string getStock(int id)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        return ConceptosCommon.ObtenerStock(id);

    }

    //[WebMethod(true)]
    //public static void Duplicar(int id)
    //{
    //    try
    //    {
    //        using (var dbContext = new ACHEEntities())
    //        {
    //            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
    //            var concepto = dbContext.Conceptos.Where(x => x.IDConcepto == id).FirstOrDefault();
    //            var maxIdCocnepto = dbContext.Conceptos.Where(x => x.IDUsuario == usu.IDUsuario && x.IDConcepto == id).Max(x => x.IDConcepto);

    //            var cd = new Conceptos();
    //            cd.Codigo = maxIdCocnepto.ToString();
    //            cd.CostoInterno = concepto.CostoInterno;
    //            cd.Descripcion = concepto.Descripcion;
    //            cd.Estado = concepto.Estado;
    //            cd.FechaAlta = DateTime.Now.Date;
    //            cd.IDUsuario = usu.IDUsuario;
    //            cd.Iva = concepto.Iva;
    //            cd.Nombre = concepto.Nombre;
    //            cd.Observaciones = concepto.Observaciones;
    //            cd.PrecioUnitario = concepto.PrecioUnitario;
    //            cd.Stock = concepto.Stock;
    //            cd.StockMinimo = concepto.StockMinimo;
    //            cd.Tipo = concepto.Tipo;
    //            cd.Foto = concepto.Foto;


    //            dbContext.Conceptos.Add(cd);
    //            dbContext.SaveChanges();

    //            if (!string.IsNullOrWhiteSpace(concepto.Foto))
    //            {
    //                var pathOrigen = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario.ToString() + "/Productos-Servicios/" + concepto.Foto);

    //                if (File.Exists(pathOrigen))
    //                {
    //                    var filename = "concepto-" + cd.IDConcepto.ToString() + Path.GetExtension(pathOrigen);
    //                    var pathdestino = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario.ToString() + "/Productos-Servicios/" + filename);
    //                    File.Copy(pathOrigen, pathdestino);
    //                    cd.Foto = filename;
    //                }
    //            }
    //        }
    //    }
    //    catch (CustomException ex)
    //    {
    //        throw new CustomException(ex.Message);
    //    }
    //}
}
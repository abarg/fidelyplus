﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Contabilidad;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;

public partial class importar : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            Session["DataImport"] = null;
            var tipo = Request.QueryString["tipo"];
            var idLista = Request.QueryString["lista"];

            if (!string.IsNullOrEmpty(tipo))
                ddlTipo.SelectedValue = tipo;

            if (!string.IsNullOrEmpty(tipo))
                hdnIDLista.Value = idLista;

            using (var dbContext = new ACHEEntities()) {
                var tieneDatos = dbContext.PlanDeCuentas.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                hdnTieneCuentasContables.Value = (tieneDatos) ? "1" : "0";
            }
        }
    }

    #region PRODUCTOS
    [WebMethod(true)]
    public static List<ProductosCSVTmp> leerArchivoCSVProductos(string nombre, string tipo) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");


        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;
        List<ProductosCSVTmp> listaproductosCSV = new List<ProductosCSVTmp>();


        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaproductosCSV = ImportacionMasiva.LeerArchivoCSVProductos(tipo, usu.IDUsuario, path);

            if (listaproductosCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaproductosCSV;

                return listaproductosCSV;
            }
            else {
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
            }
        }
        catch (Exception) {
            throw;
        }

    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionProductos() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var lista = (List<ProductosCSVTmp>)HttpContext.Current.Session["DataImport"];
        ImportacionMasiva.RealizarImportacionProductos(lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString);
    }
    #endregion

    #region PERSONAS
    [WebMethod(true)]
    public static List<PersonasCSVTmp> leerArchivoCSVPersonas(string nombre, string tipo) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;

        List<PersonasCSVTmp> listaPersonasCSV = new List<PersonasCSVTmp>();
        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaPersonasCSV = ImportacionMasiva.LeerArchivoCSVPersonas(tipo, usu.IDUsuario, path);

            if (listaPersonasCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaPersonasCSV;
                return listaPersonasCSV;
            }
            else
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
        }
        catch (Exception) {
            throw;
        }

    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionPersonas() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        if ((List<PersonasCSVTmp>)HttpContext.Current.Session["DataImport"] == null)
            throw new Exception("No se encontraron datos");
        var lista = (List<PersonasCSVTmp>)HttpContext.Current.Session["DataImport"];
        ImportacionMasiva.RealizarImportacionPersonas(lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString);

    }
    #endregion

    #region LISTA DE PRECIOS

    [WebMethod(true)]
    public static List<ProductosPreciosCSVTmp> leerArchivoCSVListaPrecios(string nombre, string idLista) {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;

        List<ProductosPreciosCSVTmp> listaPreciosCSV = new List<ProductosPreciosCSVTmp>();
        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaPreciosCSV = ImportacionMasiva.LeerArchivoCSVListaPrecios(idLista, usu.IDUsuario, path);

            if (listaPreciosCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaPreciosCSV;

                return listaPreciosCSV;
            }
            else {
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
            }
        }
        catch (Exception) {
            throw;
        }

    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionListaPrecios() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        var lista = (List<ProductosPreciosCSVTmp>)HttpContext.Current.Session["DataImport"];

        int idProceso = LogCommon.IniciarProceso(usu.IDUsuario, 0, "ActualizarMasivamentePrecios");//se envia 0 en integracion, porque no se usa.

        ImportacionMasiva.RealizarImportacionListaPrecios(usu.IDUsuario, lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString, idProceso);
    }

    #endregion

    #region STOCK

    [WebMethod(true)]
    public static List<StockCSVTmp> leerArchivoCSVStock(string nombre, string idLista) {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;

        List<StockCSVTmp> listaStockCSV = new List<StockCSVTmp>();
        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaStockCSV = ImportacionMasiva.LeerArchivoCSVStock(usu.IDUsuario, path);

            if (listaStockCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaStockCSV;

                return listaStockCSV;
            }
            else {
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
            }
        }
        catch (Exception) {
            throw;
        }

    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionStock() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión"); var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        var lista = (List<StockCSVTmp>)HttpContext.Current.Session["DataImport"];

        int idProceso = LogCommon.IniciarProceso(usu.IDUsuario, 0, "ActualizarMasivamenteStock");//se envia 0 en integracion, porque no se usa.


        ImportacionMasiva.RealizarImportacionStock(usu.IDUsuario, lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString, idProceso);
    }

    #endregion

    #region PLAN DE CUENTAS
    [WebMethod(true)]
    public static List<PlanDeCuentasCSVTmp> LeerArchivoCSVPlanDeCuentas(string nombre) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;
        List<PlanDeCuentasCSVTmp> listaPlanDeCuentasCSV = new List<PlanDeCuentasCSVTmp>();
        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaPlanDeCuentasCSV = ImportacionMasiva.LeerArchivoCSVPlanDeCuentas(usu.IDUsuario, path);
            if (listaPlanDeCuentasCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaPlanDeCuentasCSV;
                return listaPlanDeCuentasCSV;
            }
            else
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
        }
        catch (Exception) {
            throw;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionPlanDeCuentas() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        try {
            var lista = (List<PlanDeCuentasCSVTmp>)HttpContext.Current.Session["DataImport"];
            ImportacionMasiva.RealizarImportacionPlanDeCuentas(lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString, usu.IDUsuario);
            ImportacionMasiva.ReferenciarPlanDeCuentas(usu.IDUsuario, lista);
        }
        catch (Exception) {
            ContabilidadCommon.EliminarPlanDeCuentasActual(usu.IDUsuario);
            ContabilidadCommon.EliminarConfiguracionPlanDeCuenta(usu.IDUsuario);
            throw new Exception("El plan de cuentas no pudo ser importado.");
        }

    }
    #endregion

    #region FACTURAS VENTA
    [WebMethod(true)]
    public static List<FacturasCSVTmp> leerArchivoCSVFacturas(string nombre) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;

        List<FacturasCSVTmp> listaFacturasCSV = new List<FacturasCSVTmp>();
        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaFacturasCSV = ImportacionMasiva.LeerArchivoCSVFacturas(usu, path);

            if (listaFacturasCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaFacturasCSV;
                return listaFacturasCSV;
            }
            else {
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
            }
        }
        catch (Exception) {
            throw;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionFacturas() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if ((List<FacturasCSVTmp>)HttpContext.Current.Session["DataImport"] == null)
            throw new Exception("No se encontraron datos");
        var lista = (List<FacturasCSVTmp>)HttpContext.Current.Session["DataImport"];
        ImportacionMasiva.RealizarImportacionFacturas(lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString, usu.IDUsuario);
        ImportacionMasiva.ReferenciarFacturasClientes(lista, usu);

    }
    #endregion

    #region FACTURAS COMPRA
    [WebMethod(true)]
    public static List<FacturasCompraCSVTmp> leerArchivoCSVFacturasCompras(string nombre) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;

        List<FacturasCompraCSVTmp> listaFacturasCSV = new List<FacturasCompraCSVTmp>();
        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaFacturasCSV = ImportacionMasiva.LeerArchivoCSVFacturasCompra(usu, path);

            if (listaFacturasCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaFacturasCSV;
                return listaFacturasCSV;
            }
            else {
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
            }
        }
        catch (Exception) {
            throw;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionFacturasCompras() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if ((List<FacturasCompraCSVTmp>)HttpContext.Current.Session["DataImport"] == null)
            throw new Exception("No se encontraron datos");
        var lista = (List<FacturasCompraCSVTmp>)HttpContext.Current.Session["DataImport"];
        ImportacionMasiva.RealizarImportacionFacturasCompra(lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString, usu.IDUsuario);
        ImportacionMasiva.ReferenciarFacturasComprasClientes(lista, usu);
    }

    #endregion

    #region COMPROBANTES
    [WebMethod(true)]
    public static List<ComprobantesDetalleCSVTmp> leerArchivoCSVComprobantes(string nombre) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        HttpContext.Current.Session["DataImport"] = null;

        List<ComprobantesDetalleCSVTmp> listaComprobantesCSV = new List<ComprobantesDetalleCSVTmp>();
        try {
            string path = string.Empty;
            if (!string.IsNullOrEmpty(nombre))
                path = HttpContext.Current.Server.MapPath("~/files/importaciones/Datos/" + nombre);

            listaComprobantesCSV = ImportacionMasiva.LeerArchivoCSVComprobantesDetalle(usu, path);

            if (listaComprobantesCSV.Any()) {
                HttpContext.Current.Session["DataImport"] = listaComprobantesCSV;
                return listaComprobantesCSV;
            }
            else {
                throw new Exception("No se encontraron datos en el archivo. Asegurese que el formato del archivo sea correcto.");
            }
        }
        catch (Exception ex) {
            throw;
        }

    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void RealizarImportacionComprobantes() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if ((List<ComprobantesDetalleCSVTmp>)HttpContext.Current.Session["DataImport"] == null)
            throw new Exception("No se encontraron datos");
        var lista = (List<ComprobantesDetalleCSVTmp>)HttpContext.Current.Session["DataImport"];
        ImportacionMasiva.RealizarImportacionComprobantes(lista, ConfigurationManager.ConnectionStrings["ACHEString"].ConnectionString);
        //     ImportacionMasiva.ReferenciarFacturasComprasClientes(lista, usu);
    }


    #endregion
}
﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using ACHE.Negocio.Facturacion;
using ACHE.Model;
using System.Web.Services;
using System.Data.Entity;
using ACHE.Negocio.Common;

public partial class cobranzas : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            using (var dbContext = new ACHEEntities()) {
                var TieneDatos = dbContext.Cobranzas.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos) {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [WebMethod(true)]
    public static void delete(int id) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            CobranzasCommon.EliminarCobranza(id, usu.IDUsuario);
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosComprobantesViewModel getResults(int idPersona, string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            return CobranzasCommon.ObtenerCobranzas(condicion, periodo, fechaDesde, fechaHasta, page, pageSize, usu);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(int idPersona, string condicion, string periodo, string fechaDesde, string fechaHasta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return CobranzasCommon.ExportarCobranzas(idPersona, condicion, periodo, fechaDesde, fechaHasta, usu.IDUsuario);
    }

    [WebMethod(true)]
    public static string obtenerACuenta() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return CobranzasCommon.ObtenerAcuenta(usu.IDUsuario);
    }
}
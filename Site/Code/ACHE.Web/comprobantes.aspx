﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="comprobantes.aspx.cs" Inherits="comprobantes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        
        .clsDatePicker {
            z-index: 100000 !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-file-text"></i>Facturas realizadas <span>Administración</span></h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li class="active">Facturación</li>
            </ol>
        </div>
    </div>
    <div id="divConDatos" runat="server">
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12 col-md-12 table-results">
                    <div class="panel panel-default">
                        <div class="panel-heading">

                            <form runat="server" id="frmSearch">
                                <input type="hidden" id="hdnPage" runat="server" value="1" />
                                <asp:HiddenField runat="server" ID="hdnPrecioConIva" />
                                <asp:HiddenField runat="server" ID="hdnTieneFE" Value="0" ClientIDMode="Static" />

                                <div class="col-sm-12" style="padding-left: inherit">
                                    <div class="col-sm-5 col-md-7">
                                        <div class="row">
                                            <div class="col-sm-8 col-md-8 form-group">
                                                <input type="text" class="form-control" id="txtCondicion" maxlength="128" placeholder="Ingrese el número y/o tipo de la factura y/o cliente " />
                                            </div>

                                            <div class="col-sm-4 col-md-4 form-group">
                                                <select class="form-control"id="ddlPeriodo" onchange="otroPeriodo();">
                                                    <option value="30" selected="selected">Últimos 30 dias</option>
                                                    <option value="15">Últimos 15 dias</option>
                                                    <option value="7">Últimos 7 dias</option>
                                                    <option value="1">Ayer</option>
                                                    <option value="0">Hoy</option>
                                                    <option value="-1">Otro período</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div id="divMasFiltros" style="display: none">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Fecha desde" MaxLength="10" onchange="filtrar();"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-6 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Fecha hasta" MaxLength="10" onchange="filtrar();"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-7 col-md-5 responsive-buttons md">
                                        <div class="btn-group" id="btnAcciones">
                                            <a class="btn btn-warning mr10" onclick="nuevo();">
                                                <i class="fa fa-plus"></i>&nbsp;Nueva Factura
                                            </a>
                                        </div>
                                        <div class="btn-group dropdown">
                                            <button type="button" class="btn btn-btn-default"><i class="fa fa-tasks"></i> Otras acciones</button>
                                            <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="facturaRapida.showModal();">Venta de mostrador</a></li>
                                                <li><a href="/modulos/ventas/facturasElectronicasPendientes.aspx">Facturación masiva de pendientes</a></li>
                                                <li><a href="/abonos.aspx">Ver abonos actuales</a></li>
                                                <li><a href="/abonose.aspx">Crear abono</a></li>
                                                <li><a href="/generarAbonos.aspx">Facturar abonos</a></li>
                                                <li><a href="#" onclick="impresionMasiva.showModal();">Impresión masiva</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="col-sm-12"><hr class="mt0" /></div>
                            <div class="row">
                                <div class="pull-right">
                                    <div class="btn-group mr10" id="divExportar" runat="server">
                                        <div class="btn btn-white tooltips">
                                            <a id="divIconoDescargar" href="javascript:exportar();">
                                                <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                            </a>
                                            <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                            <a href="" id="lnkDownload" onclick="resetearExportacion();" download="Comprobantes" style="display: none">Descargar</a>
                                        </div>
                                    </div>

                                    <div class="btn-group mr10" id="divPagination" style="display: none">
                                        <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                        <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                    </div>
                                </div>

                                <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                                <p id="msjResultados" style="padding-left: 20px"></p>
                            </div>
                        </div>
                        <!-- panel-heading -->
                        <div class="panel-body">
                            <div class="alert alert-danger" id="divError" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Lo sentimos!</strong> <span id="msgError"></span>
                            </div>

                            <div class="table-responsive">
                                <table class="table mb30">
                                    <thead>
                                        <tr>
                                            <!--th>#</!--th-->
                                            <th>Tipo</th>
                                            <th>Número</th>
                                            <th>Proveedor/Cliente</th>
                                            <th>Fecha</th>
                                            <th>Modo</th>
                                            <th>Importe</th>
                                            <th>Total Fact.</th>
                                            <th class="columnIcons"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="resultsContainer">
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script id="resultTemplate" type="text/x-jQuery-tmpl">
            {{each results}}
            <tr>
                <!--td>${ID}</td-->
                <td>${Tipo}</td>
                <td style="min-width: 130px">${Numero}</td>
                <td>${RazonSocial}</td>
                <td>${Fecha}</td>
                <td>${Modo}</td>
                <td>${ImporteTotalBruto}</td>
                <td>${ImporteTotalNeto}</td>
                <td class="table-action">
                    {{if $value.PuedeAdm == "T"}}
                        <a onclick="editar(${ID});" style="cursor: pointer; font-size: 16px" title="Editar"><i class="fa fa-pencil"></i></a>
                    {{/if}} 
                    {{if $value.PuedeAdm == "F"}}
                        <a onclick="editar(${ID});" style="cursor: pointer; font-size: 16px" title="Ver factura"><i class="fa fa-search"></i></a>           
                    {{/if}} 
                    <a onclick="duplicar(${ID});" style="cursor: pointer; font-size: 16px" class="delete-row" title="Duplicar"><i class="fa fa-files-o"></i></a>
                    {{if $value.PuedeAnular == "T"}}
                        <a onclick="anular(${ID});" style="cursor: pointer; font-size: 16px" class="delete-row" title="Crear NC"><i class="fa fa-undo"></i></a>
                    {{/if}} 
                    {{if $value.PuedeAdm == "T"}}
                        <a onclick='eliminar(${ID},"${RazonSocial}");' style="cursor: pointer; font-size: 16px" class="delete-row" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                    {{/if}} 
                </td>
            </tr>
            {{/each}}
        </script>

        <script id="noResultTemplate" type="text/x-jQuery-tmpl">
            <tr>
                <td colspan="8">No se han encontrado resultados</td>
            </tr>
        </script>

    </div>
    <div id="divSinDatos" runat="server">
        <div class="panel-heading" style="background-color: white; height: 30%; width: 100%; border-radius: 4px; text-align: center;">
            <h2 id="hTitulo">Aún no has creado ningún comprobante</h2>
            <br />
            <a class="btn btn-warning" onclick="nuevo();" id="btnNuevoSinDatos">Crea un comprobante</a>
        </div>
    </div>

    <!-- MODAL FACTURACION RAPIDA -->
    <div class="modal modal-wide fade" id="modalFacturacionRapida" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Venta de mostrador</h4>
                </div>
                <div class="modal-body">
                    <form id="frmFacturaRapida">
                        <div class="alert alert-danger" id="divErrorFactRapida" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgErrorFactRapida"></span>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Fecha</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    <input type="text" id="txtFactRapidaFecha" class="form-control input-sm required validDate validFechaActual" placeholder="dd/mm/yyyy" maxlength="10"></input>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Razón Social</label>
                                <input type="text" id="txtFactRapidaRazonSocial" class="form-control input-sm required" placeholder="dd/mm/yyyy" value="CONSUMIDOR FINAL" maxlength="128" onkeyup="remplazarCaracteresEspeciales('txtFactRapidaRazonSocial');"></input>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Categoría Impositiva</label>
                                <select id="ddlFactRapidaCondicionIva" class="form-control input-sm required" onchange="facturaRapida.changeCondicionIva();">
                                    <option value="RI">Responsable Inscripto</option>
                                    <option value="CF" selected="selected">Consumidor Final</option>
                                    <option value="MO">Monotributista<//option>
                                    <option value="EX">Exento<//option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk" id="spIdentificacionObligatoria">*</span> Indicanos como identificarlo</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <select id="ddlFactRapidaTipoDoc" class="form-control input-sm required">
                                    <option value="DNI" selected="selected">DNI</option>
                                    <option value="CUIT">CUIT/CUIL<//option>
                                </select>
                            </div>
                            <div class="col-sm-6 form-group">
                                <input type="text" id="txtFactRapidaNroDocumento" class="form-control input-sm required number validCuit" placeholder="Número" maxlength="13"></input>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Tipo de comprobante</label>
                                <select class="form-control input-sm required" id="ddlFactRapidaTipo" data-placeholder="Seleccione un tipo">
                                </select>
                            </div>




                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Condición de Venta</label>
                                <select class="form-control input-sm required" id="ddlFactRapidaCondicionVenta">
                                  <!--  <option value="Efectivo">Efectivo</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="Cuenta corriente">Cuenta corriente</option>
                                    <option value="MercadoPago">MercadoPago</option>
                                    <option value="Tarjeta de debito">Tarjeta de debito</option>
                                    <option value="Tarjeta de credito">Tarjeta de credito</option>
                                    <option value="Ticket">Ticket</option>
                                    <option value="Otro">Otro</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 form-group">
                                <span class="asterisk">*</span> <label id="lblFactRapidaImporte">Importe</label>
                                <input type="text" class="form-control input-sm required" maxlength="10" id="txtFactRapidaImporte"  onchange="facturaRapida.changeImporte()"/>
                            </div>
                            <div class="col-sm-3 form-group">
                                <span class="asterisk">*</span> IVA %
                                <select class="form-control input-sm" id="ddlFactRapidaIva" onchange="facturaRapida.changeImporte()">
                                    <option value="0,00">0</option>
                                    <option value="2,50">2,5</option>
                                    <option value="5,00">5</option>
                                    <option value="10,50">10,5</option>
                                    <option value="21,00">21</option>
                                    <option value="27,00">27</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <strong>TOTAL</strong>
                                <h4 class="text-primary" style="color: green" id="divFactRapidaTotal">$ 0,00</h4>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnIDUsuario" value="0" />
                    <a class="btn btn-success" onclick="facturaRapida.grabar();" id="lnkFactRapidaAceptar">Guardar</a>
                    <asp:HyperLink runat="server" ID="lnkGenerarCAE" onclick="facturaRapida.generarCae();" CssClass="btn btn-info"><i class="fa fa-dollar"></i> Emitir factura eléctronica</asp:HyperLink>
                    <a href="#" id="lnkDownloadPdf"  class="btn btn-success mr5" download>Descargar en PDF</a>
                    <asp:HyperLink runat="server" ID="lnkClear" onclick="facturaRapida.limpiarCampos();" CssClass="btn btn-default"><i class="fa fa-eraser"></i> Limpiar campos</asp:HyperLink>
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

     <!-- MODAL IMPRESION MASIVA -->
    <div class="modal modal-wide fade" id="modalImpresionMasiva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Impresión masiva de comprobantes</h4>
                </div>
                <div class="modal-body">
                    <form id="frmImpresionMasiva">
                        <div class="alert alert-danger" id="divErrorImpresionMasiva" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgErrorImpresionMasiva"></span>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Fecha desde</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    <input type="text" id="txtFechaDesdeImpresionMasiva" class="form-control input-sm required validDate clsDatePicker" placeholder="dd/mm/yyyy" maxlength="10"></input>
                                </div>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Fecha hasta</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    <input type="text" id="txtFechaHastaImpresionMasiva" class="form-control input-sm required validDate clsDatePicker" placeholder="dd/mm/yyyy" maxlength="10"></input>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Tipo de comprobante</label>
                                <select class="form-control input-sm required" id="ddlTiposComprobanteImpresionMasiva" data-placeholder="Seleccione un tipo">
                                </select>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Puntos de venta</label>
                                <input type="text" id="txtPuntosImpresionMasiva" class="form-control input-sm required" placeholder="todos"></input>
                                <small>Ej: 1,2. Si no ingresa ninguno, se imprimirán todos.</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label"><span class="asterisk">*</span> Hojas</label>
                                <select class="form-control input-sm required" id="ddlHojasImpresionMasiva" data-placeholder="Seleccione una hoja">
                                    <option value="D">Duplicado</option>
                                    <option value="O">Original</option>
                                    <option value="T">Original y duplicado</option>
                                </select>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Puntos de venta</label>
                                <input type="text" id="txtPuntosImpresionMasiva" class="form-control input-sm required" placeholder="todos"></input>
                                <small>Ej: 1,2. Si no ingresa ninguno, se imprimirán todos.</small>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" onclick="impresionMasiva.generar();" id="lnkImpresionMasivaGenerar">Generar</a>
                    <a href="#" id="lnkImpresionMasivaDownloadPdf"  class="btn btn-default mr5" download>Descargar</a>
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/comprobantes.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            if ($('#divConDatos').is(":visible")) {
                filtrar();
                configFilters();
            }
        });
    </script>
</asp:Content>

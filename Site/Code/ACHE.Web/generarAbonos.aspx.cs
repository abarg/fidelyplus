﻿using ACHE.Model;
using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Common;
using ACHE.Model.Negocio;
using ACHE.Negocio.Ventas;
using ACHE.Negocio.Abono;

public partial class generarAbonos : BasePage {
    protected static ResultadosAbonosAGenerarViewModel resultadosAbonos {
        get { return (ResultadosAbonosAGenerarViewModel)HttpContext.Current.Session["GenerarAbonos_resultadosAbonos"]; }
        set { HttpContext.Current.Session["GenerarAbonos_resultadosAbonos"] = value; }
    }

    protected static List<string> listaIdPersonas {
        get { return (List<string>)HttpContext.Current.Session["GenerarAbonos_listaIdPersonas"]; }
        set { HttpContext.Current.Session["GenerarAbonos_listaIdPersonas"] = value; }
    }

    protected static int cantidadGenerados {
        get { return (int)HttpContext.Current.Session["GenerarAbonos_cantidadGenerados"]; }
        set { HttpContext.Current.Session["GenerarAbonos_cantidadGenerados"] = value; }
    }

    protected static int cantidadErrores {
        get { return (int)HttpContext.Current.Session["GenerarAbonos_cantidadErrores"]; }
        set { HttpContext.Current.Session["GenerarAbonos_cantidadErrores"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e) {

        resultadosAbonos = new ResultadosAbonosAGenerarViewModel(2);
        hdnFileYear.Value = DateTime.Now.Year.ToString();

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        listaIdPersonas = null;
        using (var dbContext = new ACHEEntities()) {
            //ddlPuntoVenta.DataSource = UsuarioCommon.ObtenerPuntosDeVenta(usu.IDUsuario);
            ddlPuntoVenta.DataSource = UsuarioCommon.ObtenerPuntosDeVenta(usu);
            ddlPuntoVenta.DataTextField = "Nombre";
            ddlPuntoVenta.DataValueField = "ID";
            ddlPuntoVenta.DataBind();
        }

        CondicionIva.Value = usu.CondicionIVA;

        hdnEnvioFE.Value = CurrentUser.EnvioAutomaticoComprobante ? "1" : "0";

        ddlModo.Items.Add(new ListItem("Cotización", "COT"));
        if (usu.TieneFE) {
            ddlModo.Items.Add(new ListItem("Facturación electrónica", "E"));
            ddlModo.SelectedValue = "E";
        }
        else
            ddlModo.Items.Add(new ListItem("Talonario preimpreso", "T"));

    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosAbonosAGenerarViewModel getResults(string fecha, string frecuencia, string nombre) {
        try {
            resultadosAbonos = new ResultadosAbonosAGenerarViewModel(2);

            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                listaIdPersonas = null;
                cantidadErrores = 0;
                cantidadGenerados = 0;
                var results = AbonosCommon.ObtenerAbonosParaGenerar(usu, fecha, frecuencia, nombre, ConfiguracionHelper.ObtenerCantidadDecimales());

                ResultadosAbonosAGenerarViewModel resultado = new ResultadosAbonosAGenerarViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                resultado.Items = results.Where(x => x.Cantidad > 0).ToList();

                if (usu.CondicionIVA == "RI") {
                    var puedeDarFacturaA = results.Any(x => x.Items.Count(y => y.CondicionIva == "RI") > 0);
                }

                resultadosAbonos = new ResultadosAbonosAGenerarViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                resultadosAbonos = resultado;
                return resultado;


        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    //private static string obtenerTipoDeFacturaAFacturar(string CondicionIva) {
    //    //TODO: PASAR A NEGOCIO
    //    string tipoDeFactura = string.Empty;
    //    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
    //    if (usu.CondicionIVA == "MO") {
    //        tipoDeFactura = "FCC";
    //    }
    //    else {
    //        switch (CondicionIva) {
    //            case "RI":
    //                tipoDeFactura = usu.CondicionIVA == "RI" ? "FCA" : "FCB";
    //                break;
    //            case "EX":
    //                tipoDeFactura = usu.CondicionIVA == "RI" ? "FCB" : "FCC";
    //                break;
    //            default:
    //                tipoDeFactura = usu.CondicionIVA == "RI" ? "FCB" : "FCC";
    //                break;
    //        }
    //    }
    //    return tipoDeFactura;
    //}

    [WebMethod(true)]
    public static object generarComprobanteAbono(string idAbonos, string modo, int idPuntoVenta, string numeroFacturaA, string numeroFacturaB, string numeroFacturaC, string fechaComprobante, string fechaVencimiento, int item, int desde, int hasta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
            int nroFacturaA = (numeroFacturaA == "") ? 0 : Convert.ToInt32(numeroFacturaA);
            int nroFacturaB = (numeroFacturaB == "") ? 0 : Convert.ToInt32(numeroFacturaB);
            int nroFacturaC = (numeroFacturaC == "") ? 0 : Convert.ToInt32(numeroFacturaC);

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            if (modo != "COT" && ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(fechaComprobante)))
                throw new CustomException("No puede agregar ni modificar un comprobante con fecha anterior a la fecha límite de carga.");

            if (listaIdPersonas == null) {
                listaIdPersonas = Regex.Split(idAbonos, "chkAbono_").ToList();
                listaIdPersonas.RemoveAt(0);
            }

            int idPersona;// id cliente Proveedor 
            string fecha = fechaComprobante;
            if (string.IsNullOrEmpty(fechaVencimiento))
                fechaVencimiento = DateTime.Now.AddDays(5).ToString("dd/MM/yyyy");
            string nroComprobante = string.Empty;
            string obs = string.Empty;
            string tipodeFactura = string.Empty;
            int idComprobante = 0;

            var puntoDeVenta = ObtenerPuntodeVenta(idPuntoVenta, usu.IDUsuario);
            var abono = resultadosAbonos.Items[item];

            var itemsHasta = hasta - desde;
            var listaAbonos = resultadosAbonos.Items[item].Items.Skip(desde).Take(itemsHasta + 1).ToList();

            foreach (var clienteAbono in listaAbonos) {
                clienteAbono.Estado = "Comprobante no seleccionado.";
                clienteAbono.FEGenerada = "";
                clienteAbono.nroComprobante = "";

                if (listaIdPersonas.Contains(clienteAbono.nroRegistro)) {
                    listaIdPersonas.Remove(clienteAbono.nroRegistro);
                    cantidadGenerados = cantidadGenerados + 1;
                    try {
                        idPersona = clienteAbono.IDPersona;
                        nroComprobante = "";
                        tipodeFactura = obtenerDatosFactura(ref nroFacturaA, ref nroFacturaB, ref nroFacturaC, ref nroComprobante, clienteAbono.CondicionIva, modo, idPuntoVenta);
                        idComprobante = generarComprobante(0, idPersona, tipodeFactura, modo, fecha, fechaVencimiento, idPuntoVenta, nroComprobante, clienteAbono, abono);

                        obs = abono.Observaciones;

                        clienteAbono.nroComprobante = tipodeFactura + "-" + puntoDeVenta + "-" + nroComprobante;
                        clienteAbono.Estado = "Comprobante generado.";

                        if (modo == "E") {
                            nroComprobante = "";
                            string afipObs = "";
                            bool enviarFC = false;

                            try {
                                Common.CrearComprobante(usu, idComprobante, idPersona, tipodeFactura, modo, fecha, clienteAbono.CondicionVenta, abono.tipoConcepto, fechaVencimiento, idPuntoVenta, ref nroComprobante, obs, Common.ComprobanteModo.Generar, ref afipObs);
                                clienteAbono.nroComprobante = nroComprobante;

                                var razonSocial = clienteAbono.RazonSocial.RemoverCaracteresParaPDF();
                                clienteAbono.URL = usu.IDUsuario + "/" + razonSocial + "_" + tipodeFactura + "-" + nroComprobante + ".pdf";
                                clienteAbono.FEGenerada = "Más opciones";
                                enviarFC = true;

                            }
                            catch (Exception ex) {
                                cantidadErrores = cantidadErrores + 1;
                                clienteAbono.FEGenerada = "";
                                clienteAbono.Estado = "ERROR: " + ex.Message;
                                ComprobanteCart.Retrieve().Items.Clear();

                                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                                msg = msg + "- USUARIO: " + usu.IDUsuario;
                                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
                            }

                            if (enviarFC && usu.EnvioAutomaticoComprobante) {
                                try {
                                    clienteAbono.EnvioFE = EnviarComprobanteAutomaticamente(idComprobante);
                                }
                                catch (Exception ex) {
                                    cantidadErrores = cantidadErrores + 1;
                                    //clienteAbono.FEGenerada = "";
                                    clienteAbono.Estado = "ERROR: No se puedo enviar el mail. Detalle: " + ex.Message;
                                    //ComprobanteCart.Retrieve().Items.Clear();

                                    var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                                    msg = msg + "- USUARIO: " + usu.IDUsuario;
                                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
                                }
                            }
                        }
                        //if (tipodeFactura != "COT")
                        GenerarAsientosContables(idComprobante);
                    }
                    catch (Exception ex) {
                        clienteAbono.FEGenerada = "";
                        clienteAbono.Estado = "ERROR: " + ex.Message;
                        ComprobanteCart.Retrieve().Items.Clear();

                        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                        msg = msg + "- USUARIO: " + usu.IDUsuario;
                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
                    }
                }

            }

            return new { Items = resultadosAbonos, Categoria = item, Desde = desde, Hasta = hasta, CantidadGenerados = cantidadGenerados, CantidadErrores = cantidadErrores };
    }

    private static string ObtenerPuntodeVenta(int idPunto, int idUsuario) {
        using (var dbContext = new ACHEEntities()) {
            var punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == idPunto && x.IDUsuario == idUsuario).FirstOrDefault();
            if (punto != null)
                return punto.Punto.ToString("#0000");
            else
                return "0000";
        }
    }

    private static void GenerarAsientosContables(int id) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            try {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ContabilidadCommon.AgregarAsientoDeVentas(usu, id);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }
    }

    private static int generarComprobante(int id, int idPersona, string tipodeFactura, string modo, string fecha, string fechaVencimiento, int idPuntoVenta, string nroComprobante, AbonosAGenerarViewModel clienteAbono, AbonosAGenerarGrupoViewModel abono) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            ComprobanteCart.Retrieve().Items.Clear();
            ComprobanteCart.Retrieve().Tributos.Clear();
            if (abono.Detalles.Count > 0) {
                foreach (var det in abono.Detalles) {
                    ComprobanteCart.Retrieve().Items.Add(new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales()) {
                        PrecioUnitario = det.PrecioUnitario,
                        Iva = det.Iva,
                        Concepto = det.Concepto,
                        IDConcepto = det.IDConcepto,
                        Cantidad = clienteAbono.Cantidad * det.Cantidad,
                        IDAbonos = abono.ID,
                        Bonificacion = det.Bonificacion,
                        IDPlanDeCuenta = (det.IDPlanDeCuenta == null) ? 0 : det.IDPlanDeCuenta,
                    });
                }
            }
            else {
                ComprobanteCart.Retrieve().Items.Add(new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales()) {
                    PrecioUnitario = clienteAbono.Importe,
                    Iva = clienteAbono.Iva,
                    Concepto = abono.Nombre,
                    Cantidad = clienteAbono.Cantidad,
                    Bonificacion = 0,
                    IDAbonos = abono.ID,
                    IDPlanDeCuenta = (abono.IDPlanDeCuenta == null) ? 0 : abono.IDPlanDeCuenta,
                });
            }

            ComprobanteCartDto compCart = new ComprobanteCartDto();
            compCart.IDComprobante = id;
            compCart.IDPersona = idPersona;
            compCart.TipoComprobante = tipodeFactura;
            compCart.TipoConcepto = abono.tipoConcepto.ToString();
            compCart.IDUsuario = usu.IDUsuario;
            compCart.Modo = modo;
            compCart.FechaComprobante = Convert.ToDateTime(fecha);
            compCart.FechaVencimiento = Convert.ToDateTime(fechaVencimiento);
            compCart.IDPuntoVenta = idPuntoVenta;
            compCart.Numero = nroComprobante;
            compCart.Observaciones = abono.Observaciones;
            compCart.IDCondicionVenta = clienteAbono.IDCondicionVenta;
            compCart.CondicionVenta = "";//SACAR DSPS
            //compCart.IDJuresdiccion = idJurisdiccion;

            compCart.Items = new List<ComprobantesDetalleViewModel>();
            compCart.Tributos = new List<ComprobantesTributosViewModel>();
            compCart.ImporteNoGrabado = ComprobanteCart.Retrieve().ImporteNoGrabado;
            compCart.PercepcionIVA = ComprobanteCart.Retrieve().PercepcionIVA;
            compCart.PercepcionIIBB = ComprobanteCart.Retrieve().PercepcionIIBB;

            compCart.Items = ComprobanteCart.Retrieve().Items;
            using (var dbContext = new ACHEEntities()) {
                var entity = ComprobantesCommon.GuardarComprobante(dbContext, compCart);
                return entity.IDComprobante;
            }

    }

    [WebMethod(true)]
    public static string EnviarComprobanteAutomaticamente(int idComprobante) {
        var resultado = string.Empty;
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            if (usu.EnvioAutomaticoComprobante == true) {
                using (var dbContext = new ACHEEntities()) {
                    var mensaje = "";
                    var mail = "";

                    try {
                        var plandeCuenta = PermisosModulosCommon.ObtenerPlanActual(dbContext, usu.IDUsuario);
                        if (plandeCuenta.IDPlan >= 3)//Plan Pyme
                        {
                            //bool CompEnviado = dbContext.ComprobantesEnviados.Any(x => x.IDUsuario == usu.IDUsuario && x.IDComprobante == idComprobante && x.Resultado == true);
                            //if (!CompEnviado)
                            //{
                            var avisos = dbContext.AvisosVencimiento.Where(x => x.IDUsuario == usu.IDUsuario && x.TipoAlerta == "Envio FE").FirstOrDefault();
                            if (avisos != null) {
                                var c = dbContext.Comprobantes.Include("PuntosDeVenta").Where(x => x.IDComprobante == idComprobante && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                                mail = string.IsNullOrEmpty(c.Personas.EmailsEnvioFc) ? c.Personas.Email : c.Personas.EmailsEnvioFc;

                                if (!string.IsNullOrEmpty(mail)) {
                                    mensaje = avisos.Mensaje.ReplaceAll("\n", "<br/>");
                                    var file = c.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_" + c.Tipo + "-" + c.PuntosDeVenta.Punto.ToString("#0000") + "-" + c.Numero.ToString("#00000000") + ".pdf";
                                    var emailUsuarioTo = !string.IsNullOrEmpty(usu.EmailAlerta) ? usu.EmailAlerta : usu.Email;

                                    // ComprobantesCommon.EnviarComprobantePorMail(c.Personas.RazonSocial, mail, avisos.Asunto, mensaje, c.FechaComprobante.Year, file, usu.Logo, usu.IDUsuario, usu.Email, usu.EmailDisplayFrom ?? usu.RazonSocial, c.IDComprobante);
                                    //ComprobantesCommon.GuardarComprobantesEnviados(dbContext, idComprobante, mensaje, null, "Abono enviado correctamente", true, usu, mail);
                                    ComprobantesCommon.GuardarEnvioComprobanteMail(c.Personas.RazonSocial, mail, avisos.Asunto, mensaje, c.FechaComprobante.Year, file, usu.Logo, usu.IDUsuario, emailUsuarioTo, usu.EmailDisplayFrom ?? usu.RazonSocial, c.IDComprobante);

                                }
                                else
                                    return "El cliente no tiene configurado el correo electrónico";
                            }
                            //}
                        }
                    }
                    catch (Exception ex) {
                        ComprobantesCommon.GuardarComprobantesEnviados(dbContext, idComprobante, mensaje, null, "Abonos: " + ex.Message, false, usu, mail);
                        resultado = "No se pudo enviar automáticamente la factura electrónonica";
                    }
                }
            }
            return resultado;

    }

    private static string obtenerDatosFactura(ref int numeroFacturaA, ref int numeroFacturaB, ref int numeroFacturaC, ref string nroComprobante, string CondicionIva, string modo, int idPuntoDeVenta) {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        var tipodeFactura = string.Empty;
        var _nroComprobante = 0;
        if (modo == "COT") {
            tipodeFactura = "COT";
            nroComprobante = ComprobantesCommon.ObtenerProxNroComprobante(tipodeFactura, usu.IDUsuario, idPuntoDeVenta);
        }
        else {
            tipodeFactura = ComprobantesCommon.ObtenerTipoDeFacturaAFacturar(CondicionIva);
            if (modo != "E") {
                switch (tipodeFactura) {
                    case "FCA":
                        _nroComprobante = numeroFacturaA;
                        numeroFacturaA++;
                        break;
                    case "FCB":
                        _nroComprobante = numeroFacturaB;
                        numeroFacturaB++;
                        break;
                    case "FCC":
                        _nroComprobante = numeroFacturaC;
                        numeroFacturaC++;
                        break;
                }
            }
            nroComprobante = _nroComprobante.ToString("#00000000");
        }
        return tipodeFactura;
    }
}
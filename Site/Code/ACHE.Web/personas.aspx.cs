﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using System.Web.Services;
using ACHE.Negocio.Common;

public partial class personas : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            var tipo = Request.QueryString["tipo"];
            hdnTipo.Value = tipo;

            if (tipo == "c")
                litTipo.Text = "cliente";
            else if (tipo == "p")
                litTipo.Text = "proveedor";

            if (CurrentUser.TipoUsuario == "B") {
                if (!PermisosModulos.mostrarPersonaSegunPermiso(tipo))
                    Response.Redirect("home.aspx");
            }
            if (tipo == "c") {
                litTitulo.Text = "<i class='fa fa-suitcase'></i> Clientes";
                litPath.Text = "Clientes";
            }
            else if (tipo == "p") {
                litTitulo.Text = "<i class='fa fa-users'></i> Proveedores";
                litPath.Text = "Proveedores";
            }
            else
                Response.Redirect("home.aspx");

            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
       CurrentUser.TipoUsuario);

            using (var dbContext = new ACHEEntities()) {
                var TieneDatos = dbContext.Personas.Any(x => x.IDUsuario == CurrentUser.IDUsuario && x.Tipo == tipo);
                if (TieneDatos) {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }
        }
    }

    [WebMethod(true)]
    public static void delete(int id) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            PersonasCommon.EliminarPersona(id, usu.IDUsuario);
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosPersonasViewModel getResults(string condicion, string tipo, int page, int pageSize) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return PersonasCommon.ObtenerPersonas(condicion, tipo, page, pageSize, usu.IDUsuario);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(string condicion, string tipo) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return PersonasCommon.ExportarPersonas(condicion, tipo, usu.IDUsuario);


    }
}
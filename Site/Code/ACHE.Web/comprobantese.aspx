﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="comprobantese.aspx.cs" Inherits="comprobantese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .modal.modal-wide .modal-dialog {
            width: 90%;
            max-width: 900px;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }

        .txtPercepciones {
            text-align: right;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-file-text"></i>Facturación </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/comprobantes.aspx">Facturación</a></li>
                <li class="active">
                    <asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>


    <div class="contentpanel">

        <div class="row">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="alert alert-danger" id="divError" style="display: none">
                        <strong>Lo sentimos! </strong><span id="msgError"></span>
                    </div>
                    <div class="alert alert-success" id="divOk" style="display: none">
                        <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                    </div>

                    <div class="panel-body">

                        <asp:Panel runat="server" ID="pnlContenido">

                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <h3>1. Datos generales</h3>
                                    <label class="control-label">Acá van los datos más generales de la factura.</label>
                                </div>
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-10 col-md-8 col-lg-5">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Proveedor/Cliente</label>
                                        <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <a href="#" class="btn btn-warning btn-add" data-toggle="modal" data-target="#modalNuevoCliente">
                                            <i class="glyphicon glyphicon-plus"></i> Nuevo cliente
                                        </a>
                                    </div>
                                </div>
                                
                            </div>
                            <div id="divFactura" style="display: none">
                                <div class="row mb15">
                                    <div class="col-sm-4 col-md-2">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Tipo de comprobante</label>
                                            <select class="form-control required" id="ddlTipo" onchange="changeTipoComprobante();" data-placeholder="Seleccione un tipo">
                                                <option value=""></option>
                                                <option value="FCA">Factura A</option>
                                                <option value="FCB">Factura B</option>
                                                <option value="FCC">Factura C</option>
                                                <option value="NCA">Nota crédito A</option>
                                                <option value="NCB">Nota crédito B</option>
                                                <option value="NCC">Nota crédito C</option>
                                                <option value="NDA">Nota débito A</option>
                                                <option value="NDB">Nota débito B</option>
                                                <option value="NDC">Nota débito C</option>
                                                <option value="COT">Cotización</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-2" id="divModo">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Modo</label>
                                            <asp:DropDownList runat="server" ID="ddlModo" CssClass="form-control required" onchange="changeModo();">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-2">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Condición de Venta</label>
                                            <select class="form-control required" id="ddlCondicionVenta" onchange="changeCondicionDeVenta();">
                                          <!--      <option value="Efectivo">Efectivo</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="Cuenta corriente">Cuenta corriente</option>
                                                <option value="MercadoPago">MercadoPago</option>
                                                <option value="Tarjeta de debito">Tarjeta de debito</option>
                                                <option value="Tarjeta de credito">Tarjeta de credito</option>
                                                <option value="Ticket">Ticket</option>
                                                 <option value="PayU">PayU</option>
                                                <option value="Otro">Otro</option>-->
                                            </select>
                                        </div>
                                    </div>
                                    <div class="md-full-width">
                                        <div class="col-sm-4 col-md-2">
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Punto de venta</label>
                                                <select id="ddlPuntoVenta" class="form-control required" onchange="changePuntoDeVenta();">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-2" id="divNroComprobante">
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Nro. Comprobante</label>
                                                <asp:TextBox runat="server" ID="txtNumero" CssClass="form-control required" MaxLength="9"></asp:TextBox>
                                            </div>
                                        </div>
                                          <div class="col-sm-4 col-md-3 col-lg-2 ">
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Depósito</label>
                                                <asp:DropDownList runat="server" ID="ddlInventarios" ClientIDMode="Static" CssClass="form-control required">
                                                    
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <%--<div class="col-sm-4 col-md-3 col-lg-2"  id="divOrdenVenta">
                                           <div class="form-group">
                                            <label class="control-label"> Orden de Venta</label>
                                               
                                               <select class="select3" data-placeholder="Ingrese el nro de orden" id="ddlNroDeOrden" runat="server"  onchange="cargarOrden();">
                                               <option value=""></option>
                                               </select>
                                            </div>
                                        </div>--%>
                                    </div>
                                </div>

                                <div class="row mb15">
                                    <div class="col-sm-12">
                                        <hr />
                                        <h3>2. Período facturado</h3>
                                        <label class="control-label">Acá van la fecha de emisión y la fecha de Vencimiento del cobro de la factura.</label>
                                    </div>
                                </div>
                                <div class="row mb15">
                                    <div class="col-sm-2 col-md-2 hide">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Producto o Servicio</label>
                                            <select id="ddlProducto" class="form-control required" onchange="Common.obtenerConceptosCodigoyNombre('ddlProductos', this.value, true);changeConcepto();">
                                                <option value="2">Servicio</option>
                                                <option value="1">Producto</option>
                                                <option value="3" selected="selected">Producto y servicio</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-2">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Fecha</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFecha" CssClass="form-control required validDate validFechaActual" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-2">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Vencimiento del cobro</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaVencimiento" CssClass="form-control required validDate" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div id="divPercepciones">
                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <hr />
                                            <h3>3. Percepciones especiales</h3>
                                            <label class="control-label">Estas son las percepciones especiales que aplican a tu perfil y data fiscal.</label>
                                        </div>
                                    </div>
                                    <div class="row row mb15">
                                        <div class="col-sm-4 col-md-2" id="divPercepcionesIVA">
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Percepción IVA %</label>
                                                <asp:TextBox runat="server" ID="txtPercepcionIVA" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static" Text="0" onBlur="obtenerTotales();"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div id="divPercepcionesIIBB">
                                            <div class="col-sm-4 col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Percepción IIBB %</label>
                                                    <asp:TextBox runat="server" ID="txtPercepcionIIBB" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static" Text="0" onBlur="obtenerTotales();"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Jurisdicción</label>
                                                    <asp:DropDownList ID="ddlJuresdiccion" runat="server" CssClass="select3" data-placeholder="Seleccione una jurisdicción">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb40"></div>

                                <div class="row mb15">
                                    <div class="col-sm-12">
                                        <hr />
                                        <h3 id="htextoFactura">Items a facturar</h3>
                                        <label class="control-label">Agrega a continuación los productos o servicios que vas a incluir en la factura.</label>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="alert alert-danger" id="divErrorDetalle" style="display: none">
                                        <strong>Lo siento! </strong><span id="msgErrorDetalle"></span>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                            <span class="asterisk">*</span> Cantidad
                                            <input type="text" class="form-control" maxlength="10" id="txtCantidad" />
                                        </div>
                                        <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                                            <span class="asterisk">*</span> Seleccione un Producto/Servicio
                                            <div class="row">
                                                <div class="col-sm-12 form-group">
                                                    <select class="select4" data-placeholder="Seleccione un Producto/Servicio" id="ddlProductos" onchange="changeConcepto();">
                                                    </select>
                                                </div>
                                            </div>
                                       </div>
                                       <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                                           o ingrese una descripción libre
                                           <div class="row">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" class="form-control" maxlength="500" id="txtConcepto" />
                                                </div>
                                            </div>
                                       </div>
                                       <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                            <span class="asterisk">*</span>
                                            <asp:Literal ID="liPrecioUnitario" runat="server"></asp:Literal>
                                            <input type="text" class="form-control" maxlength="10" id="txtPrecio" />
                                        </div>
                                        <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                            Bonif %
                                            <input type="text" class="form-control" maxlength="6" id="txtBonificacion" />
                                        </div>
                                        <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                            <span class="asterisk">*</span> IVA %
                                            <select class="form-control" id="ddlIva">
                                                <option value="0,00">0</option>
                                                <option value="2,50">2,5</option>
                                                <option value="5,00">5</option>
                                                <option value="10,50">10,5</option>
                                                <option value="21,00">21</option>
                                                <option value="27,00">27</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-lg-2 form-group divPlanDeCuentas" style="clear:left">
                                            <div class="form-group">
                                                <span class="asterisk">*</span> Cuenta contable
                                                <asp:DropDownList runat="server" ID="ddlPlanDeCuentas" CssClass="select3" data-placeholder="Seleccione una cuenta...">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 form-group">
                                            <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="agregarItem();">Agregar</a>
                                            <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="cancelarItem();">Cancelar</a>
                                            <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-invoice">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th style="text-align: right">Cantidad</th>
                                                <th style="text-align: left">Código</th>
                                                <th style="text-align: left">Descripción</th>
                                                <th style="text-align: right" id="tdPrecio">Precio unit. sin IVA</th>
                                                <th style="text-align: right">Bonif. %</th>
                                                <th style="text-align: right">IVA %</th>
                                                <th class="divPlanDeCuentas" style="text-align: left">Cuenta</th>
                                                <th style="text-align: right">Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyDetalle">
                                        </tbody>
                                    </table>
                                </div>
                                <!-- table-responsive -->
                                <div class="col-xs-12 col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Observaciones</label>
                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="6" ID="txtObservaciones" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-7">
                                    <table class="table table-total">
                                        <tbody>
                                            <tr id="trImpNoGrabado" class="hide">
                                                <td><strong>Neto no gravado :</strong></td>
                                                <%--<td id="trImpNoGrabadoTotal">$ 0</td>--%>
                                                <td id="Td1">
                                                    <asp:TextBox runat="server" ID="txtImporteNoGrabado" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static" Text="0" onBlur="obtenerTotales();"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Neto gravado:</strong></td>
                                                <td id="divSubtotal">$ 0</td>
                                            </tr>
                                            <tr>
                                                <td><strong>NO gravado/Exento:</strong></td>
                                                <td id="divNoGravado">$ 0</td>
                                            </tr>
                                            <tr>
                                                <td><strong>IVA:</strong></td>
                                                <td id="divIVA">$ 0</td>
                                            </tr>

                                            <tr id="trIVA">
                                                <td><strong>Percepción IVA :</strong></td>
                                                <td id="trIVATotal">$ 0</td>
                                                <%--<asp:TextBox runat="server" ID="txtPercepcionIVA" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static"></asp:TextBox></td>--%>
                                            </tr>
                                            <tr id="trIIBB">
                                                <td><strong>Percepción IIBB :</strong></td>
                                                <td id="trIIBBTotal">$ 0</td>
                                                <%--<asp:TextBox runat="server" ID="txtPercepcionIIBB" CssClass="form-control txtPercepciones" MaxLength="10" ClientIDMode="Static"></asp:TextBox></td>--%>
                                            </tr>

                                            <tr>
                                                <td><strong>TOTAL :</strong></td>
                                                <td id="divTotal" style="color: green">$ 0</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="mb40"></div>
                                <div class="row mb15">
                                    <div class="col-sm-12">
                                        <hr />
                                        <h5 class="subtitle">Usuario alta: <asp:Literal runat="server" ID="litUsuarioAlta"></asp:Literal></h5>
                                        <h5 class="subtitle">Usuario modifica: <asp:Literal runat="server" ID="litUsuarioModifica"></asp:Literal></h5>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>
                    </div>
                    <%--<div class="panel-footer" runat="server" id="divFooter">
                        <a class="btn btn-success" onclick="grabar(false);" id="lnkAceptar">Aceptar</a>
                        <asp:HyperLink runat="server" ID="lnkGenerarCAE" onclick="grabar(true);" CssClass="btn btn-info mr5"><i class="fa fa-dollar mr5"></i>Emitir factura eléctronica</asp:HyperLink>
                        <a href="#" onclick="cancelar();" style="margin-left: 20px">Cancelar</a>
                        <a class="btn btn-white" onclick="previsualizar();" style="float: right;" id="lnkPrevisualizar"><i class="fa fa-desktop fa-fw mr5"></i>Previsualizar en PDF</a>
                    </div>--%>
                     <div id="divFooter" runat="server" class="panel-footer responsive-buttons">
                        <a class="btn btn-success" onclick="grabar(false);" id="lnkAceptar">Aceptar</a>
                        <asp:HyperLink runat="server" ID="lnkGenerarCAE" onclick="grabar(true);" CssClass="btn btn-info mr5"><i class="fa fa-dollar mr5"></i>Emitir factura eléctronica</asp:HyperLink>
                        <a href="#" onclick="cancelar();" class="btn left-spacing">Cancelar</a>
                        <a class="btn btn-white float-right" onclick="previsualizar();" id="lnkPrevisualizar"><i class="fa fa-desktop fa-fw mr5"></i>Previsualizar en PDF</a>
                    </div>
                    
                    <asp:HiddenField runat="server" ID="hdnAnular" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnDuplicar" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnEnvioFE" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnUsaPlanCorporativo" Value="" />
                    <asp:HiddenField runat="server" ID="hdnCodigo" Value="" />
                    <asp:HiddenField runat="server" ID="hdnUsaPrecioConIVA" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnPercepcionIIBB" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnPercepcionIVA" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIDPrecarga" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIntegracion" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnVentaIntegracion" Value="" />
                    <asp:HiddenField runat="server" ID="hdnIDUsuario" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnFileYear" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIDPersona" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnIDCondicionVenta" Value="0" />
                    <asp:HiddenField runat="server" ID="hdnCobranzaAutomatica" Value="0" />

                    <asp:HiddenField runat="server" ID="hdnTieneFE" Value="0" ClientIDMode="Static" />
                       <asp:HiddenField runat="server" ID="hdnIDNroOrden" Value="0" />
                    
                    <input type="hidden" id="hdnRazonSocial" runat="server" />
                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalPdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Visualización de comprobante</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="alert alert-danger" id="divErrorCat" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgErrorCat"></span>
                        </div>

                        <iframe id="ifrPdf" src="" width="900px" height="500px" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" type="button" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal fade" id="modalOk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='comprobantes.aspx'">&times;</button>
                    <h4 class="modal-title" id="litModalOkTitulo">Comprobante emitido correctamente. </h4>
                </div>
                <div class="modal-body" style="min-height: 250px;">
                    <div class="alert alert-danger" id="divErrorAsientos" style="display: none">
                        <strong>Lo sentimos! </strong><span id="msgErrorAsientos"></span>
                    </div>
                    <div class="alert alert-success" id="divOkAsientos" style="display: none">
                        <strong>Bien hecho! </strong>Asientos Contables generados correctamente
                    </div>

                    <div class="alert alert-warning" id="divOkObservaciones" style="display: none">
                       
                    </div>

                    <div class="alert alert-danger" id="divErrorEnvioFE" style="display: none">
                        <strong>Lo sentimos! </strong><span id="msgErrorEnvioFE"></span>
                    </div>
                    <div class="alert alert-success" id="divOkMail" style="display: none">
                        <strong>Bien hecho! </strong>El mensaje ha sido enviado correctamente
                    </div>

                    <div id="divToolbar">
                        <div id="divSendMail" class="col-sm-3 CAJA_BLANCA_AZUL" onclick="Toolbar.mostrarEnvio();">
                            <a id="lnkSendMail">
                                <span class="glyphicon glyphicon-envelope" id="imgMailEnvio" style="font-size: 30px;"></span>
                                <br />
                                <span id="spSendMail">Enviar por Email</span>&nbsp;<i style="color: #17a08c" id="iCheckEnvio" title="El correo automático fue enviado correctamente"></i>
                            </a>
                        </div>
                        <div id="divDownloadPdf" class="col-sm-3 CAJA_BLANCA_AZUL">
                            <a href="#" id="lnkDownloadPdf" download>
                                <span class="fa fa-file-text" style="font-size: 35px;"></span>
                                <br />
                                Descargar en PDF
                            </a>
                        </div>
                        <div class="col-sm-3 CAJA_BLANCA_AZUL hide">
                            <i class="glyphicon glyphicon-usd" style="font-size: 30px; opacity: 1; border: none; padding: 0"></i>
                            <br />
                            Cobrar
                        </div>
                        <div id="divPrintpdf" class="col-sm-3 CAJA_BLANCA_AZUL" onclick="Common.imprimirArchivoDesdeIframe('');">
                            <a id="lnkPrintPdf">
                                <span class="glyphicon glyphicon-print" style="font-size: 30px;"></span>
                                <br />
                                Imprimir
                            </a>
                        </div>
                        <div id="divComprobante" class="col-sm-3 CAJA_BLANCA_AZUL" onclick="cobrarComprobante();">
                            <a id="lnkcobrarComprobante">
                                <i class="glyphicon glyphicon-usd" id="imgCobranza" style="font-size: 30px; opacity: 1; border: none; padding: 0"></i>
                                <br />
                                <span id="spRealizarCobranza">Realizar cobranza</span>&nbsp;<i style="color: #17a08c" id="iCheckCobranza" title="La cobranza fue generada correctamente"></i>
                                
                            </a>
                        </div>
                        <div id="divRemito" class="col-sm-3 CAJA_BLANCA_AZUL" onclick="generarRemito()">
                            <a id="lnkDownloadRemito">
                                <span class="fa fa-file-text" style="font-size: 35px;"></span>
                                <br />
                                Descargar Remito
                            </a>
                        </div>
                    
                        <div id="divDuplicar" class="col-sm-3 CAJA_BLANCA_AZUL" onclick="duplicar(hdnID.value);">
                            <a id="lnkDuplicar">
                                <i class="fa fa-files-o" style="font-size:50px"></i>
                                <br />
                                Duplicar
                            </a>
                        </div>
                    </div>
                    <div id="divSendEmail" style="display: none">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-danger" id="divErrorMail" style="display: none">
                                    <strong>Lo sentimos! </strong><span id="msgErrorMail"></span>
                                </div>
                                <form id="frmSendMail">
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Para: <small>(separa las direcciones mediante una coma) </small></p>
                                        <input id="txtEnvioPara" class="form-control required multiemails" type="text" runat="server" />
                                        <%--<span id="msgErrorEnvioPara" class="help-block" style="display: none">Una de las direcciones ingresadas es inválida.</span>--%>
                                    </div>
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Asunto: </p>
                                        <input id="txtEnvioAsunto" class="form-control required" type="text" maxlength="150" />
                                        <span id="msgErrorEnvioAsunto" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                    </div>
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Mensaje: </p>
                                        <textarea rows="5" id="txtEnvioMensaje" class="form-control required"></textarea>
                                        <span id="msgErrorEnvioMensaje" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                    </div>
                                    <input type="hidden" id="hdnFile" />
                                    <br />
                                    <a id="btnEnviar" type="button" class="btn btn-success" onclick="Toolbar.enviarComprobantePorMail();">Enviar</a>
                                    <a style="margin-left: 20px" href="#" onclick="Toolbar.cancelarEnvio();">Cancelar</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" type="button" data-dismiss="modal" onclick="window.location.href='comprobantes.aspx'">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/comprobantes.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <UC:NuevaPersona runat="server" ID="ucCliente" />

    <script>
        jQuery(document).ready(function () {
            configForm();
        });
    </script>
</asp:Content>

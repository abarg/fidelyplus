﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="presupuestose.aspx.cs" Inherits="presupuestose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .modal.modal-wide .modal-dialog {
            width: 102%;
            max-width: 900px;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-file-text"></i>Presupuestos </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/presupuestos.aspx">Presupuestos</a></li>
                <li class="active">
                    <asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Datos Principales</h3>
                                <label class="control-label">Estos son los datos básicos que el sistema necesita para poder generar un presupuesto.</label>
                            </div>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-10 col-md-8 col-lg-5">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Proveedor/Cliente</label>
                                    <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <a href="#" class="btn btn-warning btn-add" data-toggle="modal" data-target="#modalNuevoCliente">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-2 hide">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Nombre</label>
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--<div class="row mb15">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Proveedor/Cliente</label>
                                    <select class="select2 required" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                        <option value=""></option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#modalNuevoCliente" style="margin: 29px -10px; padding: 7px 15px">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-2 hide">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Nombre</label>
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                </div>
                            </div>

                        </div>--%>
                        <div class="row mb15">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Fecha</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        <asp:TextBox runat="server" ID="txtFechaEmision" CssClass="form-control required validDate" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Fecha validez</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        <asp:TextBox runat="server" ID="txtFechaValidez" CssClass="form-control required validDateFuture" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Numero</label>
                                    <asp:TextBox runat="server" ID="txtNumero" CssClass="form-control required" MaxLength="8"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Datos generales</h3>
                                <label class="control-label">Estos son los datos adicionales para poder gestionar su presupuesto</label>
                            </div>
                        </div>
                        <%--<div class="row mb15">
                            <div class="col-sm-12">
                                <hr />
                                <h3 id="htextoFactura">Items a presupuestar</h3>
                                <label class="control-label">Agrega a continuación los productos o servicios que vas a incluir en el presupuesto.</label>
                            </div>
                        </div>--%>

                        <div class="row mb15">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label">Estado</label>
                                    <asp:DropDownList runat="server" ID="ddlEstado" CssClass="form-control required" onchange="Presupuestos.showbtnfacturarPresupuesto();">
                                        <asp:ListItem Text="Borrador" Value="B"></asp:ListItem>
                                        <asp:ListItem Text="Enviado" Value="E"></asp:ListItem>
                                        <asp:ListItem Text="Aprobado" Value="A"></asp:ListItem>
                                        <asp:ListItem Text="Rechazado" Value="R"></asp:ListItem>
                                        <asp:ListItem Text="Facturado" Value="F"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label">Condiciones de pago</label>
                                    <select class="form-control required" id="ddlCondicionVenta">

                                       <!-- <asp:ListItem Text="Contado" Value="Contado"></asp:ListItem>
                                        <asp:ListItem Text="Transferencia" Value="Transferencia"></asp:ListItem>
                                        <asp:ListItem Text="Cheque al día" Value="Cheque al día"></asp:ListItem>
                                        <asp:ListItem Text="Cheque diferido" Value="Cheque diferido"></asp:ListItem>
                                        <asp:ListItem Text="A 15 días" Value="A 15 dias"></asp:ListItem>
                                        <asp:ListItem Text="A 30 días" Value="A 30 dias"></asp:ListItem>
                                        <asp:ListItem Text="A 60 días" Value="A 60 dias"></asp:ListItem>
                                        <asp:ListItem Text="50% y 50%" Value="50% y 50%"></asp:ListItem>
                                        <asp:ListItem Text="30%, 40% y 30%" Value="30%, 40% y 30%"></asp:ListItem>
                                        <asp:ListItem Text="A convenir" Value="A convenir" Selected="True"></asp:ListItem>-->
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2 col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Producto o Servicio</label>
                                    <asp:DropDownList runat="server" ID="ddlProducto" CssClass="form-control required"  onchange="Common.obtenerConceptosCodigoyNombre('ddlProductos', this.value, true);Presupuestos.changeConcepto();">
                                        <asp:ListItem Text="Servicio" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Producto" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Producto y servicio" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                    <%--<select id="ddlProducto" class="form-control required">
                                        <option value="2">Servicio</option>
                                        <option value="1">Producto</option>
                                        <option value="3" selected="selected">Producto y servicio</option>
                                    --%></select>
                                </div>
                            </div>
                        </div>


                        <div class="mb40"></div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <hr />
                                <h3 id="htextoFactura">Items a presupuestar</h3>
                                <label class="control-label">Agrega a continuación los productos o servicios que vas a incluir en el presupuesto.</label>
                            </div>
                        </div>
                        <div class="well">
                            <div class="alert alert-danger" id="divErrorDetalle" style="display: none">
                                <strong>Lo siento! </strong><span id="msgErrorDetalle"></span>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                    <span class="asterisk">*</span> Cantidad
                                    <input type="text" class="form-control" maxlength="10" id="txtCantidad" />
                                </div>
                                <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                                    <span class="asterisk">*</span> Seleccione un Producto/Servicio
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <select class="select4" data-placeholder="-" id="ddlProductos" onchange="Presupuestos.changeConcepto();">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-lg-3 form-group">
                                    o ingrese una descripción libre
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <input type="text" class="form-control" maxlength="500" id="txtConcepto" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                    <span class="asterisk">*</span>
                                    <asp:Literal ID="liPrecioUnitario" runat="server"></asp:Literal>
                                    <input type="text" class="form-control" maxlength="10" id="txtPrecio" />
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                    Bonif %
                                    <input type="text" class="form-control" maxlength="6" id="txtBonificacion" />
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-1 form-group">
                                    <span class="asterisk">*</span> IVA %
                                    <select class="form-control" id="ddlIva" runat="server">
                                        <option value="0,00">0</option>
                                        <option value="2,50">2,5</option>
                                        <option value="5,00">5</option>
                                        <option value="10,50">10,5</option>
                                        <option value="21,00">21</option>
                                        <option value="27,00">27</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-md-12 col-lg-12 form-group">
                                    <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="Presupuestos.agregarItem();">Agregar</a>
                                    <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="Presupuestos.cancelarItem();">Cancelar</a>
                                    <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                                </div>
                            </div>
                            <%--<div class="row">
                                <div class="col-sm-1 col-md-1">
                                    <span class="asterisk">*</span> Cantidad
                                    <input type="text" class="form-control" maxlength="5" id="txtCantidad" />
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <span class="asterisk">*</span> Producto/Servicio/Concepto
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-6">
                                            <select class="select3" data-placeholder="-" id="ddlProductos" onchange="Presupuestos.changeConcepto();">
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-lg-6">
                                            <input type="text" class="form-control" maxlength="200" id="txtConcepto" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-md-2">
                                    <span class="asterisk">*</span><asp:Literal ID="liPrecioUnitario" runat="server"></asp:Literal>
                                    <input type="text" class="form-control" maxlength="10" id="txtPrecio" />
                                </div>
                                <div class="col-sm-1 col-md-1">
                                    <span class="asterisk">*</span> Bonif %
                                            <input type="text" class="form-control" maxlength="6" id="txtBonificacion" />
                                </div>
                                <div class="col-sm-1 col-md-1">
                                    <span class="asterisk">*</span> IVA %
                                    <select class="select2" id="ddlIva" runat="server">
                                        <option value="0,00">0</option>
                                        <option value="2,50">2,5</option>
                                        <option value="5,00">5</option>
                                        <option value="10,50">10,5</option>
                                        <option value="21,00">21</option>
                                        <option value="27,00">27</option>
                                    </select>
                                </div>
                                <div class="col-sm-2 col-md-2">
                                    <br />
                                    <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="Presupuestos.agregarItem();">Agregar</a>
                                    <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="Presupuestos.cancelarItem();">Cancelar</a>
                                    <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                                </div>
                            </div>--%>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-invoice">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="text-align: right">Cantidad</th>
                                        <th style="text-align: left">Código</th>
                                        <th style="text-align: left">Descripción</th>
                                        <th style="text-align: right" id="tdPrecio">Precio unit. sin IVA</th>
                                        <th style="text-align: right">Bonif. %</th>
                                        <th style="text-align: right">IVA %</th>
                                        <th style="text-align: right">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody id="bodyDetalle">
                                    <tr>
                                        <td colspan='8'>No tienes items agregados</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- table-responsive -->
                        <div class="col-xs-12 col-sm-5">
                            <div class="form-group">
                                <label class="control-label">Observaciones</label>
                                <asp:TextBox runat="server" TextMode="MultiLine" Rows="6" ID="txtObservaciones" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7">
                            <table class="table table-total">
                                <tbody>
                                    <tr>
                                        <td><strong>Neto gravado:</strong></td>
                                        <td id="divSubtotal">$ 0</td>
                                    </tr>
                                    <tr>
                                        <td><strong>NO gravado/Exento:</strong></td>
                                        <td id="divNoGravado">$ 0</td>
                                    </tr>
                                    <tr>
                                        <td><strong>IVA:</strong></td>
                                        <td id="divIVA">$ 0</td>
                                    </tr>
                                    <tr>
                                        <td><strong>TOTAL :</strong></td>
                                        <td id="divTotal" style="color: green">$ 0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <hr />
                                <h5 class="subtitle">Usuario alta: <asp:Literal runat="server" ID="litUsuarioAlta"></asp:Literal></h5>
                                <h5 class="subtitle">Usuario modifica: <asp:Literal runat="server" ID="litUsuarioModifica"></asp:Literal></h5>
                            </div>
                        </div>

                        <div class="mb40"></div>

                    </div>
                    <div class="panel-footer responsive-buttons">
                        <a class="btn btn-success" id="btnActualizar" onclick="Presupuestos.grabar();">Aceptar</a>
                        <a id="btnFacturarPresupuesto" class="btn btn-primary" onclick="Presupuestos.facturarPresupuesto();" style="display: none">Facturar Presupuesto</a>
                        <a href="#" onclick="Presupuestos.cancelar();" style="margin-left: 20px">Cancelar</a>
                        <a class="btn btn-white" onclick="Presupuestos.previsualizar();" style="float: right;"><i class="fa fa-desktop fa-fw mr5"></i>Previsualizar en PDF</a>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnIDCondicionVenta" Value="0" />
                <asp:HiddenField runat="server" ID="hdnCodigo" Value="" />
                <asp:HiddenField runat="server" ID="hdnUsaPrecioConIVA" Value="0" />
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDPersona" Value="0" />
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalPdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 102%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Visualización de comprobante</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="alert alert-danger" id="divErrorCat" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgErrorCat"></span>
                        </div>

                        <iframe id="ifrPdf" src="" width="900px" height="500px" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal fade" id="modalOk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.href='presupuestos.aspx'">&times;</button>
                    <h4 class="modal-title" id="litModalOkTitulo">Comprobante emitido correctamente. </h4>
                </div>
                <div class="modal-body" style="min-height: 200px;">
                    <div class="alert alert-success" id="divOkMail" style="display: none">
                        <strong>Bien hecho! </strong>El mensaje ha sido enviado correctamente
                    </div>
                    <div id="divToolbar">
                        <div id="divRemito" class="col-sm-3 CAJA_BLANCA_AZUL">
                            <a href="#" id="lnkDownloadRemito" onclick="Presupuestos.generarRemito();">
                                <span class="fa fa-file-text" style="font-size: 35px;"></span>
                                <br />
                                Descargar Remito
                            </a>
                        </div>
                        <div id="divDownloadPdf" class="col-sm-3 CAJA_BLANCA_AZUL">
                            <a href="#" id="lnkDownloadPresupuestoPdf" onclick="Presupuestos.generarPresupuesto();">
                                <span class="fa fa-file-text" style="font-size: 35px;"></span>
                                <br />
                                Descargar presupuesto
                            </a>
                        </div>
                        <div class="col-sm-3 CAJA_BLANCA_AZUL" onclick="Common.imprimirArchivoDesdeIframe('');">
                            <a id="lnkPrintPdf">
                                <span class="glyphicon glyphicon-print" style="font-size: 30px;"></span>
                                <br />
                                Imprimir
                            </a>
                        </div>
                        <div id="divDuplicar" class="col-sm-3 CAJA_BLANCA_AZUL" onclick="Presupuestos.duplicar(hdnID.value);">
                            <a id="lnkDuplicar">
                                <i class="fa fa-files-o" style="font-size:50px"></i>
                                <br />
                                Duplicar
                            </a>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal" onclick="window.location.href='presupuestos.aspx'">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/presupuestos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <UC:NuevaPersona runat="server" ID="ucCliente" />

    <script>
        jQuery(document).ready(function () {
            Presupuestos.configForm();
        });
    </script>

</asp:Content>

﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using System.Web.Services;
using ACHE.Negocio.Presupuesto;
using System.Data.Entity;
using ACHE.Negocio.Common;

public partial class presupuestos : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            //txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            //txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");

            using (var dbContext = new ACHEEntities()) {
                var TieneDatos = dbContext.Presupuestos.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos) {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
              CurrentUser.TipoUsuario);
        }
    }

    [WebMethod(true)]
    public static void delete(int id) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            PresupuestosCommon.EliminarPresupuesto(id, usu.IDUsuario);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosPresupuestosViewModel getResults(string condicion, string periodo,
        string fechaDesde, string fechaHasta, int page, int pageSize) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return PresupuestosCommon.ObtenerPresupuesto(condicion, periodo, fechaDesde, fechaHasta, page, pageSize, usu.IDUsuario);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(string condicion, string periodo, string fechaDesde, string fechaHasta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return PresupuestosCommon.ExportarPresupuestos(usu.IDUsuario, condicion, periodo, fechaDesde, fechaHasta);

    }
}
﻿
var cantidadDecimales = 2;

var AplicarPagosACuenta = {
    cancelar: function() {
        window.location.href = "/modulos/compras/pagos.aspx";
    },

    /*** SEARCH ***/
    configFilters: function () {
        $("#lnkAceptar").hide();
        //$(".select2").select2({ width: '100%', allowClear: true });
     
        Common.obtenerPersonas("ddlPersona", "", true);
        $("#ddlPersona").attr("onchange", "changePersona()");

        $("#txtCondicion").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                AplicarPagosACuenta.resetearPagina();
                AplicarPagosACuenta.filtrar();
                return false;
            }
        });
    },
    aplicar: function () {

        Common.mostrarProcesando("lnkAceptar");
        var list = $(".ddlComprobantes");
        var result = [];
        $.each(list, function (i, val) {
            if ($("#" + val.id + " option:selected").val() != -1)
                 result.push({idPagoACuenta: val.id.replace("ddlPagos", ""),idCompra: $("#" + val.id + " option:selected").val()});
        });
        if (result.length > 0) {
            var info = {
                idPersona: parseInt($("#ddlPersona").val()),
                pagosAcuenta: result
            };

            $.ajax({
                type: "POST",
                url: "AplicarPagosACuenta.aspx/AplicarPagosACuenta",
                data: JSON.stringify(info),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    AplicarPagosACuenta.filtrar();
                    Common.ocultarProcesando("lnkAceptar", "Aplicar");
                },
                error: function(response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $('#divOk').hide();
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    Common.ocultarProcesando("lnkAceptar", "Aplicar");
                }
            });
        } else {
            $("#msgError").html("Debe seleccionar al menos un comprobante.");
            $("#divError").show();
            Common.ocultarProcesando("lnkAceptar", "Aplicar");
        }
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        AplicarPagosACuenta.filtrar();
    },
    mostrarPagProxima: function (id, nombre) {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        AplicarPagosACuenta.filtrar();
    },
    filtrar: function () {
        $("#divError").hide();

        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");

            var currentPage = parseInt($("#hdnPage").val());

        

            var info = "{ idPersona: '" + parseInt($("#ddlPersona").val())
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + "}";

            
            $.ajax({
                type: "POST",
                url: "AplicarPagosACuenta.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.TotalPage > 0) {
                        if (data.d.TotalItems>0)
                            $("#lnkAceptar").show();
                        else
                            $("#lnkAceptar").hide();
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);

                       
                    }
                    else {
                        $("#lnkAceptar").hide();
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    $("#resultsContainer").empty();
                    // Render using the template
                    if (data.d.Items.length > 0)
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                   
                    $.ajax({
                        type: "POST",
                        url: "/modulos/compras/pagose.aspx/obtenerComprasPendientes",
                        data: "{ id: " + parseInt($("#ddlPersona").val()) + ", idPago: 0 }",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (comprobantes) {
                            if (comprobantes != null) {
                                $(".ddlComprobantes").empty();
                               

                                for (var i = 0; i < comprobantes.d.length; i++) {
                                    $("<option/>").attr("value", comprobantes.d[i].ID).text(comprobantes.d[i].Nombre).appendTo($(".ddlComprobantes"));
                                }

                                $(".ddlComprobantes").val("-1");
                            }
                        }
                    });
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
        }
    },
    verTodos: function () {
        $("#txtNumero, #txtFechaDesde, #txtFechaHasta").val("");//, #ddlTipo
        $("#ddlPersona").val("").trigger("change");
        AplicarPagosACuenta.filtrar();
    },
    
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        AplicarPagosACuenta.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        AplicarPagosACuenta.filtrar();
    },
    resetearPagina: function () {
        $('#divOk').hide();
        $('#lnkAceptar').hide();
        $("#divError").hide();
        $("#hdnPage").val("1");
        $("#resultsContainer").empty();
        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
    }
}

function changePersona() {
    $('#divOk').hide();
    $("#divError").hide();
    if ($("#ddlPersona").val()) {
        AplicarPagosACuenta.filtrar();
    } else {
        AplicarPagosACuenta.resetearPagina();
    }
}

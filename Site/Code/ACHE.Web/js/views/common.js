﻿/*** GLOBAL VARIABLES ***/
var PAGE_SIZE = 50;

/*** METHODS ***/

function toDotandComma(number) {
    return number.toLocaleString("es-ES", { minimumFractionDigits: 2 })
}

function enviarAyuda() {
    $("#divAyudaError").hide();

    if ($("#txtAyudaMensaje").val() == "") {
        $("#msgAyudaError").html("Por favor, describenos tu consulta y/o problema");
        $("#divAyudaError").show();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    else {

        var info = "{ mensaje: '" + $("#txtAyudaMensaje").val() + "'}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/ayuda",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtAyudaMensaje").val("");
                $("#divAyudaError").hide();

                $('#modalAyuda').modal('hide');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgAyudaError").html(r.Message);
                $("#divAyudaError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function enviarSugerencia() {
    $("#divAyudaError").hide();

    if ($("#txtAyudaMensaje").val() == "") {
        $("#msgAyudaError").html("Por favor, describenos tu sugerencia");
        $("#divAyudaError").show();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    else {

        var info = "{ mensaje: '" + $("#txtAyudaMensaje").val() + "'}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/sugerencia",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#txtAyudaMensaje").val("");
                $("#divAyudaError").hide();

                $('#modalSugerencia').modal('hide');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgAyudaError").html(r.Message);
                $("#divAyudaError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

function obtenerValorDecimalDefault(cantidadDecimales, valor) {
    var valorDefault = "";
    switch (cantidadDecimales) {
        case 1:
            valorDefault = valor.toString() + ".0";
            break;
        case 2:
            valorDefault = valor.toString() + ".00";
            break;
        case 3:
            valorDefault = valor.toString() + ".000";
            break;
        case 4:
            valorDefault = valor.toString() + ".0000";
            break;
        default:
            valorDefault = valor.toString() + ".00";
            break;
    }
    return valorDefault;
}

function obtenerCantidadDecimales(fnCallback) {
    $.ajax({
        type: "GET",
        url: "/common.aspx/ObtenerCantidadDecimales",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (serverData) {
            if (serverData != null) {
                fnCallback(serverData);
            }
        }
    });
}

function obtenerPuntosDeVenta(controlName) {
    $.ajax({
        type: "GET",
        url: "/common.aspx/obtenerPuntosDeVenta",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }
            }
        }
    });
}

function obtenerCondicionesDeVenta(controlName, idSelected) {
    $.ajax({
        type: "GET",
        url: "/common.aspx/obtenerCondicionesDeVenta",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }
                if (idSelected != "" && idSelected != "0")
                    $("#" + controlName).val(idSelected);
                else
                    $("#" + controlName + " option:contains('Efectivo')").attr('selected', true);

                //$("#" + controlName).trigger("change");

            }
        }
    });
}
function obtenerPuntosDeVentaIntegracion(controlName) {
    $.ajax({
        type: "GET",
        url: "/common.aspx/obtenerPuntosDeVentaIntegracion",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }
            }
        }
    });
}

function obtenerPuntosDeVentaYNroCobranza(controlName) {
    $.ajax({
        type: "GET",
        url: "/common.aspx/obtenerPuntosDeVenta",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }

                Common.obtenerUltimoNroRecibo("txtNumero", $("#ddlTipo").val(), parseInt($("#ddlPuntoVenta").val()));
            }
        }
    });
}

function obtenerCategorias(controlName, showEmpty) {
    $.ajax({
        type: "GET",
        url: "/common.aspx/obtenerCategorias",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                if (showEmpty)
                    $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }

                $("#" + controlName).trigger("change");
            }
        }
    });
}

function obtenerConceptos(controlName, tipo, showEmpty) {

    var aux = "";
    if (tipo == 1)
        aux = "P";
    else if (tipo == 2)
        aux = "S";

    $("#" + controlName).html("");

    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerConceptos",
        data: "{tipo: '" + aux + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                if (showEmpty)
                    $("<option/>").attr("value", "").text("-").appendTo($("#" + controlName));

                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }

                $("#" + controlName).trigger("change");
            }
            else {
                $("<option/>").attr("value", "").text("-").appendTo($("#" + controlName));
                $("#" + controlName).trigger("change");
            }
        }
    });
}

function obtenerPersonas(controlName, idSelected, showEmpty) {
    //$.ajax({
    //    type: "GET",
    //    url: "/common.aspx/obtenerPersonas",
    //    async: false,
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (data) {
    //        if (data != null) {
    //            $("#" + controlName).html("");

    //            if (showEmpty)
    //                $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

    //            for (var i = 0; i < data.d.length; i++) {
    //                $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
    //            }

    //            if (idSelected != "")
    //                $("#" + controlName).val(idSelected);

    //            //$("#" + controlName).trigger("change");
    //            $("#" + controlName).trigger("change");
    //            //$("#" + controlName).select2({width: '100%'});
    //        }
    //    }
    //}); 

    //if (idSelected != "")
    //    $("<option/>").attr("value", idSelected).text("test " + idSelected).appendTo($("#" + controlName));

    $("#" + controlName).select2({
        ajax: {
            type: "GET",
            url: "/common.aspx/obtenerPersonasFilter",
            contentType: "application/json; charset=utf-8",
            data: function (params) {


                var query = {
                    search: "'" + params.term + "'",
                    page: params.page || 1

                }


                // Query parameters will be ?search=[term]&page=[page]
                return query;
            },
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.d.Items,
                    pagination: {
                        more: (params.page * 30) < data.d.TotalItems
                    }
                };
            }
        },
        placeholder: 'Seleccione un cliente/proveedor...',
        minimumInputLength: 2,
        allowClear: true,
        width: '100%',
        theme: 'default'
    });

    //$(".select2-selection").css({ height: "40px", padding:'5px' });


    if (idSelected != "" && idSelected != "0") {
        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerPersonaSelected?id=" + idSelected,
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null)
                    $("#" + controlName).empty().append("<option value=" + idSelected + ">" + data.d.Nombre + "</option>").val(idSelected).trigger('change');

            }
        });

        //$("#" + controlName).empty().append("<option value=" + idSelected + ">" + showEmpty + "</option>").val(idSelected).trigger('change');
        //$("#" + controlName).select2("trigger", "select", {
        //    data: { id: "5",text:"aa" }
        //});
    }

    /* alert(idSelected);
    
         $("#" + controlName).select2({ width: '100%' });
         $("#" + controlName).trigger("change");
 
     }*/

}

function obtenerNrosOrdenes(controlName, idSelected, showEmpty, idProveedor, conciliado) {
    if (idProveedor == "")
        idProveedor = "0";
    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerOrdenes",
        data: "{idProveedor: " + parseInt(idProveedor) + ", conciliado: "+ conciliado +"}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#" + controlName).html("");

                if (showEmpty)
                    $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }

                if (idSelected != "")
                    $("#" + controlName).val(idSelected);

                //$("#" + controlName).trigger("change");
                $("#" + controlName).trigger("change");
                //$("#" + controlName).select2({width: '100%'});
                if(conciliado)
                    $("#" + controlName).prop('disabled', true);

            }
        }
    });
}

function obtenerJuridicciones(controlName) {
    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerJurisdiccionUsuario",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#" + controlName).html("");
                $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));
                for (var i = 0; i < data.d.length; i++) {
                    $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                }
            }
        }
    });
}

function obtenerProxNroComprobante(controlName, tipo, idpuntoDeVenta) {

    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerProxNroComprobante",
        data: "{tipo: '" + tipo + "', idPuntoDeVenta: " + idpuntoDeVenta + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#" + controlName).val(data.d);
            }
        }
    });
}

function obtenerProxNroCobranza(controlName, tipo) {

    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerProxNroCobranza",
        data: "{tipo: '" + tipo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#" + controlName).val(data.d);
            }
        }
    });
}

function padZeros(str, max) {
    str = str.toString();
    return str.length < max ? padZeros("0" + str, max) : str;
}


/*** VALIDATIONS ***/
function validateEmail(field) {
    var regex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i;
    return (regex.test(field)) ? true : false;
}

function EmailsAreValid(value) {
    var result = value.split(",");
    for (var i = 0; i < result.length; i++)
        if (!validateEmail(result[i]))
            return false;
    return true;
}


function remplazarCaracteresEspeciales(nombre) {

    var valor = $("#" + nombre).val();
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
    for (var i = 0; i < specialChars.length; i++) {
        valor = valor.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
    }
    $("#" + nombre).val(valor);
}

function remplazarCaracteresEspeciales2(nombre) {

    var valor = $("#" + nombre).val();
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,";
    for (var i = 0; i < specialChars.length; i++) {
        valor = valor.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
    }
    $("#" + nombre).val(valor);
}

function CuitEsValido(value) {
    var cuit = value;
    if (typeof (cuit) == 'undefined')
        return true;

    cuit = cuit.toString().replace(/[-_]/g, "");
    if (cuit == '')
        return true; //No estamos validando si el campo esta vacio, eso queda para el "required"

    if (cuit.length != 11)
        return false;
    else {

        var mult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
        var total = 0;

        for (var i = 0; i < mult.length; i++) {
            total += parseInt(cuit.charAt(i)) * mult[i];
        }

        var mod = total % 11;
        var digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
    }

    return digito == parseInt(cuit.charAt(10));
}

function configDatePicker() {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $('.validDate').datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        maxDate: '0'
    });

    $('.validDateFuture').datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true
        //maxDate: '0'
    });

    Common.configFechas('dd/mm/yy');
}
/*
function configFechas(format) {
    $.validator.addMethod("validDate", function (value, element) {
        var check = false;
        var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (format == 'mm/yy')
            re = /^\d{1,2}\/\d{4}$/;
        if (re.test(value)) {
            var adata = value.split('/');
            var dd = 0;
            var mm = 0;
            var yyyy = 1900;
            if (format != 'mm/yy') {
                dd = parseInt(adata[0], 10);
                mm = parseInt(adata[1], 10);
                yyyy = parseInt(adata[2], 10);
            }
            else {
                dd = 1;
                mm = parseInt(adata[0], 10);
                yyyy = parseInt(adata[1], 10);
            }

            var xdata = new Date(yyyy, mm - 1, dd);

            if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd))
                check = true;
            else
                check = false;
        }
        else
            check = false;
        return this.optional(element) || check;

    }, "La fecha no es válida.");
}

function configFechasDesdeHasta(date, end) {

    $.validator.addMethod("greaterThan", function () {
        var valid = true;
        var desde = $("#" + date).val();
        var hasta = $("#" + end).val();
        if (isNaN(hasta) && isNaN(desde)) {
            var fDesde = parseEnDate(desde);
            var fHasta = parseEnDate(hasta);
            if (fDesde > fHasta) {
                valid = false;
            }
        }
        return valid;
    }, "La fecha desde es mayor a la fecha hasta");
    $('form').validate({
        errorClass: 'error',
        validClass: 'valid',
        highlight: function (element) {
            $(element).closest('div').addClass("f_error");
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass("f_error");
        },
        errorPlacement: function (error, element) {
            $(element).closest('div').append(error);
        }
    });

    //$("#" + date).datepicker({ dateFormat: 'dd/mm/yy' });
    //$("#" + end).datepicker({ dateFormat: 'dd/mm/yy' });

    $('#' + date).change(cleanErrors);
    $('#' + end).change(cleanErrors);
}
*/
function cleanErrors() {
    $('form').validate();
    if ($('form').valid()) {
        $('label .error').hide();
    }
}

function parseEnDate(value) {
    var bits = value.match(/([0-9]+)/gi), str;
    str = bits[1] + '/' + bits[0] + '/' + bits[2];
    return new Date(str);
}

var Common = {
    configDatePicker: function () {
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);

        $('.validDate').datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            maxDate: '0'
        });

        $('.validDateFuture').datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true
            //maxDate: '0'
        });

        Common.configFechas();
    },
    configDateTimePicker: function () {
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy hh:ii',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);

        $('.validDate').timepicker({
            dateFormat: "dd/mm/yy hh:ii",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            maxDate: '0'
        });

        Common.configFechas();
    },
    configDatePickerDosMeses: function () {
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            numberOfMonths: 2,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);

        $('.validDate').datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            maxDate: '0'
        });

        Common.configFechas();
    },
    configFechas: function (format) {
        $.validator.addMethod("validDate", function (value, element) {
            /*var bits = value.match(/([0-9]+)/gi), str;
            if (!bits) return this.optional(element) || false; str = bits[1] + '/' + bits[0] + '/' + bits[2];
            return this.optional(element) || !/Invalid|NaN/.test(new Date(str));*/

            var check = false;
            var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
            if (format == 'mm/yy')
                re = /^\d{1,2}\/\d{4}$/;
            if (re.test(value)) {
                var adata = value.split('/');
                var dd = 0;
                var mm = 0;
                var yyyy = 1900;
                if (format != 'mm/yy') {
                    dd = parseInt(adata[0], 10);
                    mm = parseInt(adata[1], 10);
                    yyyy = parseInt(adata[2], 10);
                }
                else {
                    dd = 1;
                    mm = parseInt(adata[0], 10);
                    yyyy = parseInt(adata[1], 10);
                }

                var xdata = new Date(yyyy, mm - 1, dd);

                if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd))
                    check = true;
                else
                    check = false;
            }
            else
                check = false;
            return this.optional(element) || check;

        }, "La fecha no es válida.");

        $.validator.addMethod("validDateFuture", function (value, element) {
            /*var bits = value.match(/([0-9]+)/gi), str;
            if (!bits) return this.optional(element) || false; str = bits[1] + '/' + bits[0] + '/' + bits[2];
            return this.optional(element) || !/Invalid|NaN/.test(new Date(str));*/

            var check = false;
            var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
            if (format == 'mm/yy')
                re = /^\d{1,2}\/\d{4}$/;
            if (re.test(value)) {
                var adata = value.split('/');
                var dd = 0;
                var mm = 0;
                var yyyy = 1900;
                if (format != 'mm/yy') {
                    dd = parseInt(adata[0], 10);
                    mm = parseInt(adata[1], 10);
                    yyyy = parseInt(adata[2], 10);
                }
                else {
                    dd = 1;
                    mm = parseInt(adata[0], 10);
                    yyyy = parseInt(adata[1], 10);
                }

                var xdata = new Date(yyyy, mm - 1, dd);

                if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd))
                    check = true;
                else
                    check = false;
            }
            else
                check = false;
            return this.optional(element) || check;

        }, "La fecha no es válida.");
    },
    configFechasDesdeHasta: function (date, end) {
        $.validator.addMethod("greaterThan", function () {
            var valid = true;
            var desde = $("#" + date).val();
            var hasta = $("#" + end).val();
            if (isNaN(hasta) && isNaN(desde)) {
                var fDesde = parseEnDate(desde);
                var fHasta = parseEnDate(hasta);
                if (fDesde > fHasta) {
                    valid = false;
                }
            }
            return valid;
        }, "La fecha desde es mayor a la fecha hasta");
        $('form').validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        //$("#" + date).datepicker({ dateFormat: 'dd/mm/yy' });
        //$("#" + end).datepicker({ dateFormat: 'dd/mm/yy' });

        $('#' + date).change(cleanErrors);
        $('#' + end).change(cleanErrors);
    },
    GetURLParameter: function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
    obtenerPersonas: function (controlName, idSelected, showEmpty) {
        obtenerPersonas(controlName, idSelected, showEmpty);
    },
    obtenerNrosOrdenes: function (controlName, idSelected, showEmpty, idProveedor, conciliado) {
        obtenerNrosOrdenes(controlName, idSelected, showEmpty, idProveedor, conciliado);
    },
    obtenerInventarios: function (controlName, showEmpty, soloActivos) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerInventarios",
            data: "{soloActivos: " + soloActivos + "}",
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");
                    if (data.d.length > 1 && showEmpty)
                        $("<option/>").attr("value", "0").text("Seleccione un depósito").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");

                    if (data.d.length <= 1)
                        $(".inventario").hide();
                }
            }
        });
    },
    obtenerInventariosConLeyenda: function (controlName, showEmpty, soloActivos, leyenda) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerInventarios",
            data: "{soloActivos: " + soloActivos + "}",
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");
                    if (data.d.length > 1 && showEmpty)
                        $("<option/>").attr("value", "0").text(leyenda).appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");

                    if (data.d.length <= 1)
                        $(".inventario").hide();
                }
            }
        });
    },
    obtenerInventariosActivos: function (controlName, showEmpty) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerInventariosActivos",
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");
                    if (data.d.length > 1 && showEmpty)
                        $("<option/>").attr("value", "0").text("Seleccione un depósito").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (data.d.length <= 1) {
                        $("#" + controlName).val(data.d[0].ID);
                        $(".inventario").hide();
                    }
                    else
                        $(".inventario").show();
                }
            }
        });
    },
    obtenerJuridicciones: function (controlName) {
        obtenerJuridicciones(controlName);
    },
    obtenerComprobantesVentaPorCondicion: function (controlName, condicionIva, selectedValue) {

        if (MI_CONDICION == "RI") {
            if (condicionIva == "CF" || condicionIva == "EX" || condicionIva == "MO") {
                $("#" + controlName).append('<option value="FCB">Factura B</option>');
                $("#" + controlName).append('<option value="NDB">Nota débito B</option>');

                $("#" + controlName).append('<option value="NCB">Nota crédito B</option>');
                //$("#" + controlName).append('<option value="RCB">Recibo B</option>');
            }
            else {
                $("#" + controlName).append('<option value="FCA">Factura A</option>');
                //$("#" + controlName).append('<option value="FCB">Factura B</option>');
                $("#" + controlName).append('<option value="NDA">Nota débito A</option>');
                //$("#" + controlName).append('<option value="NDB">Nota débito B</option>');

                $("#" + controlName).append('<option value="NCA">Nota crédito A</option>');
                //$("#" + controlName).append('<option value="NCB">Nota crédito B</option>');
                //$("#" + controlName).append('<option value="RCA">Recibo A</option>');
                //$("#" + controlName).append('<option value="RCB">Recibo B</option>');
            }
            //$("#" + controlName).append('<option value="COT">Cotización</option>');
        }
        else if (MI_CONDICION == "MO" || MI_CONDICION == "EX") {
            $("#" + controlName).append('<option value="FCC">Factura C</option>');
            $("#" + controlName).append('<option value="NDC">Nota débito C</option>');

            $("#" + controlName).append('<option value="NCC">Nota crédito C</option>');
            //$("#" + controlName).append('<option value="RCC">Recibo C</option>');
        }
        //else if (MI_CONDICION == "EX") {
        //    $("#" + controlName).append('<option value="FCB">Factura B</option>');
        //    $("#" + controlName).append('<option value="NDB">Nota débito B</option>');
        //    //$("#" + controlName).append('<option value="RCB">Recibo B</option>');
        //}
        $("#" + controlName).append('<option value="COT">Cotización</option>');
        //$("#" + controlName).append('<option value="TIC">Ticket</option>');

        if (selectedValue != "")
            $("#" + controlName).val(selectedValue);
    },
    obtenerNotaCreditoAnulacion: function (controlName, tipoFC) {

        var selectedValue = "";
        if (tipoFC == "FCB") {
            if ($("#" + controlName + " option[value='NCB']").length == 0)
                $("#" + controlName).append('<option value="NCB">Nota crédito B</option>');
            selectedValue = "NCB";
        }
        else if (tipoFC == "FCA") {
            if ($("#" + controlName + " option[value='NCA']").length == 0)
                $("#" + controlName).append('<option value="NCA">Nota crédito A</option>');
            selectedValue = "NCA";
        }
        else if (tipoFC == "FCC") {
            if ($("#" + controlName + " option[value='NCC']").length == 0)
                $("#" + controlName).append('<option value="NCC">Nota crédito C</option>');
            selectedValue = "NCC";
        }
        else if (tipoFC == "FCM") {
            if ($("#" + controlName + " option[value='NCM']").length == 0)
                $("#" + controlName).append('<option value="NCM">Nota crédito M</option>');
            selectedValue = "NCM";
        }
        if (selectedValue != "") {
            $("#" + controlName).val(selectedValue);
            //alert(selectedValue);
        }

    },
    obtenerComprobantesPagoPorCondicion: function (controlName, condicionIva, selectedValue) {
        //if (MI_CONDICION == "RI") {
        if (condicionIva == "MO" || condicionIva == "EX") {
            $("#" + controlName).append('<option value="FCC">Factura C</option>');
            $("#" + controlName).append('<option value="NCC">Nota crédito C</option>');
            $("#" + controlName).append('<option value="NDC">Nota débito C</option>');
            //$("#" + controlName).append('<option value="RCC">Recibo C</option>');
        }
        else if (condicionIva == "RI") {
            $("#" + controlName).append('<option value="FCA">Factura A</option>');
            $("#" + controlName).append('<option value="FCB">Factura B</option>');
            $("#" + controlName).append('<option value="FCE">Factura E</option>');
            $("#" + controlName).append('<option value="FCM">Factura M</option>');
            $("#" + controlName).append('<option value="NCA">Nota crédito A</option>');
            $("#" + controlName).append('<option value="NCB">Nota crédito B</option>');
            $("#" + controlName).append('<option value="NCB">Nota crédito E</option>');
            $("#" + controlName).append('<option value="NCM">Nota crédito M</option>');
            $("#" + controlName).append('<option value="NDA">Nota débito A</option>');
            $("#" + controlName).append('<option value="NDB">Nota débito B</option>');
            $("#" + controlName).append('<option value="NDM">Nota débito E</option>');
            $("#" + controlName).append('<option value="NDM">Nota débito M</option>');
            //$("#" + controlName).append('<option value="RCA">Recibo A</option>');
            //$("#" + controlName).append('<option value="RCB">Recibo B</option>');
        }

        $("#" + controlName).append('<option value="TIC">Ticket</option>');
        $("#" + controlName).append('<option value="COT">Cotización</option>');

        if (selectedValue != "")
            $("#" + controlName).val(selectedValue);
    },
    obtenerComprobantesCobranzasPorCondicion: function (controlName, condicionIva, selectedValue) {//TODO: TMB AGREGAR CONDICION EMISOR
        $("#" + controlName).html("");
        $("#" + controlName).append('<option value="RC">Recibo</option>');
        /*if (MI_CONDICION == "RI") {
            if (condicionIva != "RI") {
                //    $("#" + controlName).append('<option value="RCC">Recibo C</option>');
                //} else if (condicionIva == "CF" || condicionIva == "EX") {
                $("#" + controlName).append('<option value="RCB">Recibo</option>');
            }
            else {
                $("#" + controlName).append('<option value="RCA">Recibo</option>');
            }
        }
        else if (MI_CONDICION == "MO") {
            $("#" + controlName).append('<option value="RCC">Recibo</option>');
        }
        else if (MI_CONDICION == "EX") {
            $("#" + controlName).append('<option value="RCB">Recibo</option>');
        }

        $("#" + controlName).append('<option value="SIN">Ninguno</option>');

        if (selectedValue != "")
            $("#" + controlName).val(selectedValue);*/
    },
    obtenerCondicionIvaDesc: function (tipo) {
        if (tipo == "RI")
            return "Responsable Inscripto";
        else if (tipo == "MO")
            return "Monotributo";
        else if (tipo == "EX")
            return "Exento";
        else
            return "Cliente final";
        //else if (tipo == "NI")
        //    return "Responsable NO inscripto";

    },
    obtenerProxNroComprobante: function (controlName, tipo, idpuntoDeVenta) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerProxNroComprobante",
            data: "{tipo: '" + tipo + "', idPuntoDeVenta: " + idpuntoDeVenta + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).val(data.d);
                }
            }
        });
    },
    obtenerProxNroCobranza: function (controlName, tipo) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerProxNroCobranza",
            data: "{tipo: '" + tipo + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).val(data.d);
                }
            }
        });
    },
    obtenerPuntosDeVenta: function (controlName) {
        obtenerPuntosDeVenta(controlName);
    },
    obtenerCondicionesDeVenta: function (controlName, selectedValue) {
        obtenerCondicionesDeVenta(controlName, selectedValue);
    },
    obtenerPuntosDeVentaYNroCobranza: function (controlName) {
        obtenerPuntosDeVentaYNroCobranza(controlName);
    },
    previsualizarComprobanteGenerado: function (comprobante) {
        $("#divError").hide();
        var year = new Date().getFullYear().toString();
        if (comprobante != "") {
            var version = new Date().getTime();

            $("#ifrPdf").attr("src", "/files/explorer/" + MI_IDUSUARIO + "/comprobantes/" + year + "/" + comprobante + "?" + version + "#zoom=100&view=FitH,top");
        }
        $('#modalPdf').modal('show');
    },
    previsualizarComprobanteGeneradoPorAnio: function (comprobante, year) {
        $("#divError").hide();
        //var year = new Date().getFullYear().toString();
        if (comprobante != "") {
            var version = new Date().getTime();

            $("#ifrPdf").attr("src", "/files/explorer/" + MI_IDUSUARIO + "/comprobantes/" + year + "/" + comprobante + "?" + version + "#zoom=100&view=FitH,top");
        }
        $('#modalPdf').modal('show');
    },
    imprimirArchivoDesdeIframe: function (comprobante) {
        if (comprobante != "") {
            var d = new Date();
            var n = d.getFullYear();
            var version = new Date().getTime();
            if ($("#fileName").val() != null || $("#fileName").val() != undefined)
                var pathFile = "/files/explorer/" + $("#hdnIDUsuario").val() + "/comprobantes/" + n + "/" + fileName + "?" + version + "#zoom=100&view=FitH,top";
            else
                var pathFile = "/files/explorer/" + $("#hdnIDUsuario").val() + "/comprobantes/" + n + "/" + comprobante + "?" + version + "#zoom=100&view=FitH,top";

            printJS(pathFile);

            ///$("#ifrPdf").attr("src", pathFile);

            //$("#ifrPdf").attr("src", "/files/comprobantes/" + comprobante + "#zoom=100&view=FitH,top");
        }
        else {
            var getMyFrame = document.getElementById("ifrPdf");
            // alert(getMyFrame.src);
            if (getMyFrame.src != "")
                printJS(getMyFrame.src);
        }

        //var getMyFrame = document.getElementById("ifrPdf");
        //getMyFrame.focus();
        //getMyFrame.contentWindow.print();

    },
    obtenerCategorias: function (controlName, showEmpty) {
        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerCategorias",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");
                }
            }
        });
    },
    obtenerConceptosCodigoyNombre: function (controlName, tipo, showEmpty) {

        var aux = "";
        if (tipo == 1)
            aux = "P,C";
        else if (tipo == 2)
            aux = "S";
        else if (tipo == 4)//se excluyen combos
            aux = "P";

        $("#" + controlName).html("");

        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerConceptosCodigoyNombre",
            data: "{tipo: '" + aux + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");
                }
                else {
                    $("<option/>").attr("value", "").text("-").appendTo($("#" + controlName));
                    $("#" + controlName).trigger("change");
                }
            }
        });
    },
    obtenerConceptosCodigoyNombrePorInventario: function (controlName, idInventario, stockMin, showEmpty) {


        $("#" + controlName).html("");

        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerConceptosCodigoyNombrePorInventario",
            data: "{idInventario: '" + idInventario + "',stockMin: " + stockMin + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");
                }
                else {
                    $("<option/>").attr("value", "").text("-").appendTo($("#" + controlName));
                    $("#" + controlName).trigger("change");
                }
            }
        });
    },

    obtenerConceptosCodigoyNombreSinCombos: function (controlName, tipo, showEmpty) {

        var aux = "";
        if (tipo == 1)
            aux = "P";
        else if (tipo == 2)
            aux = "S";

        $("#" + controlName).html("");

        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerConceptosCodigoyNombreSinCombos",
            data: "{tipo: '" + aux + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");
                }
                else {
                    $("<option/>").attr("value", "").text("-").appendTo($("#" + controlName));
                    $("#" + controlName).trigger("change");
                }
            }
        });
    },
    obtenerAnios: function (controlName, tipo, showEmpty) {

        var aux = "";
        if (tipo == 1)
            aux = "P";
        else if (tipo == 2)
            aux = "S";

        $("#" + controlName).html("");

        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerAnios",
            data: "{tipo: '" + aux + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");
                }
                else {
                    $("<option/>").attr("value", "").text("-").appendTo($("#" + controlName));
                    $("#" + controlName).trigger("change");
                }
            }
        });
    },
    obtenerMeses: function (controlName, tipo, showEmpty) {

        var aux = "";
        if (tipo == 1)
            aux = "P";
        else if (tipo == 2)
            aux = "S";

        $("#" + controlName).html("");

        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerMeses",
            data: "{tipo: '" + aux + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    $("#" + controlName).trigger("change");
                }
                else {
                    $("<option/>").attr("value", "").text("-").appendTo($("#" + controlName));
                    $("#" + controlName).trigger("change");
                }
            }
        });
    },

    obtenerCheques: function (controlName, idSelected, showEmpty, propios) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerCheques",
            data: "{ EsPropio :" + propios + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected);
                }
            }
        });
    },
    obtenerChequesParaCobranzas: function (controlName, idSelected, showEmpty, propios) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerChequesCobranzas",
            data: "{ EsPropio :" + propios + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected);
                }
            }
        });
    },
    obtenerBancos: function (controlName, idSelected) {
        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerBancos",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    if (data.d.length > 1)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected);

                }
            }
        });

    },
    obtenerCajas: function (controlName, idSelected) {
        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerCajas",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    //if (data.d.length > 1)
                    $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected);

                }
            }
        });

    },
    obtenerTodosBancos: function (controlName, idSelected) {
        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerTodosBancos",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    if (data.d.length > 1)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected);

                }
            }
        });

    },
    obtenerUltimoNroRecibo: function (controlName, tipoComprobante, idPunto) {
        var info = "{tipoComprobante: '" + tipoComprobante + "',idPunto:" + idPunto + "}";
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerUltimoNroRecibo",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).val("");

                    $("#" + controlName).val(data.d);
                }
            }
        });

    },
    obtenerRubros: function (controlName, showEmpty) {
        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerRubros",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data != null) {

                    $("#" + controlName).html("");
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }



                }
            }
        });
    },
    obtenerSubRubros: function (idPadre, controlName) {
        if (idPadre != "" && idPadre != "0") {
            $.ajax({
                type: "POST",
                url: "/common.aspx/obtenerSubRubros",
                data: "{ idPadre :" + idPadre + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data != null) {
                        $("#" + controlName).html("");
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));


                        for (var i = 0; i < data.d.length; i++) {
                            $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                        }


                    }
                }
            });
        } else {
            $("#" + controlName).html("");

        }
    },
    soloNumerosConGuiones: function (input) {
        $("#" + input).keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) && (e.keyCode != 173 && e.keyCode != 109)) {
                e.preventDefault();
            }
        });
    },
    mostrarProcesando: function (controlName) {
        $("#" + controlName).attr("disabled", true);
        $("#" + controlName).html("Procesando...");
        $("#" + controlName).val("Procesando...");
    },
    ocultarProcesando: function (controlName, texto) {
        $("#" + controlName).attr("disabled", false);
        $("#" + controlName).html(texto);
        $("#" + controlName).val(texto);
    },
    obtenerListaPrecios: function (controlName, idSelected, showEmpty) {

        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerListaPrecios",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    if (showEmpty)
                        $("<option/>").attr("value", "0").text("Default").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected);
                }
            }
        });
    },
    obtenerPaises: function (controlName, idSelected, showEmpty) {

        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerPaises",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected).trigger("change");
                }
            }
        });
    },
    obtenerProvincias: function (controlName, idSelected, idPais, showEmpty) {
        if (idPais != "" && idPais != null) {
            $.ajax({
                type: "GET",
                url: "/common.aspx/obtenerProvincias?idPais=" + idPais,
                //data: "{id: " + idPais + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data != null) {
                        $("#" + controlName).html("");

                        if (showEmpty)
                            $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                        for (var i = 0; i < data.d.length; i++) {
                            $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                        }

                        if (idSelected != "")
                            $("#" + controlName).val(idSelected).trigger("change");
                    }
                }
            });
        }
        else {

            $("#" + controlName).val("").trigger("change");
            $("#ddlProvincia").val("").trigger("change");
            $("#ddlProvincia").html("").trigger("change");
            $("#ddlCiudad").val("").trigger("change");
            $("#ddlCiudad").html("").trigger("change");

        }
    },
    obtenerCiudades: function (controlName, idSelected, idProvincia, showEmpty) {
        if (idProvincia != "" && idProvincia != null) {
            $.ajax({
                type: "GET",
                url: "/common.aspx/obtenerCiudades?idProvincia=" + idProvincia,
                //data: "{idProvincia: " + idProvincia + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data != null) {
                        $("#" + controlName).html("");

                        if (showEmpty)
                            $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                        for (var i = 0; i < data.d.length; i++) {
                            $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                        }

                        if (idSelected != "")
                            $("#" + controlName).val(idSelected).trigger("change");
                    }
                }
            });
        }
        else {
            $("#" + controlName).val("").trigger("change");
            $("#" + controlName).html("").trigger("change");
        }
    },
    obtenerProxNroPresupuesto: function (controlName) {
        $.ajax({
            type: "GET",
            url: "/common.aspx/obtenerProxNroPresupuesto",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).val(data.d);
                }
            }
        });
    },
    pagarPlanActual: function (nombre) {
        window.location.href = "/modulos/seguridad/pagoDePlanes.aspx?plan=" + nombre;
    },
    cambiarSesion: function (id, nombre) {

        bootbox.confirm("¿Está seguro que desea iniciar sesión con la empresa " + nombre + "?", function (result) {
            if (result) {

                var info = "{ idEmpresa: '" + id + "'}";
                $.ajax({
                    type: "POST",
                    url: "/common.aspx/cambiarEmpresa",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        window.location.href = "/home.aspx";
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        });
    },
    obtenerSelectPlanCuentas: function (controlName, idSelected, showEmpty) {

        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerSelectPlanCuentas",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#" + controlName).html("");

                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }

                    if (idSelected != "")
                        $("#" + controlName).val(idSelected).trigger("change");
                }
            }
        });
    },
    showModalCheque: function (propio) {
        $("#divOkCheque,#divErrorCheque").hide();
        $("#ddlBancosCheque").val("").trigger("change");
        if (propio == "1") {
            var NombreCliente = $("#ddlPersona option:selected").text().split("-")[1];
            $("#txtEmisorCheque").val(MI_RAZONSOCIAL);
        } else {
            if ($("#ddlPersona").val() != "" || $("#ddlPersona").val() != null) {
                var NombreCliente = $("#ddlPersona option:selected").text().split("-")[1];
                $("#txtEmisorCheque").val(NombreCliente);
            }
        }
        if ($("#ddlFormaPago").val() == 'Cheque Propio')
            Common.obtenerBancos("ddlBancosCheque");
        else
            Common.obtenerTodosBancos("ddlBancosCheque");
        $("#hdnChequePropio").val(propio);
        $("#modalNuevoCheque").show("modal");
    },
    sumarDiasFecha: function (days, fecha) {
        var date = new Date();

        var tiempo = date.getTime();
        var milisegundos = parseInt(days * 24 * 60 * 60 * 1000);
        var total = date.setTime(tiempo + milisegundos);
        var day = total.getDate();
        var month = total.getMonth() + 1;
        var year = total.getFullYear();
        return (day + "/" + month + "/" + year).toString();
    },
    configTelefono: function (controlName) {
        $("#" + controlName).mask("?(9999) 9999-9999");
        $("#" + controlName).on("blur", function () {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);
            if (last.length == 3) {
                var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
                var lastfour = move + last;
                var first = $(this).val().substr(0, 10);
                $(this).val(first + '-' + lastfour);
            }
        });
    },
    calcularPrecioFinal: function (iva, monto, usaPrecioFinalConIVA) {
        var total = monto;

        if (usaPrecioFinalConIVA == "0") {
            if (iva > 0) {
                switch (iva.toPrecision()) {
                    case "0":
                        iva = 1;
                        break;
                    case "2.5":
                        iva = 1.025;
                        break;
                    case "5":
                        iva = 1.050;
                        break;
                    case "10.5":
                        iva = 1.105;
                        break;
                    case "21":
                        iva = 1.210;
                        break;
                    case "27":
                        iva = 1.270;
                        break;


                }
                total = total / parseFloat(iva);
            }
            else {
                total = (total + ((total * iva) / 100));
            }
        }

        return total.toFixed(2);
        //$("#divPrecioIVA").html("$ " + addSeparatorsNF(total.toFixed(cantidadDecimales), '.', ',', '.'));

    },
    verCobranzasACuenta: function () {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/cobranzas.aspx/obtenerACuenta",
            success: function (data) {
                if (data != null) {
                    var det = '<table class="table table-success mb30 smallTable"><thead><tr><th>Fecha</th><th>Cobranza</th><th>Cliente</th><th>Importe</th></tr></thead><tbody id="bodyDetalle">' + data.d + '</tbody></table>';
                    $("#bodyModalCommon").html(det);

                    $("#modalCommon").modal("show");
                    $("#titModalCommon").html("Cobranzas a cuenta pendientes de imputación");
                }
            }
        });
    },
    verPagosACuenta: function () {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/modulos/compras/pagos.aspx/obtenerACuenta",
            success: function (data) {
                if (data != null) {
                    var det = '<table class="table table-success mb30 smallTable"><thead><tr><th>Fecha</th><th>Cobranza</th><th>Cliente</th><th>Importe</th></tr></thead><tbody id="bodyDetalle">' + data.d + '</tbody></table>';
                    $("#bodyModalCommon").html(det);

                    $("#modalCommon").modal("show");
                    $("#titModalCommon").html("Pagos a cuenta pendientes de imputación");
                }
            }
        });
    }
}

var Toolbar = {
    mostrarEnvio: function () {
        $("#divToolbar").slideUp("slow");
        $("#txtEnvioAsunto, #txtEnvioMensaje").val("");
        $("#divSendEmail").slideDown("slow");
        $("#litModalOkTitulo").html("Enviar comprobante por mail");
    },
    cancelarEnvio: function () {
        Common.ocultarProcesando("btnEnviar", "Enviar");
        $("#litModalOkTitulo").html("Comprobante emitido correctamente");
        $("#divSendEmail").slideUp("slow");
        $("#divToolbar").slideDown("slow");
    },
    toggleEnviosError: function () {
        var isValid = true;

        if ($("#txtEnvioPara").val() == "") {
            $("#msgErrorEnvioPara").show();
            $("#txtEnvioPara").closest('.form-group').removeClass('has-success').addClass('has-error');
            isValid = false;
        }
        else if (!$("#txtEnvioPara").valid()) {
            $("#msgErrorEnvioPara").show();
            $("#txtEnvioPara").closest('.form-group').removeClass('has-success').addClass('has-error');
            isValid = false;
        }
        else {
            $("#msgErrorEnvioPara").hide();
            $("#txtEnvioPara").closest('.form-group').removeClass('has-error');
        }

        if ($("#txtEnvioAsunto").val() == "") {
            $("#msgErrorEnvioAsunto").show();
            $("#txtEnvioAsunto").closest('.form-group').removeClass('has-success').addClass('has-error');
            isValid = false;
        }
        else {
            $("#msgErrorEnvioAsunto").hide();
            $("#txtEnvioAsunto").closest('.form-group').removeClass('has-error');
        }

        if ($("#txtEnvioMensaje").val() == "") {
            $("#msgErrorEnvioMensaje").show();
            $("#txtEnvioMensaje").closest('.form-group').removeClass('has-success').addClass('has-error');
            isValid = false;
        }
        else {
            $("#msgErrorEnvioMensaje").hide();
            $("#txtEnvioMensaje").closest('.form-group').removeClass('has-error');
        }

        if (isValid) {
            $("#txtEnvioPara").closest('.form-group').removeClass('has-error');
            $("#txtEnvioAsunto").closest('.form-group').removeClass('has-error');
            $("#txtEnvioMensaje").closest('.form-group').removeClass('has-error');
        }

        return isValid;
    },
    enviarComprobantePorMail: function () {
        var isValid = Toolbar.toggleEnviosError();

        if (isValid) {
            Common.mostrarProcesando("btnEnviar");
            //var info = "{ para:'" + $("#txtEnvioPara").val() + "', asunto: '" + $("#txtEnvioAsunto").val() + "', id: " + $("#hdnID").val() + "}";
            var info = "{ nombre:'" + $("#hdnRazonSocial").val() + "', para:'" + $("#txtEnvioPara").val() + "', asunto: '" + $("#txtEnvioAsunto").val() + "', mensaje: '" + $("#txtEnvioMensaje").val() + "', file: '" + $("#hdnFile").val() + "', year: " + $("#hdnFileYear").val() + ", idComprobante: " + $("#hdnID").val() + "}";
            $.ajax({
                type: "POST",
                url: "/common.aspx/enviarComprobantePorMail",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    Toolbar.cancelarEnvio();
                    $("#divOkMail").show();

                    setTimeout(function () {
                        $("#divOkMail").fadeOut(1500);
                    }, 3000);
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    Common.ocultarProcesando("btnEnviar", "Enviar");
                    $("#msgErrorMail").html(r.Message);
                    $("#divErrorMail").show();
                    $("#divOkMail").hide();
                    //$('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else {
            return false;
        }
    },
    enviarDocumentoPorMail: function (tipoComprobante) {
        var isValid = Toolbar.toggleEnviosError();

        if (isValid) {
            Common.mostrarProcesando("btnEnviar");
            //var info = "{ para:'" + $("#txtEnvioPara").val() + "', asunto: '" + $("#txtEnvioAsunto").val() + "', id: " + $("#hdnID").val() + "}";
            var info = "{ nombre:'" + $("#hdnRazonSocial").val() + "', para:'" + $("#txtEnvioPara").val() + "', asunto: '" + $("#txtEnvioAsunto").val() + "', mensaje: '" + $("#txtEnvioMensaje").val() + "', file: '" + $("#hdnFile").val() + "', year: " + $("#hdnFileYear").val() + ", idComprobante: " + $("#hdnID").val() + ",tipoComprobante:'" + tipoComprobante + "'}";
            $.ajax({
                type: "POST",
                url: "/common.aspx/enviarDocumentoPorMail",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    Toolbar.cancelarEnvio();
                    $("#divOkMail").show();

                    setTimeout(function () {
                        $("#divOkMail").fadeOut(1500);
                    }, 3000);
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    Common.ocultarProcesando("btnEnviar", "Enviar");
                    $("#msgErrorMail").html(r.Message);
                    $("#divErrorMail").show();
                    $("#divOkMail").hide();
                    //$('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else {
            return false;
        }
    },
}

var ExportarCC = {
    exportar: function () {
        ExportarCC.resetearExportacion();

        $("#imgLoadingModal").show();

        if ($("#hdnTipoPersonaExportar").val() == 0) {
            ExportarCC.exportarDetalleProveedor();
        }
        else {
            ExportarCC.exportarDetalleCliente();
        }
    },
    exportarPDF: function () {
        ExportarCC.resetearExportacion();

        $("#imgLoadingModal").show();

        if ($("#hdnTipoPersonaExportar").val() == 0) {
            ExportarCC.exportarDetalleProveedorPDF();
        }
        else {
            ExportarCC.exportarDetalleClientePDF();
        }
    },
    resetearExportacion: function () {
        $("#imgLoadingModal, #lnkDownloadModal").hide();
    },
    resetearExportacionPDF: function () {
        $("#imgLoadingModal, #imgLoadingModal").hide();
    },
    exportarDetalleProveedor: function () {

        $("#imgLoadingModal").show();
        var idPersona = $("#hdnIDPersona").val();
        var info = "{ idPersona: " + idPersona + "}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/exportarProveedor",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoadingModal").hide();
                    $("#lnkDownloadModal").show();
                    $("#lnkDownloadModal").attr("href", data.d);
                    $("#lnkDownloadModal").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    exportarDetalleCliente: function () {
        $("#imgLoadingModal").show();

        var idPersona = $("#hdnIDPersona").val();
        var info = "{ idPersona: " + idPersona + "}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/exportarCliente",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoadingModal").hide();
                    $("#lnkDownloadModal").show();
                    $("#lnkDownloadModal").attr("href", data.d);
                    $("#lnkDownloadModal").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    exportarDetalleClientePDF: function () {
        $("#imgLoadingModal").show();

        var idPersona = $("#hdnIDPersona").val();
        var info = "{ idPersona: " + idPersona + ", total:'" + $("#spnTotal").text() + "'}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/exportarClientePDF",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoadingModal").hide();
                    $("#lnkDownloadModal").show();
                    $("#lnkDownloadModal").attr("href", data.d);
                    $("#lnkDownloadModal").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                resetearExportacion();
            }
        });
    },
    exportarDetalleProveedorPDF: function () {

        $("#imgLoadingModal").show();

        var idPersona = $("#hdnIDPersona").val();
        var info = "{ idPersona: " + idPersona + ", total:'" + $("#spnTotal").text() + "'}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/exportarProveedorPDF",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoadingModal").hide();
                    $("#lnkDownloadModal").show();
                    $("#lnkDownloadModal").attr("href", data.d);
                    $("#lnkDownloadModal").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

/*** NUMBERS ***/
function addSeparatorsNF(nStr, inD, outD, sep) {
    nStr += '';
    var dpos = nStr.indexOf(inD);
    var nStrEnd = '';
    if (dpos != -1) {
        nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
        nStr = nStr.substring(0, dpos);
    }
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(nStr)) {
        nStr = nStr.replace(rgx, '$1' + sep + '$2');
    }
    return nStr + nStrEnd;
}

/*** DATES ***/
var MONTH_NAMES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
var MONTH_NAMES_SHORT = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

/*
Date.prototype.getMonthName = function () {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};
*/

Date.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
    var y = this.getFullYear();
    return (((y % 4 === 0) && (y % 100 !== 0)) || (y % 400 === 0));
};

Date.prototype.getDaysInMonth = function () {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};


function gd(year, month, day) {
    return new Date(year, month, day).getTime();
}

function convertToDate(timestamp) {
    var newDate = new Date(timestamp);
    var dateString = newDate.getMonth();
    var monthName = getMonthName(dateString);

    return monthName;
}



//** AUTO COMPLETE HEADER **//
$(document).ready(function () {
    $(function () {
        var data = [
           //Modulo ventas
           { label: 'Buscar facturas', value: '/comprobantes.aspx' },
           { label: 'Buscar facturas pendientes', value: '/modulos/ventas/facturasElectronicasPendientes.aspx' },
           { label: 'Buscar cobranzas', value: '/cobranzas.aspx' },
           { label: 'Buscar presupuestos', value: '/presupuestos.aspx' },
           { label: 'Buscar productos y servicios', value: '/conceptos.aspx' },
           { label: 'Buscar rubros', value: '/rubros.aspx' },
           { label: 'Buscar clientes', value: '/personas.aspx?tipo=c' },
           { label: 'Buscar abonos', value: '/abonos.aspx' },
           { label: 'Nueva factura', value: '/comprobantese.aspx' },
           { label: 'Nueva cobranza', value: '/cobranzase.aspx' },
           { label: 'Nuevo presupuesto', value: '/presupuestose.aspx' },
           { label: 'Nuevo producto o servicio', value: '/conceptose.aspx' },
           { label: 'Nuevo cliente', value: '/personase.aspx?tipo=c' },
           { label: 'Nuevo abonos', value: '/abonose.aspx' },
           { label: 'Facturar abonos', value: '/generarAbonos.aspx' },
           { label: 'Aplicar cobranzas a cuenta', value: '/aplicarCobranzasACuenta.aspx' },
           { label: 'Aumento de precios', value: '/modulos/ventas/aumentoMasivoPrecios.aspx' },
           { label: 'Condiciones de venta', value: '/modulos/ventas/condicionesVentas.aspx' },
           //Modulo Compras
           { label: 'Buscar ordenes de compras', value: '/ordenesCompras.aspx' },
           { label: 'Buscar compras', value: '/compras.aspx' },
           { label: 'Buscar pagos', value: '/pagos.aspx' },
           { label: 'Buscar proveedores', value: '/personas.aspx?tipo=p' },
           { label: 'Nueva compra', value: '/comprase.aspx' },
           { label: 'Nuevo pago', value: '/pagose.aspx' },
           { label: 'Nuevo proveedor', value: '/personase.aspx?tipo=p' },
           { label: 'Aplicar pagos a cuenta', value: '/aplicarPagosACuenta.aspx' },
           //Modulo tesoreria
           { label: 'Buscar bancos', value: '/modulos/tesoreria/bancos.aspx' },
           //{ label: 'Buscar gastos bancarios', value: '/modulos/tesoreria/gastosBancarios.aspx' },
           { label: 'Buscar cheques', value: '/modulos/tesoreria/cheques.aspx' },
           { label: 'Buscar Movimientos de fondos', value: '/modulos/tesoreria/MovimientoDeFondos' },
           { label: 'Buscar caja', value: '/modulos/tesoreria/caja.aspx' },
           { label: 'Nuevo banco', value: '/modulos/tesoreria/bancose.aspx' },
           { label: 'Nuevo gastos bancarios', value: '/modulos/tesoreria/bancosDetalle.aspx' },
           { label: 'Nuevo cheque', value: '/modulos/tesoreria/chequese.aspx' },
           { label: 'Detalle Bancario', value: '/modulos/reportes/DetalleBancario.aspx' },
           //Modulo inventarios
           { label: 'Buscar depósitos', value: '/modulos/inventarios/inventarios.aspx' },
           { label: 'Nuevo depósitos', value: '/modulos/inventarios/inventariose.aspx' },
           { label: 'Modificar stock', value: '/modulos/inventarios/modificacionStock.aspx' },
           { label: 'Movimientos de stock', value: '/modulos/inventarios/movimientosDeStock.aspx' },
           { label: 'Movimientos entre depósitos', value: '/modulos/inventarios/movimientosDeStock.aspx' },
           //Modulo contabilidad
           { label: 'Libro diario', value: '/modulos/contabilidad/LibroDiario.aspx' },
           { label: 'Libro diario copiativo', value: '/modulos/contabilidad/LibroDiarioCopiativo.aspx' },
           { label: 'Libro mayor', value: '/modulos/contabilidad/LibroMayor.aspx' },
           { label: 'Plan de cuentas', value: '/modulos/contabilidad/plandecuentas.aspx' },
           { label: 'Balance general', value: '/modulos/reportes/BalanceGeneral.aspx' },
           { label: 'Estado de resultado', value: '/modulos/reportes/EstadoResultado.aspx' },
           { label: 'Asientos manuales', value: '/modulos/contabilidad/asientosManuales.aspx' },
           //Reportes
            { label: 'Reporte Ventas vs Compras', value: '/modulos/reportes/ventas-vs-compras.aspx' },
            { label: 'Reporte Ventas vs Pagos', value: '/modulos/reportes/ventas-vs-pagos.aspx' },
            { label: 'Reporte Compras por categoría', value: '/modulos/reportes/compras-por-categoria.aspx' },
            { label: 'Reporte Compras por categorias mensuales', value: '/modulos/reportes/compras-por-categoria-mensual.aspx' },
            { label: 'Reporte Liquidez anual', value: '/modulos/reportes/utilidad.aspx' },
            { label: 'Reporte Cobrado vs Pagado', value: '/modulos/reportes/cobrado-vs-pagado.aspx' },
            { label: 'Reporte Ventas mensuales', value: '/modulos/reportes/ventasDelMes.aspx' },
            { label: 'Reporte IVA Compras', value: '/reportes/iva-compras.aspx' },
            { label: 'Reporte IVA Ventas', value: '/modulos/reportes/iva-ventas.aspx' },
            { label: 'Reporte IVA Saldo', value: '/reportes/iva-saldo.aspx' },
            { label: 'Reporte Retenciones', value: '/reportes/retenciones.aspx' },
            { label: 'Reporte Percepciones', value: '/modulos/reportes/percepciones.aspx' },
            { label: 'Reporte C.I.T.I: compras y ventas', value: 'modulos/reportes/citiVentas.aspx' },
            { label: 'Reporte Cuenta Corriente', value: '/modulos/reportes/cc.aspx' },
            { label: 'Reporte Cobranzas pendientes', value: '/reportes/cobranzasPendientes.aspx' },
            { label: 'Reporte Pagos a proveedores', value: '/modulos/reportes/pagoprov.aspx' },
            { label: 'Reporte Stock productos', value: '/modulos/reportes/stock.aspx' },
            { label: 'Reporte Cuentas a pagar', value: '/modulos/reportes/cuentasPagar.aspx' },
            { label: 'Reporte Ranking por cliente', value: '/modulos/reportes/rnk-clientes.aspx' },
            { label: 'Reporte Ranking por producto/servicio', value: '/modulos/reportes/rnk-conceptos.aspx' },
            { label: 'Reporte Tracking de horas', value: '/modulos/reportes/trackingHora.aspx' },
            { label: 'Reporte Productos/servicios por cliente', value: '/modulos/reportes/productosPorCliente.aspx' },
            { label: 'Reporte Compras por cliente', value: '/modulos/reportes/comprasPorCliente.aspx' },
            { label: 'Reporte Entradas y salidas de productos', value: '/modulos/reportes/historicoStock.aspx' },
            { label: 'Reporte Rentabilidad', value: '/modulos/reportes/rentabilidad-de-productos.aspx' },
            { label: 'Reporte Rentabilidad x Venta', value: '/modulos/reportes/rentabilidad-de-productos-det.aspx' },
            { label: 'Reporte Ventas por puntos de venta mensuales', value: '/modulos/reportes/ventas-por-pdv-mensual.aspx' },
            { label: 'Reporte Comisiones por vendedor', value: '/modulos/reportes/comisiones.aspx' },

           //Importaciones
           { label: 'Importar clientes', value: '/importar.aspx?tipo=Clientes' },
           { label: 'Importar proveedores', value: '/importar.aspx?tipo=Proveedores' },
           { label: 'Importar productos', value: '/importar.aspx?tipo=Productos' },
           { label: 'Importar servicios', value: '/importar.aspx?tipo=Servicios' },
           { label: 'Importar compras', value: '/importar.aspx?tipo=FacturasCompras' },
           { label: 'Importar ventas', value: '/importar.aspx?tipo=Facturas' },
           //Otros
           { label: 'Explorador de archivos', value: '/file-explorer.aspx' },
           { label: 'Alertas', value: '/alertas.aspx' },
           { label: 'Administrar usuarios', value: '/modulos/seguridad/usuarios.aspx' }
        ];
        try {
            $("#autocompleteHeader,#autocompleteHeader2").autocomplete({
                source: data,
                focus: function (event, ui) {
                    $(event.target).val(ui.item.label);
                    return false;
                },
                select: function (event, ui) {
                    $(event.target).val(ui.item.label);
                    window.location = ui.item.value;
                    return false;
                }
            });
        }
        catch (E) { }
    });
});


$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
﻿var generarAbonos = {
    configFilters: function() {
        $('#txtFechaComprobante').datepicker();
        $('#txtFechaVencimiento').datepicker();
        Common.configDatePicker();

        $("#ddlModo").attr("onchange", "generarAbonos.changeModoFacturacion()");

        $("#txtNombre").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                generarAbonos.filtrar();
                return false;
            }
        });

        /*$.validator.addMethod("validFechaActual", function (value, element) {

            var fInicio = $("#txtFecha").val().split("/");
            var fechainicio = new Date(fInicio[2], (fInicio[1] - 1), fInicio[0]);

            var today = new Date();
            var fechaFin = new Date(today);
            fechaFin.setDate(today.getDate() + 5);

            if (fechainicio > fechaFin) {
                return false;
            }
            else {
                return true;
            }
        }, "Solo puede facturar hasta 5 dias despues de la fecha actual");*/
        $.validator.addMethod("validFechaComprobanteActual", function(value, element) {

            var fInicio = $("#txtFechaComprobante").val().split("/");
            var fechainicio = new Date(fInicio[2], (fInicio[1] - 1), fInicio[0]);

            var today = new Date();
            var fechaFin = new Date(today);
            fechaFin.setDate(today.getDate() + 5);

            if (fechainicio > fechaFin) {
                return false;
            } else {
                return true;
            }
        }, "Sólo puede facturar hasta 5 días después de la fecha actual");

        $("#txtNumeroFacturaA").mask("?99999999");
        $("#txtNumeroFacturaA").blur(function() {
            $("#txtNumeroFacturaA").val(padZeros($("#txtNumeroFacturaA").val(), 8));
        });

        $("#txtNumeroFacturaB").mask("?99999999");
        $("#txtNumeroFacturaB").blur(function() {
            $("#txtNumeroFacturaB").val(padZeros($("#txtNumeroFacturaB").val(), 8));
        });

        $("#txtNumeroFacturaC").mask("?99999999");
        $("#txtNumeroFacturaC").blur(function() {
            $("#txtNumeroFacturaC").val(padZeros($("#txtNumeroFacturaC").val(), 8));
        });


        if ($("#CondicionIva").val() == "MO") {
            $("#divNroComprobanteA,#divNroComprobanteB").hide();
            $("#divNroComprobanteC").show();
        } else {
            $("#divNroComprobanteA,#divNroComprobanteB").show();
            $("#divNroComprobanteC").hide();
        }

        // Validation with select boxes
        $("#fromEdicion").validate({
            highlight: function(element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            onkeyup: function(element) {
                this.element(element); // <- "eager validation"
            },
            onfocusout: function(element) {
                this.element(element); // <- "eager validation"
            }
        });
    },
    resetearPagina: function() {
        $("#hdnPage").val("1");
    },
    filtrar: function() {
        $("#divError,#divOk").hide();
        $("#resultsContainer").html("");
        $("#divfooter").hide();
        $("#txtFechaComprobante,#txtNumeroFacturaC,#txtNumeroFacturaA,#txtNumeroFacturaB").val("");
        if ($("#txtFecha").valid()) {

            Common.mostrarProcesando("btnBuscar");
            var info = "{ fecha: '" + $("#txtFecha").val() + "',frecuencia:'" + $("#ddlFrecuencia").val() + "',nombre:'" + $("#txtNombre").val() + "'}";

            $.ajax({
                type: "POST",
                url: "generarAbonos.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, text) {
                    generarAbonos.changePuntoDeVenta();
                    generarAbonos.changeModoFacturacion();

                    if (data.d.Items.length > 0)
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                    var categoriasAbonos = [];
                    for (i = 0; i < data.d.Items.length; i++) {
                        categoriasAbonos.push(data.d.Items[i].Cantidad);
                    }

                    $("#hdnCantAbonos").val(data.d.Items.length);
                    $("#hdnCantAbonosPorCategoria").val(categoriasAbonos);

                    if (data.d.Items.length > 0) {
                        $("#ContResultados").show();
                        $("#divfooter").show();
                        generarAbonos.ActualizarTotales();

                        //$("#idTotalImportes").html(data.d.totalImporte);
                        //$("#idTotalIva").html(data.d.TotalIva);
                        //$("#idTotal").html(data.d.Total);
                    }

                    Common.ocultarProcesando("btnBuscar", "Buscar");
                },
                error: function(response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    Common.ocultarProcesando("btnBuscar", "Buscar");
                }
            });
        } else {
            return false;

        }
    },
    changeModoFacturacion: function() {
        if ($("#ddlModo").val() == "E")
            $("#divNroComprobantes").hide();
        else {
            $("#divNroComprobantes").show();

            if ($("#CondicionIva").val() == "MO") {
                $("#divNroComprobanteC").show();
                $("#divNroComprobanteA,#divNroComprobanteB").hide();
            } else {
                $("#divNroComprobanteC").hide();
                $("#divNroComprobanteA,#divNroComprobanteB").show();
            }
        }
    },
    ingresarDatos: function() {
        $("#imgLoading2").show();
        $("#btnGenerar").attr("disabled", true);

        if (generarAbonos.validarGenerarAbonos()) {
            $("#divWait").html("Por favor, espere unos minutos. Se estan facturando los abonos...");
            generarAbonos.openModalGeneracion();
            generarAbonos.generarCAE(0, 0);
        } else {
            $("#btnGenerar").attr("disabled", false);
            $("#imgLoading2").hide();
        }
    },
    validarGenerarAbonos: function() {

        if (!$("#ddlPuntoVenta,#ddlModo").valid()) {
            $("#msgError").html("Todos los campos son obligatorios.");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }

        if (!$("#txtFechaComprobante").valid()) {
            return false;
        }

        if ($("#txtFechaComprobante").val() == "" || $("#txtFechaComprobante").val() == null) {
            $("#msgError").html("La fecha del comprobante es obligatoria.");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }

        if ($("#txtFechaVencimiento").val() == "" || $("#txtFechaVencimiento").val() == null) {
            $("#msgError").html("La fecha de vencimiento del comprobante es obligatoria.");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }

        if ($("#ddlModo").val() != "E") {
            if ($("#CondicionIva").val() == "MO") {
                if (parseInt($("#txtNumeroFacturaC").val()) == 0) {
                    $("#msgError").html("El numero del comprobante tiene que ser mayor a 0");
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    return false; //remarcar los comprobantes con el error
                }
            } else {
                if (parseInt($("#txtNumeroFacturaA").val()) == 0 || parseInt($("#txtNumeroFacturaB").val()) == 0) {
                    $("#msgError").html("El numero del comprobante tiene que ser mayor a 0");
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    return false; //remarcar los comprobantes con el error
                }
            }
        }

        var datos = 0;
        $("input:checkbox:checked").each(function() {
            if ($(this).attr("id").indexOf("chkAbono") >= 0) {
                datos++;
            }
        });

        if (datos == 0) {
            $("#msgError").html("Debe elejir al menos un cliente. ");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false; //remarcar los comprobantes con el error
        }
        return true;
    },
    openModalGeneracion: function () {
        $("#modalGeneracionAbonos").modal("toggle");
    },
    generarCAE: function (cat,desde) {
        $("#divError,#imgAbonoOK,#imgAbonoError,#msgAbonoOK,#msgAbonoError").hide();
        $("#divCerrar").hide();
        $("#divWait,#imgAbonoCargando").show();
        
        if ($("#txtFechaComprobante").val() != "" && $("#txtFechaVencimiento").val() != "") {

            var countCategorias = $("#hdnCantAbonos").val();
            var countItemsCategoriasStr = $("#hdnCantAbonosPorCategoria").val();
            var countItemsCategorias = JSON.parse("[" + countItemsCategoriasStr + "]");;

            //console.log("llamada" + cat + " " + desde);
            var hasta = parseInt(desde) + 5;

            var info = "{idAbonos: '";
            $("input:checkbox:checked").each(function() {
                if ($(this).attr("id").indexOf("chkAbono") >= 0) {
                    info += $(this).attr("id");
                }
            });

            info += "',modo:'" + $("#ddlModo").val()
            info += "',idPuntoVenta:" + $("#ddlPuntoVenta").val()
            info += ",numeroFacturaA:'" + $("#txtNumeroFacturaA").val()
            info += "',numeroFacturaB:'" + $("#txtNumeroFacturaB").val()
            info += "',numeroFacturaC:'" + $("#txtNumeroFacturaC").val()
            info += "',fechaComprobante:'" + $("#txtFechaComprobante").val()
            info += "',fechaVencimiento:'" + $("#txtFechaVencimiento").val()
            info += "',item:" + cat
            info += ",desde:" + desde
            info += ",hasta:" + hasta
            info += "}";

            $.ajax({
                type: "POST",
                url: "generarAbonos.aspx/generarComprobanteAbono",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, text) {
                    //console.log("Resultados Items", data.d);
                    if (data.d.CantidadGenerados>0)
                        $("#divWait").html("Por favor, espere unos minutos. Procesando " + data.d.CantidadGenerados + " abonos de " + $("#idTotalCant").html()+"...");
                    $("#divWait").show();
                    $("#resultsContainer").html("");
                    $("#divfooter").hide();
                    

                    if ($("#ddlModo").val() == "E")
                        $("#EsFacturaElectronica").val("1");
                    else
                        $("#EsFacturaElectronica").val("0");

                    var categoria = data.d.Categoria;
                    var hasta = data.d.Hasta;
                    var desde = 0;
                   
                   
                    var finEjecucion = false;
                    var countCat = countItemsCategorias[categoria];
                    if (countCat > parseInt(hasta) + parseInt(1)) {
                        desde = parseInt(hasta) + parseInt(1);

                    } else {
                        categoria = parseInt(categoria) + parseInt(1);
                       
                        if (categoria > countCategorias - 1) {
                            //FIN EJECUCION
                            finEjecucion = true;
                            $("#divWait,#imgAbonoCargando").hide();
                            if (data.d.CantidadErrores > 0) {
                                $("#msgAbonoError,#imgAbonoError").show();
                                $("#spnAbonoOK").html((data.d.CantidadGenerados - data.d.CantidadErrores));
                                $("#spnAbonoError").html(data.d.CantidadErrores);

                                //$("#divOk").html("<div class='col-md-3 col-md-offset-2'><h5 class='subtitle'>Finalizados</h5><label class='label label-success' style='font-size: 15px;'>" + (data.d.CantidadGenerados - data.d.CantidadErrores) + "</label></div><div class='col-md-3'><h5 class='subtitle'>Errores</h5><label class='label label-danger' style='font-size: 15px;'>" + data.d.CantidadErrores + "</label></div>")
                                $("#lblAbonoResultadoOK").show();
                                $("#lblAbonoResultadoError").show();
                            }
                            else {
                                $("#msgAbonoOK,#imgAbonoOK").show();
                                $("#spnAbonoOK").html(data.d.CantidadGenerados);
                                $("#lblAbonoResultadoOK").show();
                                $("#lblAbonoResultadoError").hide();
                            }

                            $("#divOk").show();
                            $("#divCerrar").show();
                           
                            $("#btnGenerar").attr("disabled", false);
                            $("#imgLoading2").hide();
                            //$("#modalGeneracionAbonos").modal("toggle");
                        } else {
                            desde = 0;
                        }
                    }
                    //console.log("Categoria", categoria);
                    //console.log("Desde", desde);

                    if (data.d.Items.Items.length > 0)
                        $("#resultComprobanteFETemplate").tmpl({ results: data.d.Items.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items.Items }).appendTo("#resultsContainer");

                    if (!finEjecucion) {
                        generarAbonos.generarCAE(categoria, desde);
                    }
                },
                error: function(response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#btnGenerar").attr("disabled", false);
                    $("#imgLoading2").hide();
                    $("#modalGeneracionAbonos").modal("hide");
                }
            });
        
        } else {
            $("#msgError").html("Debe seleccionar las fechas de emisión y vencimiento.");
            $("#divError").show();
            $("#imgLoading2").hide();
            $("#btnGenerar").attr("disabled", false);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    },
    showModalFacturacion: function (archivo, razonSocial, email, envioFE) {
        $("#divErrorEnvioFE,#divErrorMail").hide();
        if ($("#ddlModo").val() == "E") {
            $("#hdnRazonSocial").val(razonSocial);
            $("#txtEnvioPara").val(email);
            //Seteo la opcion de imprimir
            var version = new Date().getTime();
            $("#ifrPdf").attr("src", "/files/comprobantes/" + archivo + "?" + version + "#zoom=100&view=FitH,top");

            //Seteo el link de download
            var fileName = archivo.split("/")[1];
            $("#lnkDownloadPdf").attr("href", "/pdfGenerator.ashx?file=" + fileName);

            //Seteo el nombre del archivo para envio de mail
            $("#hdnFile").val(fileName);

            //Muestro la ventana
            $('#ModalFacturacion').modal('show');

            if (envioFE != "") {
                $("#msgErrorEnvioFE").html(envioFE);
                $("#divErrorEnvioFE").show();
            }

            if ($("#hdnEnvioFE").val() == "1" && envioFE == "") {
                $("#spSendMail").html("Enviado");
                $("#imgMailEnvio").attr("style", "color:#17a08c;font-size: 30px;");
                $("#spSendMail").attr("style", "color:#17a08c");
                $("#iCheckEnvio").addClass("fa fa-check");
            }
            else {
                $("#iCheckEnvio").removeClass("fa fa-check");
            }
        }
    },
    ActualizarTotales: function () {

        var totalImporte = 0;
        var totalIVA = 0;
        var totalCant = 0;
        $("input:checkbox:checked").each(function () {
            if ($(this).attr("id").indexOf("chkAbono") >= 0) {
                try{
                    var cantidad = parseInt($(this).attr("cantidad"));
                    var importe = $(this).attr("importe").replace(",", ".")
                    var iva = $(this).attr("Iva").replace(".", "").replace(",", ".")

                    totalImporte += (parseFloat(importe) * cantidad);
                    totalIVA += (parseFloat(iva) * cantidad);
                    totalCant++;
                }catch (Ex) { }
            }
        })

        $("#idTotalCant").html(totalCant);
        $("#idTotalImportes").html(totalImporte.toFixed(2));
        $("#idTotalIva").html(totalIVA.toFixed(2));
        $("#idTotal").html((totalImporte + totalIVA).toFixed(2));
    },
    ToggleCheck: function (obj) {
        $(obj).closest('table').find('input:checkbox').each(function () {
            this.checked = obj.checked ? false : true;
        });

        obj.checked = !obj.checked;

        this.ActualizarTotales();
    },
    changePuntoDeVenta: function () {
        if ($("#ddlModo").val() != "COT") {
            if ($("#CondicionIva").val() == "MO") {
                Common.obtenerProxNroComprobante("txtNumeroFacturaC", "FCC", $("#ddlPuntoVenta").val());
            }
            else {
                Common.obtenerProxNroComprobante("txtNumeroFacturaA", "FCA", $("#ddlPuntoVenta").val());
                Common.obtenerProxNroComprobante("txtNumeroFacturaB", "FCB", $("#ddlPuntoVenta").val());
            }
        }
    }

}
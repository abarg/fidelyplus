﻿/*** SEARCH ***/
var asientosManuales = {
    configFilters: function () {
        $(".select2").select2({ width: '100%', allowClear: true });

        Common.obtenerPersonas("ddlPersona", "", true);

        // Date Picker
        //configDatePicker();

        $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                asientosManuales.resetearPagina();
                asientosManuales.filtrar();
                return false;
            }
        });

        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

        // Validation with select boxes
        $("#frmSearch").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                //alert(element.parent('.input-group').length);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },

    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        asientosManuales.filtrar();
    },

    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        asientosManuales.filtrar();
    },

    resetearPagina: function () {
        $("#hdnPage").val("1");
    },

    filtrar: function () {

        $("#divError").hide();

        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");
            var currentPage = parseInt($("#hdnPage").val());

            Common.mostrarProcesando("btnBuscar");
            //var soloAsientosManuales = $("#chkAsientosManuales").is(":checked") ? "1" : "0";
            var idPersona = 0;
            //if ($("#ddlPersona").val() != "" && $("#ddlPersona").val() != null)
            //    idPersona = parseInt($("#ddlPersona").val());

            var info = "{ leyenda: '" + $("#txtCondicion").val()
                       + "', fechaDesde: '" + $("#txtFechaDesde").val()
                       + "', fechaHasta: '" + $("#txtFechaHasta").val()
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + "}";


            $.ajax({
                type: "POST",
                url: "/modulos/contabilidad/asientosManuales.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");
                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.d.Asientos.length > 0) {
                        $("#resultTemplate").tmpl({ results: data.d.Asientos }).appendTo("#resultsContainer");

                        $("#totalDebe").html(data.d.TotalDebe);
                        $("#totalHaber").html(data.d.TotalHaber);

                    }
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Asientos }).appendTo("#resultsContainer");

                    Common.ocultarProcesando("btnBuscar", "Buscar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
            asientosManuales.resetearExportacion();
        }
    },

    verTodos: function () {
        $("#txtFechaDesde, #txtFechaHasta, #txtCondicion").val("");
        asientosManuales.filtrar();
    },

    exportar: function () {
        asientosManuales.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        //var soloAsientosManuales = $("#chkAsientosManuales").is(":checked");
        var idPersona = 0;
        //if ($("#ddlPersona").val() != "")
        //    idPersona = parseInt($("#ddlPersona").val());

        var info = "{ leyenda: '" + $("#txtCondicion").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/contabilidad/asientosManuales.aspx/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                asientosManuales.resetearExportacion();
            }
        });
    },

    exportarPDF: function () {
        asientosManuales.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        //var soloAsientosManuales = $("#chkAsientosManuales").is(":checked");
        var idPersona = 0;
        //if ($("#ddlPersona").val() != "")
        //    idPersona = parseInt($("#ddlPersona").val());

        var info = "{ leyenda: '" + $("#txtCondicion").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/contabilidad/asientosManuales.aspx/exportPDF",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                asientosManuales.resetearExportacion();
            }
        });
    },

    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },

    editar: function (idAsiento) {
        window.location.href = "/modulos/contabilidad/asientosManualese.aspx?id=" + idAsiento;
    },

    eliminar: function (id, nombre) {
        bootbox.confirm("¿Está seguro que desea eliminar el asiento: " + nombre + "?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: "/modulos/contabilidad/asientosManuales.aspx/delete",
                    data: "{ id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        asientosManuales.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        });
    },

    nuevo: function () {
        window.location.href = "/modulos/contabilidad/asientosManualese.aspx";
    },

    nuevoModelo: function () {
        window.location.href = "/modulos/contabilidad/asientosManualese.aspx?nM=0";
    },

    administrarModelos: function () {
        window.location.href = "/modulos/contabilidad/asientosModelo.aspx";
    }


}

var ListaDeCuentas = new Array();
var asientosManualese = {
    configForm: function () {
        Common.configDatePicker();
        $(".select2").select2({ width: '100%', allowClear: true, });
        $("#txtDebe,#txtHaber").maskMoney({ thousands: '', decimal: '.', allowZero: true });
        $("#noResultTemplate").tmpl({ results: ListaDeCuentas }).appendTo("#resultsContainer");

        $("#txtDebe, #txtHaber").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                asientosManualese.agregarAsiento();
                return false;
            }
        });

        asientosManualese.limpiarDatos();
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        })
    },
    agregarAsiento: function () {
        var obj = new Object();

        if ($("#frmEdicion").valid()) {

            if (parseFloat($("#txtDebe").val()) > 0 && parseFloat($("#txtHaber").val()) > 0) {
                $("#msgError").html("Solo puede ingresar un valor.");
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }
            if (parseFloat($("#txtDebe").val()) == 0 && parseFloat($("#txtHaber").val()) == 0) {
                $("#msgError").html("Ingrese un valor al debe o al haber.");
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }
            if ($("#ddlPlanDeCuentas").val() == "") {
                $("#msgError").html("Seleccione una cuenta contable");
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            var existe = false;

            //existe = asientosManualese.modificarSiExiste($("#ddlPlanDeCuentas").val(), $("#txtDebe").val(), $("#txtHaber").val());

            //for (var i = 0; i < ListaDeCuentas.length; i++) {
            //    //alert(ListaDeCuentas[i].idPlanDeCuenta);
            //    if (ListaDeCuentas[i].IDPlanDeCuenta == $("#ddlPlanDeCuentas").val()) {
            //        existe = true;
            //        ListaDeCuentas[i].Debe = $("#txtDebe").val();
            //        ListaDeCuentas[i].Haber = $("#txtHaber").val();
            //    }
            //}


            //if (!existe) {            
            obj.IDPlanDeCuenta = $("#ddlPlanDeCuentas").val();
            obj.CodigoEnArray = $("#ddlPlanDeCuentas").val() + (ListaDeCuentas.length);
            //obj.Codigo = $("#ddlPlanDeCuentas").val();
            obj.NombreCuenta = $("#ddlPlanDeCuentas option:selected").text();
            obj.Debe = $("#txtDebe").val();
            obj.Haber = $("#txtHaber").val();
            obj.MostrarDebe = toDotandComma(parseFloat(obj.Debe));
            obj.MostrarHaber = toDotandComma(parseFloat(obj.Haber));

            ListaDeCuentas.push(obj);
            //}

            var totalDebe = 0;
            var totalHaber = 0;
            for (var i = 0; i < ListaDeCuentas.length; i++) {
                totalDebe = totalDebe + parseFloat(ListaDeCuentas[i].Debe);
                totalHaber = totalHaber + parseFloat(ListaDeCuentas[i].Haber);
            }

            $("#spnTotalDebe").html(toDotandComma(totalDebe));
            $("#spnTotalHaber").html(toDotandComma(totalHaber));

            $("#resultsContainer").html("");
            $("#resultTemplate").tmpl({ results: ListaDeCuentas }).appendTo("#resultsContainer");
            asientosManualese.limpiarDatos();

        }
    },
    agregarAsientoModelo: function () {
        var obj = new Object();

        if ($("#frmEdicion").valid()) {

            //if (parseFloat($("#txtDebe").val()) > 0 && parseFloat($("#txtHaber").val()) > 0) {
            //    $("#msgError").html("Solo puede ingresar un valor.");
            //    $("#divError").show();
            //    $('html, body').animate({ scrollTop: 0 }, 'slow');
            //    return false;
            //}
            //if (parseFloat($("#txtDebe").val()) == 0 && parseFloat($("#txtHaber").val()) == 0) {
            //    $("#msgError").html("Ingrese un valor al debe o al haber.");
            //    $("#divError").show();
            //    $('html, body').animate({ scrollTop: 0 }, 'slow');
            //    return false;
            //}

            var existe = false;

            existe = asientosManualese.modificarSiExiste($("#ddlPlanDeCuentas").val(), $("#txtDebe").val(), $("#txtHaber").val());

            //for (var i = 0; i < ListaDeCuentas.length; i++) {
            //    //alert(ListaDeCuentas[i].idPlanDeCuenta);
            //    if (ListaDeCuentas[i].IDPlanDeCuenta == $("#ddlPlanDeCuentas").val()) {
            //        existe = true;
            //        ListaDeCuentas[i].Debe = $("#txtDebe").val();
            //        ListaDeCuentas[i].Haber = $("#txtHaber").val();
            //    }
            //}

            if (!existe) {
                obj.IDPlanDeCuenta = $("#ddlPlanDeCuentas").val();
                obj.CodigoEnArray = $("#ddlPlanDeCuentas").val() + (ListaDeCuentas.length);
                //obj.Codigo = $("#ddlPlanDeCuentas").val();
                obj.NombreCuenta = $("#ddlPlanDeCuentas option:selected").text();
                obj.Debe = $("#txtDebe").val();
                obj.Haber = $("#txtHaber").val();

                ListaDeCuentas.push(obj);
            }

            var totalDebe = 0;
            var totalHaber = 0;
            for (var i = 0; i < ListaDeCuentas.length; i++) {
                totalDebe = totalDebe + parseFloat(ListaDeCuentas[i].Debe);
                totalHaber = totalHaber + parseFloat(ListaDeCuentas[i].Haber);
            }

            $("#spnTotalDebe").html(totalDebe.toFixed(2));
            $("#spnTotalHaber").html(totalHaber.toFixed(2));

            $("#resultsContainer").html("");
            $("#resultTemplate").tmpl({ results: ListaDeCuentas }).appendTo("#resultsContainer");
            asientosManualese.limpiarDatos();

            $(".ocultarEnEditar").hide();

        }
    },
    modificarSiExiste: function (plan, debe, haber) {

        var bool = false;
        for (var i = 0; i < ListaDeCuentas.length; i++) {
            //alert(ListaDeCuentas[i].idPlanDeCuenta);
            if (ListaDeCuentas[i].CodigoEnArray == plan) {
                bool = true;
                ListaDeCuentas[i].Debe = debe;
                ListaDeCuentas[i].Haber = haber;
                ListaDeCuentas[i].MostrarDebe = toDotandComma(debe);
                ListaDeCuentas[i].MostrarHaber = toDotandComma(haber);
            }
        }
        return bool;
    },
    limpiarDatos: function () {
        $("#ddlPlanDeCuentas").val("");
        $("#txtDebe,#txtHaber").val("0.00");
        $("#ddlPlanDeCuentas").trigger("change");
        $("#divOk,#divError").hide();
        $("#btnAgregar").html("Agregar asiento");

        //$("#spnTotalDebe").html("0");
        //$("#spnTotalHaber").html("0");
    },
    guardar: function (guardarModelo) {
        $("#divOk,#divError").hide();

        if ($('#frmEdicion').valid()) {
            //alert(asientosManuales.validarPartidaDoble());
            if (asientosManualese.validarPartidaDoble()) {

                bootbox.confirm({
                    message: "El debe y el haber no son iguales. ¿Desea crear igualmente el asiento?",
                    callback: function (result) {
                        if (result) {
                            asientosManualese.grabarAsientos(guardarModelo);
                        }
                    }
                });
            }
            else {
                asientosManualese.grabarAsientos(guardarModelo);
            }
        }
        else {
            return false;
        }
    },
    guardarModelo: function () {
        if ($('#frmEdicion').valid()) {

            if (ListaDeCuentas.length < 1) {
                $("#msgError").html("Debe ingresar las cuentas contables");
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }
            Common.mostrarProcesando("btnCrearModelo");

            var info = "{ listaAsientos: " + JSON.stringify(ListaDeCuentas)
            + ", leyenda: '" + $("#txtLeyenda").val()
            + "', id: " + $("#hdnID").val()
            + "}";
            $.ajax({
                type: "POST",
                url: "/modulos/contabilidad/asientosManualese.aspx/guardarModelo",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                    $("#divOk").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    location.href = "/modulos/contabilidad/asientosModelo.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnCrearModelo", "Aceptar");
                }
            });
        }
        else {
            return false;
        }
    },
    grabarAsientos: function (guardarModelo) {
        if (ListaDeCuentas.length < 1) {
            $("#msgError").html("Debe ingresar las cuentas contables");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }

        for (var i = 0; i < ListaDeCuentas.length; i++) {
            if (ListaDeCuentas[i].Debe == 0 && ListaDeCuentas[i].Haber == 0) {
                $("#msgError").html("Debe ingresar valores");
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }
        }

        if ($('#txtFecha,#txtLeyenda').valid()) {
            Common.mostrarProcesando("btnActualizar");
            Common.mostrarProcesando("btnActualizarModelo");

            var info = "{ listaAsientos: " + JSON.stringify(ListaDeCuentas)
                    + " , fecha: '" + $("#txtFecha").val()
                    + "', leyenda: '" + $("#txtLeyenda").val()
                    + "', id: " + $("#hdnID").val()
                    + " , idModelo: " + $("#hdnIDModelo").val()
                    + " , guardarModelo: " + guardarModelo
                    + "}";
            $.ajax({
                type: "POST",
                url: "/modulos/contabilidad/asientosManualese.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                    $("#divOk").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    location.href = "/modulos/contabilidad/asientosManuales.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                    Common.ocultarProcesando("btnActualizarModelo", "Aceptar y guardar como modelo");

                }
            });
        }
        else {
            return false;
        }
    },
    validarPartidaDoble: function () {

        var totalDebe = 0;
        var totalHaber = 0;

        for (var i = 0; i < ListaDeCuentas.length; i++) {
            totalDebe = totalDebe + parseFloat(ListaDeCuentas[i].Debe);
            totalHaber = totalHaber + parseFloat(ListaDeCuentas[i].Haber);
        }

        if (totalDebe.toFixed(2) != totalHaber.toFixed(2)) {
            /*$("#msgError").html("El debe y el haber no son iguales");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');*/
            return true;
        }
        else {
            return false;
        }
    },
    eliminar: function (CodigoEnArray) {
        bootbox.confirm("¿Está seguro que desea eliminar el asiento?", function (result) {
            if (result) {

                ListaDeCuentas = ListaDeCuentas.filter(function (el) {
                    return el.CodigoEnArray != CodigoEnArray;
                });

                $("#resultsContainer").html("");
                if (ListaDeCuentas.length > 0)
                    $("#resultTemplate").tmpl({ results: ListaDeCuentas }).appendTo("#resultsContainer");
                else
                    $("#noResultTemplate").tmpl({ results: ListaDeCuentas }).appendTo("#resultsContainer");

                var totalDebe = 0;
                var totalHaber = 0;
                for (var i = 0; i < ListaDeCuentas.length; i++) {
                    totalDebe = totalDebe + parseFloat(ListaDeCuentas[i].Debe);
                    totalHaber = totalHaber + parseFloat(ListaDeCuentas[i].Haber);
                }

                $("#spnTotalDebe").html(toDotandComma(totalDebe));
                $("#spnTotalHaber").html(toDotandComma(totalHaber));

                if ($("#hdnTipo").val() == "Mo")
                    $(".ocultarEnEditar").hide();
            }
        });
    },
    editar: function (idPlanDeCuenta, debe, haber) {
        $(".EditInput").maskMoney({ thousands: '', decimal: '.', allowZero: true });//HACERLO CUANDO TERMINA LA CARGA DE LA TABLA
        $(".keyPressEdit-" + idPlanDeCuenta).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                asientosManualese.saveEditar(idPlanDeCuenta);
                return false;
            }
        });

        $("#inputDebe-" + idPlanDeCuenta + ",#inputHaber-" + idPlanDeCuenta).show();
        $("#spnDebe-" + idPlanDeCuenta + ",#spnHaber-" + idPlanDeCuenta).hide();
        $("#btnSave-" + idPlanDeCuenta).show();

        for (var i = 0; i < ListaDeCuentas.length; i++) {
            //alert(ListaDeCuentas[i].idPlanDeCuenta);
            if (ListaDeCuentas[i].CodigoEnArray == idPlanDeCuenta) {
                bool = true;
                debe = ListaDeCuentas[i].Debe;
                haber = ListaDeCuentas[i].Haber;
            }
        }
        $("#inputDebe-" + idPlanDeCuenta).val(debe);
        $("#inputHaber-" + idPlanDeCuenta).val(haber);




        //$("#ddlPlanDeCuentas").val(idPlanDeCuenta);
        //$("#ddlPlanDeCuentas").trigger("change");

        //$("#btnAgregar").html("Actualizar asiento");

    },
    saveEditar: function (idPlanDeCuenta) {
        var debe = $("#inputDebe-" + idPlanDeCuenta).val();
        var haber = $("#inputHaber-" + idPlanDeCuenta).val();
        asientosManualese.modificarSiExiste(idPlanDeCuenta, debe, haber);

        $("#btnSave-" + idPlanDeCuenta).hide();
        $("#inputDebe-" + idPlanDeCuenta + ",#inputHaber-" + idPlanDeCuenta).hide();

        $("#spnDebe-" + idPlanDeCuenta).html(toDotandComma(parseFloat(debe)));
        $("#spnHaber-" + idPlanDeCuenta).html(toDotandComma(parseFloat(haber)));
        $("#spnDebe-" + idPlanDeCuenta + ",#spnHaber-" + idPlanDeCuenta).show();

        var totalDebe = 0;
        var totalHaber = 0;
        for (var i = 0; i < ListaDeCuentas.length; i++) {
            totalDebe = totalDebe + parseFloat(ListaDeCuentas[i].Debe);
            totalHaber = totalHaber + parseFloat(ListaDeCuentas[i].Haber);
        }

        $("#spnTotalDebe").html(toDotandComma(totalDebe));
        $("#spnTotalHaber").html(toDotandComma(totalHaber));

        asientosManualese.limpiarDatos();
    },
    obtenerAsientosManuales: function () {
        var info = "{ id: " + $("#hdnID").val() + "}";
        $.ajax({
            type: "POST",
            url: "/modulos/contabilidad/asientosManualese.aspx/obtenerAsientosManuales",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                for (var i = 0; i < data.d.items.length; i++) {
                    data.d.items[i].CodigoEnArray = data.d.items[i].IDPlanDeCuenta + '' + i;
                    data.d.items[i].MostrarDebe = toDotandComma(data.d.items[i].Debe);
                    data.d.items[i].MostrarHaber = toDotandComma(data.d.items[i].Haber);


                    //data.d.items[i].Debe = toDotandComma(data.d.items[i].Debe);
                    //data.d.items[i].Haber = toDotandComma(data.d.items[i].Haber);
                }

                $("#txtFecha").val(data.d.Fecha);
                $("#txtLeyenda").val(data.d.Leyenda);

                $("#resultsContainer").html("");
                $("#resultTemplate").tmpl({ results: data.d.items }).appendTo("#resultsContainer");

                ListaDeCuentas = data.d.items;

                var totalDebe = 0;
                var totalHaber = 0;
                for (var i = 0; i < ListaDeCuentas.length; i++) {
                    totalDebe = totalDebe + parseFloat(ListaDeCuentas[i].Debe);
                    totalHaber = totalHaber + parseFloat(ListaDeCuentas[i].Haber);
                }

                $("#spnTotalDebe").html(toDotandComma(totalDebe));
                $("#spnTotalHaber").html(toDotandComma(totalHaber));


            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    obtenerAsientosModelo: function () {
        var info = "{ id: " + $("#hdnID").val() + "}";
        $.ajax({
            type: "POST",
            url: "/modulos/contabilidad/asientosManualese.aspx/obtenerAsientoModelo",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                for (var i = 0; i < data.d.items.length; i++) {
                    data.d.items[i].CodigoEnArray = data.d.items[i].IDPlanDeCuenta + '' + i;
                }

                $("#txtFecha").val(data.d.Fecha);
                $("#txtLeyenda").val(data.d.Leyenda);

                $("#resultsContainer").html("");
                $("#resultTemplate").tmpl({ results: data.d.items }).appendTo("#resultsContainer");

                ListaDeCuentas = data.d.items;

                $(".ocultarEnEditar").hide();

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    cancelar: function () {
        window.location.href = "/modulos/contabilidad/asientosManuales.aspx";
    },
    actualizarDatosModelo: function () {
        var idModelo = $("#ddlAsientosModelo").val();
        if (idModelo != "") {
            var info = "{ id: " + idModelo + "}";

            $.ajax({
                type: "POST",
                url: "/modulos/contabilidad/asientosManualese.aspx/obtenerAsientoModelo",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {

                    for (var i = 0; i < data.d.items.length; i++) {
                        data.d.items[i].CodigoEnArray = data.d.items[i].IDPlanDeCuenta + '' + i;
                    }

                    $("#txtFecha").val(data.d.Fecha);
                    $("#txtLeyenda").val(data.d.Leyenda);

                    $("#resultsContainer").html("");
                    $("#resultTemplate").tmpl({ results: data.d.items }).appendTo("#resultsContainer");

                    ListaDeCuentas = data.d.items;

                    $("#hdnIDModelo").val(idModelo);
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else {
            $("#msgError").html("Debe seleccionar un modelo");
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    }

}

var asientosModelo = {
    configFilters: function () {
        //$(".select2").select2({ width: '100%', allowClear: true });

        $("#txtCondicion").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                asientosModelo.resetearPagina();
                asientosModelo.filtrar();
                return false;
            }
        });

        // Validation with select boxes
        $("#frmSearch").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                //alert(element.parent('.input-group').length);
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },

    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        asientosModelo.filtrar();
    },

    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        asientosModelo.filtrar();
    },

    resetearPagina: function () {
        $("#hdnPage").val("1");
    },

    filtrar: function () {

        $("#divError").hide();

        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");
            var currentPage = parseInt($("#hdnPage").val());

            Common.mostrarProcesando("btnBuscar");
            //var soloAsientosManuales = $("#chkAsientosManuales").is(":checked") ? "1" : "0";
            var idPersona = 0;
            //if ($("#ddlPersona").val() != "" && $("#ddlPersona").val() != null)
            //    idPersona = parseInt($("#ddlPersona").val());

            var info = "{ leyenda: '" + $("#txtCondicion").val()
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + "}";


            $.ajax({
                type: "POST",
                url: "/modulos/contabilidad/asientosModelo.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");
                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.d.Asientos.length > 0) {
                        $("#resultTemplate").tmpl({ results: data.d.Asientos }).appendTo("#resultsContainer");

                        //$("#totalDebe").html(data.d.TotalDebe);
                        //$("#totalHaber").html(data.d.TotalHaber);

                    }
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Asientos }).appendTo("#resultsContainer");





                    Common.ocultarProcesando("btnBuscar", "Buscar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
            asientosModelo.resetearExportacion();
        }
    },

    verTodos: function () {
        $("#txtCondicion").val("");
        asientosModelo.filtrar();
    },

    exportar: function () {
        asientosModelo.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        //var soloAsientosManuales = $("#chkAsientosManuales").is(":checked");
        var idPersona = 0;
        //if ($("#ddlPersona").val() != "")
        //    idPersona = parseInt($("#ddlPersona").val());

        var info = "{ leyenda: '" + $("#txtCondicion").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/contabilidad/asientosModelo.aspx/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                asientosModelo.resetearExportacion();
            }
        });
    },

    exportarPDF: function () {
        asientosModelo.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        //var soloAsientosManuales = $("#chkAsientosManuales").is(":checked");
        var idPersona = 0;
        //if ($("#ddlPersona").val() != "")
        //    idPersona = parseInt($("#ddlPersona").val());

        var info = "{ leyenda: '" + $("#txtCondicion").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/contabilidad/asientosModelo.aspx/exportPDF",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                asientosModelo.resetearExportacion();
            }
        });
    },

    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },

    editar: function (idAsiento) {
        window.location.href = "/modulos/contabilidad/asientosManualese.aspx?nM=0&id=" + idAsiento;
    },

    eliminar: function (id, nombre) {
        bootbox.confirm("¿Está seguro que desea eliminar el asiento modelo: " + nombre + "?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: "/modulos/contabilidad/asientosModelo.aspx/delete",
                    data: "{ id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        asientosModelo.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        });
    },

    cancelar: function () {
        window.location.href = "/modulos/contabilidad/asientosModelo.aspx";
    },
}





﻿var asientosPendientes = {
    configFilters: function () {
        $(".select2").select2({ width: '100%', allowClear: true });

        //Common.obtenerPersonas("ddlPersona", "", true);

        // Date Picker
        //configDatePicker();

        $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                resetearPagina();
                filtrar();
                return false;
            }
        });

        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
        $("#divResultado").hide();
    },
    procesar: function () {
        if ($('#frmSearch').valid()) {
            Common.mostrarProcesando("btnAceptar");

            $("#divResultado").show();
            $("#divTitle").html("Procesando...Este proceso puede tardar varios minutos, por favor espere a que el mismo finalice.");
            $("#msgResultado").html("");

            var info = "{ tipo: '" + $("#ddlTipo").val()
                       + "', fechaDesde: '" + $("#txtFechaDesde").val()
                       + "', fechaHasta: '" + $("#txtFechaHasta").val()
                       + "'}";

            $.ajax({
                type: "POST",
                url: "/modulos/contabilidad/asientospendientes.aspx/reprocesar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //timeout: 10000,
                success: function (data, text) {
                    $("#divTitle").html("Proceso finalizado");
                    $("#msgResultado").html(data.d);

                    Common.ocultarProcesando("btnAceptar", "Reprocesar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    Common.ocultarProcesando("btnAceptar", "Reprocesar");
                }
            });
        }
    },
    reprocesarPorID: function (tipo, id) {

        var info = "{ tipo: '" + $("#ddlTipo").val()
                   + "', id: " + id
                   + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/contabilidad/asientospendientes.aspx/reprocesarPorID",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //alert(data.d)
                bootbox.alert(data.d, function() {
                    
                });
                
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });

    }

}

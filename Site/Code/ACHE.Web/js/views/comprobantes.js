﻿/*** FORM EDICION ***/
var nroActual = "";//guarda nro del comprobante actual
var tipoActual = "";//guarda tipo del comprobante actual
var esModificacion = false;
var cantidadDecimales = 2;
var valorDecimalDefault = "1.00";

function loadInfo(id) {
    var integracion = false;
    if ($("#hdnIntegracion").val() != "0")
        integracion = true;
    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/obtenerDatos",
        data: "{ id: " + id + ",integracion:" + integracion + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null) {
                limpiarNuevoComprobante();

                var idPersona = 0;
                idPersona = data.d.IDPersona;
                if (integracion)
                    obtenerInfoPersona(parseInt($("#ddlPersona").val()), '');

                $("#ddlModo").val(data.d.Modo);
                if (data.d.Modo == "E")
                    $("#lnkGenerarCAE").show();
                else
                    $("#lnkGenerarCAE").hide();


                $("#txtFecha").val(data.d.Fecha);
                if (integracion && data.d.IDCondicionVenta == 0) {
                    $("#ddlCondicionVenta option:contains('MercadoPago')").attr('selected', true);
                }
                else
                    $("#ddlCondicionVenta").val(data.d.IDCondicionVenta);
                //$("#ddlCondicionVenta").val(data.d.IDCondicionVenta);
                $("#ddlProducto").val(data.d.TipoConcepto);
                $("#txtFechaVencimiento").val(data.d.FechaVencimiento);
                //fix para que traiga valores por defecto
                if (data.d.IDPuntoVenta > 0)
                    $("#ddlPuntoVenta").val(data.d.IDPuntoVenta);
                if (data.d.IDInventario > 0)
                    $("#ddlInventarios").val(data.d.IDInventario);

                $("#txtNumero").val(data.d.Numero);
                nroActual = $("#txtNumero").val();
                tipoActual = data.d.Tipo;
                $("#txtObservaciones").val(data.d.Observaciones);

                $("#txtImporteNoGrabado").val(data.d.Personas.ImporteNoGrabado);
                $("#txtPercepcionIVA").val(data.d.Personas.PercepcionIVA);
                $("#txtPercepcionIIBB").val(data.d.Personas.PercepcionIIBB);
                $("#hdnRazonSocial").val(data.d.Personas.RazonSocial);
                $("#txtEnvioPara").val(data.d.Personas.Email);

                $("#litPersonaCondicionIva").html("<strong>Condición IVA:</strong> " + Common.obtenerCondicionIvaDesc(data.d.Personas.CondicionIva));
                $("#ddlTipo").html("<option value=''></option>");
                Common.obtenerComprobantesVentaPorCondicion("ddlTipo", data.d.Personas.CondicionIva, data.d.Tipo);
                changelblPrecioUnitario(data.d.Personas.CondicionIva);
                Common.obtenerProvincias("ddlJuresdiccion", data.d.Personas.IDJuresdiccion, 10, true);
                if (data.d.Tipo != "")
                    changeTipoComprobante();

                obtenerItems();
                obtenerTotales();

                $("#divFactura,#lnkAceptar,#divFactura,#lnkPrevisualizar").show();

                setTimeout(function () {
                    $("#ddlPersona").attr("onchange", "changePersona()");
                }, 1000);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
        }
    });
}

function loadInfoAnulacion(id) {
    var integracion = false;
    if ($("#hdnIntegracion").val() != "0")
        integracion = true;
    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/obtenerDatosAnulacion",
        data: "{ id: " + id + ",integracion:" + integracion + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null) {
                limpiarNuevoComprobante();

                var idPersona = 0;
                idPersona = data.d.IDPersona;
                if (integracion)
                    obtenerInfoPersona(parseInt($("#ddlPersona").val()), '');

                $("#ddlModo").val(data.d.Modo);
                if (data.d.Modo == "E")
                    $("#lnkGenerarCAE").show();
                else
                    $("#lnkGenerarCAE").hide();


                $("#txtFecha").val(data.d.Fecha);
                //  $("#ddlCondicionVenta").val(data.d.CondicionVenta);
                $("#hdnIDCondicionVenta").val(data.d.IDCondicionVenta);

                $("#ddlCondicionVenta").val(data.d.IDCondicionVenta);

                $("#ddlProducto").val(data.d.TipoConcepto);
                $("#txtFechaVencimiento").val(data.d.FechaVencimiento);
                $("#ddlPuntoVenta").val(data.d.IDPuntoVenta);
                $("#ddlInventarios").val(data.d.IDInventario);

                $("#txtNumero").val(data.d.Numero);
                nroActual = $("#txtNumero").val();
                tipoActual = data.d.Tipo;
                $("#txtObservaciones").val(data.d.Observaciones);

                $("#txtImporteNoGrabado").val(data.d.Personas.ImporteNoGrabado);
                $("#txtPercepcionIVA").val(data.d.Personas.PercepcionIVA);
                $("#txtPercepcionIIBB").val(data.d.Personas.PercepcionIIBB);
                $("#hdnRazonSocial").val(data.d.Personas.RazonSocial);
                $("#txtEnvioPara").val(data.d.Personas.Email);

                $("#litPersonaCondicionIva").html("<strong>Condición IVA:</strong> " + Common.obtenerCondicionIvaDesc(data.d.Personas.CondicionIva));


                changelblPrecioUnitario(data.d.Personas.CondicionIva);
                Common.obtenerProvincias("ddlJuresdiccion", data.d.Personas.IDJuresdiccion, 10, true);
                changeModo();
                obtenerItems();
                obtenerTotales();

                $("#divFactura,#lnkAceptar,#divFactura,#lnkPrevisualizar").show();

                setTimeout(function () {
                    $("#ddlPersona").attr("onchange", "changePersona();");
                    //alert("a");
                    Common.obtenerNotaCreditoAnulacion('ddlTipo', data.d.Tipo);
                }, 1000);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
        }
    });
}

function cobrarComprobante() {
    window.location.href = "/cobranzase.aspx?IDComprobante=" + $("#hdnID").val();
}

/*** FORM ALTA ***/

function changeModo() {
    if ($("#ddlModo").val() == "E" && $("#ddlTipo").val() != "COT") {
        $("#divNroComprobante").hide();
        $("#lnkGenerarCAE").show();
        $("#lnkAceptar").text("Guardar borrador");
    }
    else {
        $("#divNroComprobante").show();
        $("#lnkGenerarCAE").hide();
        $("#lnkAceptar").text("Guardar");
        if ($("#txtNumero").val() == "0")
            Common.obtenerProxNroComprobante("txtNumero", $("#ddlTipo").val(), $("#ddlPuntoVenta").val());
    }

    if ($("#ddlTipo").val() == "COT") {
        $("#lnkAceptar").text("Guardar");
    }

    if ($("#txtNumero").val() == "")
        Common.obtenerProxNroComprobante("txtNumero", $("#ddlTipo").val(), $("#ddlPuntoVenta").val());
    if ($("#ddlTipo").val() == "NCA" || $("#ddlTipo").val() == "NCB" || $("#ddlTipo").val() == "NCC" || $("#ddlTipo").val() == "NDA" || $("#ddlTipo").val() == "NDB" || $("#ddlTipo").val() == "NDC") {
     
        //$("#ddlNroDeOrden").val("").trigger("change");;
        
        $("#divOrdenVenta").hide();
    }
    else {
        //$("#divOrdenVenta").show();
    }
}

function changePuntoDeVenta() {
    Common.obtenerProxNroComprobante("txtNumero", $("#ddlTipo").val(), $("#ddlPuntoVenta").val());
}

function changePersona() {
    limpiarNuevoComprobante();

    //if (parseInt($("#hdnIDPrecarga").val()) == 0)
    //    clearInfo();

    if ($("#ddlPersona").val() != "") {
        $("#lnkAceptar,#lnkGenerarCAE,#lnkPrevisualizar,#divFactura").show();
        obtenerInfoPersona(parseInt($("#ddlPersona").val()), '');

    }
    else {
        $("#lnkAceptar,#lnkGenerarCAE,#lnkPrevisualizar,#divFactura").hide();
    }
 
}

function changeConcepto() {
    //if (esModificacion == false) {
    if ($("#ddlProductos").val() != "" && $("#ddlProductos").val() != null) {
        $("#txtConcepto").attr("disabled", true);

        $.ajax({
            type: "POST",
            url: "conceptose.aspx/obtenerDatos",
            data: "{id: " + parseInt($("#ddlProductos").val()) + ",idPersona: " + parseInt($("#ddlPersona").val()) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null) {
                    $("#txtConcepto").val(data.d.Nombre);
                    $("#hdnCodigo").val(data.d.Codigo);
                    if (!esModificacion) {
                        $("#txtPrecio").val(data.d.Precio);
                        $("#ddlIva").val(data.d.Iva);//.trigger("change");
                        if (data.d.idPlanDeCuentaVenta > 0) {
                            $("#ddlPlanDeCuentas").val(data.d.idPlanDeCuentaVenta).trigger("change");
                            //  $("#ddlPlanDeCuentas").trigger("change");
                        }
                    }
                    esModificacion = false;
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorDetalle").html(r.Message);
                $("#divErrorDetalle").show();
            }
        });
    }
    else {
        $("#txtConcepto").attr("disabled", false);
    }
}

function limpiarNuevoComprobante() {
    obtenerCantidadDecimales(function (serverData) {
        cantidadDecimales = serverData.d;
        valorDecimalDefault = obtenerValorDecimalDefault(serverData.d, 1);

        ocultarMensajes();
        $("#ddlProducto").val("3");
        //$("#ddlPuntoVenta").html("<option value=''></option>");
        $("#txtNumero").val("");
        $("#ddlTipo").html("<option value=''></option>");
        $("#txtCantidad, #txtConcepto, #txtPrecio, #txtBonificacion").val("");

        $("#txtCantidad").val(valorDecimalDefault);
        //$("#ddlIva").val("0,00");

        if (MI_CONDICION == "MO") {
            $("#ddlIva").val("0,00");//.trigger("change");
        }
        else if (MI_CONDICION == "RI") {
            $("#ddlIva").val("21,00");//.trigger("change");
        }

        $("#hdnIDItem").val("0");
        //$("#bodyDetalle").html("<tr><td colspan='9' style='text-align:center'>No tienes items agregados</td></tr>");
        //$("#divSubtotal, #divIVA, #divTotal").html("0");
        //$("#txtObservaciones").val("");
    });
}

function modificarTipoComprobante() {
    if ($("#ddlTipo").val() == tipoActual)
        $("#txtNumero").val(nroActual)
    else
        $("#txtNumero").val("")
}

function obtenerInfoPersona(idPersona, tipo) {
    $.ajax({
        type: "POST",
        url: "personase.aspx/obtenerDatos",
        data: "{ id: " + idPersona + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null) {
                $("#ddlTipo").html("<option value=''></option>");

                Common.obtenerComprobantesVentaPorCondicion("ddlTipo", data.d.CondicionIva, tipo);

                $("#hdnRazonSocial").val(data.d.RazonSocial);
                $("#txtEnvioPara").val(data.d.Email);
                changelblPrecioUnitario(data.d.CondicionIva);

                if (tipo != "") {
                    changeTipoComprobante();
                } else {
                    ObtenerUltimoTipoFacturacionCliente();
                    if ($("#ddlModo").val() == "E") {
                        $("#txtNumero").val("0");
                        $("#divNroComprobante").hide();
                        $("#lnkAceptar").text("Guardar borrador");
                    }
                }
                //obtenerOrdenesVenta();
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
        }
    });
}

function changelblPrecioUnitario(condicionIVA) {
    //if (condicionIVA == "MO" || condicionIVA == "CF") {
    //    $("#spPrecioUnitario").text("Precio Unit. con IVA");
    //}
    //else {
    if ($("#hdnUsaPrecioConIVA").val() == "1") {
        $("#spPrecioUnitario").text("Precio Unit. con IVA");
    } else {
        $("#spPrecioUnitario").text("Precio Unit. sin IVA");
    }
    //}
}

function changeCondicionDeVenta() {
    var info = "{idCondicionDeVenta: " + $("#ddlCondicionVenta").val() + "}";

    $.ajax({
        type: "POST",
        url: "/common.aspx/TieneCobranzaAutomatica",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                $("#hdnCobranzaAutomatica").val(data.d);
            }
        }
    });
}

function ObtenerUltimoTipoFacturacionCliente() {

    var info = "{idPersona: " + $("#ddlPersona").val() + "}";
    $.ajax({
        type: "POST",
        url: "/common.aspx/ObtenerUltimoTipoFacturacionCliente",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null && data.d != "") {
                $("#ddlTipo").val(data.d);
                changeTipoComprobante();
            }
        }
    });

}

function changeTipoComprobante() {
    modificarTipoComprobante();
    if ($("#ddlTipo").val() == "COT") {
        $("#lnkGenerarCAE,#divModo").hide();
    }
    else {
        if ($("#hdnTieneFE").val() == "1") {
            if ($("#hdnID").val() == "0") {
                $("#ddlModo").val("E");
                $("#lnkAceptar").text("Guardar borrador");
            }
            //changeModo();
        }
        else {
            $("#ddlModo").val("T");
            $("#lnkAceptar").text("Aceptar");
        }
        $("#divModo").show();
    }
    changeModo();
   
}

/*** ITEMS ***/
function cancelarItem() {
    obtenerCantidadDecimales(function (serverData) {
        cantidadDecimales = serverData.d;
        valorDecimalDefault = obtenerValorDecimalDefault(serverData.d, 1);

        $("#txtConcepto, #txtCantidad, #txtPrecio, #txtBonificacion, #hdnCodigo").val("");
        $("#txtCantidad").val(valorDecimalDefault);
        $("#hdnIDItem").val("0");
        $("#ddlProductos").val("").trigger("change");
        $("#btnAgregarItem").html("Agregar");
    });
}

function agregarItem() {
    ocultarMensajes();
    esModificacion = false;
    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        if ($("#ddlPlanDeCuentas").val() == "") {
            $("#msgErrorDetalle").html("Debes ingresar la cuenta contable.");
            $("#divErrorDetalle").show();
            return false;
        }
    }

    if ($("#txtCantidad").val() != "" && $("#txtConcepto").val() != "" && $("#txtPrecio").val() != "") {

        if (parseFloat($("#txtPrecio").val()) == 0) {
            $("#msgErrorDetalle").html("El precio debe ser mayor a 0.");
            $("#divErrorDetalle").show();
        }
        else if (parseFloat($("#txtCantidad").val()) == 0) {
            $("#msgErrorDetalle").html("La cantidad debe ser mayor a 0.");
            $("#divErrorDetalle").show();
        }
        else {

            var idPersona = ($("#ddlPersona").val() == "" || $("#ddlPersona").val() == null) ? 0 : parseInt($("#ddlPersona").val());
            var idPlanDeCuenta = ($("#ddlPlanDeCuentas").val() == "" || $("#ddlPlanDeCuentas").val() == null) ? 0 : parseInt($("#ddlPlanDeCuentas").val());
            var idInventario = ($("#ddlInventarios").val() != null && $("#ddlInventarios").val() != "") ? parseInt($("#ddlInventarios").val()) : 0;

            var codigoCta = (idPlanDeCuenta == 0) ? "" : $("#ddlPlanDeCuentas option:selected").text();
            var url = "";
            if ($("#hdnID").val() != "" && $("#hdnID").val() != "0") {
                var info = "{ id: " + parseInt($("#hdnIDItem").val())
                        + ", idConcepto: '" + $("#ddlProductos").val()
                        + "', concepto: '" + $("#txtConcepto").val()
                        + "', iva: '" + $("#ddlIva").val()
                        + "', precio: '" + $("#txtPrecio").val()
                        + "', bonif: '" + $("#txtBonificacion").val()
                        + "', cantidad: '" + $("#txtCantidad").val()
                        + "' , idPersona: " + idPersona
                        + " , codigo:'" + $("#hdnCodigo").val()
                        + "', idPlanDeCuenta:' " + idPlanDeCuenta
                        + "', codigoCta:' " + codigoCta
                        + "', idInventario: " + idInventario
                        + ", idComprobante: " + $("#hdnID").val()
                        + "}";
                url = "comprobantese.aspx/agregarItemEdicion";
            } else {
                var info = "{ id: " + parseInt($("#hdnIDItem").val())
                        + ", idConcepto: '" + $("#ddlProductos").val()
                        + "', concepto: '" + $("#txtConcepto").val()
                        + "', iva: '" + $("#ddlIva").val()
                        + "', precio: '" + $("#txtPrecio").val()
                        + "', bonif: '" + $("#txtBonificacion").val()
                        + "', cantidad: '" + $("#txtCantidad").val()
                        + "' , idPersona: " + idPersona
                        + " , codigo:'" + $("#hdnCodigo").val()
                        + "', idPlanDeCuenta:' " + idPlanDeCuenta
                        + "', codigoCta:' " + codigoCta
                        + "', idInventario: " + idInventario
                        + "}";
                url = "comprobantese.aspx/agregarItem";

            }

            $.ajax({
                type: "POST",
                url: url,
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#txtConcepto, #txtCantidad, #txtPrecio, #txtBonificacion").val("");
                    $("#txtConcepto").attr("disabled", false);
                    $("#ddlProductos").val("").trigger("change");
                    $("#hdnIDItem").val("0");
                    $("#hdnCodigo").val("");
                    $("#btnAgregarItem").html("Agregar");
                    $("#txtCantidad").focus();
                    $("#txtCantidad").val(valorDecimalDefault);

                    if ($("#hdnUsaPlanCorporativo").val() == "1") {
                        $("#ddlPlanDeCuentas").val("").trigger("change");
                    }

                    verificarPlanDeCuentas();
                    obtenerItems();
                    obtenerTotales();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorDetalle").html(r.Message);
                    $("#divErrorDetalle").show();
                }
            });
        }

    }
    else {
        $("#msgErrorDetalle").html("Debes ingresar la cantidad, concepto y precio.");
        $("#divErrorDetalle").show();
    }
}

function eliminarItem(id) {

    var info = "{ id: " + parseInt(id) + "}";

    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/eliminarItem",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            obtenerItems();
            obtenerTotales();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgErrorDetalle").html(r.Message);
            $("#divErrorDetalle").show();
        }
    });
}

function modificarItem(id, idConcepto, cantidad, concepto, precio, iva, bonif, idPlanDeCuenta, codigo) {

    $("#txtCantidad").val(cantidad);
    $("#txtConcepto").val(concepto);
    $("#txtPrecio").val(precio);
    $("#ddlIva").val(iva);//.trigger("change");
    $("#txtBonificacion").val(bonif);


    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        $("#ddlPlanDeCuentas").val(idPlanDeCuenta);
        $("#ddlPlanDeCuentas").trigger("change");
    }

    esModificacion = true;
    if (idConcepto != "") {
        $("#ddlProductos").val(idConcepto).trigger("change");
        $("#txtConcepto").attr("disabled", true);
    }
    //$("#ddlIva").val(iva);//.trigger("change");

    $("#hdnIDItem").val(id);
    $("#hdnCodigo").val(codigo)
    $("#btnAgregarItem").html("Actualizar");
}

function obtenerItems() {

    var idPersona = ($("#ddlPersona").val() == "" || $("#ddlPersona").val() == null) ? 0 : parseInt($("#ddlPersona").val());

    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/obtenerItems",
        data: "{idPersona: " + idPersona + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
        }
    });
}

function obtenerTotales() {

    var PercepcionIIBB = $("#txtPercepcionIIBB").val() == "" ? "0" : $("#txtPercepcionIIBB").val();
    var PercepcionIVA = $("#txtPercepcionIVA").val() == "" ? "0" : $("#txtPercepcionIVA").val();
    var ImporteNoGrabado = $("#txtImporteNoGrabado").val() == "" ? "0" : $("#txtImporteNoGrabado").val();
    var juris = $("#ddlJuresdiccion").val() == "" ? "" : $("#ddlJuresdiccion option:selected").text();

    var info = "{ percepcionesIIBB: '" + PercepcionIIBB
            + "', percepcionesIVA: '" + PercepcionIVA
            + "', importeNoGrabado: '" + ImporteNoGrabado
            + "', jurisdiccion: '" + juris
            + "'}";
    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/obtenerTotales",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null) {
                $("#divSubtotal").html("$ " + data.d.Subtotal);
                $("#divNoGravado").html("$ " + data.d.ImporteNoGrabado);
                $("#divIVA").html("$ " + data.d.Iva);

                $("#trIVATotal").html("$ " + data.d.PercepcionIVA);
                $("#trIIBBTotal").html("$ " + data.d.PercepcionIIBB);


                $("#divTotal").html("$ " + data.d.Total);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgErrorDetalle").html(r.Message);
            $("#divErrorDetalle").show();
        }
    });
}

function ocultarMensajes() {
    $("#divError, #divOk, #divErrorDetalle").hide();
}

function grabar(generarCAE) {
    ocultarMensajes();

    if ($('#frmEdicion').valid()) {
        if ($('#ddlPuntoVenta > option').length == 0) {
            $("#msgError").html("Su usuario NO posee puntos de venta habilitados. Contactese con el administrador de su cuenta");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else if ($("#ddlTipo").val() != "COT" && $("#ddlModo").val() == "O") {
            $("#msgError").html("El modo del comprobante es inválido para un " + $("#ddlTipo option:selected").text());
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else if ($("#ddlPuntoVenta").val() == "" || $("#ddlPuntoVenta").val() == null || $("#ddlPuntoVenta").val() == undefined) {
            $("#msgError").html("Seleccione el punto de venta.");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else {
            Common.mostrarProcesando("lnkAceptar");
            Common.mostrarProcesando("lnkGenerarCAE");

            var idJurisdiccion = ($("#ddlJuresdiccion").val() != null && $("#ddlJuresdiccion").val() != "") ? parseInt($("#ddlJuresdiccion").val()) : 0;
            var tipoConcepto = ($("#ddlProducto").val() != null && $("#ddlProducto").val() != "") ? parseInt($("#ddlProducto").val()) : 0;
            var idInventario = ($("#ddlInventarios").val() != null && $("#ddlInventarios").val() != "") ? parseInt($("#ddlInventarios").val()) : 0;
            //var idOrden = (($("#ddlNroDeOrden").val() == "" || $("#ddlNroDeOrden").val() == null) ? "0" : $("#ddlNroDeOrden").val());
            var idOrden = ($("#hdnIDNroOrden").val());


            var info = "{id: " + parseInt($("#hdnID").val())
                    + ", idPersona: " + parseInt($("#ddlPersona").val())
                    + ", tipo: '" + $("#ddlTipo").val()
                    + "', modo: '" + $("#ddlModo").val()
                    + "', fecha: '" + $("#txtFecha").val()
                    + "', condicionVenta: '" + $("#ddlCondicionVenta").val()
                    + "', tipoConcepto: " + parseInt(tipoConcepto)
                    + ", fechaVencimiento: '" + $("#txtFechaVencimiento").val()
                    + "', idPuntoVenta: " + $("#ddlPuntoVenta").val()
                    + ", nroComprobante: '" + $("#txtNumero").val()
                    + "', idInventario: " + idInventario
                    + ", obs: '" + $("#txtObservaciones").val()
                    + "',idJurisdiccion: " + idJurisdiccion
                    + ",idIntegracion: " + $("#hdnIntegracion").val()
                    + ",idVentaIntegracion: '" + $("#hdnVentaIntegracion").val()
                    + "',idOrdenVenta: '" + idOrden
                    + "'}";
            $.ajax({
                type: "POST",
                url: "comprobantese.aspx/guardar",
                data: info,
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    Common.ocultarProcesando("lnkAceptar", "Aceptar");
                    Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
                    $("#hdnID").val(data.d);

                    if ($("#ddlTipo").val() == "NCA" || $("#ddlTipo").val() == "NCB" || $("#ddlTipo").val() == "NCC" || $("#ddlTipo").val() == "NDA" || $("#ddlTipo").val() == "NDB" || $("#ddlTipo").val() == "NDC") {
                        $("#divRemito").hide();
                    }
                    else {
                        $("#divRemito").show();
                    }
                    if (generarCAE) {
                        // $("#hdnID").val(data.d);
                        $("#litModalOkTitulo").html("Comprobante emitido correctamente");
                        generarCae();
                        //$('#modalOk').modal('show');
                    }
                    else {
                        $('#divOk').show();
                        $("#divError").hide();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        //Muestro la ventana

                        $('#divPrintpdf,#divDownloadPdf,#divSendMail').hide();

                        //Muestro la ventana           
                        if ($("#hdnUsaPlanCorporativo").val() == "1") {// && $("#ddlModo").val() == "T") {
                            generarAsientosContables();
                        }
                        else {
                         
                            $('#modalOk').modal('show');
                        }
                        if ($("#hdnCobranzaAutomatica").val() == "1" && ($("#ddlTipo").val() != "NCA" && $("#ddlTipo").val() != "NCB" && $("#ddlTipo").val() != "NCC" && $("#ddlTipo").val() != "NCE")) {
                            $("#spRealizarCobranza").html("Cobranza emitida");
                            $("#imgCobranza").attr("style", "color:#17a08c;font-size: 30px;opacity: 1; border: none; padding: 0");
                            $("#spRealizarCobranza").attr("style", "color:#17a08c");
                            $("#iCheckCobranza").addClass("fa fa-check");
                            $('#divComprobante').removeAttr('onclick');
                        }
                    }

                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("lnkAceptar", "Aceptar");
                    Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
                }
            });
        }
    }
    else {
        return false;
    }
}


function generarAsientosContables() {
    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/GenerarAsientosContables",
        data: "{ id: " + $("#hdnID").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#divOkAsientos").show();
            $('#modalOk').modal('show');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgErrorAsientos").html(r.Message);
            $("#divErrorAsientos").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            $('#modalOk').modal('show');
        }
    });
}

function enviarComprobanteAutomaticamente() {
    var info = "{ idComprobante: " + $("#hdnID").val() + " }";
    $.ajax({
        type: "POST",
        url: "/comprobantese.aspx/enviarComprobanteAutomaticamente",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            $("#spSendMail").html("Enviado");
            $("#imgMailEnvio").attr("style", "color:#17a08c;font-size: 30px;");
            $("#spSendMail").attr("style", "color:#17a08c");
            $("#iCheckEnvio").addClass("fa fa-check");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgErrorEnvioFE").html(r.Message);
            $("#divErrorEnvioFE").show();
            $("#divSendMail").show();
            $("#iCheckEnvio").removeClass("fa fa-check");
            $('#modalOk').modal('show');
        }
    });
}

function previsualizar() {
    if ($("#hdnFile").val() == "") {//Si no fue generado el archivo
        if ($('#frmEdicion').valid()) {
            var info = "{id: " + parseInt($("#hdnID").val())
                    + ", idPersona: " + parseInt($("#ddlPersona").val())
                    + ", tipo: '" + $("#ddlTipo").val()
                    + "', modo: '" + $("#ddlModo").val()
                    + "', fecha: '" + $("#txtFecha").val()
                    + "', condicionVenta: '" + $("#ddlCondicionVenta").val()
                    + "', tipoConcepto: " + parseInt($("#ddlProducto").val())
                    + ", fechaVencimiento: '" + $("#txtFechaVencimiento").val()
                    + "', idPuntoVenta: " + $("#ddlPuntoVenta").val()
                    + ", nroComprobante: '" + $("#txtNumero").val()
                    + "', obs: '" + $("#txtObservaciones").val()
                    + "'}";

            $.ajax({
                type: "POST",
                url: "comprobantese.aspx/previsualizar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    var version = new Date().getTime();
                    $("#ifrPdf").attr("src", "/files/comprobantes/" + data.d + "?" + version + "#zoom=100&view=FitH,top");

                    $('#modalPdf').modal('show');
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else {
            $("#msgError").html("Formulario Invalido Complete todos los datos obligatorios");
            $("#divError").show();
            return false;
        }
    }
    else {
        return false;
    }
}

function generarCae() {

    console.log("thos one")

    Common.mostrarProcesando("lnkAceptar");
    Common.mostrarProcesando("lnkGenerarCAE");
    var tipoConcepto = ($("#ddlProducto").val() != null && $("#ddlProducto").val() != "") ? parseInt($("#ddlProducto").val()) : 0;
    var info = "{id: " + parseInt($("#hdnID").val())
            + ", idPersona: " + parseInt($("#ddlPersona").val())
            + ", tipo: '" + $("#ddlTipo").val()
            + "', modo: '" + $("#ddlModo").val()
            + "', fecha: '" + $("#txtFecha").val()
            + "', condicionVenta: '" + $("#ddlCondicionVenta").val()
            + "', tipoConcepto: " + parseInt(tipoConcepto)
            + ", fechaVencimiento: '" + $("#txtFechaVencimiento").val()
            + "', idPuntoVenta: " + $("#ddlPuntoVenta").val()
            + ", nroComprobante: '" + $("#txtNumero").val()
            + "', obs: '" + $("#txtObservaciones").val()
            + "'}";
    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/generarCae",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            //Seteo el link de download
            //var fileName = data.d.split("/")[1];
            var fileName = data.d;
            $("#lnkDownloadPdf").attr("href", "/pdfGenerator.ashx?file=" + fileName.Comprobante);
            //$("#lnkDownloadRemito").attr("href", "/pdfGenerator.ashx?file=" + fileName.Remito);

            var d = new Date();
            var n = d.getFullYear();
            var version = new Date().getTime();
            var pathFile = "/files/explorer/" + $("#hdnIDUsuario").val() + "/comprobantes/" + n + "/" + fileName.Comprobante + "?" + version + "#zoom=100&view=FitH,top";
            $("#ifrPdf").attr("src", pathFile);

            //Seteo el nombre del archivo para envio de mail
            $("#hdnFile").val(fileName.Comprobante);

            if ($("#hdnEnvioFE").val() == "1") {
                enviarComprobanteAutomaticamente();
            }
            else {
                $("#iCheckEnvio").removeClass("fa fa-check");
            }

            if (fileName.Observaciones != "") {
                $("#divOkObservaciones").html(fileName.Observaciones);
                $("#divOkObservaciones").show();
            }
            else
                $("#divOkObservaciones").hide();

            //Muestro la ventana           
            if ($("#hdnUsaPlanCorporativo").val() == "1") {
                generarAsientosContables();
            }
            else {
                $('#modalOk').modal('show');
            }
            if ($("#hdnCobranzaAutomatica").val() == "1" && ($("#ddlTipo").val() != "NCA" && $("#ddlTipo").val() != "NCB" && $("#ddlTipo").val() != "NCC" && $("#ddlTipo").val() != "NCE")) {
                $("#spRealizarCobranza").html("Cobranza emitida");
                $("#imgCobranza").attr("style", "color:#17a08c;font-size: 30px;opacity: 1; border: none; padding: 0");
                $("#spRealizarCobranza").attr("style", "color:#17a08c");
                $("#iCheckCobranza").addClass("fa fa-check");
                $('#divComprobante').removeAttr('onclick');
            }

            Common.ocultarProcesando("lnkAceptar", "Aceptar");
            Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            Common.ocultarProcesando("lnkAceptar", "Aceptar");
            Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
        }
    });
}

function cancelar() {
    window.location.href = "/comprobantes.aspx";
}

function configForm() {
    Common.obtenerPersonas("ddlPersona", $("#hdnIDPersona").val(), true);
    Common.obtenerConceptosCodigoyNombre("ddlProductos", 3, true);
    Common.obtenerInventariosActivos("ddlInventarios", false);

    //$(".select2").select2({
    //    width: '100%', allowClear: true, minimumInputLength: 2,
    //    formatNoMatches: function (term) {
    //        return "<a style='cursor:pointer' onclick=\"$('#modalNuevoCliente').modal('show');$('.select2').select2('close');\">+ Agregar</a>";
    //    }
    //});

    if (MI_CONDICION == "MO") {
        $("#ddlIva").attr("disabled", true);
    }
    else if (MI_CONDICION == "RI") {
        $("#ddlIva").val("21,00");//.trigger("change");
    }
    else
        $("#ddlIva").val("0,00");//.trigger("change");

    $.validator.addMethod("validFechaActual", function (value, element) {

        var fInicio = $("#txtFecha").val().split("/");
        var fechainicio = new Date(fInicio[2], (fInicio[1] - 1), fInicio[0]);

        var today = new Date();
        var fechaFin = new Date(today);
        fechaFin.setDate(today.getDate() + 5);

        if (fechainicio > fechaFin) {
            return false;
        }
        else {
            return true;
        }
    }, "Solo puede facturar hasta 5 dias despues de la fecha actual");

    $('#txtFechaVencimiento').datepicker();
    Common.configDatePicker();

    $("#txtCantidad, #txtPrecio,#txtBonificacion").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $("#txtCantidad").focus();
            agregarItem();
            return false;
        }
    });

    //$("#txtCantidad").numericInput();

    $("#txtNumero").mask("?99999999");
    $("#txtNumero").blur(function () {
        $("#txtNumero").val(padZeros($("#txtNumero").val(), 8));
    });

    obtenerCantidadDecimales(function (serverData) {
        cantidadDecimales = serverData.d;
        valorDecimalDefault = obtenerValorDecimalDefault(serverData.d, 1);
        $("#txtCantidad, #txtBonificacion,#txtImporteNoGrabado,#txtPercepcionIVA,#txtPercepcionIIBB").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            precision: serverData.d
        });
        $("#txtPrecio").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            allowNegative: true,
            precision: serverData.d
        });

        $("#txtCantidad").val(valorDecimalDefault);
        //alert($("#txtCantidad").val());
    });

    $("#txtEnvioPara, #txtEnvioAsunto, #txtEnvioMensaje").keypress(function (event) {
        var aux = Toolbar.toggleEnviosError();
    });
    $("#txtEnvioPara, #txtEnvioAsunto, #txtEnvioMensaje").blur(function (event) {
        var aux = Toolbar.toggleEnviosError();
    });

    // Validation with select boxes
    $("#frmEdicion").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    })

    $("#lnkAceptar,#lnkGenerarCAE").hide();

    //if ($("#hdnIntegracion").val() != "0") {
    //    obtenerPuntosDeVentaIntegracion("ddlPuntoVenta");
    //}
    //else
    obtenerPuntosDeVenta("ddlPuntoVenta");
    obtenerCondicionesDeVenta("ddlCondicionVenta", $("#hdnIDCondicionVenta").val());

    $(".select3").select2({ width: '100%', allowClear: true });
    $(".select4").select2({ width: '100%', allowClear: true, minimumInputLength: 2 });

    if ($("#hdnDuplicar").val() != "" && $("#hdnDuplicar").val() != "0") {
        loadInfo($("#hdnDuplicar").val());
    }
    else if ($("#hdnAnular").val() != "" && $("#hdnAnular").val() != "0") {
        loadInfoAnulacion($("#hdnAnular").val());
    }
    else if (($("#hdnID").val() != "" && $("#hdnID").val() != "0") || $("#hdnIntegracion").val() != "0") {
        loadInfo($("#hdnID").val());
        $("#lnkAceptar").html("Actualizar");
    }
    else {
        Common.obtenerProvincias("ddlJuresdiccion", "", 10, true);
        $("#ddlPersona").attr("onchange", "changePersona()");
        $("#lnkPrevisualizar").hide();
    }
    mostrarPreCarga();
    mostrarPercepciones();
    verificarPlanDeCuentas();
    //obtenerOrdenesVenta();
}

//function obtenerOrdenesVenta() {
//    var idComprador = $("#ddlPersona").val();
//    if (idComprador == "")
//        idComprador = "0";
//    var idorden = $("#hdnIDNroOrden").val();
//    var idComprobante = $("#hdnID").val();
//    if ((idComprobante != "0" && idorden != "0") || idComprobante == "0") {
//        $.ajax({
//            type: "POST",
//            url: "/comprobantese.aspx/obtenerOrdenesVenta",
//            data: "{idComprador: " + parseInt(idComprador) + ",idOrden:" + parseInt(idorden) + "}",
//            async: false,
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function(data) {
//                if (data != null) {
//                    $("#ddlNroDeOrden").html("");
//                    var idSelected = $("#hdnIDNroOrden").val();

//                    $("<option/>").attr("value", "").text("").appendTo($("#ddlNroDeOrden"));


//                    for (var i = 0; i < data.d.length; i++) {

//                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlNroDeOrden"));
//                    }

//                    if (idSelected != "0")
//                        $("#ddlNroDeOrden").val(idSelected);

//                    $("#ddlNroDeOrden").trigger("change");

//                }
//            }
//        });
//    }
//}

function cargarOrden() {
    //si es edicion o viene desde integraciones.aspx no se llama a este metodo porque sino se pisarian los items con los de la orden de venta
    //si es edicion, lo unico que puede hacer el usuario es sacar la orden de venta asociada
    var idComprobante = $("#hdnID").val();
    var idIntegracion = $("#hdnIntegracion").val();
   
    if ($("#ddlNroDeOrden").val() == null || $("#ddlNroDeOrden").val() == "")
        $("#hdnIDNroOrden").val("0");
    else
        $("#hdnIDNroOrden").val($("#ddlNroDeOrden").val());
        
    if ((idComprobante == "0" && idIntegracion == "0") || $("#hdnIDNroOrden").val()=="0") {
        if ($("#hdnIDNroOrden").val() != "0") {
            $.ajax({
                type: "POST",
                url: "comprobantese.aspx/CargarOrdenVenta",
                data: "{ idOrden: " + $("#hdnIDNroOrden").val() + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, text) {

                    verificarPlanDeCuentas();
                    obtenerItems();
                    obtenerTotales();
                },
                error: function(response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }

    }
}


function verificarPlanDeCuentas() {
    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        $(".divPlanDeCuentas").show();
    }
    else {
        $(".divPlanDeCuentas").hide();
    }
}

function mostrarPreCarga() {
    if (parseInt($("#hdnIDPrecarga").val()) > 0) {
        $("#ddlPersona").trigger("change");
        obtenerItems();
        obtenerTotales();
        $("#ddlPersona").attr("onchange", "changePersona()");
    }
}

function mostrarPercepciones() {
    if (MI_CONDICION == "MO") {
        $("#trIVA,#trIIBB,#divPercepcionesIIBB,#divPercepcionesIVA,#divPercepciones").hide();//#trImpNoGrabado,
    }
    else {
        //$("#trImpNoGrabado").show();
        $("#divPercepciones").hide();

        if ($("#hdnPercepcionIVA").val() == "1")
            $("#trIVA,#divPercepcionesIVA,#divPercepciones").show();
        else
            $("#trIVA,#divPercepcionesIVA").hide();

        if ($("#hdnPercepcionIIBB").val() == "1")
            $("#trIIBB,#divPercepcionesIIBB,#divPercepciones").show();
        else
            $("#trIIBB,#divPercepcionesIIBB").hide();

        if ($("#hdnPercepcionIVA").val() == "1" || $("#hdnPercepcionIIBB").val() == "1") {
            $("#htextoFactura").html("4. Productos o servicios facturados");
        }
        else {
            $("#htextoFactura").html("3. Productos o servicios facturados");
        }
    }
}

function clearInfo() {
    $.ajax({
        type: "POST",
        url: "comprobantese.aspx/clearInFo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
    });
}

/*** SEARCH ***/

function configFilters() {
    //$(".select2").select2({ width: '100%', allowClear: true });

    $("#txtCondicion").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            resetearPagina();
            filtrar();
            return false;
        }
    });

    // Date Picker
    Common.configDatePicker();
    Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    // validation with select boxes
    $("#frmsearch").validate({
        highlight: function (element) {
            jquery(element).closest('.form-group').removeclass('has-success').addclass('has-error');
        },
        success: function (element) {
            jquery(element).closest('.form-group').removeclass('has-error');
        },
        errorelement: 'span',
        errorclass: 'help-block',
        errorplacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertafter(element.parent());
            } else {
                error.insertafter(element);
            }
        }
    });

    facturaRapida.configForm();
    impresionMasiva.configForm();
}

function nuevo() {
    window.location.href = "/comprobantese.aspx";
}

function editar(id) {
    window.location.href = "/comprobantese.aspx?ID=" + id;
}

function duplicar(id) {
    window.location.href = "/comprobantese.aspx?ID=" + id + "&Duplicar=1";
}

function anular(id) {
    window.location.href = "/comprobantese.aspx?ID=" + id + "&Anular=1";
}

function generarAbono(id) {
    bootbox.confirm("¿Desea generar un abono a partir de este comprobante?", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: "comprobantes.aspx/generarAbono",
                data: "{ id: " + id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    window.location = "abonose.aspx?ID=" + data.d;
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function eliminar(id, nombre) {
    bootbox.confirm("¿Está seguro que desea eliminar el comprobante realizado a " + nombre + "?", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: "comprobantes.aspx/delete",
                data: "{ id: " + id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function mostrarPagAnterior() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual--;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function mostrarPagProxima() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual++;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function resetearPagina() {
    $("#hdnPage").val("1");
}

function filtrar() {
    $("#divError").hide();

    $("#resultsContainer").empty();

    if ($('#frmSearch').valid()) {

        var currentPage = parseInt($("#hdnPage").val());

        var info = "{ condicion: '" + $("#txtCondicion").val()
                   + "', periodo: '" + $("#ddlPeriodo").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                   + "}";

        $.ajax({
            type: "POST",
            url: "comprobantes.aspx/getResults",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#resultsContainer").empty();
                if (data.d.TotalPage > 0) {

                    $("#divPagination").show();

                    $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                    if (data.d.TotalPage == 1)
                        $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                    else if (currentPage == data.d.TotalPage)
                        $("#lnkNextPage").attr("disabled", "disabled");
                    else if (currentPage == 1)
                        $("#lnkPrevPage").attr("disabled", "disabled");

                    var aux = (currentPage * PAGE_SIZE);
                    if (aux > data.d.TotalItems)
                        aux = data.d.TotalItems;
                    $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                }
                else {
                    $("#divPagination").hide();
                    $("#msjResultados").html("");
                }

                // Render using the template
                if (data.d.Items.length > 0)
                    $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                else
                    $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

        resetearExportacion();
    }
}

function verTodos() {
    $("#txtCondicion, #txtFechaDesde, #txtFechaHasta").val("");
    $("#ddlPersona").val("").trigger("change");
    filtrar();
}

function exportar() {
    resetearExportacion();

    $("#imgLoading").show();
    $("#divIconoDescargar").hide();

    var info = "{  condicion: '" + $("#txtCondicion").val()
              + "', periodo: '" + $("#ddlPeriodo").val()
              + "', fechaDesde: '" + $("#txtFechaDesde").val()
              + "', fechaHasta: '" + $("#txtFechaHasta").val()
              + "'}";

    $.ajax({
        type: "POST",
        url: "comprobantes.aspx/export",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            resetearExportacion();
        }
    });
}

function resetearExportacion() {
    $("#imgLoading, #lnkDownload").hide();
    $("#divIconoDescargar").show();
}

function otroPeriodo() {
    if ($("#ddlPeriodo").val() == "-1")
        $('#divMasFiltros').toggle(600);
    else {
        if ($("#divMasFiltros").is(":visible"))
            $('#divMasFiltros').toggle(600);

        $("#txtFechaDesde,#txtFechaHasta").val("");
        filtrar();
    }
}

function enviarEmail() {
    $("#divErrorMail").hide();
    Toolbar.mostrarEnvio();
    $('#modalEMail').modal('show');
}

function generarRemito() {
    var info = "{ id: " + $("#hdnID").val() + "}";

    $.ajax({
        type: "POST",
        url: "comprobantesv.aspx/generarRemito",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            window.location.href = "/pdfGenerator.ashx?file=" + data.d + "&tipoDeArchivo=remito";
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}

var facturaRapida = {
    armarSelectSegunCondicionIVA: function () {

        var valor = $("#ddlFactRapidaTipoDoc").val();

        if ($("#ddlFactRapidaCondicionIva").val() == "CF") {
            $("#ddlFactRapidaTipoDoc").html("");
            //$("<option/>").attr("value", "").text("").appendTo($("#ddlFactRapidaTipoDoc"));
            $("<option/>").attr("value", "DNI").text("DNI").appendTo($("#ddlFactRapidaTipoDoc"));
            $("#ddlFactRapidaTipoDoc").val("DNI");
        }
        else {
            $("#ddlFactRapidaTipoDoc").html("");
            //$("<option/>").attr("value", "").text("").appendTo($("#ddlFactRapidaTipoDoc"));
            $("<option/>").attr("value", "CUIT").text("CUIT/CUIL").appendTo($("#ddlFactRapidaTipoDoc"));
            //$("<option/>").attr("value", "DNI").text("DNI").appendTo($("#ddlFactRapidaTipoDoc"));
            $("#ddlFactRapidaTipoDoc").val("CUIT");

        }

        //if (valor != null && valor != "") {
        //    $("#ddlFactRapidaTipoDoc").val(valor);
        //    $("#ddlFactRapidaTipoDoc").trigger("change");
        //}
    },
    changeCondicionIva: function () {
        facturaRapida.armarSelectSegunCondicionIVA();
        if ($("#ddlFactRapidaCondicionIva").val() == "CF") {
            $("#ddlFactRapidaTipoDoc").val("DNI");
            $("#ddlFactRapidaTipoDoc").trigger("change");
            //$("#divPersoneria").hide();
            //$("#spIdentificacionObligatoria").hide();
            $("#ddlFactRapidaTipoDoc,#txtFactRapidaNroDocumento").removeClass("required");
        }
        else if ($("#ddlFactRapidaCondicionIva").val() == "RI") {
            $("#ddlFactRapidaTipoDoc").val("CUIT");
            $("#ddlFactRapidaTipoDoc").trigger("change");
            //$("#divPersoneria").show();
            $("#spIdentificacionObligatoria").show();
            $("#ddlFactRapidaTipoDoc,#txtFactRapidaNroDocumento").addClass("required");
        }
        else {
            $("#ddlTipoDoc").val("");
            $("#ddlTipoDoc").trigger("change");
            //$("#divPersoneria").show();
            //$("#spIdentificacionObligatoria").show();
            $("#ddlFactRapidaTipoDoc,#txtFactRapidaNroDocumento").addClass("required");
        }
        $("#ddlFactRapidaTipo").html("");
        Common.obtenerComprobantesVentaPorCondicion("ddlFactRapidaTipo", $("#ddlFactRapidaCondicionIva").val(), "");
    },
    configForm: function () {
        obtenerCondicionesDeVenta("ddlFactRapidaCondicionVenta", "");

        $("#frmFacturaRapida").validate({
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorelement: 'span',
            errorclass: 'help-block',
            errorplacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#txtFactRapidaNroDocumento").numericInput();

        if (MI_CONDICION == "MO") {
            $("#ddlFactRapidaIva").attr("disabled", true);
        }
        else if (MI_CONDICION == "RI") {
            $("#ddlFactRapidaIva").val("21,00").trigger("change");
        }
        else
            $("#ddlFactRapidaIva").val("0,00").trigger("change");

        if ($("#hdnPrecioConIva").val() == "1")
            $("#lblFactRapidaImporte").html("Importe c/IVA");
        else
            $("#lblFactRapidaImporte").html("Importe s/IVA");

        //$('#txtFactRapidaFecha').datepicker();
        $.validator.addMethod("validFechaActual", function (value, element) {

            var fInicio = $("#txtFactRapidaFecha").val().split("/");
            var fechainicio = new Date(fInicio[2], (fInicio[1] - 1), fInicio[0]);

            var today = new Date();
            var fechaFin = new Date(today);
            fechaFin.setDate(today.getDate() + 5);

            if (fechainicio > fechaFin) {
                return false;
            }
            else {
                return true;
            }
        }, "Solo puede facturar hasta 5 dias despues de la fecha actual");

        $.validator.addMethod("validCuit", function (value, element) {
            var check = true;
            if ($("#ddlFactRapidaTipoDoc").val() == "CUIT") {
                return CuitEsValido($("#txtFactRapidaNroDocumento").val());
            }
            else
                return check;

        }, "CUIT Inválido");

        $("#txtFactRapidaImporte").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            allowNegative: true,
            precision: 2
        });
    },
    showModal: function () {
        facturaRapida.limpiarCampos();
        $("#modalFacturacionRapida").modal("show");
    },
    changeImporte: function () {
        if ($("#txtFactRapidaImporte").val() != "") {

            var monto = Common.calcularPrecioFinal(parseFloat($("#ddlFactRapidaIva").val()), parseFloat($("#txtFactRapidaImporte").val()), $("#hdnPrecioConIva").val());
            $("#divFactRapidaTotal").html("$" + monto);

            /*var iva = $("#ddlFactRapidaIva").val();
            if (iva == "0,00")
                $("#divFactRapidaTotal").html("$" + (parseFloat($("#txtFactRapidaImporte").val())).toFixed(2));
            else if (iva == "2,50")
                $("#divFactRapidaTotal").html("$" + (parseFloat($("#txtFactRapidaImporte").val()) * 1.025).toFixed(2));
            else if (iva == "5,00")
                $("#divFactRapidaTotal").html("$" + (parseFloat($("#txtFactRapidaImporte").val()) * 1.05).toFixed(2));
            else if (iva == "10,50")
                $("#divFactRapidaTotal").html("$" + (parseFloat($("#txtFactRapidaImporte").val()) * 1.105).toFixed(2));
            else if (iva == "21,00")
                $("#divFactRapidaTotal").html("$" + (parseFloat($("#txtFactRapidaImporte").val()) * 1.21).toFixed(2));
            else if (iva == "27,00")
                $("#divFactRapidaTotal").html("$" + (parseFloat($("#txtFactRapidaImporte").val()) * 1.27).toFixed(2));*/
        }
    },
    limpiarCampos: function () {
        $("#divError").hide();
        $("#lnkDownloadPdf").hide();
        $("#lnkGenerarCAE").show();

        $("#txtFactRapidaFecha").datepicker().datepicker("setDate", new Date());

        $("#ddlFactRapidaCondicionIva").val("CF");
        $("#txtFactRapidaRazonSocial").val("CONSUMIDOR FINAL");
        facturaRapida.changeCondicionIva();
        //$("#ddlFactRapidaCondicionVenta").val("Efectivo");
        $("#ddlFactRapidaCondicionVenta option:contains('Efectivo')").attr('selected', true);
        $("#txtFactRapidaNroDocumento").val("");
        $("#txtFactRapidaImporte").val("");

        if (MI_CONDICION == "MO") {
            $("#ddlFactRapidaIva").attr("disabled", true);
        }
        else if (MI_CONDICION == "RI") {
            $("#ddlFactRapidaIva").val("21,00").trigger("change");
        }
        else
            $("#ddlFactRapidaIva").val("0,00").trigger("change");

        $("#ddlFactRapidaTipo").html("");
        $("#divFactRapidaTotal").html("$ 0,00");
        Common.obtenerComprobantesVentaPorCondicion("ddlFactRapidaTipo", $("#ddlFactRapidaCondicionIva").val(), "");
    },
    generarCae: function () {
        if ($('#frmFacturaRapida').valid()) {
            if ($("#txtFactRapidaFecha").val() != "" && $("#txtFactRapidaNroDocumento").val() != "" && $("#txtFactRapidaImporte").val() != "" && $("#txtFactRapidaImporte").val() != "0.00") {

                Common.mostrarProcesando("lnkGenerarCAE");
                $("#divErrorFactRapida").hide();

                var importe = Common.calcularPrecioFinal(parseFloat($("#ddlFactRapidaIva").val()), parseFloat($("#txtFactRapidaImporte").val()), "0");

                var info = "{ fecha: '" + $("#txtFactRapidaFecha").val()
                    + "', razonSocial: '" + $("#txtFactRapidaRazonSocial").val()
                    + "', condicionIva: '" + $("#ddlFactRapidaCondicionIva").val()
                    + "', tipoDoc: '" + $("#ddlFactRapidaTipoDoc").val()
                    + "', nroDoc: '" + $("#txtFactRapidaNroDocumento").val()
                    + "', tipoComprobante: '" + $("#ddlFactRapidaTipo").val()
                    + "', condicionVenta: '" + $("#ddlFactRapidaCondicionVenta").val()
                    + "', importe: '" + importe
                    + "', iva: '" + $("#ddlFactRapidaIva").val()
                    + "'}";

                $.ajax({
                    type: "POST",
                    url: "comprobantese.aspx/generarCaeRapido",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {

                        //Seteo el link de download
                        $("#lnkDownloadPdf").show();
                        $("#lnkGenerarCAE").hide();
                        var fileName = data.d;
                        $("#lnkDownloadPdf").attr("href", "/pdfGenerator.ashx?file=" + fileName.Comprobante);

                        Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
                        //$("#lnkEditarExpress").hide();
                        //integraciones.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#msgErrorDetalle").html(r.Message);

                        //$("#modalFacturacionRapida").modal("toggle");
                        Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
                        //integraciones.filtrar();
                        $("#divErrorDetalle").show();
                    }
                });
            }
            else {
                $("#divErrorFactRapida").show();
                $("#msgErrorFactRapida").html("Debe completar todos los campos");
            }
        }
        else {
            return false;
        }
    },
    grabar: function () {
        if ($('#frmFacturaRapida').valid()) {
            if ($("#txtFactRapidaFecha").val() != "" && $("#txtFactRapidaNroDocumento").val() != "" && $("#txtFactRapidaImporte").val() != "" && $("#txtFactRapidaImporte").val() != "0.00") {

                Common.mostrarProcesando("lnkFactRapidaAceptar");
                $("#divErrorFactRapida").hide();

                var importe = Common.calcularPrecioFinal(parseFloat($("#ddlFactRapidaIva").val()), parseFloat($("#txtFactRapidaImporte").val()), "0");

                var info = "{ fecha: '" + $("#txtFactRapidaFecha").val()
                    + "', razonSocial: '" + $("#txtFactRapidaRazonSocial").val()
                    + "', condicionIva: '" + $("#ddlFactRapidaCondicionIva").val()
                    + "', tipoDoc: '" + $("#ddlFactRapidaTipoDoc").val()
                    + "', nroDoc: '" + $("#txtFactRapidaNroDocumento").val()
                    + "', tipoComprobante: '" + $("#ddlFactRapidaTipo").val()
                    + "', condicionVenta: " + $("#ddlFactRapidaCondicionVenta").val()
                    + ", importe: '" + importe
                    + "', iva: '" + $("#ddlFactRapidaIva").val()
                    + "'}";

                $.ajax({
                    type: "POST",
                    url: "comprobantese.aspx/guardarRapido",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {

                        //Seteo el link de download
                        // $("#lnkDownloadPdf").show();
                        //$("#lnkGenerarCAE").hide();
                        //var fileName = data.d;
                        //$("#lnkDownloadPdf").attr("href", "/pdfGenerator.ashx?file=" + fileName.Comprobante);

                        Common.ocultarProcesando("lnkFactRapidaAceptar", "Guardar");
                        $("#modalFacturacionRapida").modal("hide");
                        //$("#lnkEditarExpress").hide();
                        //integraciones.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#msgErrorDetalle").html(r.Message);

                        //$("#modalFacturacionRapida").modal("toggle");
                        Common.ocultarProcesando("lnkFactRapidaAceptar", "Guardar");
                        //integraciones.filtrar();
                        $("#divErrorDetalle").show();
                    }
                });
            }
            else {
                $("#divErrorFactRapida").show();
                $("#msgErrorFactRapida").html("Debe completar todos los campos");
            }
        }
        else {
            return false;
        }
    }
}

var impresionMasiva = {
    configForm: function () {
        $("#frmImpresionMasiva").validate({
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorelement: 'span',
            errorclass: 'help-block',
            errorplacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        var controlName = "ddlTiposComprobanteImpresionMasiva";
        $("#" + controlName).append('<option value="">Todos los tipos</option>');
        if (MI_CONDICION == "RI") {
            $("#" + controlName).append('<option value="FCB">Factura B</option>');
            $("#" + controlName).append('<option value="NDB">Nota débito B</option>');
            $("#" + controlName).append('<option value="NCB">Nota crédito B</option>');
            $("#" + controlName).append('<option value="FCA">Factura A</option>');
            $("#" + controlName).append('<option value="NDA">Nota débito A</option>');
            $("#" + controlName).append('<option value="NCA">Nota crédito A</option>');

        }
        else if (MI_CONDICION == "MO" || MI_CONDICION == "EX") {
            $("#" + controlName).append('<option value="FCC">Factura C</option>');
            $("#" + controlName).append('<option value="NDC">Nota débito C</option>');
            $("#" + controlName).append('<option value="NCC">Nota crédito C</option>');
        }
    },
    showModal: function () {
        $("#lnkImpresionMasivaDownloadPdf").hide();
        Common.ocultarProcesando("lnkImpresionMasivaGenerar", "Generar");
        $("#modalImpresionMasiva").modal("show");
    },
    generar: function () {
        if ($('#frmImpresionMasiva').valid()) {
            if ($("#txtFechaDesdeImpresionMasiva").val() != "" && $("#txtFechaHastaImpresionMasiva").val() != "") {
                $("#lnkImpresionMasivaDownloadPdf").hide();
                Common.mostrarProcesando("lnkImpresionMasivaGenerar");
                $("#divErrorImpresionMasiva").hide();


                var info = "{ fechaDesde: '" + $("#txtFechaDesdeImpresionMasiva").val()
                    + "', fechaHasta: '" + $("#txtFechaHastaImpresionMasiva").val()
                    + "', tipoComprobante: '" + $("#ddlTiposComprobanteImpresionMasiva").val()
                    + "', puntos: '" + $("#txtPuntosImpresionMasiva").val()
                    + "', hojas: '" + $("#ddlHojasImpresionMasiva").val()
                    + "'}";

                $.ajax({
                    type: "POST",
                    url: "comprobantes.aspx/imprimirMasivamente",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {

                        //Seteo el link de download
                        $("#lnkImpresionMasivaDownloadPdf").show();
                        var fileName = data.d;
                        $("#lnkImpresionMasivaDownloadPdf").attr("href", fileName);

                        Common.ocultarProcesando("lnkImpresionMasivaGenerar", "Generar");
                        //$("#modalImpresionMasiva").modal("hide");
                        //$("#lnkEditarExpress").hide();
                        //integraciones.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#msgErrorImpresionMasiva").html(r.Message);

                        //$("#modalFacturacionRapida").modal("toggle");
                        Common.ocultarProcesando("lnkImpresionMasivaGenerar", "Generar");
                        //integraciones.filtrar();
                        $("#divErrorImpresionMasiva").show();
                    }
                });
            }
            else {
                $("#divErrorImpresionMasiva").show();
                $("#msgErrorImpresionMasiva").html("Debe completar todos los campos");
            }
        }
        else {
            return false;
        }
    }
}
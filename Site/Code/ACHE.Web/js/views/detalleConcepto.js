﻿var esModificacion = false;
var cantidadDecimales = 2;
var valorDecimalDefault = "1.00";

var ConceptosCombo = {
    configForm: function () {
        $("#txtConcepto").attr("disabled", true);
        obtenerCantidadDecimales(function (serverData) {
            cantidadDecimales = serverData.d;
            valorDecimalDefault = obtenerValorDecimalDefault(serverData.d, 0);
            $("#txtCantidad").maskMoney({
                thousands: '',
                decimal: '.',
                allowZero: true,
                precision: serverData.d
            });
        });

        Common.obtenerConceptosCodigoyNombreSinCombos("ddlProductos", 3, true);
        obtenerItems();
        $("#txtConcepto,#txtCantidad").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                $("#txtCantidad").focus();
                agregarItem();
                return false;
            }
        });
    }
}

function changeConcepto() {

    if ($("#ddlProductos").val() != "" && $("#ddlProductos").val() != null) {

        $.ajax({
            type: "POST",
            url: "conceptose.aspx/obtenerDatosParaCompras",
            data: "{id: " + parseInt($("#ddlProductos").val()) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != null) {
                    $("#txtConcepto").val(data.d.Nombre);
                    $("#hdnCodigo").val(data.d.Codigo);
                    $("#txtCantidad").focus();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorDetalle").html(r.Message);
                $("#divErrorDetalle").show();
            }
        });
    }
    else {

        $("#hdnCodigo").val("");
    }
}

function ocultarMensajes() {
    $("#divError, #divOk").hide();
}

/*** ITEMS ***/

function obtenerItems() {

    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerConceptosCombo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
                actualizarTotalCombo();
            }
        }
    });
}

function actualizarTotalCombo() {
    if ($("#ddlTipo").val() == "C" && $("#ddlPrecioAutomatico").val() == "Si") {

        $.ajax({
            type: "POST",
            url: "/common.aspx/actualizarTotalCombo",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#txtCostoInterno").val(data.d);
                    calcularPrecioConRentabilidad();
                }
            }
        });
    }
}

function agregarItem() {

    ocultarMensajes();
    esModificacion = false;


    if ($("#txtCantidad").val() != "" && $("#txtConcepto").val() != "") {

        if (parseFloat($("#txtCantidad").val()) == 0) {
            $("#msgErrorDetalle").html("La cantidad debe ser mayor a 0.");
            $("#divErrorDetalle").show();
        } else {

            var info = "{ id: " + parseInt($("#hdnIDItem").val())
                + ", idConcepto: '" + $("#ddlProductos").val()
                + "', concepto: '" + $("#txtConcepto").val()
                + "', cantidad: '" + $("#txtCantidad").val()
                + "' , codigo:'" + $("#hdnCodigo").val()
                + "'}";

            $.ajax({
                type: "POST",
                url: "/common.aspx/agregarItemCombo",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#divErrorDetalle").hide();
                    $("#txtConcepto,#txtCantidad").val("");

                    $("#ddlProductos").val("").trigger("change");
                    $("#hdnIDItem").val("0");
                    $("#hdnCodigo").val("");
                    $("#btnAgregarItem").html("Agregar");
                    $("#txtCantidad").focus();
                    $("#txtCantidad").val(valorDecimalDefault);
                    obtenerItems();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorDetalle").html(r.Message);
                    $("#divErrorDetalle").show();
                }
            });
        }

    } else {
        $("#msgErrorDetalle").html("Debes ingresar la cantidad y concepto.");
        $("#divErrorDetalle").show();
    }
}

function verificarPlanDeCuentas() {
    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        $(".divPlanDeCuentas").show();
    } else {
        $(".divPlanDeCuentas").hide();
    }
}

function cancelarItem() {
    obtenerCantidadDecimales(function (serverData) {
        cantidadDecimales = serverData.d;
        valorDecimalDefault = obtenerValorDecimalDefault(serverData.d, 0);
        $("#divErrorDetalle").hide();
        $("#txtConcepto, #txtCantidad, #txtPrecioDet, #txtBonificacion").val("");
        $("#txtCantidad").val(valorDecimalDefault);
        $("#hdnIDItem").val("0");
        $("#ddlProductos").val("").trigger("change");
        $("#btnAgregarItem").html("Agregar");

        $("#txtCantidad").val(obtenerValorDecimalDefault(serverData.d, 1));
    });
}

function eliminarItem(id) {

    var info = "{ id: " + parseInt(id) + "}";

    $.ajax({
        type: "POST",
        url: "/common.aspx/eliminarItem",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            obtenerItems();
            //   obtenerTotales();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgErrorDetalle").html(r.Message);
            $("#divErrorDetalle").show();
        }
    });
}

function modificarItem(id, idConcepto, cantidad, codigo, concepto, precio, iva, bonif, idPlanDeCuenta) {
    $("#divErrorDetalle").hide();
    $("#txtCantidad").val(cantidad);
    $("#txtConcepto").val(concepto);
    $("#txtPrecioDet").val(precio);
    $("#ddlIvaDet").val(iva);
    $("#txtBonificacion").val(bonif);

    $("#ddlIvaDet").trigger("change");

    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        $("#ddlPlanDeCuentas2").val(idPlanDeCuenta);
        $("#ddlPlanDeCuentas2").trigger("change");
    }

    $("#hdnCodigo").val(codigo);
    esModificacion = true;
    if (idConcepto != "") {
        $("#ddlProductos").val(idConcepto).trigger("change");
        $("#txtConcepto").attr("disabled", true);
    }

    $("#hdnIDItem").val(id);
    $("#btnAgregarItem").html("Actualizar");
}


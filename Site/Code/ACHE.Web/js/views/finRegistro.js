﻿var finRegistro = {
    grabar: function () {
        finRegistro.guardar();
        /*bootbox.dialog({
            message: "Usted va a empezar el per&iacute;odo de prueba de <b>15 días gratis</b> con acceso a todas las funcionalidades para que pueda evaluar las mismas. Una vez finalizado dicho per&iacute;odo podrá elegir continuar con el plan gratuito o con alguno de los pagos. <br><br>Para conocer las diferencias de los planes ingrese a <a href='http://www.contabilium.com/planes/' target='_blank'>http://www.contabilium.com/planes/</a>.<br><br> Ante cualquier duda, comun&iacute;quese sin cargo al <b>0800-22-01234</b>.",
            buttons: {
                success: {
                    label: "Aceptar",
                    className: "btn-success",
                    callback: function () {
                        finRegistro.guardar();
                    }
                },
                danger: {
                    label: "Cancelar",
                    className: "btn-default",
                    callback: function () {

                    }
                }
            }
        });*/
    },
    guardar: function () {
        if ($('#frmEdicion').valid()) {

            var esContador = ($("input[name='ctl00$MainContent$rEsContador']:checked").attr('value') == "1") ? true : false;
            var usaPlanCorporativo = ($("input[name='ctl00$MainContent$rUsaContabilidad']:checked").attr('value') == "1") ? true : false;
            Common.mostrarProcesando("btnActualizar");
            var info = "{ cuit: '" + $("#txtNroDocumento").val()
                    + "', condicionIva: '" + $("#ddlCondicionIva").val()
                    + "', personeria: '" + $("#ddlPersoneria").val()
                    + "', idProvincia: '" + $("#ddlProvincia").val()
                    + "', idCiudad: '" + $("#ddlCiudad").val()
                    + "', domicilio: '" + $("#txtDomicilio").val()
                    + "', pisoDepto: '" + $("#txtPisoDepto").val()
                    + "', cp: '" + $("#txtCp").val()
                    + "', esContador: " + esContador
                    + " , usaPlanCorporativo: " + usaPlanCorporativo
                    + " }";

            $.ajax({
                type: "POST",
                url: "/finRegistro.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    if ($("#hdnCodigoPromo").val() == "REALTRENDS")
                        window.location.href = "modulos/ventas/integraciones.aspx?tipoIntegracion=MercadoLibre";
                    else
                        window.location.href = "home.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Finalizar");
                }
            });
        }
        else {
            return false;
        }
    },
    configForm: function () {
        $("#txtNroDocumento,#txtArea,#txtTelefono").numericInput();

        $(".select2").select2({ width: '100%', allowClear: true });
        // Validation with select boxes
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.validator.addMethod("validCuit", function (value, element) {
            var check = true;
            return CuitEsValido($("#txtNroDocumento").val());
        }, "CUIT/CUIL Inválido");

        Common.obtenerProvincias("ddlProvincia", "", 10, true);
        //Common.configTelefono("txtTelefono");
    },
    changeProvincia: function () {
        Common.obtenerCiudades("ddlCiudad", $("#ddlCiudad").val(), $("#ddlProvincia").val(), true);
    },
    changeCondicionIVA: function () {
        //if ($("#ddlCondicionIva").val() != "RI") {
        //    $('#rUsaContabilidadNO').attr('checked', true);
        //}

        //LH: No se usa. Se mantiene funcion para que las landings no den error,
    }
}
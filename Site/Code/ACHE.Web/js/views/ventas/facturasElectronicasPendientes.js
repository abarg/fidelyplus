﻿var importaciones = {
    /*** SEARCH ***/
    configFilters: function () {
        $("#lnkFacturacionMasiva").hide();
        $("#lnkDownloadPdf").hide();

        $(".select2").select2({ width: '100%', allowClear: true });

        $("#txtFechaVencimiento").datepicker();

        Common.configDatePicker();

        //$("#txtFactRapidaNroDocumento").numericInput();

        /*   $("#txtFechaDesde, #txtFechaHasta, #txtApodo").keypress(function (event) {
               var keycode = (event.keyCode ? event.keyCode : event.which);
               if (keycode == '13') {
                   resetearPagina();
                   integraciones.filtrar();
                   return false;
               }
           });*/

        $("#chkFacturacionMasiva").click(function () {
            $('.chkVenta').not(this).prop('checked', this.checked);
            facturacionMasiva.actualizarTotales();
        });

        importaciones.filtrar();
    },
    filtrar: function () {
        var PAGE_SIZE = 100;

        $("#lnkFacturacionMasiva").hide();
        $("#divError").hide();
        Common.mostrarProcesando("btn_buscar");

        $("#resultsContainer").html("");
        var currentPage = parseInt($("#hdnPage").val());

        var info = "{page: " + currentPage + ", pageSize: " + PAGE_SIZE + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/ventas/facturasElectronicasPendientes.aspx/getResults",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#hdnCantItemsPag").val(data.d.Items.length);
                if (data.d.TotalPage > 0) {
                    $("#divPagination").show();
                    $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                    if (data.d.TotalPage == 1)
                        $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                    else if (currentPage == data.d.TotalPage)
                        $("#lnkNextPage").attr("disabled", "disabled");
                    else if (currentPage == 1)
                        $("#lnkPrevPage").attr("disabled", "disabled");

                    var aux = (currentPage * PAGE_SIZE);
                    if (aux > data.d.TotalItems)
                        aux = data.d.TotalItems;

                    $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                }
                else {
                    if (data.d.TotalPage == -1)
                        $("#msjResultados").html("Mostrando " + data.d.TotalItems);
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }
                }

                // Render using the template
                if (data.d.Items.length > 0)
                    $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                else
                    $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");

                Common.ocultarProcesando("btn_buscar", "Buscar");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").show();
                $("#msgError").html(r.Message);

                Common.ocultarProcesando("btn_buscar", "Buscar");
            }
        });
    },
    editarDatosFacturaExpress: function () {
        $("#divErrorFactRapida").hide();
        $("#divLectura").hide();
        $("#infoContainer").hide();
        $("#divEscritura").show();
        $("#editContainer").show();
        $("#divItems").hide();
        $("#divFooter").hide();
        $("#txtFactRapidaNroDocumento").numericInput();
    },
    guardarDatosFacturaExpress: function () {
        if ($('#frmEditFacturaExpress').valid()) {

            if ($("#ddlFactRapidaTipoDoc").val() == "CUIT" && CuitEsValido($("#txtFactRapidaNroDocumento").val()) == false) {
                $("#divErrorFactRapida").show();
                $("#msgErrorFactRapida").html("El CUIT es inválido");
            }
            else if ($("#txtFactRapidaEmail").val() != "" && !validateEmail($("#txtFactRapidaEmail").val())) {
                $("#divErrorFactRapida").show();
                $("#msgErrorFactRapida").html("El Email es inválido");
            }
            else if ($("#txtFactRapidaRazonSocial").val().replace(" ", "") != "" && $("#txtFactRapidaNroDocumento").val() != "") {
                $("#divErrorFactRapida").hide();

                var info = "{  razonSocial: '" + $("#txtFactRapidaRazonSocial").val()
                    + "', condicionIva: '" + $("#ddlFactRapidaCondicionIva").val()
                    + "', tipoDoc: '" + $("#ddlFactRapidaTipoDoc").val()
                    + "', nroDoc: '" + $("#txtFactRapidaNroDocumento").val()
                    + "', email: '" + $("#txtFactRapidaEmail").val()
                    + "'}";
                $.ajax({
                    type: "POST",
                    url: "/modulos/ventas/facturasElectronicasPendientes.aspx/guardarDatosPersonaFacturacionExpress",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        if (data.d) {
                            $("#infoContainer").empty();
                            $("#infoTemplate").tmpl({ comprobante: data.d }).appendTo("#infoContainer");
                            $("#editContainer").empty();
                            $("#editTemplate").tmpl({ comprobante: data.d }).appendTo("#editContainer");
                        }
                        $("#divLectura").show();
                        $("#infoContainer").show();
                        $("#divEscritura").hide();
                        $("#editContainer").hide();
                        $("#divItems").show();
                        $("#divFooter").show();

                        if ($("#txtFactRapidaNroDocumento").val() == "")
                            $("#divErrorDNI").show();
                        else
                            $("#divErrorDNI").hide();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        alert(r.Message);
                    }
                });
            } else {
                $("#divErrorFactRapida").show();
                $("#msgErrorFactRapida").html("Debe completar todos los campos");
            }
        }
    },
    cancelarDatosFacturaExpress: function () {
        $("#divErrorFactRapida").hide();
        $("#divLectura").show();
        $("#infoContainer").show();
        $("#divEscritura").hide();
        $("#editContainer").hide();
        $("#divItems").show();
        $("#divFooter").show();
    },
    facturarExpress: function (id, idIntegracion) {
        $("#divItems").show();
        $("#divFooter").show();
        $("#divErrorFactRapida").hide();
        $("#divLectura").show();
        $("#infoContainer").show();
        $("#divEscritura").hide();
        $("#editContainer").hide();

        $("#hdnIDVenta").val(id);
        $("#hdnIDInt").val(idIntegracion);

        $("#lnkDownloadPdf").hide();
        $("#lnkGenerarCAE").show();
        $("#lnkGuardarBorradorExpress").show();
        $("#lnkEditarExpress").show();
        var info = "{ id: " + id + ", idIntegracion: " + idIntegracion + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/ventas/facturasElectronicasPendientes.aspx/facturarVentaExpress",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d) {
                    if (data.d.Items.length > 0) {
                        $("#resultsContainerItems").empty();
                        $("#itemsTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainerItems");
                    }
                    $("#infoContainer").empty();
                    $("#infoTemplate").tmpl({ comprobante: data.d }).appendTo("#infoContainer");
                    $("#editContainer").empty();
                    $("#editTemplate").tmpl({ comprobante: data.d }).appendTo("#editContainer");

                    //$("#infoTotalContainer").empty();
                    //$("#totalTemplate").tmpl({ comprobante: data.d }).appendTo("#infoTotalContainer");

                    $("#divSubTotal").html(data.d.SubTotalComprobante.toFixed(2));
                    $("#divTotalIva").html(data.d.IvaComprobante.toFixed(2));
                    $("#divTotal").html(data.d.TotalComprobante.toFixed(2));

                    $("#modalFacturacionRapida").modal("toggle");
                    $("#hdnIDUsuario").val(data.d.IDUsuario);

                    if (data.d.NombrePersona.indexOf(' - ') == 0)
                        $("#divErrorDNI").show();
                    else
                        $("#divErrorDNI").hide();

                    if (MI_CONDICION == "MO" || MI_CONDICION == "EX")
                        $(".iva").hide();
                }
                else {
                    $("#msgError").html("No se pudo generar la factura");
                    $("#modalFacturacionRapida").modal("toggle");
                    $("#divError").show();
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

    },
    facturar: function (id) {

        var info = "{ id: " + id + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/ventas/facturasElectronicasPendientes.aspx/facturarVenta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d) {
                    ///   window.location.href = "/comprobantese.aspx?IdIntegracion=" + idIntegracion + "&IdVentaIntegracion=" + id; PREGUNTAR
                }
                else {
                    $("#msgError").html("No se pudo generar la factura");
                    $("#divError").show();
                }

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
    },
    facturarBorrador: function (id) {

        var info = "{ id: " + id + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/ventas/facturasElectronicasPendientes.aspx/facturarVentaBorrador",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d) {
                    $("#modalFacturacionRapida").modal("toggle");
                    importaciones.filtrar();

                }
                else {
                    $("#msgError").html("No se pudo generar el borrador de la factura");
                    $("#divError").show();
                }

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
    },
    editar: function () {
        if ($("#txtFactRapidaRazonSocial").val().replace(" ", "") == "") {
            $("#divErrorFactRapida").show();
            $("#msgErrorFactRapida").html("Debe ingresar su razón social.");
        }
        else
            importaciones.facturar($("#hdnIDVenta").val());
    },
    guardarBorrador: function () {
        if ($("#txtFactRapidaRazonSocial").val().replace(" ", "") == "") {
            $("#divErrorFactRapida").show();
            $("#msgErrorFactRapida").html("Debe ingresar su razón social.");
        }
        else
            importaciones.facturarBorrador($("#hdnIDVenta").val());
    },
    resetearConexion: function () {
        var info = "{" + "idCuenta: " + $("#ddlCuenta").val() + "}";
        $.ajax({
            type: "POST",
            data: info,
            url: "facturasElectronicasPendientes.aspx/resetearConexion",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                // var tipoIntegracion = $("#lblTipoIntegracion").html();
                // window.location.href = "/modulos/ventas/integraciones.aspx?TipoIntegracion=" + tipoIntegracion;
            },
            error: function (response) {
                $("#msgError").html("No se pudo resetear la conexión.");
                $("#divError").show();

            }
        });
    },
    generarCae: function () {
        if ($("#txtFactRapidaRazonSocial").val().replace(" ", "") == "") {
            $("#divErrorFactRapida").show();
            $("#msgErrorFactRapida").html("Debe ingresar su razón social.");
        }
        else if ($("#txtFactRapidaNroDocumento").val() == "" && $("#ddlFactRapidaCondicionIva").val() == "CF" && parseFloat(divTotal.innerText) >= 1000) {
            $("#divErrorFactRapida").show();
            $("#msgErrorFactRapida").html("Para facturas con importe >= a $1.000, el DNI es obligatorio.");
        }
        else if ($("#ddlFactRapidaTipoDoc").val() == "CUIT" && ($("#txtFactRapidaNroDocumento").val() == "" || CuitEsValido($("#txtFactRapidaNroDocumento").val()) == false)) {
            $("#divErrorFactRapida").show();
            $("#msgErrorFactRapida").html("El CUIT es inválido");
        }
        else {
            Common.mostrarProcesando("lnkGenerarCAE");

            var info = "{ id: " + $("#hdnIDVenta").val() + ", idIntegracion: " + $("#hdnIDInt").val() + "}";

            $.ajax({
                type: "POST",
                data: info,
                url: "facturasElectronicasPendientes.aspx/generarCae",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {

                    //Seteo el link de download
                    $("#lnkDownloadPdf").show();
                    $("#lnkGenerarCAE").hide();
                    $("#lnkGuardarBorradorExpress").hide();
                    var fileName = data.d;
                    $("#lnkDownloadPdf").attr("href", "/pdfGenerator.ashx?file=" + fileName.Comprobante);

                    Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
                    $("#lnkEditarExpress").hide();
                    importaciones.filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);

                    $("#modalFacturacionRapida").modal("toggle");
                    Common.ocultarProcesando("lnkGenerarCAE", "Emitir factura eléctronica");
                    importaciones.filtrar();
                    $("#divError").show();

                }
            });
        }
    },
    abrirModalConfig: function () {
        $("#msgErrorConfiguracion").html("");
        $("#divErrorConfiguracion").hide();
        importaciones.CargarConfiguraciones();
        $("#modalConfiguracion").modal("toggle");

    },
    guardarConfig: function () {
        var info = "{ facturasElectronicasPendientes: " + $("#chkCostoEnvio").is(':checked')
            + "}"
        $.ajax({
            type: "POST",
            data: info,
            url: "facturasElectronicasPendientes.aspx/guardarConfiguracion",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                importaciones.filtrar();
                $("#modalConfiguracion").modal("hide");

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorConfiguracion").html(r.Message);

                $("#divErrorConfiguracion").show();

            }
        });
    },
    CargarConfiguraciones: function () {
        var info = "{ idIntegracion: " + $("#ddlCuenta").val()
          + "}"
        $.ajax({
            type: "POST",
            data: info,
            url: "facturasElectronicasPendientes.aspx/getConfiguraciones",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d.configuraciones[0].Valor == "0") //INCLUIR COSTO DE ENVIO
                    $("#chkCostoEnvio").prop('checked', false);


            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorConfiguracion").html(r.Message);

                $("#divErrorConfiguracion").show();

            }
        });
    }
}
var facturaRapida = {
    armarSelectSegunCondicionIVA: function () {

        var valor = $("#ddlFactRapidaTipoDoc").val();

        if ($("#ddlFactRapidaCondicionIva").val() == "CF") {
            $("#ddlFactRapidaTipoDoc").html("");
            //$("<option/>").attr("value", "").text("").appendTo($("#ddlFactRapidaTipoDoc"));
            $("<option/>").attr("value", "DNI").text("DNI").appendTo($("#ddlFactRapidaTipoDoc"));
            $("#ddlFactRapidaTipoDoc").val("DNI");
        }
        else {
            $("#ddlFactRapidaTipoDoc").html("");
            //$("<option/>").attr("value", "").text("").appendTo($("#ddlFactRapidaTipoDoc"));
            $("<option/>").attr("value", "CUIT").text("CUIT/CUIL").appendTo($("#ddlFactRapidaTipoDoc"));
            //$("<option/>").attr("value", "DNI").text("DNI").appendTo($("#ddlFactRapidaTipoDoc"));
            $("#ddlFactRapidaTipoDoc").val("CUIT");

        }


    },
    changeCondicionIva: function () {
        facturaRapida.armarSelectSegunCondicionIVA();
        if ($("#ddlFactRapidaCondicionIva").val() == "CF") {
            $("#ddlFactRapidaTipoDoc").val("DNI");
            $("#ddlFactRapidaTipoDoc").trigger("change");
            //$("#divPersoneria").hide();
            //$("#spIdentificacionObligatoria").hide();
            $("#ddlFactRapidaTipoDoc,#txtFactRapidaNroDocumento").removeClass("required");
        }
        else if ($("#ddlFactRapidaCondicionIva").val() == "RI") {
            $("#ddlFactRapidaTipoDoc").val("CUIT");
            $("#ddlFactRapidaTipoDoc").trigger("change");
            //$("#divPersoneria").show();
            $("#spIdentificacionObligatoria").show();
            $("#ddlFactRapidaTipoDoc,#txtFactRapidaNroDocumento").addClass("required");
        }
        else {
            $("#ddlTipoDoc").val("");
            $("#ddlTipoDoc").trigger("change");
            //$("#divPersoneria").show();
            //$("#spIdentificacionObligatoria").show();
            $("#ddlFactRapidaTipoDoc,#txtFactRapidaNroDocumento").addClass("required");
        }
        $("#ddlFactRapidaTipo").html("");
        Common.obtenerComprobantesVentaPorCondicion("ddlFactRapidaTipo", $("#ddlFactRapidaCondicionIva").val(), "");
    }
}

var facturacionMasiva = {

    elegirTipoFacturacionMasiva: function () {
        facturacionMasiva.openModalPreGeneracion();
        $("#divMsjPreFacturacion").html("Usted ha seleccionado " + $("#hdnTotalCant").val() + " items. Por favor haga click en el botón correspondiente para facturar los items seleccionados.");

    },

    facturar: function (modoFE) {
        if ($("#txtFechaVencimiento").closest('.form-group').hasClass("has-error"))
            return false;

        $("#modalPreGeneracionAbonos").modal("toggle");
        $("#lnkFacturacionMasiva").hide();

        facturacionMasiva.openModalGeneracion();
        $("#divWait").html("Por favor, espere unos minutos. Se estan procesando los items...");
        $("#divOk").hide();
        facturacionMasiva.generarCAE(0, modoFE);

    },
    openModalPreGeneracion: function () {
        $("#txtFechaEmision").val("");
        $("#modalPreGeneracionAbonos").modal("toggle");


    },
    openModalGeneracion: function () {
        $("#modalGeneracionAbonos").modal("toggle");
    },
    actualizarTotales: function () {
        if (($(".chkVenta:checked").length) > 0)
            $("#lnkFacturacionMasiva").show();
        else
            $("#lnkFacturacionMasiva").hide();
        var totalCant = 0;
        $(".chkVenta:checked").each(function () {
            if ($(this).attr("id").indexOf("chkVenta") >= 0) {
                totalCant = totalCant + 1;
            }
        });
        $("#hdnTotalCant").val(totalCant);
    },
    generarCAE: function (desde, modoFE) {


            $("#divError,#imgAbonoOK,#imgAbonoError,#msgAbonoOK,#msgAbonoError").hide();
            $("#divCerrar").hide();
            $("#divWait,#imgAbonoCargando").show();
            $("#resultsContainerModal").html("");
            $("#divresultsContainerModal").hide();

            var countItems = $("#hdnCantItemsPag").val();

            var hasta = parseInt(desde) + 5;
            //var totalCant = 0;
            var info = "{idAbonos: '";
            $(".chkVenta:checked").each(function () {
                if ($(this).attr("id").indexOf("chkVenta") >= 0) {
                    info += $(this).attr("id");
                    //totalCant = totalCant + 1;
                }
            });

            info += "',desde:" + desde;
            info += ",hasta:" + hasta;
            info += ",modoFacturacionElectronica:" + modoFE;
            info += ",seleccionarTodos:" + $("#chkFacturacionMasiva").is(':checked');
            info += ",emision:'" + $("#txtFechaEmision").val();
            info += "', vencimiento: '" + $("#txtFechaVencimiento").val();
            info += "'}";

            //$("#hdnTotalCant").val(totalCant);

            $.ajax({
                type: "POST",
                url: "facturasElectronicasPendientes.aspx/facturacionMasiva",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.CantidadGenerados > 0)
                        $("#divWait").html("Por favor, espere unos minutos. Procesando " + data.d.CantidadGenerados + " items de " + $("#hdnTotalCant").val() + "...");
                    $("#divWait").show();

                    var hasta = data.d.Hasta;
                    var desde = 0;
                    var finEjecucion = false;
                    var countCat = countItems;
                    if (countCat > parseInt(hasta) + parseInt(1)) {
                        desde = parseInt(hasta) + parseInt(1);

                    } else {

                        //FIN EJECUCION
                        finEjecucion = true;
                        $("#divWait,#imgAbonoCargando").hide();
                        if (data.d.CantidadErrores > 0) {
                            $("#msgAbonoError,#imgAbonoError").show();
                            $("#spnAbonoOK").html((data.d.CantidadGenerados - data.d.CantidadErrores));
                            $("#spnAbonoError").html(data.d.CantidadErrores);
                            $("#lblAbonoResultadoOK").show();
                            $("#lblAbonoResultadoError").show();
                        }
                        else {
                            $("#msgAbonoOK,#imgAbonoOK").show();
                            $("#spnAbonoOK").html(data.d.CantidadGenerados);
                            $("#lblAbonoResultadoOK").show();
                            $("#lblAbonoResultadoError").hide();
                        }

                        $("#divOk").show();
                        $("#divCerrar").show();
                        $("#btnGenerar").attr("disabled", false);
                        $("#imgLoading2").hide();
                    }


                    if (!finEjecucion) {
                        facturacionMasiva.generarCAE(desde, modoFE);
                    } else {
                        importaciones.filtrar();
                        $("#lnkFacturacionMasiva").hide();
                        if (data.d.CantidadErrores > 0) {
                            $("#resultComprobanteFETemplate").tmpl({ results: data.d.Items.Items }).appendTo("#resultsContainerModal");
                            $("#divresultsContainerModal").show();
                        }
                    }
                },
                error: function (response) {

                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#btnGenerar").attr("disabled", false);
                    $("#imgLoading2").hide();
                    $("#modalGeneracionAbonos").modal("hide");
                }
            });        
    },
    validarFechaFacturacion: function () {
        var valid = true;
        var desde = $("#txtFechaEmision").val();
        var hasta = $("#txtFechaVencimiento").val();
        if (isNaN(hasta) && isNaN(desde)) {
            var fDesde = parseEnDate(desde);
            var fHasta = parseEnDate(hasta);
            if (fDesde > fHasta) {
                valid = false;
            }
        }
        if (!valid) {
            $("#errorFechaVencimiento").show();
            $("#txtFechaVencimiento").closest('.form-group').removeClass('has-success').addClass('has-error');
        }
        else {
            $("#errorFechaVencimiento").hide();
            $("#txtFechaVencimiento").closest('.form-group').removeClass('has-error');
        }

    },
}


function mostrarPagAnterior() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual--;
    $("#hdnPage").val(paginaActual);
    importaciones.filtrar();
}
function mostrarPagProxima() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual++;
    $("#hdnPage").val(paginaActual);
    importaciones.filtrar();

}
function resetearPagina() {
    $("#hdnPage").val("1");
}


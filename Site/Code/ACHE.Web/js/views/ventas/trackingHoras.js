﻿var trackingHoras = {
    /*** FORM ***/
    grabar: function (enviarMail) {
        $("#divError").hide();
        $("#divOk").hide();

        var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        if ($('#frmEdicion').valid()) {
            Common.mostrarProcesando("btnActualizar");

            var info = "{ id: " + parseInt(id)
                    + ", IDPersona: '" + $("#ddlPersona").val()
                    + "', fecha: '" + $("#txtFechaTarea").val()
                    + "', horaDesde: '" + $("#txtDesde").val()
                    + "', horaHasta: '" + $("#txtHasta").val()
                    + "', duracion: '" + $("#txtCantHoras").val()
                    + "', Tarea: '" + $("#ddlTarea").val()
                    + "', estado: '" + $("#ddlEstado").val()
                    + "', abono: '" + $("#ddlAbono").val()
                    + "', Observaciones: '" + $("#txtObservaciones").val()
                    + "', idUsuarioAdicional: '" + $("#ddlUuarios").val()
                    + "', ValorHora: '" + $("#txtValorHora").val().replace('.', ',')
                    + "', enviarMail: " + enviarMail
                    + "}";

            $.ajax({
                type: "POST",
                url: "/modulos/ventas/trackingHorase.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    //if (enviarMail)
                    //    $("#modalEMail").modal("show");
                    //else
                    //    
                    window.location.href = "/modulos/ventas/trackingHoras.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                }
            });
        }
        else {
            return false;
        }
    },
    cancelar: function () {
        window.location.href = "/modulos/ventas/trackingHoras.aspx";
    },
    configForm: function () {

        //$("#txtCantHoras").numericInput();
        //$("#txtCantHoras").maskMoney({ thousands: '', decimal: '.', allowZero: true });
        $("#txtValorHora").maskMoney({ thousands: '', decimal: '.', allowZero: true });

        Common.obtenerPersonas("ddlPersona", $("#hdnIDPersona").val(), true);
       // $(".select2").select2({ width: '100%', allowClear: true });

        $('#txtFechaTarea').datepicker();
        trackingHoras.mostrarValorHora();
        $("#ddlEstado").attr("onchange", "trackingHoras.mostrarValorHora()");

        // Date Picker
        Common.configDatePicker();
        //Common.configFechasDesdeHasta("txtFecha");
        //$(".horas").datetimepicker({ format: 'yyyy-mm-dd hh:ii', language: 'es' });

        $('.horas').timepicker({ minuteStep: 15, showMeridian: false }).on('changeTime.timepicker', function (e) {

            trackingHoras.actualizarDuracion();
        });

        // Validation with select boxes
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    mostrarValorHora: function () {
        if ($('#ddlEstado').val() == 'Facturable') {
            $('#divValorHora').show();
        } else {
            $('#divValorHora').hide();
        }
    },
    actualizarDuracion: function () {
        var fecha = $("txtFechaTarea").val();

        var end_time = $("#txtDesde").val();
        var start_time = $("#txtHasta").val();

        if (end_time != "" && start_time != "") {
            var timeStart = new Date("01/01/2007 " + "08:30:20 AM");
            var timeEnd = new Date("01/02/2007 " + "05:30:30 PM");

            $("#txtCantHoras").val(datediff('01/01/2011 ' + end_time, '01/01/2011 ' + start_time, 'hours').toString());
        }
        else
            $("#txtCantHoras").val("0");
    },
    enviarMail: function () {
        Common.mostrarProcesando("btnEnviar");
        var info = "{ para:'" + $("#txtEnvioPara").val() + "', asunto: '" + $("#txtEnvioAsunto").val() + "', id: " + $("#hdnID").val() + "}";
        $.ajax({
            type: "POST",
            url: "trackingHorase.aspx/enviarMail",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //Toolbar.cancelarEnvio();
                $("#divOkMail").show();

                setTimeout(function () { $("#divOkMail").fadeOut(1500); }, 3000);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                Common.ocultarProcesando("btnEnviar", "Enviar");
                $("#msgErrorMail").html(r.Message);
                $("#divErrorMail").show();
                $("#divOkMail").hide();
                //$('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    ,
    /*** SEARCH ***/
    configFilters: function () {
        //$(".select2").select2({ width: '100%', allowClear: true });

        $("#txtCondicion, #txtFechaHasta, #txtNombre").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                trackingHoras.resetearPagina();
                trackingHoras.filtrar();
                return false;
            }
        });
        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    },
    nuevo: function () {
        window.location.href = "/modulos/ventas/trackingHorase.aspx";
    },
    editar: function (id) {
        window.location.href = "/modulos/ventas/trackingHorase.aspx?ID=" + id;
    },
    eliminar: function (id, nombre) {
        bootbox.confirm("¿Está seguro que desea eliminar la tarea " + nombre + "?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: "/modulos/ventas/trackingHoras.aspx/delete",
                    data: "{ id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        trackingHoras.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        });
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        trackingHoras.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        trackingHoras.filtrar();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    },
    filtrar: function () {
        $("#divError").hide();
        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");
            var currentPage = parseInt($("#hdnPage").val());

            var info = "{ condicion: '" + $("#txtCondicion").val()
                       + "', periodo: '" + $("#ddlPeriodo").val()
                       + "', fechaDesde: '" + $("#txtFechaDesde").val()
                       + "', fechaHasta: '" + $("#txtFechaHasta").val()
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + "}";

            $.ajax({
                type: "POST",
                url: "/modulos/ventas/trackingHoras.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#resultsContainer").empty();

                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.d.Items.length > 0)
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
            trackingHoras.resetearExportacion();
        }
    },
    verTodos: function () {
        $("#txtFechaDesde, #txtFechaHasta").val("");
        $("#ddlPersona").val("").trigger("change");
        trackingHoras.filtrar();
    },
    exportar: function () {
        if ($('#frmSearch').valid()) {

            trackingHoras.resetearExportacion();
            $("#imgLoading").show();
            $("#divIconoDescargar").hide();

            var info = "{ condicion: '" + $("#txtCondicion").val()
                        + "', periodo: '" + $("#ddlPeriodo").val()
                        + "', fechaDesde: '" + $("#txtFechaDesde").val()
                        + "', fechaHasta: '" + $("#txtFechaHasta").val()
                       + "'}";

            $.ajax({
                type: "POST",
                url: "/modulos/ventas/trackingHoras.aspx/export",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d != "") {

                        $("#divError").hide();
                        $("#imgLoading").hide();
                        $("#lnkDownload").show();
                        $("#lnkDownload").attr("href", data.d);
                        $("#lnkDownload").attr("download", data.d);
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    trackingHoras.resetearExportacion();
                }
            });
        }
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    otroPeriodo: function () {
        if ($("#ddlPeriodo").val() == "-1")
            $('#divMasFiltros').toggle(600);
        else {
            if ($("#divMasFiltros").is(":visible"))
                $('#divMasFiltros').toggle(600);

            $("#txtFechaDesde,#txtFechaHasta").val("");
            trackingHoras.filtrar();
        }
    },
}


function datediff(fromDate, toDate, interval) {
    /*
    * DateFormat month/day/year hh:mm:ss
    * ex.
    * datediff('01/01/2011 12:00:00','01/01/2011 13:30:00','seconds');
    */

    var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    fromDate = new Date(fromDate);
    toDate = new Date(toDate);
    var timediff = toDate - fromDate;

    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years": return toDate.getFullYear() - fromDate.getFullYear();
        case "months": return (
          (toDate.getFullYear() * 12 + toDate.getMonth())
          -
          (fromDate.getFullYear() * 12 + fromDate.getMonth())
        );
        case "weeks": return Math.floor(timediff / week);
        case "days": return Math.floor(timediff / day);
        case "hours": return (timediff / hour);
        case "minutes": return Math.floor(timediff / minute);
        case "seconds": return Math.floor(timediff / second);
        default: return undefined;
    }
}
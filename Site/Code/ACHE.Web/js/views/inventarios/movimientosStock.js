﻿var cantItems = 0;

var movimientosStock = {
    /*** SEARCH ***/
    configFilters: function () {
        //$(".select2").select2({ width: '100%', allowClear: true });
        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

        Common.obtenerInventariosConLeyenda("ddlInventarioOrigen", true, false, "Seleccione el depósito origen");
        Common.obtenerInventariosConLeyenda("ddlInventarioDestino", true, false, "Seleccione el depósito destino");

       // $("").attr(("onChange", "movimientosStock.filtrar();"));

        $("#ddlInventarioOrigen,#ddlInventarioDestino").change(function (e) {
            movimientosStock.resetearPagina();
            movimientosStock.filtrar();
        });

        $("#txtConcepto").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                movimientosStock.resetearPagina();
                movimientosStock.filtrar();
                return false;
            }
        });

        // validation with select boxes
        $("#frmSearch").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    nuevo: function () {
        window.location.href = "/modulos/inventarios/movimientosDeStocke.aspx";
    },
    filtrar: function () {

        $("#divError").hide();
        if ($('#frmSearch').valid()) {

            $("#resultsContainer").html("");
            var currentPage = parseInt($("#hdnPage").val());

            var origen = 0;
            var destino = 0;

            if ($("#ddlInventarioOrigen").val() != null)
                origen = $("#ddlInventarioOrigen").val();

            if ($("#ddlInventarioDestino").val() != null)
                destino = $("#ddlInventarioDestino").val();

            var info = "{  idOrigen: " + origen
                + ", idDestino: " + destino
                + ", concepto: '" + $("#txtConcepto").val()
                + "', periodo: '" + $("#ddlPeriodo").val()
                + "', fechaDesde: '" + $("#txtFechaDesde").val()
                + "', fechaHasta: '" + $("#txtFechaHasta").val()
                + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                + "}";

            $.ajax({
                type: "POST",
                url: "/modulos/inventarios/movimientosDeStock.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template

                    if (data.d.Items.length > 0)
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");

                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
            movimientosStock.resetearExportacion();
        }
    },

    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        movimientosStock.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        movimientosStock.filtrar();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    },

    verTodos: function () {
        $("#ddlInventarioOrigen, #ddlInventarioDestino").val("");
        movimientosStock.filtrar();
    },
    exportar: function () {
        movimientosStock.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        if ($("#ddlInventarioOrigen").val() != null)
            origen = $("#ddlInventarioOrigen").val();

        if ($("#ddlInventarioDestino").val() != null)
            destino = $("#ddlInventarioDestino").val();

        var info = "{  idOrigen: " + origen
            + ", idDestino: " + destino
            + ", concepto: '" + $("#txtConcepto").val()
            + "', periodo: '" + $("#ddlPeriodo").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/inventarios/movimientosDeStock.aspx/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {
                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                movimientosStock.resetearExportacion();
            }
        });
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    otroPeriodo: function () {
        if ($("#ddlPeriodo").val() == "-1")
            $('#divMasFiltros').toggle(600);
        else {
            if ($("#divMasFiltros").is(":visible"))
                $('#divMasFiltros').toggle(600);

            $("#txtFechaDesde,#txtFechaHasta").val("");
            movimientosStock.filtrar();
        }
    },


    /*** FORM ***/
    configForm: function () {

        $("#txtCantidad").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            precision: 2
        });

        Common.obtenerInventarios("ddlInventarioOrigen", false, false);
        Common.obtenerInventarios("ddlInventarioDestino", false, false);

        if ($("#ddlInventarioOrigen").val() != null)
            $("#hdnIDInventarioOrigen").val($("#ddlInventarioOrigen").val());

        $("#lblStockActual").html("0");
        $("#lblStockReservado").html("0");

        $(".select4").select2({ width: '100%', allowClear: true, minimumInputLength: 2 });

        $("#txtCantidad").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                //$("#ddlProductos").focus();
                movimientosStock.agregarItem();
                return false;
            }
        });

        movimientosStock.obtenerItems();
    },
    cancelar: function () {
        window.location.href = "/modulos/inventarios/inventarios.aspx";
    },
    cambiarInventario: function () {
        //if ($("#hdnID").val() != null)
        //inventarios.ObtenerStockConceptos();

        if (cantItems > 0) {
            if ($("#hdnIDInventarioOrigen").val() != $("#ddlInventarioOrigen").val()) {
                bootbox.confirm("Al cambiar de inventario se eliminarán las transferencias no realizadas", function (result) {
                    if (result) {
                        movimientosStock.limpiarItems();
                        movimientosStock.obtenerItems();
                        movimientosStock.cargarProductosPorInventario();
                    }
                    else {
                        $("#ddlInventarioOrigen").val($("#hdnIDInventarioOrigen").val());
                        $("#ddlInventarioOrigen > option[value=" + $("#hdnIDInventarioOrigen").val() + "]").attr("selected", "selected");
                    }
                });
            }
        }
        else {
            movimientosStock.cargarProductosPorInventario();
        }
    },
    cantidadItems: function () {
        $.ajax({
            type: "POST",
            url: "/modulos/inventarios/movimientosDeStocke.aspx/cantidadItems",
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, text) {
                if (data.d != null) {
                    cantItems = data.d;
                }
                else
                    cantItems = 0
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    cargarProductosPorInventario: function () {
        if (parseInt($("#ddlInventarioOrigen").val()) > 0) {
            Common.obtenerConceptosCodigoyNombrePorInventario("ddlProductos", parseInt($("#ddlInventarioOrigen").val()), 1, true);
            $("#hdnIDInventarioOrigen").val($("#ddlInventarioOrigen").val());
        }
        else {
            $("#divError").html("No hay un inventario de origen seleccionado para cargar los productos");
            $("#divError").show();
        }
    },
    transferirStock: function () {
        $("#divError").hide();
        $("#divOk").hide();

        //var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        var idOrigen = parseInt($("#ddlInventarioOrigen").val());
        var idDestino = parseInt($("#ddlInventarioDestino").val());

        if (idOrigen > 0 && idDestino > 0) {

            if ($('#frmEdicion').valid()) {
                Common.mostrarProcesando("btnActualizar");
                var info = "{ idInventarioOrigen: " + idOrigen
                        + ", idInventarioDestino: " + idDestino
                        + ", observaciones: '" + $("#txtObservaciones").val()
                        + "'}";

                $.ajax({
                    type: "POST",
                    url: "/modulos/inventarios/movimientosDeStocke.aspx/TransferirStock",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        movimientosStock.limpiarItems();
                        movimientosStock.obtenerItems();

                        $('#divOk').show();
                        $("#divError").hide();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        Common.ocultarProcesando("btnActualizar", "Aceptar");
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#msgError").html(r.Message);
                        $("#divError").show();
                        $("#divOk").hide();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        Common.ocultarProcesando("btnActualizar", "Aceptar");
                    }
                });
            }
            else {
                return false;
            }
        }
        else {
            $("#divError").html("Debe seleccionar inventario de origen y destino para poder realizar la transferencia");
            $("#divError").show()
        }
    },
    agregarItem: function () {
        movimientosStock.ocultarMensajes();
        esModificacion = false;

        if ($("#txtCantidad").val() != "") {

            if (!parseFloat($("#txtCantidad").val()) > 0) {
                $("#msgErrorDetalle").html("La cantidad debe ser mayor a 0.");
                $("#divErrorDetalle").show();
            }
            else if (parseFloat($("#txtCantidad").val()) > (parseFloat($("#lblStockActual").html()) - parseFloat($("#lblStockReservado").html()))) {
                $("#msgErrorDetalle").html("La cantidad a transferir no puede superar al stock con reservas");
                $("#divErrorDetalle").show();
            }
            else if ($("#ddlProductos").val() == "" || $("#ddlProductos").val() == null) {
                $("#msgErrorDetalle").html("Debe elegir un producto para continuar");
                $("#divErrorDetalle").show();
            }
            else if ($("#ddlInventarioOrigen").val() == "" || $("#ddlInventarioOrigen").val() == null) {
                $("#msgErrorDetalle").html("Debe elegir un inventario de origen para continuar");
                $("#divErrorDetalle").show();
            }
            else {

                var info = "{ idConcepto: " + parseInt($("#ddlProductos").val())
                        + ", idInventario: " + parseInt($("#ddlInventarioOrigen").val())
                        + ", cantidad: '" + $("#txtCantidad").val()
                        + "'}";

                $.ajax({
                    type: "POST",
                    url: "/modulos/inventarios/movimientosDeStocke.aspx/agregarItem",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        if (esModificacion)
                            $('#ddlProductos').prop('disabled', true);

                        $("#txtCantidad").val("");
                        $("#ddlProductos").val("").trigger("change");
                        $("#hdnIDItem").val("0");
                        $("#hdnCodigo").val("");
                        $("#btnAgregarItem").html("Agregar");
                        $("#txtCantidad").focus();
                        $("#lblStockActual").html("0.0000");
                        $("#lblStockReservado").html("0.0000");
                        movimientosStock.obtenerItems();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#msgErrorDetalle").html(r.Message);
                        $("#divErrorDetalle").show();
                    }
                });
            }

        }
        else {
            $("#msgErrorDetalle").html("Debes ingresar el producto y la cantidad a transferir.");
            $("#divErrorDetalle").show();
        }
    },
    cancelarItem: function () {

        $("#txtConcepto, #txtCantidad").val("");
        $("#lblStockActual").html(0, 00);
        $("#lblStockReservado").html(0, 00);
        $("#txtCantidad").val("");
        //$("#hdnIDItem").val("0");
        $("#ddlProductos").val("").trigger("change");
        $("#btnAgregarItem").html("Agregar");

    },
    eliminarItem: function (id) {
        var info = "{ id: " + parseInt(id) + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/inventarios/movimientosDeStocke.aspx/eliminarItem",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                movimientosStock.obtenerItems();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorDetalle").html(r.Message);
                $("#divErrorDetalle").show();
            }
        });
    },
    ocultarMensajes: function () {
        $("#divError, #divOk, #divErrorDetalle").hide();
    },
    obtenerItems: function () {
        $.ajax({
            type: "POST",
            url: "movimientosDeStocke.aspx/obtenerItems",
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                    movimientosStock.cantidadItems();
                }
            }
        });
    },
    obtenerStockConcepto: function () {

        if ($("#ddlInventarioOrigen").val() != null && $("#ddlInventarioOrigen").val() != "" && $("#ddlProductos").val() != null && $("#ddlProductos").val() != "") {

            var info = "{ idInventario: " + parseInt($("#ddlInventarioOrigen").val())
                   + ", idConcepto: '" + $("#ddlProductos").val()
                   + "'}";

            $.ajax({
                type: "POST",
                url: "/modulos/inventarios/movimientosDeStocke.aspx/obtenerStockConcepto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.Stock != null) {

                        $("#lblStockActual").html(data.d.Stock.replace(",0000", ""));
                        $("#lblStockReservado").html(data.d.StockReservado.replace(",0000", ""));
                    }
                    else {
                        $("#txtConcepto, #txtCantidad, #txtPrecio, #txtBonificacion").val("");
                        $("#txtConcepto").attr("disabled", false);
                        $("#ddlProductos").val("").trigger("change");
                        $("#hdnIDItem").val("0");
                        $("#hdnCodigo").val("");
                        $("#btnAgregarItem").html("Agregar");
                        //$("#txtCantidad").focus();
                        //$("#txtCantidad").val(data.d.stock);
                        $("#lblStockActual").html("0,00");
                        $("#lblStockReservado").html("0,00");
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorDetalle").html(r.Message);
                    $("#divErrorDetalle").show();
                }
            });
        }
    },
    modificarItem: function (id, idConcepto, cantidad) {

        if (idConcepto != "") {
            $("#ddlProductos").val(idConcepto).trigger("change");
        }
        $("#txtCantidad").val(cantidad);
        esModificacion = true;
        $("#hdnIDItem").val(id);
        $("#btnAgregarItem").html("Actualizar");
        $('#ddlProductos').prop('disabled', false);
    },
    limpiarItems: function () {
        $.ajax({
            type: "POST",
            url: "/modulos/inventarios/movimientosDeStocke.aspx/limpiarItems",
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

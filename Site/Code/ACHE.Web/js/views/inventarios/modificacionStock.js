﻿var modificacionStock = {
    configForm: function () {

        $("#txtCantidad").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            precision: 2
        });

        Common.obtenerInventarios("ddlInventarioOrigen", false, true);
        Common.obtenerConceptosCodigoyNombre("ddlProductos", "4", true);
        if ($("#ddlInventarioOrigen").val() != null)
            $("#hdnIDInventarioOrigen").val($("#ddlInventarioOrigen").val());

        $("#lblStockActual").html("0");
        $("#lblStockReservado").html("0");

        $(".select4").select2({ width: '100%', allowClear: true, minimumInputLength: 2 });

        $("#txtCantidad").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                //$("#ddlProductos").focus();
                modificacionStock.agregarItem();
                return false;
            }
        });

        modificacionStock.obtenerItems();
    },
    cancelar: function () {
        window.location.href = "/modulos/inventarios/inventarios.aspx";
    },
    cambiarInventario: function () {
        //if ($("#hdnID").val() != null)
        //inventarios.ObtenerStockConceptos();

        if (cantItems > 0) {
            if ($("#hdnIDInventarioOrigen").val() != $("#ddlInventarioOrigen").val()) {
                bootbox.confirm("Al cambiar de inventario se eliminarán las transferencias no realizadas", function (result) {
                    if (result) {
                        modificacionStock.limpiarItems();
                        modificacionStock.obtenerItems();
                        //modificacionStock.cargarProductosPorInventario();
                    }
                    else {
                        $("#ddlInventarioOrigen").val($("#hdnIDInventarioOrigen").val());
                        $("#ddlInventarioOrigen > option[value=" + $("#hdnIDInventarioOrigen").val() + "]").attr("selected", "selected");
                    }
                });
            }
        }
        else {
            //modificacionStock.cargarProductosPorInventario();
        }
    },
    cantidadItems: function () {
        $.ajax({
            type: "POST",
            url: "/modulos/inventarios/modificacionStock.aspx/cantidadItems",
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, text) {
                if (data.d != null) {
                    cantItems = data.d;
                }
                else
                    cantItems = 0
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    /*cargarProductosPorInventario: function () {
        if (parseInt($("#ddlInventarioOrigen").val()) > 0) {
            //Common.obtenerConceptosCodigoyNombrePorInventario("ddlProductos", parseInt($("#ddlInventarioOrigen").val()),0, true);
            $("#hdnIDInventarioOrigen").val($("#ddlInventarioOrigen").val());
        }
        else {
            $("#divError").html("No hay un inventario de origen seleccionado para cargar los productos");
            $("#divError").show();
        }
    },*/
    modificarStock: function () {
        $("#divError").hide();
        $("#divOk").hide();

        var idOrigen = parseInt($("#ddlInventarioOrigen").val());


        if ($('#frmEdicion').valid()) {
            Common.mostrarProcesando("btnActualizar");
            var info = "{ idInventarioOrigen: " + idOrigen
                    + ", observaciones: '" + $("#txtObservaciones").val()
                    + "'}";

            $.ajax({
                type: "POST",
                url: "/modulos/inventarios/modificacionStock.aspx/ModificarStock",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                    modificacionStock.limpiarItems();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                }
            });
        }
        else {
            return false;
        }

    },
    agregarItem: function () {

        modificacionStock.ocultarMensajes();
        esModificacion = false;

        if ($("#txtCantidad").val() != "") {

            var cantidadNueva = parseFloat($("#txtCantidad").val());
            var cantidadReservada = parseFloat($("#lblStockReservado").html());

            if ($("#ddlProductos").val() == "" || $("#ddlProductos").val() == null) {
                $("#msgErrorDetalle").html("Debe elegir un producto para continuar");
                $("#divErrorDetalle").show();
            }
            else if ($("#ddlInventarioOrigen").val() == "" || $("#ddlInventarioOrigen").val() == null) {
                $("#msgErrorDetalle").html("Debe elegir un inventario de origen para continuar");
                $("#divErrorDetalle").show();
            }
            else if (cantidadNueva < cantidadReservada) {
                $("#msgErrorDetalle").html("El nuevo stock debe ser mayor o igual a la reserva");
                $("#divErrorDetalle").show();
            }
            else {

                var info = "{ idConcepto: " + parseInt($("#ddlProductos").val())
                        + ", idInventario: " + parseInt($("#ddlInventarioOrigen").val())
                        + ", cantidad: '" + $("#txtCantidad").val()
                        + "'}";

                $.ajax({
                    type: "POST",
                    url: "/modulos/inventarios/modificacionStock.aspx/agregarItem",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        if (esModificacion)
                            $('#ddlProductos').prop('disabled', true);

                        $("#txtCantidad").val("");
                        $("#ddlProductos").val("").trigger("change");
                        $("#hdnIDItem").val("0");
                        $("#hdnCodigo").val("");
                        $("#btnAgregarItem").html("Agregar");
                        $("#txtCantidad").focus();
                        $("#lblStockActual").html("0");
                        $("#lblStockReservado").html("0");
                        modificacionStock.obtenerItems();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#msgErrorDetalle").html(r.Message);
                        $("#divErrorDetalle").show();
                    }
                });
            }

        }
        else {
            $("#msgErrorDetalle").html("Debes ingresar el producto y la cantidad a transferir.");
            $("#divErrorDetalle").show();
        }
    },
    cancelarItem: function () {

        $("#txtConcepto, #txtCantidad").val("");
        $("#lblStockActual").html(0, 00);
        $("#lblStockReservado").html(0, 00);
        $("#txtCantidad").val("");
        //$("#hdnIDItem").val("0");
        $("#ddlProductos").val("").trigger("change");
        $("#btnAgregarItem").html("Agregar");

    },
    eliminarItem: function (id) {
        var info = "{ id: " + parseInt(id) + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/inventarios/modificacionStock.aspx/eliminarItem",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                modificacionStock.obtenerItems();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorDetalle").html(r.Message);
                $("#divErrorDetalle").show();
            }
        });
    },
    ocultarMensajes: function () {
        $("#divError, #divOk, #divErrorDetalle").hide();
    },
    obtenerItems: function () {
        $.ajax({
            type: "POST",
            url: "modificacionStock.aspx/obtenerItems",
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                    modificacionStock.cantidadItems();
                }
            }
        });
    },
    obtenerStockConcepto: function () {

        if ($("#ddlInventarioOrigen").val() != null && $("#ddlInventarioOrigen").val() != "" && $("#ddlProductos").val() != null && $("#ddlProductos").val() != "") {

            var info = "{ idInventario: " + parseInt($("#ddlInventarioOrigen").val())
                   + ", idConcepto: '" + $("#ddlProductos").val()
                   + "'}";

            $.ajax({
                type: "POST",
                url: "/modulos/inventarios/modificacionStock.aspx/obtenerStockConcepto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d != null) {

                        $("#lblStockActual").html(data.d.StockActual.toString().replace(",0000", ""));
                        $("#lblStockReservado").html(data.d.StockReservado.toString().replace(",0000", ""));
                    }
                    else {
                        $("#txtConcepto, #txtCantidad, #txtPrecio, #txtBonificacion").val("");
                        $("#txtConcepto").attr("disabled", false);
                        $("#ddlProductos").val("").trigger("change");
                        $("#hdnIDItem").val("0");
                        $("#hdnCodigo").val("");
                        $("#btnAgregarItem").html("Agregar");
                        //$("#txtCantidad").focus();
                        //$("#txtCantidad").val(data.d.stock);
                        $("#lblStockActual").html("0");
                        $("#lblStockReservado").html("0");
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorDetalle").html(r.Message);
                    $("#divErrorDetalle").show();
                }
            });
        }
    },
    modificarItem: function (id, idConcepto, cantidad) {
        if (idConcepto != "") {
            $("#ddlProductos").val(idConcepto).trigger("change");
        }
        $("#txtCantidad").val(cantidad);
        esModificacion = true;
        $("#hdnIDItem").val(id);
        $("#btnAgregarItem").html("Actualizar");
        $('#ddlProductos').prop('disabled', false);
    },
    limpiarItems: function () {
        $.ajax({
            type: "POST",
            url: "/modulos/inventarios/modificacionStock.aspx/limpiarItems",
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#bodyDetalle").html("<tr><td colspan='6' style='text-align:center'>No tienes items agregados</td></tr>");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
}

var cantItems = 0;
﻿function crearPersona() {
    $("#divClienteError").hide();

    if ($("#frmNuevaPersona").valid()) {

        var razonSocial = $("#txtRazonSocial").val();
        var condicionIva = $("#ddlCondicionIva").val();
        var personeria = $("#ddlPersoneria").val();
        var nombre = $("#txtNombreFantasia").val();
        var tipoDocumento = $("#ddlTipoDoc").val();
        var nroDocumento = $("#txtNroDocumento").val();
        //var pais = ($("#ddlCiudadCliente").val() == "" || $("#ddlProvincia").val() == null) ? "0" : $("#ddlPais").val();
        //var provincia = ($("#ddlCiudadCliente").val() == "" || $("#ddlProvincia").val() == null) ? "0" : $("#ddlProvincia").val();
        //var ciudad = ($("#ddlCiudadCliente").val() == "" || $("#ddlCiudadCliente").val() == null) ? "0" : $("#ddlCiudadCliente").val();
        var pais = $("#ddlPais").val();
        var provincia = $("#ddlProvincia").val();
        var ciudad = $("#ddlCiudadCliente").val();

        var domicilio = $("#txtDomicilio").val();
        var pisoDepto = $("#txtPisoDepto").val();
        var codigoPostal = $("#txtCp").val();
        var tipo = $("#hdnNuevaPersonaTipo").val();
        var codigo = $("#txtCodigoPersonas").val();
        var emailNuevaPersona = $("#txtEmailNuevaPersona").val();
        if ($("#ddlCondicionIva").val() == "CF") {
            personeria = "F";
            $("#ddlTipoDoc").val("DNI");
            $("#ddlTipoDoc").trigger("change");
            tipoDocumento = "DNI";
        }
        if (pais == "" || pais == null || pais == "null")
            pais = 10;
        if (provincia == "" || provincia == null || provincia == "null")
            provincia = 0;
        if (ciudad == "" || ciudad == null || ciudad == "null")
            ciudad = 0;

        if (provincia == 0) {
            $("#msgAyudaCliente").html("Debe seleccionar la provincia");
            $("#divAyudaCliente").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else {

            var info = "{ razonSocial: '" + razonSocial
                + "', condicionIva: '" + condicionIva
                + "', personeria: '" + personeria
                + "', nombreFantasia: '" + nombre
                + "', tipoDocumento: '" + tipoDocumento
                + "', nroDocumento: '" + nroDocumento
                + "', idPais: " + pais
                + " , idProvincia: '" + provincia
                + "', idCiudad: '" + ciudad
                + "', domicilio: '" + domicilio
                + "', pisoDepto: '" + pisoDepto
                + "', codigoPostal: '" + codigoPostal
                + "', tipo: '" + tipo
                + "', codigo: '" + codigo
                + "', email: '" + emailNuevaPersona
                + "', idUsuarioAdicional: '0"
                + "' }";

            $.ajax({
                type: "POST",
                url: "/common.aspx/CrearPersona",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#txtAyudaMensaje").val("");
                    $("#divAyudaError").hide();

                    $('#modalNuevoCliente').modal('hide');

                    $("#txtRazonSocial,#ddlCondicionIva, #txtNombreFantasia,#ddlTipoDoc,#txtNroDocumento,#txtCiudadCliente,#txtDomicilio,#txtPisoDepto,#txtCp,#txtCodigoPersonas,#txtEmailNuevaPersona").val("");

                    if (data.d != 0)
                        Common.obtenerPersonas('ddlPersona', data.d, false);

                    Common.obtenerPaises("ddlPais", $("#hdnPais").val(), true);
                    Common.obtenerProvincias("ddlProvincia", $("#hdnProvincia").val(), $("#hdnPais").val(), true);

                    $("#ddlCiudadCliente").val("");
                    $("#ddlCiudadCliente").trigger("change");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgAyudaCliente").html(r.Message);
                    $("#divAyudaCliente").show();
                }
            });
        }
    }
    else
        return false;
}

function cargarDatosAfip() {
    $("#msgAyudaCliente").html("");
    $("#divAyudaCliente").hide();
    if ($("#txtNroDocumento").val() != '') {

        $("#btnAfip").hide();
        $("#loadingAfip").show();
        $("#btnCrear").attr("disabled", true);

        var info = "{ nroDocumento: '" + $("#txtNroDocumento").val() + "'}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/ObtenerDatosAfipPersona",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#btnAfip").show();
                $("#loadingAfip").hide();
                $("#btnCrear").attr("disabled", false);
                if (data == null || data.d == null) {
                    $("#msgAyudaCliente").html("No se han encontrado los datos de Afip");
                    $("#divAyudaCliente").show();
                } else {
                    $("#txtAyudaMensaje").val("");
                    $("#divAyudaError").hide();
                    if (data.d.RazonSocial != null) {
                        $("#txtRazonSocial").val(data.d.RazonSocial);
                        $("#txtNombreFantasia").val(data.d.RazonSocial);
                    }
                    if (data.d.Domicilio != null)
                        $("#txtDomicilio").val(data.d.Domicilio);
                    if (data.d.PisoDto != null)
                        $("#txtPisoDepto").val(data.d.PisoDto);
                    if (data.d.CodigoPostal != null)
                        $("#txtCp").val(data.d.CodigoPostal);
                    if (data.d.Personeria != null)
                        $("#ddlPersoneria").val(data.d.Personeria);

                    if (data.d.ProvinciaId != null) {

                        $("#ddlProvincia").val(data.d.ProvinciaId);
                        $("#ddlProvincia").trigger("change");
                        $("#ddlCiudadCliente").trigger("change");
                    }
                    if (data.d.CiudadId != null) {

                        $("#ddlCiudadCliente").val(data.d.CiudadId);
                        $("#ddlCiudadCliente").trigger("change");
                    }
                    if (data.d.TipoDoc != null) {
                        $("#ddlTipoDoc").val(data.d.TipoDoc);
                    }
                    if (data.d.NroDoc != null) {
                        $("#txtNroDocumento").val(data.d.NroDoc);
                    }

                    if (data.d.CategoriaImpositiva != null) {
                        $("#ddlCondicionIva").val(data.d.CategoriaImpositiva);
                    }


                }

            },
            error: function (response) {
                $("#btnAfip").show();
                $("#loadingAfip").hide();
                $("#btnCrear").attr("disabled", false);
                var r = jQuery.parseJSON(response.responseText);
                $("#msgAyudaCliente").html(r.Message);
                $("#divAyudaCliente").show();
            }
        });
    } else {
        $("#msgAyudaCliente").html("Debe ingresar su documento para poder solicitar sus datos a Afip");
        $("#divAyudaCliente").show();
    }
}

function actualizarClientes() {
    //alert("RECARGAR CLIENTES");
    $.ajax({
        type: "GET",
        url: "common.aspx/ActualizarClientes",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null) {
                for (var i = 0; u < data.d.length; i += 2) {
                    $("#ddlPersona").select2('data', { id: data.d[i], text: data.d[i + 1] });
                }
            }
        },
        error: function (response) {
            //var r = jQuery.parseJSON(response.responseText);
            //$("#msgAyudaCliente").html(r.Message);
            //$("#divAyudaCliente").show();
        }
    });
}

function changePais() {
    //Common.obtenerProvincias("ddlCiudadCliente", "", $("#ddlProvincia").val(), true);
    Common.obtenerProvincias("ddlProvincia", $("#ddlProvincia").val(), $("#ddlPais").val(), true);
    $("#ddlProvincia").trigger("change");
    $("#ddlCiudadCliente").trigger("change");
}

function changeProvincia() {
    Common.obtenerCiudades("ddlCiudadCliente", "", $("#ddlProvincia").val(), true);
    $("#ddlCiudadCliente").trigger("change");
}

function changeCondicionIvaNuevaPersona() {
    if ($("#ddlCondicionIva").val() == "CF") {
        $("#divPersoneriaNuevaPersona").hide();
        $("#spIdentificacionObligatoria,#spNuevaPersonaDomicilio").hide();
        $("#ddlTipoDoc,#txtNroDocumento,#txtDomicilio").removeClass("required");
        $("#ddlTipoDoc").val("DNI");
        $("#ddlTipoDoc").trigger("change");
    }
    else {
        $("#ddlTipoDoc").val("");
        $("#ddlTipoDoc").trigger("change");
        $("#divPersoneriaNuevaPersona").show();
        $("#spIdentificacionObligatoria,#spNuevaPersonaDomicilio").show();
        $("#ddlTipoDoc,#txtNroDocumento,#txtDomicilio").addClass("required");
    }
}

$(document).ready(function () {
    $("#txtNroDocumento").numericInput();

    /*$("#ddlCondicionIva").change(function () {
        if ($("#ddlCondicionIva").val() == "RI" || $("#ddlCondicionIva").val() == "EX")
            $("#divFantasia").show();
        else
            $("#divFantasia").hide();
    });*/

    $(".select2").select2({ width: '100%', allowClear: true });

    $("#ddlCondicionIva").change();
    // Validation with select boxes
    $("#frmNuevaPersona").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    })

    Common.obtenerPaises("ddlPais", $("#hdnPais").val(), true);
    Common.obtenerProvincias("ddlProvincia", "2", $("#hdnPais").val(), true);

    $.validator.addMethod("validCuit", function (value, element) {
        var check = true;
        if ($("#ddlTipoDoc").val() == "CUIT") {
            return CuitEsValido($("#txtNroDocumento").val());
        }
        else
            return check;

    }, "CUIT Inválido");
});

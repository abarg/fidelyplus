﻿var cantidadDecimales = 2;

/*** FORM ***/
function changeTipo(resetear) {
    if ($("#ddlTipo").val() == "S") {
        $("#divStock,#divStockMinimo,#divConceptosCombo,#divCodigoBarra,#divPrecioAutomatico").hide();
        if (resetear) {
            $("#txtStockMinimo").val("0");
            $("#txtCodigoDeBarra").val("");
            $("#txtStock").val("1.00");
        }
        $("#divCostoInterno,#divRentabilidad").show();
    }
    else if ($("#ddlTipo").val() == "C") {
        $("#divStock,#divStockMinimo,#divConceptosCombo,#divCodigoBarra,#divPrecioAutomatico").show();
        if ($("#ddlPrecioAutomatico").val() == "Si") {
            $("#txtCostoInterno").prop('disabled', true);
            $("#txtPrecio").prop('disabled', true);

        }
        //    $("#divCostoInterno,#divRentabilidad").hide();
        if (resetear)
            $("#txtStock,#txtStockMinimo,#txtCodigoDeBarra").val("");

    }
    else {
        //$("#divCostoInterno,#divRentabilidad").show();
        $("#divConceptosCombo,#divPrecioAutomatico").hide();
        $("#divStock,#divStockMinimo,#divCodigoBarra").show();
        if (resetear)
            $("#txtStock,#txtStockMinimo,#txtCodigoDeBarra").val("");
    }
}

function changePrecioAutomatico(value) {
    if (value == "Si") {
        $("#txtCostoInterno").prop('disabled', true);
        actualizarTotalCombo();
        $("#txtPrecio").prop('disabled', true);

    } else {
        $("#txtCostoInterno").prop('disabled', false);
        $("#txtPrecio").prop('disabled', false);

    }
}

function verificarPlanDeCuentas() {

    if ($("#hdnPlanCorporativo").val() == "1") {
        $(".divPlanDeCuentas").show();

        if ($("#hdnIDCuentaCompras").val() != "" && $("#hdnIDCuentaCompras").val() != "0") {
            var idPlanCuentaCompra = parseInt($("#hdnIDCuentaCompras").val());
            $("#ddlPlanDeCuentasCompra").val(idPlanCuentaCompra);
            $("#ddlPlanDeCuentasCompra").trigger("change");

        }

        if ($("#hdnIDCuentaVentas").val() != "" && $("#hdnIDCuentaVentas").val() != "0") {
            var idPlanCuentaVenta = parseInt($("#hdnIDCuentaVentas").val());
            $("#ddlPlanDeCuentasVentas").val(idPlanCuentaVenta);
            $("#ddlPlanDeCuentasVentas").trigger("change");

        }
    }
    else {
        $(".divPlanDeCuentas").hide();
    }
}

function ocultarMensajes() {
    $("#divError, #divOk, #divErrorCat").hide();
}

function grabar() {
    ocultarMensajes();

    var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
    if ($('#frmEdicion').valid()) {
        Common.mostrarProcesando("actualizarConcepto");

        var idPersona = ($("#ddlPersonas").val() == "" || $("#ddlPersonas").val() == null) ? 0 : $("#ddlPersonas").val();
        var obj = new Object();
        obj.id = parseInt(id);
        obj.nombre = $("#txtNombre").val();
        obj.codigo = $("#txtCodigo").val();
        obj.codigoOem = $("#txtCodigoOem").val();
        obj.tipo = $("#ddlTipo").val();
        obj.descripcion = $("#txtDescripcion").val();
        obj.estado = $("#ddlEstado").val();
        obj.precio = $("#txtPrecio").val();
        obj.iva = $("#ddlIva").val();
        obj.stock = "";
        obj.obs = $("#txtObservaciones").val();
        obj.constoInterno = $("#txtCostoInterno").val();
        obj.stockMinimo = $("#txtStockMinimo").val();
        obj.codigoBarra = $("#txtCodigoDeBarra").val();
        obj.idPersona = idPersona;
        obj.rentabilidad = $("#txtRentabilidad").val();

        obj.rubro = $("#ddlRubros").val();
        obj.subRubro = $("#ddlSubRubros").val();
        obj.idPlanDeCuentaCompra = $("#ddlPlanDeCuentasCompra").val();
        obj.idPlanDeCuentaVenta = $("#ddlPlanDeCuentasVentas").val();
        obj.precioAutomatico = $("#ddlPrecioAutomatico").val();

        $.ajax({
            type: "POST",
            url: "conceptose.aspx/guardar",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#hdnID").val(data.d);

                if ($("#hdnSinCombioDeFoto").val() == "0") {
                    window.location.href = "/conceptos.aspx";
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                Common.ocultarProcesando("actualizarConcepto", "Aceptar");
            }
        });
    }
    else {
        return false;
    }
}

function cancelar() {
    window.location.href = "/conceptos.aspx";
}

function calcularPrecioConRentabilidad() {

    var total = 0;
    if ($("#txtCostoInterno").val() != "" && parseFloat($("#txtCostoInterno").val()) > 0) {
        total += parseFloat($("#txtCostoInterno").val());
    } else {
        if ($("#txtPrecio").val() != "")
            total += parseFloat($("#txtPrecio").val());
    }

    var rentabilidad = 0;
    if ($("#txtRentabilidad").val() != '') {
        rentabilidad = parseFloat($("#txtRentabilidad").val().replace(",", "."));
    }


    if ($("#txtCostoInterno").val() != "" && parseFloat($("#txtCostoInterno").val()) > 0 && rentabilidad > 0) {
        total = (total + ((total * rentabilidad) / 100));
    }

    //alert(total);

    if (rentabilidad > 0 || ($("#ddlPrecioAutomatico").val() == "Si" && $("#ddlTipo").val() == 'C')) {

        $("#txtPrecio").prop('disabled', true);
        if ($("#ddlPrecioAutomatico").val() != "Si" && $("#ddlTipo").val() != 'C') {
            $("#txtPrecio").val(total.toFixed(cantidadDecimales));
            $("#hdnTotal").val(total.toFixed(cantidadDecimales));
        }
    }
    else
        $("#txtPrecio").prop('disabled', false);

}

function calcularPrecioFinal() {
    var total = 0;
    if ($("#txtRentabilidad").val() == "" || parseFloat($("#txtRentabilidad").val()) == 0) {
        $("#hdnTotal").val($("#txtPrecio").val());
    }
    total += parseFloat($("#hdnTotal").val().replace(",", "."));
    var iva = parseFloat($("#ddlIva").val().replace(",", "."));

    if ($("#hdnUsaPrecioFinalConIVA").val() == "1") {

        switch (iva.toPrecision()) {
            case "0":
                iva = 1;
                break;
            case "2.5":
                iva = 1.025;
                break;
            case "5":
                iva = 1.050;
                break;
            case "10.5":
                iva = 1.105;
                break;
            case "21":
                iva = 1.210;
                break;
            case "27":
                iva = 1.270;
                break;


        }
        var totalConIva = 0;
        if ($("#txtCostoInterno").val() != "" && parseFloat($("#txtCostoInterno").val()) > 0 && $("#txtRentabilidad").val() != "" && parseFloat($("#txtRentabilidad").val()) > 0) {
            totalConIva = total * parseFloat(iva);
        } else {
            totalConIva = total;
            total = total / parseFloat(iva);
        }
        alert(totalConIva);

        $("#txtPrecio").val(totalConIva.toFixed(cantidadDecimales));
        $("#divPrecioIVA").html("$ " + addSeparatorsNF(total.toFixed(cantidadDecimales), '.', ',', '.'));

    }
    else {

        var totalConIva = (total + ((total * iva) / 100));
        $("#divPrecioIVA").html("$ " + addSeparatorsNF(totalConIva.toFixed(cantidadDecimales), '.', ',', '.'));
    }

}

function configForm() {
    $(".select2").select2({ width: '100%', allowClear: true });
    //$(".select3").select2({ width: '100%', allowClear: true });
    //$("#ddlPlanDeCuentas2").trigger("change");

    Common.obtenerPersonas("ddlPersonas", $("#hdnIDPersona").val(), true);
    Common.obtenerRubros("ddlRubros", true);

    $("#txtStockMinimo").numericInput();

    obtenerCantidadDecimales(function (serverData) {
        cantidadDecimales = serverData.d;

        $("#txtStock,#txtPrecio,#txtCostoInterno").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            allowNegative: true,
            precision: serverData.d
        });
    });

    //$("#txtRentabilidad").maskMoney({
    //    thousands: '',
    //    decimal: '.',
    //    allowZero: true,
    //    allowNegative: true,
    //    precision: 2
    //});

    $("#txtPrecio").blur(function () {
        calcularPrecioFinal();
    });
    $("#txtCostoInterno,#txtRentabilidad").change(function () {
        calcularPrecioConRentabilidad();
        calcularPrecioFinal();
    });
    $("#txtPrecio, #ddlIva").change(function () {
        calcularPrecioFinal();
    });

    // Validation with select boxes
    $("#frmEdicion").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    var rentabilidad = 0;
    if ($("#txtRentabilidad").val() != '') {
        rentabilidad = parseFloat($("#txtRentabilidad").val().replace(",", "."));
    }
    if (rentabilidad > 0 || ($("#ddlPrecioAutomatico").val() == "Si" && $("#ddlTipo").val() == 'C'))
        $("#txtPrecio").prop('disabled', true);
    else
        $("#txtPrecio").prop('disabled', false);

    if ($("#hdnID").val() != "" && $("#hdnID").val() != "0") {
        changeTipo(false);
        cargarInfoRubros();
    }
    adjuntarFoto();

    if ($("#ddlTipo").val() == 'C') {
        $("#divConceptosCombo,#divPrecioAutomatico").show();
        if ($("#ddlPrecioAutomatico").val() == "Si") {
            $("#txtCostoInterno").prop('disabled', true);
            $("#txtPrecio").prop('disabled', true);
        }
        // $("#divCostoInterno,#divRentabilidad").hide();
    } else {
        $("#divConceptosCombo,#divPrecioAutomatico").hide();
        //   $("#divCostoInterno,#divRentabilidad").show();
    }

    //verificarPlanDeCuentas();
    if ($("#hdnPlanCorporativo").val() == "1") {
        $(".divPlanDeCuentas").show();

        if ($("#hdnIDCuentaCompras").val() != "" && $("#hdnIDCuentaCompras").val() != "0") {
            var idPlanCuentaCompra = parseInt($("#hdnIDCuentaCompras").val());
            $("#ddlPlanDeCuentasCompra").val(idPlanCuentaCompra);
            $("#ddlPlanDeCuentasCompra").trigger("change");

        }

        if ($("#hdnIDCuentaVentas").val() != "" && $("#hdnIDCuentaVentas").val() != "0") {
            var idPlanCuentaVenta = parseInt($("#hdnIDCuentaVentas").val());
            $("#ddlPlanDeCuentasVentas").val(idPlanCuentaVenta);
            $("#ddlPlanDeCuentasVentas").trigger("change");

        }
    }
    else {
        $(".divPlanDeCuentas").hide();
    }
}

function cargarInfoRubros() {
    var idRubro = $("#hdnIDRubro").val();
    $("#ddlRubros").val(idRubro);
    //  $("#ddlRubros").trigger("change");
    var idSubrubro = $("#hdnIDSubRubro").val();
    Common.obtenerSubRubros(idRubro, 'ddlSubRubros');
    if ($("#hdnIDSubRubro").val() != "" && $("#hdnIDSubRubro").val() != "0") {
        $("#ddlSubRubros").val(idSubrubro);

    }
}

function elimininarTodos() {
    bootbox.confirm("¿Está seguro que desea eliminar <b>TODOS</b> sus productos/servicios?<br>Se excluirán aquellos facturados y/o comprados", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: "conceptos.aspx/deleteAll",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}


/*** SEARCH ***/

function configFilters() {
    $("#txtCondicion,#txtCodigo").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            resetearPagina();
            filtrar();
            return false;
        }
    });
}

function nuevo() {
    window.location.href = "/conceptose.aspx";
}

function importar(tipo) {
    window.location.href = "/importar.aspx?tipo=" + tipo;
}

function editar(id) {
    window.location.href = "/conceptose.aspx?ID=" + id;
}

function verReservas(id, nombre, tipo) {
    $("#bodyReservas").html();
    $("#modalReservasTitle").html("Reservas del producto " + nombre);
    $("#headerReservas").html("<th>Fecha</th><th>Nro Orden</th><th>Origen</th><th>Cliente</th><th>Cantidad</th><th></th>");

    var info = "{id:" + id + ", tipo: '" + tipo.substr(0, 1) + "'}";
    $.ajax({
        type: "POST",
        url: "conceptos.aspx/getReservas",
        contentType: "application/json; charset=utf-8",
        data: info,
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyReservas").html(data.d);
            }
            $('#modalReservas').modal('show');
        }
    });
}

function anularOrdenVenta(id) {

    bootbox.confirm("¿Está seguro que desea anular la venta? <br><b>El stock de los productos dejará de estar reservado</b>", function (result) {
        if (result) {

            var info = "{ idOrdenVenta: " + id + "}";
            $.ajax({
                type: "POST",
                data: info,
                url: "/modulos/ventas/integraciones.aspx/anularOrdenVenta",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#modalReservas").modal("hide");
                    $("#trReserva_" + id).remove();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").show();
                    $("#msgError").html(r.Message);
                }
            });
        }
    });
}

function verStock(id, nombre) {
    $("#bodyReservas").html();
    $("#modalReservasTitle").html("Stock disponible del producto " + nombre);
    $("#headerReservas").html("<th>Depósito</th><th>Cantidad</th>");
    $.ajax({
        type: "GET",
        url: "conceptos.aspx/getStock?id=" + id,
        contentType: "application/json; charset=utf-8",
        //data: info,
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyReservas").html(data.d);
            }
            $('#modalReservas').modal('show');
        }
    });
}

function eliminar(id, nombre) {
    bootbox.confirm("¿Está seguro que desea eliminar el producto/servicios " + nombre + "?", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: "conceptos.aspx/delete",
                data: "{ id: " + id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function duplicar(id) {
    window.location.href = "/conceptose.aspx?ID=" + id + "&Duplicar=1";
}

function mostrarPagAnterior() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual--;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function mostrarPagProxima() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual++;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function resetearPagina() {
    $("#hdnPage").val("1");
}

function filtrar() {

    $("#divError").hide();
    $("#resultsContainer").html("");
    var currentPage = parseInt($("#hdnPage").val());

    var info = new Object();
    info.condicion = $("#txtCondicion").val();
    info.codigo = $("#txtCodigo").val();
    info.page = currentPage;
    info.pageSize = PAGE_SIZE;
    info.tipo = $("#ddlTipo").val();

    $.ajax({
        type: "POST",
        url: "conceptos.aspx/getResults",
        data: JSON.stringify(info),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#resultsContainer").empty();

            if (data.d.TotalPage > 0) {
                $("#divPagination").show();

                $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                if (data.d.TotalPage == 1)
                    $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                else if (currentPage == data.d.TotalPage)
                    $("#lnkNextPage").attr("disabled", "disabled");
                else if (currentPage == 1)
                    $("#lnkPrevPage").attr("disabled", "disabled");

                var aux = (currentPage * PAGE_SIZE);
                if (aux > data.d.TotalItems)
                    aux = data.d.TotalItems;
                $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
            }
            else {
                $("#divPagination").hide();
                $("#msjResultados").html("");
            }

            // Render using the template
            if (data.d.Items.length > 0)
                $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
            else
                $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    resetearExportacion();
}

function verTodos() {
    $("#txtNombre, #txtCodigo, #ddlTipo").val("");
    filtrar();
}

function exportar() {
    resetearExportacion();
    $("#imgLoading").show();
    $("#divIconoDescargar").hide();

    var info = "{ tipo: '" + $("#ddlTipo").val() + "',codigo: '" + $("#txtCodigo").val() + "',condicion: '" + $("#txtCondicion").val() + "'}";

    $.ajax({
        type: "POST",
        url: "conceptos.aspx/export",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {

                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

            resetearExportacion();
        }
    });
}

function resetearExportacion() {
    $("#imgLoading, #lnkDownload").hide();
    $("#divIconoDescargar").show();
}

function obtenerHistorial(id, nombre) {

    $("#bodyDetalle").html();
    $("#modalTitle").html("Historial de precios de " + nombre);
    $.ajax({
        type: "GET",
        url: "conceptos.aspx/getHistorial?id=" + id,
        contentType: "application/json; charset=utf-8",
        //data: info,
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
            $('#modalHistorial').modal('show');
        }
    });
}

function verDetalleCombo(id, sku) {

    $("#bodyDetalle").html();
    $("#modalComboTitle").html("Detalle de combo: " + sku);
    $.ajax({
        type: "GET",
        url: "conceptos.aspx/getDetalleCombo?id=" + id,
        contentType: "application/json; charset=utf-8",
        //data: info,
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyCombo").html(data.d);
            }
            $('#modalCombo').modal('show');
        }
    });
}

/*** FOTOS***/

function adjuntarFoto() {

    $('#flpArchivo').fileupload({
        url: '/subirImagenes.ashx?idconcepto=' + $("#hdnID").val() + "&opcionUpload=conceptos" + "&idUsuario=" + $("#hdnIdUsuario").val(),
        success: function (response, status) {
            if (response == "OK") {
                $("#divError").hide();
                $("#divOk").show();
                window.location.href = "/conceptos.aspx";
            }
            else {
                $("#hdnFileName").val("");
                $("#msgError").html(response);
                $("#divError").show();
                $("#divOk").hide();
                Common.ocultarProcesando("actualizarConcepto", "Aceptar");
            }
        },
        error: function (error) {
            $("#hdnFileName").val("");
            $("#msgError").html(error.responseText);
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');

        },
        autoUpload: false,
        add: function (e, data) {
            $("#hdnSinCombioDeFoto").val("1");

            $("#actualizarConcepto").on("click", function () {
                $("#imgLoading").show();
                grabar();
                if ($("#hdnID").val() != "0") {
                    data.url = '/subirImagenes.ashx?idconcepto=' + $("#hdnID").val() + "&opcionUpload=conceptos" + "&idUsuario=" + $("#hdnIdUsuario").val();
                    data.submit();
                }
            })
        }
    });

    showBtnEliminar();
}

function showBtnEliminar() {
    if ($("#hdnTieneFoto").val() == "1") {
        $("#divEliminarFoto").show();
        //$("#divAdjuntarFoto").removeClass("col-sm-12").addClass("col-sm-6");
    }
    else {
        $("#divEliminarFoto").hide();
        //$("#divAdjuntarFoto").removeClass("col-sm-6").addClass("col-sm-12");
    }
}

function showInputFoto() {
    $("#divFoto").slideToggle();
}

function grabarsinImagen() {

    if ($("#hdnSinCombioDeFoto").val() == "0") {
        grabar();
    }
}

function eliminarFotoProducto() {

    var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
    if (id != "") {


        var info = "{ idConcepto: " + parseInt(id) + "}";

        $.ajax({
            type: "POST",
            url: "conceptose.aspx/eliminarFoto",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgFoto").attr("src", "/files/usuarios/no-producto.png");
                $("#hdnTieneFoto").val("0");
                showBtnEliminar();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    }
    else {
        $("#msgError").html("El producto no tiene una imagen guardada");
        $("#divError").show();
        return false;
    }
}
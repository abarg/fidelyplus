﻿function grabar() {
    $("#divError").hide();
    if ($('#frmRegistro').valid()) {
        if ($("#txtPwd").val() != $("#txtPwd2").val()) {
            $("#divError").html("Las contraseñas no coinciden");
            $("#divError").show();
        }
        else {
            if ($("#chkTyC").is(':checked')) {
                Common.mostrarProcesando("lnkRegistrarme");

                //var info = "{ cuit: '" + $("#txtNroDocumento").val()
                var info = "{ nombre: '" + $("#txtNombre").val()
                        + "', email: '" + $("#txtEmail").val()
                        + "', pwd: '" + $("#txtPwd").val()
                        + "', telefono: '" + $("#txtArea").val() + "-" + $("#txtTelefono").val()
                        + "', codigoPromocion: '" + $("#txtCodigoPromocion").val()
                        + "', actividad: '" + $("#ddlActividad").val()
                        + "'}";

                $.ajax({
                    type: "POST",
                    url: "registro.aspx/guardar",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        //window.location.href = "mis-datos.aspx";
                        window.location.href = "home.aspx";
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        Common.ocultarProcesando("lnkRegistrarme", "Registrarme gratis");
                    }
                });
            }
            else {
                $("#divError").html("Debe aceptar los terminos y condiciones");
                $("#divError").show();
            }
        }
    }
    else {
        return false;
    }
}

function enviar_consulta() {
    $("#divError").hide();
    if ($('#frmRegistro').valid()) {

        Common.mostrarProcesando("lnkRegistrarme");

        //var info = "{ cuit: '" + $("#txtNroDocumento").val()
        var info = "{ nombre: '" + $("#txtNombre").val()
                + "', email: '" + $("#txtEmail").val()
                + "', telefono: '" + $("#txtTelefono").val()
                + "', actividad: '" + $("#ddlActividad").val()
                + "', mensaje: '" + $("#txtMensaje").val()
                + "'}";

        $.ajax({
            type: "POST",
            url: "/registro.aspx/enviarContacto",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //window.location.href = "mis-datos.aspx";
                window.location.href = "gracias.html";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                Common.ocultarProcesando("lnkRegistrarme", "Enviar consulta");
            }
        });
        

    }
    else {
        return false;
    }
}

function grabar_landing() {
    $("#divError").hide();
    if ($('#frmRegistro').valid()) {
        /*if ($("#txtPwd").val() != $("#txtPwd2").val()) {
            $("#divError").html("Las contraseñas no coinciden");
            $("#divError").show();
        }
        else {
            if ($("#chkTyC").is(':checked')) {
                Common.mostrarProcesando("lnkRegistrarme");*/

        Common.mostrarProcesando("lnkRegistrarme");

        //var info = "{ cuit: '" + $("#txtNroDocumento").val()
        var info = "{ nombre: '" + $("#txtNombre").val()
                + "', email: '" + $("#txtEmail").val()
                + "', pwd: '" + $("#txtPwd").val()
                + "', telefono: '" + $("#txtTelefono").val()
                + "', codigoPromocion: 'ADWORDS', actividad: '" + $("#ddlActividad").val() + "'}";

        $.ajax({
            type: "POST",
            url: "/registro.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //window.location.href = "mis-datos.aspx";
                window.location.href = "/home.aspx?showconversion=true";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                Common.ocultarProcesando("lnkRegistrarme", "Quiero probarlo ahora");
            }
        });
        /*}
        else {
            $("#divError").html("Debe aceptar los terminos y condiciones");
            $("#divError").show();
        }
    }*/
    }
    else {
        return false;
    }
}

function grabar_landing2() {
    $("#divError").hide();
    if ($('#frmRegistro').valid()) {
        /*if ($("#txtPwd").val() != $("#txtPwd2").val()) {
            $("#divError").html("Las contraseñas no coinciden");
            $("#divError").show();
        }
        else {
            if ($("#chkTyC").is(':checked')) {
                Common.mostrarProcesando("lnkRegistrarme");*/

        Common.mostrarProcesando("lnkRegistrarme");

        //var info = "{ cuit: '" + $("#txtNroDocumento").val()
        var info = "{ nombre: '" + $("#txtNombre").val()
                + "', email: '" + $("#txtEmail").val()
                + "', pwd: '" + $("#txtPwd").val()
                + "', telefono: '" + $("#txtTelefono").val()
                + "', codigoPromocion: '" + $("#txtCodigoPromocion").val()
                + "', actividad: '" + $("#ddlActividad").val()
                + "'}";

        $.ajax({
            type: "POST",
            url: "/registro.aspx/guardar",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //window.location.href = "mis-datos.aspx";
                window.location.href = "/home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                Common.ocultarProcesando("lnkRegistrarme", "Quiero probarlo ahora");
            }
        });
        /*}
        else {
            $("#divError").html("Debe aceptar los terminos y condiciones");
            $("#divError").show();
        }
    }*/
    }
    else {
        return false;
    }
}

function grabar_landing_desc(descuento) {
    $("#divError").hide();
    if ($('#frmRegistro').valid()) {
        /*if ($("#txtPwd").val() != $("#txtPwd2").val()) {
            $("#divError").html("Las contraseñas no coinciden");
            $("#divError").show();
        }
        else {
            if ($("#chkTyC").is(':checked')) {
                Common.mostrarProcesando("lnkRegistrarme");*/

        Common.mostrarProcesando("lnkRegistrarme");

        //var info = "{ cuit: '" + $("#txtNroDocumento").val()
        var info = "{ nombre: '" + $("#txtNombre").val()
                + "', email: '" + $("#txtEmail").val()
                + "', pwd: '" + $("#txtPwd").val()
                + "', telefono: '" + $("#txtArea").val() + "-" + $("#txtTelefono").val()
                + "', codigoPromocion: '" + $("#txtCodigoPromocion").val()
                + "', actividad: '" + $("#ddlActividad").val()
                + "', descuento: '" + descuento
                + "'}";

        $.ajax({
            type: "POST",
            url: "/registro.aspx/guardarConDescuento",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                //window.location.href = "mis-datos.aspx";
                window.location.href = "/home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#divError").html(r.Message);
                $("#divError").show();
                Common.ocultarProcesando("lnkRegistrarme", "Quiero probarlo ahora");
            }
        });
        /*}
        else {
            $("#divError").html("Debe aceptar los terminos y condiciones");
            $("#divError").show();
        }
    }*/
    }
    else {
        return false;
    }
}

function configForm() {
    $("#txtArea,#txtTelefono").numericInput();

    //$('#txtTelefono,#txtNroDocumento').bind("cut copy paste", function (e) {
    //    e.preventDefault();
    //});

    // Validation with select boxes
    $("#frmRegistro").validate({
        onkeyup: false,
        highlight: function (element) {
            jQuery(element).closest('#divRegistroForm').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('#divRegistroForm').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    //Common.configTelefono("txtTelefono");
    /*$.validator.addMethod("validCuit", function (value, element) {
        var check = true;
        return CuitEsValido($("#txtNroDocumento").val());
    }, "CUIT/CUIL Inválido");*/
}

jQuery(document).ready(function () {
    configForm();
});
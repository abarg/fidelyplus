﻿var saldosClientes = {
    configFilters: function () {
        //$(".select2").select2({ width: '100%', allowClear: true, minimumInputLength: 2 });

        Common.obtenerPersonas("ddlPersona", "", true);

        //$("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        //    var keycode = (event.keyCode ? event.keyCode : event.which);
        //    if (keycode == '13') {
        //        resetearPagina();
        //        filtrar($("#chkAgrupar").is(':checked'));
        //        return false;
        //    }
        //});

        $("#chkMasiva").click(function () {
            $('.chkItem').not(this).prop('checked', this.checked);
            saldosClientes.actualizarTotales();
        });

        // Date Picker
        //Common.configDatePicker();
        //Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

        //$(".validDate").datepicker('option', 'maxDate', new Date("2100-01-01"));//Fix: Permito fechas futuras

        // Validation with select boxes
        $("#frmSearch").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        saldosClientes.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        saldosClientes.filtrar();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
        //$('#chkAgrupar').prop('checked', false);
    },
    clearItems: function () {
        $('#chkMasiva').prop('checked', false);
    },
    filtrar: function (agrupar) {
        //alert(agrupar);

        $("#divError").hide();

        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");
            var currentPage = parseInt($("#hdnPage").val());

            var idPersona = 0;
            if ($("#ddlPersona").val() != null && $("#ddlPersona").val() != "")
                idPersona = parseInt($("#ddlPersona").val());

            var info = "{ idPersona: " + idPersona + ", page: " + currentPage + ", pageSize: " + PAGE_SIZE + "}";


            $.ajax({
                type: "POST",
                url: "/modulos/reportes/saldosClientes.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.d.Items.length > 0) {
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                        $("#resultTemplateTotales").tmpl({ results: data }).appendTo("#resultsContainer");

                    }
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
            saldosClientes.resetearExportacion();
        }
    },
    verTodos: function () {
        $("#txtFechaDesde, #txtFechaHasta").val("");
        $("#ddlPersona").val("").trigger("change");
        saldosClientes.filtrar();
    },
    exportar: function () {
        saldosClientes.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        var idPersona = 0;
        if ($("#ddlPersona").val() != "" && $("#ddlPersona").val() != null)
            idPersona = parseInt($("#ddlPersona").val());

        var info = "{ idPersona: " + idPersona + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/reportes/saldosClientes.aspx/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                saldosClientes.resetearExportacion();
            }
        });
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    actualizarTotales: function () {
        if (($(".chkItem:checked").length) > 0)
            $("#lnkMasiva").show();
        else
            $("#lnkMasiva").hide();
        var totalCant = 0;
        $(".chkItem:checked").each(function () {
            if ($(this).attr("id").indexOf("chkItem") >= 0) {
                totalCant = totalCant + 1;
            }
        });
        $("#hdnTotalCant").val(totalCant);
    },
    showModalMasivo: function () {
        $("#modalEnvioMasivo").modal("show");
        $("#btnEnviar").html("Enviar");
        $("#btnEnviar").attr("disabled", false);
    },
    enviarMail: function () {
        $("#divOkMail,#divErrorMail").hide();

        var valid = true;

        if ($("#txtMasivoAsunto").val() == "") {
            $("#msgErrorEnvioAsunto").show();
            $("#txtMasivoAsunto").closest('.form-group').removeClass('has-success').addClass('has-error');
            valid = false;
        }
        else {
            $("#msgErrorEnvioAsunto").hide();
            $("#txtMasivoAsunto").closest('.form-group').removeClass('has-error');
        }

        if ($("#txtMasivoMensaje").val() == "") {
            $("#msgErrorEnvioMensaje").show();
            $("#txtMasivoMensaje").closest('.form-group').removeClass('has-success').addClass('has-error');
            valid = false;
        }
        else {
            $("#msgErrorEnvioMensaje").hide();
            $("#txtMasivoMensaje").closest('.form-group').removeClass('has-error');
        }

        if (valid) {

            var info = "{ids: '";
            $(".chkItem:checked").each(function () {
                if ($(this).attr("id").indexOf("chkItem") >= 0) {
                    info += $(this).attr("id");
                }
            });
            //info += "', fechaDesde: '" + $("#txtFechaDesde").val();
            //info += "', fechaHasta: '" + $("#txtFechaHasta").val();
            info += "', asunto: '" + $("#txtMasivoAsunto").val();
            info += "', mensaje: '" + $("#txtMasivoMensaje").val();
            info += "'}";


            $("#btnEnviar").html("Enviando...");
            $("#btnEnviar").attr("disabled", true);

            $.ajax({
                type: "POST",
                url: "saldosClientes.aspx/envioMailMasivo",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#modalEnvioMasivo").modal("hide");
                },
                error: function (response) {

                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorMail").html(r.Message);
                    $("#divErrorMail").show();
                    //$("#btnGenerar").attr("disabled", false);
                    //$("#imgLoading2").hide();
                    //$("#modalGeneracionAbonos").modal("hide");
                }
            });
        }
        else
            return;
    },
    verCC: function (id, desc) {
        $("#titDetalle").html("Cuenta corriente de " + desc);
        $("#hdnIDPersona").val(id);

        $("#modalLinea1,#modalLinea2").html("");

        var info = "{ idPersona: " + id + ", verComo: 1}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/getResultsCtaCte",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                
                $("#bodyDetalle").html("");
                // Render using the template
                if (data.d.Items.length > 0) {
                    $("#modalLinea1").html(data.d.DatosAdicionalesLinea1);
                    $("#modalLinea2").html(data.d.DatosAdicionalesLinea2);
                    $("#resultTemplateDetalle").tmpl({ results: data.d.Items }).appendTo("#bodyDetalle");
                }
                else
                    $("#noResultTemplateDetalle").tmpl({ results: data.d.Items }).appendTo("#bodyDetalle");

                $('#modalDetalle').modal('show');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
    }
}
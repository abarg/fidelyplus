﻿/*** SEARCH ***/
function configFilters() {
    $(".select2").select2({ width: '100%', allowClear: true, minimumInputLength: 2 });

    Common.obtenerInventarios("ddlInventarios", true, false);
    Common.obtenerConceptosCodigoyNombre("ddlConcepto", 1, true);

    $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            resetearPagina();
            filtrarBusqueda();
            return false;
        }
    });

    // Date Picker
    Common.configDatePicker();
    Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    // Validation with select boxes
    $("#frmSearch").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    if ($("#ddlInventarios").find("option").length <= 2) {
        $("#ddlInventarios").val("0");
        $(".inventario").hide();
    }
}

function mostrarPagAnterior() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual--;
    $("#hdnPage").val(paginaActual);
    filtrarBusqueda();
}

function mostrarPagProxima() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual++;
    $("#hdnPage").val(paginaActual);
    filtrarBusqueda();
}

function resetearPagina() {
    $("#hdnPage").val("1");
}

function filtrarBusqueda() {
    filtrar();
}

function filtrar() {
    $("#divError").hide();
   
    if ($('#frmSearch').valid()) {
        $("#resultsContainer").html("");

        Common.mostrarProcesando("btnBuscar");

        var currentPage = parseInt($("#hdnPage").val());

        var idConcepto = 0;
        if ($("#ddlConcepto").val() != null && $("#ddlConcepto").val() != "")
            idConcepto = parseInt($("#ddlConcepto").val());

        var idDeposito = 0;
        if ($("#ddlInventarios").val() != null && $("#ddlInventarios").val() != "")
            idDeposito = parseInt($("#ddlInventarios").val());

        var info = "{ idConcepto: " + idConcepto
                   + ", idDeposito: " + idDeposito
                   + ", fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                   + "}";


        $.ajax({
            type: "POST",
            url: "historicoStock.aspx/getResults",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d.TotalPage > 0) {
                    $("#divPagination").show();

                    $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                    if (data.d.TotalPage == 1)
                        $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                    else if (currentPage == data.d.TotalPage)
                        $("#lnkNextPage").attr("disabled", "disabled");
                    else if (currentPage == 1)
                        $("#lnkPrevPage").attr("disabled", "disabled");

                    var aux = (currentPage * PAGE_SIZE);
                    if (aux > data.d.TotalItems)
                        aux = data.d.TotalItems;
                    $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                }
                else {
                    $("#divPagination").hide();
                    $("#msjResultados").html("");
                }

                // Render using the template
                if (data.d.Items.length > 0) {
                    $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");

                    if ($("#ddlInventarios").find("option").length <= 2) {
                        $("#ddlInventarios").val("0");
                        $(".inventario").hide();
                    }
                }
                else
                    $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");

                Common.ocultarProcesando("btnBuscar", "Buscar");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
        resetearExportacion();
    }
}

function exportar()
{
    resetearExportacion();
    $("#imgLoading").show();
    $("#divIconoDescargar").hide();

 $.ajax({
        type: "POST",
        url: "historicoStock.aspx/export",
        data: null,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {

                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            resetearExportacion();
        }
    });
}

function resetearExportacion() {
    $("#imgLoading, #lnkDownload").hide();
    $("#divIconoDescargar").show();
}


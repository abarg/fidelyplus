﻿/*** SEARCH ***/

function configFilters() {
    Common.obtenerInventarios("ddlInventarios", true,false);
    $(".select2").select2({ width: '100%', allowClear: true });

    $("#txtConcepto").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            resetearPagina();
            filtrar($("#chkAgrupar").is(':checked'));
            return false;
        }
    });

}

function mostrarPagAnterior() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual--;
    $("#hdnPage").val(paginaActual);
    filtrar($("#chkAgrupar").is(':checked'));
}

function mostrarPagProxima() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual++;
    $("#hdnPage").val(paginaActual);
    filtrar($("#chkAgrupar").is(':checked'));
}

function resetearPagina() {
    $("#hdnPage").val("1");
}

function filtrar(agrupar) {
    var inventario = 0;
    if ($("#ddlInventarios").val() != "" && $("#ddlInventarios").val() != null)
        inventario = $("#ddlInventarios").val();
    $("#divError").hide();

    if (agrupar) {
        $("#divResultados").hide();
        $("#divResultadosAgrupados").show();
    } else {
        $("#divResultados").show();
        $("#divResultadosAgrupados").hide();
    }


    $("#resultsContainer").html("");
    $("#resultsContainerAgrupados").html("");
    var currentPage = parseInt($("#hdnPage").val());
    var info = "{ estado: '" + $("#ddlEstado").val()
                + "',idInventario: " + inventario
                + ",concepto: '" + $("#txtConcepto").val()
                + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                                       + ", agrupar:" + agrupar

                + "}";

    $.ajax({
        type: "POST",
        url: "stock.aspx/getResults",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#resultsContainer").empty();

            if (data.d.TotalPage > 0) {
                $("#divPagination").show();

                $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                if (data.d.TotalPage == 1)
                    $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                else if (currentPage == data.d.TotalPage)
                    $("#lnkNextPage").attr("disabled", "disabled");
                else if (currentPage == 1)
                    $("#lnkPrevPage").attr("disabled", "disabled");

                var aux = (currentPage * PAGE_SIZE);
                if (aux > data.d.TotalItems)
                    aux = data.d.TotalItems;
                $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
            }
            else {
                $("#divPagination").hide();
                $("#msjResultados").html("");
            }

            // Render using the template
            if (data.d.Items.length > 0){
                $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
            $("#resultTemplateAgrupados").tmpl({ results: data.d.Items }).appendTo("#resultsContainerAgrupados");
        }
            else
             $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
     
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });

    resetearExportacion();
}

function verTodos() {
    $("#ddlEstado").val("");
    $("#ddlInventarios").val("0");
    $("#txtConcepto").val("");

    filtrar($("#chkAgrupar").is(':checked'));
}

function exportar() {
    resetearExportacion();
    $("#imgLoading").show();
    $("#divIconoDescargar").hide();

    var inventario = 0;
    if ($("#ddlInventarios").val() != "" && $("#ddlInventarios").val() != null)
        inventario = $("#ddlInventarios").val();

    var info = "{ estado: '" + $("#ddlEstado").val()
                + "',idInventario: " + inventario
                + ",concepto: '" + $("#txtConcepto").val()
                + "', agrupar:" + $("#chkAgrupar").is(':checked')
                + "}";

    $.ajax({
        type: "POST",
        url: "stock.aspx/export",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {
                
                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            resetearExportacion();
        }
    });
}

function resetearExportacion() {
    $("#imgLoading, #lnkDownload").hide();
    $("#divIconoDescargar").show();
}
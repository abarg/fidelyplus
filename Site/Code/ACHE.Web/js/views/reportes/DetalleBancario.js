﻿var DetalleBancario = {
    configFilters: function () {
        $(".select2").select2({ width: '100%', allowClear: true });

        Common.obtenerBancos("ddlBanco", "");
        $("#txtFechaDesde, #txtFechaHasta, #txtFiltro").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                DetalleBancario.resetearPagina();
                DetalleBancario.filtrar();
                return false;
            }
        });

        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

        // Validation with select boxes
        $("#frmSearch").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        DetalleBancario.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        DetalleBancario.filtrar();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    },
    filtrar: function () {

        $("#divError").hide();

        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");

            Common.mostrarProcesando("btnBuscar");

            var currentPage = parseInt($("#hdnPage").val());

            var idBanco = ($("#ddlBanco").val() != null && $("#ddlBanco").val() != "") ? parseInt($("#ddlBanco").val()) : 0;

            var info = "{ idBanco: " + idBanco
                       + ", filtro: '" + $("#txtFiltro").val()
                       + "', fechaDesde: '" + $("#txtFechaDesde").val()
                       + "', fechaHasta: '" + $("#txtFechaHasta").val()
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + "}";


            $.ajax({
                type: "POST",
                url: "/modulos/reportes/DetalleBancario.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.d.Items.length > 0)
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");

                    Common.ocultarProcesando("btnBuscar", "Buscar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
            DetalleBancario.resetearExportacion();
        }
    },
    verTodos: function () {
        $("#txtFechaDesde, #txtFechaHasta").val("");
        $("#ddlBanco").val("").trigger("change");
        DetalleBancario.filtrar();
    },
    nuevo: function () {
        window.location.href = "/modulos/tesoreria/bancosDetalle.aspx";
    },
    editar: function (entidad, id) {
        if (entidad == "Banco Detalle")
            window.location.href = "/modulos/tesoreria/bancosDetalle.aspx?Id=" + id;
        else if (entidad == "Pago")
            window.location.href = "/modulos/compras/pagose.aspx?Id=" + id;
        else if (entidad == "Cobranza")
            window.location.href = "/cobranzase.aspx?Id=" + id;
        else if (entidad == "Movimiento de Fondo")
            window.location.href = "/modulos/tesoreria/MovimientoDeFondose.aspx?Id=" + id;
        else if (entidad == "Asiento Manual")
            window.location.href = "/modulos/contabilidad/asientosManuales.aspx?id=" + id;
    },
    eliminar: function (id) {
        bootbox.confirm("¿Está seguro que desea eliminar el movimiento ?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: "/modulos/tesoreria/bancosDetalle.aspx/delete",
                    data: "{ id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        DetalleBancario.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        });
    },
    exportar: function () {
        DetalleBancario.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        var idBanco = ($("#ddlBanco").val() != null && $("#ddlBanco").val() != "") ? parseInt($("#ddlBanco").val()) : 0;

        var info = "{ idBanco: " + idBanco
                   + ", filtro: '" + $("#txtFiltro").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/reportes/DetalleBancario.aspx/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                DetalleBancario.resetearExportacion();
            }
        });
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
}
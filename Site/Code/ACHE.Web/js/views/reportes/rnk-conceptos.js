﻿/*** SEARCH ***/

function configFilters() {

    $(".select2").select2({ width: '100%', allowClear: true, });

    $('#ddlRubros').on('change', function () {
        llenarSelectSubRubros(this.value);
    })

    $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            resetearPagina();
            filtrar();
            return false;
        }
    });

    // Date Picker
    Common.configDatePicker();
    Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

    // Validation with select boxes
    $("#frmSearch").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function mostrarPagAnterior() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual--;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function mostrarPagProxima() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual++;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function resetearPagina() {
    $("#hdnPage").val("1");
}

function filtrar() {
    $("#divError").hide();

    if ($('#frmSearch').valid()) {
        Common.mostrarProcesando("btnBuscar");
        $("#resultsContainer").html("");

        var currentPage = parseInt($("#hdnPage").val());
        var info = "{ tipo: '" + $("#ddlTipo").val()
                    + "', estado: '" + $("#ddlEstado").val()
                    + "', fechaDesde: '" + $("#txtFechaDesde").val()
                    + "', fechaHasta: '" + $("#txtFechaHasta").val()
                    + "', idRubro: '" + $("#ddlRubros").val()
                    + "', idSubRubro: '" + $("#ddlSubRubros").val()
                    + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                    + "}";

        $.ajax({
            type: "POST",
            url: "rnk-conceptos.aspx/getResults",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d.TotalPage > 0) {
                    $("#divPagination").show();

                    $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                    if (data.d.TotalPage == 1)
                        $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                    else if (currentPage == data.d.TotalPage)
                        $("#lnkNextPage").attr("disabled", "disabled");
                    else if (currentPage == 1)
                        $("#lnkPrevPage").attr("disabled", "disabled");

                    var aux = (currentPage * PAGE_SIZE);
                    if (aux > data.d.TotalItems)
                        aux = data.d.TotalItems;
                    $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                }
                else {
                    $("#divPagination").hide();
                    $("#msjResultados").html("");
                }

                // Render using the template
                if (data.d.Items.length > 0)
                    $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                else
                    $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");

                Common.ocultarProcesando("btnBuscar", "Buscar");



            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);

            }
        });
        resetearExportacion();

    }

}

function verTodos() {
    $("#txtFechaDesde, #txtFechaHasta, #ddlEstado, #ddlTipo, #ddlRubros, #ddlSubRubros").val("");
    filtrar();
}

function exportar() {
    resetearExportacion();
    $("#imgLoading").show();
    $("#divIconoDescargar").hide();

    var info = "{ tipo: '" + $("#ddlTipo").val()
                + "', estado: '" + $("#ddlEstado").val()
                + "', fechaDesde: '" + $("#txtFechaDesde").val()
                + "', fechaHasta: '" + $("#txtFechaHasta").val()
                + "'}";

    var info = "{ tipo: '" + $("#ddlTipo").val()
            + "', estado: '" + $("#ddlEstado").val()
            + "', fechaDesde: '" + $("#txtFechaDesde").val()
            + "', fechaHasta: '" + $("#txtFechaHasta").val()
            + "', idRubro: '" + $("#ddlRubros").val()
            + "', idSubRubro: '" + $("#ddlSubRubros").val()
            + "'}";

    $.ajax({
        type: "POST",
        url: "rnk-conceptos.aspx/export",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {

                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            resetearExportacion();
        }
    });
}

function resetearExportacion() {
    $("#imgLoading, #lnkDownload").hide();
    $("#divIconoDescargar").show();
}

function llenarSelectSubRubros(id) {
    if (id == "") {
        var html = "";
        html += "<select id='ddlSubRubros' class='form-control'>";
        html += "<option value=''>Seleccione un sub rubro</option>";
        html += "</select>";
        $('#selectSubRubros').html(html);
    } else { 
    var info = "{ idRubro: " + id + "}";
    $.ajax({
        type: "POST",
        url: "rnk-conceptos.aspx/ddlSubRubro",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            var html = "";
            html += "<select id='ddlSubRubros' class='form-control'>";
            html += "<option value=''>Seleccione un sub rubro</option>";
            for (var i = 1; i < data.d.length; i++) {
                html += "<option value='" + data.d[i].IDSubRubro + "'>" + data.d[i].Nombre + "</option>";
            }
            html += "</select>";
            $('#selectSubRubros').html(html);
            //$("#dllSubRubros").select2({ width: '100%', allowClear: true, });

        },
        error: function (response) {
        }
    });
    }
}

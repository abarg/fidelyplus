﻿var cobranzasPendientes = {
    configFilters: function () {
        //$(".select2").select2({ width: '100%', allowClear: true, minimumInputLength: 2 });

        Common.obtenerPersonas("ddlPersona", "", true);

        $("#txtFechaDesde, #txtFechaHasta").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                resetearPagina();
                filtrar($("#chkAgrupar").is(':checked'));
                return false;
            }
        });

        $("#chkMasiva,#chkMasiva2").click(function () {
            $('.chkItem').not(this).prop('checked', this.checked);
            cobranzasPendientes.actualizarTotales();
        });
        
        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");

        $(".validDate").datepicker('option', 'maxDate', new Date("2100-01-01"));//Fix: Permito fechas futuras

        // Validation with select boxes
        $("#frmSearch").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        cobranzasPendientes.filtrar($("#chkAgrupar").is(':checked'));
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        cobranzasPendientes.filtrar($("#chkAgrupar").is(':checked'));
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
        $('#chkAgrupar').prop('checked', false);
    },
    clearItems: function () {
        $('#chkMasiva,#chkMasiva2').prop('checked', false);
    },
    filtrar: function (agrupar) {
        //alert(agrupar);
        if (agrupar) {
            $("#divResultados").hide();
            $("#divResultadosAgrupados").show();
        } else {
            $("#divResultados").show();
            $("#divResultadosAgrupados").hide();
        }

        $("#divError").hide();

        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");
            $("#resultsContainerAgrupados").html("");
            var currentPage = parseInt($("#hdnPage").val());

            var idPersona = 0;
            if ($("#ddlPersona").val() != null && $("#ddlPersona").val() != "")
                idPersona = parseInt($("#ddlPersona").val());

            var info = "{ idPersona: " + idPersona
                       + ", fechaDesde: '" + $("#txtFechaDesde").val()
                       + "', fechaHasta: '" + $("#txtFechaHasta").val()
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + ", agrupar:"+agrupar
                       + "}";


            $.ajax({
                type: "POST",
                url: "/modulos/reportes/cobranzasPendientes.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.d.Items.length > 0) {
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                        $("#resultTemplateTotales").tmpl({ results: data }).appendTo("#resultsContainer");
                        $("#resultTemplateAgrupados").tmpl({ results: data.d.Items }).appendTo("#resultsContainerAgrupados");
                        $("#resultTemplateTotalesAgrupados").tmpl({ results: data }).appendTo("#resultsContainerAgrupados");
                    }
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
            cobranzasPendientes.resetearExportacion();
        }
    },
    verTodos: function () {
        $("#txtFechaDesde, #txtFechaHasta").val("");
        $("#ddlPersona").val("").trigger("change");
        cobranzasPendientes.filtrar($("#chkAgrupar").is(':checked'));
    },
    exportar: function () {
        cobranzasPendientes.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        var idPersona = 0;
        if ($("#ddlPersona").val() != "" && $("#ddlPersona").val() != null)
            idPersona = parseInt($("#ddlPersona").val());

        var info = "{ idPersona: " + idPersona
                   + ", fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "', agrupar:"  + $("#chkAgrupar").is(':checked')
                   + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/reportes/cobranzasPendientes.aspx/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                cobranzasPendientes.resetearExportacion();
            }
        });
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    actualizarTotales: function () {
        if (($(".chkItem:checked").length) > 0)
            $("#lnkMasiva").show();
        else
            $("#lnkMasiva").hide();
        var totalCant = 0;
        $(".chkItem:checked").each(function () {
            if ($(this).attr("id").indexOf("chkItem") >= 0) {
                totalCant = totalCant + 1;
            }
        });
        $("#hdnTotalCant").val(totalCant);
    },
    showModalMasivo: function () {
        $("#modalEnvioMasivo").modal("show");
        $("#btnEnviar").html("Enviar");
        $("#btnEnviar").attr("disabled", false);
    },
    enviarMail: function () {
        $("#divOkMail,#divErrorMail").hide();

        var valid = true;

        if ($("#txtMasivoAsunto").val() == "") {
            $("#msgErrorEnvioAsunto").show();
            $("#txtMasivoAsunto").closest('.form-group').removeClass('has-success').addClass('has-error');
            valid = false;
        }
        else {
            $("#msgErrorEnvioAsunto").hide();
            $("#txtMasivoAsunto").closest('.form-group').removeClass('has-error');
        }

        if ($("#txtMasivoMensaje").val() == "") {
            $("#msgErrorEnvioMensaje").show();
            $("#txtMasivoMensaje").closest('.form-group').removeClass('has-success').addClass('has-error');
            valid = false;
        }
        else{
            $("#msgErrorEnvioMensaje").hide();
            $("#txtMasivoMensaje").closest('.form-group').removeClass('has-error');
        }

        if (valid) {
            
            //var modo = "I";//individual
            //if ($("#chkAgrupar").is(':checked'))
            //    modo = "A";

            var info = "{ids: '";
            $(".chkItem:checked").each(function () {
                if ($(this).attr("id").indexOf("chkItem") >= 0) {
                    info += $(this).attr("id");
                    //totalCant = totalCant + 1;
                }
            });
            info += "',agrupar: " + $("#chkAgrupar").is(':checked');
            info += ", fechaDesde: '" + $("#txtFechaDesde").val();
            info += "', fechaHasta: '" + $("#txtFechaHasta").val();
            info += "', asunto: '" + $("#txtMasivoAsunto").val();
            info += "', mensaje: '" + $("#txtMasivoMensaje").val();
            info += "'}";

            
            $("#btnEnviar").html("Enviando...");
            $("#btnEnviar").attr("disabled", true);

            $.ajax({
                type: "POST",
                url: "cobranzasPendientes.aspx/envioMailMasivo",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#modalEnvioMasivo").modal("hide");

                    /*if (data.d.CantidadGenerados > 0)
                        $("#divWait").html("Por favor, espere unos minutos. Procesando " + data.d.CantidadGenerados + " items de " + $("#hdnTotalCant").val() + "...");
                    $("#divWait").show();
                    
                    var hasta = data.d.Hasta;
                    var desde = 0;

                    var finEjecucion = false;
                    var countCat = countItems;
                    if (countCat > parseInt(hasta) + parseInt(1)) {
                        desde = parseInt(hasta) + parseInt(1);

                    } else {

                        //FIN EJECUCION
                        finEjecucion = true;
                        $("#divWait,#imgAbonoCargando").hide();
                        if (data.d.CantidadErrores > 0) {
                            $("#msgAbonoError,#imgAbonoError").show();
                            $("#spnAbonoOK").html((data.d.CantidadGenerados - data.d.CantidadErrores));
                            $("#spnAbonoError").html(data.d.CantidadErrores);
                            $("#lblAbonoResultadoOK").show();
                            $("#lblAbonoResultadoError").show();
                        }
                        else {
                            $("#msgAbonoOK,#imgAbonoOK").show();
                            $("#spnAbonoOK").html(data.d.CantidadGenerados);
                            $("#lblAbonoResultadoOK").show();
                            $("#lblAbonoResultadoError").hide();
                        }

                        $("#divOk").show();
                        $("#divCerrar").show();
                        $("#btnGenerar").attr("disabled", false);
                        $("#imgLoading2").hide();
                    }

                    if (!finEjecucion) {
                        facturacionMasiva.generarCAE(desde, modoFE);
                    } else {

                        integraciones.filtrar();
                        $("#lnkFacturacionMasiva").hide();
                        if (data.d.CantidadErrores > 0) {
                            $("#resultComprobanteFETemplate").tmpl({ results: data.d.Items.Items }).appendTo("#resultsContainerModal");
                            $("#divresultsContainerModal").show();
                        }
                    }*/
                },
                error: function (response) {

                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorMail").html(r.Message);
                    $("#divErrorMail").show();
                    //$("#btnGenerar").attr("disabled", false);
                    //$("#imgLoading2").hide();
                    //$("#modalGeneracionAbonos").modal("hide");
                }
            });
        }
        else
            return;
    }
}
﻿/*** SEARCH ***/
function configFilters() {
    $(".select2").select2({ width: '100%', allowClear: true });

    Common.obtenerAnios("ddlAnio", "", true);

    // Validation with select boxes
    $("#frmSearch").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
}

function filtrarBusqueda() {
    if ($("#ddlAnio").val() == null || $("#ddlAnio").val() == "") {
        $("#msgError").html("Seleccione un año");
        $("#divError").show();
        return false;
    }
    filtrar();
}

function filtrar() {
    $("#table").html("");
    $("#divError").hide();
    Common.mostrarProcesando("btn_buscar");

    if ($('#frmSearch').valid()) {
        $("#resultsContainer").html("");

        var anio = 0;
        if ($("#ddlAnio").val() != null && $("#ddlAnio").val() != "")
            anio = parseInt($("#ddlAnio").val());

        var incluir = $("#chkIva").is(":checked") ? "1" : "0";
        var incluirCot = $("#ddlIncluirCot").val();

        var info = "{ anio: " + anio + ",incluirIva: " + incluir + ", incluirCot: "+ incluirCot+" }";

        $.ajax({
            type: "POST",
            url: "compras-por-categoria-mensual.aspx/getResults",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $("#table").html(data.d);
                Common.ocultarProcesando("btn_buscar", "Buscar");

            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
                Common.ocultarProcesando("btn_buscar", "Buscar");
            }
        });
    }
    else
        Common.ocultarProcesando("btn_buscar", "Buscar");
}

function exportar() {
    resetearExportacion();
    $("#imgLoading").show();
    $("#divIconoDescargar").hide();
    var anio = 0;
    if ($("#ddlAnio").val() != null && $("#ddlAnio").val() != "")
        anio = parseInt($("#ddlAnio").val());

    var incluirCot = $("#ddlIncluirCot").val();


    var incluir = $("#chkIva").is(":checked") ? "1" : "0";

    var info = "{ anio: " + anio + ",incluirIva: " + incluir + ", incluirCot: " + incluirCot + "}";

    $.ajax({
        type: "POST",
        url: "compras-por-categoria-mensual.aspx/export",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {

                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            resetearExportacion();
        }
    });
}

function resetearExportacion() {
    $("#imgLoading, #lnkDownload").hide();
    $("#divIconoDescargar").show();
}

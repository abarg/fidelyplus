﻿var tipo = "";

function descargarModelo() {

    if ($("#ddlTipo").val() == "Clientes" || $("#ddlTipo").val() == "Proveedores") {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/personas.csv");
    } else if ($("#ddlTipo").val() == "Productos" || $("#ddlTipo").val() == "Servicios") {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/productos.csv");
    } else if ($("#ddlTipo").val() == "PlanDeCuentas") {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/PlanDeCuentas.csv");
    } else if ($("#ddlTipo").val() == "Facturas") {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/FacturasVentas.csv");
    } else if ($("#ddlTipo").val() == "FacturasCompras") {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/FacturasCompras.csv");
    }
    else if ($("#ddlTipo").val() == "Stock") {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/Stock.csv");
    }
    else if ($("#ddlTipo").val() == "FacturasDetalle") {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/FacturasDetalle.csv");
    }
    else {
        $("#DescargarModelo").attr("href", "/ayuda/Modelo/ListaPrecios.csv");
    }
}

function descargarEjemplo() {
    $("#DescargarEjemplo").attr("href", "/ayuda/Ejemplo/" + $("#ddlTipo").val() + ".xlsx");
}

function ocultarMensajes() {
    $("#divError, #divOk, #divErrorCat").hide();
}

function limpiarResultados() {
    $("#ContResultados").hide();
    ocultarMensajes();
    $("#resultThead").empty();
}

function importar() {
    ocultarMensajes();
    Common.mostrarProcesando("btnContinuar");

    if ($('#frmEdicion').valid()) {

        var nombre = $("#hdnFileName").val();
        if (nombre != "") {
            $("#imgLoading").show();
            Common.ocultarProcesando("btnContinuar", "Continuar");
            leerArchivoCSV(nombre);
            return false;
        }
        else {
            $("#msgError").html("Extensión no permitida. El archivo debe ser CSV");
            $("#divError").show();
            $("#divOk").hide();
            Common.ocultarProcesando("btnContinuar", "Continuar");

            return false;
        }
    }
    else {
        Common.ocultarProcesando("btnContinuar", "Continuar");
        return false;
    }
}

function configForm() {
    $('#flpArchivo').fileupload({
        url: 'importarDatos.ashx?upload=start',
        success: function (response, status) {
            if (response.name != "ERROR")
                $("#hdnFileName").val(response.name);
            else {
                $("#hdnFileName").val("");
                $("#msgError").html("Extensión no permitida. El archivo debe ser CSV");
                $("#divError").show();
                $("#divOk").hide();
            }
        },
        error: function (error) {
            $("#hdnFileName").val("");
        }
    }).on('fileuploadsend', function (e, data) {
        // displaying the loading & progress bar
        Common.mostrarProcesando("btnContinuar");
    }).on('fileuploaddone', function (e, data) {
        Common.ocultarProcesando("btnContinuar", "Continuar");
    });


    $("#ddlTipo").change(function () {
        if (this.value == "") {
            $("#divArchivos").hide();
        }
        else {
            $("#divArchivos").show();
        }
    });

    $("#frmEdicion").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    if ($("#ddlTipo").val() == "ListaPrecios") {
        $("#msgListaPrecios").show()
    }
    else {
        $("#msgListaPrecios").hide()
    }
}

function leerArchivoCSV(nombre) {

    tipo = $("#ddlTipo").val();

    var idLista = ($("#hdnIDLista").val() == "" || $("#hdnIDLista").val() == null) ? "0" : $("#hdnIDLista").val();
    var info = "";
    var URL = "";

    if (tipo == "Productos" || tipo == "Servicios") {
        var info = "{ nombre: '" + nombre + "', tipo:'" + tipo + "'}";
        URL = "importar.aspx/leerArchivoCSVProductos";
    }
    else if (tipo == "Clientes" || tipo == "Proveedores") {
        var info = "{ nombre: '" + nombre + "', tipo:'" + tipo + "'}";
        URL = "importar.aspx/leerArchivoCSVPersonas";
    }
    else if (tipo == "ListaPrecios") {
        var info = "{ nombre: '" + nombre + "', idLista:" + idLista + "}";
        URL = "importar.aspx/leerArchivoCSVListaPrecios";
    }
    else if (tipo == "Stock") {
        var info = "{ nombre: '" + nombre + "', idLista:" + idLista + "}";
        URL = "importar.aspx/leerArchivoCSVStock";
    }
    else if (tipo == "PlanDeCuentas") {
        var info = "{ nombre: '" + nombre + "', idLista:" + idLista + "}";
        URL = "importar.aspx/leerArchivoCSVPlanDeCuentas";
    }
    else if (tipo == "Facturas") {
        var info = "{ nombre: '" + nombre + "'}";
        URL = "importar.aspx/leerArchivoCSVFacturas";
    }
    else if (tipo == "FacturasCompras") {
        var info = "{ nombre: '" + nombre + "'}";
        URL = "importar.aspx/leerArchivoCSVFacturasCompras";
    } else if (tipo == "FacturasDetalle") {
        var info = "{ nombre: '" + nombre + "'}";
        URL = "importar.aspx/leerArchivoCSVComprobantes";
    }

    $("#resultThead").empty();
    $("#btnContinuar").attr("disabled", true);

    $.ajax({
        type: "POST",
        url: URL,
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {

            if (tipo == "Productos" || tipo == "Servicios") {
                ArmarTablaResultadosProductos(data.d)
            }
            else if (tipo == "Clientes" || tipo == "Proveedores") {
                ArmarTablaResultadosPersonas(data.d)
            }
            else if (tipo == "ListaPrecios") {
                ArmarTablaResultadosListaPrecios(data.d)
            }
            else if (tipo == "Stock") {
                ArmarTablaResultadosStock(data.d)
            }
            else if (tipo == "PlanDeCuentas") {
                ArmarTablaResultadosPlanDeCuentas(data.d)
            }
            else if (tipo == "Facturas") {
                ArmarTablaResultadosFacturas(data.d)
            }
            else if (tipo == "FacturasCompras") {
                ArmarTablaResultadosFacturasCompras(data.d)
            } else if (tipo == "FacturasDetalle") {
                ArmarTablaResultadosComporbantes(data.d)

            }

            $('[data-toggle="tooltip"]').tooltip({ 'placement': 'bottom' });
            $("#imgLoading").hide();
            $("#btnContinuar").attr("disabled", false);
        },
        error: function (response) {
            $("#imgLoading").hide();
            $("#btnContinuar").attr("disabled", false);
            var r = jQuery.parseJSON(response.responseText);

            $("#msgError").html(r.Message.toString());
            $("#divError").show();
            $("#divOk").hide();
            return false;
        }
    });
}

function ArmarTablaResultadosProductos(mtxProd) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Codigo</th><th>Nombre</th><th>Descripcion</th><th>Observaciones</th><th>Precio Unitario</th><th>Costo Interno</th><th>Stok</th><th>Stok Minimo</th><th>Rentabilidad</th><th>Iva</th><th>Codigo Proveedor</th><th>Deposito</th><th>Codigo de barra</th><th>Rubro</th><th>SubRubro</th></tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    //$("#resultsContainer").empty();
    for (var i = 0; i < mtxProd.length; i++) {

        if (mtxProd[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxProd[i].resultados + "</td><td>" + mtxProd[i].codigo + "</td><td>" + mtxProd[i].nombre + "</td><td>" + mtxProd[i].descripcion + "</td><td>" + mtxProd[i].observaciones + "</td><td>" + mtxProd[i].precioUnitario + "</td><td>" + mtxProd[i].CostoInterno + "</td><td>" + mtxProd[i].stock + "</td><td>" + mtxProd[i].StockMinimo + "</td><td>" + mtxProd[i].rentabilidad + "</td><td>" + mtxProd[i].iva + "</td><td>" + mtxProd[i].CodigoProveedor + "</td><td>" + mtxProd[i].Deposito + "</td><td>" + mtxProd[i].CodigoBarra + "</td><td>" + mtxProd[i].Rubro + "</td><td>" + mtxProd[i].SubRubro + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxProd.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}

function ArmarTablaResultadosPersonas(mtxPersonas) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Código</th><th>RazonSocial</th><th>NombreFantasia</th><th>TipoDocumento</th><th>NroDocumento</th><th>CondicionIva</th>"
        + "<th>Telefono</th><th>Celular</th><th>Web</th><th>Email</th><th>Provincia</th><th>Ciudad</th><th>Domicilio</th>"
        + "<th>PisoDepto</th><th>CodigoPostal</th><th>EmailsEnvioFc</th><th>Personeria</th><th>AlicuotaIvaDefecto</th><th>CBU</th>"
        + "<th>Banco</th><th>Contacto</th><th>UsuarioVendedor</th></tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    //$("#resultsContainer").empty();
    for (var i = 0; i < mtxPersonas.length; i++) {

        if (mtxPersonas[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxPersonas[i].resultados + "</td><td>" + mtxPersonas[i].Codigo + "</td><td>" + mtxPersonas[i].RazonSocial + "</td><td>" + mtxPersonas[i].NombreFantansia + "</td><td>" + mtxPersonas[i].TipoDocumento + "</td><td>" + mtxPersonas[i].NroDocumento + "</td><td>" + mtxPersonas[i].CondicionIva + "</td>"
                                    + "<td>" + mtxPersonas[i].Telefono + "</td><td>" + mtxPersonas[i].Celular + "</td><td>" + mtxPersonas[i].Web + "</td><td>" + mtxPersonas[i].Email + "</td><td>" + mtxPersonas[i].Provincia + "</td><td>" + mtxPersonas[i].Ciudad + "</td><td>" + mtxPersonas[i].Domicilio + "</td>"
                                    + "<td>" + mtxPersonas[i].PisoDepto + "</td><td>" + mtxPersonas[i].CodigoPostal + "</td><td>" + mtxPersonas[i].EmailsEnvioFc + "</td><td>" + mtxPersonas[i].Personeria + "</td><td>" + mtxPersonas[i].AlicuotaIvaDefecto + "</td><td>" + mtxPersonas[i].CBU + "</td>"
                                    + "<td>" + mtxPersonas[i].Banco + "</td><td>" + mtxPersonas[i].Contacto + "</td><td>" + mtxPersonas[i].Vendedor + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxPersonas.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}

function ArmarTablaResultadosListaPrecios(mtxPersonas) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Código</th><th>Costo</th><th>Precio</th><th>Rentabilidad</th></tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    //$("#resultsContainer").empty();
    for (var i = 0; i < mtxPersonas.length; i++) {

        if (mtxPersonas[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxPersonas[i].resultados + "</td><td>" + mtxPersonas[i].Codigo + "</td><td>" + mtxPersonas[i].Costo + "</td><td>" + mtxPersonas[i].Precio + "</td><td>" + mtxPersonas[i].Rentabilidad + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxPersonas.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}

function ArmarTablaResultadosStock(mtxPersonas) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Código</th><th>Stock</th><th>Inventario</th></tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    //$("#resultsContainer").empty();
    for (var i = 0; i < mtxPersonas.length; i++) {

        if (mtxPersonas[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxPersonas[i].resultados + "</td><td>" + mtxPersonas[i].Codigo + "</td><td>" + mtxPersonas[i].Stock + "</td><td>" + mtxPersonas[i].Inventario + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxPersonas.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}

function ArmarTablaResultadosPlanDeCuentas(mtxPlanDeCuentas) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Código</th><th>Nombre</th><th>AdmiteAsientoManual</th><th>TipoDeCuenta</th><th>CodigoPadre</th></tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    for (var i = 0; i < mtxPlanDeCuentas.length; i++) {

        if (mtxPlanDeCuentas[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxPlanDeCuentas[i].resultados + "</td><td>" + mtxPlanDeCuentas[i].Codigo + "</td><td>" + mtxPlanDeCuentas[i].Nombre + "</td><td>" + mtxPlanDeCuentas[i].AdmiteAsientoManual + "</td><td>" + mtxPlanDeCuentas[i].TipoDeCuenta + "</td><td>" + mtxPlanDeCuentas[i].CodigoPadre + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxPlanDeCuentas.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}

function ArmarTablaResultadosFacturas(mtxFacturas) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Razón social</th><th>Email</th><th>Tipo documento</th><th>Nro. documento</th><th>Condicion IVA</th><th>Provincia</th><th>Ciudad</th><th>Domicilio</th><th>Web</th><th>Concepto</th><th>Fecha</th><th>TipoComprobante</th><th>PuntoDeVenta</th><th>NroComprobante</th><th>CAE</th><th>ImporteNeto</th><th>ImporteNoGrabado</th><th>IVA 27,00%</th><th>IVA 21,00%</th><th>IVA 10,50%</th><th>IVA 05,00%</th><th>IVA 02,50%</th><th>IVA 00,00%</th><th>Percepciones IVA</th><th>Percepciones IIBB</th><th>Total</th><th>Forma de pago</th><th>Banco</th><th>Caja</th><th>Fecha de pago</th><th>Monto pagado</th><th>Codigo Cuenta Contable</th><th>Observaciones</th> </tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    for (var i = 0; i < mtxFacturas.length; i++) {

        if (mtxFacturas[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxFacturas[i].resultados + "</td><td>" + mtxFacturas[i].RazonSocial + "</td><td>" + mtxFacturas[i].Email + "</td><td>" + mtxFacturas[i].TipoDocumento + "</td><td>" + mtxFacturas[i].NroDocumento + "</td><td>" + mtxFacturas[i].CondicionIva + "</td><td>" + mtxFacturas[i].Provincia + "</td><td>" + mtxFacturas[i].Ciudad + "</td><td>" + mtxFacturas[i].Domicilio + "</td><td>" + mtxFacturas[i].Web + "</td><td>" + mtxFacturas[i].Concepto + "</td><td>" + mtxFacturas[i].Fecha + "</td><td>" + mtxFacturas[i].TipoComprobante + "</td><td>" + mtxFacturas[i].PuntoDeVenta + "</td><td>" + mtxFacturas[i].NroComprobante + "</td><td>" + mtxFacturas[i].CAE + "</td><td>" + mtxFacturas[i].ImporteNeto + "</td><td>" + mtxFacturas[i].ImporteNoGrabado + "</td><td>" + mtxFacturas[i].IVA2700 + "</td><td>" + mtxFacturas[i].IVA2100 + "</td><td>" + mtxFacturas[i].IVA1005 + "</td><td>" + mtxFacturas[i].IVA0500 + "</td><td>" + mtxFacturas[i].IVA0205 + "</td><td>" + mtxFacturas[i].IVA0000 + "</td><td>" + mtxFacturas[i].PercepcionesIVA + "</td><td>" + mtxFacturas[i].PercepcionesIIBB + "</td><td>" + mtxFacturas[i].Total + "</td><td>" + mtxFacturas[i].FormaDePago + "</td><td>" + mtxFacturas[i].Banco + "</td><td>" + mtxFacturas[i].Caja + "</td><td>" + mtxFacturas[i].FechaDePago + "</td><td>" + mtxFacturas[i].MontoPagado + "</td><td>" + mtxFacturas[i].CodigoCuentaContable + "</td><td>" + mtxFacturas[i].Observaciones + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxFacturas.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}

function ArmarTablaResultadosFacturasCompras(mtxFacturas) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Razón social</th><th>Email</th><th>Tipo documento</th><th>Nro. documento</th><th>Condicion IVA</th><th>Provincia</th><th>Fecha Emision</th><th>Fecha Contable</th><th>Tipo Comprobante</th><th>Punto de Venta</th><th>Nro Comprobante</th><th>Importe Mon</th><th>Gravado al 27</th><th>Gravado al 21</th><th>Gravado al 10.5</th><th>IVA 27,00%</th><th>No gravado</th><th>Exento</th><th>Percepción IVA</th><th>Imp. Nacionales</th><th>Imp. Municipales</th><th>Imp. Internos</th><th>Otros</th><th>Categoria</th><th>Rubro</th><th>Cuenta Contable</th><th>Forma de pago</th><th>Banco</th><th>Caja</th><th>Observaciones</th><th>Fecha pago</th><th>Monto pagado</th> </tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    for (var i = 0; i < mtxFacturas.length; i++) {

        if (mtxFacturas[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxFacturas[i].resultados + "</td><td>" + mtxFacturas[i].RazonSocial + "</td><td>" + mtxFacturas[i].Email + "</td><td>" + mtxFacturas[i].TipoDocumento + "</td><td>" + mtxFacturas[i].NroDocumento + "</td><td>" + mtxFacturas[i].CondicionIva + "</td><td>" + mtxFacturas[i].Provincia + "</td><td>" + mtxFacturas[i].FechaEmision + "</td><td>" + mtxFacturas[i].FechaContable + "</td><td>" + mtxFacturas[i].TipoComprobante + "</td><td>" + mtxFacturas[i].PuntoDeVenta + "</td><td>" + mtxFacturas[i].NroComprobante + "</td><td>" + mtxFacturas[i].ImporteMonotributo + "</td><td>" + mtxFacturas[i].Gravado2700 + "</td><td>" + mtxFacturas[i].Gravado2100 + "</td><td>" + mtxFacturas[i].Gravado1005 + "</td><td>" + mtxFacturas[i].NoGravado + "</td><td>" + mtxFacturas[i].Exento + "</td><td>" + mtxFacturas[i].PercepcionesIVA + "</td><td>" + mtxFacturas[i].PercepcionesIVA + "</td><td>" + mtxFacturas[i].PercepcionesIVA + "</td><td>" + mtxFacturas[i].ImpuestosMunicipales + "</td><td>" + mtxFacturas[i].ImpuestosInternos + "</td><td>" + mtxFacturas[i].Otros + "</td><td>" + mtxFacturas[i].Categoria + "</td><td>" + mtxFacturas[i].Rubro + "</td><td>" + mtxFacturas[i].CodigoCuentaContable + "</td><td>" + mtxFacturas[i].FormaDePago + "</td><td>" + mtxFacturas[i].Banco + "</td><td>" + mtxFacturas[i].Caja + "</td><td>" + mtxFacturas[i].Observaciones + "</td><td>" + mtxFacturas[i].FechaDePago + "</td><td>" + mtxFacturas[i].MontoPagado + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxFacturas.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}


function ArmarTablaResultadosComporbantes(mtxComprobantes) {
    var totalOK = 0;
    $("#resultThead").empty();
    $("#resultThead").append("<thead><tr><th>Resultado</th><th>Numero</th><th>Tipo factura</th><th>Punto de venta</th><th>Fecha</th><th>Periodo desde</th><th>Periodo hasta</th><th>Fecha vencimiento</th><th>CUIT</th><th>Razon social</th><th>Condicion de venta</th><th>Codigo</th><th>Concepto</th><th>Cantidad</th><th>Precio unitario</th><th>Bonificacion</th><th>IVA</th></tr></thead>");
    $("#resultThead").append("<tbody id='resultsContainer'></tbody>");
    for (var i = 0; i < mtxComprobantes.length; i++) {

        if (mtxComprobantes[i].Estado == "A")
        { totalOK++ }

        $("#resultsContainer").append("<tr><td>" + mtxComprobantes[i].resultados + "</td><td>" + mtxComprobantes[i].Numero + "</td><td>" + mtxComprobantes[i].Tipo + "</td><td>" + mtxComprobantes[i].PuntoDeVenta + "</td><td>" + mtxComprobantes[i].Fecha + "</td><td>" + mtxComprobantes[i].PeriodoDesde + "</td><td>" + mtxComprobantes[i].PeriodoHasta + "</td><td>" + mtxComprobantes[i].FechaVencimiento + "</td><td>" + mtxComprobantes[i].CUIT + "</td><td>" + mtxComprobantes[i].RazonSocial + "</td><td>" + mtxComprobantes[i].CondicionVenta + "</td><td>" + mtxComprobantes[i].Codigo + "</td><td>" + mtxComprobantes[i].Concepto + "</td><td>" + mtxComprobantes[i].Cantidad + "</td><td>" + mtxComprobantes[i].PrecioUnitario + "</td><td>" + mtxComprobantes[i].Bonificacion + "</td><td>" + mtxComprobantes[i].IVA + "</td></tr>");
    }

    if (totalOK == 0) {
        $("#divIconoDescargarOK").hide();
    }
    else {
        $("#divIconoDescargarOK").show();
    }

    $("#msjResultados").html("Cantidad a importar " + totalOK + "/" + mtxComprobantes.length + "- Presione importar para continuar o bien corrija los cambios e intente nuevamente");
    $("#ContResultados").show();
}

function importarDatos() {
    var URL = "";
    if (tipo == "Productos" || tipo == "Servicios") {
        URL = "importar.aspx/RealizarImportacionProductos";
    }
    else if (tipo == "Clientes" || tipo == "Proveedores") {
        URL = "importar.aspx/RealizarImportacionPersonas";
    }
    else if (tipo == "ListaPrecios") {
        URL = "importar.aspx/RealizarImportacionListaPrecios";
    }
    else if (tipo == "Stock") {
        URL = "importar.aspx/RealizarImportacionStock";
    }
    else if (tipo == "PlanDeCuentas") {
        URL = "importar.aspx/RealizarImportacionPlanDeCuentas";
    }
    else if (tipo == "Facturas") {
        URL = "importar.aspx/RealizarImportacionFacturas";
    }
    else if (tipo == "FacturasCompras") {
        URL = "importar.aspx/RealizarImportacionFacturasCompras";
    } else if (tipo == "FacturasDetalle") {
        URL = "importar.aspx/RealizarImportacionComprobantes";
    }

    if ($("#hdnTieneCuentasContables").val() == "1" && tipo == "PlanDeCuentas") {
        bootbox.confirm("Al importar este plan de cuentas eliminara todos los datos anteriores <br/>¿Está seguro que desea eliminar el plan de cuentas actual?", function (result) {
            if (result) {
                importarCSV(URL);
            }
        });
    }
    else if (tipo == "Stock" || tipo == "ListaPrecios") {
        bootbox.confirm("¿Está seguro que desea realizar la importación? <b><br><br>Este proceso se ejecuta en segundo plano, cuando finalice, le enviaremos un mail indicándolo.<br><br>Dependiendo la cantidad de informacion a importar puede tardar algunos minutos</b>", function (result) {
            if (result) {
                importarCSV(URL);
            }
        });
    }
    else {
        importarCSV(URL);
    }
}

function importarCSV(URL) {
    $("#divbtnImportar").attr("disabled", true);
    $("#imgLoading2").show();

    $.ajax({
        type: "GET",
        url: URL,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#resultsContainer").empty();
            if(tipo == "Comprobantes")
                window.location.href = "/modulos/ventas/facturasElectronicasPendientes.aspx";
            else
                $("#divOk").show();
            $("#imgLoading2,#ContResultados").hide();
            $("#divbtnImportar").attr("disabled", false);
        },
        error: function (response) {
            $("#imgLoading2,#divOk").hide();
            $("#divbtnImportar").attr("disabled", false);
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message.toString());
            $("#divError").show();
            return false;
        }
    });
}
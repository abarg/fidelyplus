﻿
function grabar() {
    $("#divError").hide();
    if ($('#frmRegistro').valid()) {
        if ($("#txtPwd").val() != $("#txtPwd2").val()) {
            $("#divError").html("Las contraseñas no coinciden");
            $("#divError").show();
        }
        else {
            if ($("#chkTyC").is(':checked')) {
                Common.mostrarProcesando("lnkRegistrarme");
                var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                var code = '';
                var cbk = '';
                var tipoIntegracion = '';
                for (var i = 0; i < url.length; i++) {
                    var urlparam = url[i].split('=');
                    if (urlparam[0] == 'code') {
                        code= urlparam[1];
                    }
                    if (urlparam[0] == 'cbk') {
                        cbk = urlparam[1];
                    }
                    if (urlparam[0] == 'TipoIntegracion') {
                        tipoIntegracion = urlparam[1];
                    }
                }
               
                var info = "{ nombre: '" + $("#txtNombre").val()
                        + "', email: '" + $("#txtEmail").val()
                        + "', pwd: '" + $("#txtPwd").val()
                        + "', cuit: '" + $("#txtNroDocumento").val()
                        + "', condicionIva: '" + $("#ddlCondicionIva").val()
                        + "', telefono: '" + $("#hdnTelefono").val()
                        + "', domicilio: '" + $("#hdnDomicilio").val()
                        + "', idProvincia: " + $("#hdnPciaId").val()
                         + ", idCiudad: " + $("#hdnCiudadId").val()
                          + ", codigoPostal: '" + $("#hdnCodigoPostal").val()
                         + "', codigo: '" + code
                         + "', cbk: '" + cbk
                          + "', tipoIntegracion: '" + tipoIntegracion
                        + "'}";

                $.ajax({
                    type: "POST",
                    url: "registroIntegracion.aspx/guardar",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        window.location.href = data.d;
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        Common.ocultarProcesando("lnkRegistrarme", "Registrarme gratis");
                    }
                });
            }
            else {
                $("#divError").html("Debe aceptar los terminos y condiciones");
                $("#divError").show();
            }
        }
    }
    else {
        return false;
    }
}


function configForm() {
    
    $("#txtNroDocumento").numericInput();
    $(".select2").select2({ width: '100%', allowClear: true });

    // Validation with select boxes
    $("#frmRegistro").validate({
        onkeyup: false,
        highlight: function (element) {
            jQuery(element).closest('#divRegistroForm').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('#divRegistroForm').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $.validator.addMethod("validCuit", function (value, element) {
        var check = true;
        return CuitEsValido($("#txtNroDocumento").val());
    }, "CUIT/CUIL Inválido");
}

jQuery(document).ready(function () {
    configForm();
});
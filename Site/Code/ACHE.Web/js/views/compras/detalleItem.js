﻿var esModificacion = false;
var cantidadDecimales = 2;
var valorDecimalDefault = "1.00";

function configForm() {
    obtenerCantidadDecimales(function (serverData) {
        cantidadDecimales = serverData.d;
        valorDecimalDefault = obtenerValorDecimalDefault(serverData.d, 0);
        $("#txtBonificacion,#txtPrecioDet, #txtCantidad").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            precision: serverData.d
        });
    });

    Common.obtenerConceptosCodigoyNombre("ddlProductos", 3, true);
    obtenerItems();
    obtenerTotales();

    //if (MI_CONDICION == "MO") {
    //    $("#ddlIvaDet").val("0");
    //    $("#ddlIvaDet").attr("disabled", true);
    //}
    //else 
    if (MI_CONDICION == "RI") {
        $("#ddlIvaDet").val("21");
        $("#ddlIvaDet").trigger("change");
    }
   
    $("#txtPrecioDet,#txtBonificacion").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $("#txtCantidad").focus();
            agregarItem();
            return false;
        }
    });
}

function changeConcepto() {
    //if (esModificacion == false) {
        if ($("#ddlProductos").val() != "" && $("#ddlProductos").val() != null) {
            $("#txtConcepto").attr("disabled", true);

            $.ajax({
                type: "POST",
                url: "/conceptose.aspx/obtenerDatosParaCompras",
                data: "{id: " + parseInt($("#ddlProductos").val()) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d != null) {
                        $("#txtConcepto").val(data.d.Nombre);
                        if (esModificacion == false) {
                            $("#txtPrecioDet").val(data.d.Precio);
                            if (data.d.idPlanDeCuentaCompra > 0) {
                                $("#ddlPlanDeCuentas2").val(data.d.idPlanDeCuentaCompra).trigger("change");
                                //  $("#ddlPlanDeCuentas").trigger("change");
                            }
                        }
                        $("#ddlIvaDet").val(data.d.Iva);
                      
                        $("#hdnCodigo").val(data.d.Codigo);
                        $("#ddlIvaDet").trigger("change");
                       
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorDetalle").html(r.Message);
                    $("#divErrorDetalle").show();
                }
            });
        }
        else {
            $("#txtConcepto").attr("disabled", false);
            $("#hdnCodigo").val("");
        }
    //}
}

function ocultarMensajes() {
    $("#divError, #divOk").hide();
}

/*** ITEMS ***/

function obtenerItems() {

    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerItems",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#bodyDetalle").html(data.d);
            }
        }
    });
}

function agregarItem() {

    ocultarMensajes();
    esModificacion = false;
    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        if ($("#ddlPlanDeCuentas2").val() == "") {
            $("#msgErrorDetalle").html("Debes ingresar la cuenta contable.");
            $("#divErrorDetalle").show();
            return false;
        }
    }

    if ($("#txtCantidad").val() != "" && $("#txtConcepto").val() != "" && $("#txtPrecioDet").val() != "") {

        if (parseFloat($("#txtPrecioDet").val()) == 0) {
            $("#msgErrorDetalle").html("El precio debe ser mayor a 0.");
            $("#divErrorDetalle").show();
        }
        else if (parseFloat($("#txtCantidad").val()) == 0) {
            $("#msgErrorDetalle").html("La cantidad debe ser mayor a 0.");
            $("#divErrorDetalle").show();
        }
        else {

            var idPlanDeCuenta = ($("#ddlPlanDeCuentas2").val() == "" || $("#ddlPlanDeCuentas2").val() == null) ? 0 : parseInt($("#ddlPlanDeCuentas2").val());
            var codigoCta = (idPlanDeCuenta == 0) ? "" : $("#ddlPlanDeCuentas2 option:selected").text();

            var info = "{ id: " + parseInt($("#hdnIDItem").val())
                    + ", idConcepto: '" + $("#ddlProductos").val()
                    + "', concepto: '" + $("#txtConcepto").val()
                    + "', iva: '" + $("#ddlIvaDet").val()
                    + "', precio: '" + $("#txtPrecioDet").val()
                    + "', bonif: '" + $("#txtBonificacion").val()
                    + "', cantidad: '" + $("#txtCantidad").val()
                    + "' , codigo:'" + $("#hdnCodigo").val()
                    + "', idPlanDeCuenta:' " + idPlanDeCuenta
                    + "', codigoCta:' " + codigoCta
                    + "'}";

            $.ajax({
                type: "POST",
                url: "/common.aspx/agregarItem",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#txtConcepto, #txtCantidad, #txtPrecioDet, #txtBonificacion").val("");
                    $("#txtConcepto").attr("disabled", false);
                    $("#ddlProductos").val("").trigger("change");
                    $("#hdnIDItem").val("0");
                    $("#hdnCodigo").val("");
                    $("#btnAgregarItem").html("Agregar");
                    $("#txtCantidad").focus();
                    $("#txtCantidad").val(valorDecimalDefault);

                    if ($("#hdnUsaPlanCorporativo").val() == "1") {
                        $("#ddlPlanDeCuentas2").val("").trigger("change");
                    }

                    verificarPlanDeCuentas();
                    obtenerItems();
                    obtenerTotales();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorDetalle").html(r.Message);
                    $("#divErrorDetalle").show();
                }
            });
        }

    }
    else {
        $("#msgErrorDetalle").html("Debes ingresar la cantidad, concepto y precio.");
        $("#divErrorDetalle").show();
    }
}

function verificarPlanDeCuentas() {
    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        $(".divPlanDeCuentas").show();
    }
    else {
        $(".divPlanDeCuentas").hide();
    }
} 

function cancelarItem() {
    obtenerCantidadDecimales(function (serverData) {
        cantidadDecimales = serverData.d;
        valorDecimalDefault = obtenerValorDecimalDefault(serverData.d, 0);

        $("#txtConcepto, #txtCantidad, #txtPrecioDet, #txtBonificacion").val("");
        $("#txtCantidad").val(valorDecimalDefault);
        $("#hdnIDItem").val("0");
        $("#ddlProductos").val("").trigger("change");
        $("#btnAgregarItem").html("Agregar");

        $("#txtCantidad").val(obtenerValorDecimalDefault(serverData.d, 1));
    });
}

function eliminarItem(id) {

    var info = "{ id: " + parseInt(id) + "}";

    $.ajax({
        type: "POST",
        url: "/common.aspx/eliminarItem",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            obtenerItems();
            obtenerTotales();
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgErrorDetalle").html(r.Message);
            $("#divErrorDetalle").show();
        }
    });
}

function modificarItem(id, idConcepto, cantidad,codigo, concepto, precio, iva, bonif, idPlanDeCuenta) {
    $("#txtCantidad").val(cantidad);
    $("#txtConcepto").val(concepto);
    $("#txtPrecioDet").val(precio);
    $("#ddlIvaDet").val(iva);
    $("#txtBonificacion").val(bonif);

    $("#ddlIvaDet").trigger("change");

    if ($("#hdnUsaPlanCorporativo").val() == "1") {
        $("#ddlPlanDeCuentas2").val(idPlanDeCuenta);
        $("#ddlPlanDeCuentas2").trigger("change");
    }
  
    $("#hdnCodigo").val(codigo);
    esModificacion = true;
    if (idConcepto != "") {
        $("#ddlProductos").val(idConcepto).trigger("change");
        $("#txtConcepto").attr("disabled", true);
    }

    $("#hdnIDItem").val(id);
    $("#btnAgregarItem").html("Actualizar");
}

function obtenerTotales() {

    var ImporteNoGrabado = $("#txtImporteNoGrabado").val() == "" ? "0" : $("#txtImporteNoGrabado").val();

    var info = "{ percepcionesIIBB: '" + 0
            + "', percepcionesIVA: '" + 0
            + "', importeNoGrabado: '" + ImporteNoGrabado
            + "'}";
    $.ajax({
        type: "POST",
        url: "/common.aspx/obtenerTotales",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != null) {
                $("#divSubtotal").html("$ " + data.d.Subtotal);
                $("#divIVA").html("$ " + data.d.Iva);
                

                $("#trIVATotal").html("$ " + data.d.PercepcionIVA);
                $("#trIIBBTotal").html("$ " + data.d.PercepcionIIBB);


                $("#divTotalItems").html("$ " + data.d.Total);
                $("#hdnTotal").val(data.d.Total);
                //recalcularTotal();
          
                
                actualizarInfoContenedor(data.d.Iva, data.d.Subtotal, data.d.Total);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgErrorDetalle").html(r.Message);
            $("#divErrorDetalle").show();
        }
    });
}
﻿var dataJurisdicciones = new Array();
var cantidadDecimales = 2;

var Compras = {
    /*** FORM ***/
    configForm: function () {
        
        //$(".select2").select2({
        //    width: '100%', allowClear: true, minimumInputLength: 2,
        //    formatNoMatches: function (term) {
        //        return "<a style='cursor:pointer' onclick=\"$('#modalNuevoCliente').modal('show');$('.select2').select2('close');\">+ Agregar</a>";
        //    }
        //});

        $(".select3").select2({ width: '100%', allowClear: true, });
        $('#txtFechaPrimerVencimiento,#txtFechaSegundoVencimiento').datepicker();
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaEmision", "txtFechaPrimerVencimiento");

        $("#txtNroDocumento").numericInput();

        //obtenerCantidadDecimales(function (serverData) {
            $("#txtIva, #txtImpNacionales, #txtImpMunicipales, #txtImpInternos, #txtIIBB,#txtPercepcionIVA,#txtOtros").maskMoney({
                thousands: '',
                decimal: '.',
                allowZero: true,
                precision: 2
            });
            $("#txtImporte2, #txtImporte5, #txtImporte10, #txtImporte21, #txtImporte27, #txtNoGravado,#txtExento, #txtImporteMon,#txtImporteJurisdiccion").maskMoney({
                thousands: '',
                decimal: '.',
                allowZero: true,
                precision: 2
            });
        //});

        $("#txtNroFactura").mask("?9999-99999999");
        $("#txtNroFactura").blur(function () {
            var pto = $("#txtNroFactura").val().split("-")[0];
            var nro = $("#txtNroFactura").val().split("-")[1];
            $("#txtNroFactura").val(pto + "-" + padZeros(nro, 8));
        });

        $("#txtIva, #txtImporte2, #txtImporte5, #txtImporte10, #txtImporte21, #txtImporte27, #txtNoGravado,#txtExento, #txtImpNacionales, #txtImpMunicipales, #txtImpInternos, #txtIIBB,#txtPercepcionIVA,#txtOtros, #txtImporteMon").blur(function () {//, #txtRedondeo
            Compras.changeImportes();
        });

        $("#ddlTipo").attr("onchange", "Compras.ocultarIva();Compras.changeComprobante();");

        // Validation with select boxes
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#frmEdicionJurisdiccion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#txtImporteJurisdiccion").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                Compras.grabarJurisdiccion();
                return false;
            }
        });

        Common.obtenerPersonas("ddlPersona", $("#hdnIDPersona").val(), true);
        //Common.obtenerNrosOrdenes("ddlNroDeOrden", $("#hdnIDNroOrden").val(), true, $("#ddlPersona").val(), ($("#hdnIDNroOrden").val() != 0 ? true : false))

        $("#txtImporte").change();//Obtengo el total

        var idDuplicado = ($("#hdnDuplicar").val() == "" ? "0" : $("#hdnDuplicar").val());
        var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        if (parseInt(id) == 0 &&parseInt(idDuplicado)==0) {

            Compras.ocultarIva();
            Compras.changeComprobante();
        }
        else {
            if ($("#hdnFileName").val() != "") {
                $(".fileinput-filename").html($("#hdnFileName").val());
                $("#iImgFileFoto, #divLogo").show();
            }
            Compras.obtenerJurisdicciones(1);
        }

        //obtener las del USUARIO
        Compras.obtenerJurisdiccionUsuario();
        Compras.parsearNumeros();
        Compras.verificarPlanDeCuentas();
        Compras.adjuntarFoto();
        Compras.changeImportes();
    },
    changeImportes: function () {
        obtenerCantidadDecimales(function (serverData) {
            cantidadDecimales = serverData.d;

            var total = Compras.getTotal();
            $("#divTotal").html("$ " + addSeparatorsNF(total.toFixed(cantidadDecimales), '.', ',', '.'));

            var totalImportes = Compras.getImportes();
            $("#divTotalImportes").html("$ " + addSeparatorsNF(totalImportes.toFixed(cantidadDecimales), '.', ',', '.'));

            var totalImpuestos = Compras.getImpuestos();
            $("#divTotalImpuestos").html("$ " + addSeparatorsNF(totalImpuestos.toFixed(cantidadDecimales), '.', ',', '.'));
        });
    },
    toggleRetenciones: function () {
        $("#divRetenciones").slideToggle();
    },
    changePersona: function () {
        if ($("#ddlPersona").val() != "") {
            $.ajax({
                type: "POST",
                url: "/personase.aspx/obtenerDatos",
                data: "{ id: " + parseInt($("#ddlPersona").val()) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (data.d != null) {
                        $("#ddlTipo").html("<option value=''></option>");
                        //alert(data.d.CondicionIva);
                        Common.obtenerComprobantesPagoPorCondicion("ddlTipo", data.d.CondicionIva, $("#hdnTipoComprobante").val());
                        //$("#hdnCondicion").val(data.d.CondicionIva);
                        Compras.ocultarIva();
                        Compras.changeComprobante();
                        Common.obtenerNrosOrdenes("ddlNroDeOrden", $("#hdnIDNroOrden").val(), true, $("#ddlPersona").val(), ($("#hdnIDNroOrden").val() != 0 ? true : false))
                        if (parseInt($("#hdnID").val()) == 0) {
                            Compras.ObtenerUltimaCuentaContableCliente();
                            Compras.ObtenerUltimoTipoComprobanteCliente();
                            Compras.obtenerUltimoRubroComprobanteCliente();
                            Compras.obtenerUltimaCategoriaComprobanteCliente();
                        }
                    }
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                }
            });
        }
        else {
            //$("#divFactura").hide();
            $("<option/>").attr("value", "").text("Seleccione un cliente/proveedor").appendTo($("#ddlTipo"));
        }
    },
    ObtenerUltimaCuentaContableCliente: function () {
        var info = "{idPersona: " + $("#ddlPersona").val() + "}";
        $.ajax({
            type: "POST",
            url: "/common.aspx/ObtenerUltimaCuentaContableCliente",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                //alert(data.d);
                if (data.d != null && data.d != "") {
                    $("#ddlPlanDeCuentas").val(data.d);
                    $("#ddlPlanDeCuentas").trigger("change");
                } else {
                    $("#ddlPlanDeCuentas").val("");
                    $("#ddlPlanDeCuentas").trigger("change");
                }
            }
        });
    },
    ObtenerUltimoTipoComprobanteCliente: function (idPersona, controlName) {
        var info = "{idPersona: " + $("#ddlPersona").val() + "}";
        $.ajax({
            type: "POST",
            url: "/common.aspx/ObtenerUltimoTipoComprobanteCliente",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null && data.d != "") {
                    $("#ddlTipo").val(data.d);
                    Compras.ocultarIva();
                    Compras.changeComprobante();
                }
            }
        });
    },
    changeIVA: function () {

        var iva = 0;
        var iva02 = 0;
        var iva05 = 0;
        var iva10 = 0;
        var iva21 = 0;
        var iva27 = 0;

        //obtenerCantidadDecimales(function (serverData) {
        //    cantidadDecimales = serverData.d;

            if ($("#txtImporte2").val() > 0) {
                iva02 = ($("#txtImporte2").val() * 2.5) / 100;
            }

            if ($("#txtImporte5").val() > 0) {
                iva05 = ($("#txtImporte5").val() * 5) / 100;
            }

            if ($("#txtImporte10").val() > 0) {
                iva10 = ($("#txtImporte10").val() * 10.5) / 100;
            }

            if ($("#txtImporte21").val() > 0) {
                iva21 = ($("#txtImporte21").val() * 21) / 100;
            }

            if ($("#txtImporte27").val() > 0) {
                iva27 = ($("#txtImporte27").val() * 27) / 100;
            }

            iva = parseFloat(iva02 + iva05 + iva10 + iva21 + iva27);

            if (iva != 0) {
                $("#txtIva").val(iva.toFixed(2));
            }
            else {
                $("#txtIva").val("0");
            }
        //});
    },
    verificarIVA: function () {
        var iva = 0;
        var iva02 = 0;
        var iva05 = 0;
        var iva10 = 0;
        var iva21 = 0;
        var iva27 = 0;
        var IVAOK = false;
        var ivaMAS = 0;
        var IVAMenos = 0;
        var IVAActual = 0;

        //obtenerCantidadDecimales(function (serverData) {
        //    cantidadDecimales = serverData.d;

            if ($("#txtImporte2").val() > 0) {
                iva02 = ($("#txtImporte2").val() * 2.5) / 100;
            }

            if ($("#txtImporte5").val() > 0) {
                iva05 = ($("#txtImporte5").val() * 5) / 100;
            }

            if ($("#txtImporte10").val() > 0) {
                iva10 = ($("#txtImporte10").val() * 10.5) / 100;
            }

            if ($("#txtImporte21").val() > 0) {
                iva21 = ($("#txtImporte21").val() * 21) / 100;
            }

            if ($("#txtImporte27").val() > 0) {
                iva27 = ($("#txtImporte27").val() * 27) / 100;
            }

            iva = parseFloat(iva02 + iva05 + iva10 + iva21 + iva27);
            iva = iva.toFixed(2);
            ivaMAS = parseFloat(iva) + 1.00;
            IVAMenos = parseFloat(iva) - 1.00;
            IVAActual = parseFloat($("#txtIva").val());

            if (parseFloat(iva) == 0) {
                if (IVAActual == 0) {
                    IVAOK = true;
                }
            }
            else {
                if (IVAActual >= IVAMenos && IVAActual <= ivaMAS) {
                    IVAOK = true;
                }
            }

            if (!IVAOK) {
                $("#msgError").html("El IVA solo puede modificar + o - $1");
                $("#divError").show();
                $("#txtIva").closest('.form-group').removeClass('has-success').addClass("has-error")
            }
            else {
                $("#msgError").html("");
                $("#divError").hide();
                $("#txtIva").closest('.form-group').removeClass('has-error')
            }
        //});
    },
    grabar: function () {
        Compras.ocultarMensajes();
        var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        var idPlanDeCuenta = (($("#ddlPlanDeCuentas").val() == "" || $("#ddlPlanDeCuentas").val() == null) ? "0" : $("#ddlPlanDeCuentas").val());
        var idOrden = (($("#ddlNroDeOrden").val() == "" || $("#ddlNroDeOrden").val() == null) ? "0" : $("#ddlNroDeOrden").val());

        if ($('#frmEdicion').valid()) {
            Common.mostrarProcesando("btnActualizar");


            var total = Compras.getTotal();
            if (total > 0) {
                var info = "{ id: " + parseInt(id)
                        + ", idPersona: " + $("#ddlPersona").val()
                        + ", fecha: '" + $("#txtFecha").val()
                        + "', nroFactura: '" + $("#txtNroFactura").val()
                        + "', iva: '" + $("#txtIva").val()
                        + "', importe2: '" + $("#txtImporte2").val()
                        + "', importe5: '" + $("#txtImporte5").val()
                        + "', importe10: '" + $("#txtImporte10").val()
                        + "', importe21: '" + $("#txtImporte21").val()
                        + "', importe27: '" + $("#txtImporte27").val()
                        + "', noGrav: '" + $("#txtNoGravado").val()
                        + "', importeMon: '" + $("#txtImporteMon").val()
                        + "', impNacional: '" + $("#txtImpNacionales").val()
                        + "', impMunicipal: '" + $("#txtImpMunicipales").val()
                        + "', impInterno: '" + $("#txtImpInternos").val()
                        //+ "', iibb: '" + $("#txtIIBB").val()
                        + "', percepcionIva: '" + $("#txtPercepcionIVA").val()
                        + "', otros: '" + $("#txtOtros").val()
                        + "', obs: '" + $("#txtObservaciones").val()
                        + "', tipo: '" + $("#ddlTipo").val()
                        + "', idCategoria: '" + $("#ddlCategoria").val()
                        + "', rubro: '" + $("#ddlRubro").val()
                        + "', exento: '" + $("#txtExento").val()
                        + "', FechaEmision: '" + $("#txtFechaEmision").val()
                        + "', idPlanDeCuenta: " + idPlanDeCuenta
                        + " , Jurisdicciones: " + JSON.stringify(dataJurisdicciones)
                        + " , fechaPrimerVencimiento: '" + $("#txtFechaPrimerVencimiento").val()
                        + "', fechaSegundoVencimiento: '" + $("#txtFechaSegundoVencimiento").val()
                                                  + "', idOrden: " + idOrden

                        + "}";

                $.ajax({
                    type: "POST",
                    url: "comprase.aspx/guardar",
                    data: info,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (data, text) {
                        $('#divOk').show();
                        $("#divError").hide();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');

                        $("#hdnID").val(data.d);

                        if ($("#hdnSinCombioDeFoto").val() == "0") {
                            if ($("#hdnTieneSaldoAPagar").val() == "1") {
                                $('#modalPagos').modal('show');
                            } else {
                                window.location.href = "compras.aspx";
                            }
                        }
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#msgError").html(r.Message);
                        $("#divError").show();
                        $("#divOk").hide();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                        Common.ocultarProcesando("btnActualizar", "Aceptar");
                    }
                });
            }
            else {
                Common.ocultarProcesando("btnActualizar", "Aceptar");
                $("#msgError").html("El importe debe ser mayor a 0");
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }
        }
        else {
            return false;
        }
    },
    getTotal: function () {
        var total = 0;

        if ($("#txtIva").val() != "") {
            total += parseFloat($("#txtIva").val());
        }
        if ($("#txtImporte2").val() != "") {
            total += parseFloat($("#txtImporte2").val());
        }
        if ($("#txtImporte5").val() != "") {
            total += parseFloat($("#txtImporte5").val());
        }
        if ($("#txtImporte10").val() != "") {
            total += parseFloat($("#txtImporte10").val());
        }
        if ($("#txtImporte21").val() != "") {
            total += parseFloat($("#txtImporte21").val());
        }
        if ($("#txtImporte27").val() != "") {
            total += parseFloat($("#txtImporte27").val());
        }
        if ($("#txtNoGravado").val() != "") {
            total += parseFloat($("#txtNoGravado").val());
        }
        if ($("#txtExento").val() != "") {
            total += parseFloat($("#txtExento").val());
        }


        // IMPUESTOS
        if ($("#txtImpNacionales").val() != "") {
            total += parseFloat($("#txtImpNacionales").val());
        }
        if ($("#txtImpMunicipales").val() != "") {
            total += parseFloat($("#txtImpMunicipales").val());
        }
        if ($("#txtImpInternos").val() != "") {
            total += parseFloat($("#txtImpInternos").val());
        }
        if ($("#txtIIBB").val() != "") {
            total += parseFloat($("#txtIIBB").val());
        }
        if ($("#txtPercepcionIVA").val() != "") {
            total += parseFloat($("#txtPercepcionIVA").val());
        }
        if ($("#txtOtros").val() != "") {
            total += parseFloat($("#txtOtros").val());
        }


        if ($("#txtImporteMon").val() != "") {
            total += parseFloat($("#txtImporteMon").val());
        }
        /*if ($("#txtRedondeo").val() != "") {
            total += parseFloat($("#txtRedondeo").val());
        }*/

        return total;
    },
    getImpuestos: function () {

        var total = 0;
        if ($("#txtIva").val() != "") {
            total += parseFloat($("#txtIva").val());
        }
        // IMPUESTOS
        if ($("#txtImpNacionales").val() != "") {
            total += parseFloat($("#txtImpNacionales").val());
        }
        if ($("#txtImpMunicipales").val() != "") {
            total += parseFloat($("#txtImpMunicipales").val());
        }
        if ($("#txtImpInternos").val() != "") {
            total += parseFloat($("#txtImpInternos").val());
        }
        if ($("#txtIIBB").val() != "") {
            total += parseFloat($("#txtIIBB").val());
        }
        if ($("#txtPercepcionIVA").val() != "") {
            total += parseFloat($("#txtPercepcionIVA").val());
        }
        if ($("#txtOtros").val() != "") {
            total += parseFloat($("#txtOtros").val());
        }
        return total;
    },
    getImportes: function () {

        var total = 0;
        if ($("#txtImporte2").val() != "") {
            total += parseFloat($("#txtImporte2").val());
        }
        if ($("#txtImporte5").val() != "") {
            total += parseFloat($("#txtImporte5").val());
        }
        if ($("#txtImporte10").val() != "") {
            total += parseFloat($("#txtImporte10").val());
        }
        if ($("#txtImporte21").val() != "") {
            total += parseFloat($("#txtImporte21").val());
        }
        if ($("#txtImporte27").val() != "") {
            total += parseFloat($("#txtImporte27").val());
        }
        if ($("#txtNoGravado").val() != "") {
            total += parseFloat($("#txtNoGravado").val());
        }
        if ($("#txtExento").val() != "") {
            total += parseFloat($("#txtExento").val());
        }

        if ($("#txtImporteMon").val() != "") {
            total += parseFloat($("#txtImporteMon").val());
        }

        return total;
    },
    cancelar: function () {
        window.location.href = "compras.aspx";
    },
    verificarPlanDeCuentas: function () {
        if ($("#hdnUsaPlanCorporativo").val() == "1") {
            $(".divPlanDeCuentas").show();
        }
        else {
            $(".divPlanDeCuentas").hide();
        }
    },
    ocultarIva: function () {
        if (MI_CONDICION == "RI" && ($("#ddlTipo").val() == "FCA" || $("#ddlTipo").val() == "NCA" || $("#ddlTipo").val() == "NDA" || $("#ddlTipo").val() == "FCM" || $("#ddlTipo").val() == "NCM" || $("#ddlTipo").val() == "NDM" || $("#ddlTipo").val() == "RCA" || $("#ddlTipo").val() == "TIC")) {
            $(".ivas").show();
            $("#divImporteMon").hide();
            $("#txtImporteMon").val('');
        }
        else {
            $(".ivas").hide();
            $("#divImporteMon").show();
            $("#txtIva,#txtImporte2, #txtImporte5, #txtImporte10, #txtImporte21, #txtImporte27, #txtNoGravado,#txtExento").val('');
        }
        Compras.cargarImporte();
    },
    registrarPago: function (pago) {
        //var pago = $("input[name='PagoParcial']:checked").attr('value');
        window.location.href = 'pagose.aspx?IDPersona=' + $("#ddlPersona").val() + "&IDCompra=" + $("#hdnID").val() + "&Pago=" + pago;
    },
    changeComprobante: function () {
        //obtenerCantidadDecimales(function (serverData) {
        //    cantidadDecimales = serverData.d;

            if ($("#ddlTipo").val() == "FCC" || $("#ddlTipo").val() == "NDC" || $("#ddlTipo").val() == "NCC" || $("#ddlTipo").val() == "COT" || MI_CONDICION != "RI") {
                $("#divImpuestos,#divTotal1,#divTotal2").hide();
                $("#txtIIBB,#txtPercepcionIVA, #txtImpNacionales, #txtImpMunicipales, #txtImpInternos, #txtOtros").val('');
                var total = Compras.getTotal();
                $("#divTotal").html("$ " + addSeparatorsNF(total.toFixed(2), '.', ',', '.'));


            }
            else {
                $("#divImpuestos,#divTotal1,#divTotal2").show();

                var totalImpuestos = Compras.getImpuestos();
                $("#divTotalImpuestos").html("$ " + addSeparatorsNF(totalImpuestos.toFixed(2), '.', ',', '.'));
            }

            var totalImportes = Compras.getImportes();
            $("#divTotalImportes").html("$ " + addSeparatorsNF(totalImportes.toFixed(2), '.', ',', '.'));
        //});
    },
    obtenerUltimoRubroComprobanteCliente: function () {
        var info = "{idPersona: " + $("#ddlPersona").val() + "}";
        $.ajax({
            type: "POST",
            url: "/common.aspx/ObtenerUltimoRubroComprobanteCliente",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#ddlRubro").val(data.d);
            }
        });
    },
    obtenerUltimaCategoriaComprobanteCliente: function () {
        var info = "{idPersona: " + $("#ddlPersona").val() + "}";
        $.ajax({
            type: "POST",
            url: "/common.aspx/ObtenerUltimaCategoriaComprobanteCliente",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#ddlCategoria").val(data.d);
            }
        });
    },
    //*** Categorias***/
    editarCategoria: function (id, nombre) {
        Compras.ocultarMensajes();
        $("#btnCategoria").html("Actualizar");
        $("#txtNuevaCat").val(nombre);
        $("#hdnIDCategoria").val(id);
    },
    eliminarCategoria: function (id) {
        Compras.ocultarMensajes();

        var info = "{ id: " + parseInt(id) + "}";

        $.ajax({
            type: "POST",
            url: "comprase.aspx/eliminarCategoria",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $("#txtNuevaCat").val("");
                $("#hdnIDCategoria").val("0");

                $("#ddlCategoria").html("");
                Compras.obtenerCategorias("ddlCategoria", true);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorCat").html(r.Message);
                $("#divErrorCat").show();
            }
        });
    },
    grabarCategoria: function () {
        Compras.ocultarMensajes();

        if ($("#txtNuevaCat").val() != "") {

            var info = "{id: " + $("#hdnIDCategoria").val() + ", nombre: '" + $("#txtNuevaCat").val() + "'}";

            $.ajax({
                type: "POST",
                url: "comprase.aspx/guardarCategoria",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    Compras.obtenerCategorias();
                    $("#txtNuevaCat").val("");
                    $("#hdnIDCategoria").val("0");

                    $("#ddlCategoria").html("");
                    Common.obtenerCategorias("ddlCategoria", true);
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorCat").html(r.Message);
                    $("#divErrorCat").show();
                }
            });

        }
        else {
            $("#msgErrorCat").html("Debes ingresar un valor");
            $("#divErrorCat").show();
        }
    },
    obtenerCategorias: function () {
        Compras.ocultarMensajes();

        $("#btnCategoria").html("Agregar");
        $("#bodyDetalle").html();

        var info = "{claseJS: 'Compras'}";

        $.ajax({
            type: "POST",
            url: "comprase.aspx/getCategories",
            data: info,            
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
                $('#modalCategorias').modal('show');
            }
        });
    },
    ocultarMensajes: function () {
        $("#divError, #divOk, #divErrorCat,#divErrorJurisdiccion").hide();
    },
    //*** Jurisdicciones ***/
    obtenerJurisdicciones: function (opcion) {

        Compras.ocultarMensajes();
        $("#btnJurisdiccion").html("Agregar");
        $("#bodyDetalleJurisdiccion").html();

        if (opcion == 1) {

            var info = "{idCompra: " + parseInt($("#hdnID").val()) + "}";
            $.ajax({
                type: "POST",
                data: info,
                url: "comprase.aspx/getJurisdicciones",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.d != null) {
                        for (var i = 0; i < data.d.length; i++) {
                            dataJurisdicciones.push(data.d[i]);
                        }
                    }
                },
                error: function (response) {
                }
            });
        }
        else {
            Compras.armarTablaJurisdicciones();
            $('#modalJurisdiccion').modal('show');
        }

    },
    armarTablaJurisdicciones: function () {
        obtenerCantidadDecimales(function (serverData) {
            cantidadDecimales = serverData.d;

            var total = 0;
            for (var i = 0; i < dataJurisdicciones.length; i++) {
                total += parseFloat(dataJurisdicciones[i].Importe);
            }

            $("#txtIIBB").val(total.toFixed(cantidadDecimales));
            Compras.changeImportes();

            $("#bodyDetalleJurisdiccion").html("");
            if (dataJurisdicciones.length > 0)
                $("#resultTemplate").tmpl({ results: dataJurisdicciones }).appendTo("#bodyDetalleJurisdiccion");
            else
                $("#noResultTemplate").tmpl({ results: dataJurisdicciones }).appendTo("#bodyDetalleJurisdiccion");
        });
    },
    grabarJurisdiccion: function () {
        $("#divErrorJurisdiccion").hide();
        if ($("#txtImporteJurisdiccion,#ddlJurisdiccion").valid() && Compras.validarJurisdicciones()) {
            var obj = new Object();
            obj.IDJurisdicion = $("#ddlJurisdiccion").val();
            obj.Importe = $("#txtImporteJurisdiccion").val();
            obj.IDCompra = $("#hdnID").val();
            obj.NombreJurisdiccion = $("#ddlJurisdiccion option:selected").text();
            dataJurisdicciones.push(obj);
            Compras.obtenerJurisdicciones();

            $("#txtImporteJurisdiccion,#ddlJurisdiccion").val("");
            $("#ddlJurisdiccion").trigger("change");
        }
        else {
            return false;
        }
    },
    validarJurisdicciones: function () {
        $("#ddlJurisdiccion").val()
        validJurisdicion = dataJurisdicciones.filter(function (el) {
            return el.IDJurisdicion == parseInt($("#ddlJurisdiccion").val());
        });

        if (validJurisdicion.length > 0) {
            $("#msgErrorJurisdiccion").html("La jurisdicción seleccionada ya se encuentra ingresada.")
            $("#divErrorJurisdiccion").show()
            return false;
        }
        if (!parseFloat($("#txtImporteJurisdiccion").val()) > 0) {
            $("#msgErrorJurisdiccion").html("El importe debe ser mayotr a 0.")
            $("#divErrorJurisdiccion").show()
            return false;
        }

        return true
    },
    eliminarJurisdiccion: function (id) {
        dataJurisdicciones = dataJurisdicciones.filter(function (el) {
            return el.IDJurisdicion != id;
        });
        Compras.armarTablaJurisdicciones();
    },
    parsearNumeros: function () {
        numeral.language('fr', {
            delimiters: {
                thousands: ',',
                decimal: '.'
            },
            abbreviations: {
                thousand: 'k',
                million: 'm',
                billion: 'b',
                trillion: 't'
            },
            ordinal: function (number) {
                return number === 1 ? 'er' : 'ème';
            },
            currency: {
                symbol: '€'
            }
        });
        numeral.language('fr');
    },
    obtenerJurisdiccionUsuario: function () {
        $.ajax({
            type: "GET",
            url: "comprase.aspx/obtenerJurisdiccionUsuario",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#ddlJurisdiccion").html("");
                    $("<option/>").attr("value", "").text("").appendTo($("#ddlJurisdiccion"));
                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#ddlJurisdiccion"));
                    }
                }
            }
        });
    },
    cargarImporte: function () {
        if ($("#ddlNroDeOrden").val() > 0) {
            $.ajax({
                type: "POST",
                url: "comprase.aspx/getImporteDeOC",
                data: "{ idOrden: " + $("#ddlNroDeOrden").val() + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    if (MI_CONDICION == "RI" && ($("#ddlTipo").val() == "FCA" || $("#ddlTipo").val() == "NCA" || $("#ddlTipo").val() == "NDA" || $("#ddlTipo").val() == "FCM" || $("#ddlTipo").val() == "NCM" || $("#ddlTipo").val() == "NDM" || $("#ddlTipo").val() == "RCA" || $("#ddlTipo").val() == "TIC")) {
                        $("#txtImporte21").val(data.d);
                        $("#txtImporte21").focus();
                    }
                    else {
                        $("#txtImporteMon").val(data.d);
                        $("#txtImporteMon").focus();
                    }
                   
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    },
    /*** SEARCH ***/
    configFilters: function () {
        //$(".select2").select2({ width: '100%', allowClear: true });

        //Common.obtenerPersonas("ddlPersona", "", true);
       
        // Date Picker
        Common.configDatePicker();
        Common.configFechasDesdeHasta("txtFechaDesde", "txtFechaHasta");
        Common.soloNumerosConGuiones("txtNumero");
        $("#txtNroDocumento").numericInput();

        $("#txtCondicion, #txtFechaDesde, #txtFechaHasta").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                Compras.resetearPagina();
                Compras.filtrar();
                return false;
            }
        });
    },
    nuevo: function () {
        window.location.href = "comprase.aspx";
    },
    nuevoDetallado: function () {
        window.location.href = "compraDetalladase.aspx";
    },
    editar: function (id, detallado) {
        if (detallado)
            window.location.href = "compradetalladase.aspx?ID=" + id;
        else
            window.location.href = "comprase.aspx?ID=" + id;
    },
    duplicar: function (id, detallado) {
        if (detallado)
            window.location.href = "compradetalladase.aspx?ID=" + id + "&Duplicar=1";
        else
             window.location.href = "comprase.aspx?ID=" + id + "&Duplicar=1";
    },
    eliminar: function (id, nombre) {
        bootbox.confirm("¿Está seguro que desea eliminar la compra realizada a " + nombre + "?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: "compras.aspx/delete",
                    data: "{ id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        Compras.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        });
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        Compras.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        Compras.filtrar();
    },
    filtrar: function () {
        $("#divError").hide();

        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");

            var currentPage = parseInt($("#hdnPage").val());

            var idPersona = 0;
            if ($("#ddlPersona").val() != null && $("#ddlPersona").val() != "")
                idPersona = parseInt($("#ddlPersona").val());

            var info = "{ idPersona: " + idPersona
                       + " , condicion: '" + $("#txtCondicion").val()
                       + "', periodo: '" + $("#ddlPeriodo").val()
                       + "', fechaDesde: '" + $("#txtFechaDesde").val()
                       + "', fechaHasta: '" + $("#txtFechaHasta").val()
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + "}";


            $.ajax({
                type: "POST",
                url: "compras.aspx/getResults",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $("#resultsContainer").empty();

                    if (data.d.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.d.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.d.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.d.TotalItems)
                            aux = data.d.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.d.Items.length > 0)
                        $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
        }
    },
    verTodos: function () {
        $("#txtNumero, #txtFechaDesde, #txtFechaHasta").val("");//, #ddlTipo
        $("#ddlPersona").val("").trigger("change");
        Compras.filtrar();
    },
    exportar: function () {
        Compras.resetearExportacion();
        $("#imgLoading").show();
        $("#divIconoDescargar").hide();

        var idPersona = 0;
        if ($("#ddlPersona").val() != "")
            idPersona = parseInt($("#ddlPersona").val());

        var info = "{ idPersona: " + idPersona
                   + " , condicion: '" + $("#txtCondicion").val()
                   + "', periodo: '" + $("#ddlPeriodo").val()
                   + "', fechaDesde: '" + $("#txtFechaDesde").val()
                   + "', fechaHasta: '" + $("#txtFechaHasta").val()
                   + "'}";

        $.ajax({
            type: "POST",
            url: "compras.aspx/export",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d != "") {

                    $("#divError").hide();
                    $("#imgLoading").hide();
                    $("#lnkDownload").show();
                    $("#lnkDownload").attr("href", data.d);
                    $("#lnkDownload").attr("download", data.d);
                }
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

                Compras.resetearExportacion();
            }
        });
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    },
    otroPeriodo: function () {
        if ($("#ddlPeriodo").val() == "-1")
            $('#divMasFiltros').toggle(600);
        else {
            if ($("#divMasFiltros").is(":visible"))
                $('#divMasFiltros').toggle(600);

            $("#txtFechaDesde,#txtFechaHasta").val("");
            Compras.filtrar();
        }
    },
    /*** Adjuntar Foto ***/
    adjuntarFoto: function () {

        $('#flpArchivo').fileupload({
            url: "/subirImagenes.ashx?idCompras=" + $("#hdnID").val() + "&opcionUpload=compras",
            success: function (response, status) {
                if (response == "OK") {
                    $("#divError").hide();
                    $("#divOk").show();
                    $("#btnActualizar").attr("disabled", false);
                }
                else {
                    $("#hdnFileName").val("");
                    $("#msgError").html(response);
                    $("#divError").show();
                    $("#divOk").hide();
                    $("#btnActualizar").attr("disabled", false);
                }
            },
            error: function (error) {
                $("#hdnFileName").val("");
                $("#msgError").html(error.responseText);
                $("#imgLoading").hide();
                $("#divError").show();
                $("#divOk").hide();

                $('html, body').animate({ scrollTop: 0 }, 'slow');
                Common.ocultarProcesando("btnActualizar", "Aceptar");
            },
            autoUpload: false,
            add: function (e, data) {
                $("#hdnSinCombioDeFoto").val("1");
                $("#btnActualizar").on("click", function () {
                    $("#imgLoading").show();
                    Compras.grabar();

                    if ($("#hdnID").val() != "0") {
                        data.url = "/subirImagenes.ashx?idCompras=" + $("#hdnID").val() + "&opcionUpload=compras";
                        data.submit();

                        if ($("#hdnTieneSaldoAPagar").val() == "1") {
                            $('#modalPagos').modal('show');
                        } else {
                            setTimeout(function () {
                                window.location.href = "compras.aspx";
                            }, 2000);
                        }
                    }
                });
            }
        });
        Compras.showBtnEliminar();
    },
    showInputFoto: function () {
        $("#divLogo").slideToggle();
    },
    grabarsinImagen: function () {
        if ($("#hdnSinCombioDeFoto").val() == "0") {
            Compras.grabar();
        }
    },
    eliminarFoto: function () {
        var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        if (id != "") {
            var info = "{ idCheque: " + parseInt(id) + "}";

            $.ajax({
                type: "POST",
                url: "comprase.aspx/eliminarFoto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgFoto").attr("src", "/files/usuarios/no-cheque.png");
                    $("#hdnTieneFoto").val("0");
                    Compras.showBtnEliminar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else {
            $("#msgError").html("El producto no tiene una imagen guardada");
            $("#divError").show();
            return false;
        }
    },
    showBtnEliminar: function () {
        if ($("#hdnTieneFoto").val() == "1") {
            $("#divEliminarFoto").show();
            $("#divAdjuntarFoto").removeClass("col-sm-12").addClass("col-sm-6");
            $("#divComprobante").show();
        }
        else {
            $("#divEliminarFoto").hide();
            $("#divAdjuntarFoto").removeClass("col-sm-6").addClass("col-sm-12");
            $("#divComprobante").hide();
        }
    },
}

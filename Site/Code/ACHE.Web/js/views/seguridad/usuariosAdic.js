﻿var UsuariosAdic = {
    /*** FORM ***/
    ocultarMensajes: function () {
        $("#divError, #divOk").hide();
    },
    grabar: function () {
        UsuariosAdic.ocultarMensajes();

        var accesos = "";
        $("input:checkbox:checked").each(function () {
            //cada elemento seleccionado
            accesos += "#-#" + $(this).attr("id");
        });
        
        var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        if ($('#frmEdicion').valid()) {
            Common.mostrarProcesando("btnActualizar");
            var info = "{ id: " + parseInt(id)
                    + ", email: '" + $("#txtEmail").val()
                    + "', tipo: '" + $("#ddlTipo").val()
                    + "', pwd: '" + $("#txtPwd").val()
                    + "', activo: " + $("#chkActivo").is(':checked')
                   + ", porcentajeComision: '" + $("#txtPorcentajeComision").val()
                    + "', accesos: '" + accesos + "'"
                    + "}";

            $.ajax({
                type: "POST",
                url: "/modulos/seguridad/usuariose.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    window.location.href = "/modulos/seguridad/usuarios.aspx";
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                }
            });
        }
        else {
            return false;
        }
    },
    cancelar: function () {
        window.location.href = "/modulos/seguridad/usuarios.aspx";
    },
    configForm: function () {
        // Validation with select boxes
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#txtEmail").focus(function () {
            if ($(this).val() === 'email@email.com') {
                $(this).val('');
            }
        });
        $("#txtPorcentajeComision").numericInput();
        $("#tblAccesos").on('click', '.chkEmpresa', function () {
            var isChecked = $(this).children()[0].checked;
            var elementId = $(this).children()[0].id;
            var idEmpresa = elementId.split("_")[1];
            $(".chkPuntosDeVenta_" + idEmpresa).each(function () {
                if (isChecked === false) {
                    $(this).children()[0].checked = false;
                }
                $(this).children()[0].disabled = !isChecked;
            });
        });
        
        UsuariosAdic.changeTipo();
    },
    changeTipo: function(){
        if ($("#ddlTipo").val() == "A") {
            $("#divChkAccesoModulos").hide();
            $("#divChkAccesoModulosRestringidos").hide();
            $("#divChkAccesoPermisos").hide();
            $("#divComision").show();
        }
        else if ($("#ddlTipo").val() == "B") {
            $("#divChkAccesoModulos").show();
            $("#divChkAccesoModulosRestringidos").hide();
            $("#divChkAccesoPermisos").hide();
            $("#divComision").show();
        }
        else if ($("#ddlTipo").val() == "T") {
            $("#divChkAccesoModulos").hide();
            $("#divChkAccesoModulosRestringidos").hide();
            $("#divChkAccesoPermisos").hide();
            $("#divComision").hide();
        }
        else {
            //alert($("#hdnAddinUsuariosPersonalizados").val());
            if ($("#hdnAddinUsuariosPersonalizados").val() == 0) {
                bootbox.confirm("Los usuarios personalizados tienen un costo extra. Si desea continuar, el costo se le incluirá en el próximo pago del plan.", function (result) {
                    if (result) {
                        $.ajax({
                            type: "POST",
                            url: "/modulos/seguridad/usuariose.aspx/guardarAddinUsuariosPersonalizados",
                            //data: info,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data, text) {
                                $("#divChkAccesoModulos").hide();
                                $("#divChkAccesoModulosRestringidos").show();
                                $("#divChkAccesoPermisos").show();
                                $("#divComision").show();
                                $("#hdnAddinUsuariosPersonalizados").val("1")
                            },
                            error: function (response) {
                                var r = jQuery.parseJSON(response.responseText);
                                $("#msgError").html(r.Message);
                                $("#divError").show();
                                $("#divOk").hide();
                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                                //Common.ocultarProcesando("btnActualizar", "Aceptar");
                            }
                        });
                    }
                    else {
                        $("#ddlTipo").val("A");
                        UsuariosAdic.changeTipo();
                    }
                });
            } else { 
            $("#divChkAccesoModulos").hide();
            $("#divChkAccesoModulosRestringidos").show();
            $("#divChkAccesoPermisos").show();
            $("#divComision").show();
            }
        }
    },

    /*** SEARCH ***/
    configFilters: function ()
    {
        $("#txtEmail").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                UsuariosAdic.resetearPagina();
                UsuariosAdic.filtrar();
                return false;
            }
        });
    },
    nuevo: function () {
        window.location.href = "/modulos/seguridad/usuariose.aspx";
    },
    editar: function (id) {
        window.location.href = "/modulos/seguridad/usuariose.aspx?ID=" + id;
    },
    eliminar: function (id, email) {
        bootbox.confirm("¿Está seguro que desea eliminar al usuario " + email + "?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: "/modulos/seguridad/usuarios.aspx/delete",
                    data: "{ id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, text) {
                        UsuariosAdic.filtrar();
                    },
                    error: function (response) {
                        var r = jQuery.parseJSON(response.responseText);
                        $("#divError").html(r.Message);
                        $("#divError").show();
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                });
            }
        });
    },
    filtrar: function () {
        $("#divError").hide();
        $("#resultsContainer").html("");
        var currentPage = parseInt($("#hdnPage").val());

        var info = "{ email: '" + $("#txtEmail").val()
                   + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                   + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/usuarios.aspx/getResults",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#resultsContainer").empty();

                if (data.d.TotalPage > 0) {
                    $("#divPagination").show();

                    $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                    if (data.d.TotalPage == 1)
                        $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                    else if (currentPage == data.d.TotalPage)
                        $("#lnkNextPage").attr("disabled", "disabled");
                    else if (currentPage == 1)
                        $("#lnkPrevPage").attr("disabled", "disabled");

                    var aux = (currentPage * PAGE_SIZE);
                    if (aux > data.d.TotalItems)
                        aux = data.d.TotalItems;
                    $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
                }
                else {
                    $("#divPagination").hide();
                    $("#msjResultados").html("");
                }

                // Render using the template
                if (data.d.Items.length > 0)
                    $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
                else
                    $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
    },
    verTodos: function () {
        $("#txtEmail").val("");
        UsuariosAdic.filtrar();
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        UsuariosAdic.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        UsuariosAdic.filtrar();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    }
}
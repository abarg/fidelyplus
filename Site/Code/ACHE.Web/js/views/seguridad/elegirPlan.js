﻿var MODO = "A";

var elegirPlan = {
    ActualizarPlanBasico: function () {
        Common.mostrarProcesando("btnPlanBasico");
        $.ajax({
            type: "GET",
            url: "/modulos/seguridad/pagoDePlanes.aspx/GuardarPlanBasico",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                window.location.href = "/home.aspx";
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                Common.ocultarProcesando("btnPlanBasico", "¡Comenzá a utilizarlo ya!");
            }
        });
    },
    subirPlan: function (plan) {
        if (MODO == "A")
            window.location.href = "/modulos/seguridad/pagoDePlanes.aspx?plan=" + plan + "&modo=A";
        else
            window.location.href = "/modulos/seguridad/pagoDePlanes.aspx?plan=" + plan + "&modo=M";
    },
    togglePrecios: function (tipo) {
        if (tipo == "M") {
            $(".periodicidad").html("/mensual");
            $(".anual").hide();
            $(".mensual").show('slide', { direction: 'left' }, 500);
        }
        else {
            $(".periodicidad").html("/anual");
            $(".mensual").hide();
            $(".anual").show('slide', { direction: 'right' }, 500);
        }

        MODO = tipo;
    }
}



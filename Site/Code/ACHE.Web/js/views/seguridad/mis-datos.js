﻿var MisDatos = {
    configForm: function () {
        $(".select2").select2({ width: '100%', allowClear: true });

        Common.obtenerProvincias("ddlProvincia", $("#hdnProvincia").val(), 10, true);
        if ($("#hdnProvincia").val() != null)
            Common.obtenerCiudades("ddlCiudad", $("#hdnCiudad").val(), $("#hdnProvincia").val(), true);
        $("#hdnCiudad").val("");

        $('#flpArchivo').fileupload({
            url: '/subirImagenes.ashx?IDUsuario=' + $("#IDusuario").val() + "&opcionUpload=LogoEmpresas",
            success: function (response, status) {
                if (response.name != "ERROR") {

                    //MisDatos.ActualizarLogoSesion(response.name);
                    $("#hdnFileName").val(response.name);
                    var aux = new Date();
                    $("#imgLogo").attr("src", "/files/usuarios/" + response.name + "?" + aux.getTime());//agrego fecha para cuando se actualiza el logo
                    $("#imgLogo")
                    $("#divError").hide();
                    $("#divOk").show();
                    $("#divLogo").slideToggle();
                    $("#hdnTieneFoto").val("1");
                    foto.showBtnEliminar();
                }
                else {
                    $("#hdnFileName").val("");
                    $("#msgError").html("logo no ingresado");
                    $("#divError").show();
                    $("#divOk").hide();
                }
            },
            error: function (error) {
                $("#hdnFileName").val("");
                $("#msgError").html(error.responseText);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');

            }
        });
        //Common.configTelefono("txtTelefono");
        // Date Picker
        //jQuery('.validDate').datepicker();
        Common.configDatePicker();

        $("#txtNroDocumento, #txtNuevoPunto").numericInput();
        $("#txtArea,#txtTelefono").numericInput();

        // Validation with select boxes
        $("#frmEdicion").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        // Validation with select boxes
        $("#modalPrimerAviso").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.validator.addMethod("validCuitMisDatos", function (value, element) {
            return CuitEsValido($("#txtCuit").val());
        }, "CUIT Inválido");
        MisDatos.obtenerHistorialDePagos();

        if ($("#hdnAccion").val() == "completaTusDatos") {
            $("#liTemplate,#liportalClientes,#liDatosFiscales,#liCambiarpwd,#Usuarios,#liEmpresas,#liplanPagos,#liConfiguracion").hide()
        }
        else if ($("#hdnAccion").val() == "preferencias") {
            $("#liDatosPrincipales,#liDomicilio,#divFotoPerfil,#liCambiarpwd,#Usuarios,#liEmpresas").hide()
            $("#liTemplate,#liportalClientes,#liDatosFiscales").show()

            $("#divDatosPersonales").removeClass("col-sm-9").addClass("col-sm-12");
            $("#liDatosPrincipales").removeClass("active");
            $("#liportalClientes").addClass("active");
            $("#info").removeClass("tab-pane active").addClass("tab-pane");
            $("#portalClientes").removeClass("tab-pane").addClass("tab-pane active");

            $("#spanMisDatosUbicacion").html("Preferencias");
            $("#spanMisDatos").html("<i class='fa fa-user'></i> Preferencias");

            if (MI_CONDICION == "MO") {
                $("#liDatosFiscales").hide();
            }
        }
        MisDatos.changeIIBB();
        foto.showBtnEliminar();

        MisDatos.habilitarPrimerAviso();
        MisDatos.habilitarSegundoAviso();
        MisDatos.habilitarTercerAviso();
        MisDatos.habilitarEnvioFE();
        MisDatos.habilitarEnvioCR();
        MisDatos.ObtenerAvisosVencimientos();

        $("#txtDiasPrimerAviso,#txtDiasSegundoAviso,#txtDiasTercerAviso").numericInput();

        var CtasVentas = $("#hdnCuentasVentas").val().split(",");
        var CtasCompras = $("#hdnCuentasCompras").val().split(",");
        $("#ddlCuentasVentas").val(CtasVentas);
        $("#ddlCuentasCompras").val(CtasCompras);
        $("#ddlCuentasVentas").trigger("change");
        $("#ddlCuentasCompras").trigger("change");


        var jurisdicciones = $("#hdnJuresdiccion").val().split(",");
        Common.obtenerProvincias("ddlJuresdiccion", jurisdicciones, 10, true);
        
    },
    changeProvincia: function () {
        if ($("#hdnCiudad").val() == "")
            Common.obtenerCiudades("ddlCiudad", $("#ddlCiudad").val(), $("#ddlProvincia").val(), true);
        // $("#hdnCiudad").val("");
    },
    ActualizarLogoSesion: function (nombre) {

        var info = "{ Nombrelogo: '" + nombre + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/mis-datos.aspx/ActualizarLogoSesion",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    grabar: function () {
        MisDatos.ocultarMensajes();

        if ($('#frmEdicion').valid()) {

            Common.mostrarProcesando("btnActualizarInfo");
            Common.mostrarProcesando("btnActualizarDomicilio");
            Common.mostrarProcesando("btnActualizarDatosFiscales");

            var jurisdicciones = ($("#ddlJuresdiccion").val() != null && $("#ddlJuresdiccion").val() != "") ? $("#ddlJuresdiccion").val() : "";
            var info = "{ razonSocial: '" + $("#txtRazonSocial").val()
                    + "', condicionIva: '" + $("#ddlCondicionIva").val()
                    + "', cuit: '" + $("#txtCuit").val()
                    + "', iibb: '" + $("#txtIIBB").val()
                    + "', fechaInicio: '" + $("#txtFechaInicioAct").val()
                    + "', personeria: '" + $("#ddlPersoneria").val()
                    + "', email: '" + $("#txtEmail").val()
                    + "', emailFrom: '" + $("#txtEmailFrom").val()
                    + "', emailAlertas: '" + $("#txtEmailAlertas").val()
                    + "', telefono: '" + $("#txtArea").val() + "-" + $("#txtTelefono").val()
                    + "', celular: '" + $("#txtCelular").val()
                    + "', contacto: '" + $("#txtContacto").val()
                    + "', idProvincia: '" + $("#ddlProvincia").val()
                    + "', idCiudad: '" + $("#ddlCiudad").val()
                    + "', domicilio: '" + $("#txtDomicilio").val()
                    + "', pisoDepto: '" + $("#txtPisoDepto").val()
                    + "', cp: '" + $("#txtCp").val()
                    + "', esAgentePersepcionIVA: " + $("#esAgentePersepcionIVA").is(':checked')
                    + " , esAgentePersepcionIIBB: " + $("#esAgentePersepcionIIBB").is(':checked')
                    + " , esAgenteRetencion: " + $("#esAgenteRetencion").is(':checked')
                    + " , exentoIIBB: " + $("#chkExento").is(':checked')
                    + " , fechaCierreContable: '" + $("#txtFechaCierreContable").val()
                    + "' , fechaLimiteCarga: '" + $("#txtFechaLimiteCarga").val()
                    + "', idJurisdiccion: '" + jurisdicciones
                    + " '}";

            $.ajax({
                type: "POST",
                url: "/modulos/seguridad/mis-datos.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizarInfo", "Actualizar");
                    Common.ocultarProcesando("btnActualizarDomicilio", "Actualizar");
                    Common.ocultarProcesando("btnActualizarDatosFiscales", "Actualizar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizarInfo", "Actualizar");
                    Common.ocultarProcesando("btnActualizarDomicilio", "Actualizar");
                    Common.ocultarProcesando("btnActualizarDatosFiscales", "Actualizar");
                }
            });
        }
        else {
            return false;t
        }
    },
    eliminarPunto: function (id) {
        MisDatos.ocultarMensajes();

        var info = "{ punto: " + parseInt(id) + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/mis-datos.aspx/eliminarPunto",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                MisDatos.obtenerPuntos();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorPuntos").html(r.Message);
                $("#divErrorPuntos").show();
            }
        });
    },
    habilitarPuntoVenta: function (id) {
        var info = "{ id: " + id + " }";
        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/mis-datos.aspx/HabilitarPuntoVenta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                MisDatos.obtenerPuntos();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorPuntos").html(r.Message);
                $("#divErrorPuntos").show();
            }
        });
    },
    agregarPunto: function () {
        MisDatos.ocultarMensajes();

        if ($("#txtNuevoPunto").val() != "") {

            Common.mostrarProcesando("btnActualizarPunto");
            var info = "{ punto: " + parseInt($("#txtNuevoPunto").val()) + "}";

            $.ajax({
                type: "POST",
                url: "/modulos/seguridad/mis-datos.aspx/agregarPunto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    MisDatos.obtenerPuntos();
                    Common.ocultarProcesando("btnActualizarPunto", "Agregar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorPuntos").html(r.Message);
                    $("#divErrorPuntos").show();
                    Common.ocultarProcesando("btnActualizarPunto", "Agregar");
                }
            });

        }
        else {
            $("#msgErrorPuntos").html("Debes ingresar un valor");
            $("#divErrorPuntos").show();
        }
    },
    obtenerPuntos: function () {
        $.ajax({
            type: "GET",
            url: "/modulos/seguridad/mis-datos.aspx/obtenerPuntos",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
            }
        });
    },
    ponerPorDefecto: function (idPunto) {
        $.ajax({
            type: "POST",
            data: "{ idPunto: " + idPunto + " }",
            url: "/modulos/seguridad/mis-datos.aspx/GuardarPorDefecto",
            //data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
            }
        });
    },
    ponerPorDefectoIntegraciones: function (idPunto) {
        $.ajax({
            type: "POST",
            data: "{ idPunto: " + idPunto + " }",
            url: "/modulos/seguridad/mis-datos.aspx/GuardarPorDefectoIntegraciones",
            //data: "{idFactura: " + parseInt(dataItem.ID) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
            }
        });
    },
    ocultarMensajes: function () {
        $("#divError, #divOk, #divErrorPuntos").hide();
    },
    verificarClientes: function () {
        if ($("#txtClientSecret").val() == "" && $("#txtClientId").val() != "") {
            $("#msgError").html("Si engresa El client ID debe ingresar el Client Secret");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        if ($("#txtClientId").val() == "" && $("#txtClientSecret").val() != "") {
            $("#msgError").html("Si engresa El Client Secret, debe ingresar el clientID");
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }

        return true;
    },
    portalClientes: function () {
        MisDatos.ocultarMensajes();
        if (MisDatos.verificarClientes()) {


            var info = "{ ChkCorreoPortal: " + $("#ChkCorreoPortal").is(':checked')
                     + ", chkPortalClientes: " + $("#chkPortalClientes").is(':checked')
                     + ", clientId: '" + $("#txtClientId").val()
                     + "', clientSecret: '" + $("#txtClientSecret").val()
                     + "'}";

            Common.mostrarProcesando("btnActualizarPortalClientes");
            $.ajax({
                type: "POST",
                url: "/modulos/seguridad/mis-datos.aspx/portalClientes",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizarPortalClientes", "Actualizar");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizarPortalClientes", "Actualizar");
                }
            });
        }
    },
    ActualizarTemplate: function () {
        MisDatos.ocultarMensajes();
        Common.mostrarProcesando("btnActualizarTemplate");
        var info = "{ ddlTemplate: '" + $("input[name='ctl00$MainContent$dlltemplate']:checked").attr('value') + "'}";

        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/mis-datos.aspx/ActualizarTemplate",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                Common.ocultarProcesando("btnActualizarTemplate", "Actualizar");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                Common.ocultarProcesando("btnActualizarTemplate", "Actualizar");
            }
        });
    },
    obtenerHistorialDePagos: function () {
        $("#resultsContainer").html("");
        $.ajax({
            type: "GET",
            url: "/modulos/seguridad/mis-datos.aspx/ObtenerHistorialPagos",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                if (data.d.Items.length > 0) {
                    $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");

                    $("#hdnIdPlan").val(data.d.IDPlanActual);
                    $("#spNombrePlan").html(data.d.NombrePlanActual);
                    $("#spFechaPlan").html(data.d.FechaVencimiento);
                }
                else
                    $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });

    },
    upgradePlanActual: function () {
        window.location.href = "/modulos/seguridad/elegir-plan.aspx?upgrade=1";
    },
    changeIIBB: function () {
        if ($("#chkExento").is(':checked')) {
            $("#txtIIBB").val("");
            $("#txtIIBB").attr("disabled", "true");
        }
        else {
            $("#txtIIBB").removeAttr("disabled")
        }
    },
    guardarConfiguracion: function (id) {
        $("#divErrorConfiguracion,#divError, #divOk").hide();
        var precioUnitario = ($("input[name='ctl00$MainContent$rPrecioUnitario']:checked").attr('value') == "1") ? true : false;
        var venderSinStock = ($("input[name='ctl00$MainContent$rVenderStock']:checked").attr('value') == "1") ? true : false;

        var info = "{ cantidadDecimales: " + $("#ddlCantidadDecimales").val()
            + ", usaPrecioUnitarioConIva: " + precioUnitario
            + ", venderSinStock: " + venderSinStock
            + ", obsFc: '" + $("#txtObservacionesFc").val() + "'"
            + ", obsRemito: '" + $("#txtObservacionesRemito").val() + "'"
            + ", obsPresup: '" + $("#txtObservacionesPresup").val() + "'"
            + ", obsOrdenes: '" + $("#txtObservacionesOrdenes").val() + "'"
            + " }";

        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/mis-datos.aspx/guardarConfiguracion",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divOk").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    guardarConfiguracionPlanDeCuenta: function () {
        $("#divErrorPlanDeCuenta,#divError, #divOk").hide();

        //Compras
        var IDCtaProveedores = ($("#ddlCtaProveedoresComprobante").val() == "" || $("#ddlCtaProveedoresComprobante").val() == null) ? 0 : parseInt($("#ddlCtaProveedoresComprobante").val());
        var IDCtaIVACreditoFiscal = ($("#ddlIVACreditoFiscalComprobante").val() == "" || $("#ddlIVACreditoFiscalComprobante").val() == null) ? 0 : parseInt($("#ddlIVACreditoFiscalComprobante").val());
        var IDCtaConceptosNoGravadosxCompras = ($("#ddlNoGravadoCompras").val() == "" || $("#ddlNoGravadoCompras").val() == null) ? 0 : parseInt($("#ddlNoGravadoCompras").val());

        //Pagos
        var IDCtaCaja = ($("#ddlCaja").val() == "" || $("#ddlCaja").val() == null) ? 0 : parseInt($("#ddlCaja").val());

        //Percepciones
        var IDPercIIBB = ($("#ddlPercIIBB").val() == "" || $("#ddlPercIIBB").val() == null) ? 0 : parseInt($("#ddlPercIIBB").val());
        var IDPercIVA = ($("#ddlPercIVA").val() == "" || $("#ddlPercIVA").val() == null) ? 0 : parseInt($("#ddlPercIVA").val());
        var IDPercIIBBSufrida = ($("#ddlPercIIBBSufrida").val() == "" || $("#ddlPercIIBBSufrida").val() == null) ? 0 : parseInt($("#ddlPercIIBBSufrida").val());
        var IDPercIVASufrida = ($("#ddlPercIVASufrida").val() == "" || $("#ddlPercIVASufrida").val() == null) ? 0 : parseInt($("#ddlPercIVASufrida").val());


        //Retenciones
        var IDRetIIBB = ($("#ddlRetIIBB").val() == "" || $("#ddlRetIIBB").val() == null) ? 0 : parseInt($("#ddlRetIIBB").val());
        var IDRetIVA = ($("#ddlRetIVA").val() == "" || $("#ddlRetIVA").val() == null) ? 0 : parseInt($("#ddlRetIVA").val());
        var IDRetGanancias = ($("#ddlRetGanancias").val() == "" || $("#ddlRetGanancias").val() == null) ? 0 : parseInt($("#ddlRetGanancias").val());
        var IDRetSUSS = ($("#ddlRetSUSS").val() == "" || $("#ddlRetSUSS").val() == null) ? 0 : parseInt($("#ddlRetSUSS").val());

        var IDRetIIBBSufrida = ($("#ddlRetIIBBSufridas").val() == "" || $("#ddlRetIIBBSufridas").val() == null) ? 0 : parseInt($("#ddlRetIIBBSufridas").val());
        var IDRetIVASufrida = ($("#ddlRetIVASufridas").val() == "" || $("#ddlRetIVASufridas").val() == null) ? 0 : parseInt($("#ddlRetIVASufridas").val());
        var IDRetGananciasSufrida = ($("#ddlRetGananciasSufridas").val() == "" || $("#ddlRetGananciasSufridas").val() == null) ? 0 : parseInt($("#ddlRetGananciasSufridas").val());
        var IDRetSUSSSufrida = ($("#ddlRetSUSSSufridas").val() == "" || $("#ddlRetSUSSSufridas").val() == null) ? 0 : parseInt($("#ddlRetSUSSSufridas").val());

        //Bancos
        var IDCtaBancos = ($("#ddlBanco").val() == "" || $("#ddlBanco").val() == null) ? 0 : parseInt($("#ddlBanco").val());
        var IDCtaValoresADepositar = ($("#ddlValoresADepositar").val() == "" || $("#ddlValoresADepositar").val() == null) ? 0 : parseInt($("#ddlValoresADepositar").val());
        var IDCtaChequesDif = ($("#ddlChequesDif").val() == "" || $("#ddlChequesDif").val() == null) ? 0 : parseInt($("#ddlChequesDif").val());
        var IDCtaSircreb = ($("#ddlSircreb").val() == "" || $("#ddlSircreb").val() == null) ? 0 : parseInt($("#ddlSircreb").val());
        var IDCtaCreditoComputable = ($("#ddlCreditoComputable").val() == "" || $("#ddlCreditoComputable").val() == null) ? 0 : parseInt($("#ddlCreditoComputable").val());
        var IDCtaComisiones = ($("#ddlComisiones").val() == "" || $("#ddlComisiones").val() == null) ? 0 : parseInt($("#ddlComisiones").val());
        var IDCtaOtrosGastosBancarios = ($("#ddlOtros").val() == "" || $("#ddlOtros").val() == null) ? 0 : parseInt($("#ddlOtros").val());
        var IDCtaImpDebCred = ($("#ddlImpDebCred").val() == "" || $("#ddlImpDebCred").val() == null) ? 0 : parseInt($("#ddlImpDebCred").val());
        var IDCtaGastosBancarios = ($("#ddlGastos").val() == "" || $("#ddlGastos").val() == null) ? 0 : parseInt($("#ddlGastos").val());

        var IDCtaInteresBancos = ($("#ddlInteresBanco").val() == "" || $("#ddlInteresBanco").val() == null) ? 0 : parseInt($("#ddlInteresBanco").val());
        var IDCtaImpSellos = ($("#ddlImpSellos").val() == "" || $("#ddlImpSellos").val() == null) ? 0 : parseInt($("#ddlImpSellos").val());

        //ventas
        var IDCtaIVADebitoFiscal = ($("#ddlIVADebitoFiscal").val() == "" || $("#ddlIVADebitoFiscal").val() == null) ? 0 : parseInt($("#ddlIVADebitoFiscal").val());
        var IDCtaDeudoresPorVentas = ($("#ddlDeudoresPorVenta").val() == "" || $("#ddlDeudoresPorVenta").val() == null) ? 0 : parseInt($("#ddlDeudoresPorVenta").val());
        var IDCtaConceptosNoGravadosxVentas = ($("#ddlNoGravadoVentas").val() == "" || $("#ddlNoGravadoVentas").val() == null) ? 0 : parseInt($("#ddlNoGravadoVentas").val());

        var IDAnticipoClientes = ($("#ddlAnticipoClientes").val() == "" || $("#ddlAnticipoClientes").val() == null) ? 0 : parseInt($("#ddlAnticipoClientes").val());
        var IDAnticipoProveedores = ($("#ddlAnticipoProveedores").val() == "" || $("#ddlAnticipoProveedores").val() == null) ? 0 : parseInt($("#ddlAnticipoProveedores").val());
        var IDCmv = ($("#ddlCMV").val() == "" || $("#ddlCMV").val() == null) ? 0 : parseInt($("#ddlCMV").val());

        var info = "{ IDCtaProveedores: " + IDCtaProveedores
                 + " ,IDCtaIVACreditoFiscal: " + IDCtaIVACreditoFiscal

                 + " ,IDPercIIBB: " + IDPercIIBB
                 + " ,IDPercIVA: " + IDPercIVA
                 + " ,IDPercIIBBSufrida: " + IDPercIIBBSufrida
                 + " ,IDPercIVASufrida: " + IDPercIVASufrida

                 + " ,IDCtaBancos: " + IDCtaBancos
                 + " ,IDCtaCaja: " + IDCtaCaja
                 + " ,IDCtaValoresADepositar: " + IDCtaValoresADepositar
                 + " ,IDCtaChequesDif: " + IDCtaChequesDif

                 + " ,IDCtaSircreb: " + IDCtaSircreb
                 + " ,IDCtaCreditoComputable: " + IDCtaCreditoComputable
                 + " ,IDCtaComisiones: " + IDCtaComisiones
                 + " ,IDCtaOtrosGastosBancarios: " + IDCtaOtrosGastosBancarios
                 + " ,IDCtaImpDebCred: " + IDCtaImpDebCred
                 + " ,IDCtaGastos: " + IDCtaGastosBancarios
                 + " ,IDCtaInteresBancos: " + IDCtaInteresBancos
                 + " ,IDCtaImpSellos: " + IDCtaImpSellos

                 + " ,IDRetIIBB: " + IDRetIIBB
                 + " ,IDRetIVA: " + IDRetIVA
                 + " ,IDRetGanancias: " + IDRetGanancias
                 + " ,IDRetSUSS: " + IDRetSUSS
                 + " ,IDRetIIBBSufrida: " + IDRetIIBBSufrida
                 + " ,IDRetIVASufrida: " + IDRetIVASufrida
                 + " ,IDRetGananciasSufrida: " + IDRetGananciasSufrida
                 + " ,IDRetSUSSSufrida: " + IDRetSUSSSufrida

                 + " ,IDCtaIVADebitoFiscal: " + IDCtaIVADebitoFiscal
                 + " ,IDCtaDeudoresPorVentas: " + IDCtaDeudoresPorVentas
                 + " ,ctasFiltrosCompras: '" + $("#ddlCuentasCompras").val()
                 + "',ctasFiltrosVentas: '" + $("#ddlCuentasVentas").val()
                 + "',IDCtaConceptosNoGravadosxCompras: " + IDCtaConceptosNoGravadosxCompras
                 + " ,IDCtaConceptosNoGravadosxVentas: " + IDCtaConceptosNoGravadosxVentas
                 + " ,IDAnticipoClientes: " + IDAnticipoClientes
                 + " ,IDAnticipoProveedores: " + IDAnticipoProveedores
                 + " ,IDCmv: " + IDCmv
                 + " }";

        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/mis-datos.aspx/guardarConfiguracionPlanDeCuenta",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divOkPlanDeCuenta").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorPlanDeCuenta").html(r.Message);
                $("#divErrorPlanDeCuenta").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    //*** AvisosVencimientos***/
    guardarAlertasyAvisos: function () {
        $("#divOkAlertasyAvisos,#divErrorAlertasyAvisos,#divError, #divOk").hide();
        var modoPrimeAviso = ($("#rPrimerAvisoAntes").is(":checked") == true) ? "1" : "0";
        var modoSegundoAviso = ($("#rSegundoAvisoAntes").is(":checked") == true) ? "1" : "0";
        var modoTercerAviso = ($("#rTercerAvisoAntes").is(":checked") == true) ? "1" : "0";

        var diasPrimeAviso = ($("#txtDiasPrimerAviso").val() == "" || $("#txtDiasPrimerAviso").val() == null) ? "0" : parseInt($("#txtDiasPrimerAviso").val())
        var diasSegundoAviso = ($("#txtDiasSegundoAviso").val() == "" || $("#txtDiasSegundoAviso").val() == null) ? "0" : parseInt($("#txtDiasSegundoAviso").val())
        var diasTercerAviso = ($("#txtDiasTercerAviso").val() == "" || $("#txtDiasTercerAviso").val() == null) ? "0" : parseInt($("#txtDiasTercerAviso").val())

        if (MisDatos.validarAvisosVencimientos() == false) {
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }

        var datos = new Array();
        var obj = new Object();

        obj.TipoAlerta = "Primer aviso";
        obj.Activa = $("#chkPrimerAviso").is(":checked");
        obj.ModoDeEnvio = modoPrimeAviso;
        obj.CantDias = diasPrimeAviso;
        obj.Asunto = $("#txtAsuntoPrimerAviso").val();
        obj.Mensaje = tinymce.get("txtMensajePrimerAviso").getContent();
        datos.push(obj);
        var obj = new Object();
        obj.TipoAlerta = "Segundo aviso";
        obj.Activa = $("#chkSegundoAviso").is(":checked");
        obj.ModoDeEnvio = modoSegundoAviso;
        obj.CantDias = diasSegundoAviso;
        obj.Asunto = $("#txtAsuntoSegundoAviso").val();
        obj.Mensaje = tinymce.get("txtMensajeSegundoAviso").getContent();
        datos.push(obj);
        var obj = new Object();
        obj.TipoAlerta = "Tercer aviso";
        obj.Activa = $("#chkTercerAviso").is(":checked");
        obj.ModoDeEnvio = modoTercerAviso;
        obj.CantDias = diasTercerAviso;
        obj.Asunto = $("#txtAsuntoTercerAviso").val();
        obj.Mensaje = tinymce.get("txtMensajeTercerAviso").getContent();
        datos.push(obj);

        var obj = new Object();
        obj.TipoAlerta = "Envio FE";
        obj.Activa = $("#chkEnvioFE").is(":checked");
        obj.ModoDeEnvio = 0;
        obj.CantDias = 0;
        obj.Asunto = $("#txtAsuntoEnvioFE").val();

        obj.Mensaje = tinymce.get("txtMensajeEnvioFE").getContent();
        datos.push(obj);

        var obj = new Object();
        obj.TipoAlerta = "Envio CR";
        obj.Activa = $("#chkEnvioCR").is(":checked");
        obj.ModoDeEnvio = 0;
        obj.CantDias = 0;
        obj.Asunto = $("#txtAsuntoEnvioCR").val();
        obj.Mensaje = tinymce.get("txtMensajeEnvioCR").getContent();
        datos.push(obj);

        var obj = new Object();
        obj.TipoAlerta = "Stock";
        obj.Activa = $("#chkStock").is(":checked");

        obj.ModoDeEnvio = 0;
        obj.CantDias = 0;
        obj.Asunto = "";
        obj.Mensaje = "";
        datos.push(obj);

        $.ajax({
            type: "POST",
            url: "/modulos/seguridad/mis-datos.aspx/GuardarAlertasyAvisos",
            data: "{listaAvisos: " + JSON.stringify(datos) + ", emailStock:'"+ $("#txtEmailStock").val()+"'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#divOk").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    guardarAsuntoYMensaje: function () {
        if ($("#frmNuevaAlertasyAvisos").valid()) {
            $('#modalAlertasYAvisos').modal('toggle');
        } else {
            return false;
        }
    },
    habilitarPrimerAviso: function () {
        if ($(".divPrimerAviso :input").is(":disabled")) {
            $(".divPrimerAviso :input").attr("disabled", false);
        }
        else {
            $(".divPrimerAviso :input").attr("disabled", true);
            //$("#rPrimerAvisoAntes,#rPrimerAvisoDespues").attr("checked", false);
            //$("#txtDiasPrimerAviso,#txtAsuntoPrimerAviso,#txtMensajePrimerAviso").val("");
        }
    },
    habilitarSegundoAviso: function () {
        if ($(".divSegundoAviso :input").is(":disabled")) {
            $(".divSegundoAviso :input").attr("disabled", false);
        }
        else {
            $(".divSegundoAviso :input").attr("disabled", true);
            //$("#rSegundoAvisoAntes,#rSegundoAvisoDespues").attr("checked", false);
            //$("#txtDiasSegundoAviso,#txtAsuntoSegundoAviso,#txtMensajeSegundoAviso").val("");
        }
    },
    habilitarTercerAviso: function () {
        if ($(".divTercerAviso :input").is(":disabled")) {
            $(".divTercerAviso :input").attr("disabled", false);
        }
        else {
            $(".divTercerAviso :input").attr("disabled", true);
            //$("#rTercerAvisoAntes,#rTercerAvisoDespues").attr("checked", false);
            //$("#txtDiasTercerAviso,#txtAsuntoTercerAviso,#txtMensajeTercerAviso").val("");
        }
    },
    habilitarEnvioFE: function () {
        if ($(".divEnvioFE :input").is(":disabled")) {
            $(".divEnvioFE :input").attr("disabled", false);
        }
        else {
            $(".divEnvioFE :input").attr("disabled", true);
            //$("#txtAsuntoEnvioFE,#txtMensajeEnvioFE").val("");
        }
    },
    habilitarEnvioCR: function () {
        if ($(".divEnvioCR :input").is(":disabled")) {
            $(".divEnvioCR :input").attr("disabled", false);
        }
        else {
            $(".divEnvioCR :input").attr("disabled", true);
            //$("#txtAsuntoEnvioCR,#txtMensajeEnvioCR").val("");
        }
    },
    configurarMensaje: function (tipoMensaje) {
        if (tipoMensaje == "divPrimerAviso") {
            $("#modalPrimerAviso").show();
            $("#modalSegundoAviso,#modalTercerAviso,#modalEnvioCR,#modalEnvioFE").hide();
            $("#litModalOkTitulo").html("Configuración: 1er. Aviso");
        }
        else if (tipoMensaje == "divSegundoAviso") {
            $("#modalSegundoAviso").show();
            $("#modalPrimerAviso,#modalTercerAviso,#modalEnvioCR,#modalEnvioFE").hide();
            $("#litModalOkTitulo").html("Configuración: 2er. Aviso");
        }
        else if (tipoMensaje == "divTercerAviso") {
            $("#modalTercerAviso").show();
            $("#modalPrimerAviso,#modalSegundoAviso,#modalEnvioCR,#modalEnvioFE").hide();
            $("#litModalOkTitulo").html("Configuración: 3er. Aviso");
        }
        else if (tipoMensaje == "divEnvioFE") {
            $("#modalEnvioFE").show();
            $("#modalPrimerAviso,#modalSegundoAviso,#modalTercerAviso,#modalEnvioCR").hide();
            $("#litModalOkTitulo").html("Configuración: envío automático comprobantes");
        }
        else if (tipoMensaje == "divEnvioCR") {
            $("#modalEnvioCR").show();
            $("#modalPrimerAviso,#modalSegundoAviso,#modalTercerAviso,#modalEnvioFE").hide();
            $("#litModalOkTitulo").html("Configuración: envío automático de recibos");
        }

        $('#modalAlertasYAvisos').modal('toggle');

    },
    validarAvisosVencimientos: function () {
        if ($("#chkPrimerAviso").is(":checked")) {
            if ( $("#txtAsuntoPrimerAviso").val() == "" ||tinymce.get("txtMensajePrimerAviso").getContent()=="" || $("#txtDiasPrimerAviso").val() == "0" || $("#txtDiasPrimerAviso").val() == null || $("#txtDiasPrimerAviso").val() == "" || (!$("#rPrimerAvisoAntes").is(":checked") && !$("#rPrimerAvisoDespues").is(":checked"))) {
                $("#msgError").html("Debe configurar todos los parámetros del primer aviso.");
                return false;
            }
        }
        if ($("#chkSegundoAviso").is(":checked")) {
            if ($("#txtAsuntoSegundoAviso").val() == "" ||tinymce.get("txtMensajeSegundoAviso").getContent()=="" || $("#txtDiasSegundoAviso").val() == "0" || $("#txtDiasSegundoAviso").val() == null || $("#txtDiasSegundoAviso").val() == "" || (!$("#rSegundoAvisoAntes").is(":checked") && !$("#rSegundoAvisoDespues").is(":checked"))) {
                $("#msgError").html("Debe configurar todos los parámetros del segundo aviso.");
                return false;
            }
        }
        if ($("#chkTercerAviso").is(":checked")) {
            if ($("#txtAsuntoTercerAviso").val() == "" || tinymce.get("txtMensajeTercerAviso").getContent()=="" || $("#txtDiasTercerAviso").val() == "0" || $("#txtDiasTercerAviso").val() == null || $("#txtDiasTercerAviso").val() == "" || (!$("#rTercerAvisoAntes").is(":checked") && !$("#rTercerAvisoDespues").is(":checked"))) {
                $("#msgError").html("Debe configurar todos los parámetros del tercer aviso.");
                return false;
            }
        }
        if ($("#chkEnvioFE").is(":checked")) {
            if ($("#txtAsuntoEnvioFE").val() == "" ||tinymce.get("txtMensajeEnvioFE").getContent()=="") {
                $("#msgError").html("Debe configurar todos los parámetros para el envío automático de facturas electrónicas.");
                return false;
            }
        }
        if ($("#chkEnvioCR").is(":checked")) {
            if ($("#txtAsuntoEnvioCR").val() == "" ||tinymce.get("txtMensajeEnvioCR").getContent()=="") {
                $("#msgError").html("Debe configurar todos los parámetros para el envío automático de las cobranzas.");
                return false;
            }
        }
        return true;
    },
    ObtenerAvisosVencimientos: function () {
        $.ajax({
            type: "GET",
            url: "/modulos/seguridad/mis-datos.aspx/ObtenerAvisosVencimientos",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    if (data.d[i].TipoAlerta == "Primer aviso") {
                        if (data.d[i].ModoDeEnvio == "1") {
                            $("#rPrimerAvisoAntes").attr("checked", true);
                        }
                        else {
                            $("#rPrimerAvisoDespues").attr("checked", true);
                        }

                        $("#txtDiasPrimerAviso").val(data.d[i].CantDias);
                        $("#txtAsuntoPrimerAviso").val(data.d[i].Asunto);
                        $("#txtMensajePrimerAviso").val(data.d[i].Mensaje);

                        if (data.d[i].Activa) {
                            $("#chkPrimerAviso").attr("checked", true);
                            MisDatos.habilitarPrimerAviso();
                        }
                        tinymce.init({
                            selector: "#txtMensajePrimerAviso",
                            plugins: [
                            "advlist autolink lists link image preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media contextmenu paste"
                            ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                        });

                    }
                    else if (data.d[i].TipoAlerta == "Segundo aviso") {
                        if (data.d[i].ModoDeEnvio == "1") {
                            $("#rSegundoAvisoAntes").attr("checked", true);
                        }
                        else {
                            $("#rSegundoAvisoDespues").attr("checked", true);
                        }

                        $("#txtDiasSegundoAviso").val(data.d[i].CantDias);
                        $("#txtAsuntoSegundoAviso").val(data.d[i].Asunto);
                        $("#txtMensajeSegundoAviso").val(data.d[i].Mensaje);
                        if (data.d[i].Activa) {
                            $("#chkSegundoAviso").attr("checked", true);
                            MisDatos.habilitarSegundoAviso();
                        }
                        tinymce.init({
                            selector: "#txtMensajeSegundoAviso",
                            plugins: [
                            "advlist autolink lists link image preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media contextmenu paste"
                            ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                        });
                    }
                    else if (data.d[i].TipoAlerta == "Tercer aviso") {
                        if (data.d[i].ModoDeEnvio == "1") {
                            $("#rTercerAvisoAntes").attr("checked", true);
                        } else {
                            $("#rTercerAvisoDespues").attr("checked", true); 
                        }

                        $("#txtDiasTercerAviso").val(data.d[i].CantDias);
                        $("#txtAsuntoTercerAviso").val(data.d[i].Asunto);
                        $("#txtMensajeTercerAviso").val(data.d[i].Mensaje);

                        if (data.d[i].Activa) {
                            $("#chkTercerAviso").attr("checked", true);
                            MisDatos.habilitarTercerAviso();
                        }
                        tinymce.init({
                            selector: "#txtMensajeTercerAviso",
                            plugins: [
                            "advlist autolink lists link image preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media contextmenu paste"
                            ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                        });
                    }
                    else if (data.d[i].TipoAlerta == "Envio FE") {
                        $("#txtAsuntoEnvioFE").val(data.d[i].Asunto);
                        $("#txtMensajeEnvioFE").val(data.d[i].Mensaje);
                        if (data.d[i].Activa) {
                            $("#chkEnvioFE").attr("checked", true);
                            MisDatos.habilitarEnvioFE();
                        }
                        tinymce.init({
                            selector: "#txtMensajeEnvioFE",
                            plugins: [
                            "advlist autolink lists link image preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media contextmenu paste"
                            ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                        });
                       
                    }
                    else if (data.d[i].TipoAlerta == "Envio CR") {
                        $("#txtAsuntoEnvioCR").val(data.d[i].Asunto);
                        $("#txtMensajeEnvioCR").val(data.d[i].Mensaje);
                        if (data.d[i].Activa) {
                            $("#chkEnvioCR").attr("checked", true);
                            MisDatos.habilitarEnvioCR();
                        }
                        tinymce.init({
                            selector: "#txtMensajeEnvioCR",
                            plugins: [
                            "advlist autolink lists link image preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media contextmenu paste"
                            ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                        });
                    }
                    else if (data.d[i].TipoAlerta == "Stock") {
                        $("#chkStock").attr("checked", data.d[i].Activa);
                    }
                }
                if ($("#txtMensajePrimerAviso").val() === "") {

                    tinymce.init({
                        selector: "#txtMensajePrimerAviso",
                        plugins: [
                        "advlist autolink lists link image preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media contextmenu paste"
                        ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                    });
                }
                if ($("#txtMensajeSegundoAviso").val() === "") {

                    tinymce.init({
                        selector: "#txtMensajeSegundoAviso",
                        plugins: [
                        "advlist autolink lists link image preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media contextmenu paste"
                        ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                    });
                }
                if ($("#txtMensajeTercerAviso").val() === "") {

                    tinymce.init({
                        selector: "#txtMensajeTercerAviso",
                        plugins: [
                        "advlist autolink lists link image preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media contextmenu paste"
                        ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                    });
                }
                if ($("#txtMensajeEnvioFE").val() === "") {
                  
                    tinymce.init({
                        selector: "#txtMensajeEnvioFE",
                        plugins: [
                        "advlist autolink lists link image preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media contextmenu paste"
                        ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                    });
                }
                if ($("#txtMensajeEnvioCR").val() === "") {

                    tinymce.init({
                        selector: "#txtMensajeEnvioCR",
                        plugins: [
                        "advlist autolink lists link image preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media contextmenu paste"
                        ], toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview code", menubar: "", invalid_elements: "script,object,embed,style,form,input,iframe, code"
                    });
                }
                
                
              
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
            }
        });

    },
    cancelarAsuntoYMensaje: function () {
        $('#modalAlertasYAvisos').modal('toggle');
    },
}

var Empresa = {
    configForm: function () {
        // Validation with select boxes
        $("#frmNuevaEmpresa").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.validator.addMethod("validCuit", function (value, element) {
            return CuitEsValido($("#txtCuitEmpresa").val());
        }, "CUIT Inválido");

        $("#txtCuitEmpresa").numericInput();
        Common.obtenerProvincias("ddlProvinciaEmpresa", "", 10, true);
    },
    grabar: function () {
        if ($('#frmNuevaEmpresa').valid() && $("#ddlCondicionIvaEmpresa").valid() && $("#txtEmailEmpresa").valid() && $("#ddlProvinciaEmpresa").valid() && $("#ddlCiudadEmpresa").valid() && $("#txtDomicilioEmpresa").valid() && $("#txtPwdEmpresa").valid()) {
            $("#divErrorEmpresa").hide();
            Common.mostrarProcesando("btnCrearEmpresa");

            var info = "{ razonSocial: '" + $("#txtRazonSocialEmpresa").val()
                    + "', condicionIva: '" + $("#ddlCondicionIvaEmpresa").val()
                    + "', cuit: '" + $("#txtCuitEmpresa").val()
                    + "', personeria: '" + $("#ddlPersoneriaEmpresa").val()
                    + "', email: '" + $("#txtEmailEmpresa").val()
                    + "', pwd: '" + $("#txtPwdEmpresa").val()
                    + "', idProvincia: " + $("#ddlProvinciaEmpresa").val()
                    + " , idCiudad: " + $("#ddlCiudadEmpresa").val()
                    + " , domicilio: '" + $("#txtDomicilioEmpresa").val()
                    + "', pisoDepto: '" + $("#txtPisoDeptoEmpresa").val()
                    + "'}";

            $.ajax({
                type: "POST",
                url: "/modulos/seguridad/mis-datos.aspx/CrearEmpresa",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divErrorEmpresa").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $('#modalNuevaEmpresa').modal('toggle');

                    Empresa.filtrar();
                    Common.ocultarProcesando("btnCrearEmpresa", "Crear empresa");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorEmpresa").html(r.Message);
                    $("#divErrorEmpresa").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnCrearEmpresa", "Crear empresa");
                }
            });

        }
        else {
            return false;
        }
    },
    cambiarSesion: function (id, nombre) {
        Common.cambiarSesion(id, nombre);
    },
    nuevo: function () {
        Empresa.limpiarDatos();
        $('#modalNuevaEmpresa').modal('toggle');
    },
    limpiarDatos: function () {
        $("#divErrorEmpresa").hide();
        $("#txtRazonSocialEmpresa,#ddlCondicionIvaEmpresa,#txtCuitEmpresa,#txtEmailEmpresa,#txtDomicilioEmpresa,#txtPisoDeptoEmpresa,#ddlProvinciaEmpresa,#ddlCiudadEmpresa,#txtPwdEmpresa").val("");
    },
    changeProvincia: function () {
        Common.obtenerCiudades("ddlCiudadEmpresa", $("#ddlCiudadEmpresa").val(), $("#ddlProvinciaEmpresa").val(), true);
    },
    filtrar: function () {
        $("#divError").hide();
        $("#resultsContainerEmpresas").html("");

        $.ajax({
            type: "GET",
            url: "/modulos/seguridad/mis-datos.aspx/getResults",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                if (data.d.TotalPage > 0) {
                }
                else {
                    //$("#divPagination").hide();
                    $("#msjResultados").html("");
                }

                if (parseInt($("#IDUsuarioAdicional").val()) == 0) {
                    $("#btnNuevo").show()
                }
                else {
                    $("#btnNuevo").hide()
                }

                if (data.d.SuperoLimite) {
                    $("#btnNuevo").hide();
                    $("#spMsgCantEmpresas").show();
                }

                $("#IDUsuarioActual").val(data.d.UsuLogiado);
                // Render using the template
                if (data.d.Items.length > 0)
                    $("#resultTemplateEmpresas").tmpl({ results: data.d.Items }).appendTo("#resultsContainerEmpresas");
                else
                    $("#noResultTemplateEmpresas").tmpl({ results: data.d.Items }).appendTo("#resultsContainerEmpresas");
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                alert(r.Message);
            }
        });
    },
    verTodos: function () {
        Empresa.filtrar();
    }
}

var foto = {
    showInputLogo: function () {
        $("#divLogo").slideToggle();
    },
    eliminarLogo: function () {
        $.ajax({
            type: "GET",
            url: "mis-datos.aspx/eliminarFoto",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data, text) {
                $('#divOk').show();
                $("#divError").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $("#imgFoto").attr("src", "/files/usuarios/no-photo.png");
                $("#hdnTieneFoto").val("0");
                foto.showBtnEliminar();
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    },
    showBtnEliminar: function () {
        if ($("#hdnTieneFoto").val() == "1") {
            $("#divEliminarFoto").show();
            //$("#divAdjuntarFoto").removeClass("col-sm-12").addClass("col-sm-6");
        }
        else {
            $("#imgLogo").attr("src", "/files/usuarios/no-photo.png");
            $("#divEliminarFoto").hide();
            //$("#divAdjuntarFoto").removeClass("col-sm-6").addClass("col-sm-12");
        }
    },

}
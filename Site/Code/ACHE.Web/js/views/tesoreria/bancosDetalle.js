﻿var bancosDetalle = {
    /*** FORM ***/
    grabar: function () {
        $("#divError,#divOk").hide();
        var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        if (parseFloat($("#txtImporte").val()) == 0) {
            $("#msgError").html("El importe debe ser mayor a 0");
            $("#divError").show();
            return false;
        }

        if (!$("#ddlPlanDeCuentas").valid() && $("#hdnUsaPlanCorporativo").val() == "1") {
            return false;
        }


        if ($('#txtImporte,#txtTipoMovimiento,#txtConcepto,#txtFecha').valid()) {
            var idPlanDeCuenta = ($("#ddlPlanDeCuentas").val() == "" || $("#ddlPlanDeCuentas").val() == null) ? "0" : parseInt($("#ddlPlanDeCuentas").val());

            //alert($("input[name='ctl00$MainContent$chkTipoMovDebe']:checked"));

            var tipoMov = ($("#chkTipoMovDebe").is(":checked") ? "Ingreso" : "Egreso");

            Common.mostrarProcesando("btnActualizar");
            var info = jQuery.parseJSON('{ "ID": ' + parseInt(id)
                + ', "IDBanco": ' + $("#ddlBanco").val()
                + ', "Concepto": "' + $("#txtConcepto").val()
                + '", "Observaciones": "' + $("#txtObservaciones").val()
                + '", "Importe": "' + $("#txtImporte").val()
                + '", "tipoMovimiento": "' + tipoMov
                + '", "Fecha": "' + $("#txtFecha").val()
                + '", "Ticket": "' + $("#txtTicket").val()
                + '", "IDPlanDeCuenta": "' + idPlanDeCuenta
                + '", "idCategoria": "' + $("#ddlCategoria").val()
                + '"}');

            $.ajax({
                type: "POST",
                url: "/modulos/tesoreria/bancosDetalle.aspx/guardar",
                data: "{detalle: " + JSON.stringify(info) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divErrorAlta").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    //$("#hdnID").val(data.d);
                    //if ($("#hdnUsaPlanCorporativo").val() == "1") {
                    //    caja.generarAsientosContables();
                    //}

                    window.location.href = "/modulos/reportes/detalleBancario.aspx";

                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Aceptar");
                }
            });
        } else {
            $("#msgError").html("Por favor, complete todos los campos");
            $("#divError").show();

            return false;
        }
    },
    cancelar: function () {
        window.location.href = "/modulos/reportes/detalleBancario.aspx";
    },
    configForm: function () {
        Common.configDatePicker();
        bancosDetalle.verificarPlanDeCuentas();
        $(".select2").select2({
            width: '100%',
            allowClear: true
        });

        $("#txtImporte").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: false,
            precision: 2
        });

        // Validation with select boxes
        $("#ddlBanco,#txtImporte,#txtTipoMovimiento,#ddlPlanDeCuentas,#txtFecha, #txtConcepto").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        // Validation with select boxes
        $("#frmBancoDetalle").validate({
            highlight: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                jQuery(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    },
    limpiarCampos: function () {
        $("#txtTipoMovimiento").val("Ingreso");
        $("#txtObservaciones,#txtImporte,#txtFecha,#txtTicket,#ddlPlanDeCuentas, #txtConcepto").val("");

        $("#divError,#divOk,#divErrorAlta").hide();
        $("#hdnID").val("0");
        $("#txtFecha").datepicker("setDate", new Date());

        Common.ocultarProcesando("btnActualizar", "Aceptar");
    },
    verificarPlanDeCuentas: function () {
        if ($("#hdnUsaPlanCorporativo").val() == "1") {
            $(".divPlanDeCuentas").show();
        }
        else {
            $(".divPlanDeCuentas").hide();
        }
    },
    obtenerCategorias: function () {
        bancosDetalle.ocultarMensajes();

        $("#btnCategoria").html("Agregar");
        $("#bodyDetalle").html();

        var info = "{claseJS: 'bancosDetalle'}";

        $.ajax({
            type: "POST",
            url: "/modulos/tesoreria/bancosdetalle.aspx/getCategories",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#bodyDetalle").html(data.d);
                }
                $('#modalCategorias').modal('show');
            }
        });
    },
    grabarCategoria: function () {
        bancosDetalle.ocultarMensajes();

        if ($("#txtNuevaCat").val() != "") {

            var info = "{id: " + $("#hdnIDCategoria").val() + ", nombre: '" + $("#txtNuevaCat").val() + "'}";

            $.ajax({
                type: "POST",
                url: "/modulos/tesoreria/bancosdetalle.aspx/guardarCategoria",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    bancosDetalle.obtenerCategorias();
                    $("#txtNuevaCat").val("");
                    $("#hdnIDCategoria").val("0");

                    $("#ddlCategoria").html("");
                    Common.obtenerCategorias("ddlCategoria", true);
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgErrorCat").html(r.Message);
                    $("#divErrorCat").show();
                }
            });

        }
        else {
            $("#msgErrorCat").html("Debes ingresar un valor");
            $("#divErrorCat").show();
        }
    },
    ocultarMensajes: function () {
        $("#divErrorCat").hide();
    },
    editarCategoria: function (id, nombre) {
        bancosDetalle.ocultarMensajes();
        $("#btnCategoria").html("Actualizar");
        $("#txtNuevaCat").val(nombre);
        $("#hdnIDCategoria").val(id);
    },
    eliminarCategoria: function (id) {
        bancosDetalle.ocultarMensajes();

        var info = "{ id: " + parseInt(id) + "}";

        $.ajax({
            type: "POST",
            url: "/modulos/tesoreria/bancosdetalle.aspx/eliminarCategoria",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $("#txtNuevaCat").val("");
                $("#hdnIDCategoria").val("0");

                $("#ddlCategoria").html("");
                bancosDetalle.obtenerCategorias("ddlCategoria", true);
            },
            error: function (response) {
                var r = jQuery.parseJSON(response.responseText);
                $("#msgErrorCat").html(r.Message);
                $("#divErrorCat").show();
            }
        });
    },
}
﻿/*** FORM ***/
var geocoder;
var map;
function armarSelectSegunCondicionIVA() {

    var valor = $("#ddlTipoDoc").val();

    if ($("#ddlCondicionIva").val() == "CF") {
        $("#ddlTipoDoc").html("");
        $("<option/>").attr("value", "").text("").appendTo($("#ddlTipoDoc"));
        $("<option/>").attr("value", "DNI").text("DNI").appendTo($("#ddlTipoDoc"));
    }
    else {
        $("#ddlTipoDoc").html("");
        $("<option/>").attr("value", "").text("").appendTo($("#ddlTipoDoc"));

        $("<option/>").attr("value", "DNI").text("DNI").appendTo($("#ddlTipoDoc"));
        $("<option/>").attr("value", "CUIT").text("CUIT").appendTo($("#ddlTipoDoc"));
    }

    if (valor != null && valor != "") {
        $("#ddlTipoDoc").val(valor);
        $("#ddlTipoDoc").trigger("change");
    }
}

function grabar() {
    $("#divError").hide();
    $("#divOk").hide();

    var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
    //var ciudad = (($("#ddlCiudad").val() == "" || $("#ddlCiudad").val() == null) ? "0" : $("#ddlCiudad").val());
    //var provincia = (($("#ddlProvincia").val() == "" || $("#ddlProvincia").val() == null) ? "0" : $("#ddlProvincia").val());

    var pais = $("#ddlPais").val();
    var provincia = $("#ddlProvincia").val();
    var ciudad = $("#ddlCiudad").val();
    var usuarioAdicional = $("#ddlIDUsuarioAdicional").val();

    if (usuarioAdicional == "" || usuarioAdicional == null || usuarioAdicional == "null")
        usuarioAdicional= 0;
    if (pais == "" || pais == null || pais == "null")
        pais = 10;
    if (provincia == "" || provincia == null || provincia == "null")
        provincia = 0;
    if (ciudad == "" || ciudad == null || ciudad == "null")
        ciudad = 0;

    var saldoInicial = ($("#txtSaldo").val() == "") ? 0 : parseFloat($("#txtSaldo").val());

    if ($('#frmEdicion').valid()) {
        if (provincia == 0) {
            $("#msgError").html("Debe seleccionar la provincia");
            $("#divError").show();
            $("#divOk").hide();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else {

            Common.mostrarProcesando("btnActualizar");
            /*var jsonObj = { "id": 0, "razonSocial": 0, "nombreFantasia": 0, "condicionIva": 0, "personeria": 0, "tipoDoc": 0, "nroDoc": 'CanViewEmployee', "telefono": 0,
                "email": 0, "tipo": '1' ,
                "idPais": 0, "idProvincia": '1' ,
                "idCiudad": 0, "domicilio": '1', 
                "pisoDepto":0 , "cp": '1' ,
                "obs": 0, "listaPrecio": '1',
                "codigo": 0, "saldoInicial": '1',
                "apodoML": '1'
            };*/


            var info = "{ id: " + parseInt(id)
                    + " , razonSocial: '" + $("#txtRazonSocial").val()
                    + "', nombreFantasia: '" + $("#txtNombreFantasia").val()
                    + "', condicionIva: '" + $("#ddlCondicionIva").val()
                    + "', personeria: '" + $("#ddlPersoneria").val()
                    + "', tipoDoc: '" + $("#ddlTipoDoc").val()
                    + "', nroDoc: '" + $("#txtNroDocumento").val()
                    + "', telefono: '" + $("#txtTelefono").val()
                    + "', email: '" + $("#txtEmail").val()
                    + "', emailFc: '" + $("#txtEmailFc").val()
                    + "', tipo: '" + $("#hdnTipo").val()
                    + "', idPais: " + pais
                    + " , idProvincia: " + provincia
                    + " , idCiudad: " + ciudad
                    + " , domicilio: '" + $("#txtDomicilio").val()
                    + "', pisoDepto: '" + $("#txtPisoDepto").val()
                    + "', cp: '" + $("#txtCp").val()
                    + "', obs: '" + $("#txtObservaciones").val()
                    + "', listaPrecio: " + parseInt($("#ddlListaPrecios").val())
                    + " , codigo: '" + $("#txtCodigo").val()
                    + "', saldoInicial: " + saldoInicial
                    + " , apodoML: '" + $("#txtApodoML").val()
                    + "', idUsuarioAdicional: '" + usuarioAdicional
                    + "' }";

            /*alert(info);
            alert(JSON.stringify(info));
            //var aux = JSON.stringify(info.serializeObject());
            */
            $.ajax({
                type: "POST",
                url: "personase.aspx/guardar",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, text) {
                    $("#hdnID").val(data.d);

                    // if ($("#hdnSinCombioDeFoto").val() == "0") {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    window.location.href = "/personas.aspx?tipo=" + $("#hdnTipo").val();
                    //}

                    Common.ocultarProcesando("btnActualizar", "Guardar cliente");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    Common.ocultarProcesando("btnActualizar", "Guardar cliente");
                }
            });
        }
    }
    else {
        return false;
    }
}

function cancelar() {
    window.location.href = "/personas.aspx?tipo=" + $("#hdnTipo").val();
}

function configForm() {
    $(".select2").select2({ width: '100%', allowClear: true });

    $("#txtNroDocumento").numericInput();

    Common.obtenerPaises("ddlPais", $("#hdnPais").val(), true);
    Common.obtenerProvincias("ddlProvincia", $("#hdnProvincia").val(), $("#hdnPais").val(), true);
    Common.obtenerCiudades("ddlCiudad", $("#hdnCiudad").val(), $("#hdnProvincia").val(), true);


    if ($("#hdnTipo").val() == "c") {
        Common.obtenerListaPrecios("ddlListaPrecios", $("#hdnIDListaPrecio").val(), true);
        $("#divListaClientes").show()
        $("#rCtaCteCliente").attr("checked", true)
    }
    else {
        $("#divListaClientes").hide()
        $("#rCtaCteProv").attr("checked", true)
    }

    obtenerCantidadDecimales(function (serverData) {
        $("#txtSaldo").maskMoney({
            thousands: '',
            decimal: '.',
            allowZero: true,
            allowNegative: true,
            precision: serverData.d
        });
    });

    // Validation with select boxes
    $("#frmEdicion").validate({
        highlight: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            jQuery(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    if ($("#hdnID").val() != "" && $("#hdnID").val() != "0")
        $("#btnAcciones").show();
    else {
        $("#btnAcciones, #divStats").hide();
    }

    $.validator.addMethod("validCuit", function (value, element) {
        var check = true;
        if ($("#ddlTipoDoc").val() == "CUIT") {
            return CuitEsValido($("#txtNroDocumento").val());
        }
        else
            return check;

    }, "CUIT Inválido");

    fotos.adjuntarFoto();
    armarSelectSegunCondicionIVA();

    if ($("#ddlCondicionIva").val() == "CF") {
        $("#divPersoneria").hide();
        $("#spIdentificacionObligatoria").hide();
        $("#ddlTipoDoc,#txtNroDocumento").removeClass("required");
    }
    // google.maps.event.addDomListener(window, 'load', initialize);
}

function changeProvincia() {
    Common.obtenerCiudades("ddlCiudad", $("#ddlCiudad").val(), $("#ddlProvincia").val(), true);
}

function changePais() {
    Common.obtenerProvincias("ddlProvincia", $("#ddlProvincia").val(), $("#ddlPais").val(), true);
}

function realizarAccion(pag) {
    window.location.href = pag + "?IDPersona=" + $("#hdnID").val();
}

function CambiarReporte() {

    if ($("input[name='rCtaCte']:checked").attr('value') == "0") {
        $("#rCtaCteProv").attr("checked", true);
        $("#hdnTipoPersonaExportar").val("0");
    } else {
        $("#rCtaCteCliente").attr("checked", true);
        $("#hdnTipoPersonaExportar").val("1");
    }

    var info = "{ idPersona: " + parseInt($("#hdnID").val())
           + ", verComo: " + $("input[name='rCtaCte']:checked").attr('value') + "}";

    $.ajax({
        type: "POST",
        url: "/common.aspx/getResultsCtaCte",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d.TotalPage > 0) {
                if ($("input[name='rCtaCte']:checked").attr('value') == "1") {
                    $("#thfecha").html("Fecha Cobro");
                    $("#thImporte").html("Cobrado");
                } else {
                    $("#thfecha").html("Fecha Pago");
                    $("#thImporte").html("Pagado");
                }
            }

            $("#bodyDetalle").html("");
            // Render using the template
            if (data.d.Items.length > 0) {
                $("#resultTemplateDetalle").tmpl({ results: data.d.Items }).appendTo("#bodyDetalle");
                $("#divExportar").show();
            }
            else {
                $("#noResultTemplateDetalle").tmpl({ results: data.d.Items }).appendTo("#bodyDetalle");
                $("#divExportar").hide();
            }

            $('#modalDetalle').modal('show');
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
}

function changeCondicionIva() {
    armarSelectSegunCondicionIVA();
    if ($("#ddlCondicionIva").val() == "CF") {
        $("#ddlTipoDoc").val("");
        $("#ddlTipoDoc").trigger("change");
        $("#divPersoneria").hide();
        $("#spIdentificacionObligatoria").hide();
        $("#ddlTipoDoc,#txtNroDocumento").removeClass("required");
    }
    else if ($("#ddlCondicionIva").val() == "RI") {
        $("#ddlTipoDoc").val("CUIT");
        $("#ddlTipoDoc").trigger("change");
        $("#divPersoneria").show();
        $("#spIdentificacionObligatoria").show();
        $("#ddlTipoDoc,#txtNroDocumento").addClass("required");
    }
    else {
        $("#ddlTipoDoc").val("");
        $("#ddlTipoDoc").trigger("change");
        $("#divPersoneria").show();
        $("#spIdentificacionObligatoria").show();
        $("#ddlTipoDoc,#txtNroDocumento").addClass("required");
    }
}
//*** MAPS***/
function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var myOptions = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    if ($("#hdnDireccion").val() != "") {
        codeAddress($("#hdnDireccion").val());
    }
    else {
        codeAddress("Buenos aires");
    }
    map = new google.maps.Map(document.getElementById("gmap-marker"), myOptions);
}

function codeAddress(address) {

    if (geocoder) {
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }
}
/*** SEARCH ***/

function configFilters() {
    $("#txtNroDocumento").numericInput();

    $("#txtRazonSocial, #txtNroDocumento").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            resetearPagina();
            filtrar();
            return false;
        }
    });

}

function nuevo() {
    window.location.href = "/personase.aspx?tipo=" + $("#hdnTipo").val();
}

function importar() {

    var tipo = "";

    if ($("#hdnTipo").val() == "c") {
        tipo = "Clientes";
    }
    else {
        tipo = "Proveedores";
    }
    window.location.href = "/importar.aspx?tipo=" + tipo;
}

function editar(id) {
    window.location.href = "/personase.aspx?ID=" + id + "&tipo=" + $("#hdnTipo").val();
}

function eliminar(id, nombre) {
    bootbox.confirm("¿Está seguro que desea eliminar a " + nombre + "?", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: "personas.aspx/delete",
                data: "{ id: " + id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    filtrar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#divError").html(r.Message);
                    $("#divError").show();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
    });
}

function mostrarPagAnterior() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual--;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function mostrarPagProxima() {
    var paginaActual = parseInt($("#hdnPage").val());
    paginaActual++;
    $("#hdnPage").val(paginaActual);
    filtrar();
}

function resetearPagina() {
    $("#hdnPage").val("1");
}

function filtrar() {

    $("#divError").hide();
    $("#resultsContainer").html("");
    var currentPage = parseInt($("#hdnPage").val());

    var info = "{ condicion: '" + $("#txtRazonSocial").val()
               + "', tipo: '" + $("#hdnTipo").val()
               + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
               + "}";

    $.ajax({
        type: "POST",
        url: "personas.aspx/getResults",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            $("#resultsContainer").empty();

            if (data.d.TotalPage > 0) {
                $("#divPagination").show();

                $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                if (data.d.TotalPage == 1)
                    $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                else if (currentPage == data.d.TotalPage)
                    $("#lnkNextPage").attr("disabled", "disabled");
                else if (currentPage == 1)
                    $("#lnkPrevPage").attr("disabled", "disabled");

                var aux = (currentPage * PAGE_SIZE);
                if (aux > data.d.TotalItems)
                    aux = data.d.TotalItems;
                $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.d.TotalItems);
            }
            else {
                $("#divPagination").hide();
                $("#msjResultados").html("");
            }

            // Render using the template
            if (data.d.Items.length > 0)
                $("#resultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
            else
                $("#noResultTemplate").tmpl({ results: data.d.Items }).appendTo("#resultsContainer");
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            alert(r.Message);
        }
    });
    resetearExportacion();
}

function verTodos() {
    $("#txtRazonSocial, #txtNroDocumento").val("");
    filtrar();
}

function exportar() {
    resetearExportacion();
    $("#imgLoading").show();
    $("#divIconoDescargar").hide();

    var info = "{ condicion: '" + $("#txtRazonSocial").val()
               + "', tipo: '" + $("#hdnTipo").val()
               + "'}";

    $.ajax({
        type: "POST",
        url: "personas.aspx/export",
        data: info,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data, text) {
            if (data.d != "") {

                $("#divError").hide();
                $("#imgLoading").hide();
                $("#lnkDownload").show();
                $("#lnkDownload").attr("href", data.d);
                $("#lnkDownload").attr("download", data.d);
            }
        },
        error: function (response) {
            var r = jQuery.parseJSON(response.responseText);
            $("#msgError").html(r.Message);
            $("#divError").show();
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            resetearExportacion();
        }
    });
}

function resetearExportacion() {
    $("#imgLoading, #lnkDownload").hide();
    $("#divIconoDescargar").show();
}

function configFormSinDatos() {
    if ($("#hdnTipo").val() == "c") {
        $("#hTitulo").text("Aún no has creado ningún cliente");
        $("#btnNuevoSinDatos").text("Crea un Cliente");
    }
    else {
        $("#hTitulo").text("Aún no has creado ningún proveedor");
        $("#btnNuevoSinDatos").text("Crea un proveedor");
    }
}
function cargarDatosAfip() {
    $("#msgError").html("");
    $("#divError").hide();
    if ($("#txtNroDocumento").val() != '') {
        $("#loadingAfip").show();

        $("#btnActualizar").attr("disabled", true);
        $("#btnAfip").hide();
        var info = "{ nroDocumento: '" + $("#txtNroDocumento").val() + "'}";

        $.ajax({
            type: "POST",
            url: "/common.aspx/ObtenerDatosAfipPersona",
            data: info,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                $("#btnAfip").show();
                $("#loadingAfip").hide();
                $("#btnActualizar").attr("disabled", false);
                if (data == null || data.d == null) {
                    $("#msgError").html("No se han encontrado los datos de Afip");
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                } else {

                    if (data.d.RazonSocial != null) {
                        $("#txtRazonSocial").val(data.d.RazonSocial);
                        $("#txtNombreFantasia").val(data.d.RazonSocial);
                    }
                    if (data.d.Domicilio != null)
                        $("#txtDomicilio").val(data.d.Domicilio);
                    if (data.d.PisoDto != null)
                        $("#txtPisoDepto").val(data.d.PisoDto);
                    if (data.d.CodigoPostal != null)
                        $("#txtCp").val(data.d.CodigoPostal);
                    if (data.d.Personeria != null)
                        $("#ddlPersoneria").val(data.d.Personeria);

                    if (data.d.ProvinciaId != null) {

                        $("#ddlProvincia").val(data.d.ProvinciaId);
                        $("#ddlProvincia").trigger("change");
                        if (data.d.CiudadId != null) {
                            $("#ddlCiudad").val(data.d.CiudadId);
                            $("#ddlCiudad").trigger("change");
                        }
                    }

                    if (data.d.TipoDoc != null) {
                        $("#ddlTipoDoc").val(data.d.TipoDoc);
                    }
                    if (data.d.NroDoc != null) {
                        $("#txtNroDocumento").val(data.d.NroDoc);
                    }

                    if (data.d.CategoriaImpositiva != null) {
                        $("#ddlCondicionIva").val(data.d.CategoriaImpositiva);
                    }

                }

            },
            error: function (response) {
                $("#loadingAfip").hide();
                $("#btnAfip").show();
                $("#btnActualizar").attr("disabled", false);
                var r = jQuery.parseJSON(response.responseText);
                $("#msgError").html(r.Message);
                $("#divError").show();
                $("#divOk").hide();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        });
    } else {
        $("#msgError").html("Debe ingresar su documento para poder solicitar sus datos a Afip");
        $("#divError").show();
        $("#divOk").hide();
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }
}
/*** Adjuntar Foto ***/
var fotos = {
    adjuntarFoto: function () {

        $('#flpArchivo').fileupload({
            url: "/subirImagenes.ashx?idPersona=" + $("#hdnID").val() + "&opcionUpload=persona",
            success: function (response, status) {
                if (response == "OK") {
                    $("#divError").hide();
                    $("#divOk").show();
                    $("#btnActualizar").attr("disabled", false);
                    window.location.href = "/personas.aspx?tipo=" + $("#hdnTipo").val();
                }
                else {
                    $("#hdnFileName").val("");
                    $("#msgError").html(response);
                    $("#divError").show();
                    $("#divOk").hide();
                    $("#btnActualizar").attr("disabled", false);
                }
            },
            error: function (error) {
                $("#hdnFileName").val("");
                $("#msgError").html(error.responseText);
                $("#imgLoading").hide();
                $("#divError").show();
                $("#divOk").hide();

                $('html, body').animate({ scrollTop: 0 }, 'slow');
                Common.ocultarProcesando("btnActualizar", "Guardar cliente");
            },
            autoUpload: false,
            add: function (e, data) {
                Common.ocultarProcesando("btnActualizar", "Guardar cliente");
                $("#hdnSinCombioDeFoto").val("1");
                $("#btnActualizar").on("click", function () {
                    $("#imgLoading").show();
                    grabar();
                    if ($("#hdnID").val() != "0") {
                        data.url = '/subirImagenes.ashx?idPersona=' + $("#hdnID").val() + "&opcionUpload=persona";
                        data.submit();
                    }
                })
            }
        });
        fotos.showBtnEliminar();
    },

    showInputFoto: function () {
        $("#divLogo").slideToggle();
    },
    grabarsinImagen: function () {
        if ($("#hdnSinCombioDeFoto").val() == "0") {
            grabar();
        }
    },
    eliminarFoto: function () {
        var id = ($("#hdnID").val() == "" ? "0" : $("#hdnID").val());
        if (id != "") {
            var info = "{ idPersona: " + parseInt(id) + "}";

            $.ajax({
                type: "POST",
                url: "personase.aspx/eliminarFoto",
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data, text) {
                    $('#divOk').show();
                    $("#divError").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#imgFoto").attr("src", "/files/usuarios/no-cheque.png");
                    $("#hdnTieneFoto").val("0");
                    fotos.showBtnEliminar();
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    $("#msgError").html(r.Message);
                    $("#divError").show();
                    $("#divOk").hide();
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            });
        }
        else {
            $("#msgError").html("El producto no tiene una imagen guardada");
            $("#divError").show();
            return false;
        }
    },
    showBtnEliminar: function () {
        if ($("#hdnTieneFoto").val() == "1") {
            $("#divEliminarFoto").show();
            $("#divAdjuntarFoto").removeClass("col-sm-12").addClass("col-sm-6");
        }
        else {
            $("#divEliminarFoto").hide();
            $("#divAdjuntarFoto").removeClass("col-sm-6").addClass("col-sm-12");
        }
    },
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="conceptose.aspx.cs" Inherits="conceptose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-file-text"></i>Productos y servicios </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/conceptos.aspx">Productos y servicios</a></li>
                <li class="active">
                    <asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>


                        <div class="col-sm-12">
                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <h3>Datos principales </h3>
                                    <label class="control-label">Estos son los datos básicos que el sistema necesita para poder facturar un producto.</label>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="row">
                                    <div class="col-sm-3 col-md-2">
                                        <img id="imgFoto" src="/files/usuarios/no-producto.png" style="height: 155px;" class="thumbnail img-responsive" alt="" runat="server" />
                                        <div class="mb20"></div>
                                    
                                        <div class="row" id="divAdjuntarFoto" style="padding: 0;margin-left: 0px;margin-right: 0;">
                                            <a class="btn btn-white btn-block" onclick="showInputFoto();">Adjuntar foto</a>
                                            <div id="divFoto" style="display: none">
                                                <p class="mb30">Formato JPG, PNG o GIF. Tamaño máximo recomendado: 100x70px</p>
                                                <input type="hidden" value="" name="" /><input type="file" id="flpArchivo" />
                                                <div class="mb20"></div>
                                            </div>
                                        </div>
                                        <div class="row" id="divEliminarFoto" style="padding: 0;margin-left: 0px;margin-right: 0;margin-top:10px">
                                            <a class="btn btn-white btn-block" onclick="eliminarFotoProducto();">Eliminar foto</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-6">
                                        <div class="row mb15">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Tipo</label>
                                                    <asp:DropDownList runat="server" ID="ddlTipo" CssClass="form-control" onchange="changeTipo(true);">
                                                        <asp:ListItem Value="P">Producto</asp:ListItem>
                                                        <asp:ListItem Value="S">Servicio</asp:ListItem>
                                                        
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Código (SKU)</label>
                                                    <asp:TextBox runat="server" ID="txtCodigo" CssClass="form-control required" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb15">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Nombre</label>
                                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="500"></asp:TextBox>

                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Estado</label>
                                                    <asp:DropDownList runat="server" ID="ddlEstado" CssClass="form-control required">
                                                        <asp:ListItem Text="Activo" Value="A"></asp:ListItem>
                                                        <asp:ListItem Text="Inactivo" Value="I"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="row mb5">
                                <div class="col-sm-12">
                                    <h3>Importes</h3>
                                </div>
                            </div>
                            <div class="row mb15"  id="divPrecioAutomatico">
                                  <div class="col-sm-4 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Precio Automático</label>
                                        <asp:DropDownList runat="server" ID="ddlPrecioAutomatico" CssClass="form-control required"  onchange="changePrecioAutomatico(this.value);">
                                            <asp:ListItem Text="Si" Value="Si" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                       </div>           
                                <div class="col-sm-6 col-md-6" style="padding-top: 25px;" >
                                           Si el precio es automático, se calcula en base al precio de sus productos.
                                 </div>
                               
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-6 col-md-3" id="divCostoInterno">
                                    <div class="form-group">
                                        <label class="control-label">Costo interno s/IVA</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <asp:TextBox runat="server" ID="txtCostoInterno" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3" id="divRentabilidad">
                                    <div class="form-group">
                                        <label class="control-label">Rentabilidad</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">%</span>
                                            <asp:TextBox runat="server" ID="txtRentabilidad" CssClass="form-control number" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span><asp:Literal ID="liPrecioUnitario" runat="server"></asp:Literal></label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <asp:TextBox runat="server" ID="txtPrecio" CssClass="form-control required number" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> IVA %</label>
                                        <asp:DropDownList runat="server" ID="ddlIva" CssClass="form-control">
                                            <asp:ListItem Value="0,00">0</asp:ListItem>
                                            <asp:ListItem Value="2,50">2,5</asp:ListItem>
                                            <asp:ListItem Value="5,00">5</asp:ListItem>
                                            <asp:ListItem Value="10,50">10,5</asp:ListItem>
                                            <asp:ListItem Value="21,00">21</asp:ListItem>
                                            <asp:ListItem Value="27,00">27</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-2">
                                    <h5 class="subtitle mb10"><asp:Literal ID="liPrecioTotal" runat="server"></asp:Literal></h5>
                                    <h4 class="text-primary" style="color: green" id="divPrecioIVA">$<asp:Literal runat="server" ID="litTotal"></asp:Literal></h4>
                                </div>

                                <div class="col-sm-12" id="divStockMinimo">
                                    <hr />
                                    <div class="row mb5">
                                        <div class="col-sm-12">
                                            <h3>Stock mínimo</h3>
                                        </div>
                                    </div>
                                    <div class="row mb15">
                                        <div class="col-sm-6 col-md-3">
                                            <div class="form-group">
                                                <label class="control-label"></label>
                                                <asp:TextBox runat="server" ID="txtStockMinimo" CssClass="form-control number" MaxLength="10"></asp:TextBox>
                                                <span class="help-block"><small>Te servirá para recibir alertas en tu mail.</small></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6" style="padding-top: 25px;">
                                            Para configurar las alertas de stock mínimo ingrese a Mi cuenta -> Datos de mi empresa -> Alertas y avisos.
                                        </div>
                                        
                                        <%--<div class="col-sm-6 col-md-3">
                                            <div class="form-group">
                                              <label class="control-label">¿Controlar stock?</label>
                                              <select name="" id="" class="form-control" onchange="">
                                                <option value="Si">Si</option>
                                                <option value="No">No</option>
                                              </select>
                                            </div>
                                      </div>--%>
                                    </div>
                                </div>
                  
                             
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default" id="divConceptosCombo">
                    <div class="panel-body">
                        <div>
                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <h3>Productos del combo</h3>
                                </div>
                                <UC:NuevoConcepto runat="server" ID="ucConceptoCombo" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="row mb5">
                                <div class="col-sm-12">
                                    <h3>Datos adicionales</h3>
                                </div>
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Descripción</label>
                                        <asp:TextBox runat="server" ID="txtDescripcion" CssClass="form-control" MaxLength="500"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Código OEM</label>
                                        <asp:TextBox runat="server" ID="txtCodigoOem" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3" id="divCodigoBarra">
                                    <div class="form-group">
                                        <label class="control-label">Código de barras</label>
                                        <asp:TextBox runat="server" ID="txtCodigoDeBarra" CssClass="form-control" MaxLength="128"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb15">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Rubro</label>
                                        <select id="ddlRubros" class="form-control required" onchange="Common.obtenerSubRubros(this.value,'ddlSubRubros');" data-placeholder="Seleccione un rubro..."></select>
                                        <div style="margin-top: 3px;">
                                            <span class="badge badge-success pull-right tooltips" data-toggle="tooltip" data-original-title="Haga click aquí para agregar rubros" style="cursor: pointer" onclick="window.location.href='rubros.aspx';">administrar</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Sub rubro</label>
                                        <select id="ddlSubRubros" class="form-control" data-placeholder="Seleccione un subRubro..."></select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <label class="control-label">Proveedor origen</label>
                                    <select id="ddlPersonas" class="select2" data-placeholder="Seleccione un cliente/proveedor...">
                                    </select>
                                </div>
                            </div>
                            <div class="row mb15 divPlanDeCuentas">
                                <div class="col-sm-6 col-md-3">
                                    <label class="control-label">Cuenta compra</label>
                                    <asp:DropDownList runat="server" ID="ddlPlanDeCuentasCompra" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                    </asp:DropDownList>

                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <label class="control-label">Cuenta venta</label>
                                    <asp:DropDownList runat="server" ID="ddlPlanDeCuentasVentas" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <%--<div class="row mb15">
                                <div class="col-sm-2 col-md-2" id="divStock">
                                    <div class="form-group hide">
                                        <label class="control-label"><span class="asterisk">*</span> Stock</label>
                                        <asp:TextBox runat="server" ID="txtStock" CssClass="form-control required" MaxLength="10" Text="0"></asp:TextBox>
                                    </div>
                                </div>

                            </div>--%>
                            <div class="row mb15">
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Observaciones</label>
                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="5" ID="txtObservaciones" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-success" id="actualizarConcepto" onclick="grabarsinImagen();">Aceptar</a>
                        <a href="#" onclick="cancelar();" style="margin-left: 20px">Cancelar</a>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnTotal" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDPersona" Value="0" />
                <asp:HiddenField runat="server" ID="hdnUsaPrecioFinalConIVA" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTieneFoto" Value="0" />
                <asp:HiddenField runat="server" ID="hdnSinCombioDeFoto" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIdUsuario" Value="0" />
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDSubRubro" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDRubro" Value="0" />
                <asp:HiddenField runat="server" ID="hdnPlanCorporativo" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDCuentaCompras" Value="0" />
                <asp:HiddenField runat="server" ID="hdnIDCuentaVentas" Value="0" />


            </form>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/conceptos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script src="/js/views/detalleConcepto.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    
    <script src="/js/jquery.fileupload.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            configForm();
            ConceptosCombo.configForm();
        });
    </script>

</asp:Content>

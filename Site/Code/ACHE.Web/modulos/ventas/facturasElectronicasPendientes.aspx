﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="facturasElectronicasPendientes.aspx.cs" Inherits="modulos_ventas_facturasElectronicasPendientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
     <style type="text/css">
        .padding5{
            padding:5px !important;
        }

        .clsDatePicker {
            z-index: 100000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   <div class="pageheader">
        <h2><i class="fa fa-file-text"></i>Facturación masiva de pendientes</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/comprobantes.aspx">Comprobantes</a></li>
                <li><a href="#">Facturación masiva de pendientes</a></li>
               
            </ol>
        </div>
    </div>
    <div>
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12 col-md-12 table-results">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="alert alert-danger" id="divError2" runat="server" visible="False"></div>
                            <div id="divConectar" runat="server">
                                <div class="alert alert-danger" id="divError" style="display: none">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <strong>Lo sentimos!</strong> <span id="msgError"></span>
                                </div>
                              
                            </div>
                            <div id="divFiltros" runat="server">
                                
                                <input type="hidden" id="hdnPage" runat="server" value="1" />
                                <div class="row">
                                    <div class="pull-right">
                                        <div class="btn-group mr10"></div>
                                        <div class="btn-group mr10" id="divPagination" style="display: none">
                                            <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                            <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                        </div>
                                    </div>

                                    <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                                    <p id="msjResultados" style="padding-left: 20px"></p>
                                </div>
                            </div>
                        </div>
                        <div id="divVentas" class="panel-body" runat="server">
                            <div class="alert alert-danger" id="div1" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Lo sentimos!</strong> <span id="Span1"></span>
                            </div>
                            <div class="table-responsive">
                                <table class="table mb30">
                                    <thead>
                                        <tr>
                                            <th style="max-width: 20px;">
                                                <input type='checkbox' id='chkFacturacionMasiva' />
                                            </th>
                                            <th>Tipo</th>
                                            <th>Punto de Venta</th>
                                            <th>Proveedor/Cliente</th>
                                            <th>Fecha</th>
                                            <th>Modo</th>
                                            <th>Imp. Neto Grav</th>
                                            <th>Total Fact.</th>
                                            <th class="columnIcons"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="resultsContainer">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-success" onclick="facturacionMasiva.elegirTipoFacturacionMasiva();" id="lnkFacturacionMasiva" style="display:none">Facturar Seleccionados</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="0" id="hdnCantItemsPag" runat="server" />
    <input type="hidden" value="0" id="hdnTotalCant"  />
    <input type="hidden" id="hdnIDVenta" value="0" />
    <input type="hidden" id="hdnIDUsuario" value="0" />

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td>
                <input style="left: 40px;" type='checkbox' id='chkVenta_${ID}' class="chkVenta" onchange="facturacionMasiva.actualizarTotales()"/>
            </td>
            <td>${Tipo}</td>
            <td>${Numero}</td>
            <td>${RazonSocial}</td>
            <td>${Fecha}</td>
            <td>${Modo}</td>
            <td>${ImporteTotalBruto}</td>
            <td>${ImporteTotalNeto}</td>
            <td class="table-action">
                <a href="/comprobantese.aspx?ID=${IDComprobante}" style="cursor: pointer; font-size: 16px" title="Generar Factura"><i class="fa fa-file-text"></i></a>
            </td>
        </tr>
        {{/each}}
      
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="9">No se han encontrado resultados</td>
        </tr>
    </script>
    
    
    <!-- MODAL MSJ PRE GENERACION MASIVA FACTURAS -->
    <div class="modal modal-wide fade" id="modalPreGeneracionAbonos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Facturación masiva</h4>
                </div>
                <div class="modal-body" style="padding: 25px;padding-top:0px">
                    <br />
                    <label>Fecha de emisión (si la deja en blanca se tomará la fecha de la fc)</label>
                    <div class="col-sm-4 form-group" style="margin-left: -10px;">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            <input type="text" id="txtFechaEmision" class="form-control validDate clsDatePicker" onchange="facturacionMasiva.validarFechaFacturacion()" placeholder="dd/mm/yyyy" maxlength="10"></input>
                        </div>
                    </div> 
                    <br />
                    <label>Fecha de vencimiento (debe ser mayor a la fecha de emisión)</label>
                    <div class="col-sm-4 form-group" style="margin-left: -10px;">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            <input type="text" id="txtFechaVencimiento" class="form-control validDate clsDatePicker" onchange="facturacionMasiva.validarFechaFacturacion()" placeholder="dd/mm/yyyy" maxlength="10"></input>
                        </div>
                        <span style="display:none" id="errorFechaVencimiento" class="help-block">La fecha de vencimiento debe ser mayor a la fecha de emisión</span>
                    </div>     
                    <div id="divMsjPreFacturacion" style="clear:left">
                         
                    </div>                    
                </div>
                <div class="modal-footer">
                    <asp:HyperLink runat="server" ID="HyperLink1" onclick="facturacionMasiva.facturar(true);" CssClass="btn btn-info mr5"><i class="fa fa-dollar mr5"></i> Emitir factura eléctronica</asp:HyperLink>
                </div>
            </div>
        </div>
     </div>
    
    <!-- MODAL MSJ GENERACION MASIVA FACTURAS -->
    <div class="modal modal-wide fade" id="modalGeneracionAbonos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Facturación masiva</h4>
                </div>
                <div class="modal-body">
                    <div class="icon">
                        <img style="display: none;" id="imgAbonoOK" src="/images/success.svg"><!-- exitoso -->
                        <img style="display: none;" id="imgAbonoError" src="/images/loading.svg"><!-- atención -->
                        <img style="display: none;" id="imgAbonoCargando" src="/images/reload.svg"><!-- procesando -->
                    </div>
                    
                    <div id="divWait">
                        <strong>Por favor, espere unos minutos. </strong>Se están facturando los items...
                    </div>

                    <h4 class="text-center text-uppercase title">
                        <span style="display: none;"id="msgAbonoOK" >¡Acción completada con éxito!</span><!-- exitoso -->
                        <span style="display: none;"id="msgAbonoError">¡Atención!</span><!-- exitoso -->
                    </h4>

                    <div id="divOk" style="display: none;">
                        <label class="label label-success" id="lblAbonoResultadoOK"><i class="fa fa-check"></i> <strong id="spnAbonoOK"></strong> se han procesado correctamente.</label>
                        <label class="label label-danger" id="lblAbonoResultadoError"><i class="fa fa-close"></i> <strong id="spnAbonoError"></strong> NO se pudieron procesar, errores a continuación:</label>
                    </div>
                    
                    <div style="display: none; width: 100%;font-style: italic;color: red;word-wrap: break-word;max-height: 500px;overflow: auto;" id="divresultsContainerModal">
                         <ul id="resultsContainerModal"></ul>
                    </div>
    
                    <div id="divCerrar">
                        <button class="btn btn-black" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    </div>
                </div>
                
            </div>
        </div>
     </div>
    <script id="resultComprobanteFETemplate" type="text/x-jQuery-tmpl">
       
        {{each results}}
        {{if EstadoFacturacion!=""}}        
            <li> ${EstadoFacturacion}</li>
        {{/if}}
        {{/each}}
            
        </script>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/ventas/facturasElectronicasPendientes.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            importaciones.configFilters();
        });
    </script>
</asp:Content>
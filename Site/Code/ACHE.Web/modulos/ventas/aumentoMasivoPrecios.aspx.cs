﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Common;

public partial class modulos_ventas_aumentoMasivoPrecios : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            CargarPersonas();
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static void Guardar(int idListaPrecios, decimal porcentaje, string actualizarTodos, int idPersona)
    {
        try
        {


            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
               int idProceso = LogCommon.IniciarProceso(usu.IDUsuario, 0, "ActualizarMasivamentePrecios");//se envia 0 en integracion, porque no se usa.

                ConceptosCommon.ActualizarPrecios(usu.IDUsuario,idListaPrecios, porcentaje, actualizarTodos, idPersona,usu.Email, idProceso);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private void CargarPersonas()
    {
        using (var dbContext = new ACHEEntities())
        {
            var listaPersonas = dbContext.Conceptos.Include("Personas").Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPersona.HasValue && x.IDPersona > 0).ToList();
            ddlPersonas.Items.Add(new ListItem("", ""));
            var nombre = "";
            foreach (var item in listaPersonas)
            {
                nombre = item.Personas.NombreFantansia == "" ? item.Personas.RazonSocial.ToUpper() : item.Personas.NombreFantansia.ToUpper();
                if (ddlPersonas.Items.FindByValue(item.IDPersona.ToString()) == null)
                    ddlPersonas.Items.Add(new ListItem(nombre, item.IDPersona.ToString()));
            }
        }
    }
}
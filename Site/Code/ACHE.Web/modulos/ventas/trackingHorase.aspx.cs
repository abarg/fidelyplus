﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_ventas_trackingHorase : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            litTitulo.Text = "<i class='fa fa-university'></i> Tracking Horas";
            litPathPadre.Text = "<a href='/Modulos/ventas/trackingHorase.aspx'>Tracking Horas</a>";

            litPath.Text = "Alta";
            
            using (var dbContext = new ACHEEntities())
            {

                var listaUsuariosAdicionales = dbContext.UsuariosAdicionales.Where(x => x.IDUsuario == CurrentUser.IDUsuario);

                ddlUuarios.Items.Add(new ListItem(CurrentUser.RazonSocial, ""));
                foreach (var item in listaUsuariosAdicionales)
                    ddlUuarios.Items.Add(new ListItem(item.Email, item.IDUsuarioAdicional.ToString()));


            }

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0")
                {
                    cargarEntidad(int.Parse(hdnID.Value));
                    litPath.Text = "Edición";
                }
            }
            else {
                txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFechaTarea.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }

        }
    }

    private void cargarEntidad(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.TrackingHoras.Where(x => x.IDTrackingHoras == id && x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault();
            if (entity != null)
            {
                hdnIDPersona.Value = entity.IDPersona.ToString();
                txtFecha.Text = entity.Fecha.ToString("dd/MM/yyyy");
                txtFechaTarea.Text = entity.Inicio.ToString("dd/MM/yyyy");
                //ddlTarea.SelectedValue = entity.Tarea;
                ddlTarea.Text = entity.Tarea;
                ddlAbono.Text = entity.IncluirEnAbono ? "1" : "0";
                txtCantHoras.Text = entity.Horas.ToString().Replace(".", "").Replace(",", ".");
                txtValorHora.Text = entity.ValorHora.ToString().Replace(".", "").Replace(",", ".");
                txtDesde.Text = entity.Inicio.ToString("HH:mm tt").Replace("p. m.", "PM").Replace("a. m.", "AM");
                txtHasta.Text = entity.Fin.ToString("HH:mm tt").Replace("p. m.", "PM").Replace("a. m.", "AM");
                ddlEstado.SelectedValue = entity.Estado;
                txtObservaciones.Text = entity.Observaciones;
                ddlUuarios.SelectedValue = (string.IsNullOrWhiteSpace(entity.IDUsuarioAdicional.ToString())) ? "" : entity.IDUsuarioAdicional.ToString();
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, int IDPersona, string fecha, string horaDesde, string horaHasta, string duracion, string Tarea, string Observaciones, string estado, string abono, string idUsuarioAdicional, string ValorHora, bool enviarMail)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {

                TrackingHoras entity;
                if (id > 0)
                    entity = dbContext.TrackingHoras.Where(x => x.IDTrackingHoras == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                else
                {
                    entity = new TrackingHoras();
                    entity.Fecha = DateTime.Now;
                }

                entity.IDUsuario = usu.IDUsuario;
                entity.IDPersona = IDPersona;
                entity.Inicio = Convert.ToDateTime(fecha + " " + horaDesde);
                entity.Fin = Convert.ToDateTime(fecha + " " + horaHasta);
                entity.Horas = Convert.ToDecimal(duracion.Replace(".", ","));
                entity.IncluirEnAbono = abono == "1" ? true : false;

                if (!string.IsNullOrEmpty(ValorHora) && estado == "Facturable")
                    entity.ValorHora = Convert.ToDecimal(ValorHora);
                else
                {
                    entity.ValorHora = null;
                }
                entity.Tarea = Tarea;
                entity.Observaciones = Observaciones;
                entity.Estado = estado;

                if (!string.IsNullOrWhiteSpace(idUsuarioAdicional))
                {
                    entity.IDUsuarioAdicional = Convert.ToInt32(idUsuarioAdicional);
                }

                if (id == 0)
                    dbContext.TrackingHoras.Add(entity);
                dbContext.SaveChanges();

                if (enviarMail)
                {
                    var persona = dbContext.Personas.Where(x => x.IDPersona == IDPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (persona != null && persona.Email != string.Empty)
                    {
                        MailAddressCollection listTo = new MailAddressCollection();
                        listTo.Add(persona.Email);

                        var detalleHoras = "A continuación le enviamos el resumen del trabajo realizado:";
                        detalleHoras += "<br><br>Fecha: " + entity.Inicio.ToString("dd/MM/yyyy");
                        detalleHoras += "<br>Desde/Hasta: " + entity.Inicio.ToString("HH:mm") + " - " + entity.Fin.ToString("HH:mm");
                        detalleHoras += "<br>Duración: " + entity.Horas.ToString();
                        detalleHoras += "<br>Tarea: " + entity.Tarea;

                        ListDictionary replacements = new ListDictionary();
                        replacements.Add("<NOTIFICACION>", detalleHoras);
                        replacements.Add("<USUARIO>", persona.RazonSocial);
                        //replacements.Add("<EMAIL>", persona.Email);

                        var emailUsuarioTo = !string.IsNullOrEmpty(usu.EmailAlerta) ? usu.EmailAlerta : usu.Email;
                        bool send = EmailCommon.SaveMessage(usu.IDUsuario, EmailTemplate.EnvioComprobante, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuarioTo, "Horas trabajadas de " + usu.RazonSocial, null, usu.EmailDisplayFrom ?? usu.RazonSocial);

                    }
                }
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
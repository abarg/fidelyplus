﻿using ACHE.Model;
using ACHE.Negocio.Common;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Ventas;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_ventas_facturasElectronicasPendientes : BasePage
{
    protected static ResultadosImportacionesComprobantesViewModel itemsFacturar
    {
        get { return (ResultadosImportacionesComprobantesViewModel)HttpContext.Current.Session["Importaciones_itemsFacturar"]; }
        set { HttpContext.Current.Session["Importaciones_itemsFacturar"] = value; }
    }

    protected static List<string> itemsSeleccionados
    {
        get { return (List<string>)HttpContext.Current.Session["Importaciones_itemsSeleccionados"]; }
        set { HttpContext.Current.Session["Importaciones_itemsSeleccionados"] = value; }
    }

    protected static int cantidadGenerados
    {
        get { return (int)HttpContext.Current.Session["Importaciones_cantidadGenerados"]; }
        set { HttpContext.Current.Session["Importaciones_cantidadGenerados"] = value; }
    }

    protected static int cantidadErrores
    {
        get { return (int)HttpContext.Current.Session["Importaciones_cantidadErrores"]; }
        set { HttpContext.Current.Session["Importaciones_cantidadErrores"] = value; }
    }

    protected static IList<ComprobantesViewModel> ComprobantesAFacturar
    {
        get { return (List<ComprobantesViewModel>)HttpContext.Current.Session["Importaciones_VentasIntegracion"]; }
        set { HttpContext.Current.Session["Importaciones_VentasIntegracion"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {


    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosImportacionesComprobantesViewModel getResults(int page, int pageSize)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                itemsSeleccionados = null;
                cantidadErrores = 0;
                cantidadGenerados = 0;

                var resultado = ComprobantesCommon.ObtenerComprobantesAFacturar(usu.IDUsuario, page, pageSize);
                ComprobantesAFacturar = resultado.Items;
                itemsFacturar = new ResultadosImportacionesComprobantesViewModel();
                itemsFacturar = resultado;

                //    ComprobantesImportacion = resultado.Items;

                return resultado;
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static object facturacionMasiva(string idAbonos, int desde, int hasta, bool modoFacturacionElectronica, string emision, string vencimiento)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (emision != "" && vencimiento != "") { 
            if(DateTime.Parse(emision) > DateTime.Parse(vencimiento))
                throw new Exception("La fecha de vencimiento debe ser mayor a la fecha de emisión");
            }

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            if (itemsSeleccionados == null)
            {
                itemsSeleccionados = Regex.Split(idAbonos, "chkVenta_").ToList();
            }

            var itemsHasta = hasta - desde;
            var listaAbonos = itemsFacturar.Items.Skip(desde).Take(itemsHasta + 1).ToList();

            foreach (var clienteAbono in listaAbonos)
            {
                clienteAbono.EstadoFacturacion = "";

                if (itemsSeleccionados.Contains(clienteAbono.IDComprobante.ToString()))
                {
                    cantidadGenerados = cantidadGenerados + 1;
                    try
                    {
                        generarCae(clienteAbono.IDComprobante.ToString(), emision, vencimiento);
                        //ContabilidadCommon.AgregarAsientoDeVentas(usu, clienteAbono.IDComprobante); No hace falta ya que los asientos están generados

                        /*   if (modoFacturacionElectronica)
                            {
                                generarCae(clienteAbono.IDComprobante.ToString(), clienteAbono.IDIntegracion);
                            }
                            else
                            {
                                facturarVentaBorrador(clienteAbono.Id, clienteAbono.IDIntegracion);
                            }
                            clienteAbono.EstadoFacturacion = "";*/

                    }
                    catch (Exception ex)
                    {
                        cantidadErrores = cantidadErrores + 1;
                        clienteAbono.EstadoFacturacion = string.Format("ERROR comprobante {0}: {1}",
                                 clienteAbono.RazonSocial
                              , ex.Message);
                        ComprobanteCart.Retrieve().Items.Clear();

                        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                        msg = msg + "- USUARIO: " + usu.IDUsuario;
                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
                    }
                }
            }

            return
                new
                {
                    Items = itemsFacturar,
                    Desde = desde,
                    Hasta = hasta,
                    CantidadGenerados = cantidadGenerados,
                    CantidadErrores = cantidadErrores
                };
        }

        throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static ComprobantesDescargasViewModel generarCae(string id, string emision, string vencimiento)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            try
            {
                ComprobanteCart.Dispose();
                var idComprobante = int.Parse(id);
                var comprobante = ComprobantesCommon.ObtenerPorID(idComprobante, usu.IDUsuario);
                //if (String.IsNullOrEmpty(comprobante.Tipo)) comprobante.Tipo = "FCB";
                //var pdv = UsuarioCommon.ObtenerPuntosDeVentaIntegracionesList(comprobante.IDUsuario).FirstOrDefault();
                //comprobante.IDPuntoVenta = pdv != null ? pdv.IDPuntoVenta : 0;
                //comprobante.PuntosDeVenta = pdv;
                //comprobante.TipoDestinatario = "C";
                //comprobante.Observaciones = "";
                //comprobante.FechaComprobante = DateTime.Now.Date;
                //comprobante.FechaVencimiento = DateTime.Now.AddDays(+1).Date;
                //comprobante.FechaAlta = DateTime.Now.Date;

                if (emision != "")
                {
                    comprobante.FechaComprobante = DateTime.Parse(emision);//Si indica fecha de emision la uso y pongo fecha de vencimiento +15 dias.
                    comprobante.FechaVencimiento = comprobante.FechaComprobante.AddDays(15).Date;
                }
                if(vencimiento != "")
                {
                    var fechaVencimiento = DateTime.Parse(vencimiento);
                    if (comprobante.FechaComprobante > fechaVencimiento)//Si indica fecha de vencimiento la seteo y verifico que no sea mayor a la de emisión.
                        throw new Exception("La fecha de vencimiento debe ser mayor a la fecha de emisión");

                    comprobante.FechaVencimiento = fechaVencimiento;
                }


                ComprobanteCart.Retrieve().Init(comprobante);
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                msg = msg + "- USUARIO: " + usu.IDUsuario;
                BasicLog.AppendToFile(
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg,
                    e.ToString());
                throw e;
            }
            try
            {
                return FacturaElectronicaCommon.generarCae(ComprobanteCart.Retrieve().GetComprobanteCartDto());
            }
            catch (Exception e)
            {
                var msg = "El comprobante fue reservado pero no se pudo generar la factura electrónica. " +
                          (e.InnerException != null ? e.InnerException.Message : e.Message);
                msg = msg + "- USUARIO: " + usu.IDUsuario;
                BasicLog.AppendToFile(
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg,
                    e.ToString());
                throw new Exception(msg);
            }

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }


}
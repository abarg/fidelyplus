﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="trackingHorase.aspx.cs" Inherits="modulos_ventas_trackingHorase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
   <%-- <link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" />--%>
    <link rel="stylesheet" href="/css/bootstrap-timepicker.min.css" />
    <style>
        .bootstrap-timepicker .help-block {
            display:none !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><asp:Literal runat="server" ID="litTitulo"></asp:Literal>
        </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><asp:Literal runat="server" ID="litPathPadre"></asp:Literal></li>
                <li class="active"><asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                         <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Datos generales</h3>
                                <label class="control-label">Acá van los datos más generales para crear un seguimiento de horas.</label>
                            </div>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin-left: -10px;">
                                    <div class="col-sm-12">
                                        <label class="control-label"><span class="asterisk">*</span> Fecha </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox runat="server" ID="txtFecha" CssClass="form-control required" placeholder="dd/mm/yyyy" MaxLength="10" Enabled="false"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox runat="server" ID="txtFechaTarea" CssClass="form-control validDate required" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Proveedor/Cliente</label>
                                    <asp:DropDownList runat="server" ID="ddlPersona" CssClass="select2 required" data-placeholder="Seleccione un cliente/proveedor..." ClientIDMode="Static">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Tarea</label>
                                    <%--<asp:DropDownList runat="server" ID="ddlTarea" CssClass="form-control required" ClientIDMode="Static">
                                        <asp:ListItem Text="tarea 1" Value="Tarea1"></asp:ListItem>
                                        <asp:ListItem Text="tarea 2" Value="Tarea2"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                     <asp:TextBox runat="server" ID="ddlTarea" CssClass="form-control required" MaxLength="500" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row mb15">
                             <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Usuario </label>
                                    <asp:DropDownList runat="server" ID="ddlUuarios" CssClass="form-control" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" style="margin-left: -10px;">
                                    <div class="col-sm-12">
                                        <label class="control-label"><span class="asterisk">*</span> Desde/Hasta </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                            <div class="bootstrap-timepicker"><asp:TextBox runat="server" ID="txtDesde" CssClass="form-control horas" placeholder="desde" MaxLength="20"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                            <div class="bootstrap-timepicker"><asp:TextBox runat="server" ID="txtHasta" CssClass="form-control horas" placeholder="hasta" MaxLength="20"></asp:TextBox></div>
                                        </div>
                                    </div>
                                `</div>

                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Duración Hrs.</label>
                                    <asp:TextBox runat="server" ID="txtCantHoras" CssClass="form-control required" MaxLength="20" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                                                    
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Modo</label>
                                    <asp:DropDownList runat="server" ID="ddlEstado" CssClass="form-control" ClientIDMode="Static">
                                        <asp:ListItem Text="Facturable" Value="Facturable"></asp:ListItem>
                                        <asp:ListItem Text="No Facturable" Value="No Facturable"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>     
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Incluir en abono</label>
                                    <asp:DropDownList runat="server" ID="ddlAbono" CssClass="form-control" ClientIDMode="Static">
                                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>   
                            <div class="col-sm-4">
                                <div class="form-group" id="divValorHora">
                                    <label class="control-label"><span class="asterisk">*</span> Valor hora</label>
                                    <asp:TextBox runat="server" ID="txtValorHora" CssClass="form-control required" MaxLength="20" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label"> Observaciones internas</label>
                                    <asp:TextBox runat="server" TextMode="MultiLine" Rows="5" ID="txtObservaciones" CssClass="form-control" MaxLength="128" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-success" id="btnActualizar" onclick="trackingHoras.grabar(false);">Aceptar</a>
                        <a class="btn btn-default" id="btnActualizar" onclick="trackingHoras.grabar(true);">Aceptar y enviar mail</a>
                        <a href="#" onclick="trackingHoras.cancelar();" style="margin-left:20px">Cancelar</a>

                    </div>

                </div>
                <asp:HiddenField runat="server" ID="hdnIDPersona" Value="0" />
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />

            </form>
        </div>
    </div>

     <%--<div class="modal modal fade" id="modalEMail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="litModalOkTitulo">Enviar email </h4>
                </div>
                <div class="modal-body" style="min-height: 200px;">
                    <div class="alert alert-success" id="divOkMail" style="display: none">
                        <strong>Bien hecho! </strong>El mensaje ha sido enviado correctamente
                    </div>
                    <div id="divSendEmail">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-danger" id="divErrorMail" style="display: none">
                                    <strong>Lo sentimos! </strong><span id="msgErrorMail"></span>
                                </div>

                                <form id="frmSendMail">
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Para: <small>(separa las direcciones mediante una coma) </small></p>
                                        <input id="txtEnvioPara" class="form-control required multiemails" type="text" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Asunto: </p>
                                        <input id="txtEnvioAsunto" class="form-control required" type="text" maxlength="150" />
                                        <span id="msgErrorEnvioAsunto" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                    </div>
                                   
                                    <br />
                                    <a type="button" class="btn btn-success" onclick="trackingHoras.enviarMail();" id="btnEnviar">Enviar</a>
                                    <a style="margin-left: 20px" href="#"  data-dismiss="modal">Cancelar</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/bootstrap-timepicker.min.js"></script>
    <script src="/js/views/ventas/trackingHoras.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            trackingHoras.configForm();
        });
    </script>

</asp:Content>

﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using FileHelpers;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Inventario;
using ACHE.Negocio.Common;

public partial class modulos_inventarios_movimientosDeStocke : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MovimientosStockCart.Dispose();
        }
    }

    [WebMethod(true)]
    public static string obtenerItems()
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var list = MovimientosStockCart.Retrieve().Items.OrderBy(x => x.IDMovimiento).ToList();
            if (list.Any())
            {
                int index = 1;
                foreach (var detalle in list)
                {
                    html += "<tr>";
                    html += "<td>" + index + "</td>";

                    html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                    html += "<td style='text-align:left'>" + detalle.Nombre + "</td>";
                    html += "<td style='text-align:left'>" + detalle.StockActual.Replace(",0000", "") + "</td>";
                    html += "<td style='text-align:left'>" + detalle.CantidadTransferida.ToString("#0.00") + "</td>";

                    html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:movimientosStock.eliminarItem(" + detalle.IDMovimiento + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                    html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:movimientosStock.modificarItem(" + detalle.IDMovimiento + ", '"
                        + detalle.IDConcepto.ToString()
                        + "' ,'" + detalle.CantidadTransferida.ToString().Replace(",", ".")
                        + "');\"><i class='fa fa-edit'></i></a></td>";
                    html += "</tr>";

                    index++;
                }
            }

            if (html == "")
                html = "<tr><td colspan='6' style='text-align:center'>No tienes items agregados</td></tr>";

            return html;

        }
        return html;
    }

    [WebMethod(true)]
    public static void agregarItem(int idConcepto, int idInventario, string cantidad)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            if (idConcepto > 0)
            {
                var aux = MovimientosStockCart.Retrieve().Items.Where(x => x.IDConcepto == idConcepto).FirstOrDefault();
                MovimientosStockCart.Retrieve().Items.Remove(aux);
            }
            else
                throw new Exception("Seleccione un producto para poder continuar");

            if (idConcepto > 0 && idInventario > 0)
            {
                var stock = InventariosCommon.obtenerConcepto(usu.IDUsuario, idInventario, idConcepto);
                if (decimal.Parse(cantidad.Replace(".", ",")) > (decimal.Parse(stock.Stock.Replace(".", ",")) - decimal.Parse(stock.StockReservado.Replace(".", ","))))
                    throw new Exception(string.Format("La cantidad a transferir no puede superar al stock con reservas."));

                var det = new MovimientoDetalleViewModel();
                det.IDMovimiento = MovimientosStockCart.Retrieve().Items.Count() + 1;
                det.CantidadTransferida = decimal.Parse(cantidad.Replace(".", ","));
                det.IDConcepto = idConcepto;

                var prod = ConceptosCommon.GetConcepto(idConcepto, usu.IDUsuario);
                var concepto = new Conceptos();
                det.Nombre = prod.Nombre;
                det.Codigo = prod.Codigo;
                det.StockActual = stock.Stock;
                MovimientosStockCart.Retrieve().Items.Add(det);
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarItem(int id)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var aux = MovimientosStockCart.Retrieve().Items.Where(x => x.IDMovimiento == id).FirstOrDefault();
            if (aux != null)
                MovimientosStockCart.Retrieve().Items.Remove(aux);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void limpiarItems()
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            MovimientosStockCart.Dispose();
        }
    }

    [WebMethod(true)]
    public static int cantidadItems()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var aux = MovimientosStockCart.Retrieve().Items.Count();
            if (aux != null)
                return aux;
            else
                return 0;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static ConceptosViewModel obtenerStockConcepto(int idInventario, int idConcepto)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var stockConcepto = InventariosCommon.obtenerConcepto(usu.IDUsuario,idInventario, idConcepto);
            return stockConcepto;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void TransferirStock(int idInventarioOrigen, int idInventarioDestino, string observaciones)
    {
        //string listaDePrecios;
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            if (idInventarioOrigen != idInventarioDestino)
            {
                var list = MovimientosStockCart.Retrieve().Items.OrderBy(x => x.Nombre).ToList();
                if (list.Count() > 0)
                {
                    List<MovimientosDeStockDetalle> listaMovimientos = new List<MovimientosDeStockDetalle>();
                    foreach (var concepto in list)
                    {
                        if (concepto.IDConcepto > 0)
                        {
                            MovimientosDeStockDetalle movimientoDetalle = new MovimientosDeStockDetalle();
                            movimientoDetalle.IDConcepto = concepto.IDConcepto;
                            movimientoDetalle.CantidadTransferida = concepto.CantidadTransferida;
                            listaMovimientos.Add(movimientoDetalle);
                        }
                    }
                    if (listaMovimientos.Count() > 0)
                        InventariosCommon.GenerarMovimientoStockEntreInventarios(idInventarioOrigen, idInventarioDestino, observaciones, usu.IDUsuario, listaMovimientos);
                }
                else
                    throw new Exception("Por favor, agregue algún producto.");
            }
            else
                throw new Exception("Por favor, elija un depósito de origen y destino diferente.");
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión.");
    }

}
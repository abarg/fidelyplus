﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using System.Web.Services;
using ACHE.Negocio.Inventario;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Common;


public partial class modulos_inventarios_movimientosDeStock : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptMovDepositosModelViewModel getResults(int idOrigen, int idDestino, string concepto, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                return InventariosCommon.ReporteMovimientosEntreDepositos(usu.IDUsuario, idOrigen, idDestino, concepto,periodo, fechaDesde, fechaHasta, page, pageSize);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(int idOrigen, int idDestino, string concepto, string periodo, string fechaDesde, string fechaHasta)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return InventariosCommon.ExportarMovimientos(idOrigen, idDestino,concepto,periodo,fechaDesde,fechaHasta,usu.IDUsuario);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
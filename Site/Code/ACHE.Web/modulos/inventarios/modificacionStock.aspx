﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="modificacionStock.aspx.cs" Inherits="modulos_inventarios_modificacionStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-archive'></i>Modificación de stock</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href='#'>Inventarios</a></li>
                <li>Modificación de stock</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="mb15">
	        <div class="alert alert-warning fade in nomargin">
		        El stock de los COMBOS se calcula automáticamente en base al stock de los productos que lo componen por lo que en esta pantalla no podrá buscarlos.
	        </div>
	    </div>

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Depósitos y productos</h3>
                                <label class="control-label">Seleccione el depósito en el cual desea modificar el stock.</label>
                                <br />
                                <br />
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Depósito</label>
                                    <asp:DropDownList runat="server" ID="ddlInventarioOrigen"  CssClass="form-control required"
                                        data-placeholder="Selecciona un deposito..."  onchange="modificacionStock.cambiarInventario();" >
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    

                        <div class="well">
                            <div class="alert alert-danger" id="divErrorDetalle" style="display: none">
                                <strong>Lo siento! </strong><span id="msgErrorDetalle"></span>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-4 col-lg-4 form-group">
                                    <span class="asterisk">*</span> Producto
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <select class="select4" data-placeholder="Seleccione un Producto" id="ddlProductos"  onchange="modificacionStock.obtenerStockConcepto();">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                    Stock actual
                                    <br />
                                    <label id="lblStockActual" style="margin-top: 10px;"/>
                                </div>
                                  <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                    Stock Reservado
                                    <br />
                                    <label id="lblStockReservado"  style="margin-top: 10px;"/>
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                    <span class="asterisk">*</span>
                                    <asp:Literal runat="server">Stock nuevo</asp:Literal>
                                    <input type="text" class="form-control" maxlength="10" id="txtCantidad" />
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-2 form-group" style="margin-top: 20px;">
                                    <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="modificacionStock.agregarItem();">Agregar</a>
                                    <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="modificacionStock.cancelarItem();">Cancelar</a>
                                    <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                                    <input type="hidden" runat="server" id="hdnIDInventarioOrigen" value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-invoice">
                                <thead>
                                    <tr>
                                        <th>#</th>   
                                        <th style="text-align: left">Código</th>
                                        <th style="text-align: left">Producto</th>
                                        <th style="text-align: left">Stock actual</th>
                                        <th style="text-align: left">Stock nuevo</th>
                                        <th style="text-align: right" id="tdPrecio"></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyDetalle">
                                </tbody>
                            </table>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Observaciones</h3>
                                <label class="control-label">No es es necesario agregar una observación para realizar un movimiento de stock, pero podría serte de utilidad ;-)</label>
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <asp:TextBox runat="server" ID="txtObservaciones" CssClass="form-control" MaxLength="150" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-success" id="btnActualizar" onclick="modificacionStock.modificarStock();">Aceptar</a>
                        <a href="#" onclick="modificacionStock.cancelar();" style="margin-left: 20px">Cancelar</a>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
            </form>
        </div>
    </div>




</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/inventarios/modificacionStock.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            modificacionStock.configForm();
        });
    </script>

</asp:Content>


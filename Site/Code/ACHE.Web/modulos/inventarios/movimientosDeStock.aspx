﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="movimientosDeStock.aspx.cs" Inherits="modulos_inventarios_movimientosDeStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-archive'></i>Movimientos entre depósitos</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href='#'>Inventarios</a></li>
                <li>Movimientos entre depósitos</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <asp:HiddenField runat="server" ID="hdnPrecioConIva" />
                            <asp:HiddenField runat="server" ID="hdnTieneFE" Value="0" ClientIDMode="Static" />

                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-5 col-md-10">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3 form-group">
                                            <select id="ddlInventarioOrigen" class="form-control required"
                                                data-placeholder="Selecciona el origen...">
                                            </select>
                                        </div>

                                        <div class="col-sm-12 col-md-3 form-group">
                                            <select id="ddlInventarioDestino" class="form-control required"
                                                data-placeholder="Selecciona el destino...">
                                            </select>
                                        </div>
                                        <div class="col-sm-12 col-md-4 form-group">
                                            <input type="text" class="form-control" id="txtConcepto" maxlength="128" placeholder="Ingresá el nombre o codigo del producto" />
                                        </div>
                                        <div class="col-sm-12 col-md-2 form-group">
                                            <select class="form-control" id="ddlPeriodo" onchange="movimientosStock.otroPeriodo();">
                                                <option value="30" selected="selected">Últimos 30 dias</option>
                                                <option value="15">Últimos 15 dias</option>
                                                <option value="7">Últimos 7 dias</option>
                                                <option value="1">Ayer</option>
                                                <option value="0">Hoy</option>
                                                <option value="-1">Otro período</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="divMasFiltros" style="display: none">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Fecha desde" MaxLength="10" onchange="movimientosStock.filtrar();"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-6 form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Fecha hasta" MaxLength="10" onchange="movimientosStock.filtrar();"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-7 col-md-2 responsive-buttons md">
                                    <div class="btn-group">
                                        <a class="btn btn-warning mr10" onclick="movimientosStock.nuevo();">
                                            <i class="fa fa-plus"></i>&nbsp;Nuevo movimiento
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="col-sm-12"><hr class="mt0" /></div>

                        <div class="row">
                            <div class="pull-right">
                                <div class="btn-group mr10" runat="server" id="divExportar">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:movimientosStock.exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="movimientosStock.resetearExportacion();" download="movimientosDeStock" style="display: none">Descargar</a>
                                    </div>

                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="movimientosStock.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="movimientosStock.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>

                    <!-- panel-heading -->
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Origen</th>
                                        <th>Destino</th>
                                        <th style="min-width: 80px;">Producto</th>
                                        <th>Cantidad</th>
                                        <th>Observaciones</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

       
        <script id="resultTemplate" type="text/x-jQuery-tmpl">
            {{each results}}
                <tr>
                    <td>${Fecha}</td>
                    <td>${Origen}</td>
                    <td>${Destino}</td>
                    <td>${Codigo} - ${Producto}</td>
                    <td>${Cantidad}</td>
                    <td>${Descripcion}</td>
                </tr>
            {{/each}}
        </script>
        <script id="noResultTemplate" type="text/x-jQuery-tmpl">
            <tr>
                <td colspan="6">No se han encontrado resultados</td>
            </tr>
        </script>
    </div>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">

    <script src="/js/views/inventarios/movimientosStock.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            movimientosStock.configFilters();
            movimientosStock.filtrar();
        });
    </script>
</asp:Content>

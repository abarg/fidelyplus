﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="movimientosDeStocke.aspx.cs" Inherits="modulos_inventarios_movimientosDeStocke" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-archive'></i>Movimientos entre depósitos</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href='#'>Inventarios</a></li>
                <li><a href='/modulos/inventarios/movimientosDeStock.aspx'>Movimientos entre depósitos</a></li>
                <li>Alta</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Depósitos y productos</h3>
                                <label class="control-label">Seleccione un depósito de origen para que se carguen los productos y el depósito de destino poder transferir los productos.</label>
                                <br />
                                <br />
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Depósito Origen</label>
                                    <asp:DropDownList runat="server" ID="ddlInventarioOrigen"  CssClass="form-control required"
                                        data-placeholder="Selecciona un deposito..."  onchange="movimientosStock.cambiarInventario();" >
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Depósito Destino</label>
                                    <asp:DropDownList runat="server" ID="ddlInventarioDestino"  CssClass="form-control required"
                                        data-placeholder="Selecciona un deposito...">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    

                        <div class="well">
                            <div class="alert alert-danger" id="divErrorDetalle" style="display: none">
                                <strong>Lo siento! </strong><span id="msgErrorDetalle"></span>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-lg-4 form-group">
                                    <span class="asterisk">*</span> Producto
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <select class="select4" data-placeholder="Seleccione un Producto" id="ddlProductos"  onchange="movimientosStock.obtenerStockConcepto();">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                    Stock Actual
                                    <br />
                                    <label id="lblStockActual" style="margin-top: 10px;"/>
                                </div>
                                 <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                    Stock Reservado
                                    <br />
                                    <label id="lblStockReservado" style="margin-top: 10px;"/>
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-2 form-group">
                                    <span class="asterisk">*</span>
                                    <asp:Literal runat="server">Cant. a transferir</asp:Literal>
                                    <input type="text" class="form-control" maxlength="10" id="txtCantidad" />
                                </div>
                                <div class="col-xs-12 col-md-2 col-lg-2 form-group" style="margin-top: 20px;">
                                    <a class="btn btn-default btn-sm" id="btnAgregarItem" onclick="movimientosStock.agregarItem();">Agregar</a>
                                    <a class="btn btn-default btn-sm" id="btnCancelarItem" onclick="movimientosStock.cancelarItem();">Cancelar</a>
                                    <input type="hidden" runat="server" id="hdnIDItem" value="0" />
                                    <input type="hidden" runat="server" id="hdnIDInventarioOrigen" value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-invoice">
                                <thead>
                                    <tr>
                                        <th>#</th>   
                                        <th style="text-align: left">Código</th>
                                        <th style="text-align: left">Producto</th>
                                        <th style="text-align: left">Stock Actual</th>
                                        <th style="text-align: left">Cantidad a Transferir</th>
                                        <th style="text-align: right" id="tdPrecio"></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyDetalle">
                                </tbody>
                            </table>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Observaciones</h3>
                                <label class="control-label">No es es necesario agregar una observacion para realizar un movimiento de stock, pero podría serte de utilidad ;-)</label>
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <asp:TextBox runat="server" ID="txtObservaciones" CssClass="form-control" MaxLength="150" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-success" id="btnActualizar" onclick="movimientosStock.transferirStock();">Aceptar</a>
                        <a href="movimientosDeStock.aspx" style="margin-left: 20px">Cancelar</a>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
            </form>
        </div>
    </div>




</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/inventarios/movimientosStock.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            movimientosStock.configForm();
        });
    </script>

</asp:Content>


﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model.Negocio;
using FileHelpers;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Inventario;
using ACHE.Negocio.Common;

public partial class modulos_inventarios_modificacionStock : BasePage {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            MovimientosStockCart.Dispose();
        }
    }

    [WebMethod(true)]
    public static string obtenerItems() {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            html = InventariosCommon.ObtenerItems();
        }
        return html;
    }

    [WebMethod(true)]
    public static void agregarItem(int idConcepto, int idInventario, string cantidad) {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            InventariosCommon.AgregarItem(idConcepto, idInventario, cantidad, usu.IDUsuario);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarItem(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            InventariosCommon.EliminarItem(id);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void limpiarItems() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            MovimientosStockCart.Dispose();
        }
    }

    [WebMethod(true)]
    public static int cantidadItems() {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var aux = MovimientosStockCart.Retrieve().Items.Count();
            if (aux != null)
                return aux;
            else
                return 0;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static StockDto obtenerStockConcepto(int idInventario, int idConcepto) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return InventariosCommon.obtenerStockProducto(usu.IDUsuario, idInventario, idConcepto);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void ModificarStock(int idInventarioOrigen, string observaciones) {
        //string listaDePrecios;
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            var list = MovimientosStockCart.Retrieve().Items.OrderBy(x => x.Nombre).ToList();
            if (list.Count() > 0) {
                List<MovimientosDeStockDetalle> listaMovimientos = new List<MovimientosDeStockDetalle>();
                foreach (var concepto in list) {
                    if (concepto.IDConcepto > 0) {
                        MovimientosDeStockDetalle movimientoDetalle = new MovimientosDeStockDetalle();
                        movimientoDetalle.IDConcepto = concepto.IDConcepto;
                        movimientoDetalle.CantidadTransferida = concepto.CantidadTransferida;
                        listaMovimientos.Add(movimientoDetalle);
                    }
                }
                if (listaMovimientos.Count() > 0)
                    InventariosCommon.ActualizarStockMasivo(idInventarioOrigen, observaciones, usu.IDUsuario, listaMovimientos);
            }
            else
                throw new Exception("Por favor, agregue algún producto.");

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión.");
    }

}
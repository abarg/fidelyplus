﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="bancosdetalle.aspx.cs" Inherits="modulos_tesoreria_bancosdetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-university'></i>Movimientos bancarios <span>Administración</span></h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href='#'>Tesorería</a></li>
                <li><a href='/modulos/reportes/DetalleBancario.aspx'>Movimientos bancarios</a></li>
                <li class="active">
                    <asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <form id="frmBancoDetalle" class="col-sm-12" runat="server">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-warning mb15" runat="server" id="divSync">
		                <i class="fa fa-exclamation"></i> Ahora puedes agrupar tus gastos bancarios por categoria!.
	                </div>
                    <div class="alert alert-danger" id="divError" style="display: none">
                        <strong>Lo sentimos! </strong><span id="msgError"></span>
                    </div>

                    <div class="alert alert-success" id="divOk" style="display: none">
                        <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                    </div>

                    <div class="col-sm-12">
                        <div class="row mb15">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Banco</label>
                                    <br />
                                    <asp:DropDownList ID="ddlBanco" runat="server" CssClass="select2 required" data-placeholder="Seleccione un banco..."
                                        DataTextField="Nombre" DataValueField="ID">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Fecha</label>
                                    <br />
                                    <input id="txtFecha" class="form-control  required validDate" placeholder="dd/mm/yyyy" maxlength="10" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Tipo de movimiento</label>
                                    <br />
                                    <asp:RadioButton runat="server" GroupName="TipoMovimiento" ID="chkTipoMovDebe" />&nbsp;<label for="chkTipoMovDebe" style="color: #2b9b8f">Debe (Ingreso)</label>
                                    &nbsp;&nbsp;
                                    <asp:RadioButton runat="server" GroupName="TipoMovimiento" ID="chkTipoMovHaber" Checked="true" />&nbsp;<label for="chkTipoMovHaber" style="color: #bf315f">Haber (Egreso)</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Importe</label>
                                    <input id="txtImporte" class="form-control required" maxlength="128" runat="server" />
                                </div>
                            </div>
                        </div>


                        <div class="row mb15">
                            
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Categoría</label>
                                    <span class="badge badge-success pull-right tooltips" data-toggle="tooltip" data-original-title="Haga click aquí para agregar/modificar/eliminar categorías" style="cursor: pointer" onclick="bancosDetalle.obtenerCategorias();">administrar</span>
                                    <asp:DropDownList runat="server" ID="ddlCategoria" CssClass="form-control chosen-select"
                                        data-placeholder="Seleccione una categoria...">
                                    </asp:DropDownList>
                                    <span class="help-block"><small>Asigna una categoría para poder segmentar tus movimientos bancarios.</small></span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Concepto</label>
                                    <input id="txtConcepto" class="form-control required" maxlength="200" runat="server" />
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Nro de ticket</label>
                                    <input id="txtTicket" class="form-control" maxlength="128" runat="server" />
                                </div>
                            </div>

                            <div class="col-sm-3 divPlanDeCuentas">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Cuenta contable</label>
                                    <asp:DropDownList runat="server" ID="ddlPlanDeCuentas" CssClass="select2 required" data-placeholder="Seleccione una cuenta...">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Observaciones</label>
                                    <textarea rows="5" id="txtObservaciones" class="form-control" runat="server"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a class="btn btn-success" id="btnActualizar" onclick="bancosDetalle.grabar();">Aceptar</a>
                    <a href="/modulos/reportes/DetalleBancario.aspx" tabindex="14" style="margin-left: 20px">Cancelar</a>
                </div>
            </div>
            <input id="hdnID" type="hidden" value="0" runat="server" />
            <input id="hdnUsaPlanCorporativo" type="hidden" value="0" runat="server" />
        </form>
    </div>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalCategorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Administración de categorías</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="alert alert-danger" id="divErrorCat" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgErrorCat"></span>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" maxlength="50" id="txtNuevaCat" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2"><a class="btn btn-default" id="btnCategoria" onclick="bancosDetalle.grabarCategoria();">Agregar</a></div>

                        <br />
                        <br />
                        <br />

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyDetalle">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <input type="hidden" id="hdnIDCategoria" value="0" />
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/jasny-bootstrap.min.js"></script>
    <script src="/js/views/tesoreria/bancosDetalle.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            bancosDetalle.configForm();
        });
    </script>

</asp:Content>



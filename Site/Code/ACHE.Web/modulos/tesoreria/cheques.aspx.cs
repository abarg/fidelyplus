﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using System.Web.Services;
using ACHE.Negocio.Contabilidad;

public partial class modulos_Tesoreria_cheques : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                var TieneDatos = dbContext.Cheques.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos)
                {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else
                {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);
            CargarCombos();
        }
    }

    private void CargarCombos()
    {
        using (var dbContext = new ACHEEntities())
        {
            var listaBancos = dbContext.Bancos.Where(x => x.IDUsuario == CurrentUser.IDUsuario).OrderBy(x => x.BancosBase.Nombre).ToList();
            foreach (var item in listaBancos)
                ddlBancos.Items.Add(new ListItem(item.BancosBase.Nombre + " Nro:" + item.NroCuenta, item.IDBanco.ToString()));

            var listaCajas = dbContext.Cajas.Where(x => x.IDUsuario == CurrentUser.IDUsuario).OrderBy(x => x.Nombre).ToList();
            foreach (var item in listaCajas)
                ddlCajas.Items.Add(new ListItem(item.Nombre, item.IDCaja.ToString()));
        }
    }

    [WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    //TODO: Informar en qué pago fue utilizado.
                    if (dbContext.PagosFormasDePago.Any(x => x.IDCheque == id))
                        throw new Exception("No puede eliminarse el cheque, ya que fue utilizado en un pago");
                    if (dbContext.CobranzasFormasDePago.Any(x => x.IDCheque == id))
                        throw new Exception("No puede eliminarse el cheque, ya que fue utilizado en una cobranza");
                    else
                    {
                        var entity = dbContext.Cheques.Where(x => x.IDCheque == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (entity != null)
                        {
                            dbContext.Database.ExecuteSqlCommand("delete asientos where idcheque =" + entity.IDCheque);

                            dbContext.Cheques.Remove(entity);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosChequesViewModel getResults(string tipo, string condicion, string factura, int page, int pageSize, string estado)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptChequesAcciones.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                    if (condicion != string.Empty)
                        results = results.Where(x => x.Emisor.Contains(condicion) || x.Banco.Contains(condicion) || x.Numero.Contains(condicion));

                    if (tipo != string.Empty) {
                        if (tipo == "P")
                            results = results.Where(x => x.esPropio);
                        else
                            results = results.Where(x => !x.esPropio);
                    }

                    var fecha = DateTime.Now.Date;
                    switch (estado.ToLower())
                    {
                        case "en cartera":
                            results = results.Where(x => x.Accion == "" && x.Estado == "Libre");
                            break;
                        case "acreditados":
                            results = results.Where(x => x.Accion == "Acreditado");
                            break;
                        case "a vencer":
                            results = results.Where(x => x.FechaVencimiento >= fecha && x.Accion == "" && x.Estado == "Libre");
                            break;
                        case "ventanilla":
                            results = results.Where(x => x.Accion == "Ventanilla" && !x.esPropio);
                            break;
                        case "debitados":
                            results = results.Where(x => x.Accion == "Debitado" && x.esPropio);
                            break;
                        case "depositados":
                            results = results.Where(x => x.Accion == "Depositado");
                            break;
                        case "rechazados":
                            results = results.Where(x => x.Accion == "rechazado");
                            break;
                        case "vencidos":
                            results = results.Where(x => x.FechaVencimiento <= fecha && x.Accion == "" && x.Estado == "Libre");
                            break;
                        case "utilizados":
                            results = results.Where(x => x.Accion == "" && x.Estado == "Usado");
                            break;
                    }

                    var aux = results;

                    if (factura != string.Empty)
                    {
                        var cheques = dbContext.ChequesFiltroView.Where(x => x.Factura.Contains(factura) && x.IDUsuario == usu.IDUsuario).Select(x => new { IDCheque = x.IDCheque }).ToList();
                        if (cheques.Any())
                        {
                            var listFinal = new List<RptChequesAcciones>();
                            foreach (var chq in cheques)
                            {
                                var auxCheque = results.Where(x => x.IDCheque == chq.IDCheque).FirstOrDefault();
                                if (auxCheque != null)
                                    listFinal.Add(auxCheque);
                            }

                            aux = listFinal.AsQueryable();

                        }
                        else
                            aux = new List<RptChequesAcciones>().AsQueryable();
                    }

                    page--;
                    ResultadosChequesViewModel resultado = new ResultadosChequesViewModel();

                    resultado.TotalPage = ((aux.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = aux.Count();

                    var list = aux.OrderBy(x => x.FechaCobro).Skip(page * pageSize).Take(pageSize).ToList()
                     .Select(x => new ChequesViewModel()
                     {
                         ID = x.IDCheque,
                         Banco = x.Banco.ToUpper(),
                         Numero = x.Numero,
                         Importe = x.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()),
                         Estado = x.Accion,
                         FechaEmision = x.FechaEmision.ToString("dd/MM/yyyy"),
                         FechaCobro = x.FechaCobro.Value.ToString("dd/MM/yyyy"),
                         FechaVencimiento = x.FechaVencimiento.Value.ToString("dd/MM/yyyy"),
                         //CantDiasVencimientos = ((fecha - Convert.ToDateTime(x.FechaCobro)).Days < 0 || x.Accion != "") ? ((fecha - Convert.ToDateTime(x.FechaCobro)).Days * -1).ToString() : "-",
                         CantDiasVencimientos = ((Convert.ToDateTime(x.FechaCobro) - fecha).Days > 0) ? Math.Abs((Convert.ToDateTime(x.FechaCobro) - fecha).Days).ToString() : "-",
                         Emisor = x.Emisor,
                         Observaciones = x.Observaciones
                     });


                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(string tipo, string condicion, string factura, string estado)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Cheques_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptChequesAcciones.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                    if (condicion != string.Empty)
                        results = results.Where(x => x.Emisor.Contains(condicion) || x.Banco.Contains(condicion) || x.Numero.Contains(condicion));

                    if (tipo != string.Empty)
                    {
                        if (tipo == "P")
                            results = results.Where(x => x.esPropio);
                        else
                            results = results.Where(x => !x.esPropio);
                    }

                    var fecha = DateTime.Now.Date;
                    switch (estado.ToLower())
                    {
                        case "en cartera":
                            results = results.Where(x => x.Accion == "" && x.Estado == "Libre");
                            break;
                        case "acreditados":
                            results = results.Where(x => x.Accion == "Acreditado");
                            break;
                        case "a vencer":
                            results = results.Where(x => x.FechaVencimiento >= fecha && x.Accion == "");
                            break;
                        case "ventanilla":
                            results = results.Where(x => x.Accion == "Ventanilla" && !x.esPropio);
                            break;
                        case "debitados":
                            results = results.Where(x => x.Accion == "Debitado");
                            break;
                        case "depositados":
                            results = results.Where(x => x.Accion == "Depositado");
                            break;
                        case "rechazados":
                            results = results.Where(x => x.Accion == "rechazado");
                            break;
                        case "vencidos":
                            results = results.Where(x => x.FechaVencimiento <= fecha && x.Accion == "");
                            break;
                        case "utilizados":
                            results = results.Where(x => x.Accion == "" && x.Estado == "Usado");
                            break;
                    }

                    var aux = results;

                    if (factura != string.Empty)
                    {
                        var cheques = dbContext.ChequesFiltroView.Where(x => x.Factura.Contains(factura) && x.IDUsuario == usu.IDUsuario).Select(x => new { IDCheque = x.IDCheque }).ToList();
                        if (cheques.Any())
                        {
                            var listFinal = new List<RptChequesAcciones>();
                            foreach (var chq in cheques)
                            {
                                var auxCheque = results.Where(x => x.IDCheque == chq.IDCheque).FirstOrDefault();
                                if (auxCheque != null)
                                    listFinal.Add(auxCheque);
                            }

                            aux = listFinal.AsQueryable();

                        }
                        else
                            aux = new List<RptChequesAcciones>().AsQueryable();
                    }

                    dt = aux.OrderBy(x => x.FechaCobro).ToList().Select(x => new
                    {
                        //Banco = x.BancosBase.Nombre.ToUpper(),
                        //Numero = x.Numero,
                        //Importe = x.Importe,
                        ////Estado = x.ChequeAccion.Any() ? x.ChequeAccion.OrderByDescending(y => y.Fecha).First().Accion : "Cargado",
                        //FechaEmision = x.FechaEmision.ToString("dd/MM/yyyy"),
                        //Emisor = x.Emisor

                        Banco = x.Banco.ToUpper(),
                        Numero = x.Numero,
                        Importe = x.Importe,
                        Estado = x.Accion,
                        FechaEmision = x.FechaEmision.ToString("dd/MM/yyyy"),
                        FechaCobro = x.FechaCobro.Value.ToString("dd/MM/yyyy"),
                        FechaVencimiento = x.FechaVencimiento.Value.ToString("dd/MM/yyyy"),
                        //CantDiasVencimientos = ((fecha - Convert.ToDateTime(x.FechaCobro)).Days < 0 || x.Accion != "") ? ((fecha - Convert.ToDateTime(x.FechaCobro)).Days * -1).ToString() : "-",
                        CantDiasVencimientos = ((Convert.ToDateTime(x.FechaCobro) - fecha).Days > 0) ? Math.Abs((Convert.ToDateTime(x.FechaCobro) - fecha).Days).ToString() : "-",
                        Emisor = x.Emisor,
                        Observaciones = x.Observaciones
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    #region ACCIONES

    [WebMethod(true)]
    public static int guardarAccion(int idCheque, string accion, string fechaDeposito, int idBanco, int idCaja)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var idGenerado = 0;
            bool esPropio = false;
            using (var dbContext = new ACHEEntities())
            {
                DateTime fDeposito = Convert.ToDateTime(fechaDeposito);

                var cheque = dbContext.Cheques.Include("ChequeAccion").Where(x => x.IDUsuario == usu.IDUsuario && x.IDCheque == idCheque).FirstOrDefault();
                if (accion == "Depositado" && fDeposito < cheque.FechaEmision)
                    throw new Exception("La fecha de depósito no puede ser menor a la fecha de emisión del cheque: " + cheque.FechaEmision.ToString("dd/MM/yyyy"));
                //if (accion == "Depositado" && cheque.FechaCobro.HasValue && fDeposito < cheque.FechaCobro)
                //    throw new Exception("La fecha de depósito no puede ser menor a la fecha de cobro del cheque: " + cheque.FechaCobro.Value.ToString("dd/MM/yyyy"));
                if (accion == "Ventanilla" && cheque.FechaCobro.HasValue && fDeposito < cheque.FechaCobro)
                    throw new Exception("La fecha de cobro no puede ser menor a la fecha de cobro del cheque: " + cheque.FechaCobro.Value.ToString("dd/MM/yyyy"));

                esPropio = cheque.EsPropio;

                ChequeAccion entity = new ChequeAccion();
                entity.FechaAlta = DateTime.Now;
                entity.IDUsuario = usu.IDUsuario;

                entity.IDCheque = idCheque;
                entity.Accion = accion;
                entity.Fecha = fDeposito;//Convert.ToDateTime(fechaDeposito);

                if (accion == "Depositado" || accion == "Debitado")
                    entity.IDBanco = idBanco;
                if (accion == "Ventanilla")
                    entity.IDCaja = idCaja;
                else if (accion == "Acreditado")
                {
                    var auxCheque = cheque.ChequeAccion.Where(x => x.Accion == "Depositado").FirstOrDefault();
                    if (auxCheque != null)
                        entity.IDBanco = auxCheque.IDBanco;
                }

                if (!cheque.ChequeAccion.Any(x => x.Accion == accion))
                {

                    dbContext.ChequeAccion.Add(entity);
                    dbContext.SaveChanges();

                    idGenerado = entity.IDChequeAccion;
                }

            }
            if (accion == "Depositado" || accion == "Ventanilla" || (accion == "Debitado" && esPropio))
                ContabilidadCommon.AgregarAsientoChequeAccion(usu, idGenerado);
            return idGenerado;

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void deleteAccion(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.ChequeAccion.Where(x => x.IDChequeAccion == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.ChequeAccion.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void guardarFechaMovimiento(int id, string fecha) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.ChequeAccion.Include("Cheques").Where(x => x.IDChequeAccion == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null) {
                        entity.Fecha = DateTime.Parse(fecha);
                        dbContext.SaveChanges();

                        if (entity.Accion == "Depositado" || entity.Accion == "Ventanilla" || (entity.Accion == "Debitado" && entity.Cheques.EsPropio))
                            ContabilidadCommon.AgregarAsientoChequeAccion(usu, entity.IDChequeAccion);
                    }
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosChequesViewModel getResultsAccion(int idCheque, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.ChequeAccion.Include("Cajas").Include("Cheques").Include("Cheques.BancosBase")
                        .Include("Bancos").Include("Bancos.BancosBase")
                        .Where(x => x.IDUsuario == usu.IDUsuario && x.IDCheque == idCheque).AsQueryable();

                    page--;
                    ResultadosChequesViewModel resultado = new ResultadosChequesViewModel();

                    var list = results.OrderBy(x => x.IDChequeAccion).ToList().Skip(page * pageSize).Take(pageSize).ToList()
                     .Select(x => new ChequesViewModel()
                     {
                         ID = x.IDChequeAccion,
                         Banco = "",// " en " + (x.IDBanco.HasValue ? x.Bancos.BancosBase.Nombre.ToUpper() : (x.IDCaja.HasValue ? x.Cajas.Nombre : "")),
                         Numero = x.Cheques.Numero,
                         Importe = x.Cheques.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()),
                         Accion = x.Accion != "Ventanilla" ? x.Accion : "Cobro por ventanilla",
                         FechaEmision = x.Fecha.ToString("dd/MM/yyyy"),
                         Emisor = x.Cheques.Emisor,
                         EsEditable = true
                     }).ToList();

                    var filtros = dbContext.ChequesFiltroView.Where(x => x.IDCheque == idCheque && x.IDUsuario == usu.IDUsuario).ToList();
                    var cobranza = filtros.Where(x => x.Tipo == "C").FirstOrDefault();
                    var pago = filtros.Where(x => x.Tipo == "P").FirstOrDefault();
                    if (cobranza != null)
                    {
                        var aux = dbContext.Cobranzas.Include("Personas").Where(x => x.IDCobranza == cobranza.IDAuxiliar && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (aux != null)
                        {
                            var cobrado = new ChequesViewModel();
                            cobrado.Accion = "Cobranza a " + aux.Personas.RazonSocial + " (" + aux.Personas.TipoDocumento + " " + aux.Personas.NroDocumento + ") - " + aux.PuntosDeVenta.Punto.ToString("#0000") + "-" + aux.Numero.ToString("#00000000");
                            cobrado.Accion += "&nbsp;<a href='/cobranzase.aspx?ID=" + aux.IDCobranza + "'><small>ver detalle</small></a>";
                            cobrado.FechaEmision = aux.FechaCobranza.ToString("dd/MM/yyyy");
                            cobrado.EsEditable = false;

                            list.Insert(0, cobrado);
                        }
                    }
                    if (pago != null)
                    {
                        var aux = dbContext.Pagos.Include("Personas").Where(x => x.IDPago == pago.IDAuxiliar && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (aux != null)
                        {
                            var pagado = new ChequesViewModel();
                            pagado.Accion = "Pago a " + aux.Personas.RazonSocial + " (" + aux.Personas.TipoDocumento + " " + aux.Personas.NroDocumento + ") - " + pago.Factura;
                            pagado.Accion += "&nbsp;<a href='/modulos/compras/pagose.aspx?ID=" + aux.IDPago + "'><small>ver detalle</small></a>";
                            pagado.FechaEmision = aux.FechaPago.ToString("dd/MM/yyyy");
                            pagado.EsEditable = false;

                            list.Add(pagado);
                        }
                    }

                    resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = list.Count();
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static ChequesViewModel cargarEntidadAccion(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                ChequesViewModel cheque = new ChequesViewModel();

                var entity = dbContext.ChequeAccion.Where(x => x.IDChequeAccion == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (entity != null)
                {
                    cheque.ID = entity.IDChequeAccion;
                    cheque.Accion = entity.Accion;
                    cheque.IDCheque = entity.IDCheque;
                    cheque.FechaEmision = entity.Fecha.ToString("dd/MM/yyyy");
                    return cheque;
                }

                return cheque;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerChequesSegunAcciones(string accion)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var listaAux = dbContext.RptChequesAcciones.Where(x => x.Accion != accion && x.IDUsuario == usu.IDUsuario).AsQueryable();
                switch (accion)
                {
                    case "Rechazado":
                        listaAux = listaAux.Where(x => x.Accion == "Depositado");
                        break;
                    case "Debitado":
                        listaAux = listaAux.Where(x => x.Accion == "" && x.esPropio);
                        break;
                    case "Depositado":
                        listaAux = listaAux.Where(x => x.Accion == "" && x.Estado == "Libre");
                        break;
                    case "Acreditado":
                        var fecha = DateTime.Now.Date;
                        listaAux = listaAux.Where(x => x.Accion == "Depositado");//&& x.FechaCobro >= fecha
                        break;
                    case "Ventanilla":
                        listaAux = listaAux.Where(x => x.Accion == "" && x.Estado == "Libre");
                        break;
                }

                var lista = listaAux.OrderBy(x => x.FechaEmision).ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDCheque,
                        Nombre = x.Banco + " - Nro:" + x.Numero + "  $" + x.Importe.ToString()
                    }).ToList();

                return listaAux.OrderBy(x => x.FechaEmision).ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDCheque,
                        Nombre = x.Banco + " - Nro:" + x.Numero + "  $" + x.Importe.ToString()
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    #endregion
}
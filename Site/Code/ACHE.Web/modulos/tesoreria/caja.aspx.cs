﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using System.Web.Services;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Tesoreria;
using ACHE.Extensions;

public partial class modulos_Tesoreria_caja : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //using (var dbContext = new ACHEEntities())
            //{
            //    var TieneDatos = dbContext.CajaView.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
            //    if (TieneDatos)
            //    {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                   
            //    }
            //    else
            //    {
            //        divConDatos.Visible = false;
            //        divSinDatos.Visible = true;
            //    }
            //}
                    divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
            CurrentUser.TipoUsuario);
        }
    }

    [WebMethod(true)]
    public static void delete(int id, string motivo)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                CajaCommon.EliminarMovimiento(id, motivo, usu);
            }
            else
                throw new CustomException("Por favor, vuelva a iniciar sesión");
        }
        catch (CustomException e)
        {
            throw new CustomException(e.Message);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void revertir(int id, string motivo)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                CajaCommon.RevertirMovimiento(id, motivo, usu);
            }
            else
                throw new CustomException("Por favor, vuelva a iniciar sesión");
        }
        catch (CustomException e)
        {
            throw new CustomException(e.Message);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string getCajas()
    {

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var html = "";
           
                var list = CajaCommon.ObtenerCajas(usu.IDUsuario);
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.Nombre + "</td>";
                        html += "<td><a href='#' title='Editar' style='font-size: 16px;' onclick=\"caja.editarCaja(" + detalle.IDCaja + ",'" + detalle.Nombre.Trim() + "');\"><i class='fa fa-pencil'></i></a>&nbsp;<a href='#' title='Eliminar' style='font-size: 16px;' onclick='caja.eliminarCaja(" + detalle.IDCaja + ");'><i class='fa fa-times'></i></a></td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='2'>No hay un detalle disponible</td></tr>";

            return html;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarCaja(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            CajaCommon.EliminarCajaFisica(id,usu.IDUsuario);
            using (var dbContext = new ACHEEntities())
            {
                if (!dbContext.PagosFormasDePago.Any(x => x.IDCaja == id && x.Pagos.IDUsuario == usu.IDUsuario) && !dbContext.CobranzasFormasDePago.Any(x => x.IDCaja == id && x.Cobranzas.IDUsuario == usu.IDUsuario))
                {
                    Cajas entity = dbContext.Cajas.Where(x => x.IDCaja == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        try
                        {
                            dbContext.Database.ExecuteSqlCommand("DELETE CajasPlanDeCuenta WHERE IDCaja=" + id, new object[] { });
                            
                            dbContext.Cajas.Remove(entity);
                            dbContext.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.InnerException.Message);
                        }
                    }
                }
                else
                    throw new Exception("La caja se encuentra asociada a 1 o más pagos/cobranazas registradas.");

            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosCajaViewModel getResults(int idCaja,string tipoMovimiento, string fechaDesde, string fechaHasta, string periodo, bool incluirAsientos, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                return CajaCommon.ObtenerCaja(idCaja,tipoMovimiento, fechaDesde, fechaHasta, periodo, incluirAsientos, page, pageSize, usu.IDUsuario);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (CustomException e)
        {
            throw new CustomException(e.Message);
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(int idCaja,string tipoMovimiento, string fechaDesde, string fechaHasta, string periodo,bool incluirAsientos)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Caja_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                var results = CajaCommon.ObtenerCaja(idCaja,tipoMovimiento, fechaDesde, fechaHasta, periodo, incluirAsientos, 1, 100000, usu.IDUsuario);
                /*using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.CajaView.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                    if (tipoMovimiento != string.Empty)
                        results = results.Where(x => x.TipoMovimiento == tipoMovimiento);

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.Fecha <= dtHasta);
                    }



                    var lista = results.OrderBy(x => x.Fecha).ToList().Select(x => new CajaViewModel()
                    {
                        ID = x.ID,
                        tipoMovimiento = x.TipoMovimiento,
                        Fecha = Convert.ToDateTime(x.Fecha).ToString("dd/MM/yyyy"),
                        Concepto = (x.ConceptosCaja == null) ? "" : x.ConceptosCaja,
                        Estado = x.Estado,
                        FechaAnulacion = (!string.IsNullOrWhiteSpace(x.FechaAnulacion.ToString())) ? Convert.ToDateTime(x.FechaAnulacion).ToString("dd/MM/yyyy") : "",
                        Ingreso = (x.TipoMovimiento == "Ingreso") ? Math.Abs(x.Importe).ToMoneyFormat() : "0",
                        Egreso = (x.TipoMovimiento == "Egreso") ? (-1 * Math.Abs(x.Importe)).ToMoneyFormat() : "0",
                        MedioDePago = x.MedioDePago,
                        Ticket = x.Ticket,
                        Observaciones = x.Observaciones
                    }).ToList();

                    decimal saldo = 0;
                    for (int i = lista.Count - 1; i >= 0; i--)
                    {
                        var ingreso = Convert.ToDecimal((lista[i].Ingreso == "") ? "0" : lista[i].Ingreso);
                        var egreso = Convert.ToDecimal((lista[i].Egreso == "") ? "0" : lista[i].Egreso);
                        saldo = saldo + (Math.Abs(ingreso) - Math.Abs(egreso));
                        lista[i].Saldo = saldo.ToMoneyFormat();
                    }

                    dt = lista.OrderByDescending(x => x.ID).ToList().Select(x => new
                    {
                        tipoMovimiento = x.tipoMovimiento,
                        Fecha = Convert.ToDateTime(x.Fecha).ToString("dd/MM/yyyy"),
                        Concepto = x.Concepto,
                        Estado = x.Estado,
                        FechaAnulacion = x.FechaAnulacion,
                        MedioDePago = x.MedioDePago,
                        Ticket = x.Ticket,
                        Observaciones = x.Observaciones,
                        Debe = Convert.ToDecimal(x.Ingreso),
                        Haber = Convert.ToDecimal(x.Egreso),
                        Saldo = Convert.ToDecimal(x.Saldo)
                    }).ToList().ToDataTable();
                }*/

                if (results.Items.Any())
                {

                    var aux = new List<CajaViewModel>();
                    foreach (var meses in results.Items)
                    {
                        aux.AddRange(meses.Conceptos);
                    }

                    dt = aux.ToList().Select(x => new
                    {
                        Caja = x.NombreCaja,
                        Fecha = x.Fecha,
                        Concepto = x.Concepto,
                        Estado = x.Estado,
                        FechaAnulacion = x.FechaAnulacion,
                        MedioDePago = x.MedioDePago,
                        Ticket = x.Ticket,
                        Observaciones = x.Observaciones,
                        Debe = x.Ingreso!=""? Convert.ToDecimal(x.Ingreso):0,
                        Haber = x.Egreso != "" ? Convert.ToDecimal(x.Egreso) : 0,
                        Saldo = x.Saldo != "" ? Convert.ToDecimal(x.Saldo) : 0
                        
                    }).ToList().ToDataTable();

                    if (dt.Rows.Count > 0)
                        CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                    else
                        throw new Exception("No se encuentran datos para los filtros seleccionados");

                    return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void cerrarCajas()
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    dbContext.Database.ExecuteSqlCommand("update Caja set Estado ='Conciliado' where idUsuario=" + usu.IDUsuario);
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static void guardarCaja(int id, string nombre)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                if (id == 0 && dbContext.Cajas.Any(x => x.Nombre.ToLower() == nombre.ToLower() && x.IDUsuario == usu.IDUsuario))
                    throw new Exception("El nombre ingresado ya se encuentra creado");

                Cajas entity;
                if (id > 0)
                    entity = dbContext.Cajas.Where(x => x.IDCaja == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                else
                {
                    entity = new Cajas();
                    entity.IDUsuario = usu.IDUsuario;
                }
                entity.Nombre = nombre;

                if (id > 0)
                {
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.Cajas.Add(entity);
                    dbContext.SaveChanges();
                }

                ContabilidadCommon.CrearCuentaCaja(entity.IDCaja, usu);
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string ObtenerTotalSinConsolidar(int id)
    {
        var resultado = "0";
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var listaConsolidados = dbContext.CajaView.Where(x => x.Estado == "Cargado" && x.IDCajas == id && x.IDUsuario == usu.IDUsuario).ToList();

                    if (listaConsolidados.Count() > 0)
                    {
                        var Ingreso = listaConsolidados.Where(x => x.TipoMovimiento == "Ingreso").Sum(x => Math.Abs(x.Importe));
                        var Egreso = listaConsolidados.Where(x => x.TipoMovimiento == "Egreso").Sum(x => Math.Abs(x.Importe));
                        resultado = (Ingreso - Egreso).ToMoneyFormat(2);
                    }
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
        return resultado;
    }
}
﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_Tesoreria_gastosBancariose : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            litPath.Text = "Alta";
            litTotal.Text = "0.00";

            cargarBancos();

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0")
                {
                    cargarEntidad(int.Parse(hdnID.Value));
                    litPath.Text = "Edición";
                }
            }
            else
                txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");

        }
    }

    private void cargarBancos()
    {
        using (var dbContext = new ACHEEntities())
        {
            foreach (var item in dbContext.Bancos.Where(x => x.IDUsuario == CurrentUser.IDUsuario).ToList())
                ddlBanco.Items.Add(new ListItem(item.BancosBase.Nombre + " - " + item.NroCuenta, item.IDBanco.ToString()));
        }
    }

    private void cargarEntidad(int id)
    {
        var decimalFormat = DecimalFormatter.GetDecimalStringFormat(CurrentUser.IDUsuario);

        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.GastosBancarios.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDGastosBancarios == id).FirstOrDefault();
            if (entity != null)
            {
                ddlBanco.SelectedValue = entity.IDBanco.ToString();
                txtFecha.Text = entity.Fecha.ToString("dd/MM/yyyy");

                txtImporte.Text = entity.Importe.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtIVA.Text = entity.IVA.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtDebito.Text = entity.Debito.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtCredito.Text = entity.Credito.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtIIBB.Text = entity.IIBB.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtOtros.Text = entity.Otros.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtImporte21.Text = entity.Importe21.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtCreditoComputable.Text = entity.CreditoComputable.ToString(decimalFormat).Replace(".", "").Replace(",", ".");

                txtPercepcionIVA.Text = entity.PercepcionIVA.ToString().Replace(".", "").Replace(",", ".");
                txtSIRCREB.Text = entity.SIRCREB.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtImporte10.Text = entity.Importe10.ToString(decimalFormat).Replace(".", "").Replace(",", ".");

                txtIntereses.Text = entity.Intereses.ToString(decimalFormat).Replace(".", "").Replace(",", ".");
                txtImpuestosYSellos.Text = entity.ImpuestosYSellos.ToString(decimalFormat).Replace(".", "").Replace(",", ".");

                txtConcepto.Text = entity.Concepto;

                litTotal.Text = (entity.Importe + entity.IVA + entity.Debito + entity.Credito
                                + entity.IIBB + entity.Otros + entity.Importe21 + entity.CreditoComputable
                                + entity.PercepcionIVA + entity.SIRCREB + entity.Importe10
                                + entity.Intereses + entity.ImpuestosYSellos).ToString(decimalFormat).Replace(",", ".");
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, int idBanco, string fecha, string importe, string iva, string debito,
        string credito, string IIBB, string otros, string importe21, string creditoComputable, string concepto,
        string percepcionIVA, string SIRCREB, string importe10, string intereses, string impSellos)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var idGastosBancarios = 0;
            using (var dbContext = new ACHEEntities())
            {

                GastosBancarios entity;
                if (id > 0)
                    entity = dbContext.GastosBancarios.Where(x => x.IDGastosBancarios == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                else
                {
                    entity = new GastosBancarios();
                    entity.FechaAlta = DateTime.Now;
                    entity.IDUsuario = usu.IDUsuario;
                }

                entity.IDBanco = idBanco;
                entity.Fecha = Convert.ToDateTime(fecha);

                entity.Importe = (importe != string.Empty) ? decimal.Parse(importe.Replace(".", ",")) : 0;
                entity.IVA = (iva != string.Empty) ? decimal.Parse(iva.Replace(".", ",")) : 0;
                entity.Debito = (debito != string.Empty) ? decimal.Parse(debito.Replace(".", ",")) : 0;
                entity.Credito = (credito != string.Empty) ? decimal.Parse(credito.Replace(".", ",")) : 0;
                entity.IIBB = (IIBB != string.Empty) ? decimal.Parse(IIBB.Replace(".", ",")) : 0;
                entity.Otros = (otros != string.Empty) ? decimal.Parse(otros.Replace(".", ",")) : 0;
                entity.Importe21 = (importe21 != string.Empty) ? decimal.Parse(importe21.Replace(".", ",")) : 0;
                entity.CreditoComputable = (creditoComputable != string.Empty) ? decimal.Parse(creditoComputable.Replace(".", ",")) : 0;

                entity.PercepcionIVA = (IIBB != string.Empty) ? decimal.Parse(percepcionIVA.Replace(".", ",")) : 0;
                entity.SIRCREB = (SIRCREB != string.Empty) ? decimal.Parse(SIRCREB.Replace(".", ",")) : 0;
                entity.Importe10 = (importe10 != string.Empty) ? decimal.Parse(importe10.Replace(".", ",")) : 0;
                entity.Intereses = (intereses != string.Empty) ? decimal.Parse(intereses.Replace(".", ",")) : 0;
                entity.ImpuestosYSellos = (impSellos != string.Empty) ? decimal.Parse(impSellos.Replace(".", ",")) : 0;

                entity.Concepto = concepto;

                if (entity.CreditoComputable > 0 && entity.Credito == 0)
                    throw new Exception("Debe ingresar el importe del crédito NO computable");

                if (id > 0)
                {
                    dbContext.SaveChanges();
                }
                else
                {
                    dbContext.GastosBancarios.Add(entity);
                    dbContext.SaveChanges();
                }

                idGastosBancarios = entity.IDGastosBancarios;
            }

            ContabilidadCommon.AgregarAsientoDeGastoBancario(idGastosBancarios, usu);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
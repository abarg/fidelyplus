﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using System.Web.Services;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Tesoreria;
using ACHE.Extensions;
using ACHE.Negocio.Banco;
using ACHE.Negocio.Facturacion;

public partial class modulos_tesoreria_bancosdetalle : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            litPath.Text = "Alta";
            txtFecha.Value = DateTime.Now.ToString("dd/MM/yyyy");
            CargarInfo();
            hdnID.Value = Request.QueryString["ID"];
            int id = 0;
            if (int.TryParse(hdnID.Value, out id))
            {
                CargarEntidad(id);
                litPath.Text = "Edición";
            }

            cargarCategorias();

        }
    }

    private void CargarInfo()
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var bancosList = dbContext.Bancos.Include("BancosBase").Where(x => x.IDUsuario == CurrentUser.IDUsuario).OrderBy(x => x.BancosBase.Nombre)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDBanco,
                        Nombre = x.BancosBase.Nombre + " - Nro Cuenta:" + x.NroCuenta
                    }).ToList();

                ddlBanco.DataSource = bancosList;
                ddlBanco.DataBind();

                if (CurrentUser.UsaPlanCorporativo) //Plan Corporativo
                {
                    hdnUsaPlanCorporativo.Value = "1";

                    if (dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == CurrentUser.IDUsuario))
                    {
                        var listaAux = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario).OrderBy(x => x.Codigo).ToList();
                        ddlPlanDeCuentas.Items.Add(new ListItem("", ""));
                        foreach (var item in listaAux)
                            ddlPlanDeCuentas.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CargarEntidad(int id)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.BancosDetalle.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDBancoDetalle == id).FirstOrDefault();

            txtConcepto.Value = entity.Concepto;
            txtObservaciones.Value = entity.Observaciones;
            txtImporte.Value = entity.Importe.ToString().Replace(",", ".");
            if (entity.TipoMovimiento == "Ingreso")
            {
                chkTipoMovDebe.Checked = true;
                chkTipoMovHaber.Checked = false;
            }
            else
            {
                chkTipoMovHaber.Checked = true;
                chkTipoMovDebe.Checked = false;
            }
            txtFecha.Value = Convert.ToDateTime(entity.Fecha).ToString("dd/MM/yyyy");
            txtTicket.Value = entity.Ticket;
            ddlPlanDeCuentas.SelectedValue = Convert.ToInt32(entity.IDPlanDeCuenta).ToString();
            ddlBanco.SelectedValue = entity.IDBanco.ToString();
            if (entity.IDCategoria.HasValue)
                ddlCategoria.SelectedValue = entity.IDCategoria.Value.ToString();
        }
    }

    private void cargarCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var categorias = dbContext.Categorias.Where(x => x.IDUsuario == CurrentUser.IDUsuario).OrderBy(x => x.Nombre).ToList();
            ddlCategoria.DataTextField = "Nombre";
            ddlCategoria.DataValueField = "IDCategoria";
            ddlCategoria.DataSource = categorias;
            ddlCategoria.DataBind();

            ddlCategoria.Items.Insert(0, new ListItem("", ""));
        }
    }

    [ScriptMethod(UseHttpGet = false)]
    [WebMethod(true)]
    public static string getCategories(string claseJS)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ComprasCommon.ObtenerCategorias(usu.IDUsuario, claseJS);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarCategoria(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ComprasCommon.EliminarCategoria(id, usu.IDUsuario);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static void guardar(BancosDetalleViewModel detalle)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                //if (ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime( detalle.Fecha)))
                //    throw new CustomException("No puede agregar ni modificar una comprobante que se encuentre fuera de la fecha limite de carga.");

                int id = BancosCommon.GuardarDetalle(detalle, usu.IDUsuario);
                if (usu.UsaPlanCorporativo)
                    ContabilidadCommon.AgregarAsientoDeBanco(usu, id);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(true)]
    public static void guardarCategoria(int id, string nombre)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ComprasCommon.GuardarCategoria(id, nombre, usu.IDUsuario);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void GenerarAsientosContables(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ContabilidadCommon.AgregarAsientoDeBanco(usu, id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {


                    var entity = dbContext.BancosDetalle.Where(x => x.IDBancoDetalle == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        //if (ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(entity.Fecha)))
                        //    throw new CustomException("No puede eliminar un comprobante que se encuentre fuera de la fecha limite de carga.");
                        dbContext.Database.ExecuteSqlCommand("delete Asientos where IDBancoDetalle = " + id);

                        dbContext.BancosDetalle.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="caja.aspx.cs" Inherits="modulos_Tesoreria_caja" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .modal.modal-wide .modal-dialog {
            width: 90%;
            max-width: 900px;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
    <link href="/css/jasny-bootstrap.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-university'></i>Caja<span>Administración</span></h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li class="active">Caja</li>
            </ol>
        </div>
    </div>
    <div id="divConDatos" runat="server">
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12 col-md-12 table-results">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <form id="frmSearch">
                                <input type="hidden" id="hdnTipo" runat="server" />
                                <input type="hidden" id="hdnPage" runat="server" value="1" />
                                <input type="hidden" id="hdnSaldoTotalActual" value="1" />
                                
                                <div class="col-sm-12" style="padding-left: inherit">
                                    <div class="col-sm-8 col-md-7">
                                        <div class="row">
                                            <div class="col-sm-3 col-md-3 form-group">
                                                <select class="select2" data-placeholder="Seleccione Caja" id="ddlCaja">
                                                    <option value=""></option>
                                                </select>
                                                <div style="margin-top: 3px;">
                                                    <span class="badge badge-success pull-right tooltips" data-toggle="tooltip" data-original-title="Haga click aquí para agregar Cajas" style="cursor: pointer" onclick="caja.administrarCajas();">administrar</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-md-3 form-group">
                                                <select class="select2" data-placeholder="Seleccione Movimiento" id="ddlTipoMovimiento" onchange="caja.filtrar();">
                                                    <option value="">Todos</option>
                                                    <option value="Ingreso">Ingreso</option>
                                                    <option value="Egreso">Egreso</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-3 col-md-3 form-group">
                                                <select class="form-control" id="ddlPeriodo" onchange="caja.otroPeriodo();">
                                                    <option value="30" selected="selected">Últimos 30 dias</option>
                                                    <option value="15">Últimos 15 dias</option>
                                                    <option value="7">Últimos 7 dias</option>
                                                    <option value="1">Ayer</option>
                                                    <option value="0">Hoy</option>
                                                    <option value="-1">Otro período</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3 col-md-3 form-group">
                                                <select class="form-control" id="ddlMostrarCot" onchange="caja.filtrar();">
                                                    <option value="0">Mostrar todo</option>
                                                    <%--<option value="1">Sólo Cotizaciones</option>--%>
                                                    <option value="1" selected="selected">Excluir asientos manuales</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div id="divMasFiltros" style="display: none">
                                            <div class="row">
                                                <div class="col-sm-4 col-md-4 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        <input runat="server" id="txtFechaDesde" class="form-control validDate greaterThan" placeholder="Fecha desde" maxlength="10" onchange="caja.filtrar();" />
                                                    </div>
                                                </div>

                                                <div class="col-sm-4 col-md-4 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        <input runat="server" id="txtFechaHasta" class="form-control validDate greaterThan" placeholder="Fecha hasta" maxlength="10" onchange="caja.filtrar();" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-3 responsive-buttons md">
                                        <a class="btn btn-warning" onclick="caja.nuevo();">
                                            <i class="fa fa-plus"></i>&nbsp;Nuevo movimiento.
                                        </a>
                                        <a class="btn btn-success" onclick="caja.resetearPagina();caja.cerrarCajas();">Arqueo</a>
                                        
                                    </div>
                                    <div class="col-sm-2 col-md-2">
                                        <h4 class="panel-title" style="clear: left; padding-left: 20px" id="spTotalSinConsolidar"></h4>
                                        <h5 style="clear: left; padding-left: 20px" id="spTotalGlobal"></h5>
                                    </div>
                                </div>
                            </form>

                            <div class="col-sm-12"><hr class="mt0" /></div>
                            <div class="row">
                                <div class="pull-right">
                                    <div class="btn-group mr10" id="divExportar" runat="server">
                                        <div class="btn btn-white tooltips">
                                            <a id="divIconoDescargar" href="javascript:caja.exportar();">
                                                <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                            </a>
                                            <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                            <a href="" id="lnkDownload" onclick="caja.resetearExportacion();" download="ClientesProv" style="display: none">Descargar</a>
                                        </div>
                                    </div>

                                    <div class="btn-group mr10" id="divPagination" style="display: none">
                                        <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="caja.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                        <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="caja.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                    </div>
                                </div>

                                <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                                <p id="msjResultados" style="padding-left: 20px"></p>

                            </div>
                        </div>
                        <!-- panel-heading -->
                        <div class="panel-body">

                            <div class="alert alert-danger" id="divError" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Lo sentimos!</strong> <span id="msgError"></span>
                            </div>

                            <div class="table-responsive">
                                <table class="table mb30">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Concepto</th>
                                            <th>Medio de Pago</th>
                                            <th>Estado</th>
                                            <th>Observaciones</th>
                                            <th style="color: #2b9b8f">Debe</th>
                                            <th style="color: #bf315f">Haber</th>
                                            <th>Saldo</th>
                                            <th class="columnIcons"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="resultsContainer">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script id="resultTemplate" type="text/x-jQuery-tmpl">
            {{each results}}
        <tr>
            <td class="bgTotal" colspan="9">${concepto}</td>
        </tr>
            {{each Conceptos}}
                    <tr>
                        <td>${Fecha}</td>
                        <td>${Concepto}</td>
                        <td>${MedioDePago}</td>
                        <td>${Estado}</td>
                        <td style="max-width: 400px">${Observaciones}</td>
                        <td style="color: #2b9b8f">${Ingreso}</td>
                        <td style="color: #bf315f">${Egreso}</td>
                        <td>${Saldo}</td>
                        <td class="table-action">
                            {{if Estado == "Cargado" && PuedeEditar == "T"}}
                                <a onclick="caja.editar(${ID});" style="cursor: pointer; font-size: 16px" title="Editar"><i class="fa fa-pencil"></i></a>
                                <a onclick="caja.eliminar(${ID},'${tipoMovimiento}');" style="cursor: pointer; font-size: 16px" class="delete-row" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                            {{/if}}
                        </td>
                    </tr>
            {{/each}}
            {{/each}}

        </script>
        <script id="noResultTemplate" type="text/x-jQuery-tmpl">
            <tr>
                <td colspan="9">No se han encontrado resultados</td>
            </tr>
        </script>
    </div>
    
    <!-- Modal -->
    <div class="modal modal fade" id="modalCajas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nueva Caja</h4>
                </div>
                <div class="modal-body" style="min-height: 100px;">
                    <div>
                        <div class="alert alert-danger" id="divErrorCaja" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgErrorCaja"></span>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" maxlength="50" id="txtNuevaCaja" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-2"><a class="btn btn-default" id="btnCaja" onclick="caja.grabarCaja();">Agregar</a></div>

                        <br />
                        <br />
                        <br />

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyDetalle">
                                </tbody>
                            </table>
                        </div>
                   
                        <input type="hidden" id="hdnIDCaja" value="0" />
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal" onclick="caja.cargarCajas();">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <div id="divSinDatos" runat="server">
        <div class="panel-heading" style="background-color: white; height: 30%; width: 100%; border-radius: 4px; text-align: center;">
            <h4 id="hTitulo">Aún no has creado ninguna caja</h4>
            <br />
            <a class="btn btn-warning" onclick="caja.nuevo();" id="btnNuevoSinDatos">&nbsp Crea un movimiento</a>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="/js/jasny-bootstrap.min.js"></script>
    <script src="/js/views/tesoreria/caja.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            //if ($('#divConDatos').is(":visible")) {
            //    caja.filtrar();
            //}
            caja.configFilters();
            //caja.configForm();
        });
    </script>
</asp:Content>

﻿using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using System.Collections.Specialized;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;
using ACHE.Negocio.Contabilidad;
using System.Configuration;
using ACHE.FacturaElectronica;

public partial class pagose : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PagosCart.Retrieve().Items.Clear();
            PagosCart.Retrieve().FormasDePago.Clear();
            PagosCart.Retrieve().Retenciones.Clear();

            txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnFileYear.Value = DateTime.Now.Year.ToString();

            litPath.Text = "Alta";
            cargarDatosDesdeCompra();
            cargarNroOrden(CurrentUser.IDUsuario);
            litUsuarioAlta.Text = CurrentUser.Email;

            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0")
                {
                    int id = int.Parse(hdnID.Value);
                    lnkPrint2.Attributes.Add("onclick", "Common.imprimirArchivoDesdeIframe('');");

                    litPath.Text = "Edición";

                    using (var dbContext = new ACHEEntities())
                    {
                        var entity = dbContext.Pagos.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPago == id).FirstOrDefault();
                        if (entity != null)
                        {
                            litUsuarioAlta.Text = entity.UsuarioAlta;
                            litUsuarioModifica.Text = entity.UsuarioModifica;
                            hdnFileYear.Value = entity.FechaPago.Year.ToString();
                        }
                    }
                }
            }
            else
                txtObservaciones.Text = CurrentUser.ObservacionesOrdenes;
        }
    }
    
    private void cargarNroOrden(int idUsuario)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Pagos.Where(x => x.IDUsuario == idUsuario).ToList();
            if (entity != null && entity.Count > 0)
                txtNro.Text = (entity.Max(x => x.NroPago) + 1).ToString();
            else
                txtNro.Text = "1";
        }
    }

    private void cargarDatosDesdeCompra()
    {
        var PagoParcial = Request.QueryString["Pago"];
        string resultado = "";
        if (!string.IsNullOrWhiteSpace(Request.QueryString["IDCompra"]) && !string.IsNullOrWhiteSpace(PagoParcial))
        {
            var idcompra = Convert.ToInt32(Request.QueryString["IDCompra"]);
            using (var dbContext = new ACHEEntities())
            {
                var compras = dbContext.Compras.Where(x => x.IDCompra == idcompra).FirstOrDefault();
                hdnIDPersona.Value = compras.IDPersona.ToString();

                if (PagoParcial == "100")
                    resultado = compras.Saldo.ToString();
                else if (PagoParcial == "50")
                    resultado = ((compras.Saldo * 50) / 100).ToString();
                else if (PagoParcial == "30")
                    resultado = ((compras.Saldo * 30) / 100).ToString();

                var Concepto = compras.Tipo + " " + compras.NroFactura + " (Saldo:$ " + compras.Saldo.ToString() + ")";
                agregarItem(0, compras.IDCompra.ToString(), Concepto, resultado,compras.Saldo.ToString());

                hdnCargarDatosDesdeCompra.Value = "1";
            }
        }
    }

    #region Items

    [WebMethod(true)]
    public static void agregarItem(int id, string idComprobante, string comprobante, string importe, string saldo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var idcomprobante = int.Parse(idComprobante);
            var importePagar = decimal.Parse(importe.Replace(".", ","));

            if (idcomprobante > 0)
            {
                var importeComprobante = decimal.Parse(saldo.Replace(".", ""));
                if (importePagar > importeComprobante)
                    throw new Exception("El importe debe ser menor o igual al saldo");
            }

            if (id != 0)
            {
                var aux = PagosCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
                PagosCart.Retrieve().Items.Remove(aux);
            }

            var tra = new PagosDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
            tra.ID = PagosCart.Retrieve().Items.Count() + 1;
            tra.nroFactura = comprobante;
            tra.Importe = importePagar;
         

            if (idcomprobante > 0)
            {
                tra.IDCompra = int.Parse(idComprobante);
                using (var dbContext = new ACHEEntities())
                {
                    var compra = dbContext.Compras.Where(x => x.IDCompra == idcomprobante).FirstOrDefault();
                    tra.TotalFactura = (compra.Tipo == "NCA" || compra.Tipo == "NCB" || compra.Tipo == "NCC" || compra.Tipo == "NCM" || compra.Tipo == "NCE") ? ("-" + Convert.ToDecimal(compra.TotalImpuestos + compra.Total + compra.Iva).ToString()) : Convert.ToDecimal(compra.TotalImpuestos + compra.Total + compra.Iva).ToString();
                    tra.FechaEmision = compra.FechaEmision;
                    tra.FechaVencimiento = compra.FechaPrimerVencimiento;
                }

            }
            else
            {
                tra.IDCompra = null;
            }

           
            PagosCart.Retrieve().Items.Add(tra);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarItem(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var aux = PagosCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            if (aux != null)
                PagosCart.Retrieve().Items.Remove(aux);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    public static TotalesViewModel obtenerTotales()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            TotalesViewModel totales = new TotalesViewModel();
            totales.Total = PagosCart.Retrieve().GetTotal().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

            return totales;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    public static string obtenerItems()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var list = PagosCart.Retrieve().Items.OrderBy(x => x.nroFactura).ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:left'>" + detalle.nroFactura + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:right'>" + ((detalle.IDCompra != null) ? detalle.TotalFactura.ToString() : "") + "</td>";
                        html += "<td style='text-align:right'>" + ((detalle.IDCompra != null) ? detalle.FechaEmision.ToString("dd/MM/yyyy") : "") + "</td>";
                        html += "<td style='text-align:right'>" + ((detalle.IDCompra!=null)?detalle.FechaVencimiento.ToString("dd/MM/yyyy"):"") + "</td>";

                        html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem(" + detalle.ID + ", '" + (detalle.IDCompra ?? -1) + "' ,'";
                        html += detalle.Importe.ToString("").Replace(",", ".") + "')\"";
                        html += "><i class='fa fa-edit'></i></a></td>";
                        html += "</tr>";

                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='4' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }

    #endregion

    #region Formas de Pago

    [WebMethod(true)]
    public static void agregarForma(int id, string forma, string nroRef, string importe, string idcheque, string idBanco, string idNotaCredito, string idCaja, string fecha)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (!string.IsNullOrWhiteSpace(idcheque))
            {

                if (PagosCart.Retrieve().ValidarCheque(int.Parse(idcheque), DateTime.Parse(fecha)))
                    throw new Exception("El cheque se encuentra vencido.");
            }
            if (id != 0)
            {
                var aux = PagosCart.Retrieve().FormasDePago.Where(x => x.ID == id).FirstOrDefault();
                PagosCart.Retrieve().FormasDePago.Remove(aux);
            }

            var tra = new PagosFormasDePagoViewModel();
            tra.ID = PagosCart.Retrieve().FormasDePago.Count() + 1;
            tra.FormaDePago = forma;
            tra.NroReferencia = nroRef;
            tra.Importe = decimal.Parse(importe.Replace(".", ","));

            if (!string.IsNullOrWhiteSpace(idcheque))
                tra.IDCheque = int.Parse(idcheque);

            if (!string.IsNullOrWhiteSpace(idBanco))
                tra.IDBanco = int.Parse(idBanco);

            if (!string.IsNullOrWhiteSpace(idCaja))
                tra.IDCaja = int.Parse(idCaja);

            if (!string.IsNullOrWhiteSpace(idNotaCredito))
                tra.IDNotaCredito = int.Parse(idNotaCredito);

            PagosCart.Retrieve().FormasDePago.Add(tra);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarForma(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var aux = PagosCart.Retrieve().FormasDePago.Where(x => x.ID == id).FirstOrDefault();
            if (aux != null)
                PagosCart.Retrieve().FormasDePago.Remove(aux);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    public static string obtenerFormas()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var list = PagosCart.Retrieve().FormasDePago.OrderBy(x => x.FormaDePago).ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:left'>" + detalle.FormaDePago + "</td>";
                        html += "<td style='text-align:left'>" + detalle.NroReferencia + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarForma(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarForma(" + detalle.ID + ", '" + detalle.FormaDePago + "', '" + detalle.NroReferencia + "','" + detalle.Importe.ToString("").Replace(",", ".") + "','" + detalle.IDBanco + "','" + detalle.IDCheque + "','" + detalle.IDCaja + "');\"><i class='fa fa-edit'></i></a></td>";
                        html += "</tr>";

                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='5' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }

    [WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    public static string obtenerFormasTotal()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var importeFormas = PagosCart.Retrieve().FormasDePago.ToList().Sum(x => x.Importe);
            var importeComprobantes = PagosCart.Retrieve().Items.ToList().Sum(x => x.Importe);

            return Math.Round((importeComprobantes - importeFormas), ConfiguracionHelper.ObtenerCantidadDecimales()).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    #endregion

    #region Retenciones

    [WebMethod(true)]
    public static void agregarRetencion(int id, string tipo, string nroRef, string importe, string idJuridiccion, string codigoRegimen)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            if (id != 0)
            {
                var aux = PagosCart.Retrieve().Retenciones.Where(x => x.ID == id).FirstOrDefault();
                PagosCart.Retrieve().Retenciones.Remove(aux);
            }

            var tra = new PagosRetencionesViewModel();
            tra.ID = PagosCart.Retrieve().Retenciones.Count() + 1;
            tra.Tipo = tipo;
            tra.NroReferencia = nroRef;
            tra.Importe = decimal.Parse(importe.Replace(".", ","));
            if (codigoRegimen != "")
            {
                tra.CodigoRegimen = int.Parse(codigoRegimen);
            }
            else
                tra.CodigoRegimen = null;
            if (idJuridiccion != "")
                tra.IDJurisdiccion = int.Parse(idJuridiccion);
            else
                tra.IDJurisdiccion = 0;
            PagosCart.Retrieve().Retenciones.Add(tra);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarRetencion(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var aux = PagosCart.Retrieve().Retenciones.Where(x => x.ID == id).FirstOrDefault();
            if (aux != null)
                PagosCart.Retrieve().Retenciones.Remove(aux);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    public static string obtenerRetenciones()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var list = PagosCart.Retrieve().Retenciones.OrderBy(x => x.Tipo).ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Tipo + "</td>";
                        html += "<td style='text-align:left'>" + detalle.NroReferencia + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.CodigoRegimen + "</td>";

                        html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:CobRetenciones.eliminarRet(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:CobRetenciones.modificarRet(" + detalle.ID + ", '" + detalle.Tipo + "', '" + detalle.NroReferencia + "','" + detalle.Importe.ToString("").Replace(",", ".") + "','" + ((detalle.IDJurisdiccion == 0) ? "" : detalle.IDJurisdiccion.ToString()) + "','"+detalle.CodigoRegimen+ "');\"><i class='fa fa-edit'></i></a></td>";
                        html += "</tr>";

                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='5' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }

    #endregion

    [WebMethod(true)]
    public static int guardar(int id, int idPersona, string obs, string fechaPago, bool sobranteACuenta, int nroPago)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                if (ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, Convert.ToDateTime(fechaPago)))
                    throw new Exception("No puede agregar ni modificar una cobranza que se encuentre en un periodo cerrado.");
                if (ContabilidadCommon.ValidarNroPago(usu.IDUsuario,id,nroPago))
                    throw new Exception("Ya existe un pago con el nro: " + nroPago);

                //if (ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(fechaPago)))
                //    throw new CustomException("No puede agregar ni modificar una comprobante que se encuentre fuera de la fecha limite de carga.");

                PagosCartDto pagosCartdto = new PagosCartDto();
                pagosCartdto.IDPago = id;
                pagosCartdto.IDPersona = idPersona;
                pagosCartdto.Observaciones = obs;
                pagosCartdto.FechaPago = fechaPago;
                pagosCartdto.NroPago = nroPago;
                pagosCartdto.Items = PagosCart.Retrieve().Items.ToList();
                pagosCartdto.FormasDePago = PagosCart.Retrieve().FormasDePago.ToList();
                pagosCartdto.Retenciones = PagosCart.Retrieve().Retenciones.ToList();

                var pagos = PagosCommon.Guardar(pagosCartdto, usu, sobranteACuenta);
                if (PermisosModulos.tienePlan("alertas.aspx"))
                    generarAlertas(pagos);

                ContabilidadCommon.AgregarAsientoDePago(usu, pagos.IDPago);
                return pagos.IDPago;
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(true)]
    public static PagosEditViewModel obtenerDatos(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                Pagos entity = dbContext.Pagos
                    .Include("PagosFormasDePago").Include("PagosRetenciones")
                    .Where(x => x.IDPago == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (entity != null)
                {
                    PagosEditViewModel result = new PagosEditViewModel();
                    result.ID = id;
                    result.IDPersona = entity.IDPersona;
                    result.Observaciones = entity.Observaciones;
                    result.Fecha = entity.FechaPago.ToShortDateString();
                    result.NroPago = entity.NroPago;
                    var personas = dbContext.Personas.Where(x => x.IDPersona == entity.IDPersona).FirstOrDefault();
                    PersonasEditViewModel PersonasEdit = new PersonasEditViewModel();
                    PersonasEdit.ID = id;
                    PersonasEdit.RazonSocial = personas.RazonSocial.ToUpper();
                    PersonasEdit.Email = string.IsNullOrEmpty(personas.EmailsEnvioFc) ? personas.Email : personas.EmailsEnvioFc; //personas.Email.ToLower();
                    PersonasEdit.CondicionIva = personas.CondicionIva;
                    PersonasEdit.Domicilio = personas.Domicilio.ToUpper() + " " + personas.PisoDepto;
                    PersonasEdit.Ciudad = personas.Ciudades.Nombre.ToUpper();
                    PersonasEdit.Provincia = personas.Provincias.Nombre;
                    PersonasEdit.TipoDoc = personas.TipoDocumento;
                    PersonasEdit.NroDoc = personas.NroDocumento;
                    result.Personas = PersonasEdit;
                    PersonasEdit.esAgenteRetencion = entity.Usuarios.EsAgenteRetencion;
                    foreach (var det in entity.PagosDetalle)
                    {
                        var tra = new PagosDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                        tra.ID = PagosCart.Retrieve().Items.Count() + 1;
                        if (det.Compras != null)
                        {
                            tra.nroFactura = det.Compras.Tipo + " " + det.Compras.NroFactura;
                            tra.TotalFactura = (det.Compras.Tipo == "NCA" || det.Compras.Tipo == "NCB" || det.Compras.Tipo == "NCC" || det.Compras.Tipo == "NCM" || det.Compras.Tipo == "NCE") ? ("-" + Convert.ToDecimal(det.Compras.TotalImpuestos + det.Compras.Total + det.Compras.Iva).ToString()) : Convert.ToDecimal(det.Compras.TotalImpuestos + det.Compras.Total + det.Compras.Iva).ToString();


                            tra.FechaEmision = det.Compras.FechaEmision;
                            tra.FechaVencimiento = det.Compras.FechaPrimerVencimiento;
                        }
                        else
                            tra.nroFactura = "Pago a cuenta";
                        tra.IDCompra = det.IDCompra;
                        tra.Importe = det.Importe;
                        tra.IDCompra = det.IDCompra;
                  
                        PagosCart.Retrieve().Items.Add(tra);
                    }

                    foreach (var det in entity.PagosFormasDePago)
                    {
                        var tra = new PagosFormasDePagoViewModel();
                        tra.ID = PagosCart.Retrieve().FormasDePago.Count() + 1;
                        tra.FormaDePago = det.FormaDePago;
                        tra.NroReferencia = det.NroReferencia;
                        tra.Importe = det.Importe;
                        tra.IDCheque = det.IDCheque;
                        tra.IDBanco = det.IDBanco;
                        tra.IDCaja = det.IDCaja;
                        tra.IDNotaCredito = det.IDNotaCredito;

                        PagosCart.Retrieve().FormasDePago.Add(tra);
                    }

                    foreach (var det in entity.PagosRetenciones)
                    {
                        var tra = new PagosRetencionesViewModel();
                        tra.ID = PagosCart.Retrieve().Retenciones.Count() + 1;
                        tra.Tipo = det.Tipo;
                        tra.NroReferencia = det.NroReferencia;
                        tra.Importe = det.Importe;
                        tra.IDJurisdiccion = det.IDJurisdiccion ?? 0;
                        tra.CodigoRegimen = det.CodigoRegimen;
                        PagosCart.Retrieve().Retenciones.Add(tra);
                    }

                    return result;
                }
                else
                    throw new Exception("Error al obtener los datos");
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerComprasPendientes(int id, int idPago)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var list = new List<Combo2ViewModel>();
            using (var dbContext = new ACHEEntities())
            {
                if (idPago == 0)
                {
                    list = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == id && x.Saldo > 0 && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE").ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDCompra,
                        Nombre = x.Tipo + " " + x.NroFactura + " (Saldo: $ " + (Convert.ToDecimal(x.Saldo)).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + ")"
                    }).OrderBy(x => x.Nombre).ToList();
                }
                else
                {
                    list = dbContext.PagosDetalle.Where(x => x.Compras.IDUsuario == usu.IDUsuario && x.Compras.IDPersona == id && x.IDPago == idPago && x.Compras.Tipo != "NCA" && x.Compras.Tipo != "NCB" && x.Compras.Tipo != "NCC" && x.Compras.Tipo != "NCM" && x.Compras.Tipo != "NCE").ToList()
                   .Select(x => new Combo2ViewModel()
                   {
                       ID = x.Compras.IDCompra,
                       Nombre = x.Compras.Tipo + " " + x.Compras.NroFactura + " (Saldo: $ " + (Convert.ToDecimal(x.Compras.Saldo)).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + ")"
                   }).ToList();
                }
                list.Add(new Combo2ViewModel { ID = -1, Nombre = "Pago a cuenta" });
            }
            return list;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerFormasDeCobroPagos(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersona && x.Saldo > 0 && x.Tipo != "COT" && x.Tipo != "FCA" && x.Tipo != "FCB" && x.Tipo != "FCC" && x.Tipo != "FCM" && x.Tipo != "NDA" && x.Tipo != "NDB" && x.Tipo != "NDC" && x.Tipo != "NDM").ToList()
                         .Select(x => new Combo2ViewModel()
                         {
                             ID = x.IDCompra,
                             Nombre = x.Tipo + " " + "-" + x.NroFactura + " (Saldo: $ " + x.Saldo.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + ")"
                         }).OrderBy(x => x.Nombre).OrderByDescending(x => x.ID).ToList();

                return aux;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    //TODO: PASAR A NEGOCIO
    private static void generarAlertas(Pagos pagos)
    {
        using (var dbContext = new ACHEEntities())
        {
            var listaAlertas = dbContext.Alertas.Where(x => x.IDUsuario == pagos.IDUsuario).ToList();

            foreach (var alertas in listaAlertas)
            {
                if (alertas.AvisoAlerta == "El pago a un proveedor es")
                {
                    switch (alertas.Condicion)
                    {
                        case "Mayor o igual que":
                            if (pagos.ImporteTotal >= alertas.Importe)
                            {
                                insertarAlerta(dbContext, pagos, alertas);
                            }
                            break;
                        case "Menor o igual que":
                            if (pagos.ImporteTotal <= alertas.Importe)
                            {
                                insertarAlerta(dbContext, pagos, alertas);
                            }
                            break;
                    }
                }
            }
        }
    }
    
    [WebMethod(true)]
    public static string generarPago(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                var entity = dbContext.Pagos.Where(x => x.IDPago == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                var fileNamePago = entity.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_0001-" + entity.NroPago.ToString().PadLeft(8, '0') +".pdf";
                var fecha = entity.FechaPago.ToString("dd/MM/yyyy");
                var nroPago = entity.NroPago.ToString();
                Common.GenerarPago(usu, id, entity.IDPersona, "OC", fecha, ref nroPago, entity.Observaciones, Common.ComprobanteModo.Generar);

                return fileNamePago;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    //TODO: PASAR A NEGOCIO
    private static void insertarAlerta(ACHEEntities dbContext, Pagos pagos, Alertas alertas)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        AlertasGeneradas entity = new AlertasGeneradas();

        entity.IDAlerta = alertas.IDAlerta;
        entity.IDUsuario = pagos.IDUsuario;
        entity.IDPersona = pagos.IDPersona;

        entity.ImportePagado = pagos.ImporteTotal;
        entity.Visible = true;
        entity.Fecha = DateTime.Now;
        entity.IDPagos = pagos.IDPago;
        entity.NroComprobante = "Se Pagaron los Comprobante/s: ";


        foreach (var item in pagos.PagosDetalle)
        {
            if (item.IDCompra.HasValue)
            {
                var compra = dbContext.Compras.Where(x => x.IDCompra == item.IDCompra).FirstOrDefault();
                entity.NroComprobante += compra.Tipo + " " + compra.NroFactura + " ; ";
            }
            else
            {
                entity.NroComprobante += "Pago a cuenta" + " ; ";
            }
        }

        entity.NroComprobante = entity.NroComprobante.Substring(0, entity.NroComprobante.Length - 3);
        entity.NroComprobante += ".";

        dbContext.AlertasGeneradas.Add(entity);
        dbContext.SaveChanges();

        var alerta = alertas.AvisoAlerta + " - " + alertas.Condicion + " - $" + alertas.Importe;
        var descripcion = entity.NroComprobante;
        enviarEmailAlerta(alerta, descripcion);
    }

    //TODO: PASAR A NEGOCIO
    public static void enviarEmailAlerta(string alerta, string descripcion)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            var email = (usu.EmailAlerta == "") ? usu.Email : usu.EmailAlerta;

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<ALERTA>", alerta);
            replacements.Add("<USUARIO>", usu.RazonSocial);
            replacements.Add("<DESCRIPCION>", descripcion);
            replacements.Add("<EMAIL>", email);


            bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Alertas, replacements, ConfigurationManager.AppSettings["Email.FromAlertas"], email, "Alerta en CONTABILIUM");

            if (!send)
                throw new Exception("Comprobante El mensaje no pudo ser enviado. Por favor, escribenos a <a href='mailto:ayuda@contabilium.com'>ayuda@contabilium.com</a>");

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    
    [WebMethod(true)]
    public static string previsualizar(int id, int idPersona, string fecha, string nroComprobante, string obs)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            if (PagosCart.Retrieve().Items.Count == 0)
                throw new Exception("Ingrese un comprobante");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var razonSocial = string.Empty;

            using (var dbContext = new ACHEEntities())
            {
                razonSocial = dbContext.Personas.Where(x => x.IDPersona == idPersona).FirstOrDefault().RazonSocial;
            }

            var fileNamePresup = usu.IDUsuario + "_prev.pdf";

            Common.GenerarPago(usu, id, idPersona, "OC", fecha, ref nroComprobante,obs, Common.ComprobanteModo.Previsualizar);

            return fileNamePresup;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
﻿using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using ACHE.Model;
using System.IO;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Contabilidad;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Common;

public partial class compraDetalladase : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ucDetalle.CurrentUser = CurrentUser;
            ucDetalle.MostrarPercepciones = false;
            DetalleCart.Retrieve().Items.Clear();

            using (var dbContext = new ACHEEntities())
            {
                var plan = PermisosModulos.ObtenerPlanActual(dbContext, CurrentUser.IDUsuario);
                if (PermisosModulosCommon.VerificarCantComprobantes(plan, CurrentUser.IDUsuario))
                    Response.Redirect("~/modulos/seguridad/elegir-plan.aspx?upgrade=2");
            }

            txtFecha.Text = txtFechaEmision.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFechaPrimerVencimiento.Text = DateTime.Now.ToString("dd/MM/yyyy");
            cargarCategorias();

            litTotal.Text = "0";
            liTotalImpuestos.Text = "0";
            liTotalImportes.Text = "0";
            litPath.Text = "Alta";
            hdnIdusuario.Value = CurrentUser.IDUsuario.ToString();
            hdnTieneSaldoAPagar.Value = "1";
            hdnIDPersona.Value = Request.QueryString["IDPersona"];
            litUsuarioAlta.Text = CurrentUser.Email;
            var duplicado = "0";
            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0")
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["Duplicar"]))
                    {
                        hdnDuplicar.Value = Request.QueryString["ID"];
                        duplicado = Request.QueryString["Duplicar"];
                    }
                    cargarEntidad(int.Parse(hdnID.Value), int.Parse(duplicado));
                    if (duplicado == "1")
                    {
                        litPath.Text = "Alta";
                        hdnID.Value = "0";
                    }
                    else
                        litPath.Text = "Edición";
                }
            }
        }
    }

    private void cargarCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var categorias = dbContext.Categorias.Where(x => x.IDUsuario == CurrentUser.IDUsuario).ToList();
            ddlCategoria.DataTextField = "Nombre";
            ddlCategoria.DataValueField = "IDCategoria";
            ddlCategoria.DataSource = categorias;
            ddlCategoria.DataBind();

            ddlCategoria.Items.Insert(0, new ListItem("", ""));
        }
    }

    private void cargarEntidad(int id, int duplicado)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Compras.Include("Personas").Include("ComprasDetalle").Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDCompra == id).FirstOrDefault();
            if (entity != null)
            {
                hdnIDPersona.Value = entity.IDPersona.ToString();
                hdnTipoComprobante.Value = entity.Tipo;
                hdnIDInventario.Value = entity.IDInventario.HasValue ? entity.IDInventario.Value.ToString() : "0";
                hdnIDNroOrden.Value = entity.IDOrdenCompra.HasValue ? entity.IDOrdenCompra.Value.ToString() : "0";
                txtFecha.Text = entity.Fecha.ToString("dd/MM/yyyy");
                txtFechaEmision.Text = entity.FechaEmision.ToString("dd/MM/yyyy");

                txtFechaPrimerVencimiento.Text = entity.FechaPrimerVencimiento.ToString("dd/MM/yyyy");
                if (entity.FechaSegundoVencimiento != null)
                    txtFechaSegundoVencimiento.Text = Convert.ToDateTime(entity.FechaSegundoVencimiento).ToString("dd/MM/yyyy");
                if (duplicado == 0)
                    txtNroFactura.Text = entity.NroFactura;
                txtIva.Text = entity.Iva.ToString("").Replace(",", ".");
                //txtExento.Text = entity.Exento.ToString("").Replace(",", ".");
                // IMPUESTOS
                txtImpNacionales.Text = entity.ImpNacional.ToString("").Replace(",", ".");
                txtImpMunicipales.Text = entity.ImpMunicipal.ToString("").Replace(",", ".");
                txtImpInternos.Text = entity.ImpInterno.ToString("").Replace(",", ".");
                //txtIIBB.Text = entity.IIBB.ToString("").Replace(",", ".");
                var jurisdicciones = dbContext.Jurisdicciones.Where(x => x.IDCompra == entity.IDCompra).ToList();
                if (jurisdicciones != null)
                    txtIIBB.Text = jurisdicciones.Sum(x => x.Importe).ToString("").Replace(",", ".");

                foreach (var det in entity.ComprasDetalle)
                {
                    var tra = new DetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                    tra.ID = DetalleCart.Retrieve().Items.Count() + 1;
                    tra.Concepto = det.Concepto;
                    tra.Codigo = (det.Conceptos != null) ? det.Conceptos.Codigo : "";
                    tra.CodigoPlanCta = (det.PlanDeCuentas == null) ? "" : det.PlanDeCuentas.Codigo + " - " + det.PlanDeCuentas.Nombre;
                    tra.Iva = det.Iva;
                    tra.PrecioUnitario = det.PrecioUnitario;
                    tra.Bonificacion = det.Bonificacion;
                    tra.Cantidad = det.Cantidad;
                    tra.IDConcepto = det.IDConcepto;
                    tra.IDPlanDeCuenta = det.IDPlanDeCuenta;

                    DetalleCart.Retrieve().Items.Add(tra);
                }

                DetalleCart.Retrieve().ImporteNoGrabado = entity.NoGravado;

                txtIva.Text = entity.Iva.ToString("").Replace(",", ".");
                txtPercepcionIVA.Text = entity.PercepcionIVA.ToString("").Replace(",", ".");
                txtOtros.Text = entity.Otros.ToString("").Replace(",", ".");

                txtObservaciones.Text = entity.Observaciones;
                litUsuarioAlta.Text = entity.UsuarioAlta;
                litUsuarioModifica.Text = entity.UsuarioModifica;

                litTotal.Text = Convert.ToDecimal(entity.Iva + entity.Total.Value + entity.TotalImpuestos + entity.Redondeo).ToMoneyFormat(2);
                liTotalImpuestos.Text = Convert.ToDecimal(entity.TotalImpuestos).ToMoneyFormat(2);
                liTotalImportes.Text = Convert.ToDecimal(entity.Total).ToMoneyFormat(2);
                if (entity.IDCategoria.HasValue)
                    ddlCategoria.SelectedValue = entity.IDCategoria.Value.ToString();
                if (entity.Rubro != null)
                    ddlRubro.SelectedValue = entity.Rubro;

                hdnTieneSaldoAPagar.Value = entity.Saldo > 0 ? "1" : "0";

                if (!string.IsNullOrWhiteSpace(entity.Foto))
                {
                    hdnFileName.Value = entity.Foto;
                    hdnTieneFoto.Value = "1";

                    lnkComprobante.NavigateUrl = "/pdfGenerator.ashx?file=" + entity.Foto + "&tipoDeArchivo=compras&year=" + entity.Fecha.Year; ;
                }

            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static int guardar(int id, int idPersona, string fecha, string nroFactura, int idInventario,
        string iva, string importe2, string importe5, string importe10, string importe21, string importe27, string noGrav, string importeMon,
        string impNacional, string impMunicipal, string impInterno, string percepcionIva, string otros,
        string obs, string tipo, string idCategoria, string rubro, string exento, string FechaEmision, int idPlanDeCuenta, List<JurisdiccionesViewModel> Jurisdicciones, string fechaPrimerVencimiento, string fechaSegundoVencimiento, int idOrden)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            int idCompra = 0;
            using (var dbContext = new ACHEEntities())
            {
                if (DetalleCart.Retrieve().Items.Count == 0)
                    throw new CustomException("No puede generar una compra detallada sin detalles.");

                if (tipo != "COT" && ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(fecha)))
                    throw new CustomException("No puede agregar ni modificar un comprobante con fecha anterior a la fecha límite de carga.");

                ComprasDto comCart = new ComprasDto();
                comCart.IDCompra = id;
                comCart.IDPersona = idPersona;
                comCart.Fecha = fecha;
                comCart.NroFactura = nroFactura;
                comCart.IdInventario = idInventario;
                comCart.Importe2 = importe2;
                comCart.Importe5 = importe5;
                comCart.Importe10 = importe10;
                comCart.Importe21 = importe21;
                comCart.Importe27 = importe27;
                comCart.ImporteMon = importeMon;
                comCart.Exento = exento;
                comCart.ImpNacional = impNacional;
                comCart.ImpMunicipal = impMunicipal;
                comCart.ImpInterno = impInterno;
                comCart.PercepcionIva = percepcionIva;
                comCart.Otros = otros;
                comCart.Obs = obs;
                comCart.Tipo = tipo;
                comCart.IdCategoria = idCategoria;
                comCart.Rubro = rubro;
                comCart.FechaEmision = FechaEmision;
                comCart.IdPlanDeCuenta = idPlanDeCuenta;
                comCart.Jurisdicciones = Jurisdicciones;
                comCart.FechaPrimerVencimiento = fechaPrimerVencimiento;
                comCart.FechaSegundoVencimiento = fechaSegundoVencimiento;
                comCart.Detallado = true;
                comCart.IDUsuario = usu.IDUsuario;
                comCart.ImporteMon = importeMon;
                comCart.Iva = iva;
                comCart.NoGrav = DetalleCart.Retrieve().GetImporteNoGrabado().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                comCart.Items = DetalleCart.Retrieve().Items;
                comCart.IdOrden = idOrden;
                comCart.EmailUsuario = usu.Email;
                var compras = ComprasCommon.Guardar(comCart);

                idCompra = compras.IDCompra;

            }
            GenerarAsientosContables(idCompra);
            return idCompra;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarFoto(int idCheque)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        ComprasCommon.EliminarFoto(idCheque, usu.IDUsuario);

    }

    [WebMethod(true)]
    public static void GenerarAsientosContables(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ContabilidadCommon.AgregarAsientoDeCompra(id, usu);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    [WebMethod(true)]
    public static void CargarItems(int idOrden)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            ComprasCommon.CargarItems(idOrden, ObtenerCantidadDecimales());
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");


    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static byte ObtenerCantidadDecimales()
    {
        return ConfiguracionHelper.ObtenerCantidadDecimales();
    }

    #region JURISDICCIONES
    [WebMethod(true)]
    public static List<JurisdiccionesViewModel> getJurisdicciones(int idCompra)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            return ComprasCommon.ObtenerJurisdicciones(idCompra);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> ObtenerJurisdiccionUsuario()
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            return ComprasCommon.ObtenerJurisdiccionUsuario(usu.IDJurisdiccion);


        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    
    #endregion
}
﻿using ACHE.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using System.IO;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Contabilidad;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Common;

public partial class comprase : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                var plan = PermisosModulos.ObtenerPlanActual(dbContext, CurrentUser.IDUsuario);
                if (PermisosModulosCommon.VerificarCantComprobantes(plan, CurrentUser.IDUsuario))
                    Response.Redirect("~/modulos/seguridad/elegir-plan.aspx?upgrade=2");

                if (CurrentUser.UsaPlanCorporativo) //Plan Corporativo
                    CargarPlanDeCuentas();
            }

            txtFecha.Text = txtFechaEmision.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFechaPrimerVencimiento.Text = DateTime.Now.ToString("dd/MM/yyyy");
            cargarCategorias();

            litTotal.Text = "0";
            liTotalImpuestos.Text = "0";
            liTotalImportes.Text = "0";
            litPath.Text = "Alta";
            hdnIdusuario.Value = CurrentUser.IDUsuario.ToString();
            hdnTieneSaldoAPagar.Value = "1";
            hdnIDPersona.Value = Request.QueryString["IDPersona"];

            litUsuarioAlta.Text = CurrentUser.Email;
            var duplicado = "0";
            if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                hdnID.Value = Request.QueryString["ID"];


                if (hdnID.Value != "0")
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["Duplicar"]))
                    {
                        hdnDuplicar.Value = Request.QueryString["ID"];
                        duplicado = Request.QueryString["Duplicar"];
                    }
                    cargarEntidad(int.Parse(hdnID.Value), int.Parse(duplicado));
                    if (duplicado == "1")
                    {
                        litPath.Text = "Alta";
                        hdnID.Value = "0";
                    }
                    else
                        litPath.Text = "Edición";
                }
            }
        }
    }

    private void CargarPlanDeCuentas()
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                if (dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == CurrentUser.IDUsuario))
                {
                    var idctas = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault().CtasFiltroCompras.Split(',');
                    if (idctas != null)
                    {
                        List<Int32> cuentas = new List<Int32>();
                        for (int i = 0; i < idctas.Length; i++)
                            cuentas.Add(Convert.ToInt32(idctas[i]));


                        var listaAux = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && cuentas.Contains(x.IDPlanDeCuenta)).OrderBy(x => x.Codigo).ToList();
                        ddlPlanDeCuentas.Items.Add(new ListItem("", ""));
                        foreach (var item in listaAux)
                            ddlPlanDeCuentas.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));
                        
                    }
                }
            }
            hdnUsaPlanCorporativo.Value = "1";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void cargarCategorias()
    {
        using (var dbContext = new ACHEEntities())
        {
            var categorias = dbContext.Categorias.Where(x => x.IDUsuario == CurrentUser.IDUsuario).OrderBy(x => x.Nombre).ToList();
            ddlCategoria.DataTextField = "Nombre";
            ddlCategoria.DataValueField = "IDCategoria";
            ddlCategoria.DataSource = categorias;
            ddlCategoria.DataBind();

            ddlCategoria.Items.Insert(0, new ListItem("", ""));
        }
    }

    private void cargarEntidad(int id, int duplicado)
    {
        using (var dbContext = new ACHEEntities())
        {
            var entity = dbContext.Compras.Include("Personas").Include("ComprasDetalle").Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDCompra == id).FirstOrDefault();
            if (entity != null)
            {
                if (entity.ComprasDetalle.Any())
                    Response.Redirect("compradetalladase.aspx?ID=" + entity.IDCompra);

                hdnIDPersona.Value = entity.IDPersona.ToString();
                hdnTipoComprobante.Value = entity.Tipo;
                hdnIDNroOrden.Value = entity.IDOrdenCompra.HasValue ? entity.IDOrdenCompra.ToString() : "0";
                txtFecha.Text = entity.Fecha.ToString("dd/MM/yyyy");
                txtFechaEmision.Text = entity.FechaEmision.ToString("dd/MM/yyyy");

                txtFechaPrimerVencimiento.Text = entity.FechaPrimerVencimiento.ToString("dd/MM/yyyy");
                if (entity.FechaSegundoVencimiento != null)
                    txtFechaSegundoVencimiento.Text = Convert.ToDateTime(entity.FechaSegundoVencimiento).ToString("dd/MM/yyyy");
                if (duplicado == 0)
                    txtNroFactura.Text = entity.NroFactura;
                txtImporte2.Text = entity.Importe2.ToString("").Replace(",", ".");
                txtImporte5.Text = entity.Importe5.ToString("").Replace(",", ".");
                txtImporte10.Text = entity.Importe10.ToString("").Replace(",", ".");
                txtImporte21.Text = entity.Importe21.ToString("").Replace(",", ".");
                txtImporte27.Text = entity.Importe27.ToString("").Replace(",", ".");
                txtIva.Text = entity.Iva.ToString("").Replace(",", ".");
                txtNoGravado.Text = entity.NoGravado.ToString("").Replace(",", ".");
                txtImporteMon.Text = entity.ImporteMon.ToString("").Replace(",", ".");
                txtExento.Text = entity.Exento.ToString("").Replace(",", ".");
                // IMPUESTOS
                txtImpNacionales.Text = entity.ImpNacional.ToString("").Replace(",", ".");
                txtImpMunicipales.Text = entity.ImpMunicipal.ToString("").Replace(",", ".");
                txtImpInternos.Text = entity.ImpInterno.ToString("").Replace(",", ".");
                //txtIIBB.Text = entity.IIBB.ToString("").Replace(",", ".");
                var jurisdicciones = dbContext.Jurisdicciones.Where(x => x.IDCompra == entity.IDCompra).ToList();
                if (jurisdicciones != null)
                    txtIIBB.Text = jurisdicciones.Sum(x => x.Importe).ToString("").Replace(",", ".");

                txtPercepcionIVA.Text = entity.PercepcionIVA.ToString("").Replace(",", ".");
                txtOtros.Text = entity.Otros.ToString("").Replace(",", ".");

                //txtRedondeo.Text = entity.Redondeo.ToString("").Replace(",", ".");
                txtObservaciones.Text = entity.Observaciones;

                litTotal.Text = Convert.ToDecimal(entity.Iva + entity.Total.Value + entity.TotalImpuestos + entity.Redondeo).ToMoneyFormat(2);
                liTotalImpuestos.Text = Convert.ToDecimal(entity.TotalImpuestos).ToMoneyFormat(2);
                liTotalImportes.Text = Convert.ToDecimal(entity.Total).ToMoneyFormat(2);
                if (entity.IDCategoria.HasValue)
                    ddlCategoria.SelectedValue = entity.IDCategoria.Value.ToString();
                if (entity.Rubro != null)
                    ddlRubro.SelectedValue = entity.Rubro;

                hdnTieneSaldoAPagar.Value = entity.Saldo > 0 ? "1" : "0";

                if (!string.IsNullOrWhiteSpace(entity.Foto))
                {
                    hdnFileName.Value = entity.Foto;
                    hdnTieneFoto.Value = "1";

                    lnkComprobante.NavigateUrl = "/pdfGenerator.ashx?file=" + entity.Foto + "&tipoDeArchivo=compras&year="+entity.Fecha.Year;
                }

                ddlPlanDeCuentas.SelectedValue = entity.IDPlanDeCuenta.ToString();

                litUsuarioAlta.Text = entity.UsuarioAlta;
                litUsuarioModifica.Text = entity.UsuarioModifica;
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static int guardar(int id, int idPersona, string fecha, string nroFactura,
        string iva, string importe2, string importe5, string importe10, string importe21, string importe27, string noGrav, string importeMon,
        string impNacional, string impMunicipal, string impInterno, string percepcionIva, string otros,
        string obs, string tipo, string idCategoria, string rubro, string exento, string FechaEmision, int idPlanDeCuenta, List<JurisdiccionesViewModel> Jurisdicciones, string fechaPrimerVencimiento, string fechaSegundoVencimiento, int idOrden)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            int idCompra = 0;
            using (var dbContext = new ACHEEntities())
            {
                var compras = ComprasCommon.Guardar(id, idPersona, fecha, nroFactura, iva, importe2, importe5, importe10, importe21, importe27, noGrav, importeMon,
                    impNacional, impMunicipal, impInterno, percepcionIva, otros, obs, tipo, idCategoria, rubro, exento, FechaEmision, idPlanDeCuenta, usu.IDUsuario, Jurisdicciones, fechaPrimerVencimiento, fechaSegundoVencimiento, false, new List<DetalleViewModel>(), 0, idOrden, usu.Email);

                idCompra = compras.IDCompra;

            }
            GenerarAsientosContables(idCompra);
            return idCompra;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    #region CATEGORIAS

    [WebMethod(true)]
    public static string getCategories(string claseJS)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ComprasCommon.ObtenerCategorias(usu.IDUsuario, claseJS);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void guardarCategoria(int id, string nombre)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ComprasCommon.GuardarCategoria(id, nombre, usu.IDUsuario);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarCategoria(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ComprasCommon.EliminarCategoria(id, usu.IDUsuario);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    #endregion

    #region JURISDICCIONES
    
    [WebMethod(true)]
    public static List<JurisdiccionesViewModel> getJurisdicciones(int idCompra)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            return ComprasCommon.ObtenerJurisdicciones(idCompra);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> ObtenerJurisdiccionUsuario()
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            return ComprasCommon.ObtenerJurisdiccionUsuario(usu.IDJurisdiccion);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    
    #endregion

    [WebMethod(true)]
    public static void eliminarFoto(int idCheque)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        ComprasCommon.EliminarFoto(idCheque, usu.IDUsuario);
    }

    [WebMethod(true)]
    public static string getImporteDeOC(int idOrden)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return ComprasCommon.ObtenerImporteDeOC(idOrden, usu.IDUsuario);

    }

    [WebMethod(true)]
    public static void GenerarAsientosContables(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            try
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ContabilidadCommon.AgregarAsientoDeCompra(id, usu);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
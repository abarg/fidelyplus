﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Common;

public partial class compras : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            //txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");

            using (var dbContext = new ACHEEntities())
            {
                var TieneDatos = dbContext.Compras.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos)
                {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else
                {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);
        }
    }


    [System.Web.Services.WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ComprasCommon.EliminarCompra(id, usu.IDUsuario);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosComprasViewModel getResults(int idPersona, string condicion, string periodo,
        string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                return ComprasCommon.ObtenerCompras(condicion, periodo, fechaDesde, fechaHasta, page, pageSize, usu.IDUsuario);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(int idPersona, string condicion, string periodo, string fechaDesde, string fechaHasta)
    {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ComprasCommon.ExportarCompras(idPersona, condicion, periodo,fechaDesde,fechaHasta, usu.IDUsuario);
            
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modulos_reportes_compras_por_categoria_mensual : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static string getResults(int anio, int incluirIva, int incluirCot)
    {
        try
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var html = "";
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.Database.SqlQuery<RptComprasPorCategoriaMensualViewModel>("exec RptComprasPorCategoria " + usu.IDUsuario + "," + anio + "," + incluirIva + "," + incluirCot, new object[] { }).ToList();

                    html += " <table  class='table mb30'><thead> <tr> ";
                    html += "<th>Categoría</th>";

                    for (int i = 1; i <= 12; i++)
                    {
                        DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;

                        html += "<th style='text-align: center;border-left-style: solid;'>" + dtinfo.GetMonthName(i).ToProperCase() + "</th>";
                    }
                    html += " </tr></thead> <tbody>";
                    var cat = list.OrderBy(x => x.Categoria).Select(x => x.Categoria).Distinct();
                    foreach (var a in cat)
                    {
                        html += " <tr>  <td style='text-align: left;border-right-style: solid;'>" + a + "</td>";
                        for (int i = 1; i <= 12; i++)
                        {
                            var aux = list.Where(x => x.Mes == i && x.Categoria == a).ToList();
                            decimal total = 0;
                            foreach (var b in aux)
                            {
                                total += b.TotalCompra;
                            }
                            //if (aux != null)
                            //    total = aux.TotalCompra;

                            html += "<td style='text-align: right;border-right-style: solid;'>" + total.ToString("N2") + "</td>";

                        }
                        html += "</tr>";
                    }
                    html += "</tbody> ";
                    return html;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export(int anio, int incluirIva, int incluirCot)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Reporte_CatMes_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var list = dbContext.Database.SqlQuery<RptComprasPorCategoriaMensualViewModel>("exec RptComprasPorCategoria " + usu.IDUsuario + "," + anio + "," + incluirIva + "," + incluirCot, new object[] { }).ToList();
                    dt.Columns.Add("Categoria", typeof(string));

                    for (int i = 1; i <= 12; i++)
                    {
                        DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;

                        dt.Columns.Add(dtinfo.GetMonthName(i), typeof(decimal));
                    }
                    var cat = list.OrderBy(x => x.Categoria).Select(x => x.Categoria).Distinct();
                    foreach (var a in cat)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = a;
                        for (int i = 1; i <= 12; i++)
                        {
                            //dt.Columns[i].DataType = typeof(decimal);
                            var aux = list.Where(x => x.Mes == i && x.Categoria == a).ToList();
                            decimal total = 0;
                            foreach (var b in aux)
                            {
                                total += b.TotalCompra;
                            }
                            //if (aux != null)
                            //    total = aux.TotalCompra;

                            dr[i] = total;
                        }
                        dt.Rows.Add(dr);
                    }

                    if (dt.Rows.Count > 0)
                        CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                    else
                        throw new Exception("No se encuentran datos para los filtros seleccionados");

                    return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg,
                    e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

}
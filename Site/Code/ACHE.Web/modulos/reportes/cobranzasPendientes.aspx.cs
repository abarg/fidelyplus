﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using System.Collections.Generic;
using ClosedXML.Excel;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Common;
using System.Net.Mail;
using System.Data.Entity;

public partial class modulos_reportes_cobranzasPendientes : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            //txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);
            //lnkCredility.HRef = "https://www.credility.com/landing-contabilium?email=" + CurrentUser.Email + "&nombre=" + CurrentUser.RazonSocial + "&cuit=" + CurrentUser.CUIT + "&referralid=Contabilium&telefono=" + CurrentUser.Telefono + "&tipo=" + CurrentUser.CondicionIVA;
            //lnkCredility.HRef = "http://www.vaxtcapital.com/contabilium?email=" + CurrentUser.Email + "&nombre=" + CurrentUser.RazonSocial + "&cuit=" + CurrentUser.CUIT + "&referralid=Contabilium&telefono=" + CurrentUser.Telefono + "&tipo=" + CurrentUser.CondicionIVA;

            txtMasivoAsunto.Value = CurrentUser.EmailDisplayFrom + " - CONSULTA DE PAGO";

            //TODO: pasar a businnes
            using (var dbContext = new ACHEEntities())
            {
                var cant = dbContext.CobranzasDetalle.Where(x => !x.IDComprobante.HasValue && x.Cobranzas.IDUsuario == CurrentUser.IDUsuario).Select(x => x.IDCobranza).Distinct().Count();
                if (cant > 0)
                {
                    divAlert.Visible = true;
                    litMsj.Text = "Usted tiene <a style='color: #8a6d3b;font-weight: bold;text-decoration: underline;' href='/aplicarCobranzasACuenta.aspx'>" + cant + " cobranza(s)</a> a cuenta pendiente(s) de imputación";
                }
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptCobranzasPendientesViewModel getResults(int idPersona, string fechaDesde, string fechaHasta, int page, int pageSize, bool agrupar)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptCobranzasPendientes.Where(x => x.IDUsuario == usu.IDUsuario && x.tipo != "NCA" && x.tipo != "NCB" && x.tipo != "NCC" && x.tipo != "NCM" && x.tipo != "NCE").AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(dtHasta));
                    }
                    page--;
                    ResultadosRptCobranzasPendientesViewModel resultado =
                        new ResultadosRptCobranzasPendientesViewModel();
                    var aux = new List<RptCobranzasPendientesViewModel>();
                    if (agrupar)
                    {
                        var resultsAgrupados = results.GroupBy(x => x.IDPersona).ToList();
                        resultado.TotalPage = ((resultsAgrupados.Count() - 1) / pageSize) + 1;
                        resultado.TotalItems = resultsAgrupados.Count();

                        var list = resultsAgrupados.Skip(page * pageSize).Take(pageSize).ToList()
                            .Select(x => new RptCobranzasPendientesViewModel()
                            {
                                IDPersona = x.FirstOrDefault().IDPersona,
                                RazonSocial = x.FirstOrDefault().RazonSocial,
                                NroDocumento =
                                    (x.FirstOrDefault().NroDocumento == "")
                                        ? "-"
                                        : x.FirstOrDefault().TipoDocumento + " " + x.FirstOrDefault().NroDocumento,
                                CondicionIVA = x.FirstOrDefault().CondicionIVA,
                                NroFactura = x.Count().ToString(),
                                Importe = x.Sum(y => y.Importe).ToMoneyFormat(2),
                                Iva = x.Sum(y => y.Iva).ToMoneyFormat(2),
                                importeTotal = x.Sum(y => y.ImporteTotal).ToMoneyFormat(2),
                                Saldo = x.Sum(y => y.Saldo).ToMoneyFormat(2)

                            });

                        aux.AddRange(list.OrderBy(x => x.RazonSocial.Replace(" ", "")));
                    }
                    else
                    {
                        resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                        resultado.TotalItems = results.Count();

                        var list = results.OrderByDescending(x => x.FechaComprobante).Skip(page * pageSize).Take(pageSize).ToList()
                            .Select(x => new RptCobranzasPendientesViewModel()
                            {
                                Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                                FechaVencimiento = x.FechaVencimiento.ToString("dd/MM/yyyy"),
                                RazonSocial = x.RazonSocial,
                                NroDocumento = (x.NroDocumento == "") ? "-" : x.TipoDocumento + " " + x.NroDocumento,
                                CondicionIVA = x.CondicionIVA,
                                NroFactura = x.tipo + " " + x.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                                Importe = x.Importe.ToMoneyFormat(2),
                                Iva = x.Iva.ToMoneyFormat(2),
                                importeTotal = x.ImporteTotal.ToMoneyFormat(2),
                                Saldo = x.Saldo.ToMoneyFormat(2),
                                IDComprobante = x.IDComprobante
                            });


                        foreach (var item in list.ToList())
                        {
                            var fechaVen = DateTime.Parse(item.FechaVencimiento);

                            if (fechaVen < DateTime.Now)
                                item.Estado = "Vencida";
                            else
                                item.Estado = "Por vencer";
                            aux.Add(item);
                        }
                    }

                    resultado.Items = aux.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export(int idPersona, string fechaDesde, string fechaHasta, bool agrupar)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Pago_a_proveedores_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results =
                        dbContext.RptCobranzasPendientes.Where(
                            x => x.IDUsuario == usu.IDUsuario && x.tipo != "NCA" && x.tipo != "NCB" && x.tipo != "NCC" && x.tipo != "NCM" && x.tipo != "NCE")
                            .AsQueryable();

                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(dtHasta));
                    }
                    if (agrupar)
                    {
                        var resultsAgrupados = results.GroupBy(x => x.IDPersona).ToList();
                        dt = resultsAgrupados.ToList()
                            .Select(x => new
                            {
                                RazonSocial = x.FirstOrDefault().RazonSocial,
                                NroDocumento =
                                    (x.FirstOrDefault().NroDocumento == "")
                                        ? "-"
                                        : x.FirstOrDefault().TipoDocumento + " " + x.FirstOrDefault().NroDocumento,
                                CondicionIVA = x.FirstOrDefault().CondicionIVA,
                                CantFacturas = x.Count().ToString(),
                                Importe = x.Sum(y => y.Importe),
                                Iva = x.Sum(y => y.Iva),
                                importeTotal = x.Sum(y => y.ImporteTotal),
                                Saldo = x.Sum(y => y.Saldo)
                            }).OrderBy(x => x.RazonSocial.Replace(" ", "")).ToList().ToDataTable();
                    }
                    else
                    {
                        dt = results.OrderByDescending(x => x.FechaComprobante).ToList().Select(x => new
                        {
                            Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                            FechaVencimiento = x.FechaVencimiento.ToString("dd/MM/yyyy"),
                            RazonSocial = x.RazonSocial,
                            TipoDocumento = x.TipoDocumento,
                            NroDocumento = x.NroDocumento,
                            CondicionIVA = x.CondicionIVA,
                            NroFactura = x.tipo + " " + x.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                            Importe = x.Importe,
                            Iva = x.Iva,
                            importeTotal = x.ImporteTotal,
                            Saldo = x.Saldo
                        }).ToList().ToDataTable();
                    }
                }
                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void envioMailMasivo(string ids, bool agrupar, string fechaDesde, string fechaHasta, string asunto, string mensaje)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results =
                        dbContext.RptCobranzasPendientes.Where(
                            x => x.IDUsuario == usu.IDUsuario && x.tipo != "NCA" && x.tipo != "NCB" && x.tipo != "NCC" && x.tipo != "NCM" && x.tipo != "NCE")
                            .AsQueryable();

                    var itemsSeleccionados = Regex.Split(ids, "chkItem_").ToList();
                    var searchIds = new List<int>();
                    foreach (var id in itemsSeleccionados)
                    {
                        if (id.Trim() != string.Empty)
                            searchIds.Add(int.Parse(id.Trim()));
                    }

                    if (agrupar)//obtengo los comprobantes de las personas.
                        results = results.Where(x => searchIds.Contains(x.IDPersona));
                    else
                        results = results.Where(x => searchIds.Contains(x.IDComprobante));

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    //ahora obtengo todo ordenado por idpersona y fecha
                    var resultsAEnviar = results.OrderBy(x => x.IDPersona).ThenBy(x => x.FechaComprobante).ToList().Select(x => new
                    {
                        IDPersona = x.IDPersona,
                        RazonSocial = x.RazonSocial,
                        Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                        NroFactura = x.tipo + " " + x.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                        Saldo = x.Saldo
                    }).ToList();

                    int idPersonaAnt = 0;
                    decimal saldo = 0;
                    var html = "";
                    var razonSocial = "";
                    ListDictionary replacements = null;
                    var emailUsuarioTo = !string.IsNullOrEmpty(usu.EmailAlerta) ? usu.EmailAlerta : usu.Email;
                    Personas persona = null;
                    MailAddressCollection listTo = null;

                    foreach (var comp in resultsAEnviar)
                    {
                        if (idPersonaAnt == 0)
                        {
                            idPersonaAnt = comp.IDPersona;
                            razonSocial = comp.RazonSocial;
                            html = "<table align='left' cellpadding='3' cellspacing='2' border='0' align='center' style='font-size:15px;font-family: Helvetica Neue,Arial,sans-serif;border:1px solid #ccc'><tr style='background-color:#ccc'><td>Fecha</td><td>Nro</td><td>Importe</td></tr>";//vacio el mensaje
                        }

                        if (comp.IDPersona != idPersonaAnt)//armo el mismo mail
                        {
                            //inserto el saldo
                            html += "<tr>";
                            html += "<td>&nbsp;</td>";
                            html += "<td>Saldo Total</td>";
                            html += "<td style='text-align:right'>$" + saldo.ToMoneyFormat(2) + "</td>";
                            html += "</tr></table>";

                            //TODO: envio mail
                            replacements = new ListDictionary();
                            replacements.Add("<USUARIO>", razonSocial);
                            replacements.Add("<NOTIFICACION>", mensaje + "<br><br>" + html);

                            persona = dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersonaAnt).First();
                            listTo = new MailAddressCollection();
                            if (persona.EmailsEnvioFc != "")
                                listTo.Add(persona.EmailsEnvioFc);
                            else if (persona.Email != "")
                                listTo.Add(persona.Email);

                            if (saldo>0 && listTo.Any())
                            {
                                bool send = EmailCommon.SaveMessage(usu.IDUsuario, EmailTemplate.ConsultaDePago, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuarioTo, asunto, null, usu.EmailDisplayFrom ?? usu.RazonSocial);
                            }

                            html = "<table align='left' cellpadding='3' cellspacing='2' border='0' align='center' style='font-size:15px;font-family: Helvetica Neue,Arial,sans-serif;border:1px solid #ccc'><tr style='background-color:#ccc'><td>Fecha</td><td>Nro</td><td>Importe</td></tr>";//vacio el mensaje
                            razonSocial = comp.RazonSocial;
                            saldo = 0;
                        }

                        saldo += comp.Saldo;
                        idPersonaAnt = comp.IDPersona;

                        html += "<tr>";
                        html += "<td>" + comp.Fecha + "</td>";
                        html += "<td>" + comp.NroFactura + "</td>";
                        html += "<td style='text-align:right'>$" + comp.Saldo.ToMoneyFormat(2) + "</td>";
                        html += "</tr>";
                    }

                    //envio el ultimo mail
                    //TODO: envio mail
                    //inserto el saldo
                    html += "<tr>";
                    html += "<td></td>";
                    html += "<td>Saldo Total</td>";
                    html += "<td style='text-align:right'>$" + saldo.ToMoneyFormat(2) + "</td>";
                    html += "</tr></table>";


                    replacements = new ListDictionary();
                    replacements.Add("<USUARIO>", razonSocial);
                    replacements.Add("<NOTIFICACION>", mensaje + "<br><br>" + html);

                    persona = dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersonaAnt).First();
                    listTo = new MailAddressCollection();
                    if (persona.EmailsEnvioFc != "")
                        listTo.Add(persona.EmailsEnvioFc);
                    else if (persona.Email != "")
                        listTo.Add(persona.Email);

                    if (saldo > 0 && listTo.Any())
                    {
                        bool send = EmailCommon.SaveMessage(usu.IDUsuario, EmailTemplate.ConsultaDePago, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuarioTo, asunto, null, usu.EmailDisplayFrom ?? usu.RazonSocial);
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
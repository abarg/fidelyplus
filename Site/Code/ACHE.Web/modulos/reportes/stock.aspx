﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="stock.aspx.cs" Inherits="modulos_reportes_stock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Stock de productos</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">Stock de productos</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="div1" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="Span1"></span>
                        </div>
                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-8">
                                    <div class="row">

                                       <div class="col-sm-6 col-md-3 form-group inventario">
                                            <asp:DropDownList runat="server" ID="ddlInventarios" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6 col-md-6 form-group">
                                            <input type="text" class="form-control" id="txtConcepto" maxlength="128" placeholder="Ingresá el nombre o codigo del producto" />
                                        </div>
                                         <div class="col-sm-6 col-md-3 form-group">
                                             <select runat="server" id="ddlEstado"  class="form-control">
                                                <option value=""></option>
                                                <option value="A" selected>Activo</option>
                                                <option value="I">Inactivo</option>
                                            </select>
                                        </div>
                                       <br/>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 responsive-buttons md">
                                     <a class="btn btn-black" onclick="resetearPagina();filtrar(false);">Buscar</a>
                                     <a class="btn btn-default" onclick="resetearPagina();verTodos();">Ver todos</a>
                                </div>
                            </div>

                        </form>

                        <div class="col-sm-12"><hr class="mt0" /></div>
                        
                        <div class="row">
                            <div class="pull-right">
                                 <div class="btn-group mr10">
                                    <div class="btn btn-white tooltips">
                                      <input type="checkbox" id="chkAgrupar" onchange="filtrar(checked)"/>&nbsp;Agrupar por Producto
                                    </div>

                                </div>
                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="resetearExportacion();" download="CuentaCorriente" style="display: none">Descargar</a>
                                    </div>

                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <div  class="panel-body">

                        <div class="alert alert-danger" id="divError" style="display:none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <div id="divResultados" class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Depósito</th>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Stock</th>
                                        <th>Stock Reservado</th>
                                        <th>Stock Disp.</th>
                                        <th style="text-align:right">Costo unitario</th>
                                        <th style="text-align:right">Precio Unitario</th>
                                        <th style="text-align:right">Valor Total</th>
                                        <th style="text-align:right">Costo Total</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                
                                
                                </tbody>
                            </table>
                        </div>
                           <div id="divResultadosAgrupados" class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Stock Total</th>
                                        <th>Stock Reservado</th>
                                        <th>Stock Disp.</th>
                                        <th style="text-align:right">Costo Unitario</th>
                                        <th style="text-align:right">Precio Unitario</th>
                                        <th style="text-align:right">Valor Total</th>
                                        <th style="text-align:right">Costo Total</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainerAgrupados">
                                
                                
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
             <td>
                ${Inventario}
            </td>
            <td>
                ${Valor2}
            </td>
            <td>
                ${Valor1}
            </td>
            <td>
                ${Cantidad}
            </td>
            <td>
                ${Cantidad2}
            </td>
            <td>
                ${Cantidad3}
            </td>
            <td style="text-align:right">
                ${CostoInterno}
            </td>
            <td style="text-align:right">
                ${Precio}
            </td>
            <td style="text-align:right">
                ${Total}
            </td>
            <td style="text-align:right">
                ${CostoTotal}
            </td>
        </tr>
        {{/each}}
    </script>

       <script id="resultTemplateAgrupados" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
             
            <td>
                ${Valor2}
            </td>
            <td>
                ${Valor1}
            </td>
            <td>
                ${Cantidad}
            </td>
            <td>
                ${Cantidad2}
            </td>
            <td>
                ${Cantidad3}
            </td>
            <td style="text-align:right">
                ${CostoInterno}
            </td>
            <td style="text-align:right">
                ${Precio}
            </td>
            <td style="text-align:right">
                ${Total}
            </td>
            <td style="text-align:right">
                ${CostoTotal}
            </td>
        </tr>
        {{/each}}
    </script>


     <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="7">
               No se han encontrado resultados
            </td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script src="/js/views/reportes/rpt-stock.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            configFilters();
            $("#divResultadosAgrupados").hide();
        });
    </script>
</asp:Content>


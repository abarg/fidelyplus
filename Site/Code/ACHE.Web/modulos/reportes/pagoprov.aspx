﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="pagoprov.aspx.cs" Inherits="modulos_reportes_pagoprov" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Pagos a proveedores </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">Pagos a proveedores</li>
            </ol>
        </div>
    </div>

   <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>
                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-8">
                                    <div class="row">

                                        <div class="col-sm-3 col-md-6 form-group">
                                            <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Desde" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Hasta" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 responsive-buttons md">
                                    <a class="btn btn-black" onclick="resetearPagina();filtrar(false);">Buscar</a>
                                    <a class="btn btn-default" onclick="resetearPagina();verTodos();">Ver todos</a>  
                                </div>
                            </div>

                        </form>
                        <div class="col-sm-12"><hr class="mt0" /></div>
                        
                        <div class="row">
                            <div class="pull-right">
                                 <div class="btn-group mr10" runat="server" id="divExportar">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="resetearExportacion();" download="cobranzasPendientes" style="display: none">Descargar</a>
                                    </div>

                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th style="min-width: 120px;">Número</th>
                                        <th>Fecha</th>
                                        <th>Proveedor</th>
                                        <%--<th>Documento</th>--%>
                                        <th style="min-width: 160px;">Comprobante</th>
                                        <th>Fecha emisión</th>
                                        <th>1º venc.</th>
                                        <th style="min-width: 80px;">2º venc.</th>
                                        <th style="text-align:right">Imp. Grav.</th>
                                        <th style="text-align:right">IVA</th>
                                        <th style="text-align:right">Total</th>
                                        <th style="text-align:right">Ret.</th>
                                        <th style="text-align:right">Total Abonado</th>
                                        <th style="text-align:right">Saldo</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                
                                
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td><a href="/modulos/compras/pagose.aspx?ID=${IDPago}">${NroPago}</a></td>
            <td>${Fecha}</td>
            <td>${Proveedor}</td>
            <%--<td style="min-width:145px">${TipoDocumento} ${NroDocumento}</td>--%>
            {{if $value.IDCompra != "0"}}
                <td style="min-width:130px"><a href="/modulos/compras/comprase.aspx?ID=${IDCompra}">${NroFactura}</a></td>
            {{else}} 
                <td style="min-width:130px">Pago a cuenta</td>
            {{/if}} 
            <td>${FechaEmision}</td>
            <td>${FechaPrimerVencimiento}</td>
            <td>${FechaSegundoVencimiento}</td>
            <td style="text-align:right">${Importe}</td>
            <td style="text-align:right">${Iva}</td>
            <td style="text-align:right">${Total}</td>
            <td style="text-align:right">${Retenciones}</td>
            <td style="text-align:right">${TotalAbonado}</td>
            <td style="text-align: right; font-weight:bold">${Saldo}</td>
        </tr>
        {{/each}}
    </script>
      <script id="resultTemplateTotales" type="text/x-jQuery-tmpl">
        {{each results}}
         <tr>
            <td colspan="10" class="bgTotal" ></td>
            <td colspan="2" class="bgTotal text-danger" style="text-align:right">Total: ${TotalGeneral}</td>
               <td colspan="2" class="bgTotal text-danger" style="text-align:right">Saldo: ${SaldoGeneral}</td>
        </tr>
       {{/each}}
    </script>
     <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="12">
               No se han encontrado resultados
            </td>
        </tr>
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script src="/js/views/reportes/pagoprov.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            filtrar();
            configFilters();
        });
    </script>
    
</asp:Content>


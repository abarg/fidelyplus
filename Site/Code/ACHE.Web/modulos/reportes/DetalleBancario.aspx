﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="DetalleBancario.aspx.cs" Inherits="modulos_reportes_DetalleBancario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-university'></i> Movimientos bancarios</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href='#'>Tesorería</a></li>
                <li class="active">Movimientos Bancarios</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-9">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-4 form-group">
                                        
                                            <select class="select2" data-placeholder="Seleccione un banco..." id="ddlBanco">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-12 col-md-4 form-group">
                                            <input type="text" class="form-control" id="txtFiltro" maxlength="128" placeholder="Ingresa el Tipo, Nro Ref o Comprobante" />
                                        </div>
                                        <div class="col-sm-3 col-md-2 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-2 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-3 responsive-buttons md">
                                    <a class="btn btn-black" onclick="DetalleBancario.resetearPagina();DetalleBancario.filtrar();" id="btnBuscar">Buscar</a>
                                    <%--<a class="btn btn-default" onclick="DetalleBancario.resetearPagina();DetalleBancario.verTodos();">Ver todos</a>--%>
                                    <a class="btn btn-warning" onclick="DetalleBancario.nuevo();">
                                        <i class="fa fa-plus"></i>&nbsp;Nuevo movimiento
                                    </a>
                                </div>
                            </div>
                        </form>
                        
                        <div class="col-sm-12"><hr class="mt0" /></div>
                        
                        <div class="row">
                            <div class="pull-right">
                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:DetalleBancario.exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="DetalleBancario.resetearExportacion();" download="CuentaCorriente" style="display: none">Descargar</a>
                                    </div>
                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="DetalleBancario.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="DetalleBancario.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Banco</th>
                                        <th>Tipo</th>
                                        <th>Forma de pago</th>
                                        <th>Nro Ref</th>
                                        <th>Comprobante</th>
                                        <%--<th>Tipo</th>--%>
                                        <th style="text-align: right">Importe</th>
                                        <th class="columnIcons"></th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td>${Fecha}</td>
            <td>${Nombre}</td>
            <td>${Tipo}</td>
            <td>${FormaDePago}</td>
            <td>${NroReferencia}</td>
            <td style="min-width: 130px">${Comprobante}</td>
            <%--<td>${TipoMovimiento}</td>--%>
            <td style="text-align: right">${Importe}</td>
            <td style="text-align: right">
                {{if PuedeEditar == "SI" }}
                    <a onclick="DetalleBancario.editar('${Entidad}',${IDEntidad});" style="cursor: pointer; font-size: 16px; color: currentColor" title="Editar">
                        <i class="fa fa-pencil"></i>
                    </a>
                    {{if Entidad == "Banco Detalle" }}
                        <a onclick="DetalleBancario.eliminar(${IDEntidad});" style="cursor: pointer; font-size: 16px; color: currentColor" title="Editar">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    {{/if}}
                {{/if}}
            </td>
           <%-- <td style="text-align: right; font-weight:bold">${Saldo}</td>--%>
        </tr>
        {{/each}}
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="8">No se han encontrado resultados</td>
        </tr>
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/DetalleBancario.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            DetalleBancario.configFilters();
            DetalleBancario.filtrar();
        });
    </script>

</asp:Content>
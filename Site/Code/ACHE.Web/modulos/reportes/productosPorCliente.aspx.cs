﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity;

public partial class modulos_reportes_productosPorCliente : BasePage
{
    private static List<RptProductosPorClienteViewModel> productosPorClienteResults = new List<RptProductosPorClienteViewModel>();
    private static List<RptProductosPorClienteDetalleViewModel> comprasDetalles = new List<RptProductosPorClienteDetalleViewModel>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
            divExportarDetalle.Visible = divExportar.Visible;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptProductosPorClienteViewModel getResults(int idConcepto, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            productosPorClienteResults.Clear();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptProductosPorCliente.Where(x => x.IDUsuario == usu.IDUsuario && x.IDConcepto == idConcepto);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        //results = results.Where(x => x.FechaComprobante <= dtHasta);
                        results = results.Where(x => DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    var tmp = results.OrderBy(x => x.FechaComprobante).ToList();

                    var groupResults = new Dictionary<int, RptProductosPorClienteViewModel>();
                    foreach (var item in tmp)
                    {
                        if (groupResults.ContainsKey(item.IDPersona))
                        {
                            groupResults[item.IDPersona].Cantidad += item.Cantidad;
                            groupResults[item.IDPersona].Total += item.Total;
                        }
                        else
                        {
                            var det = new RptProductosPorClienteViewModel();
                            det.IDCliente = item.IDPersona;
                            det.Cantidad = item.Cantidad;
                            det.Total = item.Total;
                            det.Cliente = item.RazonSocial;
                            groupResults.Add(item.IDPersona, det);
                        }
                    }

                    page--;
                    ResultadosRptProductosPorClienteViewModel resultado = new ResultadosRptProductosPorClienteViewModel();
                    resultado.TotalPage = ((groupResults.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = groupResults.Count();
                    productosPorClienteResults = groupResults.Values.Skip(page * pageSize).Take(pageSize).ToList();
                    resultado.Items = productosPorClienteResults.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Productos_Por_Cliente_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                dt = productosPorClienteResults.Select(x => new
                {
                    Cliente = x.Cliente,
                    Cantidad = x.Cantidad,
                    MontoTotal = x.Total

                }).ToList().ToDataTable();

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(
                    HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg,
                    e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string getDetail(int idPersona, int idConcepto, string fechaDesde, string fechaHasta)
    {
        comprasDetalles.Clear();
        DateTime dtDesde = DateTime.MinValue;
        DateTime dtHasta = DateTime.MinValue;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var html = "";
            if (fechaDesde != string.Empty)
            {
                dtDesde = DateTime.Parse(fechaDesde);
            }
            if (fechaHasta != string.Empty)
            {
                dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
            }
            comprasDetalles = ObtenerDetalle(idPersona, idConcepto, dtDesde, dtHasta);

            if (comprasDetalles.Any())
            {
                decimal total = 0;
                decimal cant = 0;
                foreach (var detalle in comprasDetalles)
                {
                    html += "<tr>";
                    html += "<td class='bgRow'>" + detalle.Fecha + "</td>";
                    html += "<td class='bgRow'>" + detalle.Comprobante + "</td>";
                    html += "<td class='bgRow' style='text-align: right'>" + detalle.Cantidad.ToMoneyFormat(2) + "</td>";
                    html += "<td class='bgRow' style='text-align: right'>" + detalle.PrecioUnitario.ToMoneyFormat(2) + "</td>";
                    html += "<td class='bgRow' style='text-align: right'>" + detalle.Total.ToMoneyFormat(2) + "</td>";
                    html += "</tr>";
                    total += detalle.Total;
                    cant += detalle.Cantidad;
                }

                html += "<tr>";
                html += "<td class='bgTotal' colspan='2'></td>";
                html += "<td class='bgTotal text-danger' style='text-align:right'>" + cant.ToMoneyFormat(2) + "</td>";
                html += "<td class='bgTotal text-danger' style='text-align:right'></td>";
                html += "<td class='bgTotal text-danger' style='text-align:right'>" + total.ToMoneyFormat(2) + "</td>";
                html += "</tr>";
            }
            else
                html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";


            return html;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static List<RptProductosPorClienteDetalleViewModel> ObtenerDetalle(int idPersona, int idConcepto, DateTime fechaDesde, DateTime fechaHasta)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        RptProductosPorClienteDetalleViewModel det;
        using (var dbContext = new ACHEEntities())
        {
            var listEgr = dbContext.ComprobantesDetalle.Include("Comprobantes").Where(
                    x => x.Comprobantes.IDUsuario == usu.IDUsuario &&
                        x.Comprobantes.IDPersona == idPersona && x.Comprobantes.FechaComprobante >= fechaDesde &&
                        DbFunctions.TruncateTime(x.Comprobantes.FechaComprobante) <= DbFunctions.TruncateTime(fechaHasta)
                        && x.IDConcepto == idConcepto)
                    .OrderBy(x => x.Comprobantes.FechaComprobante)
                    .ToList();



            foreach (var item in listEgr)
            {
                det = new RptProductosPorClienteDetalleViewModel();
                det.RazonSocial = item.Comprobantes.Personas.RazonSocial;
                det.Comprobante = item.Comprobantes.Tipo + " " + item.Comprobantes.PuntosDeVenta.Punto.ToString("#0000") + "-" + item.Comprobantes.Numero.ToString("#00000000");
                det.Fecha = item.Comprobantes.FechaComprobante.ToString("dd/MM/yyyy");
                if (item.Comprobantes.Tipo == "NCA" || item.Comprobantes.Tipo == "NCB" || item.Comprobantes.Tipo == "NCC")
                {
                    det.Cantidad = (-1) * item.Cantidad;
                    det.PrecioUnitario = (-1) * item.PrecioUnitario;
                    det.Total = (-1) * Convert.ToDecimal(item.Cantidad) * item.PrecioUnitario;
                }
                else
                {
                    det.Cantidad = item.Cantidad;
                    det.PrecioUnitario = item.PrecioUnitario;
                    det.Total = Convert.ToDecimal(item.Cantidad) * item.PrecioUnitario;
                }
                comprasDetalles.Add(det);
            }
        }

        return comprasDetalles;
    }

    [WebMethod(true)]
    public static string exportDetalle()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            string fileName = "Detalle_Prods_Por_Cliente_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                //if (comprasDetalles.Count > 0)
                //    fileName += comprasDetalles[0].RazonSocial + "_";
                
                DataTable dt = comprasDetalles.ToDataTable();
                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="cobranzasPendientes.aspx.cs" Inherits="modulos_reportes_cobranzasPendientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Cobranzas pendientes </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li>Reportes</li>
                <li class="active">Cobranzas pendientes</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <%--<div class="alert alert-info">
                            <strong>Nuevo reporte de saldos!</strong> 
                            <span>Ya se encuentra disponible el nuevo reporte de <b><a id="lnkCredility" href="saldosClientes.aspx">saldos de clientes</a></b> para que veas más rápido aquellos clientes que te adeudan.</span>
                        </div>--%>
                        
                        <div class="alert alert-warning mb15" id="divAlert" runat="server" visible="false">
		                    <asp:Literal runat="server" ID="litMsj"></asp:Literal>
	                    </div>

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <%--<div class="alert alert-info">
                            <strong>Beneficio EXCLUSIVO!</strong> 
                            <span>Gracias a nuestra alianza con <b>Vaxt Capital</b>, ahora podés acceder a financiamiento de forma rápida, sin fricciones y a una tasa preferencial. <a id="lnkCredility" runat="server" target="_blank">Más información</a></span>
                        </div>--%>

                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <input type="hidden" value="0" id="hdnTotalCant"  />
                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-8">
                                    <div class="row">

                                       <div class="col-sm-6 col-md-6 form-group">
                                            <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Desde" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Hasta" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 responsive-buttons md">
                                    <a class="btn btn-black" onclick="cobranzasPendientes.resetearPagina();cobranzasPendientes.filtrar(false);">Buscar</a>
                                    <a class="btn btn-default" onclick="cobranzasPendientes.resetearPagina();cobranzasPendientes.verTodos();">Ver todos</a>
                                    
                                </div>
                            </div>

                            <!-- MODAL MSJ PRE GENERACION MAILS MASIVA -->
                            <div class="modal modal-wide fade" id="modalEnvioMasivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Envio de mail con detalle de saldo pendiente</h4>
                                        </div>
                                        <div class="modal-body" style="padding: 25px;padding-top:0px">
                                            <br />
                                            <div class="alert alert-success" id="divOkMail" style="display: none">
                                                <strong>Bien hecho! </strong>El mensaje ha sido enviado correctamente
                                            </div>
                                            <div class="alert alert-danger" id="divErrorMail" style="display: none">
                                                <strong>Lo sentimos! </strong><span id="msgErrorMail"></span>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Asunto:</label>
                                                <input type="text" id="txtMasivoAsunto" class="form-control required" runat="server" />
                                                <span id="msgErrorEnvioAsunto" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Mensaje predefinido:</label>
                                                <textarea rows="5" id="txtMasivoMensaje" class="form-control required">Se consulta fecha de pago para la/s factura/s:</textarea>
                                                <small>Debajo del mensaje predefinido se mostrará el detalle de deuda actual de forma automática al enviar el mail.</small>
                                                <span id="msgErrorEnvioMensaje" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                            </div>
                                            <div id="divMsjPreMasivo" style="clear:left">
                        
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <a type="button" class="btn btn-success" onclick="cobranzasPendientes.enviarMail();" id="btnEnviar">Enviar</a>
                                            <a style="margin-left: 20px" href="#"  data-dismiss="modal">Cancelar</a>
                    
                                        </div>
                                    </div>
                                </div>
                             </div>


                        </form>

                        <div class="col-sm-12"><hr class="mt0" /></div>
                        
                        <div class="row">
                            <div class="pull-right">
                                 <div class="btn-group mr10">
                                    <div class="btn btn-white tooltips">
                                      <input type="checkbox" id="chkAgrupar" onchange="cobranzasPendientes.filtrar(checked);cobranzasPendientes.clearItems();"/>&nbsp;Agrupar por Cliente
                                    </div>

                                </div>
                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:cobranzasPendientes.exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="cobranzasPendientes.resetearExportacion();" download="cobranzasPendientes" style="display: none">Descargar</a>
                                    </div>

                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="cobranzasPendientes.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="cobranzasPendientes.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body" id="divResultados">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th style="max-width: 20px;">
                                            <input type='checkbox' id='chkMasiva' />
                                        </th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>Venc.</th>
                                        <th>Proveedor/Cliente</th>
                                        <th>Documento</th>
                                        <%--<th>Condición Iva</th>--%>
                                        <th>Nro Factura</th>
                                        <th style="text-align: right">Importe</th>
                                        <th style="text-align: right">IVA</th>
                                        <th style="text-align: right">Total</th>
                                        <th style="text-align: right">Saldo</th>
                                         <th ></th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="panel-body" id="divResultadosAgrupados">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th style="max-width: 20px;">
                                            <input type='checkbox' id='chkMasiva2' />
                                        </th>
                                        <th>Proveedor/Cliente</th>
                                        <th>Documento</th>
                                        <th>Cant Facturas</th>
                                        <th style="text-align: right">Importe</th>
                                        <th style="text-align: right">IVA</th>
                                        <th style="text-align: right">Total</th>
                                        <th style="text-align: right">Saldo</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainerAgrupados">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-white" onclick="cobranzasPendientes.showModalMasivo();" id="lnkMasiva" style="display:none">
                            <i class="glyphicon glyphicon-envelope"></i> Enviar consulta de pago
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td>
                <input style="left: 40px;" type='checkbox' id='chkItem_${IDComprobante}' class="chkItem" onchange="cobranzasPendientes.actualizarTotales()"/>
            </td>
            <td>
                {{if $value.Estado == "Vencida"}}
                <span class="badge badge-danger">Vencida</span>
                {{/if}} 
                {{if $value.Estado != "Vencida"}}
                <span class="badge badge-warning">Por vencer</span>
                {{/if}}
            </td>
            <td>${Fecha}</td>
            <td>${FechaVencimiento}</td>
            <td>${RazonSocial}</td>
            <td style="min-width: 145px">${NroDocumento}</td>
            <%--<td>${CondicionIVA}</td>--%>
            <td style="min-width: 155px"><a href="/comprobantesv.aspx?ID=${IDComprobante}">${NroFactura}</a></td>
            <td style="text-align: right">${Importe}</td>
            <td style="text-align: right">${Iva}</td>
            <td style="text-align: right">${importeTotal}</td>
            <td style="text-align: right; font-weight:bold">${Saldo}</td>
            <td><a title="Cobrar" href="/cobranzase.aspx?IDComprobante=${IDComprobante}">Cobrar</a></td>
        </tr>
        {{/each}}
    </script>
     <script id="resultTemplateAgrupados" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>            
            <td>
                <input style="left: 40px;" type='checkbox' id='chkItem_${IDPersona}' class="chkItem" onchange="cobranzasPendientes.actualizarTotales()"/>
            </td>
            <td>${RazonSocial}</td>
            <td style="min-width: 145px">${NroDocumento}</td>            
            <td style="min-width: 155px">${NroFactura}</td>
            <td style="text-align: right">${Importe}</td>
            <td style="text-align: right">${Iva}</td>
            <td style="text-align: right">${importeTotal}</td>
            <td style="text-align: right; font-weight:bold">${Saldo}</td>
        </tr>
        {{/each}}
    </script>

    <script id="resultTemplateTotales" type="text/x-jQuery-tmpl">
        {{each results}}
         <tr>
            <td colspan="10" class="bgTotal" ></td>
            <td colspan="2" class="bgTotal text-danger" style="text-align:right">Total: ${TotalGeneral}</td>
        </tr>
       {{/each}}
    </script>
    <script id="resultTemplateTotalesAgrupados" type="text/x-jQuery-tmpl">
        {{each results}}
         <tr>
            <td colspan="6" class="bgTotal" ></td>
            <td colspan="2" class="bgTotal text-danger" style="text-align:right">Total: ${TotalGeneral}</td>
        </tr>
       {{/each}}
    </script>
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="12">No se han encontrado resultados</td>
        </tr>
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/cobranzasPendientes.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            cobranzasPendientes.filtrar(false);
            cobranzasPendientes.configFilters();
        });
    </script>

</asp:Content>




﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="SicoreRetEmitidas.aspx.cs" Inherits="modulos_reportes_Sicore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
      <style>
        .ui-datepicker-calendar {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
       <div class="pageheader">
        <h2>
        <i class='fa fa-archive'></i>Sicore y e-Arciba
        </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">Sicore y e-Arciba</li>
            </ol>
        </div>
    </div>
     <div class="contentpanel">
        <div class="mb15">
            <div class="alert alert-warning fade in nomargin">
		        Este reporte trae las retenciones emitidas de Ganancias e IVA.
                Las retenciones de SUSS se informan directamente mediante un aplicativo online de AFIP. 
	        </div>
        </div>
         
         
         <div class="row">
            <div class="col-sm-4 col-md-3">
                <form runat="server" id="frmSearch">
                    <%--<div class="mb20"></div>
                    <h4 class="subtitle mb5">Filtros disponibles</h4>

                    <div class="mb20"></div>--%>

                    <h4 class="subtitle mb5">Periodo</h4>
                    <div class="row row-pad-5">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate " placeholder="seleccione el período" MaxLength="10"></asp:TextBox>
                            </div>
                            <%--<label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>--%>
                        </div>
                    </div>

                    <a class="btn btn-black" onclick="sicore.filtrar();" id="btnGenerar">Generar archivo</a>
                    <br />
                </form>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-right">
                            <div class="btn-group mr10">
                            </div>
                        </div>

                        <h4 class="panel-title">Resultado</h4>
                        <p id="msjResultados"></p>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <div class="table-responsive">
                            <div id="resultsContainer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        <table class="table mb30">
            <thead>
                <tr>
                    <th>Nombre del archívo</th>
                    <th>Descargar</th>
                </tr>
            </thead>
            <tbody>
                {{each results}}
                    <tr>
                        <td>${Nombre}</td>
                        <td>
                            {{if Errores ==""}}
                                <div class="btn btn-white">
                                    <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                    <a id="lnkDownload_${NombreArchivo}" onclick="sicore.descargar('lnkDownload_${NombreArchivo}','${URL}')" download>Descargar</a>
                                </div>
                                {{if Observaciones != "" }}
                                    <span class='label label-warning dropdown-toggle tooltips' data-placement="bottom" data-toggle="dropdown" data-original-title="${Observaciones}">Alerta. Haga click para ver el detalle</span>  
                                {{/if}}
                            {{else}}
                                <span class='label label-danger dropdown-toggle tooltips' data-placement="bottom" data-toggle="dropdown" data-original-title="${Errores}">ERROR. Haga click para ver el detalle</span>  
                            {{/if}}
                        </td>
                    </tr>
                {{/each}}
            </tbody>
        </table>
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="2">No se han encontrado resultados</td>
        </tr>
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
    <script src="/js/views/reportes/sicore.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            sicore.configFilters();
        });
    </script>
</asp:Content>


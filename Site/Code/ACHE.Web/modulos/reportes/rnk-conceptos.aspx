﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="rnk-conceptos.aspx.cs" Inherits="modulos_reportes_rnk_conceptos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Ranking de productos y servicios</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">Ranking de productos y servicios</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>
                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">

                                <div class="col-sm-8 col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-2 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-6 col-md-2 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>

                                        <div class="col-sm-6 col-md-3 form-group">
                                            <asp:DropDownList runat="server" ID="ddlTipo" CssClass="form-control">
                                                <asp:ListItem Value="">Productos, combos y servicios</asp:ListItem>
                                                <asp:ListItem Value="P">Productos</asp:ListItem>
                                                <asp:ListItem Value="S">Servicios</asp:ListItem>
                                                <asp:ListItem Value="C">Combos</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-2 form-group">
                                            <asp:DropDownList runat="server" ID="ddlEstado" CssClass="form-control">
                                                <asp:ListItem Text="" Value="">Activos e inactivos</asp:ListItem>
                                                <asp:ListItem Text="Activo" Value="A"></asp:ListItem>
                                                <asp:ListItem Text="Inactivo" Value="I"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-sm-6 col-md-2 form-group">
                                            <asp:DropDownList runat="server" ID="ddlRubros" CssClass="form-control" ClientIDMode="Static" data-placeholder="Seleccione un rubro">
                                                <asp:ListItem Text="" Value="">Selecciones un rubro</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6 col-md-2 form-group">
                                            <span id="selectSubRubros">
                                                <select id='ddlSubRubros' class="form-control">
                                                    <option value="">Seleccione un sub rubro</option>
                                                </select>
                                            </span>
                                        </div>

                                        <div class="col-sm-6 col-md-3 form-group">
                                            <a class="btn btn-black" onclick="resetearPagina();filtrar();" id="btnBuscar">Buscar</a>
                                            <a class="btn btn-default" onclick="resetearPagina();verTodos();">Ver todos</a>
<%--                                            <img src="../../images/loaders/loader1.gif" id="gifLoader" />--%>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </form>

                        <div class="col-sm-12">
                            <hr class="mt0" />
                        </div>
                        <div class="row">
                            <div class="pull-right">

                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="resetearExportacion();" download="RankingConceptos" style="display: none">Descargar</a>
                                    </div>
                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>
                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>

                    <div class="panel-body" id="divResultados">

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Código</th>
                                        <th>Concepto</th>
                                        <th>Rubro</th>
                                        <th>Sub Rubro</th>
                                        <th style="text-align: right">Cantidad</th>
                                        <th style="text-align: right">Total sin IVA</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <th>${TipoConcepto}</th>
            <td>${Valor2}</td>
            <td>${Valor1}</td>
            <td>${Rubro}</td>
            <td>${SubRubro}</td>

            <td style="text-align: right">${Cantidad}</td>
            <td style="text-align: right">${Total}</td>
        </tr>
        {{/each}}
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="7">No se han encontrado resultados</td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/rnk-conceptos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            configFilters();
            //filtrar();

        });
    </script>
</asp:Content>


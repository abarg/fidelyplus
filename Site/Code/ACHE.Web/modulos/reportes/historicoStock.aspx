﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="historicoStock.aspx.cs" Inherits="modulos_reportes_historicoStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Entradas y salidas de productos </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">Entradas y salidas de productos</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="alert alert-warning  mb15">
		            <b>Nueva visualización!</b> Hemos mejorado el reporte para que puedas ver el stock a una determinada fecha y te sea más simple ver los cambios sufridos.
	            </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                       <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>
                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-9">
                                    <div class="row">
                                       <div class="col-sm-6 col-md-3 form-group inventario">
                                            <asp:DropDownList runat="server" ID="ddlInventarios" CssClass="form-control">
                                            </asp:DropDownList>
                                       </div>
                                       <div class="col-sm-6 col-md-5 form-group">
                                            <select class="select2" data-placeholder="Seleccione un producto..." id="ddlConcepto">
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-md-2 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Desde" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-2 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Hasta" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-3 responsive-buttons md">
                                    <a class="btn btn-black" onclick="resetearPagina();filtrarBusqueda();" id="btnBuscar">Buscar</a>
                                    
                                </div>
                            </div>

                        </form>

                        <div class="col-sm-12"><hr class="mt0" /></div>
                        
                        <div class="row">
                            <div class="pull-right">
                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="resetearExportacion();" download="HistoricoStock" style="display: none">Descargar</a>
                                    </div>

                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Depósito</th>
                                        <th>Acción</th>
                                        <%--<th>Proveedor/Cliente</th>
                                        <th>Documento</th>
                                        <th>Comprobante</th>--%>
                                        <th style="text-align: right">Código</th>
                                        <th>Producto</th>
                                        <%--<th style="text-align: right">Precio Unit.</th>--%>
                                        <th style="text-align: right">Cant. modificada</th>
                                        <th style="text-align: right">Stock actualizado</th>
                                        <th>Observaciones</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>


    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}        
        <tr>           
            <td>${FechaStr}</td>
            <td>${Deposito}</td>
            <td><%--${Tipo}</td>--%>
                {{if Tipo=='Compra'}}
                    <a href="/modulos/compras/compradetalladase.aspx?ID=${IdFactura}">${Factura}</a>
                {{else}}
                    {{if Tipo == 'Venta' || Tipo == 'Nota de crédito'}}
                        <a href="/comprobantese.aspx?ID=${IdFactura}">${Factura}</a>
                    {{else}}
                        ${Tipo}
                    {{/if}}
                {{/if}}
            </td>
            <td style="text-align: right">${Codigo}</td>    
            <td>${Nombre}</td>
            <td style="text-align: right">${CantModificada}</td>  
            <td style="text-align: right">${StockActualizado}</td>      
            <td>${Observaciones}</td>
        </tr>
        {{/each}}
    </script>

    
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="8">No se han encontrado resultados</td>
        </tr>
    </script>

    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/historicoStock.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            configFilters();
        });
    </script>

</asp:Content>


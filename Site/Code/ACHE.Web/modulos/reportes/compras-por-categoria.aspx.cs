﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;

public partial class modulos_reportes_compras_por_categoria : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [WebMethod(true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Chart> obtenerCompras(string desde, string hasta, int incluirCot)
    {
        List<Chart> aux = null;
        List<Chart> list = new List<Chart>();

        string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fechaDesde = "";
            string fechaHasta = "";

            if (desde != string.Empty)
                fechaDesde = DateTime.Parse(desde).ToString(formato);
            if (hasta != string.Empty)
                fechaHasta = DateTime.Parse(hasta).ToString(formato) + " 23:59:59 pm";

            using (var dbContext = new ACHEEntities())
            {
                aux = dbContext.Database.SqlQuery<Chart>("exec Dashboard_ComprasPorCategoria '" + fechaDesde + "','" + fechaHasta + "', " + usu.IDUsuario + ", " + incluirCot, new object[] { }).ToList();
            }
            foreach (var item in aux)
            {
                if (list.Any(x => x.label.ToLower() == item.label.ToLower()))
                {
                    var itemaux = list.Where(x => x.label.ToLower() == item.label.ToLower()).FirstOrDefault();
                    itemaux.data += item.data;
                }
                else
                    list.Add(item);
            }
        }

        return list;
    }

    [WebMethod(true)]
    public static string getDetail(string fechaDesde, string fechaHasta, string Etiqueta, int incluirCot)
    {
        string formato = "dd/MM/yyyy";//ConfigurationManager.AppSettings["FormatoFechasSQL"];

        fechaDesde = DateTime.Parse(fechaDesde).ToString(formato) + " 00:00:00 am";

        fechaHasta = DateTime.Parse(fechaHasta).ToString(formato) + " 23:59:59 pm";

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var html = "";
            var compras = ObtenerComprasPorFecha(Convert.ToDateTime(fechaDesde), Convert.ToDateTime(fechaHasta), Etiqueta, incluirCot);

            if (compras.Any())
            {
                decimal total = 0;
                foreach (var detalle in compras)
                {
                    total += Convert.ToDecimal(detalle.Total);

                    html += "<tr>";
                    html += "<td class='bgRow'>" + detalle.RazonSocial + "</td>";
                    html += "<td class='bgRow'>" + detalle.Comprobante + "</td>";
                    html += "<td class='bgRow'>" + detalle.TipoComprobante + "</td>";
                    html += "<td class='bgRow'>" + detalle.Fecha + "</td>";
                    html += "<td class='bgRow' style='text-align: right'>" + Convert.ToDecimal(detalle.Total).ToMoneyFormat(2) + "</td>";
                    html += "</tr>";
                }

                html += "<tr>";
                html += "<td class='bgTotal' colspan='3'></td>";
                html += "<td class='bgTotal text-danger' style='text-align: right'>Total</td>";
                html += "<td class='bgTotal text-danger' style='text-align: right'>" + total.ToMoneyFormat(2) + "</td>";
                html += "</tr>";
            }
            else
                html += "<tr><td colspan='4'>No hay un detalle disponible</td></tr>";


            return html;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static List<RptIngresoEgresoViewModel> ObtenerComprasPorFecha(DateTime fechaDesde, DateTime fechaHasta, string Etiqueta, int incluirCot)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        List<RptIngresoEgresoViewModel> IngresosEgresos = new List<RptIngresoEgresoViewModel>();
        RptIngresoEgresoViewModel det;
        var listEgr = new List<Compras>();
        var listGastos = new List<BancosDetalle>();
        using (var dbContext = new ACHEEntities())
        {
            if (Etiqueta.ToLower() == "mov. bancarios")
            {
                listGastos = dbContext.BancosDetalle.Where(x => x.TipoMovimiento == "Egreso" && x.IDUsuario == usu.IDUsuario && x.Fecha >= fechaDesde && x.Fecha <= fechaHasta && !x.IDCategoria.HasValue).OrderBy(x => x.Fecha).ToList();//.Select(x => new RptIngresoEgresoViewModel {
                //        RazonSocial = x.Bancos.BancosBase.Nombre.ToUpper(),
                //        Comprobante = x.Concepto,
                //        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                //        Total = Convert.ToDecimal(x.Importe).ToMoneyFormat(2)
                //}).ToList();
                //IngresosEgresos.AddRange(listGastos);
                foreach (var item in listGastos)
                {
                    det = new RptIngresoEgresoViewModel();
                    det.RazonSocial = item.Bancos.BancosBase.Nombre.ToUpper();
                    det.Comprobante = item.Concepto;
                    det.Fecha = item.Fecha.ToString("dd/MM/yyyy");
                    det.Total = Convert.ToDecimal(item.Importe).ToMoneyFormat(2);
                    det.TipoComprobante = "Mov. bancario";
                    IngresosEgresos.Add(det);
                }
            }
            else
            {
                if (Etiqueta.ToLower() != "sin asignar")
                {
                    listEgr = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.FechaEmision >= fechaDesde && x.FechaEmision <= fechaHasta && x.Categorias.Nombre.ToLower() == Etiqueta.ToLower()).OrderBy(x => x.Fecha).ToList();
                    listGastos = dbContext.BancosDetalle.Where(x => x.TipoMovimiento == "Egreso" && x.IDUsuario == usu.IDUsuario && x.Fecha >= fechaDesde && x.Fecha <= fechaHasta && x.Categorias.Nombre.ToLower() == Etiqueta.ToLower()).OrderBy(x => x.Fecha).ToList();
                }
                else
                    listEgr = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.FechaEmision >= fechaDesde && x.FechaEmision <= fechaHasta && !x.IDCategoria.HasValue).OrderBy(x => x.Fecha).ToList();

                if (incluirCot == 0)
                    listEgr = listEgr.Where(x => x.Tipo != "COT").ToList();

                foreach (var item in listEgr)
                {
                    det = new RptIngresoEgresoViewModel();
                    det.RazonSocial = item.Personas.RazonSocial;
                    det.Comprobante = item.Tipo + " " + item.NroFactura;
                    det.Fecha = item.FechaEmision.ToString("dd/MM/yyyy");
                    if (item.Tipo == "NCA" || item.Tipo == "NCB" || item.Tipo == "NCC" || item.Tipo == "NCE")
                        det.Total = ((item.Total ?? 0) * -1).ToMoneyFormat(2);
                    else
                        det.Total = (item.Total ?? 0).ToMoneyFormat(2);
                    det.TipoComprobante = "Compras";
                    //((item.Total ?? 0) + (item.TotalImpuestos ?? 0)).ToMoneyFormat(2);
                    IngresosEgresos.Add(det);
                }

                foreach (var item in listGastos)
                {
                    det = new RptIngresoEgresoViewModel();
                    det.RazonSocial = item.Bancos.BancosBase.Nombre.ToUpper();
                    det.Comprobante = item.Concepto;
                    det.Fecha = item.Fecha.ToString("dd/MM/yyyy");
                    det.TipoComprobante = "Mov. bancario";
                    det.Total = Convert.ToDecimal(item.Importe).ToMoneyFormat(2);
                    IngresosEgresos.Add(det);

                }
            }

        }
        return IngresosEgresos.OrderBy(x => x.Fecha).ToList();
    }

    [WebMethod(true)]
    public static string exportDetalle(string fechaDesde, string fechaHasta, string Etiqueta, int incluirCot)
    {
        string formato = "dd/MM/yyyy"; //ConfigurationManager.AppSettings["FormatoFechasSQL"];

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {

            fechaDesde = DateTime.Parse(fechaDesde).ToString(formato) + " 00:00:00 am";

            fechaHasta = DateTime.Parse(fechaHasta).ToString(formato) + " 23:59:59 pm";

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            string fileName = "Detalle" + Etiqueta + "_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                var resultados = ObtenerComprasPorFecha(Convert.ToDateTime(fechaDesde), Convert.ToDateTime(fechaHasta), Etiqueta, incluirCot);
                DataTable dt = new DataTable();
                dt = resultados.Select(x => new
                {
                    RazonSocial = x.RazonSocial,
                    Comprobante = x.Comprobante,
                    Fecha = x.Fecha,
                    Tipo = x.TipoComprobante,
                    Total = Convert.ToDecimal(x.Total)
                }).ToList().ToDataTable();

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

}
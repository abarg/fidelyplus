﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using System.Data.Entity;

public partial class modulos_reportes_iva_ventas : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptIvaVentasViewModel getResults(int idPersona, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];


                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptIvaVentas.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    page--;
                    ResultadosRptIvaVentasViewModel resultado = new ResultadosRptIvaVentasViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    //TODO: MEJORAR ESTO
                    var auxList = results.ToList();
                    decimal TotalIva = 0;
                    decimal TotalImporte = 0;
                    decimal TotalTotal = 0;
                    decimal TotalIVA27 = 0;
                    decimal TotalIVA21 = 0;
                    decimal TotalIVA10 = 0;
                    decimal TotalIVA5 = 0;
                    decimal TotalIVA2 = 0;
                    decimal TotalIIBB = 0;
                    decimal TotalNoGrav = 0;
                    decimal TotalServicios = 0;
                    decimal TotalProductos = 0;
                    decimal TotalRI = 0;
                    decimal TotalMO = 0;
                    decimal TotalEX = 0;
                    decimal TotalCF = 0;
                    decimal TotalOtrosImp = 0;

                    foreach (var x in auxList)
                    {
                        TotalImporte += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.ImporteBruto - x.ImporteNoGrabado) : -1 * Convert.ToDecimal(x.ImporteBruto - x.ImporteNoGrabado);
                        TotalNoGrav += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.ImporteNoGrabado) : -1 * Convert.ToDecimal(x.ImporteNoGrabado);
                        TotalIVA2 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA2) : -1 * Convert.ToDecimal(x.IVA2);
                        TotalIVA10 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA10) : -1 * Convert.ToDecimal(x.IVA10);
                        TotalIVA5 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA5) : -1 * Convert.ToDecimal(x.IVA5);
                        TotalIVA21 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA21) : -1 * Convert.ToDecimal(x.IVA21);
                        TotalIVA27 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA27) : -1 * Convert.ToDecimal(x.IVA27);
                        TotalIva += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Iva : -1 * x.Iva;
                        TotalTotal += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto;
                        TotalServicios += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Servicios : -1 * x.Servicios;
                        TotalProductos += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Productos : -1 * x.Productos;
                        TotalOtrosImp += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.OtrosImp : -1 * x.OtrosImp;
                        TotalIIBB += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.PercepcionIIBB : -1 * x.PercepcionIIBB;

                        TotalRI += x.CondicionIVA == "RI" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                        TotalMO += x.CondicionIVA == "MO" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                        TotalEX += x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                        TotalCF += x.CondicionIVA == "CF" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                    }

                    resultado.TotalIva = TotalIva.ToMoneyFormat(2);
                    resultado.TotalNoGrav = TotalNoGrav.ToMoneyFormat(2);
                    resultado.TotalImporte = TotalImporte.ToMoneyFormat(2);
                    resultado.TotalTotal = TotalTotal.ToMoneyFormat(2);
                    resultado.TotalIVA27 = TotalIVA27.ToMoneyFormat(2);
                    resultado.TotalIVA21 = TotalIVA21.ToMoneyFormat(2);
                    resultado.TotalIVA10 = TotalIVA10.ToMoneyFormat(2);
                    resultado.TotalIVA5 = TotalIVA5.ToMoneyFormat(2);
                    resultado.TotalIVA2 = TotalIVA2.ToMoneyFormat(2);
                    resultado.TotalIIBB = TotalIIBB.ToMoneyFormat(2);
                    resultado.TotalOtrosImp = TotalOtrosImp.ToMoneyFormat(2);
                    resultado.TotalServicios = TotalServicios.ToMoneyFormat(2);
                    resultado.TotalProductos = TotalProductos.ToMoneyFormat(2);

                    resultado.TotalRI = TotalRI.ToMoneyFormat(2);
                    resultado.TotalMO = TotalMO.ToMoneyFormat(2);
                    resultado.TotalEX = TotalEX.ToMoneyFormat(2);
                    resultado.TotalCF = TotalCF.ToMoneyFormat(2);

                    var list = auxList.OrderBy(x => x.Fecha).ThenBy(x => x.PuntoVenta).ThenBy(x => x.Factura).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new RptIvaVentasViewModel()
                        {
                            Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                            RazonSocial = x.RazonSocial,
                            Cuit = x.CUIT,
                            NroFactura = x.Tipo + " " + x.PuntoVenta.ToString("#0000") + "-" + x.Factura.ToString("#00000000"),
                            CondicionIVA = x.CondicionIVA,
                            NoGrav = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.ImporteNoGrabado).ToMoneyFormat(2) : "-" + Convert.ToDecimal(x.ImporteNoGrabado).ToMoneyFormat(2),
                            Importe = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.ImporteBruto - x.ImporteNoGrabado).ToMoneyFormat(2) : "-" + Convert.ToDecimal(x.ImporteBruto - x.ImporteNoGrabado).ToMoneyFormat(2),
                            IVA2 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA2).ToMoneyFormat(2) : "-" + Convert.ToDecimal(x.IVA2).ToMoneyFormat(2),
                            IVA10 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA10).ToMoneyFormat(2) : "-" + Convert.ToDecimal(x.IVA10).ToMoneyFormat(2),
                            IVA5 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA5).ToMoneyFormat(2) : "-" + Convert.ToDecimal(x.IVA5).ToMoneyFormat(2),
                            IVA21 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA21).ToMoneyFormat(2) : "-" + Convert.ToDecimal(x.IVA21).ToMoneyFormat(2),
                            IVA27 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA27).ToMoneyFormat(2) : "-" + Convert.ToDecimal(x.IVA27).ToMoneyFormat(2),
                            Iva = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Iva.ToMoneyFormat(2) : "-" + x.Iva.ToMoneyFormat(2),
                            IIBB = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.PercepcionIIBB.ToMoneyFormat(2) : "-" + x.PercepcionIIBB.ToMoneyFormat(2),
                            OtrosImp = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.OtrosImp.ToMoneyFormat(2) : "-" + x.OtrosImp.ToMoneyFormat(2),
                            Total = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto.ToMoneyFormat(2) : "-" + x.ImporteNeto.ToMoneyFormat(2),
                            Servicios = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Servicios.ToMoneyFormat(2) : "-" + x.Servicios.ToMoneyFormat(2),
                            Productos = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Productos.ToMoneyFormat(2) : "-" + x.Productos.ToMoneyFormat(2)

                        });

                    /*foreach (var det in list)
                    {
                        var servicios = dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == det.IDComprobante && x.IDConcepto.HasValue && x.Conceptos.Tipo == "S").AsQueryable();
                        det.Servicios = servicios.Sum(x => x.Iva).ToMoneyFormat();

                        var productos = dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == det.IDComprobante && x.IDConcepto.HasValue && x.Conceptos.Tipo == "P").AsQueryable();
                        det.Productos = productos.Sum(x => x.Iva).ToMoneyFormat();
                    }*/

                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export(int idPersona, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "IvaVentas_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptIvaVentas.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    dt = results.OrderBy(x => x.Fecha).ThenBy(x => x.PuntoVenta).ThenBy(x => x.Factura).ToList().Select(x => new
                    {
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        RazonSocial = x.RazonSocial,
                        Cuit = x.CUIT,
                        Provincia = x.Provincia,
                        Ciudad = x.Nombre,
                        CondicionIVA = x.CondicionIVA,
                        NroFactura = x.Tipo + " " + x.PuntoVenta.ToString("#0000") + "-" + x.Factura.ToString("#00000000"),

                        ImporteNetoGravado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? (x.ImporteBruto - x.ImporteNoGrabado) : ((x.ImporteBruto - x.ImporteNoGrabado) * -1),
                        ImporteNoGrav = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNoGrabado : (x.ImporteNoGrabado * -1),
                        IVA2 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA2) : -Convert.ToDecimal(x.IVA2),
                        IVA10 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA10) : -Convert.ToDecimal(x.IVA10),
                        IVA5 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA5) : -Convert.ToDecimal(x.IVA5),
                        IVA21 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA21) : -Convert.ToDecimal(x.IVA21),
                        IVA27 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA27) : -Convert.ToDecimal(x.IVA27),
                        IIBB = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.PercepcionIIBB) : -Convert.ToDecimal(x.PercepcionIIBB),
                        OtrosImp = (x.Tipo!="NCA" && x.Tipo!="NCB" && x.Tipo!="NCC") ? Convert.ToDecimal(x.OtrosImp): -Convert.ToDecimal(x.OtrosImp),
                        TotalIVA = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.Iva) : -Convert.ToDecimal(x.Iva),
                        TotalFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -x.ImporteNeto,
                        Servicios = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Servicios : -x.Servicios,
                        Productos = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Productos : -x.Productos,
                        Observaciones = x.Observaciones
                    }).ToList().ToDataTable();

                    /*foreach (var det in aux)
                    {
                        var servicios = dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == det.IDComprobante && x.IDConcepto.HasValue && x.Conceptos.Tipo == "S").AsQueryable();
                        det.Servicios = servicios.Sum(x => x.Iva);

                        var productos = dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == det.IDComprobante && x.IDConcepto.HasValue && x.Conceptos.Tipo == "P").AsQueryable();
                        det.Productos = productos.Sum(x => x.Iva);
                    }
                    
                    dt = aux.Select(x => new
                    {
                        Fecha = x.Fecha,
                        RazonSocial = x.RazonSocial,
                        Cuit = x.Cuit,
                        CondicionIVA = x.CondicionIVA,
                        NroFactura = x.NroFactura,
                        ImporteNetoGravado = x.ImporteNetoGravado,
                        IVA2 = x.IVA2,
                        IVA10 = x.IVA10,
                        IVA5 = x.IVA5,
                        IVA21 = x.IVA21,
                        IVA27 = x.IVA27,
                        TotalIVA = x.TotalIVA,
                        TotalFacturado = x.TotalFacturado,
                        Servicios = x.Servicios,
                        Productos = x.Productos
                    }).ToList().ToDataTable();*/
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string exportPDF(int idPersona, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "IvaVentas_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                decimal TotalRI = 0;
                decimal TotalMO = 0;
                decimal TotalEX = 0;
                decimal TotalCF = 0;
                decimal TotalIva = 0;
                decimal TotalImporte = 0;
                decimal TotalTotal = 0;
                decimal TotalIVA27 = 0;
                decimal TotalIVA21 = 0;
                decimal TotalIVA10 = 0;
                decimal TotalIVA5 = 0;
                decimal TotalIVA2 = 0;
                decimal TotalIIBB = 0;
                decimal TotalOtrosImp = 0;
                decimal TotalNoGrav = 0;
                decimal TotalServicios = 0;
                decimal TotalProductos = 0;
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptIvaVentas.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }
                    var auxList = results.ToList();

                    foreach (var x in auxList)
                    {
                        TotalImporte += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.ImporteBruto - x.ImporteNoGrabado) : -1 * Convert.ToDecimal(x.ImporteBruto - x.ImporteNoGrabado);
                        TotalNoGrav += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ?Convert.ToDecimal(x.ImporteNoGrabado) : -1 * Convert.ToDecimal(x.ImporteNoGrabado);
                        TotalIVA2 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA2) : -1 * Convert.ToDecimal(x.IVA2);
                        TotalIVA10 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA10) : -1 * Convert.ToDecimal(x.IVA10);
                        TotalIVA5 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA5) : -1 * Convert.ToDecimal(x.IVA5);
                        TotalIVA21 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA21) : -1 * Convert.ToDecimal(x.IVA21);
                        TotalIVA27 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA27) : -1 * Convert.ToDecimal(x.IVA27);
                        TotalIva += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Iva : -1 * x.Iva;
                        TotalTotal += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto;
                        TotalOtrosImp += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.OtrosImp : -1 * x.OtrosImp;
                        TotalServicios += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Servicios : -1 * x.Servicios;
                        TotalProductos += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Productos : -1 * x.Productos;
                        TotalIIBB += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.PercepcionIIBB : -1 * x.PercepcionIIBB;
                        

                        TotalRI += x.CondicionIVA == "RI" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                        TotalMO += x.CondicionIVA == "MO" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                        TotalEX += x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                        TotalCF += x.CondicionIVA == "CF" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto : -1 * x.ImporteNeto) : 0;
                    }
                    /*
                    resultado.TotalIva = TotalIva.ToMoneyFormat(2);
                    resultado.TotalNoGrav = TotalNoGrav.ToMoneyFormat(2);
                    resultado.TotalImporte = TotalImporte.ToMoneyFormat(2);
                    resultado.TotalTotal = TotalTotal.ToMoneyFormat(2);
                    resultado.TotalIVA27 = TotalIVA27.ToMoneyFormat(2);
                    resultado.TotalIVA21 = TotalIVA21.ToMoneyFormat(2);
                    resultado.TotalIVA10 = TotalIVA10.ToMoneyFormat(2);
                    resultado.TotalIVA5 = TotalIVA5.ToMoneyFormat(2);
                    resultado.TotalIVA2 = TotalIVA2.ToMoneyFormat(2);
                    resultado.TotalIIBB = TotalIIBB.ToMoneyFormat(2);
                    resultado.TotalServicios = TotalServicios.ToMoneyFormat(2);
                    resultado.TotalProductos = TotalProductos.ToMoneyFormat(2);

                 
                    */
                    dt = auxList.OrderBy(x => x.Fecha).ThenBy(x => x.PuntoVenta).ThenBy(x => x.Factura).ToList().Select(x => new
                    {
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        RazonSocial = x.RazonSocial,
                        Cuit = x.CUIT,
                        Provincia = x.Provincia,
                        CondicionIVA = x.CondicionIVA,
                        NroFactura = x.Tipo + " " + x.PuntoVenta.ToString("#0000") + "-" + x.Factura.ToString("#00000000"),
                        ImporteNetoGravado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.ImporteBruto - x.ImporteNoGrabado).ToMoneyFormat(2) : Convert.ToDecimal((x.ImporteBruto - x.ImporteNoGrabado) * -1).ToMoneyFormat(2),
                        ImporteNoGrav = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.ImporteNoGrabado).ToMoneyFormat(2) : Convert.ToDecimal(x.ImporteNoGrabado * -1).ToMoneyFormat(2),
                        IVA2 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA2).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.IVA2)).ToMoneyFormat(2),
                        IVA10 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA10).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.IVA10)).ToMoneyFormat(2),
                        IVA5 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA5).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.IVA5)).ToMoneyFormat(2),
                        IVA21 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA21).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.IVA21)).ToMoneyFormat(2),
                        IVA27 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.IVA27).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.IVA27)).ToMoneyFormat(2),
                        IIBB = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.PercepcionIIBB).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.PercepcionIIBB)).ToMoneyFormat(2),
                        OtrosImp = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.OtrosImp).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.OtrosImp)).ToMoneyFormat(2),
                        TotalIVA = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? Convert.ToDecimal(x.Iva).ToMoneyFormat(2) : (-1 * Convert.ToDecimal(x.Iva)).ToMoneyFormat(2),
                        TotalFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.ImporteNeto.ToMoneyFormat(2) : (-1 * x.ImporteNeto).ToMoneyFormat(2),
                        //Servicios = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Servicios.ToMoneyFormat(2) : (-1 * x.Servicios).ToMoneyFormat(2),
                        //Productos = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Productos.ToMoneyFormat(2) : (-1 * x.Productos).ToMoneyFormat(2)

                    }).ToList().ToDataTable();


                }



                if (dt.Rows.Count > 0)
                {
                    ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
                    ///razon social , cuit y periodo de fechas 
                    List<DetalleCabecera> cabecera = new List<DetalleCabecera>();
                    DetalleCabecera c = new DetalleCabecera();
                    c.Titulo = "Razón Social";
                    c.Valor = usu.RazonSocial;
                    cabecera.Add(c);

                    DetalleCabecera c1 = new DetalleCabecera();
                    c1.Titulo = "CUIT";
                    c1.Valor = usu.CUIT;
                    cabecera.Add(c1);
                    DetalleCabecera c2 = new DetalleCabecera();

                    c2.Titulo = "Período";
                    c2.Valor = fechaDesde + " - " + fechaHasta;
                    cabecera.Add(c2);
                    contenido.Cabecera = cabecera;

                    contenido.TituloReporte = "LIBRO IVA VENTAS";
                    List<Total> totales = new List<Total>();
                    Total tot1 = new Total();
                    tot1.Importe = TotalRI.ToMoneyFormat(2);
                    tot1.Nombre = "Responsables Incriptos";
                    totales.Add(tot1);
                    Total tot2 = new Total();
                    tot2.Importe = TotalMO.ToMoneyFormat(2);
                    tot2.Nombre = "Monotributistass";
                    totales.Add(tot2);
                    Total tot3 = new Total();
                    tot3.Importe = TotalCF.ToMoneyFormat(2);
                    tot3.Nombre = "Consumidores Finales";
                    totales.Add(tot3);
                    Total tot4 = new Total();
                    tot4.Importe = TotalEX.ToMoneyFormat(2);
                    tot4.Nombre = "Exentos";
                    totales.Add(tot4);
                    contenido.Totales = totales;
                    List<string> ultimaFilaTotales = new List<string>();
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");

                    ultimaFilaTotales.Add(TotalImporte.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalNoGrav.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA2.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA5.ToMoneyFormat(2));

                    ultimaFilaTotales.Add(TotalIVA10.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA21.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA27.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIIBB.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalOtrosImp.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIva.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalTotal.ToMoneyFormat(2));

                    //ultimaFilaTotales.Add(TotalServicios.ToMoneyFormat(2));
                    //ultimaFilaTotales.Add(TotalProductos.ToMoneyFormat(2));

                    ultimaFilaTotales.Add("");
                    contenido.UltimaFila = ultimaFilaTotales;
                    CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, false, false);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using System.Collections.Generic;
using ClosedXML.Excel;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Common;
using System.Net.Mail;

public partial class modulos_reportes_saldosProveedores : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            //txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);
            //lnkCredility.HRef = "https://www.credility.com/landing-contabilium?email=" + CurrentUser.Email + "&nombre=" + CurrentUser.RazonSocial + "&cuit=" + CurrentUser.CUIT + "&referralid=Contabilium&telefono=" + CurrentUser.Telefono + "&tipo=" + CurrentUser.CondicionIVA;
            //lnkCredility.HRef = "http://www.vaxtcapital.com/contabilium?email=" + CurrentUser.Email + "&nombre=" + CurrentUser.RazonSocial + "&cuit=" + CurrentUser.CUIT + "&referralid=Contabilium&telefono=" + CurrentUser.Telefono + "&tipo=" + CurrentUser.CondicionIVA;

            //TODO: pasar a businnes
            using (var dbContext = new ACHEEntities())
            {
                var cant = dbContext.PagosDetalle.Where(x => !x.IDCompra.HasValue && x.Pagos.IDUsuario == CurrentUser.IDUsuario).Select(x => x.IDPago).Distinct().Count();
                if (cant > 0)
                {
                    divAlert.Visible = true;
                    litMsj.Text = "Usted tiene <a style='color: #8a6d3b;font-weight: bold;text-decoration: underline;' href='javascript:Common.verPagosACuenta();'>" + cant + " pagos(s)</a> a cuenta pendiente(s) de imputación";
                }
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptSaldosViewModel getResults(int idPersona, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var aux = dbContext.Database.SqlQuery<RptSaldosProvViewModel>("exec Rpt_SaldosProveedores " + usu.IDUsuario + "," + idPersona, new object[] { }).ToList();

                    //todo: fix provisorio hasta corregir decimales
                    //aux = aux.Where(x => (x.SaldoInicial + x.Saldo + x.CotNC - x.CobranzasACuenta + x.NCSinAplicar)).ToList();

                    page--;
                    ResultadosRptSaldosViewModel resultado = new ResultadosRptSaldosViewModel();
                    //resultado.Items = aux.OrderByDescending(x => x.RazonSocial).ToList();

                    resultado.TotalPage = ((aux.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = aux.Count();

                    resultado.TotalGeneral = aux.Sum(x => (x.SaldoInicial + x.Saldo + x.CotNC - x.PagosACuenta + x.NCSinAplicar)).ToMoneyFormat(2);

                    var list = aux.OrderBy(x => x.RazonSocial).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new RptSaldosViewModel
                        {
                            IDPersona = x.IDPersona,
                            RazonSocial = x.RazonSocial,
                            NroDocumento = (x.NroDocumento == "") ? "-" : x.TipoDocumento + " " + x.NroDocumento,
                            CondicionIVA = x.CondicionIVA,
                            SaldoDesc = (x.SaldoInicial + x.Saldo + x.CotNC - x.PagosACuenta + x.NCSinAplicar).ToMoneyFormat(2)
                        });

                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Saldos_Prov_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.Database.SqlQuery<RptSaldosProvViewModel>("exec Rpt_SaldosProveedores " + usu.IDUsuario + "," + idPersona, new object[] { }).ToList();

                    //if (idPersona > 0)
                    //    results = results.Where(x => x.IDPersona == idPersona);

                    dt = results.OrderBy(x => x.RazonSocial).ToList().Select(x => new
                    {
                        RazonSocial = x.RazonSocial,
                        TipoDocumento = x.TipoDocumento,
                        NroDocumento = x.NroDocumento,
                        CondicionIVA = x.CondicionIVA,
                        Saldo = Math.Round((x.SaldoInicial + x.Saldo + x.CotNC - x.PagosACuenta + x.NCSinAplicar),2)
                    }).ToList().ToDataTable();

                }
                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
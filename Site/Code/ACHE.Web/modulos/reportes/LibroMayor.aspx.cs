﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using System.Data.Entity;


public partial class modulos_reportes_LibroMayor : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);

            using (var dbContext = new ACHEEntities())
            {
                var listaPlanDeCuenta = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDPadre.HasValue).OrderBy(x => x.Codigo).ToList();
                ddlCuenta.Items.Add(new ListItem("", ""));

                foreach (var item in listaPlanDeCuenta)
                    ddlCuenta.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosLibroMayorViewModel getResults(int idCuenta, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idCuenta > 0)
                    {
                        results = results.Where(x => x.IDPlanDeCuenta == idCuenta);
                    }
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    page--;
                    ResultadosLibroMayorViewModel resultado = new ResultadosLibroMayorViewModel();
                    resultado.TotalPage = ((results.GroupBy(x => x.Codigo).Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.GroupBy(x => x.Codigo).Count();

                    var aux = results.GroupBy(x => x.Codigo).ToList().OrderBy(x => x.FirstOrDefault().Codigo).Skip(page * pageSize).Take(pageSize).ToList();

                    //resultado.Asientos = results.GroupBy(x => x.Codigo).OrderBy(x => x.FirstOrDefault().Codigo).Skip(page * pageSize).Take(pageSize).ToList()
                    resultado.Asientos = aux.Select(x => new CuentasViewModel()
                       {
                           nroCuenta = x.FirstOrDefault().Codigo,
                           NombreCuenta = x.FirstOrDefault().Nombre,
                           Items = DetalleComprobantesMayor(x.FirstOrDefault().Codigo, results.ToList()),
                           TotalDebe = (x.Sum(y => y.Debe)).ToMoneyFormat(2),
                           TotalHaber = (x.Sum(y => y.Haber)).ToMoneyFormat(2),
                           Saldo = (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToMoneyFormat(2)
                       }).ToList();

                    resultado.TotalDebe = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalDebe)).ToMoneyFormat(2);
                    resultado.TotalHaber = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalHaber)).ToMoneyFormat(2);
                    resultado.TotalSaldo = resultado.Asientos.Sum(x => Convert.ToDecimal(x.Saldo)).ToMoneyFormat(2);
                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    public class DetalleComprobantesMayorAuxViewModel
    {
        public string Fecha { get; set; }
        public string Leyenda { get; set; }
        //public string DebeString { get { return this.Debe.ToMoneyFormat(4); } }
        //public string HaberString { get { return this.Haber.ToMoneyFormat(4); } }
        //public string SaldoString { get { return this.Saldo.ToMoneyFormat(4); } }

        public decimal Debe { get; set; }
        public decimal Haber { get; set; }
        public decimal Saldo { get; set; }
        public decimal Total { get; set; }
    }

    private static List<DetalleComprobantesMayorViewModel> DetalleComprobantesMayor(string Codigo, List<rptImpositivoLibroDiario> results)
    {
        var auxList = results.Where(x => x.Codigo == Codigo).OrderBy(x => x.Fecha).Select(x => new DetalleComprobantesMayorAuxViewModel()
        {
            Fecha = x.Fecha.ToString("dd/MM/yyyy"),
            Leyenda = x.Leyenda + (x.RazonSocial != "" ? " (" + x.RazonSocial + ")" : ""),
            Debe = x.Debe,//.ToMoneyFormat(2),
            Haber = x.Haber,//.ToMoneyFormat(2),
            Saldo = decimal.Parse("0"),
            Total = decimal.Parse("0")
            //Saldo = (x.Debe - x.Haber).ToMoneyFormat(2)
        }).ToList();

        List<DetalleComprobantesMayorViewModel> list = new List<DetalleComprobantesMayorViewModel>();

        //decimal total = 0;
        //for (int i = 0; i < lista.Count; i++)
        //{
        //    total += Convert.ToDecimal(lista[i].Debe) - Convert.ToDecimal(lista[i].Haber);
        //    lista[i].Saldo = total.ToMoneyFormat(4);
        //}
        //return lista;


        decimal total = 0;
        for (int i = 0; i < auxList.Count; i++)
        {
            total += auxList[i].Debe - auxList[i].Haber;
            auxList[i].Saldo = total;

            list.Add(new DetalleComprobantesMayorViewModel()
            {
                Fecha = auxList[i].Fecha,
                Leyenda = auxList[i].Leyenda,
                Debe = auxList[i].Debe.ToMoneyFormat(2),
                Haber = auxList[i].Haber.ToMoneyFormat(2),
                Saldo = auxList[i].Saldo.ToMoneyFormat(2),
                //Saldo = (x.Debe - x.Haber).ToMoneyFormat(2)
            });

            //total += Convert.ToDecimal(lista[i].Debe) - Convert.ToDecimal(lista[i].Haber);
            //auxList[i].Saldo = auxList[i].Debe - auxList[i].Haber;
            //lista[i].Saldo = total.ToMoneyFormat(2);
        }
        return list;
    }

    [WebMethod(true)]
    public static string export(int idCuenta, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "LibroMayor_" + usu.IDUsuario + "_"; ;
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idCuenta > 0)
                    {
                        results = results.Where(x => x.IDPlanDeCuenta == idCuenta);
                    }
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    //var listaAux = results.OrderBy(x => x.Codigo).ToList().Select(x => new LibroMayorExport()
                    //{
                    //    Codigo = x.Codigo,
                    //    Nombre = x.Nombre,
                    //    Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                    //    Leyenda = x.Leyenda,
                    //    Debe = x.Debe,
                    //    Haber = x.Haber,
                    //}).ToList();

                    //decimal total = 0;
                    //for (int i = 0; i < listaAux.Count; i++)
                    //{
                    //    total += Convert.ToDecimal(listaAux[i].Debe) - Convert.ToDecimal(listaAux[i].Haber);
                    //    listaAux[i].Saldo = total;
                    //}


                    var listaCuentas = results.GroupBy(x => x.Codigo).ToList().OrderBy(x => x.FirstOrDefault().Codigo).ToList()
                       .Select(x => new CuentasViewModel()
                       {
                           nroCuenta = x.FirstOrDefault().Codigo,
                           NombreCuenta = x.FirstOrDefault().Nombre,
                           Items = DetalleComprobantesMayor(x.FirstOrDefault().Codigo, results.ToList()),
                           TotalDebe = (x.Sum(y => y.Debe)).ToMoneyFormat(2),
                           TotalHaber = (x.Sum(y => y.Haber)).ToMoneyFormat(2),
                           Saldo = (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToMoneyFormat(2)
                       }).ToList();


                    List<LibroMayorExport> listaFinal = new List<LibroMayorExport>();
                    LibroMayorExport asiento;
                    foreach (var item in listaCuentas)
                    {
                        asiento = new LibroMayorExport();
                        asiento.Codigo = item.nroCuenta;
                        asiento.Nombre = item.NombreCuenta;
                        listaFinal.Add(asiento);
                        foreach (var itemC in item.Items)
                        {
                            asiento = new LibroMayorExport();
                            asiento.Fecha = itemC.Fecha;
                            asiento.Leyenda = itemC.Leyenda;
                            asiento.Debe = Convert.ToDecimal(itemC.Debe).ToMoneyFormat(2);
                            asiento.Haber = Convert.ToDecimal(itemC.Haber).ToMoneyFormat(2);
                            asiento.Saldo = Convert.ToDecimal(itemC.Saldo).ToMoneyFormat(2);
                            listaFinal.Add(asiento);
                        }
                    }

                    dt = listaFinal.ToList().Select(x => new LibroMayorExport()
                    {
                        Codigo = x.Codigo,
                        Nombre = x.Nombre,
                        Fecha = x.Fecha,
                        Leyenda = x.Leyenda,
                        Debe = x.Debe,
                        Haber = x.Haber,
                        Saldo = x.Saldo,
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string exportPDF(int idCuenta, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "LibroMayor_" + usu.IDUsuario + "_"; ;
            string path = "~/tmp/";
            try
            {
                string TotalDebito = "0";
                string TotalCredito = "0";
                string TotalSaldo = "0";


                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idCuenta > 0)
                    {
                        results = results.Where(x => x.IDPlanDeCuenta == idCuenta);
                    }
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    //var listaAux = results.OrderBy(x => x.Codigo).ToList().Select(x => new LibroMayorExport()
                    //{
                    //    Codigo = x.Codigo,
                    //    Nombre = x.Nombre,
                    //    Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                    //    Leyenda = x.Leyenda,
                    //    Debe = x.Debe,
                    //    Haber = x.Haber,
                    //}).ToList();

                    //decimal total = 0;
                    //for (int i = 0; i < listaAux.Count; i++)
                    //{
                    //    total += Convert.ToDecimal(listaAux[i].Debe) - Convert.ToDecimal(listaAux[i].Haber);
                    //    listaAux[i].Saldo = total;
                    //}


                    var listaCuentas = results.GroupBy(x => x.Codigo).ToList().OrderBy(x => x.FirstOrDefault().Codigo).ToList()
                       .Select(x => new CuentasViewModel()
                       {
                           nroCuenta = x.FirstOrDefault().Codigo,
                           NombreCuenta = x.FirstOrDefault().Nombre,
                           Items = DetalleComprobantesMayor(x.FirstOrDefault().Codigo, results.ToList()),
                           TotalDebe = (x.Sum(y => y.Debe)).ToMoneyFormat(2),
                           TotalHaber = (x.Sum(y => y.Haber)).ToMoneyFormat(2),
                           Saldo = (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToMoneyFormat(2)
                       }).ToList();


                    List<LibroMayorExport> listaFinal = new List<LibroMayorExport>();
                    LibroMayorExport asiento;
                    foreach (var item in listaCuentas)
                    {
                        asiento = new LibroMayorExport();
                        asiento.Codigo = item.nroCuenta;
                        asiento.Nombre = item.NombreCuenta;
                        listaFinal.Add(asiento);
                        foreach (var itemC in item.Items)
                        {
                            asiento = new LibroMayorExport();
                            asiento.Fecha = itemC.Fecha;
                            asiento.Leyenda = itemC.Leyenda;
                            asiento.Debe = Convert.ToDecimal(itemC.Debe).ToMoneyFormat(2);
                            asiento.Haber = Convert.ToDecimal(itemC.Haber).ToMoneyFormat(2);
                            asiento.Saldo = Convert.ToDecimal(itemC.Saldo).ToMoneyFormat(2);
                            listaFinal.Add(asiento);
                        }
                    }

                    dt = listaFinal.ToList().Select(x => new LibroMayorExport()
                    {
                        Codigo = x.Codigo,
                        Nombre = x.Nombre,
                        Fecha = x.Fecha,
                        Leyenda = x.Leyenda,
                        Debe = x.Debe,
                        Haber = x.Haber,
                        Saldo = x.Saldo,
                    }).ToList().ToDataTable();

                    TotalDebito = listaCuentas.Sum(x => Convert.ToDecimal(x.TotalDebe)).ToMoneyFormat(2);
                    TotalCredito = listaCuentas.Sum(x => Convert.ToDecimal(x.TotalHaber)).ToMoneyFormat(2);
                    TotalSaldo = listaCuentas.Sum(x => Convert.ToDecimal(x.Saldo)).ToMoneyFormat(2);
                }



                if (dt.Rows.Count > 0)
                {
                    ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
                    ///razon social , cuit y periodo de fechas 
                    List<DetalleCabecera> cabecera = new List<DetalleCabecera>();
                    DetalleCabecera c = new DetalleCabecera();
                    c.Titulo = "Razón Social";
                    c.Valor = usu.RazonSocial;
                    cabecera.Add(c);

                    DetalleCabecera c1 = new DetalleCabecera();
                    c1.Titulo = "CUIT";
                    c1.Valor = usu.CUIT;
                    cabecera.Add(c1);
                    DetalleCabecera c2 = new DetalleCabecera();

                    c2.Titulo = "Período";
                    c2.Valor = fechaDesde + " - " + fechaHasta;
                    cabecera.Add(c2);
                    contenido.Cabecera = cabecera;

                    contenido.TituloReporte = "LIBRO MAYOR";
                    List<string> ultimaFilaTotales = new List<string>();
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");

                    ultimaFilaTotales.Add("Totales");

                    ultimaFilaTotales.Add(TotalDebito);
                    ultimaFilaTotales.Add(TotalCredito);
                    ultimaFilaTotales.Add(TotalSaldo);



                    contenido.UltimaFila = ultimaFilaTotales;

                    contenido.Widths = new float[] { 20f, 20f, 20f, 80f, 30f, 30f, 30f };

                    CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, false, true);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
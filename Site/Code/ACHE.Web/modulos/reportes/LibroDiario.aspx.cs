﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Contabilidad;
using System.Data.Entity;

public partial class modulos_reportes_LibroDiario : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosLibroDiarioViewModel getResults(int idPersona, bool soloAsientosManuales, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (soloAsientosManuales)
                        results = results.Where(x => x.EsManual);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    var resultsFinal = results.ToList();

                    page--;
                    ResultadosLibroDiarioViewModel resultado = new ResultadosLibroDiarioViewModel();
                    resultado.TotalPage = ((resultsFinal.GroupBy(x => x.IDAsiento).Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = resultsFinal.GroupBy(x => x.IDAsiento).Count();


                    var CantAsientos = 1;
                    resultado.Asientos = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new AsientoViewModel()
                        {
                            IDAsiento = x.FirstOrDefault().IDAsiento,
                            NroAsiento = NroAsiento(ref CantAsientos).ToString(),
                            Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
                            Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
                            Items = AsientosLibroDiario(x.FirstOrDefault().IDAsiento, results.ToList()),
                            TotalDebe = x.Sum(y => y.Debe).ToMoneyFormat(2),
                            TotalHaber = x.Sum(y => y.Haber).ToMoneyFormat(2),
                            EsManual = x.FirstOrDefault().EsManual ? "SI" : "NO"
                        }).ToList();

                    resultado.TotalDebe = resultado.Asientos.Sum(x =>  Convert.ToDecimal(x.TotalDebe)).ToMoneyFormat(2);
                    resultado.TotalHaber = resultado.Asientos.Sum(x =>  Convert.ToDecimal(x.TotalHaber)).ToMoneyFormat(2);
                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private static List<LibroDiarioViewModel> AsientosLibroDiario(int IDAsiento, List<rptImpositivoLibroDiario> results)
    {
        return results.Where(x => x.IDAsiento == IDAsiento).Select(x => new LibroDiarioViewModel()
                                {
                                    nroCuenta = x.Codigo,
                                    NombreCuenta = x.Nombre,
                                    Debe = x.Debe.ToMoneyFormat(2),
                                    Haber = x.Haber.ToMoneyFormat(2),
                                    TipoDeAsiento = x.tipodeasiento
                                }).OrderBy(x => x.TipoDeAsiento).ThenBy(x => x.nroCuenta).ToList();
    }

    private static int NroAsiento(ref int cantidad)
    {
        return cantidad++;
    }

    [WebMethod(true)]
    public static string export(int idPersona, bool soloAsientosManuales, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "LibroDiario_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (soloAsientosManuales)
                        results = results.Where(x => x.EsManual);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    var resultsFinal = results.ToList();

                    var CantAsientos = 1;
                    var listaCuentas = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).ToList()
                    .Select(x => new AsientoViewModel()
                    {
                        IDAsiento = x.FirstOrDefault().IDAsiento,
                        NroAsiento = NroAsiento(ref CantAsientos).ToString(),
                        Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
                        Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
                        Items = AsientosLibroDiario(x.FirstOrDefault().IDAsiento, results.ToList()),
                        TotalDebe = x.Sum(y => y.Debe).ToMoneyFormat(2),
                        TotalHaber = x.Sum(y => y.Haber).ToMoneyFormat(2)
                    }).ToList();

                    List<AsientoExportacionViewModel> listaFinal = new List<AsientoExportacionViewModel>();
                    AsientoExportacionViewModel asiento;
                    foreach (var item in listaCuentas)
                    {
                        asiento = new AsientoExportacionViewModel();
                        asiento.NroAsiento = item.NroAsiento;
                        asiento.Fecha = item.Fecha;
                        asiento.Leyenda = item.Leyenda;
                        listaFinal.Add(asiento);
                        foreach (var itemC in item.Items)
                        {
                            asiento = new AsientoExportacionViewModel();
                            asiento.codigo = itemC.nroCuenta;
                            asiento.NombreCuenta = itemC.NombreCuenta;
                            asiento.Debe = Convert.ToDecimal(itemC.Debe).ToMoneyFormat(2);
                            asiento.Haber = Convert.ToDecimal(itemC.Haber).ToMoneyFormat(2);
                            listaFinal.Add(asiento);
                        }
                    }

                    dt = listaFinal.ToList().Select(x => new AsientoExportacionViewModel()
                    {
                        NroAsiento = x.NroAsiento,
                        Fecha = x.Fecha,
                        Leyenda = x.Leyenda,
                        codigo = x.codigo,
                        NombreCuenta = x.NombreCuenta,
                        Debe = x.Debe,
                        Haber = x.Haber
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string exportPDF(int idPersona, bool soloAsientosManuales, string fechaDesde, string fechaHasta) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "LibroDiario_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                string TotalDebito = "0";
                string TotalCredito = "0";
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (soloAsientosManuales)
                        results = results.Where(x => x.EsManual);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    var resultsFinal = results.ToList();

                    var CantAsientos = 1;
                    var listaCuentas = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).ToList()
                    .Select(x => new AsientoViewModel()
                    {
                        IDAsiento = x.FirstOrDefault().IDAsiento,
                        NroAsiento = NroAsiento(ref CantAsientos).ToString(),
                        Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
                        Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
                        Items = AsientosLibroDiario(x.FirstOrDefault().IDAsiento, results.ToList()),
                        TotalDebe = x.Sum(y => y.Debe).ToMoneyFormat(2),
                        TotalHaber = x.Sum(y => y.Haber).ToMoneyFormat(2)
                    }).ToList();

                    List<AsientoExportacionViewModel> listaFinal = new List<AsientoExportacionViewModel>();
                    AsientoExportacionViewModel asiento;
                    foreach (var item in listaCuentas)
                    {
                        asiento = new AsientoExportacionViewModel();
                        asiento.NroAsiento = item.NroAsiento;
                        asiento.Fecha = item.Fecha;
                        asiento.Leyenda = item.Leyenda;
                        listaFinal.Add(asiento);
                        foreach (var itemC in item.Items)
                        {
                            asiento = new AsientoExportacionViewModel();
                            asiento.codigo = itemC.nroCuenta;
                            asiento.NombreCuenta = itemC.NombreCuenta;
                            asiento.Debe = Convert.ToDecimal(itemC.Debe).ToMoneyFormat(2);
                            asiento.Haber = Convert.ToDecimal(itemC.Haber).ToMoneyFormat(2);
                            listaFinal.Add(asiento);
                        }
                    }

                    dt = listaFinal.ToList().Select(x => new AsientoExportacionViewModel()
                    {
                        NroAsiento = x.NroAsiento,
                        Fecha = x.Fecha,
                        Leyenda = x.Leyenda,
                        codigo = x.codigo,
                        NombreCuenta = x.NombreCuenta,
                        Debe = x.Debe,
                        Haber = x.Haber
                    }).ToList().ToDataTable();


                    TotalDebito = listaCuentas.Sum(x =>  Convert.ToDecimal(x.TotalDebe)).ToMoneyFormat(2);
                    TotalCredito = listaCuentas.Sum(x =>  Convert.ToDecimal(x.TotalHaber)).ToMoneyFormat(2);
                }



                if (dt.Rows.Count > 0) {
                    ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
                    ///razon social , cuit y periodo de fechas 
                    List<DetalleCabecera> cabecera = new List<DetalleCabecera>();
                    DetalleCabecera c = new DetalleCabecera();
                    c.Titulo = "Razón Social";
                    c.Valor = usu.RazonSocial;
                    cabecera.Add(c);

                    DetalleCabecera c1 = new DetalleCabecera();
                    c1.Titulo = "CUIT";
                    c1.Valor = usu.CUIT;
                    cabecera.Add(c1);
                    DetalleCabecera c2 = new DetalleCabecera();

                    c2.Titulo = "Período";
                    c2.Valor = fechaDesde + " - " + fechaHasta;
                    cabecera.Add(c2);
                    contenido.Cabecera = cabecera;

                    contenido.TituloReporte = "LIBRO DIARIO";
                    List<string> ultimaFilaTotales = new List<string>();
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");

                    ultimaFilaTotales.Add("Totales");

                    ultimaFilaTotales.Add(TotalDebito);
                    ultimaFilaTotales.Add(TotalCredito);



                    //ultimaFilaTotales.Add(TotalServicios.ToMoneyFormat(4));
                    //ultimaFilaTotales.Add(TotalProductos.ToMoneyFormat(4));

                    ultimaFilaTotales.Add("");
                    contenido.UltimaFila = ultimaFilaTotales;
                    contenido.Widths =  new float[] { 20f, 20f, 150f, 30f, 80f, 30f, 30f};

                    CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, false, true);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Asientos.Where(x => x.IDAsiento == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.IDMovimientoDeFondo != null || entity.IDCheque != null || entity.IDCaja != null || entity.IDGastosBancarios != null || entity.IDCobranza != null || entity.IDCompra != null || entity.IDComprobante != null || entity.IDPago != null)
                            throw new Exception("Solo los asientos manuales se pueden eliminar.");
                        else if (!entity.EsAsientoInicio && !entity.EsAsientoCierre && ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, entity.Fecha))
                            throw new Exception("El comprobante no puede eliminarse ya que el año contable ya fue cerrado.");
                        dbContext.Asientos.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}
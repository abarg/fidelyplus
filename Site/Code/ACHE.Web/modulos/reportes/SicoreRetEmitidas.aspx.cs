﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;
using ACHE.Model.ViewModels;
using Ionic.Zip;
using System.Data.Entity;


public partial class modulos_reportes_Sicore : BasePage
{

    private static ResultadosSicoreViewModel resultado = new ResultadosSicoreViewModel();
    private static List<RegInfoSicore> listaInfoSicore = new List<RegInfoSicore>();
    private static List<RegInfoArciba> listaInfoeArciba = new List<RegInfoArciba>();
    private static List<RegInfoArcibaNC> listaInfoeArcibaNC = new List<RegInfoArcibaNC>();
    private static string obsArcibaNC = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("MMMMMMMMM yyyy");
    }

    [WebMethod(true)]
    public static ResultadosSicoreViewModel generarExportacion(string periodo)
    {
        try
        {
            var fecha = Convert.ToDateTime(periodo);
            var fechaDesde = fecha.GetFirstDayOfMonth();
            var fechaHasta = fechaDesde.GetLastDayOfMonth();

            listaInfoSicore = new List<RegInfoSicore>();
            listaInfoeArciba = new List<RegInfoArciba>();
            listaInfoeArcibaNC = new List<RegInfoArcibaNC>();
            obsArcibaNC = string.Empty;

            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    resultado = new ResultadosSicoreViewModel();

                    generarInfoSicore(usu.IDUsuario, fechaDesde, fechaHasta);
                    generarInfoeArciba(usu.IDUsuario, fechaDesde, fechaHasta);
                    generarInfoeArcibaNC(usu.IDUsuario, fechaDesde, fechaHasta);

                    generarArchivo(usu.IDUsuario);

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    //SICORE RETENCIONES EMITIDAS

    private static void generarInfoSicore(int idUsuario, DateTime fechaDesde, DateTime fechaHasta)
    {

        using (var dbContext = new ACHEEntities())
        {
            var lista = dbContext.InfoSicoreView.Where(x => x.IDUsuario == idUsuario && x.FechaRetencion >= fechaDesde
                && DbFunctions.TruncateTime(x.FechaRetencion) <= DbFunctions.TruncateTime(fechaHasta)).ToList();

            var errores = "";
            var index = 0;
            foreach (var item in lista)
            {
                index++;
                RegInfoSicore sicore = new RegInfoSicore();


                sicore.CodigoComprobante = int.Parse(item.TipoFactura).ToString("#00"); //SIEMPRE ES 6 POR LA VISTA
                sicore.FechaEmisionRetencion = item.FechaRetencion.ToString("dd/MM/yyyy");
                sicore.NumeroComprobante = item.NroCertificado.ReplaceAll("-", "").ToString().Trim().PadRight(16);
                sicore.ImporteComprobante = CompletarConCerosDecimales(Convert.ToDouble(item.ImporteTotal), 16);
                if (item.Tipo == "IVA")
                    sicore.CodigoImpuesto = "767";//IVA -> esto anda con el codigo 217
                else
                    sicore.CodigoImpuesto = "217";//ganancia
                sicore.CodigoOperacion = "1";//RETENCION 
                /*if (item.Tipo == "IVA")
                {
                    sicore.CodigoRegimen = "499";

                }
                else
                {
                    sicore.CodigoRegimen = "078";


                }*/
                if (item.CodigoRegimen.HasValue)
                    sicore.CodigoRegimen = item.CodigoRegimen.Value.ToString("#000");
                else
                    errores += "Falta el código de regimen en el pago 0001-" + item.NumeroComprobante.ToString("#00000000") + "<br/>";
                sicore.BaseCalculo = CompletarConCerosDecimales(Convert.ToDouble(item.ImporteTotal), 14);
                sicore.FechaEmisionComprobante = item.FechaCertificado.ToString("dd/MM/yyyy");
                sicore.CodigoCondicion = "01";
                sicore.ImporteRetencion = CompletarConCerosDecimales(Convert.ToDouble(item.ImporteRetencion), 14);
                //   sicore.TipoDocumentoDelRetenido = "01";
                sicore.PorcentajeExclusion = "000000";
                sicore.FechaEmisionBoletin = "          ";

                if (item.TipoDocumento == "CUIT")
                {
                    sicore.TipoDocumentoDelRetenido = "80";//PREGUNTAR SI ES LO MISMO QUE EL CODIGO DE IDENTIG
                }
                else
                {
                    //DNI 
                    sicore.TipoDocumentoDelRetenido = "96";//PREGUNTAR SI ES LO MISMO QUE EL CODIGO DE IDENTIG
                }
                //     sicore.NumeroCertificadoOriginal = long.Parse(item.NroCertificado.Replace("-", "")).ToString("#00000000000000");
                sicore.NumeroCertificadoOriginal = "00000000000000";
                sicore.NumeroDocumentoDelRetenido = item.NroDocumento.PadRight(20);

                #region esto es porque la gente tiene marcada la opcion exportacion
                sicore.RetencionPracticadaSegun = "0";//VER ACA PORQUE ES DE LONG 1
                sicore.DenominacionDelOrdenante = (" ").PadRight(30);
                sicore.Acrecentamiento = " ";
                sicore.CuitPaisRetenido = "           ";
                sicore.CuitOrdenante = "           ";
                #endregion

                listaInfoSicore.Add(sicore);
            }
            if (errores != "")
                throw new Exception(errores);
        }
    }

    private static void generarInfoeArciba(int idUsuario, DateTime fechaDesde, DateTime fechaHasta)
    {


        using (var dbContext = new ACHEEntities())
        {
            var lista = dbContext.InfoEArcibaView.Where(x => x.IDUsuario == idUsuario && x.FechaComprobante >= fechaDesde
                && DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(fechaHasta)).OrderBy(x => x.FechaComprobante).ToList();

            var errores = "";
            var index = 0;
            foreach (var item in lista)
            {

                RegInfoArciba reg = new RegInfoArciba();
                reg.TipoOperacion = item.TipoOperacion.ToString();
                reg.CodigoNorma = "029"; //codigo general de retenciones. el 08 quedo fuera de vigencia
                //if (item.Fecha.HasValue)
                //    reg.FechaRetPers = item.Fecha.Value.ToString("dd/MM/yyyy");
                //else
                reg.FechaRetPers = item.FechaComprobante.ToString("dd/MM/yyyy");

                reg.TipoComp = item.TipoComprobante.ToString("#00");


                if (item.TipoOperacion == 1)
                {
                    if (item.CondicionIvaDelRetenido.ToUpper() == "RI" && item.CondicionIvaSujetoRetencion.ToUpper() == "RI")
                        reg.Letra = "A";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "RI" && item.CondicionIvaSujetoRetencion.ToUpper() == "EX")
                        reg.Letra = "C";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "RI" && item.CondicionIvaSujetoRetencion.ToUpper() == "MO")
                        reg.Letra = "C";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "EX" && item.CondicionIvaSujetoRetencion.ToUpper() == "RI")
                        reg.Letra = "B";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "EX" && item.CondicionIvaSujetoRetencion.ToUpper() == "EX")
                        reg.Letra = "C";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "EX" && item.CondicionIvaSujetoRetencion.ToUpper() == "MO")
                        reg.Letra = "C";
                }
                else
                {
                    if (item.CondicionIvaDelRetenido.ToUpper() == "RI" && item.CondicionIvaSujetoRetencion == "RI")
                        reg.Letra = "A";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "RI" && item.CondicionIvaSujetoRetencion.ToUpper() == "EX")
                        reg.Letra = "B";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "RI" && item.CondicionIvaSujetoRetencion.ToUpper() == "MO")
                        reg.Letra = "B";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "EX" && item.CondicionIvaSujetoRetencion.ToUpper() == "")
                        reg.Letra = "B";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "EX" && item.CondicionIvaSujetoRetencion.ToUpper() == "RI")
                        reg.Letra = "C";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "EX" && item.CondicionIvaSujetoRetencion.ToUpper() == "MO")
                        reg.Letra = "C";
                    else if (item.CondicionIvaDelRetenido.ToUpper() == "EX" && item.CondicionIvaSujetoRetencion == "")
                        reg.Letra = "C";
                }

                reg.NroComprobante = item.Nro.ToString("#0000000000000000");
                reg.FechaComprobante = item.FechaComprobante.ToString("dd/MM/yyyy");
                reg.MontoComprobante = Math.Round(item.ImporteComprobante, 2).ToString("").PadLeft(16, '0');// item.ImporteComprobante.ToString("#0000000000000000");
                reg.NroCertificado = item.NroCertificadoPropio.PadRight(16);


                if (item.TipoDocumento.ToUpper() == "CUIT")
                    reg.TipoDocRetenido = "3";
                else if (item.TipoDocumento.ToUpper() == "CUIL")
                    reg.TipoDocRetenido = "2";
                //else if (item.TipoDocumento.ToUpper() == "DNI")
                //    reg.TipoDocRetenido = "1";
                //else
                //    throw new Exception(item.RazonSocial + " debe tener cargado un CUIL o CUIT");

                reg.NroDocRetenido = long.Parse(item.NroDocRetenido.ToNumericOnly()).ToString("#00000000000");
                //  reg.NroDocRetenido = 1.ToString("#00000000000");
                /*if (item.TipoDocumento.ToUpper() == "CUIT")
                    reg.SituacionIB = "1";
                else*/
                reg.SituacionIB = "4";

                //Si Tipo de Documento=3: Situación IB del Retenido=(1,2,4,5)
                //Si Tipo de Documento=(1,2): Situación IB del Retenido=4


                //if (!string.IsNullOrEmpty(reg.NroInscripcion))
                //    reg.NroInscripcion = int.Parse(reg.NroInscripcion).ToString("#00000000000");
                //else
                reg.NroInscripcion = "0".PadLeft(11, '0');

                if (item.CondicionIvaDelRetenido == "RI")
                    reg.SituacionFrenteALIVA = "1";


                if (item.CondicionIvaDelRetenido.ToUpper() == "EX")
                    reg.SituacionFrenteALIVA = "3";

                if (item.CondicionIvaDelRetenido.ToUpper() == "MO")
                    reg.SituacionFrenteALIVA = "4";
                if (item.RazonSocial.Length > 30)
                    reg.RazonSocialRetenido = item.RazonSocial.Substring(0, 29).PadRight(30);
                else
                    reg.RazonSocialRetenido = item.RazonSocial.PadRight(30);
                reg.ImporteOtrosConceptos = "0000000000000,00";//.PadLeft(16, '0');

                var montoIva = Math.Round(item.ImporteNeto, 2) - Math.Round(item.ImporteBruto, 2);
                reg.ImporteIVA = montoIva.ToString("").PadLeft(16, '0');

                //Math.Round(item.ImporteComprobante,2).ToString("").PadLeft(16,'0')

                //if (item.Alicuota.HasValue)
                //    reg.Alicuota = item.Alicuota.Value.ToString("#00.00"); //max iva del detalle
                //else
                //    reg.Alicuota = "00,00";
                reg.Alicuota = item.Importe.ToString("#00.00"); //max iva del detalle

                decimal aux = Math.Round(item.ImporteComprobante, 2) - montoIva;

                reg.MontoSujetoRetPer = aux.ToString("").PadLeft(16, '0');//menos    reg.ImporteOtrosConceptos que siempre es 0

                var montoSujeto = decimal.Parse(reg.MontoSujetoRetPer.Replace(".", ","));
                var iva = decimal.Parse(reg.Alicuota.Replace(".", ","));
                reg.RetPercPracticada = Math.Round(((montoSujeto * iva) / 100), 2).ToString("").PadLeft(16, '0');
                reg.MontoTotalRetPerc = Math.Round(((montoSujeto * iva) / 100), 2).ToString("").PadLeft(16, '0');

                listaInfoeArciba.Add(reg);

            }
            if (errores != "")
                throw new Exception(errores);
        }
    }

    private static void generarInfoeArcibaNC(int idUsuario, DateTime fechaDesde, DateTime fechaHasta)
    {

        using (var dbContext = new ACHEEntities())
        {
            var lista = dbContext.InfoEArcibaNCView.Where(x => x.IDUsuario == idUsuario && x.FechaComprobante >= fechaDesde
                && DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(fechaHasta)).OrderBy(x => x.FechaComprobante).ToList();

            var errores = "";
            var index = 0;
            int cant = 0;
            foreach (var item in lista)
            {

                var tipo = "FCB";
                var letra = "B";
                if (item.Tipo == "NCA")
                {
                    tipo = "FCA";
                    letra = "A";
                }
                else if (item.Tipo == "NCC")
                {
                    tipo = "FCC";
                    letra = "C";
                }

                var comprobante = dbContext.Comprobantes.Where(x => x.IDUsuario == idUsuario && x.Tipo == tipo && x.IDPersona == item.IDPersona && x.ImporteTotalNeto == item.ImporteTotalNeto).FirstOrDefault();
                if (comprobante != null)
                {

                    RegInfoArcibaNC reg = new RegInfoArcibaNC();
                    reg.TipoOperacion = item.TipoOperacion.ToString();
                    reg.NroNC = item.NroComprobante.ToString("#000000000000");//preguntar-> es el numero del comprobante de la nc 
                    reg.FechaNC = item.FechaAltaNC.ToString("dd/MM/yyyy");
                    reg.MontoNC = Math.Round(item.ImporteTotalBruto, 2).ToString("").PadLeft(16, '0');
                    reg.NroCertificadoPropio = item.NroCertificadoPropio.PadRight(16);
                    reg.TipoComprobanteOrigen = "01";//ver si es correcto esto.
                    reg.LetraComprobante = letra;//ver si es correcto esto que se informe siempre esto.
                    reg.NroComprobante = comprobante.Numero.ToString("0000000000000000");
                    reg.NroDocumentoRetenido = long.Parse(item.NroDocRetenido.ToNumericOnly()).ToString("#00000000000");
                    reg.CodigoNorma = "029"; //PREGUNTAR
                    reg.FechaRetencion = item.FechaComprobante.ToString("dd/MM/yyyy");
                    reg.Alicuota = item.Alicuota.ToString("#00.00");//alicuota de percepcion//max iva del detalle

                    var monto = decimal.Parse(reg.MontoNC.Replace(".", ","));
                    decimal iva = item.Iva.HasValue ? item.Iva.Value : 0;
                    reg.RetPercepADeducir = Math.Round(((monto * item.Alicuota) / 100), 2).ToString("").PadLeft(16, '0');

                    listaInfoeArcibaNC.Add(reg);
                }
                else
                {
                    if (errores != string.Empty)
                        errores += ", ";
                    errores += item.NroComprobante.ToString("#00000000");
                }

            }

            if (errores != "")
            {
                obsArcibaNC = "Se han excluido las NC: " + errores + " NC por no contar con un comprobante de igual monto registrado.";
            }

            //if (errores != "")
            //    throw new Exception(errores);
        }
    }

    private static void generarArchivo(int idUsuario)
    {
        resultado.Items = new List<SicoreViewModel>();
        string fileName = "SICORE_" + idUsuario;
        string path = "~/tmp/";
        var destino = HttpContext.Current.Server.MapPath(path + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");

        using (StreamWriter escritor = new StreamWriter(destino, false))
        {
            foreach (var item in listaInfoSicore)
            {
                escritor.WriteLine(item.CodigoComprobante + item.FechaEmisionComprobante + item.NumeroComprobante + item.ImporteComprobante
                    + item.CodigoImpuesto + item.CodigoRegimen + item.CodigoOperacion + item.BaseCalculo
                    + item.FechaEmisionRetencion + item.CodigoCondicion + item.RetencionPracticadaSegun + item.ImporteRetencion
                    + item.PorcentajeExclusion + item.FechaEmisionBoletin + item.TipoDocumentoDelRetenido + item.NumeroDocumentoDelRetenido
                    + item.NumeroCertificadoOriginal + item.DenominacionDelOrdenante + item.Acrecentamiento + item.CuitPaisRetenido + item.CuitOrdenante);
            }
        }

        SicoreViewModel sicore = new SicoreViewModel();
        sicore.NombreArchivo = fileName;
        sicore.Errores = "";
        sicore.Observaciones = "";
        sicore.Nombre = "SICORE";

        sicore.URL = (path + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt").Replace("~", "");
        resultado.Items.Add(sicore);
        /*----*/
        fileName = "eArciba_Facturas_" + idUsuario;
        destino = HttpContext.Current.Server.MapPath(path + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");

        using (StreamWriter escritor = new StreamWriter(destino, false))
        {
            foreach (var item in listaInfoeArciba)
            {
                escritor.WriteLine(item.TipoOperacion + item.CodigoNorma + item.FechaRetPers + item.TipoComp
                    + item.Letra + item.NroComprobante + item.FechaComprobante + item.MontoComprobante
                    + item.NroCertificado + item.TipoDocRetenido + item.NroDocRetenido + item.SituacionIB
                    + item.NroInscripcion + item.SituacionFrenteALIVA + item.RazonSocialRetenido + item.ImporteOtrosConceptos
                    + item.ImporteIVA + item.MontoSujetoRetPer + item.Alicuota + item.RetPercPracticada + item.MontoTotalRetPerc);
            }
        }

        SicoreViewModel eArciba = new SicoreViewModel();
        eArciba.NombreArchivo = fileName;
        eArciba.Errores = "";
        eArciba.Observaciones = "";
        eArciba.Nombre = "eArciba - Facturas";
        eArciba.URL = (path + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt").Replace("~", "");
        resultado.Items.Add(eArciba);


        /*----*/
        fileName = "eArciba_nc_" + idUsuario;
        destino = HttpContext.Current.Server.MapPath(path + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");

        using (StreamWriter escritor = new StreamWriter(destino, false))
        {
            foreach (var item in listaInfoeArcibaNC)
            {
                escritor.WriteLine(item.TipoOperacion + item.NroNC + item.FechaNC + item.MontoNC
                    + item.NroCertificadoPropio + item.TipoComprobanteOrigen + item.LetraComprobante + item.NroComprobante
                    + item.NroDocumentoRetenido + item.CodigoNorma + item.FechaRetencion + item.RetPercepADeducir
                    + item.Alicuota);
            }
        }

        SicoreViewModel eArcibaNC = new SicoreViewModel();
        eArcibaNC.NombreArchivo = fileName;
        eArcibaNC.Errores = "";
        eArcibaNC.Observaciones = obsArcibaNC;
        eArcibaNC.Nombre = "eArciba - Notas de crédito";
        eArcibaNC.URL = (path + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt").Replace("~", "");
        resultado.Items.Add(eArcibaNC);

        /*fileName = "eArciba_NC_" + idUsuario;
        eArciba = new SicoreViewModel();
        eArciba.NombreArchivo = fileName;
        eArciba.Errores = "";
        eArciba.Nombre = "eArciba - NC";
        eArciba.URL = (path + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt").Replace("~", "");
        resultado.Items.Add(eArciba);*/

    }

    private static string CompletarConCerosDecimales(double input, int Cantidad)
    {
        var inputs = String.Format("{0:n}", input);
        //    inputs = inputs.Replace(",", "");
        inputs = inputs.Replace(".", "");
        var res = string.Empty;
        for (int i = 0; i < (Cantidad - inputs.Length); i++)
            res += "0";

        return res + inputs;
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="EstadoResultado.aspx.cs" Inherits="modulos_reportes_EstadoResultado" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Estado de resultado</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">Estado de resultado</li>
            </ol>
        </div>
    </div>

    <div id="divConDatos" runat="server">
    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>
                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-8">
                                    <div class="row">
                                        <h4 class="subtitle mb5 hide">Proveedor/Cliente</h4>
                                        <select class="select2 hide" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                            <option value=""></option>
                                        </select>

                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Desde" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Hasta" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <select class="form-control" id="ddlMostrarCot">
                                                <option value="0">Mostrar todo</option>
                                                <option value="1">Sólo Cotizaciones</option>
                                                <option value="2">Excluir Cotizaciones</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <a class="btn btn-black" onclick="EstadoResultado.resetearPagina();EstadoResultado.filtrar();" id="btnBuscar">Buscar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 responsive-buttons md">
                                    
                                </div>

                            </div>

                        </form>

                        <div class="col-sm-12"><hr class="mt0" /></div>

                        <div class="row">
                            <div class="pull-right">
                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:EstadoResultado.exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="EstadoResultado.resetearExportacion();" download="EstadoResultado" style="display: none">Descargar</a>
                                    </div>

                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="EstadoResultado.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="EstadoResultado.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb30" id="resultsContainer">
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        <thead>
            <tr>
                <th>Cuenta</th>
                {{each ListaThead}}
                <th style="text-align: right;">${Nombre}</th>
                {{/each}}
            </tr>
        </thead>

        {{each ListaTbody}}
        <tbody>
            <tr>
                <td>${Nombre}</td>
                {{each ListaImportes}}
                <td class="bgTotal" style="text-align: right;">${ImporteDesc}</td>
                {{/each}}
            </tr>
        </tbody>
        {{/each}}
    </script>
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="6">No se han encontrado resultados</td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/EstadoResultado.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            EstadoResultado.configFilters();
            EstadoResultado.filtrar();
        });
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="compras-por-categoria-mensual.aspx.cs" Inherits="modulos_reportes_compras_por_categoria_mensual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Compras por categoría mensuales</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">Compras por categoría mensuales</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>
                        <form runat="server" id="frmSearch">

                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-3 col-md-3 form-group">
                                    <select class="select2" data-placeholder="Seleccione un año..." id="ddlAnio">
                                    </select>
                                </div>
                                <div class="col-sm-2 col-md-2 form-group">
                                    <select id="ddlIncluirCot" class="form-control">
                                        <option value="0">No incluir cotizaciones</option>
                                        <option value="1">Incluir cotizaciones</option>
                                    </select>
                                </div>
                                <div class="col-sm-2 col-md-2 form-group">
                                    <input type="checkbox" id="chkIva" checked />&nbsp;
                                    <label for="chkIva"><strong>Incluir IVA</strong></label>
                                </div>

                                <div class="col-sm-5 col-md-4 form-group">
                                    <a class="btn btn-black" onclick="filtrarBusqueda();" id="btn_buscar">Buscar</a>

                                </div>
                            </div>

                        </form>

                        <div class="col-sm-12">
                            <hr class="mt0" />
                        </div>

                        <div class="row">

                            <div class="pull-right">
                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="resetearExportacion();" download="CuentaCorriente" style="display: none">Descargar</a>
                                    </div>

                                </div>


                            </div>

                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div id="table"></div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
            <tr>
                <td>${Nombre}</td>               


                <td style="text-align: right;border-left-style: solid;border-right-style: solid;">${MontoVentaStr}</td>
                <td style="text-align: right;border-right-style: solid;">${IvaVentaStr}</td>
                <td style="text-align: right;border-right-style: solid;">${TotalVentaStr}</td>

             
            </tr>

        {{/each}}  
        <tr>
           
            <td class="bgTotal text-danger">TOTAL</td>

            <td class="bgTotal text-danger" style="text-align: right"><span id="totalMontoVenta"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="totalIvaVenta"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="totalVenta"></span></td>
            
         
        </tr>     
        <tr>
            <td class="bgTotal text-danger">PROMEDIO</td>

            <td class="bgTotal text-danger" style="text-align: right"><span id="totalMontoVentaPromedio"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="totalIvaVentaPromedio"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="totalVentaPromedio"></span></td>
            
           
        </tr>     
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="4">No se han encontrado resultados</td>
        </tr>
    </script>-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/compras-por-categoria-mensual.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            configFilters();
        });
    </script>
</asp:Content>


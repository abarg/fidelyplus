﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="iva-compras.aspx.cs" Inherits="modulos_reportes_iva_compras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>IVA Compras</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">IVA Compras</li>
            </ol>
        </div>
    </div>
    <div id="divConDatos" runat="server">
    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>
                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">
                                 <div class="col-sm-8 col-md-8">
                                    <div class="row">

                                        <div class="col-sm-6 col-md-6 form-group">
                                            <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Desde" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Hasta" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 responsive-buttons md">
                                    <a class="btn btn-black" onclick="resetearPagina();filtrar();">Buscar</a>
                                </div>

                            </div>

                        </form>

                        <div class="col-sm-12"><hr class="mt0" /></div>

                        <div class="row">
                            <div class="pull-right">
                                <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                <a href="" id="lnkDownload" onclick="resetearExportacion();" download="IvaCompras" style="display: none">Descargar</a>
                                <div class="btn-group mr10 dropdown" id="divExportar" runat="server">
                                    <button type="button" class="btn btn-btn-default"><i class="fa fa-tasks"></i> Exportar</button>
                                    <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:exportar();" >A Excel</a></li>
                                        <li><a href="javascript:exportarPDF();">A PDF</a></li>
                                                
                                    </ul>
                                    
                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <%--<th>Tipo</th>--%>
                                        <th>Nro Factura</th>
                                        <th>Razón Social</th>
                                        <th>CUIT</th>

                                        <th style="text-align: right">Monto Grav Iva 2</th>
                                        <th style="text-align: right">Monto Grav Iva 5</th>
                                        <th style="text-align: right">Monto Grav Iva 10</th>
                                        <th style="text-align: right">Monto Grav Iva 21</th>
                                        <th style="text-align: right">Monto Grav Iva 27</th>
                                        <th style="text-align: right">Monto No Grav y Ex</th>
                                        <th style="text-align: right">Monto Grav Mon</th>
                                        <th style="text-align: right">Iva Fact</th>
                                        
                                        <th style="text-align: right">Imp Interno</th>
                                        <th style="text-align: right">Imp Municipal</th>
                                        <th style="text-align: right">Imp Nacional</th>

                                        <th style="text-align: right">Otros</th>
                                        <th style="text-align: right">Percepción IVA</th>
                                        <th style="text-align: right">IIBB</th>

                                        <th style="text-align: right">Total Fact</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
   <%-- <div class="contentpanel">
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <form runat="server" id="frmSearch">
                    <input type="hidden" id="hdnPage" runat="server" value="1" />

                    <h4 class="subtitle mb5">Filtros disponibles</h4>

                    <div class="mb20"></div>
                    <h4 class="subtitle mb5">Proveedor/Cliente</h4>
                    <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                        <option value=""></option>
                    </select>

                    <div class="mb20"></div>
                    <h4 class="subtitle mb5">Fecha</h4>
                    <div class="row row-pad-5">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                            </div>
                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                            </div>
                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                        </div>

                    </div>

                    <div class="mb20"></div>

                    <a class="btn btn-black" onclick="resetearPagina();filtrar();">Buscar</a>
                    <a class="btn btn-default" onclick="resetearPagina();verTodos();">Ver todos</a>

                    <br />
                </form>
            </div>
            <div class="col-sm-10 col-md-10 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-right">
                            <div class="btn-group mr10">
                                <div class="btn btn-white tooltips">
                                    <a id="divIconoDescargar" href="javascript:exportar();">
                                        <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                    </a>
                                    <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                    <a href="" id="lnkDownload" onclick="resetearExportacion();" download="IvaCompras" style="display: none">Descargar</a>
                                </div>
                            </div>

                            <div class="btn-group mr10" id="divPagination" style="display: none">
                                <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                            </div>
                        </div>

                        <h4 class="panel-title">Resultados</h4>
                        <p id="msjResultados"></p>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Tipo</th>
                                        <th>Nro Factura</th>
                                        <th>Razón Social</th>
                                        <th>CUIT</th>

                                        <th style="text-align: right">Monto Grav Iva 2</th>
                                        <th style="text-align: right">Monto Grav Iva 5</th>
                                        <th style="text-align: right">Monto Grav Iva 10</th>
                                        <th style="text-align: right">Monto Grav Iva 21</th>
                                        <th style="text-align: right">Monto Grav Iva 27</th>
                                        <th style="text-align: right">Monto No Grav y Ex</th>
                                        <th style="text-align: right">Monto Grav Mon</th>
                                        <th style="text-align: right">Iva Fact</th>
                                        
                                        <th style="text-align: right">Imp Interno</th>
                                        <th style="text-align: right">Imp Municipal</th>
                                        <th style="text-align: right">Imp Nacional</th>

                                        <th style="text-align: right">Otros</th>
                                        <th style="text-align: right">Percepción IVA</th>
                                        <th style="text-align: right">IIBB</th>

                                        <th style="text-align: right">Total Fact</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>--%>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td>${Fecha}</td>
            <%--<td>${Tipo}</td>--%>
            <td style="min-width: 100px">${NroFactura}</td>
            <td>${RazonSocial}</td>
            <td>${Cuit}</td>
            <td style="text-align: right">${MontoGravadoIva2}</td>
            <td style="text-align: right">${MontoGravadoIva5}</td>
            <td style="text-align: right">${MontoGravadoIva10}</td>
            <td style="text-align: right">${MontoGravadoIva21}</td>
            <td style="text-align: right">${MontoGravadoIva27}</td>
            <td style="text-align: right">${MontoNoGravadoYExentos}</td>
            <td style="text-align: right">${MontoGravadoMonotributistas}</td>
            <td style="text-align: right">${IvaFacturado}</td>
            <%--<td style="text-align: right">${IvaPercepcion}</td>--%>
            <td style="text-align: right">${ImpInterno}</td>
            <td style="text-align: right">${ImpMunicipal}</td>
            <td style="text-align: right">${ImpNacional}</td>

            <td style="text-align: right">${Otros}</td>
            <td style="text-align: right">${PercepcionIVA}</td>
            <td style="text-align: right">${IIBB}</td>

            <td style="text-align: right">${TotalFacturado}</td>
        </tr>
        {{/each}}
        <tr>
            <td class="bgTotal text-danger" colspan="4">TOTALES</td>
            
            <%--<td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalImporte"></span></td>--%>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA2"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA5"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA10"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA21"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA27"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalNoGrav"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalGravMo"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIva"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalImpInterno"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalImpMun"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalImpNac"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalOtros"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalPercIva"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIIBB"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalTotal"></span></td>
        </tr>    
        <tr>
            <td class="bgTotal text-danger" colspan="19">TOTALES POR CONDICION DE IVA</td>
        </tr>
        <tr>
            <td colspan="19">Responsables Incriptos: <span id="spnTotalRI"></span></td>
        </tr>
        <tr>
            <td colspan="19">Monotributistas: <span id="spnTotalMO"></span></td>
        </tr>
        <tr>
            <td colspan="19">Consumidores Finales: <span id="spnTotalCF"></span></td>
        </tr>
        <tr>
            <td colspan="19">Exentos: <span id="spnTotalEX"></span></td>
        </tr>
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="19">No se han encontrado resultados
            </td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/iva-compras.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            filtrar();
            configFilters();
        });
    </script>
</asp:Content>


﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using System.Data.Entity;

public partial class modulos_reportes_iva_compras : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptIvaComprasViewModel getResults(int idPersona, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];


                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptIvaCompras.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    page--;
                    ResultadosRptIvaComprasViewModel resultado = new ResultadosRptIvaComprasViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    //TODO: MEJORAR ESTO
                    var auxList = results.ToList();
                    decimal TotalIva = 0;
                    decimal TotalImporte = 0;
                    decimal TotalTotal = 0;
                    decimal TotalIVA27 = 0;
                    decimal TotalIVA21 = 0;
                    decimal TotalIVA10 = 0;
                    decimal TotalIVA5 = 0;
                    decimal TotalIVA2 = 0;
                    decimal TotalNoGrav = 0;
                    decimal TotalGravMon = 0;
                    decimal TotalImpInterno = 0;
                    decimal TotalImpMun = 0;
                    decimal TotalImpNac = 0;
                    decimal TotalOtros = 0;
                    decimal TotalPercIva = 0;
                    decimal TotalIIBB = 0;

                    decimal TotalRI = 0;
                    decimal TotalMO = 0;
                    decimal TotalEX = 0;
                    decimal TotalCF = 0;

                    foreach (var x in auxList)
                    {
                        decimal auxTotal = (x.Iva + x.TotalImporte.Value + x.Percepciones);

                        TotalImporte += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.TotalImporte.Value : -1 * x.TotalImporte.Value;
                        TotalIVA2 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe2) : -1 * Convert.ToDecimal(x.Importe2);
                        TotalIVA10 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe10) : -1 * Convert.ToDecimal(x.Importe10);
                        TotalIVA5 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe5) : -1 * Convert.ToDecimal(x.Importe5);
                        TotalIVA21 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe21) : -1 * Convert.ToDecimal(x.Importe21);
                        TotalIVA27 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe27) : -1 * Convert.ToDecimal(x.Importe27);
                        TotalIva += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Iva : -1 * x.Iva;
                        TotalTotal += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal;

                        TotalNoGrav += x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.TotalImporte.Value : (x.TotalImporte.Value * -1)) : ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? (x.NoGravado + x.Exento) : ((x.NoGravado + x.Exento) * -1));
                        TotalGravMon += x.CondicionIVA != "EX" ? (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImporteMon : (x.ImporteMon * -1) : 0;
                        TotalImpInterno += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpInterno : -1 * x.ImpInterno;
                        TotalImpMun += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpMunicipal : -1 * x.ImpMunicipal;
                        TotalImpNac += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpNacional : -1 * x.ImpNacional;
                        TotalOtros += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Otros : -1 * x.Otros;
                        TotalPercIva += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.PercepcionIVA : -1 * x.PercepcionIVA;
                        TotalIIBB += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.IIBB : -1 * x.IIBB;

                        TotalRI += x.CondicionIVA == "RI" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                        TotalMO += x.CondicionIVA == "MO" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                        TotalEX += x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                        TotalCF += x.CondicionIVA == "CF" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                    }

                    resultado.TotalIva = TotalIva.ToMoneyFormat(2);
                    resultado.TotalImporte = TotalImporte.ToMoneyFormat(2);
                    resultado.TotalTotal = TotalTotal.ToMoneyFormat(2);
                    resultado.TotalIVA27 = TotalIVA27.ToMoneyFormat(2);
                    resultado.TotalIVA21 = TotalIVA21.ToMoneyFormat(2);
                    resultado.TotalIVA10 = TotalIVA10.ToMoneyFormat(2);
                    resultado.TotalIVA5 = TotalIVA5.ToMoneyFormat(2);
                    resultado.TotalIVA2 = TotalIVA2.ToMoneyFormat(2);

                    resultado.TotalNoGrav = TotalNoGrav.ToMoneyFormat(2);
                    resultado.TotalGravMon = TotalGravMon.ToMoneyFormat(2);
                    resultado.TotalImpInterno = TotalImpInterno.ToMoneyFormat(2);
                    resultado.TotalImpMun = TotalImpMun.ToMoneyFormat(2);
                    resultado.TotalImpNac = TotalImpNac.ToMoneyFormat(2);
                    resultado.TotalOtros = TotalOtros.ToMoneyFormat(2);
                    resultado.TotalPercIva = TotalPercIva.ToMoneyFormat(2);
                    resultado.TotalIIBB = TotalIIBB.ToMoneyFormat(2);

                    resultado.TotalRI = TotalRI.ToMoneyFormat(2);
                    resultado.TotalMO = TotalMO.ToMoneyFormat(2);
                    resultado.TotalEX = TotalEX.ToMoneyFormat(2);
                    resultado.TotalCF = TotalCF.ToMoneyFormat(2);

                    var list = auxList.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new RptIvaComprasViewModel()
                        {
                            Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                            //Tipo = x.Tipo,
                            NroFactura = x.Tipo + " " + x.Factura,
                            RazonSocial = x.RazonSocial,
                            Cuit = x.CUIT,

                            MontoGravadoIva2 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.Importe2.ToMoneyFormat(2) : (x.Importe2 * -1).ToMoneyFormat(2),
                            MontoGravadoIva5 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.Importe5.ToMoneyFormat(2) : (x.Importe5 * -1).ToMoneyFormat(2),
                            MontoGravadoIva10 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.Importe10.ToMoneyFormat(2) : (x.Importe10 * -1).ToMoneyFormat(2),
                            MontoGravadoIva21 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.Importe21.ToMoneyFormat(2) : (x.Importe21 * -1).ToMoneyFormat(2),
                            MontoGravadoIva27 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.Importe27.ToMoneyFormat(2) : (x.Importe27 * -1).ToMoneyFormat(2),
                            MontoNoGravadoYExentos = x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.TotalImporte.Value.ToMoneyFormat(2) : (x.TotalImporte.Value * -1).ToMoneyFormat(2)) : ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? (x.NoGravado + x.Exento).ToMoneyFormat(2) : ((x.NoGravado + x.Exento) * -1).ToMoneyFormat(2)),
                            MontoGravadoMonotributistas = x.CondicionIVA != "EX" ? (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.ImporteMon.ToMoneyFormat(2) : (x.ImporteMon * -1).ToMoneyFormat(2) : "",
                            IvaFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.Iva.ToMoneyFormat(2) : (x.Iva * -1).ToMoneyFormat(2),
                            //IvaPercepcion = x.Percepciones.ToMoneyFormat(),

                            ImpInterno = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.ImpInterno.ToMoneyFormat(2) : (x.ImpInterno * -1).ToMoneyFormat(2),
                            ImpMunicipal = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.ImpMunicipal.ToMoneyFormat(2) : (x.ImpMunicipal * -1).ToMoneyFormat(2),
                            ImpNacional = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.ImpNacional.ToMoneyFormat(2) : (x.ImpNacional * -1).ToMoneyFormat(2),
                            Otros = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.Otros.ToMoneyFormat(2) : (x.Otros * -1).ToMoneyFormat(2),
                            PercepcionIVA = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.PercepcionIVA.ToMoneyFormat(2) : (x.PercepcionIVA * -1).ToMoneyFormat(2),
                            IIBB = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? x.IIBB.ToMoneyFormat(2) : (x.IIBB * -1).ToMoneyFormat(2),
                            TotalFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCE") ? (x.Iva + x.TotalImporte.Value + x.Percepciones).ToMoneyFormat(2) : ((x.Iva + x.TotalImporte.Value + x.Percepciones) * -1).ToMoneyFormat(2),
                        });

                    /*foreach (var item in list)
                    {
                        if (item.Tipo == "NCA" || item.Tipo == "NCB" || item.Tipo == "NCC")
                        {
                            item.MontoGravadoIva10 = item.MontoGravadoIva10 * -1;
                        }
                    }*/


                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export(int idPersona, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "IvaCompras_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptIvaCompras.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    dt = results.OrderBy(x => x.Fecha).ToList().Select(x => new
                    {
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        //Tipo = x.Tipo,
                        NroFactura = x.Tipo + " " + x.Factura,
                        RazonSocial = x.RazonSocial,
                        Cuit = x.CUIT,
                        //CondicionIVA = x.CondicionIVA,
                        //Importe = x.Importe.ToMoneyFormat(),
                        MontoGravadoIva2 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe2 : x.Importe2 - 1,
                        MontoGravadoIva5 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe5 : x.Importe5 - 1,
                        MontoGravadoIva10 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe10 : x.Importe10 * -1,
                        MontoGravadoIva21 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe21 : x.Importe21 * -1,
                        MontoGravadoIva27 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe27 : x.Importe27 * -1,
                        MontoNoGravadoYExentos = x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.TotalImporte.Value : x.TotalImporte.Value * -1) : ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM") ? (x.NoGravado + x.Exento) : (x.NoGravado + x.Exento) * -1),
                        MontoGravadoMonotributistas = x.CondicionIVA != "EX" ? (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImporteMon : x.ImporteMon * -1 : 0,
                        IvaFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Iva : x.Iva * -1,
                        //IvaPercepcion = +x.Percepciones,
                        ImpInterno = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpInterno : x.ImpInterno * -1,
                        ImpMunicipal = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpMunicipal : x.ImpMunicipal * -1,
                        ImpNacional = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpNacional : x.ImpNacional * -1,
                        Otros = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Otros : x.Otros * -1,
                        PercepcionIVA = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.PercepcionIVA : x.PercepcionIVA * -1,
                        IIBB = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.IIBB : x.IIBB * -1,
                        TotalFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? (x.Iva + x.TotalImporte.Value + x.Percepciones) : (x.Iva + x.TotalImporte.Value + x.Percepciones) * -1,
                        Rubro = x.Rubro != null ? x.Rubro : "",
                        Provincia = x.Provincia,
                        Observaciones = x.Observaciones
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }


    [WebMethod(true)]
    public static string exportPDF(int idPersona, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            decimal TotalRI = 0;
            decimal TotalMO = 0;
            decimal TotalEX = 0;
            decimal TotalCF = 0;
            decimal TotalIva = 0;
            decimal TotalImporte = 0;
            decimal TotalTotal = 0;
            decimal TotalIVA27 = 0;
            decimal TotalIVA21 = 0;
            decimal TotalIVA10 = 0;
            decimal TotalIVA5 = 0;
            decimal TotalIVA2 = 0;
            decimal TotalNoGrav = 0;
            decimal TotalGravMon = 0;
            decimal TotalImpInterno = 0;
            decimal TotalImpMun = 0;
            decimal TotalImpNac = 0;
            decimal TotalOtros = 0;
            decimal TotalPercIva = 0;
            decimal TotalIIBB = 0;

            string fileName = "IvaCompras_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptIvaCompras.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }


                    var auxList = results.ToList();

                    foreach (var x in auxList)
                    {
                        decimal auxTotal = (x.Iva + x.TotalImporte.Value + x.Percepciones);

                        TotalImporte += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.TotalImporte.Value : -1 * x.TotalImporte.Value;
                        TotalIVA2 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe2) : -1 * Convert.ToDecimal(x.Importe2);
                        TotalIVA10 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe10) : -1 * Convert.ToDecimal(x.Importe10);
                        TotalIVA5 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe5) : -1 * Convert.ToDecimal(x.Importe5);
                        TotalIVA21 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe21) : -1 * Convert.ToDecimal(x.Importe21);
                        TotalIVA27 += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? Convert.ToDecimal(x.Importe27) : -1 * Convert.ToDecimal(x.Importe27);
                        TotalIva += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Iva : -1 * x.Iva;
                        TotalTotal += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal;

                        TotalNoGrav += x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.TotalImporte.Value : (x.TotalImporte.Value * -1)) : ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? (x.NoGravado + x.Exento) : ((x.NoGravado + x.Exento) * -1));
                        TotalGravMon += x.CondicionIVA != "EX" ? (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImporteMon : (x.ImporteMon * -1) : 0;
                        TotalImpInterno += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpInterno : -1 * x.ImpInterno;
                        TotalImpMun += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpMunicipal : -1 * x.ImpMunicipal;
                        TotalImpNac += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpNacional : -1 * x.ImpNacional;
                        TotalOtros += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Otros : -1 * x.Otros;
                        TotalPercIva += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.PercepcionIVA : -1 * x.PercepcionIVA;
                        TotalIIBB += (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.IIBB : -1 * x.IIBB;

                        TotalRI += x.CondicionIVA == "RI" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                        TotalMO += x.CondicionIVA == "MO" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                        TotalEX += x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                        TotalCF += x.CondicionIVA == "CF" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? auxTotal : -1 * auxTotal) : 0;
                    }
                    dt = auxList.OrderBy(x => x.Fecha).ToList().Select(x => new
                    {
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        //Tipo = x.Tipo,
                        NroFactura = x.Tipo + " " + x.Factura,
                        RazonSocial = x.RazonSocial,
                        Cuit = x.CUIT,
                        //CondicionIVA = x.CondicionIVA,
                        //Importe = x.Importe.ToMoneyFormat(),
                        MontoGravadoIva2 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe2.ToMoneyFormat(2) : (x.Importe2 - 1).ToMoneyFormat(2),
                        MontoGravadoIva5 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe5.ToMoneyFormat(2) : (x.Importe5 - 1).ToMoneyFormat(2),
                        MontoGravadoIva10 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe10.ToMoneyFormat(2) : (x.Importe10 * -1).ToMoneyFormat(2),
                        MontoGravadoIva21 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe21.ToMoneyFormat(2) : (x.Importe21 * -1).ToMoneyFormat(2),
                        MontoGravadoIva27 = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Importe27.ToMoneyFormat(2) : (x.Importe27 * -1).ToMoneyFormat(2),
                        MontoNoGravadoYExentos = x.CondicionIVA == "EX" ? ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.TotalImporte.Value.ToMoneyFormat(2) : (x.TotalImporte.Value * -1).ToMoneyFormat(2)) : ((x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM") ? (x.NoGravado + x.Exento).ToMoneyFormat(2) : ((x.NoGravado + x.Exento) * -1).ToMoneyFormat(2)),
                        MontoGravadoMonotributistas = x.CondicionIVA != "EX" ? (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImporteMon.ToMoneyFormat(2) : (x.ImporteMon * -1).ToMoneyFormat(2) : "0",
                        IvaFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Iva.ToMoneyFormat(2) : (x.Iva * -1).ToMoneyFormat(2),
                        //IvaPercepcion = +x.Percepciones,
                        ImpInterno = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpInterno.ToMoneyFormat(2) : (x.ImpInterno * -1).ToMoneyFormat(2),
                        ImpMunicipal = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpMunicipal.ToMoneyFormat(2) : (x.ImpMunicipal * -1).ToMoneyFormat(2),
                        ImpNacional = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.ImpNacional.ToMoneyFormat(2) : (x.ImpNacional * -1).ToMoneyFormat(2),
                        Otros = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.Otros.ToMoneyFormat(2) : (x.Otros * -1).ToMoneyFormat(2),
                        PercepcionIVA = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.PercepcionIVA.ToMoneyFormat(2) : (x.PercepcionIVA * -1).ToMoneyFormat(2),
                        IIBB = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? x.IIBB.ToMoneyFormat(2) : (x.IIBB * -1).ToMoneyFormat(2),
                        TotalFacturado = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE") ? (x.Iva + x.TotalImporte.Value + x.Percepciones).ToMoneyFormat(2) : ((x.Iva + x.TotalImporte.Value + x.Percepciones) * -1).ToMoneyFormat(2)
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                {
                    ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
                    contenido.TituloReporte = "LIBRO IVA COMPRAS";
                    List<DetalleCabecera> cabecera = new List<DetalleCabecera>();

                    DetalleCabecera c = new DetalleCabecera();
                    c.Titulo = "Razón Social";
                    c.Valor = usu.RazonSocial;
                    cabecera.Add(c);

                    DetalleCabecera c1 = new DetalleCabecera();
                    c1.Titulo = "CUIT";
                    c1.Valor = usu.CUIT;
                    cabecera.Add(c1);
                    DetalleCabecera c2 = new DetalleCabecera();

                    c2.Titulo = "Período";
                    c2.Valor = fechaDesde + " - " + fechaHasta;
                    cabecera.Add(c2);
                    contenido.Cabecera = cabecera;


                    List<Total> totales = new List<Total>();
                    Total tot1 = new Total();
                    tot1.Importe = TotalRI.ToMoneyFormat(2);
                    tot1.Nombre = "Responsables Incriptos";
                    totales.Add(tot1);
                    Total tot2 = new Total();
                    tot2.Importe = TotalMO.ToMoneyFormat(2);
                    tot2.Nombre = "Monotributistass";
                    totales.Add(tot2);
                    Total tot3 = new Total();
                    tot3.Importe = TotalCF.ToMoneyFormat(2);
                    tot3.Nombre = "Consumidores Finales";
                    totales.Add(tot3);
                    Total tot4 = new Total();
                    tot4.Importe = TotalEX.ToMoneyFormat(2);
                    tot4.Nombre = "Exentos";
                    totales.Add(tot4);
                    contenido.Totales = totales;

                    List<string> ultimaFilaTotales = new List<string>();
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");
                    //ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add(TotalIVA2.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA5.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA10.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA21.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIVA27.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalNoGrav.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalGravMon.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIva.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalImpInterno.ToMoneyFormat(2));

                    ultimaFilaTotales.Add(TotalImpMun.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalImpNac.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalOtros.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalPercIva.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalIIBB.ToMoneyFormat(2));
                    ultimaFilaTotales.Add(TotalTotal.ToMoneyFormat(2));

                    //  ultimaFilaTotales.Add(TotalImporte .ToMoneyFormat(2));

                    contenido.UltimaFila = ultimaFilaTotales;
                    CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, false, false);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using System.Collections.Generic;
using System.Data.Entity;

public partial class modulos_reportes_historicoStock : BasePage
{
    private static List<RptStockHistorial> historicoStockResults = new List<RptStockHistorial>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);
            //divExportarDetalle.Visible = divExportar.Visible;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptHistoricoStockViewModel getResults(int idConcepto, int idDeposito, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            historicoStockResults.Clear();
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptStockHistorial.Where(x => x.IDUsuario == usu.IDUsuario && x.TipoMovimiento != "Reserva");
                    if (idConcepto > 0)
                    {
                        results = results.Where(x => x.IDConcepto == idConcepto);
                    }
                    if (idDeposito > 0)
                    {
                        results = results.Where(x => x.IDInventario == idDeposito);
                    }
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 11:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    historicoStockResults = results.ToList();//todo:mejorar esto
                    page--;
                    ResultadosRptHistoricoStockViewModel resultado = new ResultadosRptHistoricoStockViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new RptHistoricoStockViewModel()
                        {
                            Fecha = x.Fecha,
                            Deposito = x.Deposito,
                            Tipo = x.TipoMovimiento == "Comprobante" ? (x.Cantidad > 0 ? "Nota de crédito" : "Venta") : x.TipoMovimiento,
                            IdFactura = x.TipoMovimiento == "Comprobante" ? x.IDComprobante.Value : (x.TipoMovimiento == "Compra" ? x.IDCompra.Value : 0),
                            Factura = x.TipoMovimiento == "Comprobante" && x.PuntoVenta.HasValue ? x.Tipo + " " + x.PuntoVenta.Value.ToString("#0000") + "-" + x.Numero.Value.ToString("#00000000") : (x.TipoMovimiento == "Compra" ? x.NroCompra : ""),
                            Nombre = x.Nombre,
                            Codigo = x.Codigo,                            CantModificada = (x.TipoMovimiento != "ModificacionStock" && x.TipoMovimiento != "Actualizacion masiva" && x.TipoMovimiento != "Migracion") ? x.Cantidad.ToMoneyFormat(2) : "",
                            StockActualizado = x.StockActualizado ?? 0,
                            Observaciones = x.Descripcion
                        });
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Entradas_Salidas_Productos" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                dt = historicoStockResults.Select(x => new
                {
                    Fecha = x.Fecha,
                    Deposito = x.Deposito,
                    //Tipo = x.TipoMovimiento == "Comprobante" ? (x.Cantidad > 0 ? "Nota de crédito" : "Venta") : x.TipoMovimiento,
                    Detalle = x.TipoMovimiento == "Comprobante" && x.PuntoVenta.HasValue ? x.Tipo + " " + x.PuntoVenta.Value.ToString("#0000") + "-" + x.Numero.Value.ToString("#00000000") : (x.TipoMovimiento == "Compra" ? x.NroCompra : ""),
                    Codigo = x.Codigo,
                    Nombre = x.Nombre,
                    Cantidad = (x.TipoMovimiento != "ModificacionStock" && x.TipoMovimiento != "Actualizacion masiva" && x.TipoMovimiento != "Migracion") ? x.Cantidad : 0,
                    StockActualizado = x.StockActualizado ?? 0,
                    Observaciones = x.Descripcion                }).ToList().ToDataTable();

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

}

﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;
using System.Data.Entity;

public partial class modulos_reportes_rnk_conceptos : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);

            using (var dbContext = new ACHEEntities())
            {
                var rubros = dbContext.Rubros.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDRubroPadre == null).OrderBy(x => x.IDRubro).ToList();
                foreach (var item in rubros)
                    ddlRubros.Items.Add(new ListItem(item.Nombre, item.IDRubro.ToString()));


            }

        }
    }

    public class ListaSubRubro
    {
        public string IDSubRubro { get; set; }
        public string Nombre { get; set; }

    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static List<ListaSubRubro> ddlSubRubro(int idRubro)
    {
        try
        {
            var resultado = new List<ListaSubRubro>();
            
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    resultado = dbContext.Rubros.Where(x => x.IDUsuario == usu.IDUsuario && x.IDRubroPadre.HasValue && x.IDRubroPadre == idRubro)
                        .OrderBy(x => x.Nombre).ToList().Select(x => new ListaSubRubro
                        {
                            IDSubRubro = x.IDRubro.ToString(),
                            Nombre = x.Nombre
                        }).ToList();

                    resultado.Insert(0, new ListaSubRubro() { IDSubRubro = "0", Nombre = "" });

                }

                return resultado;
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptRnkViewModel getResults(string tipo, string estado, string fechaDesde, string fechaHasta, string idRubro, string idSubRubro, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];//
                string dtHasta = "";
                string dtDesde = "";
                //string fTipo = "";
                //string fEstado = "";
                //string fidRubro = "";
                //string fidSubRubro = "";

                if (fechaDesde != string.Empty)
                {
                    //DateTime dtDesde = DateTime.Parse(fechaDesde);
                    //results = results.Where(x => x.Fecha >= dtDesde);
                    dtDesde = DateTime.Parse(fechaDesde).ToString(formato);//
                }
                if (fechaHasta != string.Empty)
                {
                    //DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                    //results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    dtHasta = DateTime.Parse(fechaHasta).ToString(formato) + " 23:59:59 pm";//
                }

                using (var dbContext = new ACHEEntities())
                {
                    var resultados = dbContext.Database.SqlQuery<RptRnkViewModelItems>("exec RptRankingConceptos "
                        + usu.IDUsuario + ",'"
                        + dtDesde + "', '"
                        + dtHasta + "', '"
                        + idRubro + "', '"
                        + idSubRubro + "', '"
                        + estado + "', '"
                        + tipo + "'"
                        , new object[] { }).AsQueryable();

                    page--;
                    ResultadosRptRnkViewModel resultado = new ResultadosRptRnkViewModel();

                    ////hacer restas en vistas RptRankingConceptos como [RptRentabilidad]
                    resultado.TotalPage = ((resultados.Where(x => x.Importe > 0).Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = resultados.Where(x => x.Importe > 0).Count();

                    var list = resultados.Where(x => x.Importe > 0).OrderByDescending(x => x.Importe).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new RptRnkViewModel()
                        {
                            Valor1 = x.Descripcion,
                            Valor2 = x.Codigo,
                            Rubro = x.Rubro,
                            SubRubro = x.SubRubro != "" ? x.SubRubro : "-",
                            Cantidad = x.Cantidad.ToString("n0"),
                            Total = x.Importe.ToMoneyFormat(2),
                            TipoConcepto = x.Tipo == "P" ? "Producto" : x.Tipo == "S" ? "Servicio" : "Combo"
                        });
                    resultado.Items = list.ToList();


                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    /*private static IList<RptRankingConceptos> RestarCantidadNC(IList<RptRankingConceptos> listaNC, IList<RptRankingConceptos> resultados)
    {
        foreach (var nc in listaNC)
        {
            foreach (var item in resultados)
            {
                if (item.Codigo == nc.Codigo)
                {
                    item.Cantidad -= nc.Cantidad;
                    item.Importe -= nc.Importe;
                    break;
                }
            }
        }
        return resultados;
    }*/

    [WebMethod(true)]
    public static string export(string tipo, string estado, string fechaDesde, string fechaHasta, string idRubro, string idSubRubro)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "RankingProductos_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];//
                    string dtHasta = "";
                    string dtDesde = "";

                    if (fechaDesde != string.Empty)
                    {
                        //DateTime dtDesde = DateTime.Parse(fechaDesde);
                        //results = results.Where(x => x.Fecha >= dtDesde);
                        dtDesde = DateTime.Parse(fechaDesde).ToString(formato);//
                    }
                    if (fechaHasta != string.Empty)
                    {
                        //DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        //results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                        dtHasta = DateTime.Parse(fechaHasta).ToString(formato) + " 23:59:59 pm";//
                    }

                    //var results = dbContext.RptRankingConceptos.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    var resultados = dbContext.Database.SqlQuery<RptRnkViewModelItems>("exec RptRankingConceptos "
                        + usu.IDUsuario + ",'"
                        + dtDesde + "', '"
                        + dtHasta + "', '"
                        + idRubro + "', '"
                        + idSubRubro + "', '"
                        + estado + "', '"
                        + tipo + "'"
                        , new object[] { }).AsQueryable();

                    dt = resultados.Where(x => x.Importe > 0).OrderByDescending(x => x.Importe).Select(x => new
                    {
                        Tipo = x.Tipo == "P" ? "Producto" : x.Tipo == "S" ? "Servicio" : "Combo",
                        Codigo = x.Codigo,
                        Concepto = x.Descripcion,
                        Rubro = x.Rubro,
                        SubRubro = x.SubRubro != "" ? x.SubRubro : "-",
                        Cantidad = x.Cantidad.ToString("n0"),
                        Total = x.Importe.ToMoneyFormat(2)
                    }).ToList().ToDataTable();
                }


                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
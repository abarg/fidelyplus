﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="iva-ventas.aspx.cs" Inherits="modulos_reportes_iva_ventas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>IVA Ventas</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Reportes</a></li>
                <li class="active">IVA Ventas</li>
            </ol>
        </div>
    </div>
    <div id="divConDatos" runat="server">
    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>
                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <div class="col-sm-12" style="padding-left: inherit">
                                  <div class="col-sm-8 col-md-8">
                                    <div class="row">

                                        <div class="col-sm-6 col-md-6 form-group">
                                            <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Desde" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-3 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Hasta" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 responsive-buttons md">
                                    <a class="btn btn-black" onclick="resetearPagina();filtrar();">Buscar</a>
                                </div>

                            </div>

                        </form>

                        <div class="col-sm-12"><hr class="mt0" /></div>

                        <div class="row">
                            <div class="pull-right">
                                <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                <a href="" id="lnkDownload" onclick="resetearExportacion();" download="IvaVentas" style="display: none">Descargar</a>
                                <div class="btn-group mr10 dropdown" id="divExportar" runat="server">
                                    <button type="button" class="btn btn-btn-default"><i class="fa fa-tasks"></i> Exportar</button>
                                    <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:exportar();" >A Excel</a></li>
                                        <li><a href="javascript:exportarPDF();">A PDF</a></li>
                                                
                                    </ul>
                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Razón Social</th>
                                        <th>CUIT</th>
                                        <th>Cond. IVA</th>
                                        <th>Nro Factura</th>
                                        <th style="text-align: right">Imp. Neto Grav.</th>
                                        <th style="text-align: right">Imp. No Grav.</th>
                                        <th style="text-align: right">IVA 2,50</th>
                                        <th style="text-align: right">IVA 5,00</th>
                                        <th style="text-align: right">IVA 10,50</th>
                                        <th style="text-align: right">IVA 21,00</th>
                                        <th style="text-align: right">IVA 27,00</th>
                                        <th style="text-align: right">IIBB</th>
                                        <th style="text-align: right">Otros Imp.</th>
                                        <th style="text-align: right">Total IVA</th>
                                        <th style="text-align: right">Total Fact.</th>
                                        <th style="text-align: right">Total Serv.</th>
                                        <th style="text-align: right">Total Prod.</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <%--<div class="contentpanel">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <form runat="server" id="frmSearch">
                    <input type="hidden" id="hdnPage" runat="server" value="1" />

                    <h4 class="subtitle mb5">Filtros disponibles</h4>

                    <div class="mb20"></div>
                    <h4 class="subtitle mb5">Proveedor/Cliente</h4>
                    <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                        <option value=""></option>
                    </select>

                    <div class="mb20"></div>
                    <h4 class="subtitle mb5">Fecha</h4>
                    <div class="row row-pad-5">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                            </div>
                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                            </div>
                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                        </div>
                    </div>

                    <div class="mb20"></div>

                    <a class="btn btn-black" onclick="resetearPagina();filtrar();">Buscar</a>
                    <a class="btn btn-default" onclick="resetearPagina();verTodos();">Ver todos</a>

                    <br />
                </form>
            </div>
            <div class="col-sm-8 col-md-9 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-right">
                            <div class="btn-group mr10">
                                <div class="btn btn-white tooltips">
                                    <a id="divIconoDescargar" href="javascript:exportar();">
                                        <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                    </a>
                                    <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                    <a href="" id="lnkDownload" onclick="resetearExportacion();" download="IvaVentas" style="display: none">Descargar</a>
                                </div>
                                
                            </div>

                            <div class="btn-group mr10" id="divPagination" style="display: none">
                                <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                            </div>
                        </div>

                        <h4 class="panel-title">Resultados</h4>
                        <p id="msjResultados"></p>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body">

                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Razón Social</th>
                                        <th>CUIT</th>
                                        <th>Condición Iva</th>
                                        <th>Nro Factura</th>
                                        <th style="text-align: right">Imp Neto Grav</th>
                                        <th style="text-align: right">IVA 2,50</th>
                                        <th style="text-align: right">IVA 5,00</th>
                                        <th style="text-align: right">IVA 10,50</th>
                                        <th style="text-align: right">IVA 21,00</th>
                                        <th style="text-align: right">IVA 27,00</th>
                                        <th style="text-align: right">Total IVA</th>
                                        <th style="text-align: right">Total Fact</th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>--%>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td>${Fecha}</td>
            <td>${RazonSocial}</td>
            <td>${Cuit}</td>
            <td>${CondicionIVA}</td>
            <td>${NroFactura}</td>
            <td style="text-align: right">${Importe}</td>
            <td style="text-align: right">${NoGrav}</td>
            <td style="text-align: right">${IVA2}</td>
            <td style="text-align: right">${IVA5}</td>
            <td style="text-align: right">${IVA10}</td>
            <td style="text-align: right">${IVA21}</td>
            <td style="text-align: right">${IVA27}</td>
            <td style="text-align: right">${IIBB}</td>
            <td style="text-align: right">${OtrosImp}</td>
            <td style="text-align: right">${Iva}</td>
            <td style="text-align: right">${Total}</td>
            <td style="text-align: right">${Servicios}</td>
            <td style="text-align: right">${Productos}</td>
        </tr>
        {{/each}}
        <tr>
            <td class="bgTotal text-danger" colspan="5">TOTALES</td>
            
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalImporte"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalNoGrav"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA2"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA5"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA10"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA21"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIVA27"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIIBB"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalOtrosImp"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalIva"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalTotal"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalServicios"></span></td>
            <td class="bgTotal text-danger" style="text-align: right"><span id="spnTotalProductos"></span></td>
        </tr>    
        <tr>
            <td class="bgTotal text-danger" colspan="18">TOTALES POR CONDICION DE IVA</td>
        </tr>
        <tr>
            <td colspan="16">Responsables Incriptos: <span id="spnTotalRI"></span></td>
        </tr>
        <tr>
            <td colspan="16">Monotributistas: <span id="spnTotalMO"></span></td>
        </tr>
        <tr>
            <td colspan="16">Consumidores Finales: <span id="spnTotalCF"></span></td>
        </tr>
        <tr>
            <td colspan="16">Exentos: <span id="spnTotalEX"></span></td>
        </tr>
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="18">No se han encontrado resultados
            </td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/iva-ventas.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            filtrar();
            configFilters();
        });
    </script>
</asp:Content>


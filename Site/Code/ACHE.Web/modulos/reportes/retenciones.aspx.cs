﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity;

public partial class modulos_reportes_retenciones : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptRetencionesSufridasViewModel getResults(int idPersona, string fechaDesde, string fechaHasta, string tipo, string impuesto, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptRetenciones.Where(x => x.IDUsuario == usu.IDUsuario && x.Tipo == tipo && x.Impuesto == impuesto).AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    page--;
                    ResultadosRptRetencionesSufridasViewModel resultado = new ResultadosRptRetencionesSufridasViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new RptRetencionesSufridasViewModel()
                        {
                            Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                            RazonSocial = x.RazonSocial,
                            Cuit = x.CUIT,
                            CondicionIVA = x.CondicionIVA,
                            Jurisdiccion = x.Jurisdiccion,
                            NroReferencia = x.NroReferencia,
                            ImporteTotal = x.ImporteTotal.ToMoneyFormat(2),
                            Importe = x.Importe.ToMoneyFormat(2)
                        });
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export(int idPersona, string fechaDesde, string fechaHasta, string tipo, string impuesto)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Retenciones_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptRetenciones.Where(x => x.IDUsuario == usu.IDUsuario && x.Tipo == tipo && x.Impuesto == impuesto).AsQueryable();
                    if (idPersona > 0)
                        results = results.Where(x => x.IDPersona == idPersona);
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    if (impuesto.ToLower() == "iibb")
                    {
                        dt = results.OrderBy(x => x.Fecha).ToList().Select(x => new
                        {
                            FechaContable = x.Fecha.ToString("dd/MM/yyyy"),
                            RazonSocial = x.RazonSocial,
                            Cuit = x.CUIT,
                            CondicionIVA = x.CondicionIVA,
                            Jurisdiccion = x.Jurisdiccion,
                            NroCertificado = x.NroReferencia,
                            ImporteTotal = x.ImporteTotal,
                            Importe = x.Importe
                        }).ToList().ToDataTable();
                    }
                    else {
                        dt = results.OrderBy(x => x.Fecha).ToList().Select(x => new
                        {
                            FechaContable = x.Fecha.ToString("dd/MM/yyyy"),
                            RazonSocial = x.RazonSocial,
                            Cuit = x.CUIT,
                            CondicionIVA = x.CondicionIVA,
                            NroCertificado = x.NroReferencia,
                            ImporteTotal = x.ImporteTotal,
                            Importe = x.Importe
                        }).ToList().ToDataTable();
                    }
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
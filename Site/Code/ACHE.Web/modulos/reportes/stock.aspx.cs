﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;


public partial class modulos_reportes_stock : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(), CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptRnkViewModel getResults(string estado, int idInventario, string concepto, int page, int pageSize, bool agrupar)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptStock.Where(x => x.IDUsuario == usu.IDUsuario && (idInventario == 0 || x.IDInventario == idInventario)).AsQueryable();
                    if (estado != string.Empty)
                        results = results.Where(x => x.Estado == estado);
                    if (concepto.Trim() != string.Empty)
                        results = results.Where(x => x.Codigo.ToLower().Contains(concepto) || x.Nombre.ToLower().Contains(concepto));

                    page--;
                    ResultadosRptRnkViewModel resultado = new ResultadosRptRnkViewModel();
                    if (agrupar)
                    {
                        var resultsAgrupados = results.GroupBy(x => new { x.Codigo, x.Nombre, x.PrecioUnitario, x.CostoInterno }).ToList();
                        resultado.TotalPage = ((resultsAgrupados.Count() - 1) / pageSize) + 1;
                        resultado.TotalItems = resultsAgrupados.Count();

                        var aux = resultsAgrupados.Skip(page * pageSize).Take(pageSize).ToList()
                             .Select(g => new
                             {
                                 Codigo = g.Key.Codigo.ToUpper(),
                                 Nombre = g.Key.Nombre,
                                 Stock = g.Sum(x => x.Stock),
                                 Reserva = g.FirstOrDefault() != null ? g.FirstOrDefault().Reserva ?? 0 : 0,
                                 StockConReservas = g.Sum(x => x.Stock) - (g.FirstOrDefault() != null ? g.FirstOrDefault().Reserva ?? 0 : 0),
                                 CostoInterno = g.Key.CostoInterno,
                                 PrecioUnitario = g.Key.PrecioUnitario,
                                 Valor = g.Sum(x => x.PrecioUnitario * x.Stock),
                                 CostoInternoTotal = g.Sum(x => x.CostoInterno * x.Stock)
                             }).ToList().OrderByDescending(x => x.Valor);

                        var list = aux.Skip(page * pageSize).Take(pageSize).ToList()
                           .Select(x => new RptRnkViewModel()
                           {
                               Valor1 = x.Nombre,
                               Valor2 = x.Codigo,
                               Cantidad = x.Stock.ToMoneyFormat(2),
                               Cantidad2 = x.Reserva.ToMoneyFormat(2),
                               Cantidad3 = x.StockConReservas.ToMoneyFormat(2),
                               CostoInterno = x.CostoInterno.HasValue ? x.CostoInterno.Value.ToMoneyFormat(4) : "",
                               Precio = x.PrecioUnitario.ToMoneyFormat(2),
                               Total = x.Valor.ToMoneyFormat(2),
                               CostoTotal = x.CostoInternoTotal.HasValue ? x.CostoInternoTotal.Value.ToMoneyFormat(2) : "",
                           });
                        resultado.Items = list.ToList();

                    }
                    else
                    {
                        resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                        resultado.TotalItems = results.Count();

                        var aux = results.GroupBy(x => new { x.Codigo, x.Nombre, x.PrecioUnitario, x.CostoInterno, x.Inventario, x.DefectoReservas, x.Reserva })
                         .Select(g => new
                         {
                             Inventario = g.Key.Inventario,
                             Codigo = g.Key.Codigo.ToUpper(),
                             Nombre = g.Key.Nombre,
                             Stock = g.Sum(x => x.Stock),
                             //Reserva = g.Key.DefectoReservas && g.Key.Reserva.HasValue ? g.Key.Reserva : 0,
                             Reserva = g.Key.Reserva.HasValue ? g.Key.Reserva : 0,
                             //StockConReservas = g.Sum(x => x.Stock) - (g.Key.DefectoReservas && g.Key.Reserva.HasValue ? g.Key.Reserva : 0),
                             StockConReservas = g.Sum(x => x.Stock) - (g.Key.Reserva.HasValue ? g.Key.Reserva : 0),
                             CostoInterno = g.Key.CostoInterno,
                             PrecioUnitario = g.Key.PrecioUnitario,
                             Valor = g.Sum(x => x.PrecioUnitario * x.Stock),
                             CostoInternoTotal = g.Sum(x => x.CostoInterno * x.Stock)
                         }).ToList().OrderByDescending(x => x.Valor);

                        if (idInventario == 0)
                            aux = aux.OrderBy(x => x.Nombre).ThenByDescending(x => x.Valor);

                        var list = aux.Skip(page * pageSize).Take(pageSize).ToList()
                            .Select(x => new RptRnkViewModel()
                            {
                                Inventario = x.Inventario,
                                Valor1 = x.Nombre,
                                Valor2 = x.Codigo,
                                Cantidad = x.Stock.ToMoneyFormat(2),
                                Cantidad2 = x.Reserva.Value.ToMoneyFormat(2),
                                Cantidad3 = x.StockConReservas.Value.ToMoneyFormat(2),
                                CostoInterno = x.CostoInterno.HasValue ? x.CostoInterno.Value.ToMoneyFormat(2) : "",
                                Precio = x.PrecioUnitario.ToMoneyFormat(2),
                                Total = x.Valor.ToMoneyFormat(2),
                                CostoTotal = x.CostoInternoTotal.HasValue ? x.CostoInternoTotal.Value.ToMoneyFormat(2) : "",
                            });
                        resultado.Items = list.ToList();

                    }
                    // resultado.Items = list.ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string export(string estado, int idInventario, string concepto, bool agrupar)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "Stock_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptStock.Where(x => x.IDUsuario == usu.IDUsuario && (idInventario == 0 || x.IDInventario == idInventario)).AsQueryable();
                    if (estado != string.Empty)
                        results = results.Where(x => x.Estado == estado);
                    if (concepto.Trim() != string.Empty)
                        results = results.Where(x => x.Codigo.ToLower().Contains(concepto) || x.Nombre.ToLower().Contains(concepto));
                    if (agrupar)
                    {
                        var aux = results.GroupBy(x => new { x.Codigo, x.Nombre, x.PrecioUnitario, x.CostoInterno })
                         .Select(g => new
                         {
                             Codigo = g.Key.Codigo.ToUpper(),
                             Tipo = g.FirstOrDefault() != null ? g.FirstOrDefault().Tipo : "",
                             Nombre = g.Key.Nombre,
                             Stock = g.Sum(x => x.Stock),
                             Reserva = g.FirstOrDefault() != null ? g.FirstOrDefault().Reserva ?? 0 : 0,
                             StockConReservas = g.Sum(x => x.Stock) - (g.FirstOrDefault() != null ? g.FirstOrDefault().Reserva ?? 0 : 0),
                             CostoInterno = g.Key.CostoInterno,
                             PrecioUnitario = g.Key.PrecioUnitario,
                             Valor = g.Sum(x => x.PrecioUnitario * x.Stock),
                             CostoTotal = g.Sum(x => x.CostoInterno * x.Stock) ?? 0
                         }).ToList().OrderByDescending(x => x.Valor);


                        if (idInventario == 0)
                            aux = aux.OrderBy(x => x.Nombre).ThenByDescending(x => x.Valor);

                        dt = aux.ToList().Select(x => new
                        {
                            Tipo = x.Tipo,
                            Nombre = x.Nombre,
                            Codigo = x.Codigo,
                            Stock = x.Stock.ToString(),
                            Reserva = x.Reserva.ToMoneyFormat(4),
                            StockDisponible = x.StockConReservas.ToMoneyFormat(4),
                            CostoInterno = Convert.ToInt32(x.CostoInterno),
                            PrecioUnitario = x.PrecioUnitario,
                            Valor = x.Valor,
                            CostoTotal = x.CostoTotal
                        }).ToList().ToDataTable();

                    }
                    else
                    {
                        var aux = results.GroupBy(x => new { x.Codigo, x.Tipo, x.Nombre, x.PrecioUnitario, x.CostoInterno, x.Inventario, x.DefectoReservas, x.Reserva })
                         .Select(g => new
                         {
                             Inventario = g.Key.Inventario,
                             Tipo = g.Key.Tipo,
                             Codigo = g.Key.Codigo.ToUpper(),
                             Nombre = g.Key.Nombre,
                             Stock = g.Sum(x => x.Stock),
                             //Reserva = g.Key.DefectoReservas && g.Key.Reserva.HasValue ? g.Key.Reserva : 0,
                             Reserva = g.Key.Reserva.HasValue ? g.Key.Reserva : 0,
                             //StockConReservas = g.Sum(x => x.Stock) - (g.Key.DefectoReservas && g.Key.Reserva.HasValue ? g.Key.Reserva : 0),
                             StockConReservas = g.Sum(x => x.Stock) - (g.Key.Reserva.HasValue ? g.Key.Reserva : 0),
                             CostoInterno = g.Key.CostoInterno,
                             PrecioUnitario = g.Key.PrecioUnitario,
                             Valor = g.Sum(x => x.PrecioUnitario * x.Stock),
                             CostoTotal = g.Sum(x => x.CostoInterno * x.Stock) ?? 0
                         }).ToList().OrderByDescending(x => x.Valor);


                        if (idInventario == 0)
                            aux = aux.OrderBy(x => x.Nombre).ThenByDescending(x => x.Valor);

                        dt = aux.ToList().Select(x => new
                        {
                            Deposito = x.Inventario,
                            Tipo = x.Tipo,
                            Codigo = x.Codigo,
                            Nombre = x.Nombre,
                            Stock = x.Stock.ToString(),
                            Reserva = x.Reserva.Value.ToMoneyFormat(4),
                            StockDisponible = x.StockConReservas.Value.ToMoneyFormat(4),
                            CostoInterno = Convert.ToInt32(x.CostoInterno),
                            PrecioUnitario = x.PrecioUnitario,
                            Valor = x.Valor,
                            CostoTotal = x.CostoTotal
                        }).ToList().ToDataTable();
                    }
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
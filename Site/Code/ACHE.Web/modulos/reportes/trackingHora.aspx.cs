﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using ACHE.Extensions;
using System.Data.Entity;

public partial class modulos_reportes_trackingHora : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDesde.Text = DateTime.Now.GetFirstDayOfMonth().ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.TrackingHoras.Where(x => x.IDTrackingHoras == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.TrackingHoras.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosTrackingHorasViewModel getResults(int idPersona, string fechaDesde, string fechaHasta, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.TrackingHoras.Include("Personas").Include("UsuariosAdicionales").Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                    if (idPersona != 0)
                        results = results.Where(x => x.IDPersona == idPersona);

                    DateTime dtDesde = new DateTime();
                    DateTime dtHasta = new DateTime();

                    if (fechaDesde != "")
                    {
                        dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Inicio >= dtDesde);
                    }
                    if (fechaHasta != "")
                    {
                        dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Inicio) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    var resultsFinal = results.ToList();

                    page--;
                    ResultadosTrackingHorasViewModel resultado = new ResultadosTrackingHorasViewModel();
                    resultado.TotalPage = ((resultsFinal.GroupBy(x => new { x.IDUsuarioAdicional, x.IDPersona }).Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = resultsFinal.GroupBy(x => new { x.IDUsuarioAdicional, x.IDPersona }).Count();


                    var list = resultsFinal.OrderBy(x => x.Personas.RazonSocial).ToList().GroupBy(x => new { x.IDUsuarioAdicional, x.IDPersona }).Skip(page * pageSize).Take(pageSize)
                     .Select(x => new TrackingHorasViewModel()
                     {
                         RazonSocial = x.FirstOrDefault().Personas.RazonSocial,
                         NombreUsuario = (string.IsNullOrWhiteSpace(x.FirstOrDefault().IDUsuarioAdicional.ToString())) ? x.FirstOrDefault().Usuarios.RazonSocial : x.FirstOrDefault().UsuariosAdicionales.Email,
                         CantHorasFacturables = x.Where(z => z.Estado == "Facturable").Sum(y => y.Horas),
                         CantHorasNOFacturables = x.Where(z => z.Estado == "No Facturable").Sum(y => y.Horas),
                         SubTotal = x.Where(z => z.Estado == "Facturable").Sum(y => Convert.ToDecimal(y.Horas) * (y.ValorHora ?? 0))
                     });



                    resultado.Items = list.ToList();

                    return resultado;
                }

            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string export(int idPersona, string fechaDesde, string fechaHasta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "TrackingHoras_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.TrackingHoras.Include("Personas").Include("UsuariosAdicionales").Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                    if (idPersona != 0)
                        results = results.Where(x => x.IDPersona == idPersona);

                    DateTime dtDesde = new DateTime();
                    DateTime dtHasta = new DateTime();

                    if (fechaDesde != string.Empty)
                    {
                        dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Inicio >= dtDesde);//.OrderBy(x => x.Personas.RazonSocial);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Inicio) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    dt = results.OrderBy(x => x.Personas.RazonSocial).ToList().GroupBy(x => new { x.IDUsuarioAdicional, x.IDPersona }).Select(x => new
                    {
                        NombreCliente = x.FirstOrDefault().Personas.RazonSocial,
                        Usuario = (string.IsNullOrWhiteSpace(x.FirstOrDefault().IDUsuarioAdicional.ToString())) ? x.FirstOrDefault().Usuarios.RazonSocial : x.FirstOrDefault().UsuariosAdicionales.Email,
                        CantHorasFacturables = x.Where(z => z.Estado == "Facturable").Sum(y => y.Horas),
                        CantHorasNOFacturables = x.Where(z => z.Estado == "No Facturable").Sum(y => y.Horas),
                        Total = x.Where(z => z.Estado == "Facturable").Sum(y => Convert.ToDecimal(y.Horas) * (y.ValorHora ?? 0))
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    //////////


}
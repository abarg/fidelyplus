﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="saldosClientes.aspx.cs" Inherits="modulos_reportes_saldosClientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .modal.modal-wide .modal-dialog {
            width: 90%;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Saldos de clientes </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li>Reportes</li>
                <li class="active">Saldos de clientes</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                 <div class="alert alert-warning mb15" id="divAlert" runat="server" visible="false">
		            <asp:Literal runat="server" ID="litMsj"></asp:Literal>
	            </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <%--<div class="alert alert-info">
                            <strong>Beneficio EXCLUSIVO!</strong> 
                            <span>Gracias a nuestra alianza con <b>Vaxt Capital</b>, ahora podés acceder a financiamiento de forma rápida, sin fricciones y a una tasa preferencial. <a id="lnkCredility" runat="server" target="_blank">Más información</a></span>
                        </div>--%>

                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />
                            <input type="hidden" value="0" id="hdnTotalCant"  />
                            <div class="col-sm-12" style="padding-left: inherit">
                                    
                                <div class="col-sm-4 col-md-4 form-group">
                                    <select class="select2" data-placeholder="Seleccione un cliente/proveedor..." id="ddlPersona">
                                    </select>
                                </div>
                                    <%--<div class="col-sm-3 col-md-3 form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="Desde" MaxLength="10"></asp:TextBox>
                                        </div>
                                        <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                    </div>
                                    <div class="col-sm-3 col-md-3 form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="Hasta" MaxLength="10"></asp:TextBox>
                                        </div>
                                        <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                    </div>--%>
                                <div class="col-sm-5 col-md-4 form-group">
                                    <a class="btn btn-black" onclick="saldosClientes.resetearPagina();saldosClientes.filtrar();">Buscar</a>
                                    <a class="btn btn-default" onclick="saldosClientes.resetearPagina();saldosClientes.verTodos();">Ver todos</a>
                                </div>
                                
                            </div>

                            <!-- MODAL MSJ PRE GENERACION MAILS MASIVA -->
                            <div class="modal modal-wide fade" id="modalEnvioMasivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Envio de mail con detalle de saldo pendiente</h4>
                                        </div>
                                        <div class="modal-body" style="padding: 25px;padding-top:0px">
                                            <br />
                                            <div class="alert alert-success" id="divOkMail" style="display: none">
                                                <strong>Bien hecho! </strong>El mensaje ha sido enviado correctamente
                                            </div>
                                            <div class="alert alert-danger" id="divErrorMail" style="display: none">
                                                <strong>Lo sentimos! </strong><span id="msgErrorMail"></span>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Asunto:</label>
                                                <input type="text" id="txtMasivoAsunto" class="form-control required" runat="server" />
                                                <span id="msgErrorEnvioAsunto" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label"><span class="asterisk">*</span> Mensaje predefinido:</label>
                                                <textarea rows="5" id="txtMasivoMensaje" class="form-control required">Se consulta fecha de pago para la/s factura/s:</textarea>
                                                <small>Debajo del mensaje predefinido se mostrará el detalle de deuda actual de forma automática al enviar el mail.</small>
                                                <span id="msgErrorEnvioMensaje" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                            </div>
                                            <div id="divMsjPreMasivo" style="clear:left">
                        
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <a type="button" class="btn btn-success" onclick="saldosClientes.enviarMail();" id="btnEnviar">Enviar</a>
                                            <a style="margin-left: 20px" href="#"  data-dismiss="modal">Cancelar</a>
                    
                                        </div>
                                    </div>
                                </div>
                             </div>


                        </form>

                        <div class="col-sm-12"><hr class="mt0" /></div>
                        
                        <div class="row">
                            <div class="pull-right">
                                <div class="btn-group mr10" id="divExportar" runat="server">
                                    <div class="btn btn-white tooltips">
                                        <a id="divIconoDescargar" href="javascript:saldosClientes.exportar();">
                                            <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                        </a>
                                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                        <a href="" id="lnkDownload" onclick="saldosClientes.resetearExportacion();" download="saldosClientes" style="display: none">Descargar</a>
                                    </div>

                                </div>

                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="saldosClientes.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="saldosClientes.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>

                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body" id="divResultados">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th style="max-width: 20px;">
                                            <input type='checkbox' id='chkMasiva' />
                                        </th>
                                        <th>Cliente</th>
                                        <th>Documento</th>
                                        <th style="text-align: right">Saldo</th>
                                        <th style="text-align: right"></th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-white" onclick="saldosClientes.showModalMasivo();" id="lnkMasiva" style="display:none">
                            <i class="glyphicon glyphicon-envelope"></i> Enviar consulta de pago
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td>
                <input style="left: 40px;" type='checkbox' id='chkItem_${IDPersona}' class="chkItem" onchange="saldosClientes.actualizarTotales()"/>
            </td>
            <td style="min-width: 300px">${RazonSocial}</td>
            <td style="min-width: 145px">${NroDocumento}</td>
            <td style="text-align: right; font-weight:bold">${SaldoDesc}</td>
            <td style="text-align: right;"><a href="javascript:saldosClientes.verCC(${IDPersona},'${RazonSocial}')" style="cursor: pointer; font-size: 16px" title="Ver detalle"><i class="glyphicon glyphicon-search"></i></a>
        </tr>
        {{/each}}
    </script>
    
    <script id="resultTemplateTotales" type="text/x-jQuery-tmpl">
        {{each results}}
         <tr>
            <td colspan="3" class="bgTotal"></td>
            <td class="bgTotal text-danger" style="text-align:right">Total: ${TotalGeneral}</td>
            <td class="bgTotal"></td>
        </tr>
       {{/each}}
    </script>
    
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="5">No se han encontrado resultados</td>
        </tr>
    </script>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalDetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="titDetalle">Cuenta corriente</h4>
                    <p id="modalLinea1" style="margin-bottom:0"></p>
                    <p id="modalLinea2"></p>
                </div>
                <div class="modal-body">
                    <div class="pull-right">   
                        <img alt="" src="/images/loaders/loader1.gif" id="imgLoadingModal" style="display:none" />
                        <a href="" id="lnkDownloadModal" onclick="ExportarCC.resetearExportacion();" download="CuentaCorriente" style="display:none">Descargar</a>
                        <div class="btn-group mr10  dropdown" id="divExportarModal" runat="server">
                            <button type="button" class="btn btn-btn-default"><i class="fa fa-tasks"></i> Exportar</button>
                            <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                            <ul class="dropdown-menu" style="min-width: 130px;">
                                <li><a href="javascript:ExportarCC.exportar();" >A Excel</a></li>
                                <li><a href="javascript:ExportarCC.exportarPDF();">A PDF</a></li>      
                            </ul>
                        </div>
                        <input hidden id="hdnIDPersona" value="" />     
                        <input hidden id="hdnTipoPersonaExportar" value="1" />
                    </div>

                    <div class="table-responsive">
                        <table class="table table-success mb30 smallTable">
                            <thead>
                                <tr>
                                    <th>Comprobante</th>
                                    <th>Fecha</th>
                                    <th>Comprobante aplicado</th>
                                    <th>Fecha Cobro</th>
                                    <th>Importe</th>
                                    <th>Cobrado</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody id="bodyDetalle">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left:20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplateDetalle" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            {{if $value.Cobrado == "SaldoAnterior"}}
                <td class="bgTotal text-danger">
                    Saldo a la fecha
                </td>
                <td class="bgTotal" colspan="5">&nbsp;</td>
                <td class="bgTotal text-danger">
                    ${Total}
                </td>
            {{else}}

                {{if $value.Cobrado != "Saldo"}}//Si no es la ultima columna, muestro todo normal. Solo por cuestiones estéticas.
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Comprobante}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Fecha}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${ComprobanteAplicado}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${FechaCobro}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Importe}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Cobrado}
                    </td>
                    <td {{if $value.Comprobante != ""}} class="bgRow" {{/if}}>
                        ${Total}
                    </td>
                {{else}}
                    <td class="bgTotal" colspan="5">&nbsp;</td>
                    <td class="bgTotal text-danger">
                        ${Cobrado}
                    </td>
                    <td class="bgTotal text-danger">
                        <span id="spnTotal">${Total}</span>
                    </td>
                {{/if}}
            {{/if}}
        </tr>
        {{/each}}
    </script>

    <script id="noResultTemplateDetalle" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="7">No se han encontrado resultados
            </td>
        </tr>
    </script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/reportes/saldosClientes.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            saldosClientes.filtrar(true);
            saldosClientes.configFilters();
        });
    </script>

</asp:Content>




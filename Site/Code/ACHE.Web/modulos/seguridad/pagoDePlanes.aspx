﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="pagoDePlanes.aspx.cs" Inherits="modulos_ventas_pagoDePlanes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="/css/jasny-bootstrap.min.css" rel="stylesheet" />

    <style>
        .activo {
            background-color: #e4e7ea;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-money'></i>Pago del plan</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/modulos/seguridad/mis-datos.aspx">Mi cuenta</a></li>
                <li class="active">Pago del plan</li>
            </ol>
        </div>
    </div>


    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>
                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>
                        <div class="alert alert-success" id="divOkDA" style="display: none">
                            <strong>Bien hecho! </strong>Enviaste una solicitud para adherir el plan al débito automático.
                        </div>
                        <%-- Datos principales--%>
                        <div class="row mb15" id="divImportes">
                            <div class="col-sm-12">
                                <h3>Confirmar el pago de tu <b>Plan <span style="color: #bf315f" id="planNombre"></span></b>&nbsp;&nbsp;<a href="javascript:history.back();" style="font-size: 14px; text-decoration: underline">modificar plan</a></h3>
                                <h4 class="text-success">El importe a pagar es <b>$<asp:Literal ID="litPlanImporte" runat="server"></asp:Literal></b></h4>
                            </div>
                        </div>

                        <div class="row mb15" id="divMetodos">
                            <div class="col-sm-12">
                                <h3>1 - Seleccione el método de pago</h3>
                            </div>
                        </div>
                        <div class="row mb15">

                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12 panelTipoPago" id="divPanelDebito">
                                        <div class="panel-item">
                                            <div class="col-sm-12">
                                                <h4 class="fm-title">
                                                   <input value="Debito" name="ctl00$MainContent$chkForma" type="radio" id="rDebito" onclick="PlanesPagos.changeFormaDePago();">
                                                   <label id="lblDebito" for="rDebito" style="color: inherit;">Débito automático con CBU<span style="margin-right: 10px;"></span> <span class="label label-warning">5% OFF</span></label>
                                                </h4>
                                                <span class="descripcionPago">Renovación automática</span>
                                                <span class="descripcionPago" style="font-size: 14px; color: #d3eeeb; margin-top: 8px; margin-bottom: 30px;">Adherite y asegurá tener tu cuenta siempre activa.</b></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-3 panelTipoPago" id="divPanelEfectivo">
                                        <div class="panel-item">
                                            <div class="col-sm-12">
                                                <h4 class="fm-title">
                                                    <input type="radio" name="chkForma" runat="server" id="rEfectivo" value="Mercado Pago" onclick="PlanesPagos.changeFormaDePago();" />
                                                    <label id="lblMP" for="rEfectivo">Pago en Efectivo</label>
                                                </h4>
                                                 <span class="descripcionPago">Tiempo de acreditación: <br />3 días hábiles</span>
                                            </div>
                                            <div class="col-sm-12 panelTarjetas" onclick="$('#rEfectivo').click();PlanesPagos.changeFormaDePago();" style="cursor:pointer">
                                                <img src="../../images/pagos/opcion1.png"  style="width: auto;"/>
                                            </div>
                                        </div>
                                    </div>

                                    <%--<div class="col-sm-6 col-lg-3 panelTipoPago" id="divPanelTarjeta">
                                        <div class="panel-item">
                                            <div class="col-sm-12">
                                                <h4 class="fm-title">
                                                    <input type="radio" name="chkForma" runat="server" id="rTarjetas" value="Mercado Pago" onclick="PlanesPagos.changeFormaDePago();" />
                                                    <label id="lblMP" for="rTarjetas">Tarjetas de crédito</label>
                                                </h4>
                                                 <span class="descripcionPago">Acreditación inmediata, 100% online, comienza ya!!</span>
                                            </div>
                                            <div class="col-sm-12 panelTarjetas">
                                                <img src="../../images/pagos/opcion2.png" style="width: auto;" />
                                            </div>
                                        </div>
                                    </div>--%>

                                    <div class="col-sm-6 col-lg-3 panelTipoPago" id="divPanelMercadoPago">
                                        <div class="panel-item">
                                            <div class="col-sm-12">
                                                <h4 class="fm-title">
                                                    <input type="radio" name="chkForma" runat="server" id="rMP" value="Mercado Pago" onclick="PlanesPagos.changeFormaDePago();" />
                                                    <label id="lblMP" for="rMP">Mercado Pago</label>
                                                </h4>
                                                 <span class="descripcionPago">Acreditación inmediata. Puedes usar crédito de tu cuenta para pagar</span>
                                            </div>
                                            <div class="col-sm-12 panelTarjetas" onclick="$('#rMP').click();PlanesPagos.changeFormaDePago();" style="cursor:pointer">
                                                <img src="../../images/pagos/MercadoPago.png"  style="width: auto;"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-3 panelTipoPago" id="divPanelPayU">
                                        <div class="panel-item">
                                            <div class="col-sm-12">
                                                <h4 class="fm-title">
                                                    <input type="radio" name="chkForma" runat="server" id="rPayU" value="PayU" onclick="PlanesPagos.changeFormaDePago();" />
                                                    <label id="lblMP" for="rPayU">PayU</label>
                                                </h4>
                                                <span class="descripcionPago">Acreditación inmediata. Puedes usar crédito de tu cuenta para pagar</span>
                                            </div>
                                            <div class="col-sm-12 panelTarjetas" onclick="$('#rPayU').click();PlanesPagos.changeFormaDePago();" style="cursor:pointer">
                                                <img src="../../images/pagos/logo_payu.png" style="width: auto;"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-3 panelTipoPago" id="divPanelTransferencia">
                                        <div class="panel-item">
                                            <div class="col-sm-12">
                                                <h4 class="fm-title">
                                                    <input type="radio" name="chkForma" runat="server" id="rTransferencia" value="Transferencia" onclick="PlanesPagos.changeFormaDePago();" />
                                                    <label id="lblMP" for="rTransferencia">Transferencia</label>
                                                </h4>
                                                <span class="descripcionPago">Tiempo de acreditación: 24/48 horas hábiles.</span>
                                                <div class="col-sm-12 panelTarjetas" onclick="$('#rTransferencia').click();PlanesPagos.changeFormaDePago();" style="cursor:pointer">
                                                    <img src="../../images/pagos/homebanking.png" style="width: auto;"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <div class="row">

                                    <%-- MercadoPago--%>
                                    <div id="divMercadoPago" style="display: none">
                                        <div class="col-sm-12">
                                            <hr />
                                            <h3>2 - Realiza el pago</h3>
                                            <p>Haga click en pagar y siga los pasos que le brinda MercadoPago</label>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <br />
                                                <asp:Literal ID="btnMercadoPago" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    
                                    </div>

                                    <div id="divPayU" style="display: none">
                                        <div class="col-sm-12">
                                            <hr />
                                            <h3>2 - Realiza el pago</h3>
                                            <p>Haga click en pagar y siga los pasos que le brinda PayU</label>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <br />
                                                <a class='btn btn-success' id='lnkComprarPayU' href='javascript: document.formPayU.submit();'>Pagar</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="divDebito" style="display:none;margin-left: 15px;">
                                        <div class="row mb15">
                                            <div class="col-sm-6 col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Número de CBU</label>
                                                    <asp:TextBox runat="server" ID="txtNroCBU" CssClass="form-control  required" MaxLength="22" placeholder="" TabIndex="11"></asp:TextBox>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row mb15">
                                            <div class="col-sm-6 col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Repetir número de CBU</label>
                                                    <asp:TextBox runat="server" ID="txtNroCBUConfirmar" CssClass="form-control  required" MaxLength="22" placeholder="" TabIndex="11"></asp:TextBox>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row mb15">
                                            <div class="col-sm-12">
                                                <a class="btn btn-success" id="btnActualizarDA" onclick="PlanesPagos.adherirseDA();">Adherirse</a>
                                                <span id="msjErrorDA" style="color:#a94442"></span>
                                            
                                            </div>
                                        </div>
                                    </div>


                                    <%-- Transferencia --%>
                                    <div id="divTransferencia" style="display: none">
                                        <div class="col-sm-12">
                                            <hr />
                                            <h3>2 - Ingresa los datos de la transferencia </h3>
                                            <p>Ingresa la siguiente información al momento de realizar la transferencia y luego adjunta el comprobante de transferencia emitido por la entidad bancaria.</label>
                                            <p>
                                                Razón Social: <b>Contabilium SA</b><br />
                                                CUIT: <b>30-71486426-9</b><br />
                                                Cuenta corriente en pesos : <b>097-011107/0 (Santander Río)</b><br />
                                                CBU: <b>07200977-20000001110704</b><br />
                                                Concepto: <b><span id="spTransfNombre"></span></b>
                                                <br />
                                                Importe: <b>AR$ <span id="spTransfImporte"></span></b>
                                                <br />
                                            </p>
                                            
                                            <div class="alert alert-danger" id="divErrorTransf" style="display: none; margin-top: 20px;">
                                                <strong>Lo sentimos! </strong><span id="msgErrorTransf"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12" style="margin-top: 20px;">
                                        
                                            <div class="row mb15">
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><span class="asterisk">*</span> Importe Pagado</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">$</span>
                                                            <asp:TextBox runat="server" ID="txtImporte" CssClass="form-control required" MaxLength="8" TabIndex="10"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><span class="asterisk">*</span> Número de referencia</label>
                                                        <asp:TextBox runat="server" ID="txtNroReferencia" CssClass="form-control  required" MaxLength="128" placeholder="" TabIndex="11"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="row mb15">
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><span class="asterisk">*</span> Fecha de pago</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            <asp:TextBox runat="server" ID="txtFechaDePago" CssClass="form-control required validDate" placeholder="dd/mm/yyyy" MaxLength="10" TabIndex="12"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><span class="asterisk">*</span> Adjunta el comprobante de pago</label>
                                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                            <div class="form-control" data-trigger="fileinput" style="height: 40px">
                                                                <i class="glyphicon glyphicon-file fileinput-exists" id="iImgFileFoto"></i>
                                                                <span class="fileinput-filename"></span>
                                                            </div>
                                                            <span class="input-group-addon btn btn-default btn-file">
                                                                <span class="fileinput-new">
                                                                    <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Seleccionar
                                                                </span>
                                                                <span class="fileinput-exists">
                                                                    <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Modificar
                                                                </span>
                                                                <input id="flpArchivo" type="file" />
                                                            </span>
                                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>&nbsp;&nbsp;Remover
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <asp:HiddenField runat="server" ID="hdnFileName" Value="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-success" id="btnActualizar" onclick="PlanesPagos.grabarsinImagen();" style="display: none">Informar Pago</a>
                        <%--<a href="#" onclick="PlanesPagos.cancelar();" style="margin-left: 20px">Cancelar</a>--%>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdnIdPlan" Value="0" />
                <asp:HiddenField runat="server" ID="hdnImporteTotal" Value="0" />
                <asp:HiddenField runat="server" ID="hdnNombrePlan" Value="0" />
                <asp:HiddenField runat="server" ID="hdnModo" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTieneFoto" Value="0" />
                <asp:HiddenField runat="server" ID="hdnSinCombioDeFoto" Value="0" />
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnGuardarFoto" Value="0" />
            </form>

            <form method="post" action="https://gateway.payulatam.com/ppp-web-gateway" name="formPayU">
                <asp:Literal runat="server" ID="litPayU"></asp:Literal>

            </form>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/seguridad/PlanesPagos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script src="/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="/js/jasny-bootstrap.min.js"></script>
    <script src="/js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="/js/jquery.iframe-transport.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            PlanesPagos.configForm();
        });
    </script>
</asp:Content>



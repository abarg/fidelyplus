﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="mis-datos.aspx.cs" Inherits="mis_datos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script src="/js/htmleditor/tinymce.min.js"></script>
 
    
    <style>
        #tabsPerfil.nav-tabs > li.active > a, #tabsPerfil.nav-tabs > li.active > a:hover, #tabsPerfil.nav-tabs > li.active > a:focus {
            color: #bf315f;
            text-decoration: none !important;
        }

        #tabsPerfil.nav-tabs.nav-justified > li > a {
            border-bottom: 0;
            text-decoration: underline;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2 id="spanMisDatos"><i class="fa fa-user"></i>Mi cuenta</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li class="active"><span id="spanMisDatosUbicacion">Mi cuenta</span></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <div class="col-sm-12" id="divDatosPersonales">
                <!-- profile-header -->
                <div class="alert alert-danger" id="divError" style="display: none">
                    <strong>Lo sentimos! </strong><span id="msgError"></span>
                </div>
                <div class="alert alert-success" id="divOk" style="display: none">
                    <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                </div>

                <ul class="nav nav-tabs nav-justified nav-profile">

                    <li id="liPerfilUsuario" class="active" runat="server">
                        <a href="#PerfilUsuario" data-toggle="tab" style="text-align: left;">
                            <span class="fa fa-smile-o" style="font-size: 18px;"></span>
                            <strong>PERFIL Y USUARIOS</strong><br />
                            <small>Información de tu empresa y usuarios del sistema</small>
                        </a>
                    </li>
                    <li id="liplanPagos" runat="server">
                        <a href="#planPagos" data-toggle="tab" style="text-align: left;">
                            <span class="fa fa-credit-card" style="font-size: 18px;"></span>
                            <strong>PLAN Y PAGOS</strong><br />
                            <small>Información sobre tu plan y los pagos realizados</small>
                        </a>
                    </li>
                    <li id="liEmpresas" runat="server">
                        <a href="#Empresas" data-toggle="tab" style="text-align: left;">
                            <span class="fa fa-briefcase" style="font-size: 18px;"></span>
                            <strong>EMPRESAS</strong><br />
                            <small>Administre los CUIT con los que desea operar</small>
                        </a>
                    </li>
                    <li id="liAlertasyAvisos" runat="server">
                        <a href="#AlertasyAvisos" data-toggle="tab" style="text-align: left;">
                            <span class="fa fa-bell-o" style="font-size: 18px;"></span>
                            <strong>Alertas y avisos</strong><br />
                            <small>Configure los envíos automáticos que desea programar</small>
                        </a>
                    </li>
                    
                </ul>
                <form id="frmEdicion" runat="server" clientidmode="Static">
                    <div class="tab-content" style="padding:0">
                        <div class="tab-pane active" id="PerfilUsuario" runat="server">
                            <ul class="nav nav-tabs nav-justified nav-profile tab-content" id="tabsPerfil" style="padding-bottom: 0;">
                                <li id="liDatosPrincipales" class="active"><a href="#info" data-toggle="tab" style="text-align: left;"><strong>Datos principales</strong></a></li>
                                <li id="liDomicilio"><a href="#domicilio" data-toggle="tab" style="text-align: left;"><strong>Domicilio fiscal</strong></a></li>
                                <li id="liDatosFiscales"><a href="#datosFiscales" data-toggle="tab" style="text-align: left;"><strong>Datos impositivos</strong></a></li>
                                <li id="liportalClientes" runat="server"><a href="#portalClientes" data-toggle="tab" style="text-align: left;"><strong>Acceso clientes</strong></a></li>
                                <li id="liTemplate" runat="server"><a href="#template" data-toggle="tab" style="text-align: left;"><strong>Formato de factura</strong></a></li>
                                <%--<li id="liCambiarpwd"><a href="/modulos/seguridad/cambiar-pwd.aspx" style="text-align: left;"><strong>Cambiar contraseña</strong></a></li>--%>
                                <%--<li id="Usuarios"><a href="/modulos/seguridad/usuarios.aspx" style="text-align: left;"><strong>Usuarios del sistema</strong></a></li>--%>
                                <li id="liPlandeCuentas" runat="server"><a href="#plandeCuentas" data-toggle="tab" style="text-align: left;"><strong>Plan de cuenta</strong></a></li>
                                <li id="liConfiguracion"><a href="#configuracion" data-toggle="tab" style="text-align: left;"><strong>Configuraciones base</strong></a></li>
                            </ul>

                            <div class="tab-content" style="padding:0">
                                <div class="tab-pane active" id="info">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2" id="divFotoPerfil">
                                                    <img id="imgLogo" src="/files/usuarios/no-photo.png" class="thumbnail img-responsive" alt="" runat="server" />
                                                    <div class="mb30"></div>

                                                    <div id="divLogo" style="display: none">
                                                        <p class="mb30">Formato JPG, PNG o GIF. Tamaño máximo recomendado: 200x60px</p>
                                                        <input type="hidden" value="" name="" /><input type="file" id="flpArchivo" />
                                                        <div class="mb20"></div>
                                                    </div>

                                                    <div class="col-sm-12" id="divAdjuntarFoto">
                                                        <a class="btn btn-white btn-block" onclick="foto.showInputLogo();">Adjuntar foto</a>
                                                    </div>
                                                    <div class="col-sm-12" id="divEliminarFoto">
                                                        <a class="btn btn-white btn-block" onclick="foto.eliminarLogo();">Eliminar foto</a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-9">
                                                    <div class="row mb15">
                                                        <div class="col-sm-12">
                                                            <h3>Datos principales </h3>
                                                            <label class="control-label">Estos son los datos básicos y obligatorios que el sistema necesita para poder facturar.</label>
                                                        </div>
                                                    </div>
                                                    <div class="row mb15">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label"><code>*</code> Razón Social</label>
                                                                <asp:TextBox runat="server" ID="txtRazonSocial" CssClass="form-control required" MaxLength="128"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label"><code>*</code> Categoría Impositiva</label>
                                                                <asp:DropDownList runat="server" ID="ddlCondicionIva" CssClass="form-control required">
                                                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                                                    <asp:ListItem Text="Exento" Value="EX"></asp:ListItem>
                                                                    <asp:ListItem Text="Responsable Inscripto" Value="RI"></asp:ListItem>
                                                                    <asp:ListItem Text="Responsable no inscripto" Value="NI"></asp:ListItem>
                                                                    <asp:ListItem Text="Monotributista" Value="MO"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb15">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label"><code>*</code> CUIT/CUIL</label>
                                                                <asp:TextBox runat="server" ID="txtCuit" CssClass="form-control required number validCuitMisDatos" MaxLength="13"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label"><code>*</code> E-mail</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">@</span>
                                                                    <asp:TextBox runat="server" ID="txtEmail" TextMode="Email" Enabled="false" CssClass="form-control required" MaxLength="128"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb15">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label"><code>*</code> Email desde</label>
                                                                <asp:TextBox runat="server" ID="txtEmailFrom" CssClass="form-control required" MaxLength="200"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6"></div>
                                                    </div>
                                                    <div class="row mb15">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Inicio de actividades</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                    <asp:TextBox runat="server" ID="txtFechaInicioAct" CssClass="form-control validDate" MaxLength="10"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label"><code>*</code> Personería</label>
                                                                <asp:DropDownList runat="server" ID="ddlPersoneria" CssClass="form-control">
                                                                    <asp:ListItem Text="Física" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="Jurídica" Value="J"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb15">
                                                        <div class="col-sm-12">
                                                            <h3>Datos generales </h3>
                                                            <label class="control-label">Estos datos no son necesarios para facturar, pero podrían serte de utilidad ;-)</label>
                                                        </div>
                                                    </div>

                                                    <div class="row mb15">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label">IIBB</label>
                                                                <asp:CheckBox runat="server" ID="chkExento" CssClass="form-control" Text="&nbsp;Soy exento" onclick="MisDatos.changeIIBB();" Checked="false" Style="border: 0; background-color: transparent;"></asp:CheckBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Número</label>
                                                                <asp:TextBox runat="server" ID="txtIIBB" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">E-mail alertas</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">@</span>
                                                                    <asp:TextBox runat="server" ID="txtEmailAlertas" CssClass="form-control multiemails" MaxLength="128"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="row mb15">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Contacto</label>
                                                                <asp:TextBox runat="server" ID="txtContacto" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Celular</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                                                    <asp:TextBox runat="server" ID="txtCelular" TextMode="Phone" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a class="btn btn-success" id="btnActualizarInfo" onclick="MisDatos.grabar();">Actualizar</a>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="domicilio">

                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row mb15">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><span class="asterisk">*</span> Provincia</label>
                                                        <asp:DropDownList runat="server" ID="ddlProvincia" CssClass="select2 required"
                                                            data-placeholder="Selecciona una provincia..." onchange="MisDatos.changeProvincia();">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><span class="asterisk">*</span> Ciudad/Localidad</label>
                                                        <asp:DropDownList runat="server" ID="ddlCiudad" CssClass="select2 required"
                                                            data-placeholder="Selecciona una ciudad...">
                                                        </asp:DropDownList>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><span class="asterisk">*</span> Domicilio</label>
                                                        <asp:TextBox runat="server" ID="txtDomicilio" CssClass="form-control required" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Piso/Depto</label>
                                                        <asp:TextBox runat="server" ID="txtPisoDepto" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Código Postal</label>
                                                        <asp:TextBox runat="server" ID="txtCp" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Teléfono</label>
                                                        <div class="input-group" style="margin-top: -10px;">
                                                            <asp:TextBox runat="server" ID="txtArea" CssClass="form-control required" MaxLength="5" MinLength="2" Style="margin-top: 10px;margin-right: 5%;width: 30%; float:left" placeholder="Área"></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control required" MaxLength="15" MinLength="6" Style="margin-top: 10px;width: 65%; float:left" placeholder="Teléfono"></asp:TextBox>
                                                        </div>
                                                        <%--<div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                                            <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control" MaxLength="50" placeholder="(0011) 1234-5678"></asp:TextBox>
                                                        </div>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a class="btn btn-success" id="btnActualizarDomicilio" onclick="MisDatos.grabar();">Actualizar</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="portalClientes">

                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h3>¿Querés que tus clientes accedan a un portal para descargar las facturas que les emitís? (VÁLIDO para plan PYME en adelante)</h3>
                                            <p class="mb30">
                                                Seleccionando "Habilitar acceso de clientes" tus clientes podrán ingresar a <a href="http://clientes.contabilium.com" target="_blank">http://clientes.contabilium.com</a> y descargar todas sus facturas de forma online.
                                            </p>

                                            <div class="row mb15">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <asp:CheckBox runat="server" ID="chkPortalClientes"></asp:CheckBox><label class="control-label"> &nbsp;Habilitar acceso de clientes</label>
                                                    </div>

                                                    <div class="form-group hide">
                                                        <asp:CheckBox runat="server" ID="ChkCorreoPortal"></asp:CheckBox><label class="control-label"> &nbsp;Enviar mail automáticamente al emitirles una factura</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <hr />
                                                <h3>¿Querés cobrarle a tus clientes las facturas que les emitís a través de MercadoPago?</h3>
                                                <p>Sigue los siguientes pasos:</p>
                                                <br />
                                                <p>
                                                    1.- Ingresa a <a href="https://www.mercadopago.com/mla/herramientas/aplicaciones" target="_blank">www.mercadopago.com/mla/herramientas/aplicaciones</a>
                                                </p>
                                                <p>
                                                    2.- Completa el usuario y contraseña de Mercado Pago en caso de ser necesario.
                                                </p>
                                                <p>
                                                    3.- Copia los valores de Client_id y Client_secret a los campos correspondientes.
                                                </p>

                                                <div>
                                                    <div class="row">
                                                        <div class="col-sm-9 col-md-5 col-lg-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Client ID</label>
                                                                <asp:TextBox runat="server" ID="txtClientId" CssClass="form-control" MaxLength="150"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-9 col-md-5 col-lg-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Client Secret</label>
                                                                <asp:TextBox runat="server" ID="txtClientSecret" CssClass="form-control" MaxLength="150"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <p>
                                                    4 - Ingresa a <a href="https://www.mercadopago.com/mla/herramientas/notificaciones" target="_blank">www.mercadopago.com/mla/herramientas/notificaciones</a> y completa el campo Url con el siguiente valor: <strong style="color: #000">
                                                        <asp:Literal ID="liUrlMercadoPago" runat="server"></asp:Literal></strong>
                                                </p>
                                                <p>
                                                    5 - Seleccione la opción <i><b>"payment"</b></i>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="panel-footer">
                                            <a class="btn btn-success" id="btnActualizarPortalClientes" onclick="MisDatos.portalClientes();">Actualizar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="template">
                                    <div class="panel-body">
                                        <h3>Tu imagen es todo</h3>
                                        <p>Elegí el diseño de factura que querés emitir para que se adapte a la imagen de tu empresa.</p>

                                        <div class="row mb15">
                                            <div class="row filemanager">
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/1.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default1" value="default" checked="true" />
                                                            <label for="default1">&nbsp;Default</label></h5>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/2.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default2" value="amarillo" />
                                                            <label for="default2">&nbsp;Amarillo</label></h5>

                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/3.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default3" value="celeste" />
                                                            <label for="default3">&nbsp;Celeste</label></h5>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-4 col-md-2 hide">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/4.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default4" value="negro" />
                                                            <label for="default4">&nbsp;Negro</label></h5>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/7.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default7" value="blanco" />
                                                            <label for="default7">&nbsp;Blanco c/gris</label></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row filemanager">
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/5.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default5" value="rojo" />
                                                            <label for="default5">&nbsp;Rojo</label></h5>

                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/6.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default6" value="verde" />
                                                            <label for="default6">&nbsp;Verde</label></h5>
                                                    </div>
                                                </div>
                                                  <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <div class="thmb">
                                                        <div class="thmb-prev">
                                                            <img src="/images/templates/8.png" class="img-responsive" alt="" />
                                                        </div>
                                                        <h5 class="fm-title">
                                                            <input type="radio" name="dlltemplate" runat="server" id="default8" value="minimal" />
                                                            <label for="default8">&nbsp;Blanco 100%</label></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb15" runat="server" id="lnkDisenioPDF">
                                            <h3>¿Necesitás un diseño a medida? Solicitalo en nuestra <a href="/modulos/seguridad/shopping.aspx">tienda de apps</a></h3>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <a class="btn btn-success" id="btnActualizarTemplate" onclick="MisDatos.ActualizarTemplate();">Actualizar</a>
                                    </div>
                                </div>
                                <div class="tab-pane" id="datosFiscales">

                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <h3>Puntos de venta </h3>
                                                    <label class="control-label">Estos son los Puntos de ventas habilitados para facturar.</label>
                                                </div>
                                            </div>
                                            <div id="puntos">
                                                <div class="alert alert-danger" id="divErrorPuntos" style="display: none">
                                                    <strong>Lo sentimos! </strong><span id="msgErrorPuntos"></span>
                                                </div>


                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <asp:TextBox runat="server" ID="txtNuevoPunto" CssClass="form-control" MaxLength="4"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2"><a class="btn btn-default" id="btnActualizarPunto" onclick="MisDatos.agregarPunto();">Agregar</a></div>

                                                <br />
                                                <br />
                                                <br />

                                                <div class="table-responsive">
                                                    <table class="table mb30">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Punto</th>
                                                                <th>Fecha alta</th>
                                                                <th>Fecha baja</th>
                                                                <th>Por defecto</th>
                                                                <th>Para Integraciones</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="bodyDetalle">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <h3>Soy agente de percepción </h3>
                                                    <hr />
                                                    <label class="control-label">
                                                        El agente de percepción recibe el importe correspondiente al tributo en el momento que el contribuyente paga la factura que se le extiende por la compra de un bien o la prestación de servicio.
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <asp:CheckBox runat="server" ID="esAgentePersepcionIVA"></asp:CheckBox><label class="control-label" for="esAgentePersepcionIVA"> &nbsp;IVA</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:CheckBox runat="server" ID="esAgentePersepcionIIBB"></asp:CheckBox><label class="control-label" for="esAgentePersepcionIIBB"> &nbsp;IIBB</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <h3>Soy agente de retención </h3>
                                                    <hr />
                                                    <label class="control-label">
                                                        El agente de retención, casi siempre debe entregar o ser partícipe de alguna manera de la entrega de un monto destinado al contribuyente, del cual detrae, amputa o resta a dicho importe la parte que le corresponde al fisco en concepto de tributo.
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <asp:CheckBox runat="server" ID="esAgenteRetencion"></asp:CheckBox><label class="control-label" for="esAgenteRetencion"> &nbsp; Soy agente de retención</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Jurisdicción IIBB</label>
                                                        <select id="ddlJuresdiccion" class="select2" data-placeholder="Seleccione una jurisdicción" multiple="multiple">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-6">
                                                    <h3>Fecha de cierre del ejercicio</h3>
                                                    <hr />
                                                    <label class="control-label">
                                                        Fecha en la cual se cierra el ejercicio contable de la empresa.
                                                    </label>
                                                    <div class="form-group" style="max-width:200px">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            <asp:TextBox runat="server" ID="txtFechaCierreContable" CssClass="form-control validDateFuture" MaxLength="10"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <h3>Fecha para restringir carga de información</h3>
                                                    <hr />
                                                    <label class="control-label">
                                                        Fecha hasta la cual no se podrá cargar más información.
                                                    </label>
                                                    <div class="form-group" style="max-width:200px">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            <asp:TextBox runat="server" ID="txtFechaLimiteCarga" CssClass="form-control validDateFuture" MaxLength="10"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-footer">
                                            <a class="btn btn-success" id="btnActualizarDatosFiscales" onclick="MisDatos.grabar();">Actualizar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="plandeCuentas">
                                    <div class="alert alert-danger" id="divErrorPlanDeCuenta" style="display: none">
                                        <strong>Lo sentimos! </strong><span id="msgErrorPlanDeCuenta"></span>
                                    </div>
                                    <div class="alert alert-success" id="divOkPlanDeCuenta" style="display: none">
                                        <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <hr />
                                                    <h3>Compras </h3>
                                                    <label class="control-label">
                                                        Son las cuentas en donde se registrarán todas las operaciones contables que se realizan mediante la compra de un bien o servicio. 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Proveedores</label>
                                                        <asp:DropDownList runat="server" ID="ddlCtaProveedoresComprobante" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IVA Crédito Fiscal</label>
                                                        <asp:DropDownList runat="server" ID="ddlIVACreditoFiscalComprobante" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Conceptos no gravados por compras</label>
                                                        <asp:DropDownList runat="server" ID="ddlNoGravadoCompras" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Anticipo a proveedores</label>
                                                        <asp:DropDownList runat="server" ID="ddlAnticipoProveedores" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <hr />
                                                    <h3>Ventas</h3>
                                                    <label class="control-label">
                                                        Son las cuentas en donde se registrarán todas las operaciones contables que se realizan mediante la venta de un bien o servicio. 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Deudores por ventas</label>
                                                        <asp:DropDownList runat="server" ID="ddlDeudoresPorVenta" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IVA Débito Fiscal</label>
                                                        <asp:DropDownList runat="server" ID="ddlIVADebitoFiscal" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Conceptos no gravados por ventas</label>
                                                        <asp:DropDownList runat="server" ID="ddlNoGravadoVentas" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Anticipo de cliente</label>
                                                        <asp:DropDownList runat="server" ID="ddlAnticipoClientes" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> CMV</label>
                                                        <asp:DropDownList runat="server" ID="ddlCMV" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <hr />
                                                    <h3>Percepciones</h3>
                                                    <label class="control-label">
                                                        Son las cuentas en donde se registrarán todas las operaciones contables relacionadas a percepciones. 
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IIBB a pagar</label>
                                                        <asp:DropDownList runat="server" ID="ddlPercIIBB" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IVA a pagar</label>
                                                        <asp:DropDownList runat="server" ID="ddlPercIVA" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IIBB sufridas</label>
                                                        <asp:DropDownList runat="server" ID="ddlPercIIBBSufrida" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IVA sufridas</label>
                                                        <asp:DropDownList runat="server" ID="ddlPercIVASufrida" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <hr />
                                                    <h3>Bancos y cheques</h3>
                                                    <label class="control-label">
                                                        Son las cuentas en donde se registrarán todas las operaciones contables que se realizan con cheques o gastos bancarios. 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Bancos</label>
                                                        <asp:DropDownList runat="server" ID="ddlBanco" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Valores a depositar</label>
                                                        <asp:DropDownList runat="server" ID="ddlValoresADepositar" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Cheques diferidos a pagar</label>
                                                        <asp:DropDownList runat="server" ID="ddlChequesDif" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> SIRCREB</label>
                                                        <asp:DropDownList runat="server" ID="ddlSircreb" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Gastos</label>
                                                        <asp:DropDownList runat="server" ID="ddlGastos" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Comisiones</label>
                                                        <asp:DropDownList runat="server" ID="ddlComisiones" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Crédito computable</label>
                                                        <asp:DropDownList runat="server" ID="ddlCreditoComputable" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Imp. Deb/Cred</label>
                                                        <asp:DropDownList runat="server" ID="ddlImpDebCred" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Intereses</label>
                                                        <asp:DropDownList runat="server" ID="ddlInteresBanco" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Impuestos y sellos</label>
                                                        <asp:DropDownList runat="server" ID="ddlImpSellos" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Otros</label>
                                                        <asp:DropDownList runat="server" ID="ddlOtros" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <hr />
                                                    <h3>Retenciones</h3>
                                                    <label class="control-label">
                                                        Son las cuentas en donde se registrarán todas las operaciones contables relacionadas a retenciones. 
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-3 hide">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Caja</label>
                                                        <asp:DropDownList runat="server" ID="ddlCaja" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IIBB a pagar</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetIIBB" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IVA a pagar</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetIVA" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Imp. a las ganancias a pagar</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetGanancias" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> SUSS a pagar</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetSUSS" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row mb15">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IIBB sufridas</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetIIBBSufridas" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> IVA sufridas</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetIVASufridas" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Imp. a las ganancias sufridas</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetGananciasSufridas" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> SUSS sufridas</label>
                                                        <asp:DropDownList runat="server" ID="ddlRetSUSSSufridas" CssClass="select2" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                                
                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <hr />
                                                    <h3>Filtros de busqueda</h3>
                                                    <label class="control-label">
                                                        Agregue aqui las cuentas que apareceran para elegir en las pantallas de compra y venta.
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row mb15">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Compras</label>
                                                        <asp:DropDownList runat="server" ID="ddlCuentasCompras" CssClass="select2" data-placeholder="Seleccione una cuenta..." multiple>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="control-label"><code>*</code> Ventas</label>
                                                        <asp:DropDownList runat="server" ID="ddlCuentasVentas" CssClass="select2" data-placeholder="Seleccione una cuenta..." multiple>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <input runat="server" id="hdnCuentasCompras" value="0" type="hidden" />
                                            <input runat="server" id="hdnCuentasVentas" value="0" type="hidden" />
                                        </div>
                                        <div class="panel-footer">
                                            <a class="btn btn-success" id="A2" onclick="MisDatos.guardarConfiguracionPlanDeCuenta();">Actualizar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="configuracion">
                                    <div class="alert alert-danger" id="divErrorConfiguracion" style="display: none">
                                        <strong>Lo sentimos! </strong><span id="msgErrorConfiguracion"></span>
                                    </div>
                                    <div class="alert alert-success" id="divOkConfiguracion" style="display: none">
                                        <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row mb15">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <h3>Cantidad de decimales a utilizar</h3>
                                                        <asp:DropDownList runat="server" ID="ddlCantidadDecimales" CssClass="form-control required" data-placeholder="Seleccione una cuenta...">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <h3>¿Desea usar el precio unitario con el IVA incluído por defecto ?</h3>
                                                        <input type="radio" name="rPrecioUnitario" runat="server" id="rPrecioUnitarioConIVA" value="1" />
                                                         <label for="rPrecioUnitarioConIVA">&nbsp;Si, usar el precio unitario con IVA incluído.</label><br />
                                                        <input type="radio" name="rPrecioUnitario" runat="server" id="rPrecioUnitarioSinIVA" value="0" />
                                                         <label for="rPrecioUnitarioSinIVA">&nbsp;No, usar el precio unitario sin incluir IVA.</label>
                                                    </div>
                                                </div>
                                               <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <h3>¿Desea vender sin stock?</h3>
                                                        <input type="radio" name="rVenderStock" runat="server" id="rVenderSinStock" value="1" />
                                                         <label for="rVenderSinStock">&nbsp;Si</label><br />
                                                        <input type="radio" name="rVenderStock" runat="server" id="rVenderConStock" value="0" />
                                                         <label for="rVenderConStock">&nbsp;No</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <hr />
                                                    <h3>Observaciones por defecto</h3>
                                                    <p>Configura las observaciones por defecto que aparecen en las facturas y remitos.</p>
                                                    <div class="form-group">
                                                        <label class="control-label">En facturas</label>
                                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="6" ID="txtObservacionesFc" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">En remitos</label>
                                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="6" ID="txtObservacionesRemito" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">En presupuestos</label>
                                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="6" ID="txtObservacionesPresup" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">En ordenes de pago</label>
                                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="6" ID="txtObservacionesOrdenes" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a class="btn btn-success" id="A1" onclick="MisDatos.guardarConfiguracion();">Actualizar</a>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <asp:HiddenField runat="server" ID="hdnJuresdiccion" Value="0" />
                            <asp:HiddenField runat="server" ID="hdnTieneFoto" Value="0" />
                            <asp:HiddenField runat="server" ID="hdnSinCombioDeFoto" Value="0" />
                            <asp:HiddenField runat="server" ID="hdnIdPlan" Value="0" />
                            <asp:HiddenField runat="server" ID="hdnAccion" Value="0" />
                            <asp:HiddenField runat="server" ID="hdnCiudad" Value="0" />
                            <asp:HiddenField runat="server" ID="hdnProvincia" Value="2" />
                            <asp:HiddenField runat="server" ID="hdnPais" Value="10" />
                            <input type="hidden" name="IDusuario" id="IDusuario" value="" runat="server" />

                        </div>
                        <div class="tab-pane" id="planPagos" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <h3>Tu plan actual es: <span id="spNombrePlan"></span></h3>
                                            <label class="control-label">La fecha de vencimiento es: <span id="spFechaPlan"></span></label>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-12" runat="server" id="divPagar">
                                            <a href="#" class="btn btn-success mt10" onclick="MisDatos.upgradePlanActual();">Cambiar de plan</a>
                                            <a href="#" class="btn btn-success mt10" onclick="MisDatos.upgradePlanActual();">Informar pago</a>
                                            <a href="referidos.aspx" class="btn btn-white mt10">Recomendá Contabilium y ganá $$</a>
                                        </div>
                                        <div runat="server" id="divNoPagar" style="margin-left:10px">
                                            <h4 class="person-name">Los pagos y/o cambios de planes deben realizarse desde la cuenta principal.</h4>
                                        </div>
                                    </div>

                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <hr />
                                            <h3>Historial de pagos </h3>
                                            <label class="control-label">Estos son los últimos 12 pagos registrados en el sistema.</label>
                                        </div>
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table mb30">
                                            <thead>
                                                <tr>
                                                    <th>Tipo de plan</th>
                                                    <th>Fecha Inicio</th>
                                                    <th>Fecha Vencimiento</th>
                                                    <th>Foma de pago</th>
                                                    <th>Nro referencia</th>
                                                    <th>Importe pagado</th>
                                                    <th>Fecha de pago</th>
                                                    <th>Estado</th>
                                                </tr>
                                            </thead>
                                            <tbody id="resultsContainer">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="Empresas" runat="server">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row mb15">
                                        <div class="col-sm-5">
                                            <h3>Seleccione el CUIT con el que desea operar</h3>
                                        </div>
                                        <div class="col-sm-7" id="btnNuevo" runat="server" style="margin-top: 10px">
                                            <button class="btn btn-warning" onclick="Empresa.nuevo();" type="button"><i class="fa fa-plus mr5"></i>Agregar CUIT</button>
                                        </div>
                                        <div class="col-sm-7" style="margin-top: 15px; font-style: italic;">
                                            <h5 id='spMsgCantEmpresas' style="display: none">Superó el máximo de empresas permitidas. Si desea obtener más comuníquese con nosotros al (011) 5263-2062.
                                            </h5>
                                        </div>
                                    </div>

                                    <hr />
                                    <div class="row">
                                        <div class="people-list">
                                            <div class="row" id="resultsContainerEmpresas">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <input type="hidden" name="name" value="" id="IDUsuarioAdicional" runat="server" />
                            <input type="hidden" name="name" value="" id="IDUsuarioActual" runat="server" />
                        </div>
                        <div class="tab-pane" id="AlertasyAvisos" runat="server">
                            <div class="alert alert-danger" id="divErrorAlertasyAvisos" style="display: none">
                                <strong>Lo sentimos! </strong><span id="msgErrorAlertasyAvisos"></span>
                            </div>
                            <div class="alert alert-success" id="divOkAlertasyAvisos" style="display: none">
                                <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <h3>Envio automático de comprobantes electrónicos (VÁLIDO para plan PYME en adelante)</h3>
                                            <label class="control-label">
                                                Cada vez que genere un comprobante electrónico, una cobranza o un abono se enviará automáticamente un correo con el comprobante Adjunto y un mensaje configurable desde aquí.
                                            </label>
                                            <hr />
                                        </div>
                                    </div>

                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <div class="col-sm-2" >
                                                <label>
                                                    <input type="checkbox" id="chkEnvioFE" onclick="MisDatos.habilitarEnvioFE();" /><span style="margin-right: -114px; display: inline-block;">&nbsp;Enviar comprobante automáticamente</span></label>
                                            </div>
                                            <div class="divEnvioFE col-sm-10">
                                                <div class="col-sm-5"></div>
                                                <div class="col-sm-3">
                                                    <input id="btnEnvioFE" class="btn btn-success btn-sm" onclick="MisDatos.configurarMensaje('divEnvioFE');" value="Configurar mensaje" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <div class="col-sm-2">
                                                <label>
                                                    <input type="checkbox" id="chkEnvioCR" onclick="MisDatos.habilitarEnvioCR();" /><span style="margin-right: -69px; display: inline-block;">&nbsp;Enviar recibo automáticamente</span></label>
                                            </div>
                                            <div class="divEnvioCR col-sm-10">
                                                <div class="col-sm-5"></div>
                                                <div class="col-sm-3">
                                                    <input id="btnEnvioCR" class="btn btn-success btn-sm" onclick="MisDatos.configurarMensaje('divEnvioCR');" value="Configurar mensaje" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <h3>Avisos de vencimiento para sus facturas de ventas (VÁLIDO para plan EMPRESA en adelante)</h3>
                                            <label class="control-label">
                                                Le enviaremos a tus clientes un email avisando la proximidad del vencimiento de las facturas con su número, 
                                                        saldo y fecha de vencimiento de acuerdo a los días de aviso que se configuren aquí. 
                                            </label>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row mb15" style="display: -webkit-box;">
                                        <div class="col-sm-12">
                                            <div class="col-sm-2" >
                                                <label>
                                                    <input type="checkbox" id="chkPrimerAviso" onclick="MisDatos.habilitarPrimerAviso();" /><span style="margin-right: 29px;">&nbsp;1er. Aviso: Días</span></label>
                                            </div>


                                            <div class="divPrimerAviso col-sm-10">
                                                <div class="col-sm-4">
                                                    <label>
                                                        <input type="radio" name="rPrimerAviso" runat="server" id="rPrimerAvisoAntes" value="Antes" /><span style="margin-right: 10px;">Antes</span></label>
                                                    <label>
                                                        <input type="radio" name="rPrimerAviso" runat="server" id="rPrimerAvisoDespues" value="Despues" /><span style="margin-right: 10px;">Después</span></label>

                                                    <span>del vencimiento</span>
                                                </div>
                                                <div class="col-sm-1" style="min-width: 100px; margin-left: -20px;">
                                                    <input id="txtDiasPrimerAviso" class="col-sm-1 form-control" maxlength="2" placeholder="Cant días" />
                                                </div>
                                                <div class="col-sm-2">
                                                    <input id="btnPrimerAviso" class="btn btn-success btn-sm" onclick="MisDatos.configurarMensaje('divPrimerAviso');" value="Configurar mensaje" />
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <div class="col-sm-2" >
                                                <label>
                                                    <input type="checkbox" id="chkSegundoAviso" onclick="MisDatos.habilitarSegundoAviso();" /><span style="margin-right: 29px;">&nbsp;2er. Aviso: Días</span></label>
                                            </div>
                                            <div class="divSegundoAviso col-sm-10">
                                                <div class="col-sm-4">
                                                    <label>
                                                        <input type="radio" name="rSegundoAviso" runat="server" id="rSegundoAvisoAntes" value="Antes" /><span style="margin-right: 10px;">Antes</span></label>

                                                    <label>
                                                        <input type="radio" name="rSegundoAviso" runat="server" id="rSegundoAvisoDespues" value="Despues" /><span style="margin-right: 10px;">Después</span></label>
                                                    <span>del vencimiento</span>
                                                </div>
                                                <div class="col-sm-1" style="min-width: 100px; margin-left: -20px;">
                                                    <input id="txtDiasSegundoAviso" class="col-sm-1 form-control" maxlength="2" placeholder="Cant días" />
                                                </div>
                                                <div class="col-sm-3">
                                                    <input id="btnSegundoAviso" class="btn btn-success btn-sm" onclick="MisDatos.configurarMensaje('divSegundoAviso');" value="Configurar mensaje" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <div class="col-sm-2" >
                                                <label>
                                                    <input type="checkbox" id="chkTercerAviso" onclick="MisDatos.habilitarTercerAviso();" /><span style="margin-right: 29px;">&nbsp;3er. Aviso: Días</span></label>
                                            </div>
                                            <div class="divTercerAviso col-sm-10">
                                                <div class="col-sm-4">
                                                    <label>
                                                        <input type="radio" name="rTercerAviso" runat="server" id="rTercerAvisoAntes" value="Antes" /><span style="margin-right: 10px;">Antes</span></label>
                                                    <label>
                                                        <input type="radio" name="rTercerAviso" runat="server" id="rTercerAvisoDespues" value="Despues" /><span style="margin-right: 10px;">Después</span></label>
                                                    <span>del vencimiento</span>
                                                </div>
                                                <div class="col-sm-1" style="min-width: 100px; margin-left: -20px;">
                                                    <input id="txtDiasTercerAviso" class="col-sm-1 form-control" maxlength="2" placeholder="Cant días" />
                                                </div>
                                                <div class="col-sm-3">
                                                    <input id="btnTercerAviso" class="btn btn-success btn-sm" onclick="MisDatos.configurarMensaje('divTercerAviso');" value="Configurar mensaje" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb15">
                                        <div class="col-sm-12">
                                            <h3>Alertas de stock (VÁLIDO para plan PYME en adelante)</h3>
                                            <label class="control-label">
                                                Le enviaremos un email avisando que su producto llegó al stock mínimo configurado. 
                                            </label>
                                            <hr />
                                        </div>

                                    </div>

                                     <div class="row mb15">
                                        <div class="col-sm-12">
                                            <div class="col-sm-2">
                                                <label>
                                                    <input type="checkbox" id="chkStock" /><span style="margin-right: -213px; display: inline-block;">&nbsp;Enviar alerta cuando el stock sea igual al stock minimo</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb15">
                                        <div class="col-sm-6" >
                                            <div class="form-group">
                                                <label class="control-label">Email stock</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <asp:TextBox runat="server" ID="txtEmailStock" CssClass="form-control multiemails" MaxLength="128" ></asp:TextBox>
                                                </div>
                                                <small>Si deseas ingresar más de 1 dirección, separa las direcciones mediante una coma.</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    
                                    <a class="btn btn-success" id="btnAlertasyAvisos" onclick="MisDatos.guardarAlertasyAvisos();">Actualizar</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- col-sm-9 -->
        </div>
        <!-- row -->
    </div>

    <!-- MODAL -->
    <div class="modal modal-wide fade" id="modalAlertasYAvisos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="contentpanel">
                <form id="frmNuevaAlertasyAvisos" class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="modal-header">
                            <h4 class="modal-title" id="litModalOkTitulo"></h4>
                        </div>
                        <div class="panel-body" id="modalPrimerAviso">
                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Asunto</label>
                                        <input id="txtAsuntoPrimerAviso" class="form-control required" maxlength="150" />
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Mensaje</label>
                                        <textarea rows="5" id="txtMensajePrimerAviso" class="form-control required"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" id="modalSegundoAviso">
                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Asunto</label>
                                        <input id="txtAsuntoSegundoAviso" class="form-control required" maxlength="150" />
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Mensaje</label>
                                        <textarea rows="5" id="txtMensajeSegundoAviso" class="form-control required"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" id="modalTercerAviso">
                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Asunto</label>
                                        <input id="txtAsuntoTercerAviso" class="form-control required" maxlength="150" />
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Mensaje</label>
                                        <textarea rows="5" id="txtMensajeTercerAviso" class="form-control required"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" id="modalEnvioFE">
                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Asunto</label>
                                        <input id="txtAsuntoEnvioFE" class="form-control required" maxlength="150" />
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Mensaje</label>
                                        <textarea rows="5" id="txtMensajeEnvioFE" class="form-control required"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" id="modalEnvioCR">
                            <div class="row mb15">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Asunto</label>
                                        <input id="txtAsuntoEnvioCR" class="form-control required" maxlength="150" />
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label"><span class="asterisk">*</span> Mensaje</label>
                                        <textarea rows="5" id="txtMensajeEnvioCR" class="form-control required"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-success" id="btnActualizar" onclick="MisDatos.guardarAsuntoYMensaje();">Aceptar</a>
                            <a href="#" onclick="MisDatos.cancelarAsuntoYMensaje();" tabindex="14" style="margin-left: 20px">Cerrar</a>
                        </div>
                    </div>
                    <input id="hdnSaldoTotalActual" type="hidden" value="0" />
                    <input id="hdnID" type="hidden" value="0" />
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-wide fade" id="modalNuevaEmpresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="contentpanel">
                <form id="frmNuevaEmpresa" class="col-sm-12" onsubmit="return false;">
                    <div class="row mb15">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="alert alert-danger" id="divErrorEmpresa" style="display: none">
                                    <strong>Lo sentimos! </strong><span id="msgErrorEmpresa"></span>
                                </div>


                                <div class="row mb15">
                                    <div class="col-sm-12">
                                        <h3>Datos principales </h3>
                                        <label class="control-label">Estos son los datos básicos y obligatorios que el sistema necesita para poder facturar.</label>
                                    </div>
                                </div>

                                <div class="row mb15">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Razón Social</label>
                                            <input type="text" id="txtRazonSocialEmpresa" name="txtRazonSocial" class="form-control required" maxlength="128" tabindex="1" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> CUIT</label>
                                            <input id="txtCuitEmpresa" type="text" class="form-control required number validCuit" maxlength="13" tabindex="2" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb15">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Categoría Impositiva</label>
                                            <select id="ddlCondicionIvaEmpresa" class="form-control required" tabindex="3">
                                                <option value=""></option>
                                                <option value="RI">Responsable Inscripto</option>
                                                <%--<option value="CF">Consumidor Final</option>--%>
                                                <option value="MO">Monotributista</option>
                                                <option value="EX">Exento</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Personería</label>
                                            <select id="ddlPersoneriaEmpresa" class="form-control required" tabindex="4">
                                                <option value="F">Física</option>
                                                <option value="J">Jurídica</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb15">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><code>*</code> E-mail</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">@</span>
                                                <input type="email" id="txtEmailEmpresa" class="form-control required" maxlength="128" tabindex="5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><code>*</code> Password</label>
                                            <input runat="server" id="txtPwdEmpresa" type="password" class="form-control required" maxlength="20" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb15">
                                    <div class="col-sm-12">
                                        <hr />
                                        <h3>Domicilio Fiscal </h3>
                                        <label class="control-label">Estos son los datos básicos y obligatorios que el sistema necesita para poder facturar.</label>
                                    </div>
                                </div>

                                <div class="row mb15">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Provincia</label>
                                            <select id="ddlProvinciaEmpresa" class="select2 required" data-placeholder="Selecciona una provincia..." tabindex="6" onchange="Empresa.changeProvincia();">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Ciudad/Localidad</label>
                                            <select id="ddlCiudadEmpresa" class="select2 required" data-placeholder="Selecciona una ciudad...">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb15">
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <label class="control-label"><span class="asterisk">*</span> Domicilio</label>
                                            <input type="text" id="txtDomicilioEmpresa" class="form-control required" maxlength="100" tabindex="7" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Piso/Depto</label>
                                            <input type="text" id="txtPisoDeptoEmpresa" class="form-control" maxlength="10" tabindex="8" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <input type="button" class="btn btn-success" value="Crear empresa" onclick="Empresa.grabar();" tabindex="9" id="btnCrearEmpresa" />
                                <a style="margin-left: 20px" href="#" onclick="$('#modalNuevaEmpresa').modal('toggle');" tabindex="10">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <tr>
            <td>${TipoDePlan}</td>
            <td>${FechaInicio}</td>
            <td>${FechaVencimiento}</td>
            <td>${FomaDePago}</td>
            <td>${NroReferencia}</td>
            <td>${ImportePagado}</td>
            <td>${FechaDePago}</td>
            <td>${Estado}</td>
        </tr>
        {{/each}}
    </script>
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="8">No se han encontrado resultados</td>
        </tr>
    </script>
    <!--Empresas-->
    <script id="resultTemplateEmpresas" type="text/x-jQuery-tmpl">
        {{each results}}
            <div class="col-md-6">
                <div class="people-item">
                    <div class="media">
                        <a href="#" class="pull-left">
                            <img alt="" src="${Logo}" class="thumbnail media-object" />
                        </a>
                        <div class="media-body">
                            <h4 class="person-name">${RazonSocial}</h4>
                            <div class="text-muted"><i class="fa fa-map-marker"></i>${Domicilio}, ${Ciudad}, ${Provincia}</div>
                            <div class="text-muted"><i class="fa fa-briefcase"></i>${CUIT}, ${CondicionIva}</div>
                            <ul class="social-list">
                                {{if ID==$("#IDUsuarioActual").val()}}
                                    <li><a class="btn btn-default" style="width: 100% !important; text-align: left; background-color: #ccc">Usted se encuentra usando ${RazonSocial}</a></li>
                                {{else}}
                                    <li><a onclick="Empresa.cambiarSesion(${ID},'${RazonSocial}');" title="Acceder" class="btn btn-success" style="width: 100% !important; text-align: left;"><i class="fa  fa-sign-in"></i>Usar empresa ${RazonSocial}</a></li>
                                {{/if}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        {{/each}}
    </script>
    <script id="noResultTemplateEmpresas" type="text/x-jQuery-tmpl">
        <h4>No se han encontrado otras empresas</h4>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
      
    <script src="/js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="/js/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="/js/jquery.fileupload.js" type="text/javascript"></script>
    <script src="/js/views/seguridad/mis-datos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
  
    <script>
        jQuery(document).ready(function () {
            MisDatos.configForm();
            MisDatos.obtenerPuntos();

            //EMPRESAS
            Empresa.configForm();
            Empresa.filtrar();
        });
        $(document).on('focusin', function (e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });
    </script>
     

</asp:Content>

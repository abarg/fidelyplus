﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="usuariose.aspx.cs" Inherits="usuariose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link href="/css/treetable.css" rel="stylesheet" />
    <style type="text/css">
        .indenter {
            display: none;
        }
    </style>   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-users"></i>Usuarios </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/modulos/seguridad/mis-datos.aspx">Mi cuenta</a></li>
                <li><a href="/modulos/seguridad/usuarios.aspx">Usuarios</a></li>
                <li class="active"><asp:Literal runat="server" ID="litPath"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12" autocomplete="off">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>

                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Email</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control required email" MaxLength="128" autocomplete="off" AutoCompleteType="Search" TextMode="SingleLine"></asp:TextBox>
                                        <%--<input runat="server" id="txtEmail" class="form-control" maxlength="128" autocomplete="off" type="search"/>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <asp:Literal ID="litPwd" runat="server"></asp:Literal>
                                    <asp:TextBox runat="server" ID="txtPwd" TextMode="Password" CssClass="form-control required" MaxLength="20" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Nivel de seguridad</label>
                                    <asp:DropDownList runat="server" ID="ddlTipo" CssClass="form-control" onchange="UsuariosAdic.changeTipo();">
                                        <asp:ListItem Value="A">Acceso total</asp:ListItem>
                                        <asp:ListItem Value="B">Acceso restringido</asp:ListItem>
                                        <asp:ListItem Value="T">Tracking horas</asp:ListItem>
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                          
                          
                             <div class="col-sm-1">
                                <div class="form-group">
                                    <label class="control-label">Activo</label><br />
                                    <asp:CheckBox runat="server" ID="chkActivo"></asp:CheckBox>
                                </div>
                            </div>  
                             <div class="col-sm-2" id="divComision">
                                <div class="form-group">
                                    <label class="control-label">Comisión %</label><br />
                                    <asp:TextBox runat="server" ID="txtPorcentajeComision" CssClass="form-control" MaxLength="2" ></asp:TextBox>
                                </div>
                            </div> 
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <asp:Label ID="lblNombre" runat="server"></asp:Label>
                                    <asp:Panel ID="panelChk" runat="server"></asp:Panel>
                                </div>
                            </div>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-6" id="divChkAccesoModulos">
                                <div class="form-group">
                                    <label class="control-label">Acceso a los siguientes módulos:</label><br />
                                    <asp:Panel ID="pnlChkAccesoModulos" runat="server"></asp:Panel>
                                    </div>
                                </div>
                            </div>
                      
                        <div class="row mb15">
                            <div class="col-sm-6" id="divChkAccesoModulosRestringidos">
                                <div class="form-group">
                                    <label class="control-label">Acceso a las siguientes funcionalidades de cada módulo:</label><br />
                                    <asp:Table ID="tblModulos"            
                                            runat="server" 
                                            CssClass="table mb30" style="margin-left: -10px;">
                                        </asp:Table>
                                    </div>
                                </div>
                            </div>
                          <div class="row mb15">
                            <div class="col-sm-6" id="divChkAccesoPermisos">
                                <div class="form-group">
                                    <h3>Permisos:</h3><br />
                                    <asp:Panel ID="pnlChkAccesoPermisos" runat="server"></asp:Panel>
                                    </div>
                                </div>
                            </div>

                        <div class="row mb15">
                            <div class="col-sm-6" id="divAccesos">
                                <div class="form-group">
                                    <h3>Establezca los accesos las diferentes empresas:</h3><br />
                                    <div class="table-responsive">
                                        <asp:Table ID="tblAccesos"            
                                            runat="server" 
                                            CssClass="table mb30">
                                        
                                            <asp:TableHeaderRow 
                                                runat="server" 
                                                Font-Bold="true">
                                                <asp:TableHeaderCell>Empresa</asp:TableHeaderCell>
                                                <asp:TableHeaderCell>Puntos de Venta</asp:TableHeaderCell>
                                            </asp:TableHeaderRow>
                                        </asp:Table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <a class="btn btn-success" id="btnActualizar" onclick="UsuariosAdic.grabar();">Aceptar</a>
                        <a href="#" onclick="UsuariosAdic.cancelar();" style="margin-left:20px">Cancelar</a>
                    </div>
                  </div>
                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnAddinUsuariosPersonalizados" Value="0" />

            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/jquery.treetable.js"></script>
    <script src="/js/views/seguridad/usuariosAdic.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            UsuariosAdic.configForm();
        });
    </script>
    <asp:Literal runat="server" ID="litScript"></asp:Literal>
</asp:Content>
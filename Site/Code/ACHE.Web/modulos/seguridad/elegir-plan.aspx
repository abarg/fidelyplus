﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="elegir-plan.aspx.cs" Inherits="modulos_seguridad_elegir_plan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .blog-meta li {
            float: inherit;
            padding: 0;
            font-size: 13px;
            border-right: 0;
        }

        /*Klaus Structure*/

        .klausbg {
	        background:#fff;
	        height:610px;
	        text-align:center;
	        font-family: 'Titillium Web', sans-serif;
	        cursor:default;
	        padding:0;
	        margin-bottom:30px;
	        border-right:1px solid #f0f0f0;
            line-height: initial;
        }

        .klausheader {height:236px;border-bottom:1px solid #f0f0f0;}
        .klausbg h5 {color:#606060 !important;font-size:30px !important;font-weight:700 !important;text-transform:uppercase;float:left;width:100%;margin:0;padding-top:40px;padding-bottom:10px;}
        .klausbg p {color:#9fa5ae;font-size:16px;font-weight:300;float:left;width:100%;margin-top:-10px}
        .klaus_package {color:#2d2f37;font-size:12px;font-weight:300;text-transform:uppercase;float:left;width:100%;height: 34px;}
        .klaus_price {font-size:46px;font-weight:300;float:left;width:100%;margin-top:10px;}
        .klaus_featured {
	        width:100%;
	        height:30px;
	        position:absolute;
	        top:0;
	        left:50%;
	        -webkit-transform: translateX(-50%);
	        -moz-transform: translateX(-50%);
	        transform: translateX(-50%);
	        background:#ccc;
	        font-size:15px;
	        padding-top:5px;
	        text-transform:uppercase;
	        color:#fff;
	        letter-spacing:0.8px;
        }

        .klaus_features {
	        border-bottom:1px solid #f0f0f0;
        }

        .klaus_features li {
	        height:40px;
	        list-style:none;
	        text-align:left;
	        font-size:14px;
	        color:#54565b;
	        font-weight:400;
	        padding-top:10px;
	        padding-left:10px;
        }
        .klaus_features li i {
	        padding-right:10px;
	        font-size:16px;
        }
        .klaus_plancolor {background:#f4f4f4;}
        .klaus_getaquote {width:200px;height:36px;margin-left:auto;margin-right:auto;margin-bottom: 10px;}
        .klaus_getaquote li {list-style:none;height:36px;margin-top:28px;}
        .klaus_getaquote li a {color:#fff;float:left;width:100%;height:36px;padding-top:8px;border-radius:5px;-webkit-transition: all 0.3s ease;-moz-transition: all 0.3s ease;transition: all 0.3s ease;}
        .klaus_getaquote li a:hover{text-decoration:none;}


        /*Skin v1*/

        .skinonecolor {color:#299a8f;}
        .skinonebg {background:#299a8f;}
        .skinonebg2 {background:#ad2d4f;}
        .skinoneklaus_features i {color:#299a8f;}
        .skinonegetquote li a {background:#299a8f;}
        .skinonegetquote li a:hover{background:#ad2d4f;}
        .skinoneultimate {background:#299a8f;border:0;color:#fff;}
        .skinoneucolor {border-bottom:1px solid #268c81;}
        .skinoneucolor h5 {color:#fff !important;}
        .skinoneucolor span {color:#fff;}
        .skinoneucolor p {color:#fff;}
        .skinoneucolor li {color:#fff;}
        .skinoneucolor i {color:#fff;}
        .skinoneklaus_plancolor {background:#268c81;}
        .skinoneucolorget li a {background:#fff;color:#b82f5b;}
        .skinoneucolorget li a:hover {background:#ad2d4f;color:#fff;}

        .toggle_radio{
          position: relative;
          background: #268c81;
          margin: 4px auto;
          overflow: hidden;
          padding: 0 !important;
          /*-webkit-border-radius: 50px;
          -moz-border-radius: 50px;
          border-radius: 50px;*/
          position: relative;
          height: 40px;
          width: 300px;
        }
        .toggle_radio > * {
          float: left;
        }
        .toggle_radio input[type=radio]{
          display: none;
          /*position: fixed;*/
        }
        .toggle_radio label{
          color: rgba(255,255,255,.9);
          z-index: 0;
          display: block;
          width: 140px;
          height: 30px;
          margin: 10px 3px;
          /*-webkit-border-radius: 50px;
          -moz-border-radius: 50px;
          border-radius: 50px;*/
          cursor: pointer;
          z-index: 1;
          /*background: rgba(0,0,0,.1);*/
          text-align: center;
          font-size: 18px;
          /*margin: 0 2px;*/
          /*background: blue;*/ /*make it blue*/
        }
        .toggle_option_slider{
          /*display: none;*/
          /*background: red;*/
          width: 140px;
          height: 35px;
          position: absolute;
          top: 3px;
          /*-webkit-border-radius: 50px;
          -moz-border-radius: 50px;
          border-radius: 50px;*/
          -webkit-transition: all .4s ease;
          -moz-transition: all .4s ease;
          -o-transition: all .4s ease;
          -ms-transition: all .4s ease;
          transition: all .4s ease;
        }

        #rdbMensual:checked ~ .toggle_option_slider{
          background: rgba(255,255,255,.3);
          left: 3px;
        }
        #rdbAnual:checked ~ .toggle_option_slider{
          background: rgba(255,255,255,.3);
          left: 150px;
        }

        .mensual {
            display:none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-comment'></i> <asp:Literal ID="liTitulo" runat="server"></asp:Literal></h2>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row mb15" style="margin-top: 20px; margin-left: 5%; margin-right: 5%">
                    <div class="alert alert-warning" id="divAddins" runat="server" Visible="False">
                        <b>Recordatorio! </b>Los add-ins que activaste se abonan de forma adicional a tu Plan.</span>
                    </div>
                    
                    <div runat="server" id="divModoPago">
                        <p style="text-align: center;color: #606060;font-size: 15px;">Seleccione el modo de pago: </p>
                        <div class="toggle_radio">
                            <input type="radio" class="toggle_option" id="rdbMensual" name="toggle_option" onclick="elegirPlan.togglePrecios('M');" />
                            <input type="radio" checked class="toggle_option" id="rdbAnual" name="toggle_option" onclick="elegirPlan.togglePrecios('A');" />
                            <label for="rdbMensual"><p>Mensual</p></label>
                            <label for="rdbAnual"><p>Anual (20% off)</p></label>
                            <div class="toggle_option_slider"></div>
                        </div>
                    </div>
                    <br />
                    <%--<!--Begin Plan Basic!-->
	                <div class="col-md-3 klausbg hvr-float" id="PlanBasico" runat="server">
		                <div class="klausheader">
		                    <h5>GRATUITO</h5>
		                    <span class="klaus_package"></span>
		                    <span class="klaus_price skinonecolor mensual">$ 0</span>
                            <span class="klaus_price skinonecolor anual">$ 0</span>
		                    <p class="periodicidad">/anual</p>
		                </div>
		
		                <div class="klaus_features skinoneklaus_features">
                            <li><i class="fa fa-check-circle"></i> 100 comprobantes de ventas y compras</li>
                            <li class="klaus_plancolor"><i class="fa fa-check-circle"></i> Ventas</li>
                            <li><i class="fa fa-check-circle"></i> Cobranzas</li>
                            <li class="klaus_plancolor"><i class="fa fa-check-circle"></i> Pago a proveedores</li>
                            <li><i class="fa fa-check-circle"></i> Tablero de contro;</li>
                            <li class=""></li>
                            <li class=""></li>
		                </div>
		
		                <div class="klaus_getaquote skinonegetquote"><li><a href="javascript:elegirPlan.ActualizarPlanBasico()">ELEGIR</a></li></div>
	                </div>--%>
	                <!--End Plan Basic!-->
                    
                    
                    <!--Begin Plan Basic!-->
	                <div class="col-md-3 klausbg hvr-float" id="planProfesional" runat="server">
		                <div class="klausheader">
		                <h5>PROFESIONAL</h5>
		                    <span class="klaus_package">PROFESIONALES INDEPENDIENTES Y EMPRENDEDORES</span>
		                    <span class="klaus_price skinonecolor mensual">$ <asp:Literal ID="liPlanProfesionalMes" runat="server"></asp:Literal></span>
                            <span class="klaus_price skinonecolor anual">$ <asp:Literal ID="liPlanProfesionalAnual" runat="server"></asp:Literal></span>
		                    <p class="periodicidad">/anual</p>
                            <span style="color:#9fa5ae"><i>* precio sin IVA</i></span>
		                </div>
		
		                <div class="klaus_features skinoneklaus_features">
                            <li><i class="fa fa-check-circle"></i> Facturas ILIMITADAS</li><li class="klaus_plancolor"><i class="fa fa-check-circle"></i> Manejo de stock</li><li><i class="fa fa-check-circle"></i> Importación masiva de información</li><li class="klaus_plancolor"><i class="fa fa-check-circle"></i> Reportes</li><li><i class="fa fa-check-circle"></i> Presupuestos</li><li class="klaus_plancolor"><i class="fa fa-comments"></i> Email, chat y teléfono</li><li class=""></li>

		                </div>
                            
		                <div class="klaus_getaquote skinonegetquote">
                            <ul><li><a href="javascript:elegirPlan.subirPlan('Profesional')">PAGAR</a></li></ul>
                            
		                </div>
	                </div>
	                <!--End Plan Basic!-->
	
	                <!--Begin Plan Personal!-->
	                <div class="col-md-3 klausbg hvr-float" id="planPyme" runat="server">
		                <div class="klausheader">
		                    <h5>PYME</h5>
		                    <span class="klaus_package">MICRO Y PEQUEÑAS EMPRESAS</span>
		                    <span class="klaus_price skinonecolor mensual">$ <asp:Literal ID="liPlanPymeMes" runat="server"></asp:Literal></span>
                            <span class="klaus_price skinonecolor anual">$ <asp:Literal ID="liPlanPymeAnual" runat="server"></asp:Literal></span>
		                    <p class="periodicidad">/anual</p>
                            <span style="color:#9fa5ae"><i>* precio sin IVA</i></span>
		                </div>
		
		                <div class="klaus_features skinoneklaus_features">
                            <asp:Literal runat="server" ID="litDescPyme"
                                Text="<li style='color:#ad2d4f !important;font-weight: bold !important;line-height: initial;'>Incluye todo lo del PLAN PROFESIONAL</li><li class='klaus_plancolor'><i class='fa fa-check-circle'></i> Manejo de cajas, bancos y cheques</li><li><i class='fa fa-check-circle'></i> Cobranza electrónica</li><li class='klaus_plancolor'><i class='fa fa-check-circle'></i> Portal de clientes</li><li style='font-size: 13px;'><i class='fa fa-check-circle'></i> Integrado a M.Libre, M.Shops, M.Pago y T.Nube</li><li class='klaus_plancolor'><i class='fa fa-comments'></i> Email, chat y teléfono</li><li class=''></li>">
                            
                            </asp:Literal>
		                </div>
		                <div class="klaus_getaquote skinonegetquote">
                            <ul><li><a href="javascript:elegirPlan.subirPlan('Pyme')">PAGAR</a></li></ul>
                            
		                </div>
	                </div>
	                <!--End Plan Personal!-->
	
	                <!--Begin Plan Business!-->
	                <div class="col-md-3 klausbg hvr-float" id="planEmpresa" runat="server">
		                <div class="klausheader">
		                <div class="klaus_featured skinonebg2">MÁS POPULAR</div>
		                    <h5>EMPRESA</h5>
		                    <span class="klaus_package">medianas empresas</span>
		                    <span class="klaus_price skinonecolor mensual">$ <asp:Literal ID="liPlanEmpresaMes" runat="server"></asp:Literal></span>
                            <span class="klaus_price skinonecolor anual">$ <asp:Literal ID="liPlanEmpresaAnual" runat="server"></asp:Literal></span>
		                    <p class="periodicidad">/anual</p>
                            <span style="color:#9fa5ae"><i>* precio sin IVA</i></span>
		                </div>
		
		                <div class="klaus_features skinoneklaus_features"><li style="color:#ad2d4f !important;font-weight: bold !important;line-height: initial;">Incluye todo lo del PLAN PYME</li><li class="klaus_plancolor"><i class="fa fa-check-circle"></i> Múltiples depósitos</li><li><i class="fa fa-check-circle"></i> Múltiples listas de precio</li><li class="klaus_plancolor"><i class="fa fa-check-circle"></i> Alertas automáticas</li><li><i class="fa fa-check-circle"></i> 2 Cuit's</li><li class="klaus_plancolor"><i class="fa fa-comments"></i> Email, chat y teléfono</li><li class=""></li></div>
		                <div class="klaus_getaquote skinonegetquote">
                            <ul><li><a href="javascript:elegirPlan.subirPlan('Empresa')">PAGAR</a></li></ul>
                            
		                </div>
	                </div>
	                <!--End Plan Business!-->
	
	                <!--Begin Plan Ultimate!-->
	                <div class="col-md-3 klausbg skinoneultimate hvr-float" id="planCorporativo" runat="server">
		                <div class="klausheader skinoneucolor">
		                    <h5>CORPORATIVO</h5>
		                    <span class="klaus_package">TODO TIPO DE EMPRESAS Y CONTADORES</span>
		                    <span class="klaus_price skinonecolor mensual">$ <asp:Literal ID="liPlanCorporativoMes" runat="server"></asp:Literal></span>
                            <span class="klaus_price skinonecolor anual">$ <asp:Literal ID="liPlanCorporativoAnual" runat="server"></asp:Literal></span>
		                    <p class="periodicidad">/anual</p>
                            <span style="color:#9fa5ae"><i>* precio sin IVA</i></span>
		                </div>
		
		                <div class="klaus_features skinoneklaus_features skinoneucolor"><li style="color:#fff !important;font-weight: bold !important;line-height: initial;">Incluye todo lo del PLAN EMPRESA</li><li><i class="fa fa-check-circle"></i> Plan de cuentas 100% configurable</li><li class="skinoneklaus_plancolor"><i class="fa fa-check-circle"></i> Libro diario y mayor</li><li><i class="fa fa-check-circle"></i> Asientos manuales y modelos</li><li class="skinoneklaus_plancolor"><i class="fa fa-check-circle"></i> Balance gral. y Estado de resultado</li><li><i class="fa fa-check-circle"></i> 5 Cuit's</li><li class="skinoneklaus_plancolor"><i class="fa fa-comments"></i> Email, chat y teléfono</li></div>
		
		                <div class="klaus_getaquote skinonegetquote skinoneucolorget">
                            <ul><li><a href="javascript:elegirPlan.subirPlan('Corporativo')">PAGAR</a></li></ul>
                            
		                </div>
	                </div>
	                <!--End Plan Ultimate!-->

                    <%-- <div class="col-lg-3 col-sm-4 col-md-3" id="PlanBasico" runat="server">
                        <div class="blog-item blog-quote" style="min-height: 361px;">
                            <div class="quote quote-success">
                                <a href="#" style="cursor: text;">PLAN BÁSICO</a>
                            </div>
                            <div class="blog-details">
                                <ul class="blog-meta" style="min-height:150px">
                                    <li>HASTA 100 comprobantes de ventas y compras</li>
                                    <li>Ventas</li>
                                    <li>Cobranzas</li>
                                    <li>Pago a proveedores</li>
                                    <li>Tablero de control</li>
                                </ul>
                                <hr />
                                <div style="text-align: center; min-height: 53px;">
                                    <h4>GRATIS</h4>
                                </div>
                                <div style="text-align: center">
                                    <button class="btn btn-white" onclick="elegirPlan.ActualizarPlanBasico()" id="btnPlanBasico">PAGAR</button>
                                </div>
                            </div>
                        </div>
                    </div>

                   <div class="col-lg-3 col-sm-4 col-md-3" id="planProfesional" runat="server">
                        <div class="blog-item blog-quote" style="min-height: 361px;">
                            <div class="quote quote-success">
                                <a href="#" style="cursor: text;">PLAN PROFESIONAL</a>
                            </div>
                            <div class="blog-details">
                                <ul class="blog-meta" style="min-height:130px">
                                    <li>FACTURAS ILIMITADAS</li>
                                    <li>Manejo de stock</li>
                                    <li>Importación masiva de información</li>
                                    <li>Reportes</li>
                                    <li>Presupuestos</li>
                                    <li>Citi Ventas y Citi Compras</li>
                                </ul>
                                <hr />
                                <div class="form-group">
                                    <span>
                                        <input id="Radio1" type="radio" name="rProfesional" runat="server" value="false" onchange="PlanesPagos.changeFormaDePago();" checked />
                                        <asp:Literal ID="liPlanProfesionalMes" runat="server"></asp:Literal>
                                    </span>
                                    <br />
                                    <span>
                                        <input id="Radio2" type="radio" name="rProfesional" runat="server" value="true" />
                                        <asp:Literal ID="liPlanProfesionalAnual" runat="server"></asp:Literal>
                                    </span>
                                </div>
                                <div style="text-align: center">
                                    <button class="btn btn-white" onclick="elegirPlan.subirPlan('Profesional')">PAGAR</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-4 col-md-3" id="planPyme" runat="server">
                        <div class="blog-item blog-quote" style="min-height: 361px;">
                            <div class="quote quote-success">
                                <a href="#" style="cursor: text;background-color: #be315f;">PLAN PYME</a>
                            </div>
                            <div class="blog-details">
                                <ul class="blog-meta" style="min-height:130px">
                                    <li>Manejo de bancos y cheques</li>
                                    <li>Cobranza electrónica</li>
                                    <li>Portal de clientes</li>
                                    <li>Integración MercadoLibre, MercadoShops y TiendaNube</li>
                                    <li><b>Y todas las funciones del plan Profesional</b></li>
                                </ul>
                                <hr/>
                                <div class="form-group">
                                    <span>
                                        <input id="Radio3" type="radio" name="rPyme" runat="server" value="false" checked />
                                        <asp:Literal ID="liPlanPymeMes" runat="server"></asp:Literal>
                                    </span>
                                    <br />
                                    <span>
                                        <input id="Radio4" type="radio" name="rPyme" runat="server" value="true" />
                                        <asp:Literal ID="liPlanPymeAnual" runat="server"></asp:Literal>
                                    </span>
                                </div>
                                <div style="text-align: center">
                                    <button class="btn btn-white" onclick="elegirPlan.subirPlan('Pyme')">PAGAR</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-4 col-md-3" id="planEmpresa" runat="server">
                        <div class="blog-item blog-quote" style="min-height: 361px;">
                            <div class="quote quote-success">
                                <a href="#" style="cursor: text;">PLAN EMPRESA</a>
                            </div>
                            <div class="blog-details">
                                <ul class="blog-meta" style="min-height:130px">
                                    <li>Manejo de Inventarios</li>
                                    <li>Listas de precio por cliente</li>
                                    <li>Alertas autómaticas</li>
                                    <li>2 Cuit's</li>
                                    <li><b>Y todas las funciones del plan PYME</b></li>
                                </ul>
                                <hr />
                                <div class="form-group">
                                    <span>
                                        <input id="Radio5" type="radio" name="rEmpresa" runat="server" value="false" checked />
                                        <asp:Literal ID="liPlanEmpresaMes" runat="server"></asp:Literal>
                                    </span>
                                    <br />
                                    <span>
                                        <input id="Radio6" type="radio" name="rEmpresa" runat="server" value="true" />
                                        <asp:Literal ID="liPlanEmpresaAnual" runat="server"></asp:Literal>
                                    </span>
                                </div>
                                <div style="text-align: center">
                                    <button class="btn btn-white" onclick="elegirPlan.subirPlan('Empresa')">PAGAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-sm-4 col-md-4" id="planCorporativo" runat="server">
                        <div class="blog-item blog-quote">
                            <div class="quote quote-success">
                                <a href="#" style="cursor: text;background-color:#666">PLAN CORPORATIVO</a>
                            </div>
                            <div class="blog-details">
                                <ul class="blog-meta" style="min-height:130px">
                                    <li>Plan de cuentas 100% configurable</li>
                                    <li>Libro diario y mayor</li>
                                    <li>Asientos manuales</li>
                                    <li>Balance general y estado de resultados</li>
                                    <li>5 Cuit's</li>
                                    <li><b>Y todas las funciones del plan EMPRESA</b></li>
                                </ul>
                                <hr />
                                <div class="form-group">
                                    <span>
                                        <input id="Radio7" type="radio" name="rCorporativo" runat="server" value="false" checked />
                                        <asp:Literal ID="liPlanCorporativoMes" runat="server"></asp:Literal>
                                    </span>
                                    <br />
                                    <span>
                                        <input id="Radio8" type="radio" name="rCorporativo" runat="server" value="true" />
                                        <asp:Literal ID="liPlanCorporativoAnual" runat="server"></asp:Literal>
                                    </span>
                                </div>
                                <div style="text-align: center">
                                    <button class="btn btn-white" onclick="elegirPlan.subirPlan('Corporativo')">PAGAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4"></div>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/seguridad/elegirPlan.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
   
</asp:Content>




﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using ACHE.Negocio.Contabilidad;


public partial class modulos_seguridad_pago_realizado : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var resultado = string.Empty;
            var MensajeResultado = string.Empty;
            var tipo = Request.QueryString["tipo"];
            var external_reference = Request.QueryString["external_reference"];

            switch (tipo)
            {
                case "1":
                    resultado = "<label class='label label-success' style='vertical-align: middle;font-size: 13px;'>Exitoso</label>";
                    MensajeResultado = "El pago fue realizado correctamente, en menos de 24 horas se actualizará su cuenta y le enviaremos el comprobante por mail.";
                    verificarPago();
                    //Response.Redirect("referidos.aspx");
                    break;

                case "2":
                    resultado = "<label class='label label-warning' style='vertical-align: middle;font-size: 13px;'>Pendiente</label>";
                    MensajeResultado = "El pago se encuentra pendiente de confirmación.";
                    break;
                case "5":
                    resultado = "<label class='label label-danger' style='vertical-align: middle;font-size: 13px;'>Rechazado</label>";
                    MensajeResultado = "El pago no pudo ser realizado correctamente";
                    break;
                case "D":
                    resultado = "<label class='label label-danger' style='vertical-align: middle;font-size: 13px;'>Pendiente</label>";
                    MensajeResultado = "La adhesión al débito automático se encuentra pendiente de confirmación.";
                    break;
            }

            if (!string.IsNullOrWhiteSpace(external_reference))
            {
                var ListaReferencias = Regex.Split(external_reference, "-").ToList();
                var idPlan = Convert.ToInt32(ListaReferencias.Where(x => x.Contains("P")).FirstOrDefault().Replace("P", ""));


                using (var dbContext = new ACHEEntities())
                {
                    var plan = dbContext.Planes.Where(x => x.IDPlan == idPlan).FirstOrDefault();

                    liPlan.Text = plan.Nombre;

                    //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];// hardcodeado por estos 6 meses para los usuarios que ya fueron creados antes de 31/07/2015
                    //var fecha = Convert.ToDateTime("31/07/2015");
                    //if (usu.FechaAlta <= fecha && idPlan == 2)
                    //    liImporte.Text = "149";
                    //else // FIN hardcodeado 
                    //liImporte.Text = plan.Precio.ToMoneyFormat(4);

                    liResultadoOperacion.Text = resultado;
                    liMensajeResultadoOperacion.Text = MensajeResultado;
                }
            }
        }
    }

    private void verificarPago()
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        using (var dbContext = new ACHEEntities())
        {
            //obtener el plan actual, si existe es x q mercadopago ya aviso, sino hay q crear un plan de pago pendiente
            var plan = dbContext.UsuariosPlanesView.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault();
            var planviejo =plan!=null && plan.FechaFinPlan < DateTime.Now;
            var idPlanActual = PermisosModulos.ObtenerTodosLosFormularios(dbContext, usu.IDUsuario);
            var PlanVigente = PermisosModulos.PlanVigente(dbContext, usu.IDUsuario);
            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, usu.IDUsuario, usu.CondicionIVA);
            HttpContext.Current.Session["CurrentUser"] = new WebUser(
            usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT, usu.CondicionIVA,
            usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia,
            usu.IDCiudad, usu.Telefono, usu.TieneFE, usu.IIBB, usu.FechaInicio,
            usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa, usu.ModoQA, idPlanActual,
            usu.EmailAlerta, usu.Provincia, usu.Ciudad, usu.AgentePercepcionIVA, usu.AgentePercepcionIIBB, usu.AgenteRetencion,
            PlanVigente, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA, usu.FechaAlta,
            usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
            usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenes, usu.MostrarBonificacionFc, usu.PorcentajeDescuento, planviejo);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="pago-realizado.aspx.cs" Inherits="modulos_seguridad_pago_realizado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
  <div class="pageheader">
        <h2><i class='fa fa-money'></i>Pago realizado</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/modulos/seguridad/mis-datos.aspx">Mi cuenta</a></li>
                <li class="active">Pago realizado</li>
            </ol>
        </div>
    </div>

<div class="contentpanel">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-center">
                        Plan elegido: <b><asp:Literal runat="server" ID="liPlan"/></b> 
                        <br />
                        El resultado de la operación es <asp:Literal runat="server" ID="liResultadoOperacion"/>
                    </h4>
                    <p class="text-center"><asp:Literal runat="server" ID="liMensajeResultadoOperacion"/></p>
                </div>
                <!-- panel-heading -->
                <div class="panel-body  text-center">
                    <div class="alert alert-danger" id="divError" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Lo sentimos!</strong> <span id="msgError"></span>
                    </div>

                    <%--<p>
                        Datos de la operación
                    </p>--%>
                    <%--<p>
                        Importe pagado: <asp:Literal runat="server" ID="liImporte"/> + IVA por mes
                    </p>--%>
                    <br />
                    <h4 class="panel-title">¿Sabías que con nuestro plan de referidos podés ganar plata en menos de 1 minuto?</h4>
                    <br />
                    <a href="referidos.aspx" class="btn btn-white mt20">Quiero más información.</a>
                    <br /><br />
                    <a href="/home.aspx"> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>




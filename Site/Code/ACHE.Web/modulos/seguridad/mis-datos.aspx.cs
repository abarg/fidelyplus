﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Facturacion;

public partial class mis_datos : BasePage
{
    private const string LOGO_PATH = "/files/usuarios/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Verifico si es un usuario adicional. Si es, lo redirijo a cambiar pwd
            if (CurrentUser.IDUsuarioPadre > 0 || CurrentUser.IDUsuarioAdicional > 0)
            {
                if (CurrentUser.IDUsuarioAdicional > 0 && CurrentUser.TipoUsuario != "A")
                    Response.Redirect("cambiar-pwd.aspx");

                if (CurrentUser.TipoUsuario != "A")
                {
                    liplanPagos.Visible = false;
                    planPagos.Visible = false;
                    liAlertasyAvisos.Visible = false;
                    AlertasyAvisos.Visible = false;
                }
            }

            hdnAccion.Value = Request.QueryString["accion"];
            IDUsuarioAdicional.Value = CurrentUser.IDUsuarioAdicional.ToString();

            liEmpresas.Visible = CurrentUser.TieneMultiEmpresa;
            liUrlMercadoPago.Text = "https://clientes.contabilium.com/mercadopago.ashx?token=" + CurrentUser.ApiKey;
            Empresas.Visible = CurrentUser.TieneMultiEmpresa;

            if (CurrentUser.IDUsuarioPadre != null && CurrentUser.IDUsuarioPadre > 0)
            {
                btnNuevo.Visible = false;
                divPagar.Visible = false;
                divNoPagar.Visible = true;
            }
            else
            {
                divPagar.Visible = true;
                divNoPagar.Visible = false;
            }

            using (var dbContext = new ACHEEntities())
            {
                var usu = dbContext.Usuarios.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault();
                IDusuario.Value = usu.IDUsuario.ToString();

                OcultarDatosUsuarioEmpresas(dbContext);
                OcultarDatosUsuarioTemplates();

                //Cargamos el combo de cantidad de decimales disponibles
                for (int iPos = 1; iPos < 4; iPos++)
                    ddlCantidadDecimales.Items.Add(new ListItem(iPos.ToString(), iPos.ToString()));

                if (usu != null)
                {
                    //Datos principales
                    txtRazonSocial.Text = usu.RazonSocial;
                    ddlCondicionIva.SelectedValue = usu.CondicionIva;
                    txtCuit.Text = usu.CUIT;
                    txtIIBB.Text = usu.IIBB;
                    if (usu.FechaInicioActividades.HasValue)
                        txtFechaInicioAct.Text = usu.FechaInicioActividades.Value.ToString("dd/MM/yyyy");
                    ddlPersoneria.SelectedValue = usu.Personeria;
                    txtEmail.Text = usu.Email;
                    txtEmailFrom.Text = usu.EmailFrom;
                    txtEmailAlertas.Text = usu.EmailAlertas;
                    txtEmailStock.Text = usu.EmailStock;
                    txtContacto.Text = usu.Contacto;
                    txtCelular.Text = usu.Celular;
                    chkExento.Checked = Convert.ToBoolean(usu.ExentoIIBB);

                    if (string.IsNullOrWhiteSpace(usu.Logo))
                    {
                        imgLogo.Src = "/files/usuarios/no-photo.png";
                        hdnTieneFoto.Value = "0";
                    }
                    else
                    {
                        imgLogo.Src = "/files/usuarios/" + usu.Logo;
                        hdnTieneFoto.Value = "1";
                    }

                    //Domicilio
                    txtDomicilio.Text = usu.Domicilio;
                    hdnProvincia.Value = usu.IDProvincia.ToString();
                    hdnCiudad.Value = usu.IDCiudad.ToString();

                    txtPisoDepto.Text = usu.PisoDepto;
                    txtCp.Text = usu.CodigoPostal;
                    if (!usu.Telefono.Contains("-"))
                        txtTelefono.Text = usu.Telefono;
                    else
                    {
                        txtArea.Text = usu.Telefono.Split("-")[0];
                        txtTelefono.Text = usu.Telefono.Split("-")[1];
                    }

                    //Portal Cliente
                    ChkCorreoPortal.Checked = Convert.ToBoolean(usu.CorreoPortal);
                    chkPortalClientes.Checked = Convert.ToBoolean(usu.PortalClientes);
                    txtClientId.Text = usu.MercadoPagoClientID;
                    txtClientSecret.Text = usu.MercadoPagoClientSecret;

                    //Datos Fiscales
                    esAgentePersepcionIVA.Checked = Convert.ToBoolean(usu.EsAgentePercepcionIVA);
                    esAgentePersepcionIIBB.Checked = Convert.ToBoolean(usu.EsAgentePercepcionIIBB);
                    esAgenteRetencion.Checked = Convert.ToBoolean(usu.EsAgenteRetencion);

                    if (usu.IDJurisdiccion != null)
                        hdnJuresdiccion.Value = usu.IDJurisdiccion.Trim();

                    if (usu.FechaCierreContable.HasValue)
                        txtFechaCierreContable.Text = Convert.ToDateTime(usu.FechaCierreContable).ToString("dd/MM/yyyy");

                    if (usu.FechaLimiteCarga.HasValue)
                        txtFechaLimiteCarga.Text = Convert.ToDateTime(usu.FechaLimiteCarga).ToString("dd/MM/yyyy");

                    //Configuraciones base
                    //chkPrecioUnitarioConIVA.Checked = usu.UsaPrecioFinalConIVA;
                    if (usu.UsaPrecioFinalConIVA)
                        rPrecioUnitarioConIVA.Checked = true;
                    else
                        rPrecioUnitarioSinIVA.Checked = true;
                    if (usu.VenderSinStock)
                        rVenderSinStock.Checked = true;
                    else
                        rVenderConStock.Checked = true;
                    txtObservacionesFc.Text = usu.ObservacionesFc;
                    txtObservacionesRemito.Text = usu.ObservacionesRemito;
                    txtObservacionesPresup.Text = usu.ObservacionesPresupuestos;
                    txtObservacionesOrdenes.Text = usu.ObservacionesOrdenesPago;

                    //Plan Corporativo
                    if (CurrentUser.UsaPlanCorporativo)
                        cargarSelectConfiguracionPlanDeCuenta();
                    else
                        liPlandeCuentas.Visible = false;

                    //Cantidad decimales configurados
                    ddlCantidadDecimales.SelectedValue = (usu.CantidadDecimales.HasValue) ? usu.CantidadDecimales.ToString() : "2";

                    //Formato factura
                    if (usu.TemplateFc == "default")
                        default1.Checked = true;
                    else if (usu.TemplateFc == "amarillo")
                        default2.Checked = true;
                    else if (usu.TemplateFc == "celeste")
                        default3.Checked = true;
                    else if (usu.TemplateFc == "negro")
                        default4.Checked = true;
                    else if (usu.TemplateFc == "rojo")
                        default5.Checked = true;
                    else if (usu.TemplateFc == "verde")
                        default6.Checked = true;
                    else if (usu.TemplateFc == "blanco")
                        default7.Checked = true;
                    else
                        default8.Checked = true;
                }
                else
                    throw new Exception("El usuario no existe");
            }

            if (ConfigurationManager.AppSettings["Config.MarcaBlanca"] != "")
                liplanPagos.Visible = liportalClientes.Visible = lnkDisenioPDF.Visible = false;
        }
    }

    private void cargarSelectConfiguracionPlanDeCuenta()
    {
        using (var dbContext = new ACHEEntities())
        {
            var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault();

            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "el id de config es", config.IDConfiguracionPlanDeCuenta.ToString());


            var lista = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario);
            foreach (var item in lista)
            {
                var nombre = item.Codigo + " - " + item.Nombre;

                //Comprobantes
                ddlCtaProveedoresComprobante.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlIVACreditoFiscalComprobante.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlNoGravadoCompras.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlAnticipoProveedores.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));

                //ventas
                ddlIVADebitoFiscal.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlDeudoresPorVenta.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlNoGravadoVentas.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlAnticipoClientes.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlCMV.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));

                //Pagos: No se usa
                ddlCaja.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));

                //Percepciones
                ddlPercIIBB.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlPercIVA.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlPercIIBBSufrida.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlPercIVASufrida.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));

                //Retenciones
                ddlRetSUSS.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlRetIIBB.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlRetIVA.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlRetGanancias.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));

                ddlRetSUSSSufridas.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlRetIIBBSufridas.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlRetIVASufridas.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlRetGananciasSufridas.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));

                // Filtro en compras y ventas
                ddlCuentasCompras.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlCuentasVentas.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));

                //bancos
                ddlValoresADepositar.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlChequesDif.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlBanco.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlSircreb.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlComisiones.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlCreditoComputable.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlOtros.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlImpDebCred.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlGastos.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlInteresBanco.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
                ddlImpSellos.Items.Add(new ListItem(nombre, item.IDPlanDeCuenta.ToString()));
            }

            if (config != null)
            {
                //Compras
                ddlCtaProveedoresComprobante.SelectedValue = config.IDCtaProveedores.ToString();
                ddlIVACreditoFiscalComprobante.SelectedValue = config.IDCtaIVACreditoFiscal.ToString();
                ddlNoGravadoCompras.SelectedValue = config.IDCtaConceptosNoGravadosxCompras.ToString();
                ddlAnticipoProveedores.SelectedValue = config.IDCtaAnticipoProveedores.ToString();

                //Ventas
                ddlNoGravadoVentas.SelectedValue = config.IDCtaConceptosNoGravadosxVentas.ToString();
                ddlIVADebitoFiscal.SelectedValue = config.IDCtaIVADebitoFiscal.ToString();
                ddlDeudoresPorVenta.SelectedValue = config.IDCtaDeudoresPorVentas.ToString();
                ddlAnticipoClientes.SelectedValue = config.IDCtaAnticipoCliente.ToString();
                ddlCMV.SelectedValue = config.IDCtaCMV.ToString();

                ddlCaja.SelectedValue = config.IDCtaCaja.ToString();

                //Percepciones
                ddlPercIIBB.SelectedValue = config.IDCtaIIBB.ToString();
                ddlPercIVA.SelectedValue = config.IDCtaPercepcionIVA.ToString();
                ddlPercIIBBSufrida.SelectedValue = config.IDCtaIIBBSufrida.ToString();
                ddlPercIVASufrida.SelectedValue = config.IDCtaPercepcionIVASufrida.ToString();

                //Retenciones
                ddlRetIIBB.SelectedValue = config.IDctaRetIIBB.ToString();
                ddlRetIVA.SelectedValue = config.IDctaRetIVA.ToString();
                ddlRetGanancias.SelectedValue = config.IDctaRetGanancias.ToString();
                ddlRetSUSS.SelectedValue = config.IDCtaRetSUSS.ToString();

                ddlRetIIBBSufridas.SelectedValue = config.IDCtaRetIIBBSufrida.ToString();
                ddlRetIVASufridas.SelectedValue = config.IDCtaRetIVASufrida.ToString();
                ddlRetGananciasSufridas.SelectedValue = config.IDCtaRetGananciasSufrida.ToString();
                ddlRetSUSSSufridas.SelectedValue = config.IDCtaRetSUSSSufrida.ToString();

                //Bancos y cheques
                ddlBanco.SelectedValue = config.IDCtaBancos.ToString();
                ddlValoresADepositar.SelectedValue = config.IDCtaValoresADepositar.ToString();
                ddlChequesDif.SelectedValue = config.IDCtaChequesDiferidosAPagar.ToString();
                ddlSircreb.SelectedValue = config.IDCtaSircreb.ToString();
                ddlComisiones.SelectedValue = config.IDCtaComisiones.ToString();
                ddlCreditoComputable.SelectedValue = config.IDCtaCreditoComputable.ToString();
                ddlOtros.SelectedValue = config.IDCtaOtrosGastosBancarios.ToString();
                ddlImpDebCred.SelectedValue = config.IDCtaImpDebCred.ToString();
                ddlGastos.SelectedValue = config.IDCtaGastosBancarios.ToString();
                ddlInteresBanco.SelectedValue = config.IDCtaInteresBancos.ToString();
                ddlImpSellos.SelectedValue = config.IDCtaImpSellos.ToString();

                hdnCuentasCompras.Value = config.CtasFiltroCompras;
                hdnCuentasVentas.Value = config.CtasFiltroVentas;
            }
        }
    }

    private void OcultarDatosUsuarioEmpresas(ACHEEntities dbContext)
    {
        var usuAdic = new UsuariosAdicionales();
        //if (!PermisosModulos.tienePlan("template"))
        //    liTemplate.Visible = false;
        //if (!PermisosModulos.tienePlan("accecoCliente"))
        //    liportalClientes.Visible = false;
        if (CurrentUser.TipoUsuario != "A")
        {
            if (CurrentUser.IDUsuarioAdicional > 0)
            {
                usuAdic = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == CurrentUser.IDUsuarioAdicional).FirstOrDefault();
                if (usuAdic.Usuarios.IDUsuarioPadre > 0)
                    Response.Redirect("/home.aspx");
            }

            liPerfilUsuario.Visible = false;
            PerfilUsuario.Visible = false;
            liPerfilUsuario.Attributes.Remove("class");
            PerfilUsuario.Attributes.Remove("class");

            liEmpresas.Attributes.Add("class", "active");
            Empresas.Attributes.Add("class", "tab-pane active");
        }

        if (CurrentUser.IDUsuarioPadre > 0 && CurrentUser.IDUsuarioAdicional > 1)
        {
            usuAdic = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == CurrentUser.IDUsuarioAdicional).FirstOrDefault();
            if (usuAdic.IDUsuario != CurrentUser.IDUsuarioPadre)
            {
                liEmpresas.Visible = false;
                Empresas.Visible = false;
            }
        }
    }

    private void OcultarDatosUsuarioTemplates()
    {
        if (!PermisosModulos.tienePlan("Empresas"))
        {
            liEmpresas.Visible = false;
            Empresas.Visible = false;
            //liAlertasyAvisos.Visible = false;
            //AlertasyAvisos.Visible = false;
        }
    }

    private static bool SuperaLimiteDeEmpresas(ACHEEntities dbContext)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (usu.IDUsuarioPadre == null || usu.IDUsuarioPadre == 0)
        {
            var cantEmpresas = dbContext.Usuarios.Where(x => x.IDUsuarioPadre == usu.IDUsuario).ToList();
            var cantEmpresasHabilitadas = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault().CantidadEmpresas;

            if (cantEmpresas.Count() >= cantEmpresasHabilitadas)
                return true;
        }
        return false;
    }

    [WebMethod(true)]
    public static void guardar(string razonSocial, string condicionIva, string cuit, string iibb, string fechaInicio,
        string personeria, string email, string emailFrom, string emailAlertas, string telefono, string celular, string contacto,
        string idProvincia, string idCiudad, string domicilio, string pisoDepto, string cp, bool esAgentePersepcionIVA,
        bool esAgentePersepcionIIBB, bool esAgenteRetencion, bool exentoIIBB, string fechaCierreContable, string fechaLimiteCarga, string idJurisdiccion)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                UsuarioCommon.GuardarConfiguracion(razonSocial, condicionIva, cuit, iibb, fechaInicio, personeria, emailFrom, emailAlertas, telefono, celular, contacto, idProvincia, idCiudad, domicilio, pisoDepto, cp, esAgentePersepcionIVA, esAgentePersepcionIIBB, esAgenteRetencion, exentoIIBB, fechaCierreContable, fechaLimiteCarga, idJurisdiccion, usu.IDUsuario);
                ActualizarDatosSesion(esAgentePersepcionIVA, esAgentePersepcionIIBB, esAgenteRetencion, (bool)exentoIIBB, iibb, condicionIva, idJurisdiccion);

                usu.RazonSocial = razonSocial;
                usu.CUIT = cuit;
                usu.CondicionIVA = condicionIva;
                usu.IIBB = iibb;
                usu.Telefono = telefono;
                usu.Domicilio = domicilio;
                usu.IDProvincia = int.Parse(idProvincia);
                usu.IDCiudad = int.Parse(idCiudad);
                HttpContext.Current.Session["CurrentUser"] = usu;
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region Punto de venta
    [WebMethod(true)]
    public static void agregarPunto(int punto)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            PuntoDeVentaCommon.GuardarPuntoDeVenta(punto, usu.IDUsuario);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void eliminarPunto(int punto)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            PuntoDeVentaCommon.EliminarPuntoDeVenta(punto, usu.IDUsuario);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string obtenerPuntos()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var list = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == usu.IDUsuario).ToList();
                //var list = UsuarioCommon.ObtenerPuntosDeVenta(usu, true);
                if (list.Any())
                {
                    int index = 1;
                    foreach (var punto in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td>" + punto.Punto.ToString("#0000") + "</td>";
                        html += "<td>" + punto.FechaAlta.ToString("dd/MM/yyyy") + "</td>";
                        if (punto.FechaBaja.HasValue)
                            html += "<td>" + punto.FechaBaja.Value.ToString("dd/MM/yyyy") + "</td>";
                        else
                            html += "<td></td>";

                        if (!punto.FechaBaja.HasValue)
                            html += "<td>" + (!punto.PorDefecto ? "<a title='Poner por defecto' style='font-size: 14px;color:lightgray;' href='javascript:MisDatos.ponerPorDefecto(" + punto.IDPuntoVenta + ");'><i class='fa fa-circle-o''></i></a>" : "<a href='#' title='Punto de venta por defecto' style='font-size: 16px;' );'><i class='fa fa-check' style='color: green;'></i></a>") + "</td>";
                        else
                            html += "<td></td>";
                        if (!punto.FechaBaja.HasValue)
                            html += "<td>" + (!punto.PorDefectoIntegraciones ? "<a title='Poner por defecto para integraciones' style='font-size: 14px;color:lightgray;' href='javascript:MisDatos.ponerPorDefectoIntegraciones(" + punto.IDPuntoVenta + ");'><i class='fa fa-circle-o''></i></a>" : "<a href='#' title='Punto de venta por defecto para integraciones' style='font-size: 16px;' );'><i class='fa fa-check' style='color: green;'></i></a>") + "</td>";
                        else
                            html += "<td></td>";

                        if (!punto.PorDefecto && !punto.FechaBaja.HasValue)
                            html += "<td><a title='Dar de baja' style='font-size: 16px;' href='javascript:MisDatos.eliminarPunto(" + punto.IDPuntoVenta + ");'><i class='fa fa-times'></i></a></td>";
                        else if (punto.FechaBaja.HasValue)
                        {
                            html += "<td><a title='Habilitar punto de venta' style='font-size: 16px;' href='javascript:MisDatos.habilitarPuntoVenta(" + punto.IDPuntoVenta + ");'><i class='fa fa-hand-o-up' style='color: green;'></i></a></td>";
                        }
                        else
                            html += "<td><a title='Dar de baja' style='font-size: 16px;' href='javascript:MisDatos.eliminarPunto(" + punto.IDPuntoVenta + ");'><i class='fa fa-times'></i></a>";
                        index++;
                        html += "</tr>";
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='5' style='text-align:center'>No tienes puntos de venta registrados</td></tr>";

        }
        return html;
    }

    [WebMethod(true)]
    public static void HabilitarPuntoVenta(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                PuntosDeVenta entity = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (entity != null)
                    entity.FechaBaja = null;
                dbContext.SaveChanges();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string GuardarPorDefecto(int idPunto)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var ListaPuntos = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == usu.IDUsuario);

                foreach (var item in ListaPuntos)
                {
                    if (item.IDPuntoVenta == idPunto)
                        item.PorDefecto = true;
                    else
                        item.PorDefecto = false;
                }
                dbContext.SaveChanges();
            }

            return obtenerPuntos();
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string GuardarPorDefectoIntegraciones(int idPunto)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var ListaPuntos = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == usu.IDUsuario);

                foreach (var item in ListaPuntos)
                {
                    if (item.IDPuntoVenta == idPunto)
                        item.PorDefectoIntegraciones = true;
                    else
                        item.PorDefectoIntegraciones = false;
                }
                dbContext.SaveChanges();
            }

            return obtenerPuntos();
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    #endregion

    [WebMethod(true)]
    public static void portalClientes(bool ChkCorreoPortal, bool chkPortalClientes, string clientId, string clientSecret)
    {

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                Usuarios entity = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                entity.CorreoPortal = ChkCorreoPortal;
                entity.PortalClientes = chkPortalClientes;
                entity.MercadoPagoClientID = clientId.Trim();
                entity.MercadoPagoClientSecret = clientSecret.Trim();
                dbContext.SaveChanges();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void ActualizarTemplate(string ddlTemplate)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                Usuarios entity = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                entity.TemplateFc = ddlTemplate;

                dbContext.SaveChanges();

                usu.TemplateFc = ddlTemplate;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static void eliminarFoto()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                Usuarios entity = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                string Serverpath = HttpContext.Current.Server.MapPath("~/files/usuarios/" + entity.Logo);
                entity.Logo = "";
                if (File.Exists(Serverpath))
                {
                    File.Delete(Serverpath);
                    dbContext.SaveChanges();
                }
                dbContext.SaveChanges();
                //HttpContext.Current.Session["CurrentUser"] = TokenCommon.ObtenerWebUser(usu.IDUsuario);
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void guardarConfiguracionPlanDeCuenta(int IDCtaProveedores, int IDCtaIVACreditoFiscal,
        //Percepciones
        int IDPercIIBB, int IDPercIVA, int IDPercIIBBSufrida, int IDPercIVASufrida,
        int IDCtaBancos,
        int IDCtaCaja, int IDCtaValoresADepositar, int IDCtaChequesDif, int IDCtaSircreb, int IDCtaCreditoComputable, int IDCtaComisiones,
        int IDCtaOtrosGastosBancarios, int IDCtaImpDebCred, int IDCtaGastos, int IDCtaInteresBancos, int IDCtaImpSellos,

        //Retenciones
        int IDRetIIBB, int IDRetIVA, int IDRetGanancias, int IDRetSUSS, int IDRetIIBBSufrida, int IDRetIVASufrida, int IDRetGananciasSufrida, int IDRetSUSSSufrida,
        int IDCtaIVADebitoFiscal, int IDCtaDeudoresPorVentas, string ctasFiltrosCompras, string ctasFiltrosVentas, int IDCtaConceptosNoGravadosxCompras, int IDCtaConceptosNoGravadosxVentas,
        int IDAnticipoClientes, int IDAnticipoProveedores, int IDCmv)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                ConfiguracionPlanDeCuenta entity = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                if (entity == null)
                {
                    entity = new ConfiguracionPlanDeCuenta();
                    entity.IDUsuario = usu.IDUsuario;
                }
                //Compras
                entity.IDCtaProveedores = IDCtaProveedores;
                entity.IDCtaIVADebitoFiscal = IDCtaIVADebitoFiscal;
                entity.IDCtaConceptosNoGravadosxCompras = IDCtaConceptosNoGravadosxCompras;
                entity.IDCtaAnticipoProveedores = IDAnticipoProveedores;

                //Ventas
                entity.IDCtaIVACreditoFiscal = IDCtaIVACreditoFiscal;
                entity.IDCtaDeudoresPorVentas = IDCtaDeudoresPorVentas;
                entity.IDCtaConceptosNoGravadosxVentas = IDCtaConceptosNoGravadosxVentas;
                entity.IDCtaAnticipoCliente = IDAnticipoClientes;
                entity.IDCtaCMV = IDCmv;

                //PAGOS
                entity.IDCtaCaja = IDCtaCaja;

                //Percepciones
                entity.IDCtaIIBB = IDPercIIBB;
                entity.IDCtaPercepcionIVA = IDPercIVA;
                entity.IDCtaIIBBSufrida = IDPercIIBBSufrida;
                entity.IDCtaPercepcionIVASufrida = IDPercIVASufrida;

                //Retenciones
                entity.IDctaRetIIBB = IDRetIIBB;
                entity.IDctaRetIVA = IDRetIVA;
                entity.IDctaRetGanancias = IDRetGanancias;
                entity.IDCtaRetSUSS = IDRetSUSS;

                entity.IDCtaRetIIBBSufrida = IDRetIIBBSufrida;
                entity.IDCtaRetIVASufrida = IDRetIVASufrida;
                entity.IDCtaRetGananciasSufrida = IDRetGananciasSufrida;
                entity.IDCtaRetSUSSSufrida = IDRetSUSSSufrida;

                //Bancos
                entity.IDCtaBancos = IDCtaBancos;
                entity.IDCtaValoresADepositar = IDCtaValoresADepositar;
                entity.IDCtaChequesDiferidosAPagar = IDCtaChequesDif;
                entity.IDCtaSircreb = IDCtaSircreb;
                entity.IDCtaCreditoComputable = IDCtaCreditoComputable;
                entity.IDCtaComisiones = IDCtaComisiones;
                entity.IDCtaOtrosGastosBancarios = IDCtaOtrosGastosBancarios;
                entity.IDCtaImpDebCred = IDCtaImpDebCred;
                entity.IDCtaGastosBancarios = IDCtaGastos;
                entity.IDCtaInteresBancos = IDCtaInteresBancos;
                entity.IDCtaImpSellos = IDCtaImpSellos;

                entity.CtasFiltroCompras = ctasFiltrosCompras;
                entity.CtasFiltroVentas = ctasFiltrosVentas;

                if (entity.IDConfiguracionPlanDeCuenta == 0)
                    dbContext.ConfiguracionPlanDeCuenta.Add(entity);

                if (usu.UsaPlanCorporativo)//Plan Corporativo
                    dbContext.SaveChanges();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void guardarConfiguracion(byte cantidadDecimales, bool usaPrecioUnitarioConIva, bool venderSinStock, string obsFc, string obsRemito, string obsPresup, string obsOrdenes)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            if (usu.CondicionIVA == "MO" && !usaPrecioUnitarioConIva)
                throw new Exception("Solo los responsables Inscriptos pueden ingresar el precio final sin IVA");

            using (var dbContext = new ACHEEntities())
            {
                Usuarios entity = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                entity.CantidadDecimales = cantidadDecimales;
                entity.UsaPrecioFinalConIVA = usaPrecioUnitarioConIva;
                entity.ObservacionesFc = obsFc;
                entity.ObservacionesRemito = obsRemito;
                entity.ObservacionesPresupuestos = obsPresup;
                entity.ObservacionesOrdenesPago = obsOrdenes;
                entity.VenderSinStock = venderSinStock;
                dbContext.SaveChanges();

                ActualizarDatosSesion(entity.UsaPrecioFinalConIVA, entity.FechaAlta, entity.EnvioAutomaticoComprobante, entity.EnvioAutomaticoRecibo, obsFc, obsRemito, obsPresup, obsOrdenes, cantidadDecimales);
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static void InsertarPlanDeCuentas(ACHEEntities dbContext)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        var planesCuentas = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
        if (planesCuentas == null)
            dbContext.InsertarPadresPlanesDeCuenta(usu.IDUsuario);
    }

    #region ActualizarDatosSesion

    private static void ActualizarDatosSesion(string TemplateFc, bool AgentePercepcionIVA, bool AgentePercepcionIIBB, bool AgenteRetencion,
        bool exentoIIBB, string nroIIBB, string logo, string condicionIVA, string IDJurisdiccion)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(usu.IDUsuario, usu.CondicionIVA);
            HttpContext.Current.Session["CurrentUser"] = new WebUser(
               usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT, condicionIVA,
               usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia, usu.IDCiudad, usu.Telefono, usu.TieneFE, nroIIBB, usu.FechaInicio,
               logo, TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa,
               usu.ModoQA, usu.IDPlan, usu.EmailAlerta, usu.Provincia, usu.Ciudad, AgentePercepcionIVA, AgentePercepcionIIBB,
               AgenteRetencion, true, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, exentoIIBB, usu.UsaPrecioFinalConIVA,
               usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, IDJurisdiccion, usu.UsaPlanCorporativo,
               usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenes, usu.MostrarBonificacionFc, usu.PorcentajeDescuento);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static void ActualizarDatosSesion(bool AgentePercepcionIVA, bool AgentePercepcionIIBB, bool AgenteRetencion,
       bool exentoIIBB, string nroIIBB, string condicionIVA, string IDJurisdiccion)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(usu.IDUsuario, usu.CondicionIVA);
            HttpContext.Current.Session["CurrentUser"] = new WebUser(
               usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT, condicionIVA,
               usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia, usu.IDCiudad, usu.Telefono, usu.TieneFE, nroIIBB, usu.FechaInicio,
               usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa,
               usu.ModoQA, usu.IDPlan, usu.EmailAlerta, usu.Provincia, usu.Ciudad, AgentePercepcionIVA, AgentePercepcionIIBB,
               AgenteRetencion, true, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, exentoIIBB, usu.UsaPrecioFinalConIVA,
               usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, IDJurisdiccion, usu.UsaPlanCorporativo,
               usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenes, usu.MostrarBonificacionFc, usu.PorcentajeDescuento);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static void ActualizarDatosSesion(bool EnvioFE, bool EnvioCR)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(usu.IDUsuario, usu.CondicionIVA);
            HttpContext.Current.Session["CurrentUser"] = new WebUser(
         usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT, usu.CondicionIVA,
         usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia, usu.IDCiudad, usu.Telefono, usu.TieneFE, usu.IIBB, usu.FechaInicio,
         usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa,
         usu.ModoQA, usu.IDPlan, usu.EmailAlerta, usu.Provincia, usu.Ciudad, usu.AgentePercepcionIVA, usu.AgentePercepcionIIBB,
         usu.AgenteRetencion, true, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA,
         usu.FechaAlta, EnvioFE, EnvioCR, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
         usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenes, usu.MostrarBonificacionFc, usu.PorcentajeDescuento);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static void ActualizarDatosSesion(bool UsaPrecioFinalConIVA, DateTime FechaAlta, bool EnvioAutomaticoComprobante, bool EnvioAutomaticoRecibo, string obsFc, string obsRemito, string obsPresup, string obsOrdenes, byte cantidadDecimales)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(usu.IDUsuario, usu.CondicionIVA);
            HttpContext.Current.Session["CurrentUser"] = new WebUser(
            usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT, usu.CondicionIVA,
            usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia, usu.IDCiudad, usu.Telefono, usu.TieneFE, usu.IIBB, usu.FechaInicio,
            usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa,
            usu.ModoQA, usu.IDPlan, usu.EmailAlerta, usu.Provincia, usu.Ciudad, usu.AgentePercepcionIVA, usu.AgentePercepcionIIBB,
            usu.AgenteRetencion, true, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, UsaPrecioFinalConIVA,
            FechaAlta, EnvioAutomaticoComprobante, EnvioAutomaticoRecibo, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
            obsFc, obsRemito, obsPresup, obsOrdenes, usu.MostrarBonificacionFc, usu.PorcentajeDescuento);

            HttpContext.Current.Session["CantidadDecimales"] = cantidadDecimales;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    #endregion

    [WebMethod(true)]
    public static void GuardarAlertasyAvisos(List<AvisosVencimientoViewModel> listaAvisos, string emailStock)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var plandeCuenta = PermisosModulosCommon.ObtenerPlanActual(dbContext, usu.IDUsuario);
                if (plandeCuenta.IDPlan >= 3)//Plan Pyme
                {
                    var avisos = dbContext.AvisosVencimiento.Where(x => x.IDUsuario == usu.IDUsuario).ToList();
                    foreach (var item in avisos)
                        dbContext.AvisosVencimiento.Remove(item);

                    foreach (var item in listaAvisos)
                    {
                        if ((item.TipoAlerta == "Primer aviso" || item.TipoAlerta == "Segundo aviso" || item.TipoAlerta == "Tercer aviso") && plandeCuenta.IDPlan >= 4)
                        {
                            var entity = new AvisosVencimiento();
                            entity.IDUsuario = usu.IDUsuario;
                            entity.Activa = item.Activa;
                            entity.Asunto = item.Asunto;
                            entity.CantDias = item.CantDias;
                            entity.Mensaje = item.Mensaje;
                            entity.ModoDeEnvio = item.ModoDeEnvio;
                            entity.TipoAlerta = item.TipoAlerta;

                            dbContext.AvisosVencimiento.Add(entity);
                        }
                        else if ((item.TipoAlerta == "Envio FE" || item.TipoAlerta == "Envio CR" || item.TipoAlerta == "Stock") && plandeCuenta.IDPlan >= 3)
                        {
                            var entity = new AvisosVencimiento();
                            entity.IDUsuario = usu.IDUsuario;
                            entity.Activa = item.Activa;
                            entity.Asunto = item.Asunto;
                            entity.CantDias = item.CantDias;
                            entity.Mensaje = item.Mensaje;
                            entity.ModoDeEnvio = item.ModoDeEnvio;
                            entity.TipoAlerta = item.TipoAlerta;
                            dbContext.AvisosVencimiento.Add(entity);
                        }
                    }
                    var usuario = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    usuario.EmailStock = emailStock;
                    dbContext.SaveChanges();
                    ActualizarDatosSecionAvisosYAlertas(dbContext, listaAvisos);
                }
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static void ActualizarDatosSecionAvisosYAlertas(ACHEEntities dbContext, List<AvisosVencimientoViewModel> listaAvisos)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        var EnvioFE = false;
        var EnvioCR = false;
        foreach (var item in listaAvisos)
        {
            if (listaAvisos.Any(x => x.TipoAlerta == "Envio FE" && x.Activa))
                EnvioFE = true;
            if (listaAvisos.Any(x => x.TipoAlerta == "Envio CR" && x.Activa))
                EnvioCR = true;
        }

        var entity = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
        entity.EnvioAutomaticoComprobante = EnvioFE;
        entity.EnvioAutomaticoRecibo = EnvioCR;
        dbContext.SaveChanges();
        ActualizarDatosSesion(EnvioFE, EnvioCR);
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static List<AvisosVencimientoViewModel> ObtenerAvisosVencimientos()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var lista = dbContext.AvisosVencimiento.Where(x => x.IDUsuario == usu.IDUsuario).ToList().Select(x => new AvisosVencimientoViewModel()
                {
                    //IDAvisosVencimientos = x.IDAvisosVencimientos,
                    // IDUsuario = x.IDUsuario,
                    Activa = x.Activa,
                    TipoAlerta = x.TipoAlerta,
                    ModoDeEnvio = x.ModoDeEnvio,
                    CantDias = x.CantDias,
                    Asunto = x.Asunto,
                    Mensaje = x.Mensaje,
                }).ToList();

                return lista;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    public class AvisosVencimientoViewModel
    {
        public int IDAvisosVencimientos { get; set; }
        public int IDUsuario { get; set; }
        public bool Activa { get; set; }
        public string TipoAlerta { get; set; }
        public string ModoDeEnvio { get; set; }
        public int CantDias { get; set; }
        public string Asunto { get; set; }
        public string Mensaje { get; set; }
    }

    //PLANES Y PAGOS
    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static ResultadosPlanDePagosViewModel ObtenerHistorialPagos()
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.PlanesPagos.Where(x => x.IDUsuario == usu.IDUsuario).ToList();
                    var planActual = results.Where(x => x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();
                    ResultadosPlanDePagosViewModel resultado = new ResultadosPlanDePagosViewModel();

                    if (planActual != null)
                    {
                        resultado.IDPlanActual = planActual.IDPlan;
                        resultado.NombrePlanActual = planActual.Planes.Nombre;
                        resultado.FechaVencimiento = Convert.ToDateTime(planActual.FechaFinPlan).ToString("dd/MM/yyyy");
                    }
                    var list = results.OrderByDescending(x => x.IDPlanesPagos).Take(12).ToList()
                        .Select(x => new PlanDePagosViewModel()
                        {
                            TipoDePlan = x.Planes.Nombre,
                            FechaDePago = x.FechaDePago.ToString("dd/MM/yyyy"),
                            ImportePagado = x.ImportePagado.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()),
                            FomaDePago = x.FormaDePago,
                            NroReferencia = x.NroReferencia,
                            Estado = x.Estado,
                            FechaInicio = Convert.ToDateTime(x.FechaInicioPlan).ToString("dd/MM/yyyy"),
                            FechaVencimiento = Convert.ToDateTime(x.FechaFinPlan).ToString("dd/MM/yyyy"),
                        });
                    resultado.Items = list.ToList();
                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    // EMPRESA
    [WebMethod(true)]
    public static void CrearEmpresa(string razonSocial, string condicionIva, string cuit, string personeria, string email, string pwd, int idProvincia, int idCiudad, string domicilio, string pisoDepto)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                if (!cuit.IsValidCUIT())
                    throw new Exception("El CUIT ingresado es incorrecto");
                else
                {
                    if (dbContext.Usuarios.Any(x => x.Email == email))
                        throw new Exception("El E-mail ingresado ya se encuentra registrado.");
                    else if (dbContext.Usuarios.Any(x => x.CUIT == cuit))
                        throw new Exception("El CUIT ingresado ya se encuentra registrado.");
                    if (dbContext.UsuariosAdicionales.Any(x => x.Email == email))
                        throw new Exception("El E-mail ingresado ya se encuentra registrado.");
                    else if (SuperaLimiteDeEmpresas(dbContext))
                        throw new Exception("Superó el máximo de empresas permitidas. Si desea obtener más comuníquese con nosotros al (11) 5263-2062.");
                    else
                    {
                        var currentUsuario = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                        Usuarios entity = new Usuarios();
                        entity.FechaAlta = DateTime.Now;
                        entity.IDUsuarioPadre = usu.IDUsuario;

                        entity.Theme = "default";
                        entity.Pais = "Argentina";
                        entity.TemplateFc = "default";
                        entity.Pwd = "";
                        entity.TieneFacturaElectronica = false;
                        entity.Logo = null;
                        entity.Activo = true;
                        entity.IDPlan = 4;
                        entity.FechaUltLogin = DateTime.Now;
                        entity.SetupRealizado = true;
                        entity.CorreoPortal = currentUsuario.CorreoPortal;
                        entity.PortalClientes = currentUsuario.PortalClientes;
                        entity.FechaFinPlan = DateTime.Now.AddDays(30);
                        entity.UsaProd = currentUsuario.UsaProd;
                        entity.UsaFechaFinPlan = false;
                        entity.UsaPlanCorporativo = usu.UsaPlanCorporativo;

                        entity.RazonSocial = razonSocial;
                        entity.EmailFrom = razonSocial;

                        entity.CondicionIva = condicionIva;
                        entity.CUIT = cuit;
                        entity.IIBB = string.Empty;
                        entity.Personeria = personeria;
                        entity.Email = email;
                        entity.EmailAlertas = email;
                        entity.EmailStock = email;
                        entity.Telefono = string.Empty;
                        entity.Celular = string.Empty;
                        entity.Contacto = string.Empty;
                        //Domicilio
                        entity.IDProvincia = idProvincia;
                        entity.IDCiudad = idCiudad;
                        entity.Domicilio = domicilio;
                        entity.PisoDepto = pisoDepto;
                        entity.CodigoPostal = string.Empty;

                        //if (entity.CondicionIva == "MO")
                        //    entity.UsaPrecioFinalConIVA = true;
                        //else
                        //    entity.UsaPrecioFinalConIVA = false;

                        entity.EsAgentePercepcionIVA = false;
                        entity.EsAgentePercepcionIIBB = false;
                        entity.IDJurisdiccion = entity.IDProvincia.ToString();
                        entity.EsAgenteRetencion = false;
                        entity.CantidadEmpresas = 0;
                        entity.ExentoIIBB = false;
                        entity.UsaPrecioFinalConIVA = true;
                        entity.EnvioAutomaticoComprobante = false;
                        entity.EnvioAutomaticoRecibo = false;
                        entity.EsContador = false;
                        //entity.UsaPlanCorporativo = false;
                        entity.MostrarBonificacionFc = true;
                        entity.PorcentajeDescuento = 0;
                        entity.VenderSinStock = currentUsuario.VenderSinStock;
                        entity.TieneDebitoAutomatico = currentUsuario.TieneDebitoAutomatico;

                        PuntosDeVenta punto = new PuntosDeVenta();
                        punto.FechaAlta = DateTime.Now;
                        punto.Punto = 1;
                        punto.PorDefecto = true;
                        entity.PuntosDeVenta.Add(punto);
                        entity.ApiKey = Guid.NewGuid().ToString().Replace("-", "");

                        Rubros rubro = new Rubros();
                        rubro.Nombre = "General";
                        rubro.Activo = true;
                        entity.Rubros.Add(rubro);

                        Inventarios inventario = new Inventarios();
                        //inventario.IDPuntoVenta = punto.IDPuntoVenta;
                        inventario.FechaAlta = DateTime.Now;
                        inventario.Nombre = "Principal";
                        inventario.IDProvincia = 2;
                        inventario.IDCiudad = 5003;
                        inventario.Direccion = string.Empty;
                        inventario.Telefono = entity.Telefono;
                        inventario.FechaUltimoInventarioFisico = DateTime.Now;
                        inventario.Activo = true;
                        entity.Inventarios.Add(inventario);

                        if (cuit.StartsWith("30"))
                        {
                            punto = new PuntosDeVenta();
                            punto.FechaAlta = DateTime.Now;
                            punto.Punto = 2;
                            punto.PorDefecto = false;
                            entity.PuntosDeVenta.Add(punto);
                        }

                        Cajas caja = new Cajas();
                        caja.Nombre = "Caja pesos";
                        entity.Cajas.Add(caja);

                        Categorias cat = new Categorias();
                        cat.Nombre = "General";
                        entity.Categorias.Add(cat);

                        Categorias cat2 = new Categorias();
                        cat2.Nombre = "Honorarios";
                        entity.Categorias.Add(cat2);

                        Categorias cat3 = new Categorias();
                        cat3.Nombre = "Gastos varios";
                        entity.Categorias.Add(cat3);

                        Categorias cat4 = new Categorias();
                        cat4.Nombre = "Nafta";
                        entity.Categorias.Add(cat4);

                        Categorias cat5 = new Categorias();
                        cat5.Nombre = "Equipamiento";
                        entity.Categorias.Add(cat5);

                        Bancos banco = new Bancos();
                        banco.Moneda = "Pesos Argentinos";
                        banco.SaldoInicial = 0;
                        banco.NroCuenta = "";
                        banco.IDBancoBase = dbContext.BancosBase.Where(x => x.Nombre == "Default").FirstOrDefault().IDBancoBase;
                        banco.FechaAlta = DateTime.Now;
                        banco.Activo = true;
                        entity.Bancos.Add(banco);

                        Bancos banco2 = new Bancos();
                        banco2.Moneda = "Pesos Argentinos";
                        banco2.SaldoInicial = 0;
                        banco2.NroCuenta = "";
                        banco2.IDBancoBase = 80;//MercadoPago dbContext.BancosBase.Where(x => x.Nombre == "Default").FirstOrDefault().IDBancoBase;
                        banco2.FechaAlta = DateTime.Now;
                        banco2.Activo = true;
                        entity.Bancos.Add(banco2);


                        #region cond ventas

                        CondicionesVentas condVenta1 = new CondicionesVentas();
                        condVenta1.Activa = true;
                        condVenta1.FechaAlta = DateTime.Now;
                        condVenta1.Nombre = "Efectivo";
                        condVenta1.CobranzaAutomatica = true;
                        condVenta1.FormaDePago = "Efectivo";
                        condVenta1.Cajas = caja;
                        entity.CondicionesVentas.Add(condVenta1);

                        CondicionesVentas condVenta2 = new CondicionesVentas();
                        condVenta2.Activa = true;
                        condVenta2.FechaAlta = DateTime.Now;
                        condVenta2.Nombre = "Cheque";
                        condVenta2.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta2);

                        CondicionesVentas condVenta3 = new CondicionesVentas();
                        condVenta3.Activa = true;
                        condVenta3.FechaAlta = DateTime.Now;
                        condVenta3.Nombre = "Cuenta corriente";
                        condVenta3.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta3);

                        CondicionesVentas condVenta4 = new CondicionesVentas();
                        condVenta4.Activa = true;
                        condVenta4.FechaAlta = DateTime.Now;
                        condVenta4.Nombre = "MercadoPago";
                        condVenta4.CobranzaAutomatica = true;
                        condVenta4.FormaDePago = "Transferencia";
                        condVenta4.Bancos = banco2;//AVISAR QUE NO SE CREA EL PLAN DE CUENTA
                        entity.CondicionesVentas.Add(condVenta4);

                        CondicionesVentas condVenta5 = new CondicionesVentas();
                        condVenta5.Activa = true;
                        condVenta5.FechaAlta = DateTime.Now;
                        condVenta5.Nombre = "Tarjeta de debito";
                        condVenta5.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta5);

                        CondicionesVentas condVenta6 = new CondicionesVentas();
                        condVenta6.Activa = true;
                        condVenta6.FechaAlta = DateTime.Now;
                        condVenta6.Nombre = "Tarjeta de credito";
                        condVenta6.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta6);

                        CondicionesVentas condVenta7 = new CondicionesVentas();
                        condVenta7.Activa = true;
                        condVenta7.FechaAlta = DateTime.Now;
                        condVenta7.Nombre = "Ticket";
                        condVenta7.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta7);

                        CondicionesVentas condVenta8 = new CondicionesVentas();
                        condVenta8.Activa = true;
                        condVenta8.FechaAlta = DateTime.Now;
                        condVenta8.Nombre = "PayU";
                        condVenta8.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta8);

                        #endregion


                        UsuariosAdicionales usuariosAdicionales = new UsuariosAdicionales();
                        usuariosAdicionales.Email = email;
                        usuariosAdicionales.Activo = true;
                        usuariosAdicionales.FechaAlta = DateTime.Now.Date;
                        usuariosAdicionales.Pwd = pwd;
                        usuariosAdicionales.Tipo = "A";
                        entity.UsuariosAdicionales.Add(usuariosAdicionales);

                        UsuariosEmpresa usuariosEmpresa = new UsuariosEmpresa();//Agrego la empresa creada al usuario adicional
                        usuariosEmpresa.IDUsuarioAdicional = usuariosAdicionales.IDUsuarioAdicional;
                        entity.UsuariosEmpresa.Add(usuariosEmpresa);

                        dbContext.Usuarios.Add(entity);

                        //da error
                        /*PuntosDeVentaUsuariosAdicionales pdvUsuariosAdicionales = new PuntosDeVentaUsuariosAdicionales();//Agrego el punto de venta creado al usuario adicional
                        pdvUsuariosAdicionales.IDPuntoVenta = punto.IDPuntoVenta;
                        pdvUsuariosAdicionales.IDUsuarioAdicional = usuariosAdicionales.IDUsuarioAdicional;
                        dbContext.PuntosDeVentaUsuariosAdicionales.Add(pdvUsuariosAdicionales);*/

                        //dbContext.Usuarios.Add(entity);
                        dbContext.SaveChanges();

                        if (usu.UsaPlanCorporativo)//Plan Corporativo
                        {
                            dbContext.ConfigurarPlanCorporativo(entity.IDUsuario);
                            var listaBancos = dbContext.Bancos.Where(x => x.IDUsuario == entity.IDUsuario).ToList();
                            var usuEmpresa = TokenCommon.ObtenerWebUser(entity.IDUsuario);
                            foreach (var item in listaBancos)
                                ContabilidadCommon.CrearCuentaBancos(item.IDBanco, null, usuEmpresa);

                            var listaCajas = dbContext.Cajas.Where(x => x.IDUsuario == entity.IDUsuario).ToList();
                            foreach (var item in listaCajas)
                                ContabilidadCommon.CrearCuentaCaja(item.IDCaja, usuEmpresa);
                        }
                    }
                }
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static ResultadosEmpresasViewModel getResults()
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                //int idusuarioLogiado = 0;
                using (var dbContext = new ACHEEntities())
                {
                    List<EmpresasViewModel> list = UsuarioCommon.ListaEmpresasDisponibles(usu, dbContext);

                    ResultadosEmpresasViewModel resultado = new ResultadosEmpresasViewModel();
                    resultado.SuperoLimite = SuperaLimiteDeEmpresas(dbContext);
                    resultado.TotalPage = 1;
                    resultado.UsuLogiado = usu.IDUsuario.ToString();
                    resultado.TotalItems = list.Count();
                    resultado.Items = list.OrderBy(x => x.RazonSocial).ToList();

                    return resultado;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}
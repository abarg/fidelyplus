﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usuariose : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (CurrentUser.TipoUsuario != "A")
                Response.Redirect("/home.aspx");

            if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0") {
                    cargarEntidad(int.Parse(hdnID.Value));
                    litPath.Text = "Edición";
                    txtPwd.CssClass = "form-control";
                    litPwd.Text = "<label class='control-label'> Contraseña</label>";
                }
            }
            else {
                litPath.Text = "Alta";
                txtEmail.Text = "email@email.com";
                txtPwd.CssClass = "form-control required";
                litPwd.Text = "<label class='control-label'><span class='asterisk'>*</span> Contraseña</label>";
                chkActivo.Checked = true;
            }
        }
    }

    private void cargarEntidad(int id) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.UsuariosAdicionales.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDUsuarioAdicional == id).FirstOrDefault();
            if (entity != null) {
                txtEmail.Text = entity.Email.ToUpper();
                ddlTipo.Text = entity.Tipo;
                chkActivo.Checked = entity.Activo;
                txtPorcentajeComision.Text = entity.PorcentajeComision.ToString();
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static void guardar(int id, string email, string tipo, string pwd, bool activo, string porcentajeComision, string accesos) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities()) {
                if (dbContext.UsuariosAdicionales.Any(x => x.Email == email && x.IDUsuarioAdicional != id))
                    throw new Exception("El Email ingresado ya se encuentra registrado.");
                if (dbContext.Usuarios.Any(x => x.Email == email && x.IDUsuarioPadre == null))
                    throw new Exception("El E-mail ingresado ya se encuentra registrado.");
                if (!accesos.Contains("chkEmpresa"))
                    throw new Exception("Debe seleccionar al menos una empresa para acceso.");
               

                UsuariosAdicionales entity;
                if (id > 0)
                    entity = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                else {
                    entity = new UsuariosAdicionales();
                    entity.FechaAlta = DateTime.Now;
                    entity.IDUsuario = usu.IDUsuario;
                }

                entity.Email = email.ToUpper();
                entity.Tipo = tipo;
                entity.CantIntentos = 0;
                entity.EstaBloqueado = false;
                if (!string.IsNullOrWhiteSpace(pwd))
                    entity.Pwd = pwd;

                entity.Activo = activo;
                if (!string.IsNullOrWhiteSpace(porcentajeComision))
                    entity.PorcentajeComision = int.Parse(porcentajeComision);
                else
                    entity.PorcentajeComision = null;
                if (id > 0)
                    dbContext.SaveChanges();
                else {
                    dbContext.UsuariosAdicionales.Add(entity);
                    dbContext.SaveChanges();
                }
                vincularPermisosUsuariosAdicionales(accesos, entity, usu);
                vincularEmpresaUsuario(accesos, entity, usu);
                vincularRolesUsuariosAdicionales(accesos, entity, usu);
                vincularPuntosDeVentaUsuariosAdicionales(accesos, entity, usu);
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    public static void vincularEmpresaUsuario(string idEmpresas, UsuariosAdicionales usuAd, WebUser usu) {
        using (var dbContext = new ACHEEntities()) {
            var usuariosEmpresas = dbContext.UsuariosEmpresa.Where(x => x.IDUsuarioAdicional == usuAd.IDUsuarioAdicional).ToList();
            foreach (var item in usuariosEmpresas) {
                dbContext.UsuariosEmpresa.Remove(item);
            }

            var lista = Regex.Split(idEmpresas, "#-#").ToList();
            lista.RemoveAt(0);
            for (int i = 0; i < lista.Count; i++) {
                var flagChk = Regex.Split(lista[i], "_")[0];
                if (flagChk != "chkEmpresa")
                    continue;

                var idEmpresa = Convert.ToInt32(Regex.Split(lista[i], "_")[1]);

                UsuariosEmpresa entity = new UsuariosEmpresa();
                entity.IDUsuario = idEmpresa;
                entity.IDUsuarioAdicional = usuAd.IDUsuarioAdicional;

                dbContext.UsuariosEmpresa.Add(entity);
            }
            dbContext.SaveChanges();
        }
    }

    public static void vincularRolesUsuariosAdicionales(string idRoles, UsuariosAdicionales usuAd, WebUser usu) {
        using (var dbContext = new ACHEEntities()) {
            var RolesUsuariosAD = dbContext.RolesUsuariosAdicionales.Where(x => x.IDUsuarioAdicional == usuAd.IDUsuarioAdicional).ToList();
            foreach (var item in RolesUsuariosAD)
                dbContext.RolesUsuariosAdicionales.Remove(item);
            var RolesPersonalizadosUsuariosAD = dbContext.RolesPersonalizadosUsuariosAdicionales.Where(x => x.IDUsuarioAdicional == usuAd.IDUsuarioAdicional).ToList();
            foreach (var item in RolesPersonalizadosUsuariosAD)
                dbContext.RolesPersonalizadosUsuariosAdicionales.Remove(item);

            if (usuAd.Tipo == "B") {
                var lista = Regex.Split(idRoles, "#-#").ToList();
                lista.RemoveAt(0);
                for (int i = 0; i < lista.Count; i++) {
                    var flagChk = Regex.Split(lista[i], "_")[0];
                    if (flagChk != "chkModulos")
                        continue;

                    var idRol = Convert.ToInt32(Regex.Split(lista[i], "_")[1]);
                    var entity = new RolesUsuariosAdicionales();
                    entity.IDRol = idRol;
                    entity.IDUsuarioAdicional = usuAd.IDUsuarioAdicional;
                    dbContext.RolesUsuariosAdicionales.Add(entity);
                }
            }
            if (usuAd.Tipo == "C") //personalizado
            {
                var lista = Regex.Split(idRoles, "#-#").ToList();
                var roles = new List<int>();
                lista.RemoveAt(0);
                for (int i = 0; i < lista.Count; i++) {
                    var flagChk = Regex.Split(lista[i], "_")[0];
                    if (flagChk != "chkFuncionalidades")
                        continue;

                    var idRol = Convert.ToInt32(Regex.Split(lista[i], "_")[1]);
                    var entity = new RolesPersonalizadosUsuariosAdicionales();
                    entity.IDRol = idRol;
                    entity.IDUsuarioAdicional = usuAd.IDUsuarioAdicional;
                    var idFormulario = Convert.ToInt32(Regex.Split(lista[i], "_")[2]);
                    entity.IDFormulario = idFormulario;
                    dbContext.RolesPersonalizadosUsuariosAdicionales.Add(entity);

                    var formulario = dbContext.Formularios.Where(x => x.IDFormulario == idFormulario).FirstOrDefault();
                    if (formulario.Grupo != null) {// Agrego los formulario del grupo.
                        foreach (var item in dbContext.Formularios.Where(x => x.Grupo == formulario.Grupo && x.IDFormulario != formulario.IDFormulario).ToList()) {
                            var entityGrupo = new RolesPersonalizadosUsuariosAdicionales();
                            entityGrupo.IDRol = idRol;
                            entityGrupo.IDUsuarioAdicional = usuAd.IDUsuarioAdicional;
                            entityGrupo.IDFormulario = item.IDFormulario;
                            dbContext.RolesPersonalizadosUsuariosAdicionales.Add(entityGrupo);
                        }
                    }
                    roles.Add(idRol);
                }
                //se agregan los formularios del rol que no son personalizables
                foreach (var idrol in roles.Distinct()) {
                    var rol = dbContext.Roles.FirstOrDefault(r => r.IDRol == idrol);
                    if (rol != null)
                        rol.Formularios.Where(f => !f.Personalizable).ToList().ForEach(
                            x => {
                                var rolPersonalizado = new RolesPersonalizadosUsuariosAdicionales();
                                rolPersonalizado.IDRol = idrol;
                                rolPersonalizado.IDFormulario = x.IDFormulario;
                                rolPersonalizado.IDUsuarioAdicional = usuAd.IDUsuarioAdicional;
                                dbContext.RolesPersonalizadosUsuariosAdicionales.Add(rolPersonalizado);
                            });
                }
            }
            dbContext.SaveChanges();
        }
    }

    public static void vincularPermisosUsuariosAdicionales(string idRoles, UsuariosAdicionales usuAd, WebUser usu) {
        using (var dbContext = new ACHEEntities()) {
            var usuario =
                dbContext.UsuariosAdicionales.Include("Permisos").FirstOrDefault(x => x.IDUsuarioAdicional == usuAd.IDUsuarioAdicional);

            foreach (var item in usuario.Permisos.ToList())
                usuario.Permisos.Remove(item);


            var lista = Regex.Split(idRoles, "#-#").ToList();
            lista.RemoveAt(0);
            for (int i = 0; i < lista.Count; i++) {
                var flagChk = Regex.Split(lista[i], "_")[0];
                if (flagChk != "chkPermisos")
                    continue;

                var idPermiso = Convert.ToInt32(Regex.Split(lista[i], "_")[1]);
                usuario.Permisos.Add(dbContext.Permisos.FirstOrDefault(p => p.IDPermiso == idPermiso));

            }

            dbContext.SaveChanges();

        }
    }

    public static void vincularPuntosDeVentaUsuariosAdicionales(string idPuntosDeVenta, UsuariosAdicionales usuAd, WebUser usu) {
        using (var dbContext = new ACHEEntities()) {
            var PuntosDeVentaUsuariosAD = dbContext.PuntosDeVentaUsuariosAdicionales.Where(x => x.IDUsuarioAdicional == usuAd.IDUsuarioAdicional).ToList();
            foreach (var item in PuntosDeVentaUsuariosAD)
                dbContext.PuntosDeVentaUsuariosAdicionales.Remove(item);

            var lista = Regex.Split(idPuntosDeVenta, "#-#").ToList();
            lista.RemoveAt(0);
            for (int i = 0; i < lista.Count; i++) {
                var flagChk = Regex.Split(lista[i], "_")[0];
                if (flagChk != "chkPuntosDeVenta")
                    continue;

                //var idPuntoDeVenta = Convert.ToInt32(Regex.Split(lista[i], "_")[1]);
                var idPuntoDeVenta = Convert.ToInt32(Regex.Split(lista[i], "_")[2]);
                var entity = new PuntosDeVentaUsuariosAdicionales();
                entity.IDPuntoVenta = idPuntoDeVenta;
                entity.IDUsuarioAdicional = usuAd.IDUsuarioAdicional;

                dbContext.PuntosDeVentaUsuariosAdicionales.Add(entity);
            }
            dbContext.SaveChanges();
        }
    }

}
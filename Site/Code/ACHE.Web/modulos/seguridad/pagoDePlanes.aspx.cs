﻿using ACHE.Model;
using ACHE.Negocio.Contabilidad;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Negocio.Common;
using System.Security.Cryptography;
using System.Text;

public partial class modulos_ventas_pagoDePlanes : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFechaDePago.Text = DateTime.Now.ToString("dd/MM/yyyy");
            var plan = Request.QueryString["plan"];
            var modo = Request.QueryString["modo"];
            if (!string.IsNullOrWhiteSpace(plan))
            {
                using (var dbContext = new ACHEEntities())
                {
                    var planSeleccionado = dbContext.Planes.Where(x => x.Nombre.ToUpper().Contains(plan.ToUpper())).FirstOrDefault();
                    if (planSeleccionado != null)
                    {
                        hdnIdPlan.Value = planSeleccionado.IDPlan.ToString();

                        if (hdnIdPlan.Value == "6")
                            Response.Redirect("/modulos/seguridad/elegir-plan.aspx?upgrade=0");

                        var NombrePlan = planSeleccionado.Nombre;
                        decimal importeTotal = planSeleccionado.Precio;
                        decimal importePlan = planSeleccionado.Precio;

                        //MODIFICAR TMB MERCADOPAGO.ASHX !!! Metodo: agregarPago
                        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];// hardcodeado por estos 6 meses para los usuarios que ya fueron creados antes de 31/07/2015
                        var fecha = Convert.ToDateTime("20/06/2018");

                        if (usu.FechaAlta >= fecha)// || usu.FechaAlta <= Convert.ToDateTime("01/08/2018"))
                        {
                            switch (planSeleccionado.IDPlan)
                            {
                                case 2:
                                    importeTotal = 680;
                                    break;
                                case 3:
                                    importeTotal = 799;
                                    break;
                                case 4:
                                    importeTotal = 999;
                                    break;
                                case 5:
                                    importeTotal = 1199;
                                    break;
                            }
                        }
                        else
                        {
                            switch (planSeleccionado.IDPlan)
                            {
                                case 2:
                                    importeTotal = 549;
                                    break;
                                case 3:
                                    importeTotal = 649;
                                    break;
                                case 4:
                                    importeTotal = 849;
                                    break;
                                case 5:
                                    importeTotal = 999;
                                    break;
                            }
                        }

                        importePlan = importeTotal;

                        if (modo == "A")
                        { //es anual
                            importeTotal = importeTotal * 12;
                            //frmDebitoAutomatico.Visible = false;
                        }
                        if (usu.PorcentajeDescuento.HasValue && usu.PorcentajeDescuento.Value > 0)
                        {
                            decimal descuento = (1 - (usu.PorcentajeDescuento.Value / 100));
                            importeTotal = importeTotal * descuento;
                        }

                        if (modo == "A")//es anual
                        {
                            decimal descuento = importeTotal * Convert.ToDecimal("0,20");
                            importeTotal = importeTotal - descuento;
                        }

                        //Si contrató apps adicionales mensuales se deben sumar al importe
                        decimal importeAddins = AddinsCommon.ObtenerPrecioAddinsMensuales(usu.IDUsuario);
                        if (importeAddins > 0)
                        {
                            importeAddins = (modo == "A") ? importeAddins * 12 : importeAddins;
                            if (modo == "A")//es anual
                            {
                                decimal descuentoAddins = importeAddins * Convert.ToDecimal("0,20");
                                importeAddins = importeAddins - descuentoAddins;
                            }

                            importeTotal += importeAddins;
                        }

                        importeTotal = importeTotal * 1.21M;

                        hdnImporteTotal.Value = importeTotal.ToString("N2");
                        litPlanImporte.Text = importeTotal.ToString("N2");
                        hdnNombrePlan.Value = NombrePlan;
                        hdnModo.Value = modo;
                        btnMercadoPago.Text = integracionMercadoPago(planSeleccionado.IDPlan, NombrePlan, (modo == "A"), importeTotal, importePlan);

                        //PayU
                        string merchantId = "649583";
                        string accountId = "652060";
                        string description = "Pago del plan: " + NombrePlan + " - " + importeTotal.ToString("N2") + " + IVA";
                        string referenceCode = usu.IDUsuario + "|" + planSeleccionado.IDPlan + "|" + DateTime.Now.ToString("ddMMyyyyHHmmss");
                        string amount = importeTotal.ToString("#0.00").Replace(",", ".");
                        //buyerEmail.Value = usu.Email;
                        //payerFullName.Value = usu.RazonSocial;
                        //signature
                        var signature = "UV4DKNDBQIVBQrJRUqyIZlC8uS~" + merchantId + "~" + referenceCode + "~" + amount + "~ARS";
                        var hashResult = MD5Hash(signature);


                        var btnPayu = @"<input id='merchantId' name='merchantId' type='hidden' value='649583'  />
                        <input id='accountId' name='accountId' type='hidden' value='652060' />";
                        btnPayu += @" <input id='description' name='description' type='hidden' value='" + description + "' />";
                        btnPayu += @"<input id='referenceCode' name='referenceCode' type='hidden' value='" + referenceCode + "' />";
                        btnPayu += @"<input id='amount' name='amount' type='hidden' value='" + amount + "' />";

                        btnPayu += @"<input id='tax' type='hidden' value='0' />
                        <input id='taxReturnBase' type='hidden' value='0' />
                        <input id='currency' type='hidden' value='ARS' />";
                        btnPayu += @"<input id='signature' name='signature' type='hidden' value='" + hashResult + "' />";
                        btnPayu += @"<input id='buyerEmail' name='buyerEmail' type='hidden' value='" + usu.Email + "' />";
                        btnPayu += @"<input id='payerFullName' name='payerFullName' type='hidden' value='" + usu.RazonSocial + "' />";
                        btnPayu += @"<input id='responseUrl' type='hidden' value='https://app.contabilium.com/modulos/seguridad/pago-realizado.aspx' />";

                        litPayU.Text = btnPayu;
                    }
                }
            }
        }
    }

    public static string MD5Hash(string text)
    {
        MD5 md5 = new MD5CryptoServiceProvider();

        //compute hash from the bytes of text
        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

        //get hash result after compute it
        byte[] result = md5.Hash;

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < result.Length; i++)
        {
            //change it into 2 hexadecimal digits
            //for each byte
            strBuilder.Append(result[i].ToString("x2"));
        }

        return strBuilder.ToString();
    }

    [WebMethod(true)]
    public static int GuardarPago(int id, int idPlan, string formaDePago, string importePagado, string fechaDePago, string nroReferencia, string pagoAnual)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    PlanesPagos p;
                    if (id > 0)
                    {
                        p = dbContext.PlanesPagos.Where(x => x.IDPlanesPagos == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    }
                    else
                    {
                        p = new PlanesPagos();
                        p.FechaDeAlta = DateTime.Now.Date;
                        p.IDUsuario = usu.IDUsuario;

                        var ultimoPago = dbContext.PlanesPagos.Where(x => x.IDUsuario == usu.IDUsuario && x.Estado == "Aceptado").OrderByDescending(x => x.IDPlanesPagos).FirstOrDefault();

                        p.IDPlan = idPlan;
                        p.ImportePagado = decimal.Parse(importePagado.Replace(".", ","));
                        p.PagoAnual = (pagoAnual.ToUpper() == "A");
                        p.FormaDePago = formaDePago;
                        p.NroReferencia = nroReferencia;
                        p.FechaDePago = Convert.ToDateTime(fechaDePago);
                        p.Estado = "Pendiente";

                        if (ultimoPago == null)
                            p.FechaInicioPlan = p.FechaDePago;
                        else
                            p.FechaInicioPlan = ultimoPago.FechaFinPlan;

                        p.FechaFinPlan = (p.PagoAnual) ? Convert.ToDateTime(p.FechaInicioPlan).AddDays(365) : Convert.ToDateTime(p.FechaInicioPlan).AddDays(30);

                        dbContext.PlanesPagos.Add(p);
                        dbContext.SaveChanges();

                        var idPlanActual = PermisosModulos.ObtenerTodosLosFormularios(dbContext, usu.IDUsuario);
                        HttpContext.Current.Session["CurrentUser"] = new WebUser(
                       usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT, usu.CondicionIVA,
                       usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia,
                       usu.IDCiudad, usu.Telefono, usu.TieneFE, usu.IIBB, usu.FechaInicio,
                       usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa, usu.ModoQA, idPlanActual,
                       usu.EmailAlerta, usu.Provincia, usu.Ciudad, usu.AgentePercepcionIVA, usu.AgentePercepcionIIBB, usu.AgenteRetencion,
                       true, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA, usu.FechaAlta,
                       usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, false,
                       usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenes, usu.MostrarBonificacionFc, usu.PorcentajeDescuento, false);

                        enviarEmail(p);
                    }
                    return p.IDPlanesPagos;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static bool AdherirDebitoAutomatico(string cbu, string importeTotal, string planNombre, string esAnual)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {

                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<PLAN>", planNombre);
                    replacements.Add("<TIPOPLAN>", esAnual == "M" ? "Mensual" : "Anual");
                    replacements.Add("<USUARIO>", usu.RazonSocial);
                    replacements.Add("<ID>", usu.IDUsuario);
                    replacements.Add("<EMAIL>", usu.Email);
                    replacements.Add("<CBU>", cbu);
                    replacements.Add("<IMPORTE>", importeTotal);

                    //replacements.Add("<NOTIFICACION>", "Se están corroborando los datos de tu pago. Pronto confirmaremos el mismo.<br><br>Gracias por seguir confiando en Contabilium! Nuestro equipo trabaja para brindarte la mejor atención siempre.");

                    bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.DebitoAutomatico, replacements, ConfigurationManager.AppSettings["Email.Administracion"], "Contabilium: Adhesion a débito automático");
                    if (!send)
                        throw new Exception("No se pudo procesar la solicitud. Intenta nuevamente");
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
            return true;
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static void GuardarPlanBasico()
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                using (var dbContext = new ACHEEntities())
                {
                    var ultimoPago = dbContext.PlanesPagos.Where(x => x.IDUsuario == usu.IDUsuario && x.Estado == "Aceptado").OrderByDescending(x => x.IDPlanesPagos).FirstOrDefault();

                    var idPlan = 1;
                    PlanesPagos p = new PlanesPagos();
                    p.IDUsuario = usu.IDUsuario;
                    p.IDPlan = idPlan;
                    p.FechaDeAlta = DateTime.Now.Date;
                    p.ImportePagado = 0;
                    p.PagoAnual = false;
                    p.FormaDePago = "-";
                    p.NroReferencia = "";
                    p.FechaDePago = DateTime.Now.Date;
                    p.Estado = "Aceptado";

                    if (ultimoPago == null)
                        p.FechaInicioPlan = p.FechaDePago;
                    else
                        p.FechaInicioPlan = ultimoPago.FechaFinPlan;

                    p.FechaFinPlan = Convert.ToDateTime(p.FechaInicioPlan).AddDays(30);

                    var usuario = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    usuario.UsaPlanCorporativo = false;

                    dbContext.PlanesPagos.Add(p);
                    dbContext.SaveChanges();

                    var idPlanActual = PermisosModulos.ObtenerTodosLosFormularios(dbContext, usu.IDUsuario);
                    //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, usu.IDUsuario, usu.CondicionIVA);
                    HttpContext.Current.Session["CurrentUser"] = new WebUser(
                    usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT, usu.CondicionIVA,
                    usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia,
                    usu.IDCiudad, usu.Telefono, usu.TieneFE, usu.IIBB, usu.FechaInicio,
                    usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa, usu.ModoQA, idPlanActual, usu.EmailAlerta,
                    usu.Provincia, usu.Ciudad, usu.AgentePercepcionIVA, usu.AgentePercepcionIIBB, usu.AgenteRetencion,
                    true, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA, usu.FechaAlta,
                    usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, usuario.UsaPlanCorporativo,
                    usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenes, usu.MostrarBonificacionFc, usu.PorcentajeDescuento, false);
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private string integracionMercadoPago(int idPlan, string plan, bool pagoAnual, decimal importeTotal, decimal importePlan)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            /*using (var dbContext = new ACHEEntities())
            {
                //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                //var plan = dbContext.Planes.Where(x => x.IDPlan == idPlan).FirstOrDefault();
                var btnMP = string.Empty;
                //if (plan != null)
                //{
                API.ClientIDMercadoPago = ConfigurationManager.AppSettings["MP.ClientID"];
                API.PasswordMercadoPago = ConfigurationManager.AppSettings["MP.ClientSecret"];

                if (!string.IsNullOrEmpty(API.ClientIDMercadoPago) && !string.IsNullOrEmpty(API.PasswordMercadoPago))
                {
                    int idUsuario = CurrentUser.IDUsuario;
                    if (CurrentUser.IDUsuarioPadre.HasValue && CurrentUser.IDUsuarioPadre.Value > 0)
                        idUsuario = CurrentUser.IDUsuarioPadre.Value;

                    string NombrePlan = plan + " - " + importePlan.ToString("N2") + " + IVA";
                    if (CurrentUser.PorcentajeDescuento.HasValue && CurrentUser.PorcentajeDescuento.Value > 0)
                        NombrePlan = plan + " - " + importeTotal.ToString("N2") + " + IVA";

                    var referencia = "P" + idPlan.ToString() + "-U" + idUsuario.ToString() + "-A" + Convert.ToByte(pagoAnual).ToString();


                    var mpRef = API.AddPreference(referencia, "Pago del plan: " + NombrePlan, string.Empty, 1, importeTotal, API.Moneda.ARS, string.Empty, CurrentUser.Email, CurrentUser.Telefono, CurrentUser.RazonSocial, CurrentUser.CUIT, CurrentUser.Domicilio, "", CurrentUser.FechaAlta);
                    btnMP = "<a class='btn btn-success' id='lnkComprar' href='" + mpRef
                            + "&payer.name=" + CurrentUser.RazonSocial
                            + "&payer.surname=" + CurrentUser.RazonSocial
                            + "&payer.email=" + CurrentUser.Email
                            + "' name='MP-Checkout' mp-mode='modal' onreturn='pagoCompleto'>Pagar</a>";
                }
                else
                {
                    throw new Exception("Mercado Pago no se encuentra configurado");
                }
                //}
                return btnMP;
            }*/

            return "";
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static void enviarEmail(PlanesPagos planesPago)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<USUARIO>", usu.RazonSocial);
            replacements.Add("<ID>", usu.IDUsuario);
            replacements.Add("<EMAIL>", usu.Email);
            replacements.Add("<FORMAPAGO>", planesPago.FormaDePago);
            replacements.Add("<IMPORTE>", planesPago.ImportePagado);
            replacements.Add("<NOTIFICACION>", "Se están corroborando los datos de tu pago. Pronto confirmaremos el mismo.<br><br>Gracias por seguir confiando en Contabilium! Nuestro equipo trabaja para brindarte la mejor atención siempre.");

            EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.PagoPlanes, replacements, ConfigurationManager.AppSettings["Email.Administracion"], "Contabilium: Pago de plan");
            EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Notificacion, replacements, usu.Email, "Contabilium: Estamos corroborando tu pago, pronto recibirás nuestra confirmación.");
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Negocio.Common;

public partial class modulos_seguridad_elegir_plan : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dbContext = new ACHEEntities())
            {
                
                
                
                
                if (verificarEstadoPendiente(dbContext, CurrentUser.IDUsuario))
                {
                    liTitulo.Text = "Tu pago se encuentra pendiente de verificación. En caso de que lo hayas abonado, comunicate con nuestro centro de atención al cliente.";
                    divModoPago.Visible = false;
                    //PlanBasico.Visible = false;
                    planProfesional.Visible = false;
                    planPyme.Visible = false;
                    planEmpresa.Visible = false;
                    planEmpresa.Visible = false;
                    planCorporativo.Visible = false;
                }
                else
                {
                    var idUsuario = CurrentUser.IDUsuarioPadre.HasValue ? CurrentUser.IDUsuarioPadre.Value : CurrentUser.IDUsuario;
                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault();
                    //bool mostrarPlanGratis = false;

                    /*if (idUsuario < 500)//Solo valido esto para mantener promesas de plan gratis "para siempre". A los nuevos usuarios, no se les ofrece más dicho plan.
                    {
                        if (dbContext.PlanesPagos.Any(x => x.IDUsuario == idUsuario && x.IDPlan == 1))
                            mostrarPlanGratis = true;
                    }*/

                    if (AddinsCommon.ObtenerAddins(idUsuario).Count > 0)
                    {
                        divAddins.Visible = true;
                    }

                    string upgrade = "";
                    var auxPlan = dbContext.UsuariosPlanesView.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    if (auxPlan != null)
                    {
                        //planProfesional.Visible = (auxPlan.IDPlan == 2);

                        var NombrePlan = auxPlan.PlanActual;

                        if (usu != null && !string.IsNullOrEmpty(usu.CodigoPromo) && usu.PorcentajeDescuento > 0)
                            NombrePlan += " (Promo: " + usu.CodigoPromo.ToUpper() + ")";

                        var tipo = Request.QueryString["tipo"];
                        upgrade = Request.QueryString["upgrade"];

                        switch (upgrade)
                        {
                            case "0":
                                liTitulo.Text = "Renová tu Plan <b>" + NombrePlan + "</b> para usar Contabilium sin interrupciones.";
                                //PlanBasico.Visible = false;
                                break;
                            case "1":
                                liTitulo.Text = "Renová tu Plan <b>" + NombrePlan + "</b> para usar Contabilium sin interrupciones.";
                                //PlanBasico.Visible = false;
                                break;
                            //case "2":
                            //    liTitulo.Text = "Alcanzo los 100 comprobantes permitidos, para continuar facturando debe actualizarse a cualquiera de los siguientes planes: ";
                            //    PlanBasico.Visible = false;
                            //    break;
                            case "3":
                                liTitulo.Text = "Por favor elige el modo con el que quieres continuar: ";
                                var planActual = PermisosModulos.ObtenerPlanActual(dbContext, CurrentUser.IDUsuario);
                                MostrarPlan(planActual.IDPlan);
                                break;
                            case "4":
                                liTitulo.Text = "Tu plan actual es " + NombrePlan + ". La funcionalidad a la que intentas acceder requiere que tengas alguno de los siguientes planes:";
                                break;
                            default:
                                if (auxPlan.FechaFinPlan.Value.Date <= DateTime.Now.Date)
                                    liTitulo.Text = "Tu plan " + NombrePlan + " ha vencido. Por favor elige el plan con el que quieres continuar: ";
                                else
                                    liTitulo.Text = "Tu plan actual es " + NombrePlan + ". Por favor elige el nuevo plan con el que quieres continuar: ";
                                break;
                        }
                        SetearPreciosPlanes(usu.PorcentajeDescuento);
                        if (!string.IsNullOrWhiteSpace(tipo))
                        {
                            var formulario = dbContext.Formularios.Where(x => x.Nombre.ToUpper() == tipo.ToUpper()).FirstOrDefault();
                            var plan = formulario.IDPlan;
                            MostrarPlan(plan);
                        }
                    }

                    //PlanBasico.Visible = mostrarPlanGratis;
                    //if (upgrade == "4")
                    //    PlanBasico.Visible = false;

                    if (CurrentUser.IDUsuario >= 6198)
                    {
                        planProfesional.Visible = false;

                        planPyme.Attributes.Clear();
                        planPyme.Attributes.Add("class", "col-md-4 klausbg hvr-float");

                        planEmpresa.Attributes.Clear();
                        planEmpresa.Attributes.Add("class", "col-md-4 klausbg hvr-float");

                        planCorporativo.Attributes.Clear();
                        planCorporativo.Attributes.Add("class", "col-md-4 klausbg skinoneultimate hvr-float");

                        litDescPyme.Text = "<li style='color:#ad2d4f !important;font-weight: bold !important;line-height: initial;'>Facturas y usuarios ILIMITADOS </li><li class='klaus_plancolor'><i class='fa fa-check-circle'></i> Módulos de ventas, compras, tesorería y stock </li><li><i class='fa fa-check-circle'></i> Importación masiva de información</li><li class='klaus_plancolor'><i class='fa fa-check-circle'></i> Portal de clientes</li><li style='font-size: 13px;'><i class='fa fa-check-circle'></i> Integrado a M.Libre, M.Shops, M.Pago y T.Nube</li><li class='klaus_plancolor'><i class='fa fa-comments'></i> Email, chat y teléfono</li><li class=''></li>";
                    }
                    else if (usu.CodigoPromo == "REALTRENDS" || usu.CodigoPromo == "EMPRENDE" || usu.CodigoPromo == "MQE" || usu.CodigoPromo == "TIENDANUBE" || usu.CodigoPromo == "MELI" || usu.CodigoPromo == "MSHOPS")
                    {
                        planProfesional.Visible = false;

                        planPyme.Attributes.Clear();
                        planPyme.Attributes.Add("class", "col-md-4 klausbg hvr-float");

                        planEmpresa.Attributes.Clear();
                        planEmpresa.Attributes.Add("class", "col-md-4 klausbg hvr-float");

                        planCorporativo.Attributes.Clear();
                        planCorporativo.Attributes.Add("class", "col-md-4 klausbg skinoneultimate hvr-float");

                        litDescPyme.Text = "<li style='color:#ad2d4f !important;font-weight: bold !important;line-height: initial;'>Facturas y usuarios ILIMITADOS </li><li class='klaus_plancolor'><i class='fa fa-check-circle'></i> Módulos de ventas, compras, tesorería y stock </li><li><i class='fa fa-check-circle'></i> Importación masiva de información</li><li class='klaus_plancolor'><i class='fa fa-check-circle'></i> Portal de clientes</li><li style='font-size: 13px;'><i class='fa fa-check-circle'></i> Integrado a M.Libre, M.Shops, M.Pago y T.Nube</li><li class='klaus_plancolor'><i class='fa fa-comments'></i> Email, chat y teléfono</li><li class=''></li>";
                    }

                }
            }
        }
    }

    private void SetearPreciosPlanes(decimal descuento)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        var fecha = Convert.ToDateTime("20/06/2018");

        decimal prof = 549;
        decimal pyme = 649;
        decimal empr = 849;
        decimal corp = 999;

        //Para los que contrataron entre Agosto 2017 y Dic 2017, se les mantiene el vaalor viejo
        //var primerPago = usu

        //logica mala. No se toma en cuenta que el usuario sea muy viejp
        if (usu.FechaAlta >= fecha)// || usu.FechaAlta <= Convert.ToDateTime("01/08/2018"))
        {
            prof = 680;
            pyme = 799;
            empr = 999;
            corp = 1199;
        }

        if (descuento > 0)
        {
            prof = prof - ((prof * descuento) / 100);
            pyme = pyme - ((pyme * descuento) / 100);
            empr = empr - ((empr * descuento) / 100);
            corp = corp - ((corp * descuento) / 100);
        }

        //decimal descuentoAnual = 20;
        decimal porcAnual = 0.20M;

        liPlanProfesionalMes.Text = prof.ToMoneyFormat(2);// "Mensual: $" + prof + " + IVA por mes";
        liPlanProfesionalAnual.Text = (prof * 12 - ((prof * 12) * porcAnual)).ToMoneyFormat(2);//"Anual: Ahorrás $" + ((prof * 12) / descuentoAnual).ToMoneyFormat(4) + " y pagás $" + (prof * 12 - ((prof * 12) * porcAnual)).ToMoneyFormat(4) + " + IVA";
        liPlanPymeMes.Text = pyme.ToMoneyFormat(2);// "Mensual: $" + pyme + " + IVA por mes";
        liPlanPymeAnual.Text = (pyme * 12 - ((pyme * 12) * porcAnual)).ToMoneyFormat(2);// "Anual: Ahorrás $" + ((pyme * 12) / descuentoAnual).ToMoneyFormat(4) + " y pagás $" + (pyme * 12 - ((pyme * 12) * porcAnual)).ToMoneyFormat(4) + " + IVA";
        liPlanEmpresaMes.Text = empr.ToMoneyFormat(2);// "Mensual: $" + empr + " + IVA por mes";
        liPlanEmpresaAnual.Text = (empr * 12 - ((empr * 12) * porcAnual)).ToMoneyFormat(2);// "Anual: Ahorrás $" + ((empr * 12) / descuentoAnual).ToMoneyFormat(4) + " y pagás $" + (empr * 12 - ((empr * 12) * porcAnual)).ToMoneyFormat(4) + " + IVA";
        liPlanCorporativoMes.Text = corp.ToMoneyFormat(2);//"Mensual: $" + corp + " + IVA por mes";
        liPlanCorporativoAnual.Text = (corp * 12 - ((corp * 12) * porcAnual)).ToMoneyFormat(4);// "Anual: Ahorrás $" + ((corp * 12) / descuentoAnual).ToMoneyFormat(4) + " y pagás $" + (corp * 12 - ((corp * 12) * porcAnual)).ToMoneyFormat(4) + " + IVA";
    }

    private void MostrarPlan(int plan)
    {
        switch (plan)
        {
            case 2:
                planProfesional.Visible = true;
                break;
            case 3:
                //PlanBasico.Visible = false;
                planProfesional.Visible = false;
                break;
            case 4:
                //PlanBasico.Visible = false;
                planProfesional.Visible = false;
                planPyme.Visible = false;
                break;
            case 5:
                //PlanBasico.Visible = false;
                planProfesional.Visible = false;
                planPyme.Visible = false;
                planEmpresa.Visible = false;
                break;
        }
    }

    private bool verificarEstadoPendiente(ACHEEntities dbContext, int idUsuario)
    {
        var usu = dbContext.UsuariosView.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
        //var UsuAdicional = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == idUsuario).FirstOrDefault();
        var Empresa = dbContext.Usuarios.Where(x => x.IDUsuarioPadre != null && x.IDUsuario == idUsuario).FirstOrDefault();
        PlanesPagos plan = new PlanesPagos();

        if (usu.IDUsuarioAdicional > 0)
            plan = dbContext.PlanesPagos.Where(x => x.IDUsuario == usu.IDUsuario && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();
        else if (Empresa != null)
            plan = dbContext.PlanesPagos.Where(x => x.IDUsuario == Empresa.IDUsuarioPadre && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();
        else
            plan = dbContext.PlanesPagos.Where(x => x.IDUsuario == idUsuario && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();


        if (plan != null && plan.Estado == "Pendiente")
            return true;
        else
            return false;
    }
}
﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Contabilidad;
using System.Data.Entity;

public partial class modulos_contabilidad_asientosManuales : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            txtFechaDesde.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
               CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosAsientoManualViewModel getResults(string leyenda, string fechaDesde, string fechaHasta, int page, int pageSize) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                var results = ContabilidadCommon.ObtenerAsientosManualesResultados(usu, leyenda, fechaDesde,fechaHasta,page,pageSize);
                return results;
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private static int NroAsiento(ref int cantidad) {
        return cantidad++;
    }

    [WebMethod(true)]
    public static string export(string leyenda, string fechaDesde, string fechaHasta) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ContabilidadCommon.ExportarAsientosManuales(usu, leyenda, fechaDesde, fechaHasta);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string exportPDF(string leyenda, string fechaDesde, string fechaHasta) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return ContabilidadCommon.ExportarAsientosManualesPDF(usu, leyenda, fechaDesde, fechaHasta);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void delete(int id) {
        try {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ContabilidadCommon.EliminarAsientoManual(usu.IDUsuario, id);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="asientosManualese.aspx.cs" Inherits="modulos_contabilidad_asientosManualese" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class='fa fa-list'></i><asp:Literal runat="server" ID="litTitulo" Text="Alta"></asp:Literal></h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href='/modulos/contabilidad/asientosmanuales.aspx'>Asientos manuales</a></li>
                <li class="active">
                    <asp:Literal runat="server" ID="litPath" Text="Alta"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15">
            <form id="frmEdicion" runat="server" class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>
                        <div class="alert alert-success" id="divOk" style="display: none">
                            <strong>Bien hecho! </strong>Los datos se han actualizado correctamente
                        </div>
                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Datos generales</h3>
                                <label class="control-label">
                                    <asp:Literal runat="server" ID="litDescripcion" Text="Alta"></asp:Literal>
                                </label>
                            </div>
                        </div>
                        <%--<div class="row mb15" runat="server" id="DivAsientosModelo">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Asientos Modelos</label>
                                    <asp:DropDownList runat="server" ID="ddlAsientosModelo" CssClass="select2" ClientIDMode="Static" data-placeholder="Seleccione un asiento modelo">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2" style="margin-top: 30px">
                                <a class="btn btn-default" onclick="asientosManualese.actualizarDatosModelo();">Seleccionar modelo</a>

                            </div>
                        </div>--%>

                        <div class="row mb15">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Leyenda</label>
                                    <asp:TextBox runat="server" ID="txtLeyenda" CssClass="form-control  required" MaxLength="128" ClientIDMode="Static"></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-sm-2" id="DivFecha" runat="server">
                                <label class="control-label"><span class="asterisk">*</span> Fecha</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    <asp:TextBox runat="server" ID="txtFecha" CssClass="form-control required validDate validFechaActual" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-12">
                                <h3>Datos generales</h3>
                                <label class="control-label">Acá ingrese todas las cuentas contables.</label>
                            </div>
                        </div>

                        <div class="row mb15">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Cuenta contable</label>
                                    <asp:DropDownList runat="server" ID="ddlPlanDeCuentas" CssClass="select2" ClientIDMode="Static" data-placeholder="Seleccione una cuenta">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div runat="server" id="divDebeHaber">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Debe</label>
                                    <asp:TextBox runat="server" ID="txtDebe" CssClass="form-control  required" MaxLength="128" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label"><span class="asterisk">*</span> Haber</label>
                                    <asp:TextBox runat="server" ID="txtHaber" CssClass="form-control  required" MaxLength="128" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-2" style="margin-top: 30px">
                                <%--<a class="btn btn-default" runat="server" onclick="asientosManualese.agregarAsiento();" id="btnAgregar">Agregar asiento</a>--%>
                                <span runat="server" id="spnBtnAgregar"></span>
                            </div>
                        </div>

                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <%--<th>Codigo</th>--%>
                                        <th style="max-width: 150px">Cuenta</th>
                                        <div runat="server" id="divDebeHaberTable">
                                        <th style="text-align: right;width:500px;">Debe</th>
                                        <th style="text-align: right;width:500px;">Haber</th>
                                        </div>
                                        <th class="columnIcons"></th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                                <tfoot runat="server" id="tableFoot">
                                    <tr>
                                        <th class="text-danger" style="FONT-WEIGHT: BOLD;">TOTAL</th>
                                        <th class="text-danger" style="FONT-WEIGHT: BOLD; text-align: right"><span id="spnTotalDebe"></span></th>
                                        <th class="text-danger" style="FONT-WEIGHT: BOLD; text-align: right"><span id="spnTotalHaber"></span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div runat="server" id="btnesAsientoManual">
                            <a class="btn btn-success" id="btnActualizar" onclick="asientosManualese.guardar(false);">Aceptar</a>
                           <%-- <a class="btn btn-default" id="btnActualizarModelo" onclick="asientosManualese.guardar(true);">Aceptar y guardar como modelo</a>--%>
                            <a href="#" onclick="asientosManualese.cancelar();" style="margin-left: 20px">Cancelar</a>
                        </div>
                        <%--<div runat="server" id="btnesAsientoModelo">
                            <a class="btn btn-success" id="btnCrearModelo" onclick="asientosManualese.guardarModelo();">Aceptar</a>
                            <a href="#" onclick="asientosModelo.cancelar();" style="margin-left: 20px">Cancelar</a>
                        </div>--%>
                    </div>

                </div>
                <script id="resultTemplate" type="text/x-jQuery-tmpl">
                    {{each results}}
                    <tr>
                        <%--<td>${Codigo}</td>--%>
                        <td>${NombreCuenta}</td>
                        <td class="ocultarEnEditar" style="text-align: right"><span id="spnDebe-${CodigoEnArray}">${MostrarDebe}</span><input class="form-control input-sm EditInput keyPressEdit-${CodigoEnArray}" style="padding: 3px; display: none; width: 200px; height: 19px; float: right" id="inputDebe-${CodigoEnArray}" /></td>
                        <td class="ocultarEnEditar" style="text-align: right"><span id="spnHaber-${CodigoEnArray}">${MostrarHaber}</span><input class="form-control input-sm EditInput keyPressEdit-${CodigoEnArray}" style="padding: 3px; display: none; width: 200px ;height: 19px; float: right" id="inputHaber-${CodigoEnArray}" /></td>
                        <td class="table-action">
                            <a onclick="asientosManualese.saveEditar(${CodigoEnArray});" id="btnSave-${CodigoEnArray}" style="cursor: pointer; font-size: 16px; display: none; color: green" title="Actualizar"><i class="fa fa-check"></i></a>
                            <a class="ocultarEnEditar" onclick="asientosManualese.editar(${CodigoEnArray},'${Debe}','${Haber}');" style="cursor: pointer; font-size: 16px" title="Editar"><i class="fa fa-pencil"></i></a>
                            <a onclick="asientosManualese.eliminar(${CodigoEnArray});" style="cursor: pointer; font-size: 16px" class="delete-row" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    {{/each}}


                </script>

                <script id="noResultTemplate" type="text/x-jQuery-tmpl">
                    <tr>
                        <td colspan="5">No se han encontrado resultados</td>
                    </tr>
                </script>

                <asp:HiddenField runat="server" ID="hdnID" Value="0" />
                <asp:HiddenField runat="server" ID="hdnTipo" Value="" />
                <input id="hdnIDModelo" value="0" hidden/>


            </form>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/contabilidad/asientosManuales.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            asientosManualese.configForm();
            if (parseInt($("#hdnID").val()) != 0) {
                if ($("#hdnTipo").val() == "Mo")
                    asientosManualese.obtenerAsientosModelo();                
                else
                    asientosManualese.obtenerAsientosManuales();
            }
        });
    </script>
</asp:Content>


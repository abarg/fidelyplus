﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using System.Web.Services;
using System.Configuration;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Contabilidad;
using System.Data;
using System.IO;

public partial class modulos_contabilidad_planDeCuentas : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod(true)]
    public static List<PlanDeCuentasViewModel> ObtenerPlanDeCuentas()
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var result = ContabilidadCommon.ObtenerPlanDeCuentas(usu);

            return result;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    private static string ObtenerTextoSaldo(int idPlanDeCuenta)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        string saldos = string.Empty;
        string debito = "0";
        string credito = "0";

        var lista = ContabilidadCommon.ObtenerBalanceDeResultados(usu.IDUsuario, "", "", 1, 10000);

        debito = (lista.Asientos.Any(x => x.IDPlanDeCuenta == idPlanDeCuenta)) ? lista.Asientos.Where(x => x.IDPlanDeCuenta == idPlanDeCuenta).FirstOrDefault().TotalDebe : "0";
        credito = (lista.Asientos.Any(x => x.IDPlanDeCuenta == idPlanDeCuenta)) ? lista.Asientos.Where(x => x.IDPlanDeCuenta == idPlanDeCuenta).FirstOrDefault().TotalHaber : "0";

        var aux = (debito != "0" || credito != "0") ? " / " : "";

        return "<span class='spanDetalle' style='float: right; margin-right: 20px;'> " + debito + aux + credito + "</span>";
    }

    [WebMethod(true)]
    public static PlanDeCuentasViewModel ObtenerCuenta(int idPlanDeCuenta)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            return ContabilidadCommon.ObtenerCuenta(usu, idPlanDeCuenta);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void Guardar(int id, string codigo, string nombre, int idPadre, string adminiteAsientoManual, string tipoDeCuenta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ContabilidadCommon.GuardarPlanDeCuenta(usu, id, codigo, nombre, idPadre, adminiteAsientoManual, tipoDeCuenta);      
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ContabilidadCommon.EliminarPlanDeCuenta(usu.IDUsuario, id);
            }
            else
                throw new CustomException("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw new Exception(e.Message);
        }
    }

    [WebMethod(true)]
    public static void CierreContable()
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                ContabilidadCommon.CierreContable(usu);
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    public static string exportar()
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            return ContabilidadCommon.ExportarPlanDeCuentas(usu);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

}
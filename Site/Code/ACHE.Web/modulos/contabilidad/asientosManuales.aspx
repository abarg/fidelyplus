﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="asientosManuales.aspx.cs" Inherits="modulos_contabilidad_asientosManuales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Asientos Manuales</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Contabilidad</a></li>
                <li class="active">Asientos Manuales</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />

                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-6">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6 form-group">
                                            <input type="text" class="form-control" id="txtCondicion" maxlength="128" placeholder="Ingresá la leyenda del asiento" />
                                        </div>
                                        <div class="col-sm-6 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaDesde" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaDesde" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <div class="col-sm-6 col-md-3 form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                <asp:TextBox runat="server" ID="txtFechaHasta" CssClass="form-control validDate greaterThan" placeholder="dd/mm/yyyy" MaxLength="10"></asp:TextBox>
                                            </div>
                                            <label for="txtFechaHasta" class="error" style="display: none">La fecha desde es mayor a la fecha hasta</label>
                                        </div>
                                        <%--                                         <div class="col-sm-6 col-md-4 form-group">
                                            <input type="checkbox" id="chkAsientosManuales" />&nbsp;
                                            <label for="chkAsientosManuales"><strong>Sólo asientos manuales</strong></label>
                                        </div>--%>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-6 responsive-buttons md">
                                    <a class="btn btn-black" onclick="asientosManuales.resetearPagina();asientosManuales.filtrar();" id="btnBuscar">Buscar</a>
                                    <%--<a class="btn btn-default" onclick="asientosManuales.resetearPagina();asientosManuales.verTodos();">Ver todos</a>--%>
                                    <a class="btn btn-warning mr10" onclick="asientosManuales.nuevo();">
                                        <i class="fa fa-plus"></i>&nbsp;Nuevo asiento manual
                                    </a>
                                   <%-- <div class="btn-group dropdown form-group" id="Div1">
                                        <button type="button" class="btn btn-btn-default"><i class="fa fa-list"></i>&nbsp;Otras acciones</button>
                                        <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:asientosManuales.administrarModelos();">Administrar asientos modelos</a></li>
                                            <li><a href="javascript:asientosManuales.nuevoModelo();">Crear nuevo asiento modelo</a></li>
                                        </ul>
                                    </div>--%>
                                </div>
                            </div>

                        </form>

                        <div class="col-sm-12">
                            <hr class="mt0" />
                        </div>
                        <div class="row">
                            <div class="pull-right">
                                <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                <a href="" id="lnkDownload" onclick="javascript:asientosManuales.resetearExportacion();" download="AsientosManuales" style="display: none">Descargar</a>
                                <div class="btn-group mr10 dropdown" id="divExportar" runat="server">
                                    <button type="button" class="btn btn-btn-default"><i class="fa fa-tasks"></i> Exportar</button>
                                    <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:asientosManuales.exportar();" >A Excel</a></li>
                                        <li><a href="javascript:asientosManuales.exportarPDF();">A PDF</a></li>
                                                
                                    </ul>
                                </div>
                                
                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="asientosManuales.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="asientosManuales.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>
                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body" id="divResultados">

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <%--<th>Nro asiento</th>--%>
                                        <th>Fecha</th>
                                        <th>Leyenda</th>
                                        <th style="text-align: right">Débito</th>
                                        <th style="text-align: right">Crédito</th>
                                        <th style="text-align: right"></th>

                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
            <tr>
                <%--<td>${NroAsiento}</td>--%>
                <td>${Fecha}</td>
                <td>${Leyenda}</td>
                <td style="text-align: right;">${AsientoDebe}</td>
                <td style="text-align: right">${AsientoHaber}</td>
                <%-- <td class="bgTotal"></td>
                <td class="bgTotal"></td>
                <td class="bgTotal"></td>--%>
                <td style="text-align: right">
                    <a onclick="asientosManuales.editar(${IDAsiento});" style="cursor: pointer; font-size: 16px; color: currentColor" title="Editar">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a onclick="asientosManuales.eliminar(${IDAsiento},'${Leyenda}');" style="cursor: pointer; font-size: 16px; color: currentColor" class="delete-row" title="Eliminar">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>


        {{/each}}
        
        <tr>
            <td class="bgTotal text-danger"></td>
            <td class="bgTotal text-danger">Totales</td>
            <td style="text-align: right" class="bgTotal text-danger"><span id="totalDebe"></span></td>
            <td style="text-align: right" class="bgTotal text-danger"><span id="totalHaber"></span></td>
            <td class="bgTotal text-danger"></td>
        </tr>
    </script>
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="7">No se han encontrado resultados</td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/contabilidad/asientosManuales.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            asientosManuales.configFilters();
            asientosManuales.filtrar();
        });
    </script>
</asp:Content>


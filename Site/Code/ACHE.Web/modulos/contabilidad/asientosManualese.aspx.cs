﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.Model;
using ACHE.Extensions;
using ACHE.Negocio.Contabilidad;
using System.Web.Services;
using ACHE.Model.ViewModels;

public partial class modulos_contabilidad_asientosManualese : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            using (var dbContext = new ACHEEntities()) {
                var listaPlanDeCuenta = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.AdminiteAsientoManual).OrderBy(x => x.Codigo).ToList();
                ddlPlanDeCuentas.Items.Add(new ListItem("", ""));

                foreach (var item in listaPlanDeCuenta)
                    ddlPlanDeCuentas.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));

                //var asientosModelo = dbContext.AsientosModelo.Where(x => x.IDUsuario == CurrentUser.IDUsuario).OrderBy(x => x.Leyenda).ToList();
                //ddlAsientosModelo.Items.Add(new ListItem("", ""));

                //foreach (var item in asientosModelo)
                //    ddlAsientosModelo.Items.Add(new ListItem(item.Leyenda, item.IDAsientoModelo.ToString()));

                var idAsiento = 0;

                if (Request.QueryString["nM"] == "0") {
                    //vAsientosModelo.Visible = false;
                    divDebeHaber.Visible = false;
                    divDebeHaberTable.Visible = false;
                    DivFecha.Visible = false;
                    btnesAsientoManual.Visible = false;
                    tableFoot.Visible = false;
                    litTitulo.Text = "Asientos Modelos";
                    hdnTipo.Value = "Mo"; // TIPO MODELO
                    if (int.TryParse(Request.QueryString["id"], out idAsiento)) {
                        var listaAsiento = dbContext.AsientosModelo.Where(x => x.IDAsientoModelo == idAsiento).FirstOrDefault();
                        litPath.Text = "Edicion Modelo";
                        spnBtnAgregar.InnerHtml = "<a class='btn btn-default' runat='server' onclick='asientosManualese.agregarAsientoModelo();' id='btnAgregar'>Agregar asiento</a>";
                        litDescripcion.Text = "Acá podra modificar los asientos modelos para usarlos luego.";
                        hdnID.Value = idAsiento.ToString();

                    }
                    else {
                        litPath.Text = "Alta Modelo";
                        spnBtnAgregar.InnerHtml = "<a class='btn btn-default' runat='server' onclick='asientosManualese.agregarAsientoModelo();' id='btnAgregar'>Agregar asiento</a>";
                        litDescripcion.Text = "Acá podra dar de alta los asientos modelos para usarlos luego.";
                    }

                }
                else {
                    litTitulo.Text = "Asientos Manuales";

                    hdnTipo.Value = "Ma"; //TIPO MANUAL
                    spnBtnAgregar.InnerHtml = "<a class='btn btn-default' runat='server' onclick='asientosManualese.agregarAsiento();' id='btnAgregar'>Agregar asiento</a>";
                    litDescripcion.Text = "Acá podra dar de alta los asientos contables manualmente.";
                    //btnesAsientoModelo.Visible = false;

                    if (int.TryParse(Request.QueryString["id"], out idAsiento)) {
                        var listaAsiento = dbContext.Asientos.Where(x => x.IDAsiento == idAsiento).FirstOrDefault();
                        if (listaAsiento.IDCobranza > 0)
                            Response.Redirect("/cobranzase.aspx?ID=" + listaAsiento.IDCobranza);
                        else if (listaAsiento.IDCompra > 0)
                            Response.Redirect("/modulos/compras/comprase.aspx?ID=" + listaAsiento.IDCompra);
                        else if (listaAsiento.IDComprobante > 0)
                            Response.Redirect("/comprobantese.aspx?ID=" + listaAsiento.IDComprobante);
                        else if (listaAsiento.IDPago > 0)
                            Response.Redirect("/modulos/compras/pagose.aspx?ID=" + listaAsiento.IDPago);
                        else if (listaAsiento.IDMovimientoDeFondo > 0)
                            Response.Redirect("/modulos/tesoreria/MovimientoDeFondose.aspx?ID=" + listaAsiento.IDMovimientoDeFondo);
                        else
                            hdnID.Value = idAsiento.ToString();

                        litPath.Text = "Edicion";

                        //DivAsientosModelo.Visible = false;
                    }
                    else
                        txtFecha.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                }
            }
        }
    }

    [WebMethod(true)]
    public static void guardar(List<AsientosManualesViewModel> listaAsientos, string leyenda, string fecha, int id, int idModelo, bool guardarModelo) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            if (ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, Convert.ToDateTime(fecha)))
                throw new Exception("No puede agregar ni modificar un asiento que se encuentre en un periodo cerrado.");

            ContabilidadCommon.AgregarAsientoManual(listaAsientos, leyenda, fecha, id, usu.IDUsuario);

            if (guardarModelo)
                ContabilidadCommon.AgregarAsientoModelo(listaAsientos, leyenda, idModelo, usu.IDUsuario);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void guardarModelo(List<AsientosManualesViewModel> listaAsientos, string leyenda, int id) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ContabilidadCommon.AgregarAsientoModelo(listaAsientos, leyenda, id, usu.IDUsuario);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static ResultadoAsientosManualesViewModel ObtenerAsientosManuales(int id) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            ResultadoAsientosManualesViewModel resultado = new ResultadoAsientosManualesViewModel();
            resultado = ContabilidadCommon.ObtenerAsientosManuales(usu, id);
            return resultado;

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static ResultadoAsientosManualesViewModel ObtenerAsientoModelo(int id) {
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var resultado = ContabilidadCommon.ObtenerAsientoModelo(usu, id);
            return resultado;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

}
﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Services;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Contabilidad;
using System.Data.Entity;

public partial class modulos_contabilidad_asientosModelo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txtFechaDesde.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //divExportar.Visible = PermisosModulos.tieneAccesoAPermiso(PermisosEnum.Exportacion.ToString(),
            //   CurrentUser.TipoUsuario);
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosAsientoManualViewModel getResults(string leyenda, int page, int pageSize)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                var resultado = ContabilidadCommon.ObtenerAsientosModelo(usu, leyenda, page, pageSize);
                return resultado;
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }


    //[WebMethod(true)]
    //public static string export(string leyenda)
    //{
    //    if (HttpContext.Current.Session["CurrentUser"] != null)
    //    {
    //        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

    //        string fileName = "AsientosManuales_" + usu.IDUsuario + "_";
    //        string path = "~/tmp/";
    //        try
    //        {
    //            DataTable dt = new DataTable();
    //            using (var dbContext = new ACHEEntities())
    //            {
    //                var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario && x.EsManual).AsQueryable();
    //                if (!string.IsNullOrEmpty(leyenda))
    //                    results = results.Where(x => x.Leyenda.ToLower().Contains(leyenda.ToLower()));
    //                if (fechaDesde != string.Empty)
    //                {
    //                    DateTime dtDesde = DateTime.Parse(fechaDesde);
    //                    results = results.Where(x => x.Fecha >= dtDesde);
    //                }
    //                if (fechaHasta != string.Empty)
    //                {
    //                    DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
    //                    results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
    //                }

    //                var resultsFinal = results.ToList();

    //                var CantAsientos = 1;
    //                var listaCuentas = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).ToList()
    //                .Select(x => new AsientoManualViewModel()
    //                {
    //                    IDAsiento = x.FirstOrDefault().IDAsiento,
    //                    NroAsiento = NroAsiento(ref CantAsientos).ToString(),
    //                    Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
    //                    Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
    //                    AsientoDebe = TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "D"),
    //                    AsientoHaber = TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "H"),
    //                    TotalDebe = x.Sum(y => y.Debe).ToMoneyFormat(2),
    //                    TotalHaber = x.Sum(y => y.Haber).ToMoneyFormat(2)
    //                }).ToList();

    //                List<AsientoManualExportacionViewModel> listaFinal = new List<AsientoManualExportacionViewModel>();
    //                AsientoManualExportacionViewModel asiento;
    //                foreach (var item in listaCuentas)
    //                {
    //                    asiento = new AsientoManualExportacionViewModel();
    //                    asiento.NroAsiento = item.NroAsiento;
    //                    asiento.Fecha = item.Fecha;
    //                    asiento.Leyenda = item.Leyenda;
    //                    asiento.Debe = item.AsientoDebe;
    //                    asiento.Haber = item.AsientoHaber;
    //                    listaFinal.Add(asiento);

    //                }

    //                dt = listaFinal.ToList().Select(x => new AsientoManualExportacionViewModel()
    //                {
    //                    NroAsiento = x.NroAsiento,
    //                    Fecha = x.Fecha,
    //                    Leyenda = x.Leyenda,
    //                    Debe = x.Debe,
    //                    Haber = x.Haber
    //                }).ToList().ToDataTable();
    //            }

    //            if (dt.Rows.Count > 0)
    //                CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
    //            else
    //                throw new Exception("No se encuentran datos para los filtros seleccionados");

    //            return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
    //        }
    //        catch (Exception e)
    //        {
    //            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
    //            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
    //            throw e;
    //        }
    //    }
    //    else
    //        throw new Exception("Por favor, vuelva a iniciar sesión");
    //}

    //[WebMethod(true)]
    //public static string exportPDF(string leyenda, string fechaDesde, string fechaHasta) {
    //    if (HttpContext.Current.Session["CurrentUser"] != null) {
    //        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

    //        string fileName = "AsientosManuales_" + usu.IDUsuario + "_";
    //        string path = "~/tmp/";
    //        try
    //        {
    //            string TotalDebito = "0";
    //            string TotalCredito = "0";
    //            DataTable dt = new DataTable();
    //            using (var dbContext = new ACHEEntities())
    //            {
    //                var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario && x.EsManual).AsQueryable();
    //                if (!string.IsNullOrEmpty(leyenda))
    //                    results = results.Where(x => x.Leyenda.ToLower().Contains(leyenda.ToLower()));
    //                if (fechaDesde != string.Empty)
    //                {
    //                    DateTime dtDesde = DateTime.Parse(fechaDesde);
    //                    results = results.Where(x => x.Fecha >= dtDesde);
    //                }
    //                if (fechaHasta != string.Empty)
    //                {
    //                    DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
    //                    results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
    //                }

    //                var resultsFinal = results.ToList();

    //                var CantAsientos = 1;
    //                var listaCuentas = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).ToList()
    //                .Select(x => new AsientoManualViewModel()
    //                {
    //                    NroAsiento = NroAsiento(ref CantAsientos).ToString(),
    //                    Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
    //                    Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
    //                    AsientoDebe = TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "D"),
    //                    AsientoHaber = TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "H"),
    //                }).ToList();

    //                List<AsientoManualExportacionViewModel> listaFinal = new List<AsientoManualExportacionViewModel>();
    //                AsientoManualExportacionViewModel asiento;
    //                foreach (var item in listaCuentas)
    //                {
    //                    asiento = new AsientoManualExportacionViewModel();
    //                    asiento.NroAsiento = item.NroAsiento;
    //                    asiento.Fecha = item.Fecha;
    //                    asiento.Leyenda = item.Leyenda;
    //                    asiento.Debe = item.AsientoDebe;
    //                    asiento.Haber = item.AsientoHaber;

    //                    listaFinal.Add(asiento);
    //                }

    //                dt = listaFinal.ToList().Select(x => new AsientoManualExportacionViewModel()
    //                {
    //                    NroAsiento = x.NroAsiento,
    //                    Fecha = x.Fecha,
    //                    Leyenda = x.Leyenda,
    //                    Debe = x.Debe,
    //                    Haber = x.Haber
    //                }).ToList().ToDataTable();


    //                TotalDebito = listaFinal.Sum(x => Convert.ToDecimal(x.Debe)).ToMoneyFormat(2);
    //                TotalCredito = listaFinal.Sum(x => Convert.ToDecimal(x.Haber)).ToMoneyFormat(2);
    //            }



    //            if (dt.Rows.Count > 0) {
    //                ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
    //                ///razon social , cuit y periodo de fechas 
    //                List<DetalleCabecera> cabecera = new List<DetalleCabecera>();
    //                DetalleCabecera c = new DetalleCabecera();
    //                c.Titulo = "Razón Social";
    //                c.Valor = usu.RazonSocial;
    //                cabecera.Add(c);

    //                DetalleCabecera c1 = new DetalleCabecera();
    //                c1.Titulo = "CUIT";
    //                c1.Valor = usu.CUIT;
    //                cabecera.Add(c1);
    //                DetalleCabecera c2 = new DetalleCabecera();

    //                c2.Titulo = "Período";
    //                c2.Valor = fechaDesde + " - " + fechaHasta;
    //                cabecera.Add(c2);
    //                contenido.Cabecera = cabecera;

    //                contenido.TituloReporte = "ASIENTOS MANUALES";
    //                List<string> ultimaFilaTotales = new List<string>();
    //                ultimaFilaTotales.Add("");
    //                ultimaFilaTotales.Add("");

    //                ultimaFilaTotales.Add("Totales");

    //                ultimaFilaTotales.Add(TotalDebito);
    //                ultimaFilaTotales.Add(TotalCredito);



    //                //ultimaFilaTotales.Add(TotalServicios.ToMoneyFormat(4));
    //                //ultimaFilaTotales.Add(TotalProductos.ToMoneyFormat(4));

    //                ultimaFilaTotales.Add("");
    //                contenido.UltimaFila = ultimaFilaTotales;
    //                contenido.Widths =  new float[] { 20f, 20f, 80f, 30f, 30f};

    //                CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, false, true);
    //            }
    //            else
    //                throw new Exception("No se encuentran datos para los filtros seleccionados");

    //            return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");
    //        }
    //        catch (Exception e) {
    //            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
    //            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
    //            throw e;
    //        }
    //    }
    //    else
    //        throw new Exception("Por favor, vuelva a iniciar sesión");
    //}

    [WebMethod(true)]
    public static void delete(int id)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ContabilidadCommon.EliminarAsientoModelo(usu.IDUsuario, id);                
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="asientosModelo.aspx.cs" Inherits="modulos_contabilidad_asientosModelo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-bar-chart-o"></i>Asientos modelos</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="#">Contabilidad</a></li>
                <li class="active">Asientos modelos</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 table-results">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos!</strong> <span id="msgError"></span>
                        </div>

                        <form runat="server" id="frmSearch">
                            <input type="hidden" id="hdnPage" runat="server" value="1" />

                            <div class="col-sm-12" style="padding-left: inherit">
                                <div class="col-sm-8 col-md-6">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 form-group">
                                            <input type="text" class="form-control" id="txtCondicion" maxlength="128" placeholder="Ingresá la leyenda del asiento modelo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-6 responsive-buttons md">
                                    <a class="btn btn-black" onclick="asientosModelo.resetearPagina();asientosModelo.filtrar();" id="btnBuscar">Buscar</a>
                                    <a class="btn btn-warning mr10" onclick="asientosManuales.nuevoModelo();">
                                        <i class="fa fa-plus"></i>&nbsp;Nuevo asiento Modelo
                                    </a>
                                </div>
                            </div>

                        </form>

                        <div class="col-sm-12">
                            <hr class="mt20" />
                        </div>
                        <div class="row">
                            <div class="pull-right">
                                <div class="btn-group mr10" id="divPagination" style="display: none">
                                    <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="asientosModelo.mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                    <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="asientosModelo.mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>
                            <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                            <p id="msjResultados" style="padding-left: 20px"></p>
                        </div>
                    </div>
                    <!-- panel-heading -->
                    <div class="panel-body" id="divResultados">

                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
<%--                                    <th>Nro asiento modelo</th>--%>
                                        <th>Leyenda</th>
                                        <th style="text-align: right"></th>
                                    </tr>
                                </thead>
                                <tbody id="resultsContainer">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
            <tr>
                <%--<td>${NroAsiento}</td>--%>
                <td>${Leyenda}</td>
                <%-- <td class="bgTotal"></td>
                <td class="bgTotal"></td>
                <td class="bgTotal"></td>--%>
                <td style="text-align: right">
                    <a onclick="asientosModelo.editar(${IDAsiento});" style="cursor: pointer; font-size: 16px; color: currentColor" title="Editar">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a onclick="asientosModelo.eliminar(${IDAsiento},'${Leyenda}');" style="cursor: pointer; font-size: 16px; color: currentColor" class="delete-row" title="Eliminar">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>


        {{/each}}       

    </script>
    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <tr>
            <td colspan="2">No se han encontrado resultados</td>
        </tr>
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/contabilidad/asientosManuales.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            asientosModelo.configFilters();
            asientosModelo.filtrar();
        });
    </script>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Common;

public partial class aplicarCobranzasACuenta : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //TODO: pasar a businnes
            using (var dbContext = new ACHEEntities())
            {
                var cant = dbContext.CobranzasDetalle.Where(x => !x.IDComprobante.HasValue && x.Cobranzas.IDUsuario == CurrentUser.IDUsuario).Select(x => x.IDCobranza).Distinct().Count();
                if (cant > 0)
                {
                    divAlert.Visible = true;
                    litMsj.Text = "Usted tiene <a style='color: #8a6d3b;font-weight: bold;text-decoration: underline;' href='javascript:Common.verCobranzasACuenta();'>" + cant + " cobranza(s)</a> a cuenta pendiente(s) de imputación";
                }
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosComprobantesViewModel getResults(int idPersona, int page, int pageSize)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            return CobranzasCommon.ObtenerCobranzasACuenta(idPersona, page, pageSize, usu.IDUsuario);


        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static void AplicarCobranzasACuenta(int idPersona, AsignacionPagoACuentaDto[] cobranzasAcuenta)
    {
        try
        {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var pagosAAplicar = new Dictionary<int, List<int>>();

            foreach (var cob in cobranzasAcuenta.Where(cob => cob.IdCompra != -1))
            {
                if (!pagosAAplicar.ContainsKey(cob.IdCompra))
                    pagosAAplicar.Add(cob.IdCompra, new List<int>());
                pagosAAplicar[cob.IdCompra].Add(cob.IdPagoACuenta);
            }

            CobranzasCommon.AplicarCobranzasACuenta(idPersona, pagosAAplicar);
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

}
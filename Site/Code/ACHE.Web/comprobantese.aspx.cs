﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Collections.Specialized;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;
using ACHE.Negocio.Inventario;
using ClosedXML.Excel;
using ACHE.Negocio.Ventas;

public partial class comprobantese : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            using (var dbContext = new ACHEEntities()) {
                //LH: No se usa mas esto. Era para el plan gratis
                //var plan = PermisosModulos.ObtenerPlanActual(dbContext, CurrentUser.IDUsuario);
                //if (PermisosModulosCommon.VerificarCantComprobantes(plan, CurrentUser.IDUsuario))
                //    Response.Redirect("~/modulos/seguridad/elegir-plan.aspx?upgrade=2");

                hdnEnvioFE.Value = (CurrentUser.EnvioAutomaticoComprobante == true) ? "1" : "0";

                if (CurrentUser.UsaPrecioFinalConIVA)
                    liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Precio Unit. con IVA</span> ";
                else
                    liPrecioUnitario.Text = "<span id='spPrecioUnitario'> Precio Unit. sin IVA</span> ";

                hdnUsaPrecioConIVA.Value = (CurrentUser.UsaPrecioFinalConIVA) ? "1" : "0";

                ComprobanteCart.Retrieve().Items.Clear();
                ComprobanteCart.Retrieve().Tributos.Clear();
                int idPresupuesto = 0;
                int idRemito = 0;

                if (Request.QueryString["IDPresupuesto"] != string.Empty)
                    idPresupuesto = Convert.ToInt32(Request.QueryString["IDPresupuesto"]);
                if (Request.QueryString["IDRemitoCustom"] != string.Empty)
                    idRemito = Convert.ToInt32(Request.QueryString["IDRemitoCustom"]);


                txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFechaVencimiento.Text = DateTime.Now.AddDays(30).ToString("dd/MM/yyyy");

                hdnPercepcionIIBB.Value = (Convert.ToBoolean(CurrentUser.AgentePercepcionIIBB)) ? "1" : "0";
                hdnPercepcionIVA.Value = (Convert.ToBoolean(CurrentUser.AgentePercepcionIVA)) ? "1" : "0";

                //ddlModo.Items.Add(new ListItem("Otro", "O"));
                ddlModo.Items.Add(new ListItem("Talonario preimpreso", "T"));//No sacar ya que sirve para que se carguen fc emitidas electronicamente desde AFIP
                if (CurrentUser.TieneFE) {
                    ddlModo.Items.Add(new ListItem("Electrónica", "E"));
                    ddlModo.SelectedValue = "E";
                    hdnTieneFE.Value = "1";
                }
                else {
                    hdnTieneFE.Value = "0";
                }

                lnkGenerarCAE.Visible = (CurrentUser.TieneFE);
                litPath.Text = "Alta";
                hdnIDPersona.Value = Request.QueryString["IDPersona"];
                hdnIDUsuario.Value = CurrentUser.IDUsuario.ToString();
                hdnFileYear.Value = DateTime.Now.Year.ToString();

                if (idPresupuesto > 0 || idRemito > 0) {
                    if (idPresupuesto > 0) {
                        hdnIDPrecarga.Value = idPresupuesto.ToString();

                        var presupuesto = dbContext.Presupuestos.Include("PresupuestoDetalle")
                            .Include("PresupuestoDetalle.Conceptos").Include("PresupuestoDetalle.Conceptos.PlanDeCuentas")
                            .Where(x => x.IDPresupuesto == idPresupuesto).FirstOrDefault();
                        if (presupuesto != null) {
                            foreach (var det in presupuesto.PresupuestoDetalle) {
                                var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                                tra.ID = ComprobanteCart.Retrieve().Items.Count() + 1;
                                tra.Concepto = det.Concepto;
                                tra.Iva = det.Iva;
                                tra.PrecioUnitario = det.PrecioUnitario;
                                tra.Bonificacion = det.Bonificacion;
                                tra.Cantidad = det.Cantidad;
                                tra.IDConcepto = det.IDConcepto;
                                tra.Codigo = det.IDConcepto.HasValue ? det.Conceptos.Codigo : "";
                                if (CurrentUser.UsaPlanCorporativo) {
                                    if (det.Conceptos != null) {
                                        tra.IDPlanDeCuenta = det.Conceptos.IDPlanDeCuentaVentas;
                                        if (det.Conceptos.PlanDeCuentas != null)
                                            tra.CodigoPlanCta = det.Conceptos.PlanDeCuentas.Codigo + " - " + det.Conceptos.PlanDeCuentas.Nombre;
                                    }
                                }

                                ComprobanteCart.Retrieve().Items.Add(tra);
                            }
                            hdnIDPersona.Value = presupuesto.IDPersona.ToString();
                            hdnIDCondicionVenta.Value = presupuesto.IDCondicionVenta.ToString();
                        }
                    }
                    //else
                    //{
                    //    hdnIDPrecarga.Value = idRemito.ToString();

                    //    var remito = dbContext.Addins_404_Remito.Include("Addins_404_RemitoDetalle").Where(x => x.IDRemito == idRemito).FirstOrDefault();
                    //    if (remito != null)
                    //    {
                    //        foreach (var det in remito.Addins_404_RemitoDetalle)
                    //        {
                    //            var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                    //            tra.ID = ComprobanteCart.Retrieve().Items.Count() + 1;
                    //            tra.Concepto = det.Concepto;
                    //            tra.Iva = det.IDConcepto.HasValue ? det.Conceptos.Iva : 0;
                    //            tra.PrecioUnitario = det.IDConcepto.HasValue ? det.Conceptos.PrecioUnitario : 0;
                    //            tra.Bonificacion = 0;
                    //            tra.Cantidad = det.Cantidad;
                    //            tra.IDConcepto = det.IDConcepto;

                    //            ComprobanteCart.Retrieve().Items.Add(tra);
                    //        }
                    //        hdnIDPersona.Value = remito.IDPersona.ToString();
                    //    }
                    //}
                }

                if (CurrentUser.UsaPlanCorporativo)
                    cargarCuentasActivo();

                //valido si se puede modificar
                litUsuarioAlta.Text = CurrentUser.Email;

                var duplicado = "0";
                var anulado = "0";
                if (!string.IsNullOrEmpty(Request.QueryString["ID"])) {
                    //Duplicar comprobante
                    if (!String.IsNullOrEmpty(Request.QueryString["Duplicar"])) {
                        hdnDuplicar.Value = Request.QueryString["ID"];
                        duplicado = Request.QueryString["Duplicar"];
                    }

                    //Anular comprobante
                    if (!String.IsNullOrEmpty(Request.QueryString["Anular"])) {
                        anulado = Request.QueryString["Anular"];
                        hdnAnular.Value = Request.QueryString["ID"];
                    }

                    hdnID.Value = Request.QueryString["ID"];
                    if (hdnID.Value != "0") {
                        var id = Convert.ToInt32(hdnID.Value);

                        bool CompEnviado = false;
                        if (String.IsNullOrEmpty(Request.QueryString["Duplicar"]))
                            CompEnviado = dbContext.ComprobantesEnviados.Any(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDComprobante == id && x.Resultado == true);
                        if (!CompEnviado && CurrentUser.EnvioAutomaticoComprobante)
                            hdnEnvioFE.Value = "1";
                        else
                            hdnEnvioFE.Value = "0";

                        var entity = dbContext.Comprobantes.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDComprobante == id).FirstOrDefault();

                        if (entity != null) {
                            if (duplicado == "1" && anulado == "1") {
                                entity.FechaCAE = null;
                            }
                            hdnIDPersona.Value = entity.IDPersona.ToString();
                            hdnFileYear.Value = entity.FechaComprobante.Year.ToString();
                            if (duplicado != "1")
                                hdnIDNroOrden.Value = entity.IDOrdenVenta.HasValue ? entity.IDOrdenVenta.Value.ToString() : "0";

                            if (entity.FechaCAE.HasValue && duplicado != "1" && anulado != "1")
                                Response.Redirect("/comprobantesv.aspx?ID=" + id);

                            litUsuarioAlta.Text = entity.UsuarioAlta;
                            litUsuarioModifica.Text = entity.UsuarioModifica;
                        }
                        if (duplicado == "1" || anulado == "1") {
                            litPath.Text = "Alta";
                            hdnID.Value = "0";

                            if (anulado == "1") {
                                hdnIntegracion.Value = entity.IDIntegracion.HasValue ? entity.IDIntegracion.Value.ToString() : "0";
                                hdnVentaIntegracion.Value = entity.IDVentaIntegracion ?? "";
                            }
                        }
                        else
                            litPath.Text = "Edición";
                    }
                }//Comprobante generado a partir de integracion
                else if (!string.IsNullOrEmpty(Request.QueryString["IdIntegracion"])) {
                    hdnIntegracion.Value = Request.QueryString["IdIntegracion"];
                    hdnVentaIntegracion.Value = Request.QueryString["IdVentaIntegracion"];
                    hdnCobranzaAutomatica.Value = "1";
                    if (CurrentUser.EnvioAutomaticoComprobante)
                        hdnEnvioFE.Value = "1";
                    else
                        hdnEnvioFE.Value = "0";

                    var entity = ComprobanteIntegracion.Retrieve();
                    if (entity != null) {
                        hdnIDPersona.Value = entity.IDPersona.ToString();
                        hdnIDCondicionVenta.Value = entity.IDCondicionVenta.ToString();

                        litUsuarioAlta.Text = entity.UsuarioAlta;
                        litUsuarioModifica.Text = entity.UsuarioModifica;
                        hdnIDNroOrden.Value = entity.IDOrdenVenta.HasValue ? entity.IDOrdenVenta.Value.ToString() : "0";
                        //ddlNroDeOrden.Disabled = true;
                    }

                    litPath.Text = "Edición";

                }
                else
                    txtObservaciones.Text = CurrentUser.ObservacionesFc;
            }
        }
    }

    private void cargarCuentasActivo() {
        using (var dbContext = new ACHEEntities()) {
            if (dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == CurrentUser.IDUsuario)) {
                var idctas = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == CurrentUser.IDUsuario).FirstOrDefault().CtasFiltroVentas.Split(',');
                List<Int32> cuentas = new List<Int32>();
                for (int i = 0; i < idctas.Length; i++)
                    cuentas.Add(Convert.ToInt32(idctas[i]));

                var listaAux = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && cuentas.Contains(x.IDPlanDeCuenta)).OrderBy(x => x.Codigo).ToList();
                ddlPlanDeCuentas.Items.Add(new ListItem("", ""));

                foreach (var item in listaAux)
                    ddlPlanDeCuentas.Items.Add(new ListItem(item.Codigo + " - " + item.Nombre, item.IDPlanDeCuenta.ToString()));
            }
            hdnUsaPlanCorporativo.Value = "1";
        }
    }

    #region Items

    [WebMethod(true)]
    public static void agregarItem(int id, string idConcepto, string concepto, string iva, string precio, string bonif, string cantidad, int idPersona, string codigo, string idPlanDeCuenta, string codigoCta, int idInventario) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        int index = id;
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        //Se saca para realizar la validacion en la capa de logica
        /*   if (idConcepto != "" && !ConceptosCommon.StockValido(int.Parse(idConcepto), idInventario, usu.IDUsuario, decimal.Parse(cantidad.Replace(".", ",")), 0))
               throw new Exception("El producto no tiene stock suficiente");
           */

        if (id != 0) {
            var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            ComprobanteCart.Retrieve().Items.Remove(aux);
        }
        else
            index = ComprobanteCart.Retrieve().Items.Count() + 1;

        var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
        tra.ID = index;// ComprobanteCart.Retrieve().Items.Count() + 1;
        tra.Concepto = concepto;
        tra.Codigo = codigo;
        tra.CodigoPlanCta = codigoCta;
        if (iva != string.Empty)
            tra.Iva = decimal.Parse(iva);
        else
            tra.Iva = 0;
        if (bonif != string.Empty)
            tra.Bonificacion = decimal.Parse(bonif.Replace(".", ","));
        else
            tra.Bonificacion = 0;

        tra.PrecioUnitario = decimal.Parse(precio.Replace(".", ","));
        tra.Cantidad = decimal.Parse(cantidad.Replace(".", ",")); ;
        if (!string.IsNullOrEmpty(idConcepto)) {
            var prod = ConceptosCommon.GetConcepto(int.Parse(idConcepto), usu.IDUsuario);

            tra.IDConcepto = int.Parse(idConcepto);
            tra.TipoConcepto = prod.Tipo;
        }
        else
            tra.IDConcepto = null;

        if (idPlanDeCuenta != "")
            tra.IDPlanDeCuenta = Convert.ToInt32(idPlanDeCuenta);

        tra.PrecioUnitario = ObtenerPrecioFinal(tra.PrecioUnitario, iva);//, idPersona);

        if (id != 0)
            ComprobanteCart.Retrieve().Items.Insert(id - 1, tra);
        else
            ComprobanteCart.Retrieve().Items.Add(tra);

    }

    [WebMethod(true)]
    public static void agregarItemEdicion(int id, string idConcepto, string concepto, string iva, string precio, string bonif, string cantidad, int idPersona, string codigo, string idPlanDeCuenta, string codigoCta, int idInventario, int idComprobante) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        int index = id;
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        //if (idConcepto != "" && !ConceptosCommon.StockValido(int.Parse(idConcepto), idInventario, usu.IDUsuario, decimal.Parse(cantidad.Replace(".", ",")), idComprobante))
        //    throw new Exception("El producto no tiene stock suficiente");


        if (id != 0) {
            var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            ComprobanteCart.Retrieve().Items.Remove(aux);
        }
        else
            index = ComprobanteCart.Retrieve().Items.Count() + 1;

        var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
        tra.ID = index;// ComprobanteCart.Retrieve().Items.Count() + 1;
        tra.Concepto = concepto;
        tra.Codigo = codigo;
        tra.CodigoPlanCta = codigoCta;
        if (iva != string.Empty)
            tra.Iva = decimal.Parse(iva);
        else
            tra.Iva = 0;
        if (bonif != string.Empty)
            tra.Bonificacion = decimal.Parse(bonif.Replace(".", ","));
        else
            tra.Bonificacion = 0;

        tra.PrecioUnitario = decimal.Parse(precio.Replace(".", ","));
        tra.Cantidad = decimal.Parse(cantidad.Replace(".", ",")); ;
        if (!string.IsNullOrEmpty(idConcepto)) {
            var prod = ConceptosCommon.GetConcepto(int.Parse(idConcepto), usu.IDUsuario);

            tra.IDConcepto = int.Parse(idConcepto);
            tra.TipoConcepto = prod.Tipo;
        }
        else
            tra.IDConcepto = null;

        if (idPlanDeCuenta != "")
            tra.IDPlanDeCuenta = Convert.ToInt32(idPlanDeCuenta);

        tra.PrecioUnitario = ObtenerPrecioFinal(tra.PrecioUnitario, iva);//, idPersona);

        if (id != 0)
            ComprobanteCart.Retrieve().Items.Insert(id - 1, tra);
        else
            ComprobanteCart.Retrieve().Items.Add(tra);

    }

    private static decimal ObtenerPrecioFinal(decimal PrecioUnitario, string iva) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        if (usu.UsaPrecioFinalConIVA) {
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);
            return ConceptosCommon.ObtenerPrecioFinal(PrecioUnitario, iva, cantidadDecimales);
        }
        else {
            return PrecioUnitario;
        }
    }

    [WebMethod(true)]
    public static void eliminarItem(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
        if (aux != null)
            ComprobanteCart.Retrieve().Items.Remove(aux);

    }

    [WebMethod(true)]
    public static string obtenerItems(int idPersona) {
        var html = string.Empty;
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        using (var dbContext = new ACHEEntities()) {
            var list = ComprobanteCart.Retrieve().Items.ToList();
            if (list.Any()) {
                int index = 1;
                foreach (var detalle in list) {
                    html += "<tr>";
                    html += "<td>" + index + "</td>";
                    html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                    html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                    html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
                    html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                    html += "<td style='text-align:right'>" + detalle.Bonificacion.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                    html += "<td style='text-align:right'>" + detalle.Iva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                    if (usu.UsaPlanCorporativo)
                        html += "<td class='divPlanDeCuentas' style='text-align:left'>" + detalle.CodigoPlanCta + "</td>";
                    else
                        html += "<td class='divPlanDeCuentas' style='text-align:left; display:none'></td>";
                    html += "<td style='text-align:right'>" + detalle.TotalConIva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";

                    decimal preciFinal = 0;
                    //var UsaPrecioConIva = dbContext.Personas.Any(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersona && (x.CondicionIva == "MO" || x.CondicionIva == "CF"));
                    //if (usu.UsaPrecioFinalConIVA || UsaPrecioConIva)
                    if (usu.UsaPrecioFinalConIVA)
                        preciFinal = detalle.PrecioUnitarioConIvaSinBonificacion;
                    else
                        preciFinal = detalle.PrecioUnitario;

                    html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                    html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem(" + detalle.ID + ", '"
                        + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
                        + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(ConfiguracionHelper.ObtenerCantidadDecimales())
                        + "','" + detalle.Concepto + "','" + preciFinal.ToMoneyFormatForEdit(ConfiguracionHelper.ObtenerCantidadDecimales())
                        + "','" + detalle.Iva.ToString("#0.00")
                        + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(ConfiguracionHelper.ObtenerCantidadDecimales())
                        + "','" + detalle.IDPlanDeCuenta.ToString()
                        + "','" + detalle.Codigo
                        + "');\"><i class='fa fa-edit'></i></a></td>";
                    html += "</tr>";

                    index++;
                }
            }
        }
        if (html == "")
            html = "<tr><td colspan='9' style='text-align:center'>No tienes items agregados</td></tr>";


        return html;
    }

    #endregion

    [WebMethod(true)]
    public static TotalesViewModel obtenerTotales(string percepcionesIIBB, string percepcionesIVA, string importeNoGrabado, string jurisdiccion) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        TotalesViewModel totales = new TotalesViewModel();

        ComprobanteCart.Retrieve().ImporteNoGrabado = Convert.ToDecimal(importeNoGrabado.Replace(".", ","));
        ComprobanteCart.Retrieve().PercepcionIVA = Convert.ToDecimal(percepcionesIVA.Replace(".", ","));
        ComprobanteCart.Retrieve().PercepcionIIBB = Convert.ToDecimal(percepcionesIIBB.Replace(".", ","));
        ComprobanteCart.Retrieve().JurisdiccionIIBB = jurisdiccion;

        totales.Iva = ComprobanteCart.Retrieve().GetIva().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
        totales.Subtotal = ComprobanteCart.Retrieve().GetGravado().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
        totales.ImporteNoGrabado = ComprobanteCart.Retrieve().GetImporteNoGrabado().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
        totales.PercepcionIVA = ComprobanteCart.Retrieve().GetPercepcionIVA().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
        totales.PercepcionIIBB = ComprobanteCart.Retrieve().GetPercepcionIIBB().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
        totales.Total = ComprobanteCart.Retrieve().GetTotal().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

        return totales;

    }

    [WebMethod(true)]
    public static void clearInfo() {
        ComprobanteCart.Retrieve().Items.Clear();
    }

    [WebMethod(true)]
    public static int guardar(int id, int idPersona, string tipo, string modo, string fecha, string condicionVenta,
        int tipoConcepto, string fechaVencimiento, int idPuntoVenta, string nroComprobante, int idInventario, string obs, int idJurisdiccion, int idIntegracion, string idVentaIntegracion, int idOrdenVenta) {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");


        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, Convert.ToDateTime(fecha)))
            throw new CustomException("No puede agregar ni modificar una comprobante que se encuentre en un periodo cerrado.");
        if (ContabilidadCommon.ValidarCorrelatividad(id, usu.IDUsuario, modo, idPuntoVenta, nroComprobante, tipo))
            throw new CustomException("Ya existe un comprobante con el mismo numero cargado.");
        if (ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(fecha)))
            throw new CustomException("No puede agregar ni modificar un comprobante con fecha anterior a la fecha límite de carga.");
        if (idJurisdiccion == 0 && ComprobanteCart.Retrieve().PercepcionIIBB > 0)
            throw new CustomException("Si informa el importe de IIBB debe informar la jurisdicción.");
        if (idJurisdiccion > 0 && ComprobanteCart.Retrieve().PercepcionIIBB == 0)
            throw new CustomException("Si informa la jurisdicción debe informar el IIBB.");
        if (idJurisdiccion > 0 && ComprobanteCart.Retrieve().PercepcionIIBB == 0)
            throw new CustomException("Si informa la jurisdicción debe informar el IIBB.");
        if (Convert.ToDateTime(fecha) > Convert.ToDateTime(fechaVencimiento))
            throw new CustomException("La fecha de vencimiento debe ser mayor o igual a la fecha de emisión.");


        using (var dbContext = new ACHEEntities()) {
            ComprobanteCartDto compCart = new ComprobanteCartDto();
            compCart.IDComprobante = id;
            compCart.IDPersona = idPersona;
            compCart.TipoComprobante = tipo;
            compCart.TipoConcepto = tipoConcepto.ToString();
            compCart.IDUsuario = usu.IDUsuario;
            compCart.Modo = modo;
            compCart.FechaComprobante = Convert.ToDateTime(fecha);
            compCart.FechaVencimiento = Convert.ToDateTime(fechaVencimiento);
            compCart.IDPuntoVenta = idPuntoVenta;
            compCart.Numero = nroComprobante;

            compCart.IDUsuarioAdicional = usu.IDUsuarioAdicional;//si se crea desde un usuario adicional se setea el id.

            var usuarioAdicionalFijo = dbContext.Personas.Where(x => x.IDUsuario == compCart.IDUsuario && x.IDPersona == compCart.IDPersona).FirstOrDefault().IDUsuarioAdicional;
            if (usuarioAdicionalFijo.HasValue)//si la persona tiene un vendedor asignado, se pisa el usuarioadicional por el vendedor de la persona.
                compCart.IDUsuarioAdicional = usuarioAdicionalFijo.Value;


            compCart.EmailUsuario = usu.Email;

            compCart.Observaciones = obs;
            compCart.IDCondicionVenta = int.Parse(condicionVenta);
            compCart.CondicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, int.Parse(condicionVenta)).Nombre;
            compCart.IDJuresdiccion = idJurisdiccion;
            //para integracion con mercado libre
            if (idIntegracion > 0) compCart.IdIntegracion = idIntegracion;
            if (!string.IsNullOrEmpty(idVentaIntegracion)) compCart.IdVentaIntegracion = idVentaIntegracion;
            compCart.Items = new List<ComprobantesDetalleViewModel>();
            compCart.Tributos = new List<ComprobantesTributosViewModel>();
            compCart.ImporteNoGrabado = ComprobanteCart.Retrieve().ImporteNoGrabado;
            compCart.PercepcionIVA = ComprobanteCart.Retrieve().PercepcionIVA;
            compCart.PercepcionIIBB = ComprobanteCart.Retrieve().PercepcionIIBB;
            compCart.Items = ComprobanteCart.Retrieve().Items;
            compCart.Tributos = ComprobanteCart.Retrieve().Tributos;
            if (idInventario > 0)
                compCart.IdInventario = idInventario;
            else
                compCart.IdInventario = null;

            if (idOrdenVenta > 0) {
                compCart.IdOrdenVenta = idOrdenVenta;
            }
            else
                compCart.IdOrdenVenta = null;


            var entity = ComprobantesCommon.Guardar(dbContext, compCart);
            //if (entity.CondicionVenta == "MercadoPago")
            //    Common.CrearCobranza(usu, id, idPersona, tipo, fecha, ref nroComprobante, obs, Common.ComprobanteModo.Generar);

            if (idIntegracion > 0)
                ComprobanteIntegracion.Dispose();
            if (entity.Modo != "E")
                InventariosCommon.AlertaStock(dbContext, entity.IDComprobante, usu.IDUsuario);

            //var mailUsuario = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).Select(x => x.EmailStock == "" ? x.Email : x.EmailStock).FirstOrDefault();

            return entity.IDComprobante;

        }

    }

    [WebMethod(true)]
    public static void EnviarComprobanteAutomaticamente(int idComprobante) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        ComprobantesCommon.EnviarComprobanteAutomaticamente(idComprobante, usu);

    }

    [WebMethod(true)]
    public static string previsualizar(int id, int idPersona, string tipo, string modo, string fecha, string condicionVenta,
        int tipoConcepto, string fechaVencimiento, int idPuntoVenta, string nroComprobante, string obs) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        string afipObs = "";
        int idcondventa = int.Parse(condicionVenta);
        string condVenta = "";
        using (var dbContext = new ACHEEntities()) {
            condVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, idcondventa).Nombre;
        }
        Common.CrearComprobante(usu, id, idPersona, tipo, modo, fecha, condVenta, tipoConcepto, fechaVencimiento, idPuntoVenta, ref nroComprobante, obs, Common.ComprobanteModo.Previsualizar, ref afipObs);
        return usu.IDUsuario + "_prev.pdf";

    }

    [WebMethod(true)]
    public static ComprobantesDescargasViewModel generarCae(int id, int idPersona, string tipo, string modo, string fecha, string condicionVenta,
        int tipoConcepto, string fechaVencimiento, int idPuntoVenta, string nroComprobante, string obs) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var archivos = new ComprobantesDescargasViewModel();
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        string afipObs = "";
        archivos.Observaciones = "";
        int idcondventa = int.Parse(condicionVenta);
        string condVenta = "";
        using (var dbContext = new ACHEEntities()) {
            condVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, idcondventa).Nombre;
        }
        Common.CrearComprobante(usu, id, idPersona, tipo, modo, fecha, condVenta, tipoConcepto, fechaVencimiento, idPuntoVenta, ref nroComprobante, obs, Common.ComprobanteModo.Generar, ref afipObs);
        if (afipObs != obs)
            archivos.Observaciones = afipObs;

        using (var dbContext = new ACHEEntities()) {
            var persona = dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersona).FirstOrDefault();

            archivos.Comprobante = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + tipo + "-" + nroComprobante + ".pdf";
            archivos.Remito = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + "R-" + nroComprobante + ".pdf";

            if (modo == "E")
                InventariosCommon.AlertaStock(dbContext, id, usu.IDUsuario);
        }
        return archivos;

    }

    [WebMethod(true)]
    public static ComprobantesEditViewModel obtenerDatos(int id, bool integracion) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        Comprobantes entity;
        using (var dbContext = new ACHEEntities()) {
            if (integracion) {
                entity = ComprobanteIntegracion.Retrieve();
            }
            else {
                entity = dbContext.Comprobantes.Include("ComprobantesDetalle").Include("ComprobantesTributos")
                   .Where(x => x.IDComprobante == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            }
            if (entity != null) {
                ComprobantesEditViewModel result = new ComprobantesEditViewModel();
                result.ID = id;
                result.IDPersona = entity.IDPersona;
                result.Fecha = entity.FechaComprobante.ToString("dd/MM/yyyy");
                result.FechaVencimiento = entity.FechaVencimiento.ToString("dd/MM/yyyy");
                result.Modo = entity.Modo;
                result.Tipo = entity.Tipo;
                result.Numero = entity.Numero.ToString("#00000000");
                result.TipoConcepto = entity.TipoConcepto;
                result.IDCondicionVenta = entity.IDCondicionVenta;

                result.IDPuntoVenta = entity.IDPuntoVenta;
                result.Observaciones = entity.Observaciones;
                result.IDInventario = entity.IDInventario ?? 0;
                result.IDOrdenVenta = entity.IDOrdenVenta ?? 0;
                var personas = dbContext.Personas.Where(x => x.IDPersona == entity.IDPersona).FirstOrDefault();
                PersonasEditViewModel PersonasEdit = new PersonasEditViewModel();
                PersonasEdit.ID = id;
                PersonasEdit.RazonSocial = personas.RazonSocial.ToUpper();
                //PersonasEdit.Email = personas.Email != null ? personas.Email.ToLower() : "";
                if (!string.IsNullOrEmpty(personas.EmailsEnvioFc) || !string.IsNullOrEmpty(personas.Email))
                    PersonasEdit.Email = !string.IsNullOrEmpty(personas.EmailsEnvioFc) ? personas.EmailsEnvioFc.ToLower() : personas.Email.ToLower(); //personas.Email.ToLower();
                PersonasEdit.CondicionIva = personas.CondicionIva;
                PersonasEdit.Domicilio = personas.Domicilio.ToUpper() + " " + personas.PisoDepto;
                PersonasEdit.Ciudad = personas.Ciudades.Nombre.ToUpper();
                PersonasEdit.Provincia = personas.Provincias.Nombre;
                PersonasEdit.TipoDoc = personas.TipoDocumento;
                PersonasEdit.NroDoc = personas.NroDocumento;

                if (usu.AgentePercepcionIIBB.HasValue && usu.AgentePercepcionIIBB.Value) {
                    PersonasEdit.PercepcionIIBB = entity.PercepcionIIBB.ToString().Replace(",", ".");
                    PersonasEdit.IDJuresdiccion = Convert.ToInt32(entity.IDJurisdiccion);
                }
                if (usu.AgentePercepcionIVA.HasValue && usu.AgentePercepcionIVA.Value)
                    PersonasEdit.PercepcionIVA = entity.PercepcionIVA.ToString().Replace(",", ".");

                PersonasEdit.ImporteNoGrabado = entity.ImporteNoGrabado.ToString();
                result.Personas = PersonasEdit;

                foreach (var det in entity.ComprobantesDetalle) {
                    var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                    tra.ID = ComprobanteCart.Retrieve().Items.Count() + 1;
                    tra.Concepto = det.Concepto;

                    if (det.IDConcepto.HasValue) {
                        var prod = dbContext.Conceptos.Where(x => x.IDUsuario == usu.IDUsuario && x.IDConcepto == det.IDConcepto).FirstOrDefault();
                        if (prod != null)
                            tra.Codigo = prod.Codigo;
                    }
                    else
                        tra.Codigo = (det.Conceptos != null) ? det.Conceptos.Codigo : "";
                    tra.CodigoPlanCta = (det.PlanDeCuentas == null) ? "" : det.PlanDeCuentas.Codigo + " - " + det.PlanDeCuentas.Nombre;
                    tra.Iva = det.Iva;
                    tra.PrecioUnitario = det.PrecioUnitario;
                    tra.Bonificacion = det.Bonificacion;
                    tra.Cantidad = det.Cantidad;
                    tra.IDConcepto = det.IDConcepto;
                    tra.IDPlanDeCuenta = det.IDPlanDeCuenta;
                    ComprobanteCart.Retrieve().Items.Add(tra);
                }
                foreach (var det in entity.ComprobantesTributos) {
                    var tra = new ComprobantesTributosViewModel();
                    tra.ID = ComprobanteCart.Retrieve().Tributos.Count() + 1;
                    tra.Descripcion = det.Descripcion;
                    tra.Alicuota = det.Iva;
                    tra.Importe = det.Importe;
                    tra.BaseImp = det.BaseImp;
                    ComprobanteCart.Retrieve().Tributos.Add(tra);
                }

                return result;
            }
            else
                throw new Exception("Error al obtener los datos");
        }
    }

    [WebMethod(true)]
    public static ComprobantesEditViewModel obtenerDatosAnulacion(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        Comprobantes entity;
        using (var dbContext = new ACHEEntities()) {
            entity = dbContext.Comprobantes.Include("ComprobantesDetalle")
                   .Where(x => x.IDComprobante == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

            if (entity != null) {
                ComprobantesEditViewModel result = new ComprobantesEditViewModel();
                result.ID = id;
                result.IDPersona = entity.IDPersona;
                result.Fecha = entity.FechaComprobante.ToString("dd/MM/yyyy");
                result.FechaVencimiento = entity.FechaVencimiento.ToString("dd/MM/yyyy");
                result.Modo = entity.Modo;

                result.Tipo = entity.Tipo;
                result.Numero = entity.Numero.ToString("#00000000");
                result.TipoConcepto = entity.TipoConcepto;
                result.IDCondicionVenta = entity.IDCondicionVenta;
                result.IDPuntoVenta = entity.IDPuntoVenta;
                result.Observaciones = entity.Observaciones;
                result.IDInventario = entity.IDInventario ?? 0;

                //result.IDIntegracion = entity.IDIntegracion;
                //result.IDVentaIntegracion = entity.IDVentaIntegracion;

                var personas = dbContext.Personas.Where(x => x.IDPersona == entity.IDPersona).FirstOrDefault();
                PersonasEditViewModel PersonasEdit = new PersonasEditViewModel();
                PersonasEdit.ID = id;
                PersonasEdit.RazonSocial = personas.RazonSocial.ToUpper();
                PersonasEdit.Email = personas.Email.ToLower();
                PersonasEdit.CondicionIva = personas.CondicionIva;
                PersonasEdit.Domicilio = personas.Domicilio.ToUpper() + " " + personas.PisoDepto;
                PersonasEdit.Ciudad = personas.Ciudades.Nombre.ToUpper();
                PersonasEdit.Provincia = personas.Provincias.Nombre;
                PersonasEdit.TipoDoc = personas.TipoDocumento;
                PersonasEdit.NroDoc = personas.NroDocumento;

                if (usu.AgentePercepcionIIBB.HasValue && usu.AgentePercepcionIIBB.Value) {
                    PersonasEdit.PercepcionIIBB = entity.PercepcionIIBB.ToString().Replace(",", ".");
                    PersonasEdit.IDJuresdiccion = Convert.ToInt32(entity.IDJurisdiccion);
                }
                if (usu.AgentePercepcionIVA.HasValue && usu.AgentePercepcionIVA.Value)
                    PersonasEdit.PercepcionIVA = entity.PercepcionIVA.ToString().Replace(",", ".");

                PersonasEdit.ImporteNoGrabado = entity.ImporteNoGrabado.ToString();
                result.Personas = PersonasEdit;

                Dictionary<decimal, ComprobantesDetalleViewModel> itemsResultado = new Dictionary<decimal, ComprobantesDetalleViewModel>();
                foreach (var det in entity.ComprobantesDetalle) {
                    var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                    tra.ID = ComprobanteCart.Retrieve().Items.Count() + 1;
                    tra.Concepto = det.Concepto;

                    if (det.IDConcepto.HasValue) {
                        var prod = dbContext.Conceptos.Where(x => x.IDUsuario == usu.IDUsuario && x.IDConcepto == det.IDConcepto).FirstOrDefault();
                        if (prod != null)
                            tra.Codigo = prod.Codigo;
                    }
                    else
                        tra.Codigo = (det.Conceptos != null) ? det.Conceptos.Codigo : "";
                    tra.CodigoPlanCta = (det.PlanDeCuentas == null) ? "" : det.PlanDeCuentas.Codigo + " - " + det.PlanDeCuentas.Nombre;
                    tra.Iva = det.Iva;
                    tra.PrecioUnitario = det.PrecioUnitario;
                    tra.Bonificacion = det.Bonificacion;
                    tra.Cantidad = det.Cantidad;
                    tra.IDConcepto = det.IDConcepto;
                    tra.IDPlanDeCuenta = det.IDPlanDeCuenta;
                    ComprobanteCart.Retrieve().Items.Add(tra);
                    /*
                   if (!itemsResultado.ContainsKey(det.Iva))
                    {

                        var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
                        tra.ID = ComprobanteCart.Retrieve().Items.Count() + 1;
                        tra.Concepto = "Anulación FC " + entity.Numero.ToString("#00000000"); ;
                        tra.IDConcepto = det.IDConcepto;
                        tra.Codigo = det.Conceptos.Codigo;
                        tra.CodigoPlanCta = (det.PlanDeCuentas == null)
                            ? ""
                            : det.PlanDeCuentas.Codigo + " - " + det.PlanDeCuentas.Nombre;
                        tra.Iva = det.Iva;
                        tra.PrecioUnitario = (det.PrecioUnitario * (1 - det.Bonificacion / (decimal)100.00)) * det.Cantidad;

                        tra.Cantidad = det.Cantidad;
                        var detallePlanCuenta = entity.ComprobantesDetalle.FirstOrDefault(c => c.PlanDeCuentas != null);
                        tra.CodigoPlanCta = (detallePlanCuenta == null) ? "" : detallePlanCuenta.PlanDeCuentas.Codigo + " - " + detallePlanCuenta.PlanDeCuentas.Nombre;
                        tra.IDPlanDeCuenta = (detallePlanCuenta == null) ? null : detallePlanCuenta.IDPlanDeCuenta;
                        itemsResultado.Add( tra);
                    }
                    else
                    {
                        ComprobantesDetalleViewModel tra = itemsResultado[det.Iva];
                        tra.PrecioUnitario += (det.PrecioUnitario * (1 - det.Bonificacion / (decimal)100.00)) *
                                              det.Cantidad;
                          
                                            }*/

                }
                foreach (var key in itemsResultado.Keys) {
                    ComprobanteCart.Retrieve().Items.Add(itemsResultado[key]);
                }

                return result;
            }
            else
                throw new Exception("Error al obtener los datos");
        }

    }

    [WebMethod(true)]
    public static void GenerarAsientosContables(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        try {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            ContabilidadCommon.AgregarAsientoDeVentas(usu, id);
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }

    }

    [WebMethod(true)]
    public static ComprobantesDescargasViewModel generarCaeRapido(string fecha, string razonSocial, string condicionIva, string tipoDoc, string nroDoc, string tipoComprobante, string condicionVenta, string importe, string iva) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        // Guardando el comprobante
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        //var comprobante = ComprobanteCart.Retrieve();

        if (ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, Convert.ToDateTime(fecha)))
            throw new CustomException("No puede agregar ni modificar una comprobante que se encuentre en un periodo cerrado.");

        if (ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(fecha)))
            throw new CustomException("No puede agregar ni modificar un comprobante con fecha anterior a la fecha límite de carga.");

        var archivos = new ComprobantesDescargasViewModel();
        using (var dbContext = new ACHEEntities()) {
            int idPersona = 0;
            var persona = PersonasCommon.ObtenerPersonaPorTipoYNroDoc(tipoDoc, nroDoc, usu.IDUsuario);
            if (persona != null)
                idPersona = persona.IDPersona;
            else {
                int idProvincia = usu.IDProvincia;
                int idCiudad = usu.IDCiudad;
                idPersona = PersonasCommon.GuardarPersonas(dbContext, 0, razonSocial, "", condicionIva, "F", tipoDoc, nroDoc, "", "", "", "C", 10, idProvincia, idCiudad, "", "", "", "", 0, "", 0, usu.IDUsuario, "0");

            }
            int idCondicion = 0;
            var strCon = "Efectivo";
            if (condicionVenta != "null" && !string.IsNullOrEmpty(condicionVenta)) {
                idCondicion = int.Parse(condicionVenta);
                strCon = "";
            }

            var pdv = UsuarioCommon.ObtenerPuntosDeVentaList(usu.IDUsuario).FirstOrDefault();

            Comprobantes comprobante = new Comprobantes {
                Personas = new Personas {
                    IDPersona = idPersona
                },
                IDUsuario = usu.IDUsuario,
                IDPuntoVenta = pdv.IDPuntoVenta,
                IDPersona = idPersona,
                FechaComprobante = Convert.ToDateTime(fecha),
                FechaVencimiento = Convert.ToDateTime(fecha).AddDays(+1),
                FechaAlta = DateTime.Now,
                Modo = "E",
                Tipo = tipoComprobante,
                TipoDocumento = tipoDoc,
                IDCondicionVenta = idCondicion,
                CondicionVenta = strCon,
                TipoConcepto = 3,
                ComprobantesDetalle = new List<ComprobantesDetalle>(),
                ComprobantesTributos = new List<ComprobantesTributos>(),
                TipoDestinatario = "C",
                Observaciones = ""
            };

            var concepto = "Varios";
            if (usu.IDUsuario == 538)
                concepto = " PRENDAS Y/O ACCESORIOS";

            comprobante.ComprobantesDetalle.Add(
                new ComprobantesDetalle {
                    Cantidad = 1,
                    Concepto = concepto,
                    PrecioUnitario = decimal.Parse(importe.Replace(".", ",")),
                    Iva = decimal.Parse(iva),
                    Bonificacion = 0,
                }
                );

            ComprobanteCart.Retrieve().Init(comprobante);
            var compCartDto = ComprobanteCart.Retrieve().GetComprobanteCartDto();
            compCartDto.IDUsuario = usu.IDUsuario;
            var entity = ComprobantesCommon.Guardar(dbContext, compCartDto);

            string afipObs = "";

            var nroComprobante = entity.Numero.ToString();

            Common.CrearComprobante(usu, entity.IDComprobante, entity.IDPersona, entity.Tipo, entity.Modo, entity.FechaComprobante.ToString("dd/MM/yyyy"), entity.CondicionVenta, entity.TipoConcepto, entity.FechaVencimiento.ToString("dd/MM/yyyy"), entity.IDPuntoVenta, ref nroComprobante, entity.Observaciones, Common.ComprobanteModo.Generar, ref afipObs);
            archivos.Observaciones = afipObs;

            if (persona == null)
                persona = dbContext.Personas.FirstOrDefault(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == entity.IDPersona);

            archivos.Comprobante = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + entity.Tipo + "-" + nroComprobante + ".pdf";
            archivos.Remito = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + "R-" + nroComprobante + ".pdf";

        }
        return archivos;

    }

    [WebMethod(true)]
    public static ComprobantesDescargasViewModel guardarRapido(string fecha, string razonSocial, string condicionIva, string tipoDoc, string nroDoc, string tipoComprobante,
        int condicionVenta, string importe, string iva) {

        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        // Guardando el comprobante
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        //var comprobante = ComprobanteCart.Retrieve();

        if (ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, Convert.ToDateTime(fecha)))
            throw new CustomException("No puede agregar ni modificar una comprobante que se encuentre en un periodo cerrado.");

        if (ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(fecha)))
            throw new CustomException("No puede agregar ni modificar un comprobante con fecha anterior a la fecha límite de carga.");

        var archivos = new ComprobantesDescargasViewModel();
        using (var dbContext = new ACHEEntities()) {
            int idPersona = 0;
            var persona = PersonasCommon.ObtenerPersonaPorTipoYNroDoc(tipoDoc, nroDoc, usu.IDUsuario);
            if (persona != null)
                idPersona = persona.IDPersona;
            else {
                int idProvincia = usu.IDProvincia;
                int idCiudad = usu.IDCiudad;

                idPersona = PersonasCommon.GuardarPersonas(dbContext, 0, razonSocial, "", condicionIva, "F", tipoDoc, nroDoc, "", "", "", "C", 10, idProvincia, idCiudad, "", "", "", "", 0, "", 0, usu.IDUsuario, "0");
            }

            var pdv = UsuarioCommon.ObtenerPuntosDeVentaList(usu.IDUsuario).FirstOrDefault();

            Comprobantes comprobante = new Comprobantes {
                Personas = new Personas {
                    IDPersona = idPersona
                },
                IDUsuario = usu.IDUsuario,
                IDPuntoVenta = pdv.IDPuntoVenta,
                IDPersona = idPersona,
                FechaComprobante = Convert.ToDateTime(fecha),
                FechaVencimiento = Convert.ToDateTime(fecha).AddDays(+1),
                FechaAlta = DateTime.Now,
                Modo = "E",
                Tipo = tipoComprobante,
                TipoDocumento = tipoDoc,
                IDCondicionVenta = condicionVenta,
                TipoConcepto = 3,
                ComprobantesDetalle = new List<ComprobantesDetalle>(),
                ComprobantesTributos = new List<ComprobantesTributos>(),
                TipoDestinatario = "C",
                Observaciones = ""
            };

            var concepto = "Varios";
            if (usu.IDUsuario == 538)
                concepto = " PRENDAS Y/O ACCESORIOS";

            comprobante.ComprobantesDetalle.Add(
                new ComprobantesDetalle {
                    Cantidad = 1,
                    Concepto = concepto,
                    PrecioUnitario = decimal.Parse(importe.Replace(".", ",")),
                    Iva = decimal.Parse(iva),
                    Bonificacion = 0,
                }
                );

            ComprobanteCart.Retrieve().Init(comprobante);
            var compCartDto = ComprobanteCart.Retrieve().GetComprobanteCartDto();
            compCartDto.IDUsuario = usu.IDUsuario;
            var entity = ComprobantesCommon.Guardar(dbContext, compCartDto);
        }
        return archivos;

    }

}
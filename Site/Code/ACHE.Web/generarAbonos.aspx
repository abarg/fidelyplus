﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="generarAbonos.aspx.cs" Inherits="generarAbonos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style type="text/css">
        .panel-heading {
            background-color: #d8dbde !important;
        }
        .panel-title {
            color: #a94442 !important;
        }
        .table-total > tbody > tr {
            background-color: #fff;
        }
        .table-total > tbody > tr > td {
            text-align: left !important;
            font-size: 18px;
        }
        .table-total > tbody > tr > td > strong {
            font-weight: 700 !important;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-tasks"></i>Facturar Abonos</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li><a href="/comprobantes.aspx">Facturación</a></li>
                <li class="active">Facturar Abonos</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">

        <div class="row">
            <form id="fromEdicion" class="col-sm-12" runat="server">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-danger" id="divError" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgError"></span>
                        </div>
                        <div class="row mb15" id="divFiltros">
                            <div class="col-sm-8 col-md-8">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            <input class="form-control required validDate validFechaActual" id="txtFecha" placeholder="Fecha de inicio" maxlength="10" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 form-group">
                                        <asp:DropDownList runat="server" ID="ddlFrecuencia" CssClass="form-control">
                                            <asp:ListItem Value="">Frecuencia</asp:ListItem>
                                            <asp:ListItem Value="A">Anual</asp:ListItem>
                                            <asp:ListItem Value="S">Semestral</asp:ListItem>
                                            <asp:ListItem Value="T">Trimestral</asp:ListItem>
                                            <asp:ListItem Value="M">Mensual</asp:ListItem>
                                            <asp:ListItem Value="Q">Quincenal</asp:ListItem>
                                        </asp:DropDownList>
                                    
                                    </div>
                                   <div class="col-sm-12 col-md-4 form-group">
                                        <input type="text" class="form-control" id="txtNombre" maxlength="128" placeholder="Ingrese el nombre del abono" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 responsive-buttons md">
                                <div class="btn-group" id="btnAcciones">
                                    <a class="btn btn-black mr10" onclick="generarAbonos.filtrar();" id="btnBuscar">
                                        Buscar
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row mb15" id="ContResultados" style="display: block">
                            <div class="alert alert-danger" id="div1" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Lo sentimos!</strong> <span id="Span1"></span>
                            </div>
                            <asp:Literal runat="server" ID="litTabla"></asp:Literal>

                            <div id="resultsContainer"></div>
                        </div>

                        <div class="row mb15 panel-area" id="divfooter" style="display: none">
                            <div class="col-sm-6">
                                <div class="panel-container">
                                    <div class="panel-header panel-color-3">
                                        <span>Generar comprobantes</span>
                                    </div>
                                    <div class="panel-content">

                                        <div class="row mb15">
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Fecha de Emisión</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        <input class="form-control required validDate validFechaComprobanteActual" id="txtFechaComprobante" placeholder="dd/mm/yyyy" maxlength="10" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Fecha de Vencimiento</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        <input class="form-control required validDate" id="txtFechaVencimiento" placeholder="dd/mm/yyyy" maxlength="10" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb15">
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Modo</label>

                                                    <asp:DropDownList runat="server" ID="ddlModo" CssClass="form-control required" onchange="generarAbonos.changeModoFacturacion();">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4" id="divPuntoVenta">
                                                <div class="form-group">
                                                    <label class="control-label"><span class="asterisk">*</span> Punto de venta</label>
                                                    <asp:DropDownList runat="server" ID="ddlPuntoVenta" CssClass="form-control required" onchange="generarAbonos.changePuntoDeVenta();">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb15" id="divNroComprobantes">
                                            <div class="col-md-6 col-lg-4" id="divNroComprobanteA">
                                                <div class="form-group">
                                                    <label class="control-label" id="lblNroComprobante"><span class="asterisk" id="NroComprobante">*</span> Proximo Nro. Comprobante Factura A</label>
                                                    <asp:TextBox runat="server" ID="txtNumeroFacturaA" CssClass="form-control required" MaxLength="9"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4" id="divNroComprobanteB">
                                                <div class="form-group">
                                                    <label class="control-label" id="Label1"><span class="asterisk" id="Span2">*</span> Proximo Nro. Comprobante Factura B</label>
                                                    <asp:TextBox runat="server" ID="txtNumeroFacturaB" CssClass="form-control required" MaxLength="9"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-lg-4" id="divNroComprobanteC" style="display: none">
                                                <div class="form-group">
                                                    <label class="control-label" id="Label2"><span class="asterisk" id="Span3">*</span> Proximo Nro. Comprobante Factura C</label>
                                                    <asp:TextBox runat="server" ID="txtNumeroFacturaC" CssClass="form-control required" MaxLength="9"></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row mb15">
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <a id="btnGenerar" class="btn btn-success" onclick="generarAbonos.ingresarDatos();">Generar</a>
                                                    <img alt="" src="/images/loaders/loader1.gif" id="imgLoading2" style="display: none; margin: 29px -10px; padding: 7px 33px" />
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="CondicionIva" runat="server" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <div class="panel-container" id="tableTotalContainer">
                                    <div class="panel-header panel-color-2">
                                        <span>TOTALES A FACTURAR</span>
                                    </div>
                                    <div class="panel-content">
                                        <table class="table table-total table-striped">
                                            <tbody>
                                                <tr>
                                                    <td><strong>Cant. Total:</strong></td>
                                                    <td style="border: 0; width: 70%"><span id="idTotalCant"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Total Importe:</strong></td>
                                                    <td style="border: 0; width: 70%">$<span id="idTotalImportes"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Total IVA:</strong></td>
                                                    <td style="border: 0; width: 70%">$<span id="idTotalIva"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Total:</strong></td>
                                                    <td style="border: 0; width: 70%">$<span id="idTotal"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <input type="hidden" value="0" id="EsFacturaElectronica" runat="server" />
                <input type="hidden" value="0" id="hdnEnvioFE" runat="server" />
                <asp:HiddenField runat="server" ID="hdnFileYear" Value="0" />
                <input type="hidden" value="0" id="hdnCantAbonos" runat="server" />
                <input type="hidden" value="0" id="hdnCantAbonosPorCategoria" runat="server" />
            </form>
        </div>
    </div>

    <!-- MODAL -->
    <div class="modal modal-wide fade" id="modalPdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Visualización de comprobante</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="alert alert-danger" id="divErrorCat" style="display: none">
                            <strong>Lo sentimos! </strong><span id="msgErrorCat"></span>
                        </div>

                        <iframe id="ifrPdf" src="" width="900px" height="500px" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" type="button" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal fade" id="ModalFacturacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="litModalOkTitulo">Comprobante emitido correctamente. </h4>
                </div>
                <div class="modal-body" style="min-height: 200px;">
                    <div class="alert alert-danger" id="divErrorEnvioFE" style="display: none">
                        <strong>Lo sentimos! </strong><span id="msgErrorEnvioFE"></span>
                    </div>
                    <div class="alert alert-success" id="divOkMail" style="display: none">
                        <strong>Bien hecho! </strong>El mensaje ha sido enviado correctamente
                    </div>
                    <div id="divToolbar">
                        <div class="col-sm-3 CAJA_BLANCA_AZUL" onclick="Toolbar.mostrarEnvio();">
                            <a id="lnkSendMail">
                                <span class="glyphicon glyphicon-envelope" id="imgMailEnvio" style="font-size: 30px;"></span>
                                <br />
                                <span id="spSendMail">Enviar por Email</span>&nbsp;<i style="color: #17a08c" id="iCheckEnvio" title="El correo automatico fue enviado correctamente"></i>
                            </a>
                        </div>
                        <div class="col-sm-3 CAJA_BLANCA_AZUL">
                            <a href="#" id="lnkDownloadPdf" download>
                                <span class="fa fa-file-text" style="font-size: 35px;"></span>
                                <br />
                                Descargar en PDF
                            </a>
                        </div>
                        <div class="col-sm-3 CAJA_BLANCA_AZUL hide">
                            <i class="glyphicon glyphicon-usd" style="font-size: 30px; opacity: 1; border: none; padding: 0"></i>
                            <br />
                            Cobrar
                        </div>
                        <div class="col-sm-3 CAJA_BLANCA_AZUL" onclick="Common.imprimirArchivoDesdeIframe('');">
                            <a id="lnkPrintPdf">
                                <span class="glyphicon glyphicon-print" style="font-size: 30px;"></span>
                                <br />
                                Imprimir
                            </a>
                        </div>
                    </div>
                    <div id="divSendEmail" style="display: none">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-danger" id="divErrorMail" style="display: none">
                                    <strong>Lo sentimos! </strong><span id="msgErrorMail"></span>
                                </div>
                                <form id="frmSendMail">
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Para: <small>(separa las direcciones mediante una coma) </small></p>
                                        <input id="txtEnvioPara" class="form-control required multiemails" type="text" />
                                        <%--<span id="msgErrorEnvioPara" class="help-block" style="display: none">Una de las direcciones ingresadas es inválida.</span>--%>
                                    </div>
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Asunto: </p>
                                        <input id="txtEnvioAsunto" class="form-control required" type="text" maxlength="150" />
                                        <span id="msgErrorEnvioAsunto" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                    </div>
                                    <div class="form-group">
                                        <p><span class="asterisk">*</span> Mensaje: </p>
                                        <textarea rows="5" id="txtEnvioMensaje" class="form-control required"></textarea>
                                        <span id="msgErrorEnvioMensaje" class="help-block" style="display: none">Este campo es obligatorio.</span>
                                    </div>
                                    <input type="hidden" id="hdnFile" />
                                    <input type="hidden" id="hdnRazonSocial" />
                                    <br />
                                    <a id="btnEnviar" type="button" class="btn btn-success" onclick="Toolbar.enviarComprobantePorMail();">Enviar</a>
                                    <a style="margin-left: 20px" href="#" type="button" onclick="Toolbar.cancelarEnvio();">Cancelar</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal" onclick="">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <script id="resultTemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <div class="panel-container">
            <div class="panel-header panel-color-1">
                <a data-toggle="collapse" href="#collapse_${ID}" aria-expanded="true" aria-controls="collapse1"><i class="fa fa-caret-down"></i> ${Nombre} - CANT: ${Cantidad} - TOTAL: $ ${TotalCant} </a>
            </div>
            <!-- panel-heading -->
            <div class="panel-content">
                <div class="collapse in" id="collapse_${ID}">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="table-header">
                                <tr>
                                    <td class="item-header">
                                        <input type='checkbox' id="chkHead_${ID}" checked='checked' title="Seleccionar/Deseleccionar todos" onclick="generarAbonos.ToggleCheck(this);" /><label for="chkHead_${ID}"></label>
                                    </td>
                                    <td style="width: 30%">Razón Social</td>
                                    <td style="width: 10%">CUIT</td>
                                    <td style="width: 10%">Condicion Iva</td>
                                    <td style="width: 5%">Cant</td>
                                    <td style="width: 10%">Importe</td>
                                    <td style="width: 10%">IVA</td>
                                    <td style="width: 10%">Total</td>
                                    <td style="width: 15%">Estado</td>
                                </tr>
                            </thead>
                            <tbody>
                                {{each Items}}
                                    <tr>
                                        <td class="item-header">{{if FEGenerada == "Comprobante ya emitido." }}
                                            <input type='checkbox' id='chkAbono_${nroRegistro}' importe="${Importe}" iva="${ivaCalculado}" cantidad="${Cantidad}" onchange="generarAbonos.ActualizarTotales()" />
                                            {{else}}
                                            <input type='checkbox' id='chkAbono_${nroRegistro}' checked="checked" importe="${Importe}" iva="${ivaCalculado}" cantidad="${Cantidad}" onchange="generarAbonos.ActualizarTotales()" />
                                            {{/if}}
                                            <label for="chkAbono_${nroRegistro}"><strong>ITEM ${nroRegistro}</strong></label>
                                        </td>
                                        <td><strong class="responsive">Razón Social</strong>${RazonSocial}</td>
                                        <td><strong class="responsive">CUIT</strong>${Cuit}</td>
                                        <td><strong class="responsive">Condicion Iva</strong>${CondicionIva}</td>
                                        <td><strong class="responsive">Cant</strong>${Cantidad}</td>
                                        <td><strong class="responsive">Importe</strong>$${ImportaPantalla}</td>
                                        <td><strong class="responsive">IVA</strong>$${ivaCalculado}</td>
                                        <td><strong class="responsive">Total</strong>$${TotalPantalla}</td>
                                        <td><strong class="responsive">Estado</strong>{{if FEGenerada == "Comprobante ya emitido." }}
                                                <label class="label label-warning">${FEGenerada} ${FEGeneradaObs}</label>
                                            {{else}}
                                                <label class="label label-danger">${FEGenerada} ${FEGeneradaObs}</label>
                                            {{/if}}
                                        </td>
                                    </tr>
                                {{/each}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{/each}}
        
    </script>

    <script id="resultComprobanteFETemplate" type="text/x-jQuery-tmpl">
        {{each results}}
        <div class="panel-container">
            <div class="panel-header panel-color-1">
                <a data-toggle="collapse" href="#collapse_${ID}" aria-expanded="true" aria-controls="collapse1"><i class="fa fa-caret-down"></i> ${Nombre} - CANT: ${Cantidad} - TOTAL: $ ${TotalCant} </a>
            </div>
            <!-- panel-heading -->
            <div class="panel-content">
                <div class="collapse in" id="collapse_${ID}">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="table-header">
                                <tr>
                                    <td style="width: 15%">Nro de Factura</td>
                                    <td style="width: 25%">Razón Social</td>
                                    <td style="width: 10%">CUIT</td>
                                    <td style="width: 5%">Condición Iva</td>
                                    <td style="width: 5%">Importe</td>
                                    <td style="width: 5%">IVA</td>
                                    <td style="width: 5%">Total</td>
                                    <td style="width: 10%">Estado</td>
                                    {{if  $("#EsFacturaElectronica").val() == "1"}}
                                        <td style='width: 10%'>Opciones envío</td>
                                    {{/if}}
                                </tr>
                            </thead>
                            <tbody>
                                {{each Items}}
                                    <tr>
                                        <td><strong class="responsive">Nro de Factura</strong>${nroComprobante}</td>
                                        <td><strong class="responsive">Razón Social</strong>${RazonSocial}</td>
                                        <td><strong class="responsive">Cuit</strong>${Cuit}</td>
                                        <td><strong class="responsive">Condición Iva</strong>${CondicionIva}</td>
                                        <td><strong class="responsive">Importe</strong>$${ImportaPantalla}</td>
                                        <td><strong class="responsive">IVA</strong>$${ivaCalculado}</td>
                                        <td><strong class="responsive">Total</strong>$${TotalPantalla}</td>

                                        {{if Estado != "Comprobante generado." }}
                                             {{if Estado == "Comprobante no seleccionado." }}
                                                <td>${Estado}</td>
                                            {{else}}
                                                <td>
                                                    <strong class="responsive">Estado</strong><label class="label label-danger" style="white-space: normal; max-width: 200px; display: inherit; min-width: 134px;">${Estado}</label>
                                                </td>
                                            {{/if}}
                                        {{else}}
                                            <td><strong class="responsive">Estado</strong><label class="label label-success">${Estado}</label></td>
                                        {{/if}}

                                        {{if $("#EsFacturaElectronica").val() == "1" }}
                                            <td>
                                                <a href='#' onclick="generarAbonos.showModalFacturacion( '${URL}' , '${RazonSocial}' , '${ClienteEmail}' , '${EnvioFE}' );">${FEGenerada}</a>
                                            </td>
                                        {{/if}}
                                    </tr>
                                {{/each}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{/each}}
        
    </script>

    <script id="noResultTemplate" type="text/x-jQuery-tmpl">
        <p>No se han encontrado resultados</p>
    </script>
    <!-- MODAL MSJ GENERACION FACTURAS -->
    <div class="modal modal-wide fade" id="modalGeneracionAbonos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <%--<div class="modal-header">
                    <h4 class="modal-title" id="modalTitle">Facturación de abonos</h4>
                </div>--%>
                <div class="modal-body">
                    <div class="icon">
                        <img style="display: none;" id="imgAbonoOK" src="/images/success.svg"><!-- exitoso -->
                        <img style="display: none;" id="imgAbonoError" src="/images/loading.svg"><!-- atención -->
                        <img style="display: none;" id="imgAbonoCargando" src="/images/reload.svg"><!-- procesando -->
                    </div>
                    
                    <div id="divWait">
                        <strong>Por favor, espere unos minutos. </strong>Se están facturando los abonos...
                    </div>

                    <h4 class="text-center text-uppercase title">
                        <span style="display: none;"id="msgAbonoOK" >¡Acción completada con éxito!</span><!-- exitoso -->
                        <span style="display: none;"id="msgAbonoError">¡Atención!</span><!-- exitoso -->
                    </h4>

                    <div id="divOk" style="display: none;">
                        <label class="label label-success" id="lblAbonoResultadoOK"><i class="fa fa-check"></i> <strong id="spnAbonoOK"></strong> se han procesado correctamente.</label>
                        <label class="label label-danger" id="lblAbonoResultadoError"><i class="fa fa-close"></i> <strong id="spnAbonoError"></strong> NO se pudieron procesar.</label>
                    </div>
                    <%--<div class="row">
                        <div id="divCerrar"  style="text-align: center;padding: 10px;">
                            <button style="margin-left: 20px" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>--%>
                    <div id="divCerrar">
                        <button class="btn btn-black" data-dismiss="modal" aria-label="Close">Cerrar</button>
                    </div>
                </div>
                <%--<div class="modal-footer" id="divCerrar">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>--%>
            </div>
        </div>
     </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/numeral.min.js"></script>
    <script src="/js/views/generarAbonos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script>
        jQuery(document).ready(function () {
            generarAbonos.configFilters();
        });
    </script>
</asp:Content>


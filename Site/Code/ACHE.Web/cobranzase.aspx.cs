﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Collections.Specialized;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;
using ACHE.Negocio.Tesoreria;

public partial class cobranzase : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            CobranzaCart.Retrieve().Items.Clear();
            CobranzaCart.Retrieve().FormasDePago.Clear();
            CobranzaCart.Retrieve().Retenciones.Clear();

            txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdnFileYear.Value = DateTime.Now.Year.ToString();

            hdnTieneFE.Value = "0";
            litPath.Text = "Alta";
            hdnEnvioCR.Value = (CurrentUser.EnvioAutomaticoRecibo == true) ? "1" : "0";

            hdnIDPersona.Value = Request.QueryString["IDPersona"];
            using (var dbContext = new ACHEEntities()) {
                if (!string.IsNullOrWhiteSpace(Request.QueryString["IDComprobante"])) {
                    var idComprobante = Convert.ToInt32(Request.QueryString["IDComprobante"]);
                    hdntieneComprobante.Value = idComprobante.ToString();
                    var comprobante = dbContext.Comprobantes.Where(x => x.IDComprobante == idComprobante).FirstOrDefault();
                    hdnIDPersona.Value = comprobante.IDPersona.ToString();
                }

                //valido si se puede modificar
                litUsuarioAlta.Text = CurrentUser.Email;
                if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                    hdnID.Value = Request.QueryString["ID"];
                    if (hdnID.Value != "0") {
                        ddlPuntoVenta.Disabled = true;

                        int id = int.Parse(hdnID.Value);
                        lnkPrint2.Attributes.Add("onclick", "Common.imprimirArchivoDesdeIframe('');");

                        litPath.Text = "Edición";

                        bool CompEnviado = dbContext.ComprobantesEnviados.Any(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDCobranza == id && x.Resultado == true);
                        if (!CompEnviado && CurrentUser.EnvioAutomaticoRecibo)
                            hdnEnvioCR.Value = "1";
                        else
                            hdnEnvioCR.Value = "0";

                        var cobranza = dbContext.Cobranzas.Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDCobranza == id).FirstOrDefault();
                        txtEnvioPara.Value = string.IsNullOrEmpty(cobranza.Personas.EmailsEnvioFc) ? cobranza.Personas.Email : cobranza.Personas.EmailsEnvioFc;
                        hdnRazonSocial.Value = cobranza.Personas.RazonSocial;
                        hdnFileYear.Value = cobranza.FechaCobranza.Year.ToString();

                        var punto = cobranza.PuntosDeVenta;
                        ddlPuntoVenta.Items.Add(new ListItem(punto.Punto.ToString("#0000"), punto.IDPuntoVenta.ToString()));

                        litUsuarioAlta.Text = cobranza.UsuarioAlta;
                        litUsuarioModifica.Text = cobranza.UsuarioModifica;
                    }
                }
            }
        }
    }

    #region Items

    [WebMethod(true)]
    public static void agregarItem(int id, string idComprobante, string comprobante, string importe, string saldo) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var _idcomprobante = int.Parse(idComprobante);
        var importePagar = decimal.Parse(importe.Replace(".", ","));

        if (_idcomprobante > 0) {
            var importeComprobante = decimal.Parse(saldo.Replace(".", ""));
            if (importePagar > importeComprobante)
                throw new Exception("El importe debe ser menor o igual al saldo");
        }

        if (id != 0) {
            var aux = CobranzaCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            CobranzaCart.Retrieve().Items.Remove(aux);
        }

        var tra = new CobranzasDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
        tra.ID = CobranzaCart.Retrieve().Items.Count() + 1;
        tra.Comprobante = comprobante;
        tra.Importe = importePagar;


        if (_idcomprobante > 0)
            tra.IDComprobante = _idcomprobante;
        else {
            tra.IDComprobante = null;
        }

        CobranzaCart.Retrieve().Items.Add(tra);

    }

    [WebMethod(true)]
    public static void eliminarItem(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var aux = CobranzaCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
        if (aux != null)
            CobranzaCart.Retrieve().Items.Remove(aux);

    }

    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static TotalesViewModel obtenerTotales() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        TotalesViewModel totales = new TotalesViewModel();
        totales.Total = CobranzaCart.Retrieve().GetTotal().ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

        return totales;

    }

    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string obtenerItems() {
        var html = string.Empty;
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        using (var dbContext = new ACHEEntities()) {
            var list = CobranzaCart.Retrieve().Items.OrderBy(x => x.Comprobante).ToList();
            if (list.Any()) {
                int index = 1;
                foreach (var detalle in list) {
                    html += "<tr>";
                    html += "<td>" + index + "</td>";
                    html += "<td style='text-align:left'>" + detalle.Comprobante + "</td>";
                    html += "<td style='text-align:right'>" + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                    //html += "<td style='text-align:right'>" + detalle.RetGanancias.ToMoneyFormat() + "</td>";
                    //html += "<td style='text-align:right'>" + detalle.IIBB.ToMoneyFormat() + "</td>";
                    //html += "<td style='text-align:right'>" + detalle.SUSS.ToMoneyFormat() + "</td>";
                    //html += "<td style='text-align:right'>" + detalle.Otros.ToMoneyFormat() + "</td>";
                    //html += "<td style='text-align:right'>" + detalle.Total.ToMoneyFormat() + "</td>";

                    html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                    html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem(" + detalle.ID + ", '" + detalle.IDComprobante + "' ,'";
                    html += detalle.Importe.ToString("").Replace(",", ".") + "')\""; //,'" + detalle.RetGanancias.ToString("").Replace(",", ".") + "','";
                    //html += detalle.IIBB.ToString("").Replace(",", ".") + "','" + detalle.SUSS.ToString("").Replace(",", ".") + "','" + detalle.Otros.ToString("").Replace(",", ".") + "')\"";
                    html += "><i class='fa fa-edit'></i></a></td>";
                    html += "</tr>";

                    index++;
                }
            }
        }
        if (html == "")
            html = "<tr><td colspan='4' style='text-align:center'>No tienes items agregados</td></tr>";


        return html;
    }

    #endregion

    #region Formas de Pago

    [WebMethod(true)]
    public static void agregarForma(int id, string forma, string nroRef, string importe, string idcheque, string idBanco, string idNotaCredito, string idCaja) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        if (id != 0) {
            var aux = CobranzaCart.Retrieve().FormasDePago.Where(x => x.ID == id).FirstOrDefault();
            CobranzaCart.Retrieve().FormasDePago.Remove(aux);
        }

        var tra = new CobranzasFormasDePagoViewModel();
        tra.ID = CobranzaCart.Retrieve().FormasDePago.Count() + 1;
        tra.FormaDePago = forma;
        tra.NroReferencia = nroRef;
        tra.Importe = decimal.Parse(importe.Replace(".", ","));

        if (!string.IsNullOrWhiteSpace(idcheque))
            tra.IDCheque = int.Parse(idcheque);

        if (!string.IsNullOrWhiteSpace(idBanco))
            tra.IDBanco = int.Parse(idBanco);

        if (!string.IsNullOrWhiteSpace(idCaja))
            tra.IDCaja = int.Parse(idCaja);

        if (!string.IsNullOrWhiteSpace(idNotaCredito))
            tra.IDNotaCredito = int.Parse(idNotaCredito);

        CobranzaCart.Retrieve().FormasDePago.Add(tra);

    }

    [WebMethod(true)]
    public static void eliminarForma(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var aux = CobranzaCart.Retrieve().FormasDePago.Where(x => x.ID == id).FirstOrDefault();
        if (aux != null)
            CobranzaCart.Retrieve().FormasDePago.Remove(aux);

    }

    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string obtenerFormas() {
        var html = string.Empty;
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        using (var dbContext = new ACHEEntities()) {
            var list = CobranzaCart.Retrieve().FormasDePago.OrderBy(x => x.FormaDePago).ToList();
            if (list.Any()) {
                int index = 1;
                foreach (var detalle in list) {
                    html += "<tr>";
                    html += "<td>" + index + "</td>";
                    html += "<td style='text-align:left'>" + detalle.FormaDePago + "</td>";
                    html += "<td style='text-align:left'>" + detalle.NroReferencia + "</td>";
                    html += "<td style='text-align:right'>" + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                    html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarForma(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                    html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarForma(" + detalle.ID + ", '" + detalle.FormaDePago + "', '" + detalle.NroReferencia + "','" + detalle.Importe.ToString("").Replace(",", ".") + "','" + detalle.IDBanco + "','" + detalle.IDCheque + "','" + detalle.IDNotaCredito + "','" + detalle.IDCaja + "');\"><i class='fa fa-edit'></i></a></td>";
                    html += "</tr>";

                    index++;
                }
            }
        }
        if (html == "")
            html = "<tr><td colspan='5' style='text-align:center'>No tienes items agregados</td></tr>";


        return html;
    }

    [WebMethod(true)]
    public static string obtenerFormasTotal() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var importeFormas = CobranzaCart.Retrieve().FormasDePago.ToList().Sum(x => x.Importe);
        //var importeComprobantes = CobranzaCart.Retrieve().Items.ToList().Sum(x => x.Importe);

        return Math.Round(importeFormas, ConfiguracionHelper.ObtenerCantidadDecimales()).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

    }

    #endregion

    #region Retenciones

    [WebMethod(true)]
    public static void agregarRetencion(int id, string tipo, string nroRef, string importe, string idJuridiccion, string fecha) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        if (id != 0) {
            var aux = CobranzaCart.Retrieve().Retenciones.Where(x => x.ID == id).FirstOrDefault();
            CobranzaCart.Retrieve().Retenciones.Remove(aux);
        }

        var tra = new CobranzasRetencionesViewModel();
        tra.ID = CobranzaCart.Retrieve().Retenciones.Count() + 1;
        tra.Tipo = tipo;
        tra.NroReferencia = nroRef;
        tra.Importe = decimal.Parse(importe.Replace(".", ","));
        tra.Fecha = fecha;
        if (idJuridiccion != "")
            tra.IDJurisdiccion = int.Parse(idJuridiccion);
        else
            tra.IDJurisdiccion = 0;
        CobranzaCart.Retrieve().Retenciones.Add(tra);

    }

    [WebMethod(true)]
    public static void eliminarRetencion(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var aux = CobranzaCart.Retrieve().Retenciones.Where(x => x.ID == id).FirstOrDefault();
        if (aux != null)
            CobranzaCart.Retrieve().Retenciones.Remove(aux);

    }

    [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string obtenerRetenciones() {

        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null) {
            //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities()) {
                var list = CobranzaCart.Retrieve().Retenciones.OrderBy(x => x.Tipo).ToList();
                if (list.Any()) {
                    int index = 1;
                    foreach (var detalle in list) {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Tipo + "</td>";
                        html += "<td style='text-align:left'>" + detalle.NroReferencia + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        //  html += "<td style='text-align:right'>" + detalle.Fecha + "</td>";

                        html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:CobRetenciones.eliminarRet(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:CobRetenciones.modificarRet(" + detalle.ID + ", '" + detalle.Tipo + "', '" + detalle.NroReferencia + "','" + detalle.Importe.ToString("").Replace(",", ".") + "','" + ((detalle.IDJurisdiccion == 0) ? "" : detalle.IDJurisdiccion.ToString()) + "','" + detalle.Fecha + "');\"><i class='fa fa-edit'></i></a></td>";
                        html += "</tr>";

                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='5' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }

    [WebMethod(true)]
    public static string obtenerRetencionesTotal() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var importe = CobranzaCart.Retrieve().Retenciones.ToList().Sum(x => x.Importe);// +CobranzaCart.Retrieve().FormasDePago.ToList().Sum(x => x.Importe);
        //var importeComprobantes = CobranzaCart.Retrieve().Items.ToList().Sum(x => x.Importe);

        //return Math.Round((importeComprobantes - importe), 2).ToMoneyFormat();
        return Math.Round(importe, ConfiguracionHelper.ObtenerCantidadDecimales()).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

    }

    #endregion

    [WebMethod(true)]
    public static void clearInfo() {
        CobranzaCart.Retrieve().Items.Clear();
        CobranzaCart.Retrieve().FormasDePago.Clear();
        CobranzaCart.Retrieve().Retenciones.Clear();
    }

    [WebMethod(true)]
    public static string obtenerSaldo() {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var importe = CobranzaCart.Retrieve().Retenciones.ToList().Sum(x => x.Importe) + CobranzaCart.Retrieve().FormasDePago.ToList().Sum(x => x.Importe);
        var importeComprobantes = CobranzaCart.Retrieve().Items.ToList().Sum(x => x.Importe);

        return Math.Round((importeComprobantes - importe), ConfiguracionHelper.ObtenerCantidadDecimales()).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

    }

    [WebMethod(true)]
    public static int guardarConSobranteACuenta() {
        return 1;
    }


    //[WebMethod(true)]
    //public static int actualizarSaldosPorComprobanteManualmente(string ids)
    //{

    //    string[] idsCobranzas = ids.Split(",");

    //    foreach (string id in idsCobranzas)
    //    {
    //        using (var dbContext = new ACHEEntities()) {


    //            int idCobranza = int.Parse(id);

    //            var entity = dbContext.Cobranzas
    //                .Include("CobranzasDetalle").Include("CobranzasFormasDePago").Include("CobranzasRetenciones").Include("CobranzasFormasDePago.Comprobantes")
    //                .Where(x => x.IDCobranza == idCobranza).FirstOrDefault();

    //            //procesamos los saldos de los detalles
    //            List<int> listComp = new List<int>();
    //            if (entity.CobranzasDetalle.Any())
    //            {
    //                foreach (var item in entity.CobranzasDetalle)
    //                {
    //                    if (item.IDComprobante.HasValue)
    //                    {
    //                        if (!listComp.Any(x => x == item.IDComprobante.Value))
    //                            listComp.Add(item.IDComprobante.Value);
    //                    }
    //                }
    //            }

    //            int result = dbContext.ActualizarSaldosPorCobranza(int.Parse(id));


    //            foreach (var item in listComp)
    //                dbContext.ActualizarSaldosPorComprobante(item);


    //            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];


    //            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), id.ToString(), result.ToString());


    //            ContabilidadCommon.AgregarAsientoDeCobranza(usu, entity.IDCobranza);



    //        }
    //    }

    //    return 0;
    //}

    [WebMethod(true)]
    public static int actualizarAsientosManualmente(string ids)
    {

        string[] idsCobranzas = ids.Split(",");

        foreach (string id in idsCobranzas)
        {
            using (var dbContext = new ACHEEntities())
            {

                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                ContabilidadCommon.AgregarAsientoDeCobranza(usu, int.Parse(id));

            }
        }

        return 0;
    }

    [WebMethod(true)]
    public static int guardar(int id, int idPersona, string tipo, string fecha, int idPuntoVenta, string nroComprobante, string obs, bool sobranteACuenta) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        CobranzaCartDto cobranza = new CobranzaCartDto();
        cobranza.IDCobranza = id;
        cobranza.IDPersona = idPersona;
        cobranza.Tipo = tipo;
        cobranza.Fecha = fecha;
        cobranza.IDPuntoVenta = idPuntoVenta;
        cobranza.NumeroCobranza = nroComprobante;
        cobranza.Observaciones = obs;

        cobranza.Items = CobranzaCart.Retrieve().Items;
        cobranza.FormasDePago = CobranzaCart.Retrieve().FormasDePago;
        cobranza.Retenciones = CobranzaCart.Retrieve().Retenciones;

        var entity = CobranzasCommon.Guardar(cobranza, usu, sobranteACuenta);

        if (PermisosModulos.tienePlan("alertas.aspx"))
            CobranzasCommon.GenerarAlertas(entity);

        return entity.IDCobranza;

    }

    [WebMethod(true)]
    public static string previsualizar(int id, int idPersona, string tipo, /*string modo,*/ string fecha, int idPuntoVenta, string nroComprobante, string obs) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        if (CobranzaCart.Retrieve().Items.Count == 0)
            throw new Exception("Ingrese un comprobante");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        Common.CrearCobranza(usu, id, idPersona, tipo, fecha, ref nroComprobante, obs, Common.ComprobanteModo.Previsualizar);
        return usu.IDUsuario.ToString() + "_prev.pdf";

    }

    [WebMethod(true)]
    public static string generarPDF(int id, int idPersona, string tipo, string fecha, int idPuntoVenta, string nroComprobante, string obs) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        Common.CrearCobranza(usu, id, idPersona, tipo, fecha, ref nroComprobante, obs, Common.ComprobanteModo.Generar);
        var razonSocial = string.Empty;
        using (var dbContext = new ACHEEntities()) {
            razonSocial = dbContext.Personas.Where(x => x.IDPersona == idPersona).FirstOrDefault().RazonSocial;
        }

        return razonSocial.RemoverCaracteresParaPDF() + "_" + nroComprobante + ".pdf";

    }

    [WebMethod(true)]
    public static void EnviarCobranzaAutomaticamente(int idCobranza, string nombre) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");


        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        CobranzasCommon.EnviarCobranzaAutomaticamente(idCobranza, nombre, usu);
        /*
        if (usu.EnvioAutomaticoRecibo == true)
        {
            using (var dbContext = new ACHEEntities())
            {
                try
                {
                    var plandeCuenta = PermisosModulosCommon.ObtenerPlanActual(dbContext, usu.IDUsuario);
                    if (plandeCuenta.IDPlan >= 3)//Plan Pyme
                    {
                        //TODO: PASAR A NEGOCIO
                        bool CompEnviado = dbContext.ComprobantesEnviados.Any(x => x.IDUsuario == usu.IDUsuario && x.IDCobranza == idCobranza && x.Resultado == true);
                        if (!CompEnviado)
                        {
                            var avisos = dbContext.AvisosVencimiento.Where(x => x.IDUsuario == usu.IDUsuario && x.TipoAlerta == "Envio CR").FirstOrDefault();
                            if (avisos != null)
                            {
                                var c = dbContext.Cobranzas.Where(x => x.IDCobranza == idCobranza && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                                if (c.Personas.Email != null && c.Personas.Email != "")
                                {
                                    var mensaje = avisos.Mensaje.ReplaceAll("\n", "<br/>");
                                    var mail = string.IsNullOrEmpty(c.Personas.EmailsEnvioFc) ? c.Personas.Email : c.Personas.EmailsEnvioFc;

                                    Common.EnviarComprobantePorMail(c.Personas.RazonSocial, mail, avisos.Asunto, mensaje, nombre, usu.Logo);
                                    ComprobantesCommon.GuardarComprobantesEnviados(dbContext, null, idCobranza, "Cobranza enviada correctamente", true, usu);
                                }
                                else
                                    throw new Exception("El cliente no tiene configurado el correo electrónico");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ComprobantesCommon.GuardarComprobantesEnviados(dbContext, null, idCobranza, "Cobranza: " + ex.Message, false, usu);
                    throw new Exception("No se pudo enviar automáticamente recibo de cobranza.");
                }
            }
        }*/

    }

    [WebMethod(true)]
    public static CobranzasEditViewModel obtenerDatos(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return CobranzasCommon.ObtenerCobranza(id, usu.IDUsuario);
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerComprobantesPendientes(int id, int idCobranza) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return CobranzasCommon.ObtenerComprobantesPendientes(id, idCobranza, usu.IDUsuario);
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerFormasDePagoCobranzas(int idPersona) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        return CobranzasCommon.ObtenerFormasDePagoCobranzas(idPersona, usu.IDUsuario);
    }


}
﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using ACHE.Extensions;
using ACHE.Model;
using System.Net.Mail;
using System.Data;
using System.IO;
using System.Web.Script.Services;
using ACHE.Negocio.Common;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Productos;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Ventas;

public partial class common : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerPuntosDeVenta()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var retorno = UsuarioCommon.ObtenerPuntosDeVenta(usu);
            return retorno;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerCondicionesDeVenta()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var retorno = UsuarioCommon.ObtenerCondicionesDeVenta(usu.IDUsuario);
            return retorno;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerPuntosDeVentaIntegracion()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var retorno = UsuarioCommon.ObtenerPuntosDeVenta(usu, false, true);
            return retorno;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerCategorias()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return UsuarioCommon.ObtenerCategorias(usu.IDUsuario);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string obtenerProxNroComprobante(string tipo, int idPuntoDeVenta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var nro = ComprobantesCommon.ObtenerProxNroComprobante(tipo, usu.IDUsuario, idPuntoDeVenta);
            return nro;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string obtenerProxNroCobranza(string tipo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var nro = ACHE.Negocio.Facturacion.CobranzasCommon.obtenerProxNroCobranza(tipo, usu.IDUsuario);
            //var nro = "";

            //using (var dbContext = new ACHEEntities())
            //{
            //    if (dbContext.Cobranzas.Any(x => x.IDUsuario == usu.IDUsuario && x.Tipo == tipo))
            //    {
            //        var aux = dbContext.Cobranzas.Where(x => x.IDUsuario == usu.IDUsuario && x.Tipo == tipo).Max(x => x.Numero);
            //        if (aux != null)
            //            nro = aux.ToString("#00000000");
            //        else
            //            nro = "00000001";
            //    }
            //    else
            //        nro = "00000001";
            //}

            return nro;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerConceptos(string tipo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return UsuarioCommon.ObtenerConceptos(usu.IDUsuario, tipo);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerConceptosCodigoyNombre(string tipo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var tipos = tipo.Split(',').ToList();
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Conceptos.Where(x => x.Estado == "A" && x.IDUsuario == usu.IDUsuario && (tipo == "" || tipos.Contains(x.Tipo))).ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDConcepto,
                        Nombre = x.Codigo + " - " + x.Nombre
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerConceptosCodigoyNombrePorInventario(int idInventario, int stockMin)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.StockConceptos.Include("Conceptos").Where(x => x.Inventarios.IDUsuario == usu.IDUsuario && x.IDInventario == idInventario && x.Stock >= stockMin && x.Conceptos.Tipo != "S").ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.Conceptos.IDConcepto,
                        Nombre = x.Conceptos.Codigo + " - " + x.Conceptos.Nombre
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerConceptosCodigoyNombreSinCombos(string tipo)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Conceptos.Where(x => x.Estado == "A" && x.IDUsuario == usu.IDUsuario && (tipo == "" || x.Tipo == tipo) && x.Tipo != "C").ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDConcepto,
                        Nombre = x.Codigo + " - " + x.Nombre
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerAnios(string tipo)
    {
        List<Combo2ViewModel> resultado = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                int anioInicio = usu.FechaAlta.Year;
                while (anioInicio <= DateTime.Now.Year)
                {
                    resultado.Add(new Combo2ViewModel()
                    {
                        ID = anioInicio,
                        Nombre = anioInicio.ToString()
                    }
                    );
                    anioInicio += 1;
                }

                return resultado;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerMeses(string tipo)
    {
        List<Combo2ViewModel> resultado = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;

            for (int i = 1; i <= 12; i++)
            {

                resultado.Add(new Combo2ViewModel()
                {
                    ID = i,
                    Nombre = dtinfo.GetMonthName(i)
                }
                );

            }

            return resultado;

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerPersonas()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario).OrderBy(x => x.RazonSocial)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDPersona,
                        Nombre = x.NroDocumento + " - " + (x.NombreFantansia == "" ? x.RazonSocial.ToUpper() : x.NombreFantansia.ToUpper())
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static Combo2ViewModel obtenerPersonaSelected(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {

                return dbContext.Personas.Where(x => x.IDPersona == id && x.IDUsuario == usu.IDUsuario)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDPersona,
                        Nombre = x.NroDocumento + " - " + (x.NombreFantansia == "" ? x.RazonSocial.ToUpper() : x.NombreFantansia.ToUpper())
                    }).FirstOrDefault(); ;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static ResultadosCombo3ViewModel obtenerPersonasFilter(string search, int page)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                ResultadosCombo3ViewModel data = new ResultadosCombo3ViewModel();

                if (!string.IsNullOrEmpty(search) && search.Length > 1)
                {
                    var aux = dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario)
                        .Where(x => x.NombreFantansia.Contains(search) || x.RazonSocial.Contains(search) || x.NroDocumento.Contains(search))
                        .OrderBy(x => x.RazonSocial)
                        .Select(x => new Combo3ViewModel()
                        {
                            id = x.IDPersona,
                            text = x.NroDocumento + " - " + (x.NombreFantansia == "" ? x.RazonSocial.ToUpper() : x.NombreFantansia.ToUpper())
                        }).AsQueryable();

                    data.TotalItems = aux.Count();
                    if (page > 1)
                        data.Items = aux.Skip(page * 30).Take(30).ToList();
                    else
                        data.Items = aux.Take(30).ToList();
                }

                return data;

            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerOrdenes(int idProveedor, bool conciliado)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.OrdenesCompras.Where(x => x.IDUsuario == usu.IDUsuario && x.IDProveedor == idProveedor && x.Estado != (conciliado == false ? "Conciliado" : "")).OrderBy(x => x.NroOrden).ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDOrdenCompra,
                        Nombre = x.NroOrden.ToString()
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerInventarios(bool soloActivos)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Inventarios.Where(x => x.IDUsuario == usu.IDUsuario).OrderBy(x => x.Nombre).AsEnumerable();

                if (soloActivos)
                    result = result.Where(x => x.Activo);

                var aux = result.Select(x => new Combo2ViewModel()
                {
                    ID = x.IDInventario,
                    Nombre = x.Nombre.ToUpper()
                }).OrderBy(x => x.Nombre).ToList();

                return aux;

            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerInventariosActivos()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var result = dbContext.Inventarios.Where(x => x.IDUsuario == usu.IDUsuario && x.Activo).OrderBy(x => x.FechaAlta).ToList();

                if (usu.IDPlan < 4)
                    result = result.Take(1).ToList();


                var aux = result.Select(x => new Combo2ViewModel()
                {
                    ID = x.IDInventario,
                    Nombre = x.Nombre.ToUpper()
                }).OrderBy(x => x.Nombre).ToList();

                return aux;
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void ayuda(string mensaje)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<MENSAJE>", mensaje);
            replacements.Add("<USUARIO>", usu.RazonSocial);
            replacements.Add("<ID>", usu.IDUsuario);
            replacements.Add("<EMAIL>", usu.Email);
            replacements.Add("<NOTIFICACION>", "¡Ya recibimos tu mensaje! Pronto vamos a llamarte o enviarte un mail.");

            bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Ayuda, replacements, ConfigurationManager.AppSettings["Email.Ayuda"], "Contabilium: Solicitud de ayuda");

            if (!send)
                throw new Exception("El mensaje no pudo ser enviado. Por favor, escribenos a <a href='mailto:soporte@contabilium.com'>soporte@contabilium.com</a>");

            send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.NotificacionDeContabilium, replacements, usu.Email, "Recibimos tu consulta! Pronto nos pondremos en contacto");
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void sugerencia(string mensaje)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<MENSAJE>", mensaje);
            replacements.Add("<USUARIO>", usu.RazonSocial);
            replacements.Add("<ID>", usu.IDUsuario);
            replacements.Add("<EMAIL>", usu.Email);
            replacements.Add("<NOTIFICACION>", "¡Ya recibimos tu sugerencia! Muchas gracias por ayudarnos a mejorar!");

            bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Ayuda, replacements, ConfigurationManager.AppSettings["Email.Ayuda"], "Contabilium: Nueva sugerencia");

            if (!send)
                throw new Exception("El mensaje no pudo ser enviado. Por favor, escribenos a <a href='mailto:atencioncliente@contabilium.com'>atencioncliente@contabilium.com</a>");

            send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.NotificacionDeContabilium, replacements, usu.Email, "Recibimos tu sugerencia!");
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void enviarComprobantePorMail(string nombre, string para, string asunto, string mensaje, string file, int year, int idComprobante)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            var emailUsuarioTo = !string.IsNullOrEmpty(usu.EmailAlerta) ? usu.EmailAlerta : usu.Email;
            if (!file.Contains("_RC-"))
                ComprobantesCommon.EnviarComprobantePorMail(nombre, para, asunto, mensaje, year, file, usu.Logo, usu.IDUsuario, emailUsuarioTo, usu.EmailDisplayFrom ?? usu.RazonSocial, idComprobante);
            else
                CobranzasCommon.EnviarComprobantePorMail(nombre, para, asunto, mensaje, year, file, usu.Logo, usu.IDUsuario, usu.EmailDisplayFrom ?? usu.RazonSocial);

            /*
             Common.EnviarComprobantePorMail(); 
             MailAddressCollection listTo = new MailAddressCollection();
            foreach (var mail in para.Split(','))
            {
                if (mail != string.Empty)
                    listTo.Add(new MailAddress(mail));
            }

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<NOTIFICACION>", mensaje);
            replacements.Add("<USUARIO>", nombre);
            replacements.Add("<LOGOEMPRESA>", "/files/usuarios/" + usu.Logo);

            List<string> attachments = new List<string>();
            attachments.Add(HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/comprobantes/" + DateTime.Now.Year.ToString() + "/" + file));

            bool send = EmailHelper.SendMessage(EmailTemplate.EnvioComprobanteConFoto, replacements, listTo, usu.Email, usu.Email, asunto, attachments);
            if (!send)
                throw new Exception("El mensaje no pudo ser enviado. Por favor, intente nuevamente. En caso de continuar el error, escribenos a <a href='" + ConfigurationManager.AppSettings["Email.Ayuda"] + "'>" + ConfigurationManager.AppSettings["Email.Ayuda"] + "</a>");*/
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void enviarDocumentoPorMail(string nombre, string para, string asunto, string mensaje, string file, int year, int idComprobante, string tipoComprobante)
    {
        //Este metodo es como el de arriba pero mas amplio porque permite encontrar archivos que estan en otro path distinto a comprobantes, como pagos
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            var emailUsuarioTo = !string.IsNullOrEmpty(usu.EmailAlerta) ? usu.EmailAlerta : usu.Email;
            ComprobantesCommon.EnviarComprobantePorMail(nombre, para, asunto, mensaje, year, file, usu.Logo, usu.IDUsuario, emailUsuarioTo, usu.EmailDisplayFrom ?? usu.RazonSocial, idComprobante, tipoComprobante);

        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void ingreso(string usuario, string pwd, bool recordarme)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var usu = dbContext.UsuariosView.Where(x => x.Email.ToLower() == usuario.ToLower() && x.Activo).FirstOrDefault();
                bool tieneMultiempresa = false;

                if (usu != null)
                {
                    if (usu.EstaBloqueado)
                    {
                        UsuarioCommon.GuardarLoginUsuarios(usuario, usu.IDUsuario, usu.IDUsuarioAdicional, "Password incorrecto, Usuario bloqueado");
                        throw new CustomException("Has superado el número máximo de intentos posibles. Por su seguridad, su cuenta ha sido bloqueada. <br/>Haga click <a href='#' onclick=\"Acceso.showForm('recupero');\"><b>aquí</b></a> para desbloquear su cuenta mediante la recuperación de su contraseña.");
                    }
                    else
                    {
                        if (usu.Pwd == pwd && usu.Tipo != "T")
                        {
                            DesBloquearUsuario(dbContext, usu);

                            if (usu.IDUsuarioAdicional != 0 && usu.IDUsuarioPadre == null)
                                tieneMultiempresa = dbContext.UsuariosEmpresa.Any(x => x.IDUsuarioAdicional == usu.IDUsuarioAdicional);
                            else
                                tieneMultiempresa = true;


                            var idPlan = PermisosModulos.ObtenerTodosLosFormularios(dbContext, usu.IDUsuario);

                            var tipo = PermisosModulos.ObtenerTipoSegunPLan(dbContext, usu.Tipo, idPlan);

                            var PlanVigente = PermisosModulos.PlanVigente(dbContext, usu.IDUsuario);
                            //}

                            PermisosModulos.obtenerRolesUsuarioAdicional(dbContext, usu.IDUsuarioAdicional, usu.Tipo);
                            PermisosModulos.obtenerPermisosUsuarioAdicional(dbContext, usu.IDUsuarioAdicional, usu.Tipo);
                            HttpContext.Current.Session["CurrentUser"] = new WebUser(
                                usu.IDUsuario, usu.IDUsuarioAdicional, tipo, usu.RazonSocial, usu.CUIT, usu.CondicionIva,
                                usu.Email, usu.EmailFrom, "", usu.Domicilio + " " + usu.PisoDepto, usu.Pais, usu.IDProvincia,
                                usu.IDCiudad, usu.Telefono, usu.TieneFacturaElectronica, usu.IIBB, usu.FechaInicioActividades,
                                usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupRealizado, tieneMultiempresa, !usu.UsaProd,
                                idPlan, usu.EmailAlertas, usu.Provincia, usu.Ciudad, usu.EsAgentePercepcionIVA, usu.EsAgentePercepcionIIBB,
                                usu.EsAgenteRetencion, PlanVigente, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA,
                                usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
                                usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenesPago, usu.MostrarBonificacionFc, usu.MostrarProductosEnRemito, usu.PorcentajeDescuento);

                            var usuOrig = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                            if (usuOrig.Activo)
                            {
                                //saco promo de RT a los 3 meses, desde el 09.2017
                                if (usuOrig.CodigoPromo == "REALTRENDS" && usuOrig.PorcentajeDescuento > 0 && usuOrig.FechaAlta >= DateTime.Parse("2017-09-01"))
                                {
                                    var cantPagos = dbContext.PlanesPagos.Where(x => x.IDUsuario == usuOrig.IDUsuario && x.IDPlan != 6 && !x.PagoAnual).Count();
                                    if (cantPagos >= 3)
                                    {
                                        usuOrig.PorcentajeDescuento = 0;
                                        dbContext.SaveChanges();
                                    }
                                    //if (planes != null)
                                    //{
                                    //    if (planes.FechaFinPlan >= DateTime.Parse("2017-09-01").AddMonths(3))
                                    //    {
                                    //        usuOrig.PorcentajeDescuento = 0;
                                    //        dbContext.SaveChanges();
                                    //    }
                                    //}
                                }

                                UsuarioCommon.GuardarLoginUsuarios(usuario, usu.IDUsuario, usu.IDUsuarioAdicional, "Usuario entra al sistema Exitosamente");
                                HttpCookie cookieRememberMe = new HttpCookie("ContabiliumRecordarme");
                                cookieRememberMe.Value = (recordarme ? "T" : "F");
                                cookieRememberMe.Expires = DateTime.Now.AddDays(14);
                                HttpContext.Current.Response.Cookies.Add(cookieRememberMe);

                                if (string.IsNullOrEmpty(usuOrig.ApiKey))
                                    usuOrig.ApiKey = Guid.NewGuid().ToString().Replace("-", "");

                                usuOrig.FechaUltLogin = DateTime.Now;
                                dbContext.SaveChanges();
                                if (recordarme)
                                {
                                    HttpCookie cookieUserName = new HttpCookie("ContabiliumUsuario");
                                    cookieUserName.Value = HttpContext.Current.Server.UrlEncode(usuario);
                                    cookieUserName.Expires = DateTime.Now.AddDays(14);
                                    HttpContext.Current.Response.Cookies.Add(cookieUserName);

                                    HttpCookie cookiePwd = new HttpCookie("ContabiliumPwd");
                                    var passEncriptado = Encriptar.EncriptarCadena(pwd); //PWD ENCRIPTADO
                                    cookiePwd.Value = HttpContext.Current.Server.UrlEncode(passEncriptado);
                                    cookiePwd.Expires = DateTime.Now.AddDays(14);
                                    HttpContext.Current.Response.Cookies.Add(cookiePwd);
                                }
                                else
                                {
                                    HttpContext.Current.Response.Cookies.Remove("ContabiliumUsuario");
                                    HttpContext.Current.Response.Cookies.Remove("ContabiliumPwd");
                                }
                            }
                            else
                            {
                                //UsuarioCommon.GuardarLoginUsuarios(usuario, usu.IDUsuario, usu.IDUsuarioAdicional, "El usuario intento entrar al sistema pero esta dado de baja");
                                throw new CustomException("Usuario y/o contraseña incorrecta.");
                            }
                        }
                        else
                        {
                            BloquearUsuario(dbContext, usu);
                            throw new CustomException("Usuario y/o contraseña incorrecta.");
                        }
                    }
                }
                else
                {
                    UsuarioCommon.GuardarLoginUsuarios(usuario, null, null, "No se encontro un usuario con el email ingresado");
                    throw new CustomException("Usuario y/o contraseña incorrecta.");
                }
            }
        }
        catch (CustomException ex)
        {
            throw new Exception(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private static void BloquearUsuario(ACHEEntities dbContext, UsuariosView usuV)
    {
        var estaBloqueado = false;
        var correo = string.Empty;
        var CantIntentos = 0;
        if (usuV.IDUsuarioAdicional > 0)
        {
            var usuad = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == usuV.IDUsuarioAdicional).FirstOrDefault();
            usuad.CantIntentos++;
            CantIntentos = usuad.CantIntentos;
            if (usuad.CantIntentos >= 3)
            {
                usuad.EstaBloqueado = true;
                estaBloqueado = true;
                correo = usuad.Email;
            }
        }
        else
        {
            var usu = dbContext.Usuarios.Where(x => x.IDUsuario == usuV.IDUsuario).FirstOrDefault();
            usu.CantIntentos++;
            CantIntentos = usu.CantIntentos;
            if (usu.CantIntentos >= 3)
            {
                usu.EstaBloqueado = true;
                estaBloqueado = true;
                correo = usu.Email;
            }
        }

        dbContext.SaveChanges();

        if (estaBloqueado)
        {
            UsuarioCommon.GuardarLoginUsuarios(usuV.Email, usuV.IDUsuario, usuV.IDUsuarioAdicional, "Password incorrecto, Se bloquea el usuario");
            /*ListDictionary replacements = new ListDictionary();
            replacements.Add("<USUARIO>", "Administrador");
            replacements.Add("<NOTIFICACION>", "Se ha detectado que el usuario " + correo + " ha superado la cantidad maximo de login permitida. Su cuenta ha sido bloqueado");

            EmailHelper.SendMessage(EmailTemplate.Notificacion, replacements, ConfigurationManager.AppSettings["Email.Ayuda"], "Usuario " + correo + " bloqueado");*/
            throw new CustomException("Has superado el número máximo de intentos posibles. Por su seguridad, su cuenta ha sido bloqueada. <br/>Haga click <a href='#' onclick=\"Acceso.showForm('recupero');\"><b>aquí</b></a> para desbloquear su cuenta mediante la recuperación de su contraseña.");
        }
        else
            UsuarioCommon.GuardarLoginUsuarios(usuV.Email, usuV.IDUsuario, usuV.IDUsuarioAdicional, "Password incorrecto, intento nro: " + CantIntentos);
    }

    private static void DesBloquearUsuario(ACHEEntities dbContext, UsuariosView usuV)
    {
        if (usuV.IDUsuarioAdicional > 0)
        {
            var usuad = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == usuV.IDUsuarioAdicional).FirstOrDefault();
            usuad.CantIntentos = 0;
            usuad.EstaBloqueado = false;
        }
        else
        {
            var usu = dbContext.Usuarios.Where(x => x.IDUsuario == usuV.IDUsuario).FirstOrDefault();
            usu.CantIntentos = 0;
            usu.EstaBloqueado = false;
        }

        dbContext.SaveChanges();
    }

    [WebMethod(true)]
    public static void cambiarEmpresa(int idEmpresa)
    {
        UsuarioCommon.cambiarEmpresa(idEmpresa);
    }

    [WebMethod(true)]
    public static int CrearPersona(string razonSocial, string condicionIva, string personeria, string nombreFantasia, string tipoDocumento,
        string nroDocumento, int idPais, int idProvincia, int idCiudad, string domicilio, string pisoDepto, string codigoPostal, string tipo, string codigo, string email, string idUsuarioAdicional)
    {
        int id = 0;

        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            id = PersonasCommon.GuardarPersonas(id, razonSocial, nombreFantasia, condicionIva, personeria, tipoDocumento, nroDocumento, "",
                email, email, tipo, idPais, idProvincia, idCiudad, domicilio, pisoDepto, codigoPostal, "", 0, codigo, 0, usu.IDUsuario, idUsuarioAdicional);

            /*using (var dbContext = new ACHEEntities())
            {

                if (tipoDocumento == "CUIT" && dbContext.Personas.Any(x => x.IDUsuario == usu.IDUsuario && x.NroDocumento == nroDocumento && x.IDPersona != id))
                    throw new Exception("El CUIT ingresado ya se encuentra registrado.");
                else if (tipoDocumento != "CUIT" && condicionIva == "CF" && dbContext.Personas.Any(x => x.IDUsuario == usu.IDUsuario && x.CondicionIva == "CF" && x.RazonSocial.ToLower() == razonSocial.ToLower() && x.IDPersona != id))
                    throw new Exception("El cliente ya se encuentra registrado.");
                else if (dbContext.Personas.Any(x => x.Codigo.ToUpper() == codigo.ToUpper() && x.Codigo != "" && x.IDPersona != id && x.IDUsuario == usu.IDUsuario))
                    throw new Exception("El Código ingresado ya se encuentra registrado.");
                else
                {
                    Personas entity = new Personas();
                    entity.Tipo = tipo;
                    entity.RazonSocial = razonSocial.ToUpper();
                    entity.CondicionIva = condicionIva;
                    entity.Personeria = personeria;
                    entity.NombreFantansia = nombreFantasia.ToUpper();
                    entity.TipoDocumento = tipoDocumento;
                    entity.NroDocumento = nroDocumento;
                    entity.IDPais = (idPais == 0) ? 2 : idPais;
                    entity.IDProvincia = (idProvincia == 0) ? 2 : idProvincia;
                    entity.IDCiudad = (idCiudad == 0) ? 5003 : idCiudad;
                    entity.Domicilio = domicilio.ToUpper();
                    entity.PisoDepto = pisoDepto;
                    entity.CodigoPostal = codigoPostal;
                    entity.AlicuotaIvaDefecto = "";
                    entity.TipoComprobanteDefecto = "";
                    entity.FechaAlta = DateTime.Now;
                    entity.IDUsuario = usu.IDUsuario;
                    entity.Email = "";
                    entity.Web = "";
                    entity.Telefono = "";
                    entity.Observaciones = "";
                    entity.Celular = "";
                    entity.EmailsEnvioFc = "";
                    entity.CBU = "";
                    entity.Banco = "";
                    entity.SaldoInicial = 0;
                    entity.Codigo = codigo;
                    entity.Email = email;
                    try
                    {
                        dbContext.Personas.Add(entity);
                        dbContext.SaveChanges();
                        id = entity.IDPersona;
                    }
                    catch (Exception)
                    {
                        throw new Exception("Hubo un error, por favor intente nuevamente");
                    }
                }
            }*/
            return id;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static bool verificarWizard()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            return usu.SetupFinalizado;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    
    [WebMethod(true)]
    public static string TieneCobranzaAutomatica(int idCondicionDeVenta)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var cond = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, idCondicionDeVenta);
                return cond.CobranzaAutomatica ? "1" : "0";
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerCheques(bool EsPropio)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.RptChequesAcciones.Where(x => x.Accion == "" && x.Estado == "Libre" && x.IDUsuario == usu.IDUsuario && x.esPropio == EsPropio).OrderBy(x => x.FechaEmision).ToList().Select(x => new Combo2ViewModel()
                {
                    ID = x.IDCheque,
                    Nombre = x.Banco + " - Nro:" + x.Numero + "  $" + x.Importe.ToString()
                }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerChequesCobranzas(bool EsPropio)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.RptChequesAcciones.Where(x => x.Accion == "" && x.Estado == "Libre" && x.IDUsuario == usu.IDUsuario
                    && !dbContext.CobranzasFormasDePago.Any(y => y.IDCheque.HasValue && y.IDCheque.Value == x.IDCheque)
                    && x.esPropio == EsPropio).OrderBy(x => x.FechaEmision).ToList();

                return aux.Select(x => new Combo2ViewModel()
                {
                    ID = x.IDCheque,
                    Nombre = x.Banco + " - Nro:" + x.Numero + "  $" + x.Importe.ToString()
                }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerSubRubros(int idPadre)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Rubros.Where(x => x.IDUsuario == usu.IDUsuario && x.IDRubroPadre == idPadre).OrderBy(x => x.Nombre)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDRubro,
                        Nombre = x.Nombre
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerBancos()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Bancos.Include("BancosBase").Where(x => x.IDUsuario == usu.IDUsuario).OrderBy(x => x.BancosBase.Nombre)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDBanco,
                        Nombre = x.BancosBase.Nombre + " - Nro Cuenta:" + x.NroCuenta
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerCajas()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Cajas.Where(x => x.IDUsuario == usu.IDUsuario).OrderBy(x => x.Nombre)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDCaja,
                        Nombre = x.Nombre
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerRubros()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Rubros.Where(x => x.IDUsuario == usu.IDUsuario && !x.IDRubroPadre.HasValue).OrderBy(x => x.Nombre)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDRubro,
                        Nombre = x.Nombre
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerTodosBancos()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.BancosBase.OrderBy(x => x.Nombre).Select(x => new Combo2ViewModel()
                {
                    ID = x.IDBancoBase,
                    Nombre = x.Nombre
                }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string obtenerUltimoNroRecibo(string tipoComprobante, int idPunto)
    {
        var nroRecibo = 0;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var listaNumeroRecibo = dbContext.Cobranzas.Where(x => x.IDUsuario == usu.IDUsuario && x.Tipo == tipoComprobante && x.IDPuntoVenta == idPunto).ToList();
                nroRecibo = (listaNumeroRecibo.Count() == 0) ? 0 : listaNumeroRecibo.GroupBy(x => x.Tipo).ToList().Max(x => x.Max(y => y.Numero));

                if (usu.IDUsuario == 2157 && nroRecibo == 0)//TODO: que sea configurable por usuario
                    nroRecibo = 999;

                nroRecibo++;
            }
        }
        return nroRecibo.ToString("#00000000");
    }

    [WebMethod(true)]
    public static string exportarCliente(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "DetalleCuentaCorriente_" + usu.IDUsuario;
            string path = "~/tmp/";
            try
            {
                ResultadosRptCcDetalleViewModel resultados = new ResultadosRptCcDetalleViewModel();
                resultados.Items = new List<RptCcDetalleViewModel>();
                DataTable dt = new DataTable();
                if (idPersona > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        verComoCliente(idPersona, resultados, dbContext, true);
                        if (resultados.Items.Any())
                            resultados.Items.RemoveAt(resultados.Items.Count() - 1);
                        dt = resultados.Items.Select(x => new
                        {
                            Comprobante = x.Comprobante ?? "",
                            Observaciones = x.Observaciones ?? "",
                            Fecha = x.Fecha ?? "",
                            ComprobanteAplicado = x.ComprobanteAplicado ?? "",
                            FechaCobro = x.FechaCobro ?? "",
                            Importe = string.IsNullOrEmpty(x.Importe) ? 0 : decimal.Parse(x.Importe.ReplaceAll("- -", "-").ReplaceAll("- ", "-")),
                            Cobrado = string.IsNullOrEmpty(x.Cobrado) ? 0 : decimal.Parse(x.Cobrado.ReplaceAll("- -", "-").ReplaceAll("- ", "-")),
                            Total = string.IsNullOrEmpty(x.Total) ? 0 : decimal.Parse(x.Total)
                        }).ToList().ToDataTable();
                    }
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string exportarProveedor(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "DetalleCuentaCorriente_" + usu.IDUsuario;
            string path = "~/tmp/";
            try
            {
                ResultadosRptCcDetalleViewModel resultados = new ResultadosRptCcDetalleViewModel();
                resultados.Items = new List<RptCcDetalleViewModel>();
                DataTable dt = new DataTable();
                if (idPersona > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        verComoProveedor(idPersona, resultados, dbContext, true);
                        if (resultados.Items.Any())
                            resultados.Items.RemoveAt(resultados.Items.Count() - 1);
                        dt = resultados.Items.Select(x => new
                        {
                            Comprobante = x.Comprobante ?? "",
                            Observaciones = x.Observaciones ?? "",
                            Fecha = x.Fecha ?? "",
                            ComprobanteAplicado = x.ComprobanteAplicado ?? "",
                            FechaCobro = x.FechaCobro ?? "",
                            Importe = string.IsNullOrEmpty(x.Importe) ? 0 : decimal.Parse(x.Importe.ReplaceAll("- -", "-").ReplaceAll("- ", "-")),
                            Cobrado = string.IsNullOrEmpty(x.Cobrado) ? 0 : decimal.Parse(x.Cobrado.ReplaceAll("- -", "-").ReplaceAll("- ", "-")),
                            Total = string.IsNullOrEmpty(x.Total) ? 0 : decimal.Parse(x.Total)
                        }).ToList().ToDataTable();
                    }
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string exportarProveedorPDF(int idPersona, string total)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "DetalleCuentaCorriente_" + usu.IDUsuario;
            string path = "~/tmp/";
            try
            {
                ResultadosRptCcDetalleViewModel resultados = new ResultadosRptCcDetalleViewModel();
                resultados.Items = new List<RptCcDetalleViewModel>();
                DataTable dt = new DataTable();
                if (idPersona > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        verComoProveedor(idPersona, resultados, dbContext, true);
                        if (resultados.Items.Any())
                            resultados.Items.RemoveAt(resultados.Items.Count() - 1);
                        dt = resultados.Items.Select(x => new
                        {
                            Comprobante = x.Comprobante ?? "",
                            Observaciones = x.Observaciones ?? "",
                            Fecha = x.Fecha ?? "",
                            ComprobanteAplicado = x.ComprobanteAplicado ?? "",
                            FechaCobro = x.FechaCobro ?? "",
                            Importe = string.IsNullOrEmpty(x.Importe) ? 0 : decimal.Parse(x.Importe.ReplaceAll("- -", "-").ReplaceAll("- ", "-")),
                            Cobrado = string.IsNullOrEmpty(x.Cobrado) ? 0 : decimal.Parse(x.Cobrado.ReplaceAll("- -", "-").ReplaceAll("- ", "-")),
                            Total = string.IsNullOrEmpty(x.Total) ? 0 : decimal.Parse(x.Total)
                        }).ToList().ToDataTable();
                    }
                }
                HttpResponse Response = HttpContext.Current.Response;

                if (dt.Rows.Count > 0)
                {
                    ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
                    string LOGO_PATH = "/files/usuarios/";
                    if (!string.IsNullOrEmpty(usu.Logo))
                        contenido.Logo = LOGO_PATH + usu.Logo;

                    var persona = PersonasCommon.ObtenerPersonaPorID(idPersona, usu.IDUsuario);
                    List<DetalleCabecera> cabecera = new List<DetalleCabecera>();
                    DetalleCabecera c = new DetalleCabecera();
                    c.Titulo = "Razón Social";
                    c.Valor = persona.RazonSocial + " - " + persona.TipoDocumento + ": " + persona.NroDocumento;
                    cabecera.Add(c);
                    c = new DetalleCabecera();
                    c.Titulo = "Domicilio";
                    c.Valor = persona.Domicilio + ", " + persona.Ciudades.Nombre + ". Teléfono: " + persona.Telefono;
                    cabecera.Add(c);
                    c = new DetalleCabecera();
                    c.Titulo = "Fecha impresión";
                    c.Valor = DateTime.Now.ToString("dd/MM/yyyy");
                    cabecera.Add(c);
                    contenido.Cabecera = cabecera;


                    List<Total> totales = new List<Total>();
                    Total tot = new Total();
                    tot.Importe = total;
                    tot.Nombre = "Saldo";
                    totales.Add(tot);
                    contenido.Totales = totales;
                    contenido.TituloReporte = "Reporte Cuenta Corriente";


                    CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, true, false);

                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string exportarClientePDF(int idPersona, string total)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            string fileName = "DetalleCuentaCorriente_" + usu.IDUsuario;
            string path = "~/tmp/";
            try
            {
                //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                ResultadosRptCcDetalleViewModel resultados = new ResultadosRptCcDetalleViewModel();
                resultados.Items = new List<RptCcDetalleViewModel>();
                DataTable dt = new DataTable();
                if (idPersona > 0)
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        verComoCliente(idPersona, resultados, dbContext, true);
                        if (resultados.Items.Any())
                            resultados.Items.RemoveAt(resultados.Items.Count() - 1);
                        dt = resultados.Items.Select(x => new
                        {
                            Comprobante = x.Comprobante ?? "",
                            Observaciones = x.Observaciones ?? "",
                            Fecha = x.Fecha ?? "",
                            ComprobanteAplicado = x.ComprobanteAplicado ?? "",
                            FechaCobro = x.FechaCobro ?? "",
                            Importe = string.IsNullOrEmpty(x.Importe) ? "0" : decimal.Parse(x.Importe.ReplaceAll("- -", "-").ReplaceAll("- ", "-")).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()),
                            Cobrado = string.IsNullOrEmpty(x.Cobrado) ? "0" : decimal.Parse(x.Cobrado.ReplaceAll("- -", "-").ReplaceAll("- ", "-")).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()),
                            Total = string.IsNullOrEmpty(x.Total) ? "0" : decimal.Parse(x.Total).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales())
                        }).ToList().ToDataTable();
                    }
                }
                HttpResponse Response = HttpContext.Current.Response;

                if (dt.Rows.Count > 0)
                {
                    ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
                    string LOGO_PATH = "/files/usuarios/";
                    if (!string.IsNullOrEmpty(usu.Logo))
                        contenido.Logo = LOGO_PATH + usu.Logo;

                    var persona = PersonasCommon.ObtenerPersonaPorID(idPersona, usu.IDUsuario);

                    List<DetalleCabecera> cabecera = new List<DetalleCabecera>();
                    DetalleCabecera c = new DetalleCabecera();
                    c.Titulo = "Razón Social";
                    c.Valor = persona.RazonSocial + " - " + persona.TipoDocumento + ": " + persona.NroDocumento;
                    cabecera.Add(c);
                    c = new DetalleCabecera();
                    c.Titulo = "Domicilio";
                    c.Valor = persona.Domicilio + ", " + persona.Ciudades.Nombre + ". Teléfono: " + persona.Telefono;
                    cabecera.Add(c);
                    c = new DetalleCabecera();
                    c.Titulo = "Fecha impresión";
                    c.Valor = DateTime.Now.ToString("dd/MM/yyyy");
                    cabecera.Add(c);
                    contenido.Cabecera = cabecera;


                    List<Total> totales = new List<Total>();
                    Total tot = new Total();
                    tot.Importe = total;
                    tot.Nombre = "Saldo";
                    totales.Add(tot);
                    contenido.Totales = totales;
                    contenido.TituloReporte = "Reporte Cuenta Corriente";
                    CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, true, false);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");

            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
        throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosRptCcDetalleViewModel getResultsCtaCte(int idPersona, int verComo)
    {
        try
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                ResultadosRptCcDetalleViewModel resultado = new ResultadosRptCcDetalleViewModel();
                resultado.TotalPage = 1;// ((results.Count() - 1) / pageSize) + 1;
                resultado.Items = new List<RptCcDetalleViewModel>();

                using (var dbContext = new ACHEEntities())
                {
                    if (verComo == 1)
                        verComoCliente(idPersona, resultado, dbContext, false);
                    else
                        verComoProveedor(idPersona, resultado, dbContext, false);
                }
                return resultado;
            }
            throw new Exception("Por favor, vuelva a iniciar sesión");
        }
        catch (Exception e)
        {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    private static RptCcDetalleViewModel AgregarSaldoInicialCliente(ACHEEntities dbContext, decimal? saldo)
    {
        var detalleCc = new RptCcDetalleViewModel();
        detalleCc.Comprobante = "Saldo inicial";
        detalleCc.Fecha = "";
        detalleCc.Importe = Convert.ToDecimal(saldo).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
        detalleCc.Total = Convert.ToDecimal(saldo).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

        return detalleCc;
    }

    private static void verComoProveedor(int idPersona, ResultadosRptCcDetalleViewModel resultado, ACHEEntities dbContext, bool verTodo)
    {
        decimal total = 0;
        //decimal totalAnterior = 0;// MONTO SALDO ANTERIOR
        RptCcDetalleViewModel detalleCc;
        RptCcDetalleViewModel detalleCob;
        List<CtaCteProveedorDto> lista;//LISTA QUE SE VA A MOSTRAR EN EL MODAL
        var listaCount = 0; // SET CANTIDAD DE ITEMS
        var mostrarCantidad = 30;

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        var list = dbContext.Compras.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).Select(d => new CtaCteProveedorDto { Fecha = d.Fecha, IDCompra = d.IDCompra, Iva = d.Iva, NroFactura = d.NroFactura, Tipo = d.Tipo, TotalImpuestos = d.TotalImpuestos.Value, Total = d.Total.Value, Saldo = d.Saldo, Observaciones = d.Observaciones }).OrderBy(x => x.Fecha).ToList();
        list.AddRange(dbContext.PagosDetalle.Include("Pagos").Where(x => x.IDCompra == null && x.Pagos.IDPersona == idPersona && x.Pagos.IDUsuario == usu.IDUsuario).Select(d => new CtaCteProveedorDto { Fecha = d.Pagos.FechaPago, NroPago = d.Pagos.NroPago, Tipo = "PC", Importe = d.Importe, Observaciones = d.Pagos.Observaciones }).OrderBy(x => x.Fecha).ToList());
        listaCount = list.Count();

        list = list.OrderBy(d => d.Fecha).ToList();

        var persona = dbContext.Personas.Where(x => x.IDPersona == idPersona).FirstOrDefault();
        resultado.TotalItems = list.Count();

        resultado.DatosAdicionalesLinea1 = persona.TipoDocumento + ": " + persona.NroDocumento;
        resultado.DatosAdicionalesLinea2 = "Domicilio: " + persona.Domicilio + ", " + persona.Ciudades.Nombre + ". Teléfono: " + persona.Telefono;

        if (persona.SaldoInicial != null && persona.SaldoInicial != 0)
        {
            resultado.Items.Add(AgregarSaldoInicialCliente(dbContext, persona.SaldoInicial));
            total = Convert.ToDecimal(persona.SaldoInicial);
            resultado.TotalItems++;
        }

        if (list.Any())
        {

            if (!verTodo && listaCount > mostrarCantidad)
            {

                //totalAnterior = total;
                detalleCc = new RptCcDetalleViewModel();
                detalleCc.Cobrado = "SaldoAnterior";
                var listaAnterior = list.Take(listaCount - mostrarCantidad).ToList();

                //total += listaAnterior.Where(x => x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "CAC" && x.Tipo != "PC").Sum(x => x.Saldo);
                //total -= listaAnterior.Where(x => x.Tipo == "CAC").Sum(x => x.Importe);
                //total -= listaAnterior.Where(x => (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCe") && x.Saldo > 0).Sum(x => x.Saldo);


                //totalAnterior += Convert.ToDecimal(
                //listaAnterior.Where(x => x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE" && x.Tipo != "PC").Sum(x => x.TotalImpuestos) +
                //listaAnterior.Where(x => x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE" && x.Tipo != "PC").Sum(x => x.Total) +
                //listaAnterior.Where(x => x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE" && x.Tipo != "PC").Sum(x => x.Iva));
                //totalAnterior -= listaAnterior.Where(x => x.Tipo == "PC").Sum(x => x.Importe);
                //totalAnterior -= listaAnterior.Where(x => x.Saldo > 0).Sum(x => x.Saldo);

                //TODO: MEJORAR PERFORMANCE
                foreach (var detalle in listaAnterior)
                {
                    if (detalle.Tipo != "NCA" && detalle.Tipo != "NCB" && detalle.Tipo != "NCC" && detalle.Tipo != "NCM" && detalle.Tipo != "NCE" && detalle.Tipo != "PC")
                    {

                        var imp = detalle.TotalImpuestos;
                        total += Convert.ToDecimal(detalle.Total + detalle.Iva + imp);

                        var pagosList = dbContext.PagosDetalle.Include("Pagos").Where(x => x.IDCompra == detalle.IDCompra && x.Pagos.IDPersona == idPersona).ToList();
                        foreach (var item in pagosList)
                        {
                            total -= item.Importe;
                        }
                    }
                    else if (detalle.Tipo == "PC")//Pagos a Cuenta
                    {
                        total -= detalle.Importe;

                    }
                    else if (detalle.Saldo > 0)
                    {
                        total -= detalle.Saldo;
                    }
                }

                detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                resultado.Items.Add(detalleCc);

                lista = list.Skip(listaCount - mostrarCantidad).ToList();


            }
            else
                lista = list;

            foreach (var detalle in lista)
            {
                if (detalle.Tipo != "NCA" && detalle.Tipo != "NCB" && detalle.Tipo != "NCC" && detalle.Tipo != "NCM" && detalle.Tipo != "NCE" && detalle.Tipo != "PC")
                {

                    var imp = detalle.TotalImpuestos;
                    total += Convert.ToDecimal(detalle.Total + detalle.Iva + imp);

                    detalleCc = new RptCcDetalleViewModel();
                    detalleCc.Comprobante = detalle.Tipo + " " + detalle.NroFactura;
                    detalleCc.Fecha = detalle.Fecha.ToString("dd/MM/yyyy");
                    detalleCc.Importe = Convert.ToDecimal(detalle.Total + detalle.Iva + imp).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Observaciones = detalle.Observaciones;
                    resultado.Items.Add(detalleCc);

                    var pagosList = dbContext.PagosDetalle.Include("Pagos").Where(x => x.IDCompra == detalle.IDCompra && x.Pagos.IDPersona == idPersona).ToList();
                    foreach (var item in pagosList)
                    {
                        total -= item.Importe;

                        detalleCob = new RptCcDetalleViewModel();
                        detalleCob.ComprobanteAplicado = "0001-" + item.Pagos.NroPago.ToString("#00000000");

                        detalleCob.Comprobante = "";
                        detalleCob.FechaCobro = item.Pagos.FechaPago.ToString("dd/MM/yyyy");
                        detalleCob.Observaciones = item.Pagos.Observaciones;
                        /*if (item.Pagos.PagosRetenciones.Any())
                        {
                            var ret = item.Pagos.PagosRetenciones.Sum(x => x.Importe);
                            total -= ret;
                            detalleCob.Cobrado = "- " + (item.Importe + ret).ToMoneyFormat();
                        }
                        else*/
                        detalleCob.Cobrado = "- " + item.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

                        detalleCob.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                        resultado.Items.Add(detalleCob);
                    }
                }
                else if (detalle.Tipo == "PC")//Pagos a Cuenta
                {
                    total -= detalle.Importe;

                    detalleCc = new RptCcDetalleViewModel();
                    detalleCc.Comprobante = "Pago a cuenta";
                    detalleCc.Fecha = detalle.Fecha.ToString("dd/MM/yyyy");
                    detalleCc.Cobrado = "- " + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Observaciones = detalle.Observaciones;
                    resultado.Items.Add(detalleCc);
                }
                else if (detalle.Saldo > 0)
                {
                    total -= detalle.Saldo;
                    var imp = detalle.TotalImpuestos;
                    //total -= Convert.ToDecimal(detalle.Total + detalle.Iva + imp);

                    detalleCc = new RptCcDetalleViewModel();
                    detalleCc.Comprobante = "Saldo " + detalle.Tipo + " " + detalle.NroFactura;
                    detalleCc.Fecha = detalle.Fecha.ToString("dd/MM/yyyy");
                    //detalleCc.Importe = "- " + Convert.ToDecimal(detalle.Total + detalle.Iva + imp).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Importe = "- " + Convert.ToDecimal(detalle.Saldo).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Observaciones = detalle.Observaciones;
                    resultado.Items.Add(detalleCc);
                }
            }

            detalleCc = new RptCcDetalleViewModel();
            detalleCc.Cobrado = "Saldo";

            detalleCc.Total = (total).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
            resultado.Items.Add(detalleCc);

        }

    }

    private static void verComoCliente(int idPersona, ResultadosRptCcDetalleViewModel resultado, ACHEEntities dbContext, bool verTodo)
    {
        decimal total = 0;
        //decimal totalAnterior = 0;// MONTO SALDO ANTERIOR
        RptCcDetalleViewModel detalleCc;
        RptCcDetalleViewModel detalleCob;
        List<CtaCteClienteDto> lista;//LISTA QUE SE VA A MOSTRAR EN EL MODAL
        var listaCount = 0; // SET CANTIDAD DE ITEMS
        var mostrarCantidad = 30;


        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];


        var list = dbContext.Comprobantes.Include("Personas").Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario)
            .Select(d => new CtaCteClienteDto
            {
                Fecha = d.FechaComprobante,
                IDComprobante = d.IDComprobante,
                ImporteTotalNeto = d.ImporteTotalNeto,
                Numero = d.Numero,
                PuntoVenta = d.PuntosDeVenta.Punto,
                Saldo = d.Saldo,
                Observaciones = d.Observaciones,
                Tipo = d.Tipo
            }).OrderBy(x => x.Fecha).ToList();

        list.AddRange(dbContext.CobranzasDetalle.Include("Cobranzas").Where(x => x.IDComprobante == null && x.Cobranzas.IDPersona == idPersona && x.Cobranzas.IDUsuario == usu.IDUsuario).Select(d => new CtaCteClienteDto { Fecha = d.Cobranzas.FechaCobranza, Tipo = "CAC", Importe = d.Importe, Observaciones = d.Cobranzas.Observaciones }).OrderBy(x => x.Fecha).ToList());
        listaCount = list.Count();
        list = list.OrderBy(d => d.Fecha).ToList();

        var persona = dbContext.Personas.Where(x => x.IDPersona == idPersona).FirstOrDefault();
        resultado.TotalItems = listaCount;
        resultado.DatosAdicionalesLinea1 = persona.TipoDocumento + ": " + persona.NroDocumento;
        resultado.DatosAdicionalesLinea2 = "Domicilio: " + persona.Domicilio + ", " + persona.Ciudades.Nombre + ". Teléfono: " + persona.Telefono;


        if (persona.SaldoInicial != null && persona.SaldoInicial != 0)
        {
            resultado.Items.Add(AgregarSaldoInicialCliente(dbContext, persona.SaldoInicial));
            total = Convert.ToDecimal(persona.SaldoInicial);
            resultado.TotalItems++;
        }

        if (list.Any())
        {

            if (!verTodo && listaCount > mostrarCantidad)
            {
                //totalAnterior = total;
                detalleCc = new RptCcDetalleViewModel();
                detalleCc.Cobrado = "SaldoAnterior";
                var listaAnterior = list.Take(listaCount - mostrarCantidad).ToList();
                total += listaAnterior.Where(x => x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "CAC" && x.Tipo != "NCE").Sum(x => x.Saldo);
                total -= listaAnterior.Where(x => x.Tipo == "CAC").Sum(x => x.Importe);
                total -= listaAnterior.Where(x => (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCe") && x.Saldo > 0).Sum(x => x.Saldo);


                //TODO: MEJORAR PERFORMANCE.
                /*foreach (var detalle in listaAnterior)
                {
                    if (detalle.Tipo != "NCA" && detalle.Tipo != "NCB" && detalle.Tipo != "NCC" && detalle.Tipo != "NCM" && detalle.Tipo != "CAC" && detalle.Tipo != "NCE")
                    {
                        total += detalle.ImporteTotalNeto;
                    }
                    else if (detalle.Tipo == "CAC")//Cobranza a cuenta
                    {
                        total -= detalle.Importe;
                    }
                    else if (detalle.Saldo > 0)
                    {
                        total -= detalle.Saldo;
                    }

                    var cobrazasList = dbContext.CobranzasDetalle.Include("Cobranzas").Where(x => x.IDComprobante == detalle.IDComprobante && x.Cobranzas.IDPersona == idPersona).ToList();
                    foreach (var cobranza in cobrazasList)
                    {
                        total -= cobranza.Importe;
                    }
                }*/
                //foreach (var listaAnteriorDetalle in listaAnterior) {
                //    total -= dbContext.CobranzasDetalle.Include("Cobranzas").Where(x => x.IDComprobante == listaAnteriorDetalle.IDComprobante 
                //        && x.Cobranzas.IDPersona == idPersona).ToList().Sum(x => x.Importe);
                //}
                detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                resultado.Items.Add(detalleCc);

                lista = list.Skip(listaCount - mostrarCantidad).ToList();
            }
            else
                lista = list;

            foreach (var detalle in lista)
            {
                if (detalle.Tipo != "NCA" && detalle.Tipo != "NCB" && detalle.Tipo != "NCC" && detalle.Tipo != "NCM" && detalle.Tipo != "CAC" && detalle.Tipo != "NCE")
                {
                    total += detalle.ImporteTotalNeto;

                    detalleCc = new RptCcDetalleViewModel();
                    detalleCc.Comprobante = detalle.Tipo + " " + detalle.PuntoVenta.ToString("#0000") + "-" + detalle.Numero.ToString("#00000000");
                    detalleCc.Observaciones = detalle.Observaciones;
                    detalleCc.Fecha = detalle.Fecha.ToString("dd/MM/yyyy");
                    detalleCc.Importe = detalle.ImporteTotalNeto.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

                    resultado.Items.Add(detalleCc);
                }
                else if (detalle.Tipo == "CAC")//Cobranza a cuenta
                {
                    total -= detalle.Importe;

                    detalleCc = new RptCcDetalleViewModel();
                    detalleCc.Comprobante = "Cobranza a cuenta";
                    detalleCc.Fecha = detalle.Fecha.ToString("dd/MM/yyyy");
                    detalleCc.Cobrado = "- " + detalle.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Observaciones = detalle.Observaciones;

                    resultado.Items.Add(detalleCc);
                }
                else if (detalle.Saldo > 0)
                {
                    total -= detalle.Saldo;

                    detalleCc = new RptCcDetalleViewModel();
                    detalleCc.Comprobante = "Saldo " + detalle.Tipo + " " + detalle.PuntoVenta.ToString("#0000") + "-" + detalle.Numero.ToString("#00000000");
                    detalleCc.Fecha = detalle.Fecha.ToString("dd/MM/yyyy");
                    detalleCc.Cobrado = "- " + detalle.Saldo.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    detalleCc.Observaciones = detalle.Observaciones;

                    resultado.Items.Add(detalleCc);
                }

                var cobrazasList = dbContext.CobranzasDetalle.Include("Cobranzas").Where(x => x.IDComprobante == detalle.IDComprobante && x.Cobranzas.IDPersona == idPersona).ToList();
                foreach (var cobranza in cobrazasList)
                {
                    total -= cobranza.Importe;

                    detalleCob = new RptCcDetalleViewModel();
                    if (cobranza.Cobranzas.Tipo != "SIN")
                        detalleCob.ComprobanteAplicado = cobranza.Cobranzas.Tipo + " " + cobranza.Cobranzas.PuntosDeVenta.Punto.ToString("#0000") + "-" + cobranza.Cobranzas.Numero.ToString("#00000000");
                    else
                        detalleCob.ComprobanteAplicado = "Cobranza a cuenta";

                    detalleCob.Comprobante = "";
                    detalleCob.Observaciones = cobranza.Cobranzas.Observaciones;
                    detalleCob.FechaCobro = cobranza.Cobranzas.FechaCobranza.ToString("dd/MM/yyyy");

                    /*if (cobranza.Cobranzas.CobranzasRetenciones.Any())
                    {
                        var ret = cobranza.Cobranzas.CobranzasRetenciones.Sum(x => x.Importe);
                        total -= ret;
                        detalleCob.Cobrado = "- " + (cobranza.Importe + ret).ToMoneyFormat();
                    }
                    else*/
                    detalleCob.Cobrado = "- " + cobranza.Importe.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());

                    detalleCob.Total = total.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());
                    resultado.Items.Add(detalleCob);
                }
            }

            detalleCc = new RptCcDetalleViewModel();
            detalleCc.Cobrado = "Saldo";
            detalleCc.Total = (total).ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales());



            resultado.Items.Add(detalleCc);
        }
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerListaPrecios()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.ListaPrecios.Where(x => x.IDUsuario == usu.IDUsuario && x.Activa == true)
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDListaPrecio,
                        Nombre = x.Nombre
                    }).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static List<Combo2ViewModel> obtenerPaises()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            return CommonModel.ObtenerPaises();
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static List<Combo2ViewModel> obtenerProvincias(int idPais)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            return CommonModel.ObtenerProvincias(idPais);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static List<Combo2ViewModel> obtenerCiudades(int idProvincia)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            return CommonModel.ObtenerCuidades(idProvincia);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string obtenerTipoPersona(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (entity != null)
                {
                    return entity.Tipo;
                }
                else
                    throw new Exception("No se pudo obtener los datos");
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static string obtenerProxNroPresupuesto()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var nro = "00000001";

            using (var dbContext = new ACHEEntities())
            {
                var presupuesto = dbContext.Presupuestos.Where(x => x.IDUsuario == usu.IDUsuario);
                if (presupuesto.Count() > 0)
                {
                    var numero = presupuesto.Max(x => x.Numero);
                    numero++;
                    nro = numero.ToString("#00000000");
                }
                //else
                //    nro = "00000001";
            }

            return nro;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string ObtenerUltimoTipoComprobanteCliente(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Compras.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario
                    && (x.Tipo == "FCA" || x.Tipo == "FCB" || x.Tipo == "FCC")).OrderByDescending(x => x.IDCompra).FirstOrDefault();
                if (entity != null)
                {
                    return entity.Tipo;
                }
                else
                    return "";
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string ObtenerUltimaCuentaContableCliente(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Compras.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).OrderByDescending(x => x.IDCompra).FirstOrDefault();
                if (entity != null)
                {
                    return entity.IDPlanDeCuenta.HasValue ? entity.IDPlanDeCuenta.ToString() : "";
                }
                else
                    return "";
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string ObtenerUltimoRubroComprobanteCliente(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Compras.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).OrderByDescending(x => x.IDCompra).FirstOrDefault();
                if (entity != null)
                    return entity.Rubro;
                else
                    return "";
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string ObtenerUltimaCategoriaComprobanteCliente(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Compras.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).OrderByDescending(x => x.IDCompra).FirstOrDefault();
                if (entity != null)
                    return entity.IDCategoria.ToString();
                else
                    return "";
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static string ObtenerUltimoTipoFacturacionCliente(int idPersona)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Comprobantes.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario
                    && (x.Tipo == "FCA" || x.Tipo == "FCB" || x.Tipo == "FCC")).OrderByDescending(x => x.IDComprobante).FirstOrDefault();
                if (entity != null)
                {
                    return entity.Tipo;
                }
                else
                    return "";
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static List<Combo2ViewModel> obtenerSelectPlanCuentas()
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                return dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario).ToList()
                    .Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDPlanDeCuenta,
                        Nombre = x.Nombre
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    //  [ScriptMethod(UseHttpGet = true)]
    [WebMethod(true)]
    public static List<Combo2ViewModel> ObtenerJurisdiccionUsuario()
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        List<Combo2ViewModel> select = new List<Combo2ViewModel>();
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            using (var dbContext = new ACHEEntities())
            {
                if (usu.IDJurisdiccion != null && usu.IDJurisdiccion.Trim() != "")
                {
                    var provincias = usu.IDJurisdiccion.Split(',');
                    List<Int32> idjurisdicciones = new List<Int32>();

                    foreach (var item in provincias)
                    {
                        if (item.Trim() != "")
                        {
                            idjurisdicciones.Add(Convert.ToInt32(item));
                        }
                    }

                    select = dbContext.Provincias.Where(x => idjurisdicciones.Contains(x.IDProvincia)).Select(x => new Combo2ViewModel()
                    {
                        ID = x.IDProvincia,
                        Nombre = x.Nombre
                    }).ToList();
                }
            }
            return select;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    

    #region ITEMS

    [WebMethod(true)]
    public static void agregarItem(int id, string idConcepto, string concepto, string iva, string precio, string bonif,
        string cantidad, string codigo, string idPlanDeCuenta, string codigoCta)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (usu != null)
        {
            int index = id;
            if (id != 0)
            {
                var aux = DetalleCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
                DetalleCart.Retrieve().Items.Remove(aux);
            }
            else
                index = DetalleCart.Retrieve().Items.Count() + 1;

            var tra = new DetalleViewModel(ObtenerCantidadDecimales());
            tra.ID = index;// DetalleCart.Retrieve().Items.Count() + 1;
            tra.Concepto = concepto;
            tra.Codigo = codigo;
            tra.CodigoPlanCta = codigoCta;
            if (iva != string.Empty)
                tra.Iva = decimal.Parse(iva);
            else
                tra.Iva = 0;
            if (bonif != string.Empty)
                tra.Bonificacion = decimal.Parse(bonif.Replace(".", ","));
            else
                tra.Bonificacion = 0;
            decimal precioUnitario = decimal.Parse(precio.Replace(".", ","));
            tra.PrecioUnitario = precioUnitario;// usu.UsaPrecioFinalConIVA ? ConceptosCommon.ObtenerPrecioFinal(precioUnitario, iva, ObtenerCantidadDecimales()) : precioUnitario;
            tra.Cantidad = decimal.Parse(cantidad.Replace(".", ",")); ;
            if (!string.IsNullOrEmpty(idConcepto))
                tra.IDConcepto = int.Parse(idConcepto);
            else
                tra.IDConcepto = null;

            if (idPlanDeCuenta != "")
                tra.IDPlanDeCuenta = Convert.ToInt32(idPlanDeCuenta);

            if (id != 0)
                DetalleCart.Retrieve().Items.Insert(id - 1, tra);
            else
                DetalleCart.Retrieve().Items.Add(tra);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }

    [WebMethod(true)]
    public static void agregarItemCombo(int id, string idConcepto, string concepto, string cantidad, string codigo)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        if (usu != null)
        {
            if (id != 0)
            {
                var aux = DetalleCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
                DetalleCart.Retrieve().Items.Remove(aux);
            }

            var tra = new DetalleViewModel(ObtenerCantidadDecimales());
            tra.ID = DetalleCart.Retrieve().Items.Count() + 1;
            tra.Codigo = codigo;
            tra.Concepto = concepto;
            var conc=ConceptosCommon.ObtenerConcepto(codigo,usu.IDUsuario);
            
            tra.CostoInterno =conc.CostoInterno??0; 
            tra.PrecioUnitario =conc.PrecioUnitario; 

            tra.Cantidad = decimal.Parse(cantidad.Replace(".", ",")); 
            if (!string.IsNullOrEmpty(idConcepto))
                tra.IDConcepto = int.Parse(idConcepto);
            else
                tra.IDConcepto = null;



            DetalleCart.Retrieve().Items.Add(tra);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }


    [WebMethod(true)]
    public static string obtenerItems()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var list = DetalleCart.Retrieve().Items.ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
                        html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Bonificacion.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Iva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        if (usu.UsaPlanCorporativo)
                            html += "<td class='divPlanDeCuentas' style='text-align:left'>" + detalle.CodigoPlanCta + "</td>";
                        else
                            html += "<td class='divPlanDeCuentas' style='text-align:left;display:none'></td>";
                        html += "<td style='text-align:right'>" + detalle.TotalConIva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";

                        decimal preciFinal = 0;

                        //if (usu.UsaPrecioFinalConIVA)
                        //    preciFinal = detalle.PrecioUnitarioConIvaSinBonificacion;
                        //else
                        preciFinal = detalle.PrecioUnitario;

                        html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem("
                            + detalle.ID
                            + ", '" + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
                            + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
                            + "' ,'" + detalle.Codigo
                            + "','" + detalle.Concepto
                            + "','" + preciFinal.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
                            + "','" + detalle.Iva.ToString("#0.00")
                            + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
                            + "','" + detalle.IDPlanDeCuenta.ToString()
                            + "');\"><i class='fa fa-edit'></i></a></td>";
                        html += "</tr>";

                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='9' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }

    [WebMethod(true)]
    public static string obtenerConceptosCombo()
    {
        var html = string.Empty;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            using (var dbContext = new ACHEEntities())
            {
                var list = DetalleCart.Retrieve().Items.ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
                        html += "<td style='text-align:right'>" + detalle.CostoInterno.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
                        
                        decimal preciFinal = 0;

                        if (usu.UsaPrecioFinalConIVA)
                            preciFinal = detalle.PrecioUnitarioConIvaSinBonificacion;
                        else
                            preciFinal = detalle.PrecioUnitario;

                        html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem("
                            + detalle.ID
                            + ", '" + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
                            + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
                            + "' ,'" + detalle.Codigo
                            + "','" + detalle.Concepto
                            + "','" + preciFinal.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
                            + "','" + detalle.Iva.ToString("#0.00")
                            + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
                            + "','" + detalle.IDPlanDeCuenta.ToString()
                            + "');\"><i class='fa fa-edit'></i></a></td>";
                        html += "</tr>";

                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='6' style='text-align:center'>No tienes items agregados</td></tr>";

        }
        return html;
    }

    [WebMethod(true)]
    public static string actualizarTotalCombo()
    {
        decimal total = 0;
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            total = DetalleCart.Retrieve().Items.Sum(x => x.CostoInterno * x.Cantidad);
           /* using (var dbContext = new ACHEEntities())
            {
                var list = DetalleCart.Retrieve().Items.ToList();
                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        var precio=dbContext.Conceptos.Where(x=>x.IDConcepto==detalle.IDConcepto).Select(x=>x.CostoInterno??0).FirstOrDefault();
                        total += precio*detalle.Cantidad;
                    }
                }
            }*/


        }
        return total.ToMoneyFormatForEdit(ConfiguracionHelper.ObtenerCantidadDecimales());
    }
    
    [WebMethod(true)]
    public static void eliminarItem(int id)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var aux = DetalleCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            if (aux != null)
                DetalleCart.Retrieve().Items.Remove(aux);
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }


    [WebMethod(true)]
    public static TotalesViewModel obtenerTotales(string percepcionesIIBB, string percepcionesIVA, string importeNoGrabado)
    {
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            TotalesViewModel totales = new TotalesViewModel();

            DetalleCart.Retrieve().ImporteNoGrabado = Convert.ToDecimal(importeNoGrabado.Replace(".", ","));
            DetalleCart.Retrieve().PercepcionIVA = Convert.ToDecimal(percepcionesIVA.Replace(".", ","));
            DetalleCart.Retrieve().PercepcionIIBB = Convert.ToDecimal(percepcionesIIBB.Replace(".", ","));

            totales.Iva = DetalleCart.Retrieve().GetIva().ToMoneyFormat(2);
            totales.Subtotal = DetalleCart.Retrieve().GetSubTotal().ToMoneyFormat(2);
            totales.ImporteNoGrabado = DetalleCart.Retrieve().GetImporteNoGrabado().ToMoneyFormat(2);
            totales.PercepcionIVA = DetalleCart.Retrieve().GetPercepcionIVA().ToMoneyFormat(2);
            totales.PercepcionIIBB = DetalleCart.Retrieve().GetPercepcionIIBB().ToMoneyFormat(2);
            totales.Total = DetalleCart.Retrieve().GetTotal().ToMoneyFormat(2);

            return totales;
        }
        else
            throw new Exception("Por favor, vuelva a iniciar sesión");
    }
    #endregion

    //#region DetalleOrdenCompra y venta
    //[WebMethod(true)]
    //public static string obtenerItemsOrdenCompra()
    //{
    //    var html = string.Empty;
    //    if (HttpContext.Current.Session["CurrentUser"] != null)
    //    {
    //        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

    //        using (var dbContext = new ACHEEntities())
    //        {
    //            var list = DetalleCart.Retrieve().Items.ToList();
    //            if (list.Any())
    //            {
    //                int index = 1;
    //                foreach (var detalle in list)
    //                {
    //                    html += "<tr>";
    //                    html += "<td>" + index + "</td>";
    //                    html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                    html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
    //                    html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
    //                    html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                    html += "<td style='text-align:right'>" + detalle.Bonificacion.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                    html += "<td style='text-align:right'>" + detalle.Iva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                    html += "<td style='text-align:right'>" + detalle.TotalConIva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";

    //                    decimal preciFinal = 0;

    //                    //if (usu.UsaPrecioFinalConIVA)
    //                    //    preciFinal = detalle.PrecioUnitarioConIvaSinBonificacion;
    //                    //else
    //                    preciFinal = detalle.PrecioUnitario;

    //                    html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
    //                    html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem("
    //                        + detalle.ID
    //                        + ", '" + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
    //                        + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
    //                        + "' ,'" + detalle.Codigo
    //                        + "','" + detalle.Concepto
    //                                             + "','" + preciFinal.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
    //                          + "','" + detalle.Iva.ToString("#0.00")
    //                      + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(ObtenerCantidadDecimales())

    //                        + "');\"><i class='fa fa-edit'></i></a></td>";
    //                    html += "</tr>";

    //                    index++;
    //                }
    //            }
    //        }
    //        if (html == "")
    //            html = "<tr><td colspan='9' style='text-align:center'>No tienes items agregados</td></tr>";

    //    }
    //    return html;
    //}

    //[WebMethod(true)]
    //public static string obtenerItemsOrdenVenta()
    //{
    //    var html = string.Empty;
    //    if (HttpContext.Current.Session["CurrentUser"] != null)
    //    {
    //        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];


    //        var list = DetalleCart.Retrieve().Items.ToList();
    //        if (list.Any())
    //        {
    //            int index = 1;
    //            foreach (var detalle in list)
    //            {

    //                html += "<tr>";
    //                html += "<td>" + index + "</td>";
    //                html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
    //                html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
    //                html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                html += "<td style='text-align:right'>" + detalle.Bonificacion.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                html += "<td style='text-align:right'>" + detalle.Iva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";
    //                html += "<td style='text-align:right'>" + detalle.TotalConIva.ToMoneyFormat(ConfiguracionHelper.ObtenerCantidadDecimales()) + "</td>";

    //                decimal preciFinal = 0;

    //                if (usu.UsaPrecioFinalConIVA)
    //                    preciFinal = detalle.PrecioUnitarioConIvaSinBonificacion;
    //                else
    //                    preciFinal = detalle.PrecioUnitario;

    //                html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
    //                html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem("
    //                    + detalle.ID
    //                    + ", '" + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
    //                    + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
    //                    + "' ,'" + detalle.Codigo
    //                    + "','" + detalle.Concepto
    //                                         + "','" + preciFinal.ToMoneyFormatForEdit(ObtenerCantidadDecimales())
    //                      + "','" + detalle.Iva.ToString("#0.00")
    //                  + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(ObtenerCantidadDecimales())

    //                    + "');\"><i class='fa fa-edit'></i></a></td>";
    //                html += "</tr>";

    //                index++;
    //            }
    //        }
    //    }
    //    if (html == "")
    //        html = "<tr><td colspan='9' style='text-align:center'>No tienes items agregados</td></tr>";


    //    return html;
    //}

    //[WebMethod(true)]
    //public static void agregarItemOrdenCompra(int id, string idConcepto, string concepto, string iva, string precio, string bonif,
    //    string cantidad, string codigo)
    //{
    //    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
    //    if (usu != null)
    //    {
    //        int index = id;
    //        if (id != 0)
    //        {
    //            var aux = DetalleCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
    //            DetalleCart.Retrieve().Items.Remove(aux);
    //        }
    //        else
    //            index = DetalleCart.Retrieve().Items.Count() + 1;

    //        var tra = new DetalleViewModel(ObtenerCantidadDecimales());
    //        tra.ID = index;// DetalleCart.Retrieve().Items.Count() + 1;
    //        tra.Concepto = concepto;
    //        tra.Codigo = codigo;
    //        if (iva != string.Empty)
    //            tra.Iva = decimal.Parse(iva);
    //        else
    //            tra.Iva = 0;
    //        if (bonif != string.Empty)
    //            tra.Bonificacion = decimal.Parse(bonif.Replace(".", ","));
    //        else
    //            tra.Bonificacion = 0;
    //        decimal precioUnitario = decimal.Parse(precio.Replace(".", ","));
    //        tra.PrecioUnitario = precioUnitario; //usu.UsaPrecioFinalConIVA ? ConceptosCommon.ObtenerPrecioFinal(precioUnitario, iva, ObtenerCantidadDecimales()) : precioUnitario;
    //        tra.Cantidad = decimal.Parse(cantidad.Replace(".", ",")); ;
    //        if (!string.IsNullOrEmpty(idConcepto))
    //            tra.IDConcepto = int.Parse(idConcepto);
    //        else
    //            tra.IDConcepto = null;

    //        if (id != 0)
    //            DetalleCart.Retrieve().Items.Insert(id - 1, tra);
    //        else
    //            DetalleCart.Retrieve().Items.Add(tra);
    //    }
    //    else
    //        throw new Exception("Por favor, vuelva a iniciar sesión");
    //}


    //[WebMethod(true)]
    //public static void agregarItemOrdenVenta(int id, string idConcepto, string concepto, string iva, string precio, string bonif,
    //    string cantidad, string codigo)
    //{
    //    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
    //    if (usu != null)
    //    {
    //        int index = id;
    //        if (id != 0)
    //        {
    //            var aux = DetalleCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
    //            DetalleCart.Retrieve().Items.Remove(aux);
    //        }
    //        else
    //            index = DetalleCart.Retrieve().Items.Count() + 1;

    //        var tra = new DetalleViewModel(ObtenerCantidadDecimales());
    //        tra.ID = index;// DetalleCart.Retrieve().Items.Count() + 1;
    //        tra.Concepto = concepto;
    //        tra.Codigo = codigo;
    //        if (iva != string.Empty)
    //            tra.Iva = decimal.Parse(iva);
    //        else
    //            tra.Iva = 0;
    //        if (bonif != string.Empty)
    //            tra.Bonificacion = decimal.Parse(bonif.Replace(".", ","));
    //        else
    //            tra.Bonificacion = 0;
    //        decimal precioUnitario = decimal.Parse(precio.Replace(".", ","));
    //        tra.PrecioUnitario = usu.UsaPrecioFinalConIVA ? ConceptosCommon.ObtenerPrecioFinal(precioUnitario, iva, ObtenerCantidadDecimales()) : precioUnitario;
    //        tra.Cantidad = decimal.Parse(cantidad.Replace(".", ",")); ;
    //        if (!string.IsNullOrEmpty(idConcepto))
    //            tra.IDConcepto = int.Parse(idConcepto);
    //        else
    //            tra.IDConcepto = null;
    //        // tra.PrecioUnitario = ConceptosCommon.ObtenerPrecioFinal(tra.PrecioUnitario, iva, ObtenerCantidadDecimales());//, idPersona);
    //        if (id != 0)
    //            DetalleCart.Retrieve().Items.Insert(id - 1, tra);
    //        else
    //            DetalleCart.Retrieve().Items.Add(tra);
    //    }
    //    else
    //        throw new Exception("Por favor, vuelva a iniciar sesión");
    //}

    //[WebMethod(true)]
    //public static void eliminarItemOrdenCompra(int id)
    //{
    //    if (HttpContext.Current.Session["CurrentUser"] != null)
    //    {
    //        var aux = DetalleCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
    //        if (aux != null)
    //            DetalleCart.Retrieve().Items.Remove(aux);
    //    }
    //    else
    //        throw new Exception("Por favor, vuelva a iniciar sesión");
    //}


    //[WebMethod(true)]
    //public static TotalesViewModel obtenerTotalesOrdenCompra()
    //{
    //    if (HttpContext.Current.Session["CurrentUser"] != null)
    //    {
    //        TotalesViewModel totales = new TotalesViewModel();


    //        totales.Iva = DetalleCart.Retrieve().GetIva().ToMoneyFormat(2);
    //        totales.Subtotal = DetalleCart.Retrieve().GetSubTotal().ToMoneyFormat(2);
    //        totales.Total = DetalleCart.Retrieve().GetTotal().ToMoneyFormat(2);

    //        return totales;
    //    }
    //    else
    //        throw new Exception("Por favor, vuelva a iniciar sesión");
    //}

    //[WebMethod(true)]
    //public static TotalesViewModel obtenerTotalesOrdenVenta()
    //{
    //    if (HttpContext.Current.Session["CurrentUser"] != null)
    //    {
    //        TotalesViewModel totales = new TotalesViewModel();


    //        totales.Iva = DetalleCart.Retrieve().GetIva().ToMoneyFormat(2);
    //        totales.Subtotal = DetalleCart.Retrieve().GetSubTotal().ToMoneyFormat(2);
    //        totales.Total = DetalleCart.Retrieve().GetTotal().ToMoneyFormat(2);

    //        return totales;
    //    }
    //    else
    //        throw new Exception("Por favor, vuelva a iniciar sesión");
    //}
    //#endregion

    #region Configuración Usuario

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = true)]
    public static byte ObtenerCantidadDecimales()
    {
        return ConfiguracionHelper.ObtenerCantidadDecimales();
    }

    #endregion
}
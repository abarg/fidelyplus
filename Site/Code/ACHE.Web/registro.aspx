﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registro.aspx.cs" Inherits="registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fidely Plus</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png" />
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/css/global") %>
    </asp:PlaceHolder>
    <style type="text/css">
        small {
            font-size: 12px;
        }
        .lockedpanel {
            width: 320px;
            margin: 0% auto 0 auto;
        }
    </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="/js/html5shiv.js"></script>
        <script src="/js/respond.min.js"></script>
    <![endif]-->
    <script>
        if (window.location.href.indexOf("/app/") > -1)
            document.location.href = window.location.href.replace("/app/", "/");
	</script>
</head>
<body class="notfound">
    <section>

        <div class="lockedpanel">
            <div class="loginuser">
                <img src="images/logo-login.png" alt="contabilium" runat="server" id="imgLogo" />
            </div>
            <div id="divRegistroForm">
                <div class="logged" style="margin-top: 10px;">
                    <h4>Probalo 10 días sin cargo</h4>
                    <br />
                    <p>¿Ya sos un usuario? Hace click <a href="login.aspx"><strong>aquí</strong></a> para ingresar</p>
                </div>

                <form runat="server" id="frmRegistro">
                    <div class="alert alert-danger" id="divError" style="display: none"></div>
                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control required" MaxLength="128" placeholder="Ingresá tu nombre o razón social"></asp:TextBox>
                    <%--<asp:TextBox runat="server" ID="txtNroDocumento" CssClass="form-control required number validCuit" MaxLength="13" placeholder="Ingresá tu CUIT/CUIL"></asp:TextBox>--%>
                    <asp:TextBox runat="server" ID="txtEmail" TextMode="Email" CssClass="form-control required" MaxLength="128" Style="margin-top: 10px" placeholder="Ingresá tu email"></asp:TextBox>
                    <asp:TextBox runat="server" TextMode="Password" ID="txtPwd" CssClass="form-control required" MaxLength="20" MinLength="6" Style="margin-top: 10px" placeholder="Ingresá tu contraseña"></asp:TextBox>
                    <asp:TextBox runat="server" TextMode="Password" ID="txtPwd2" CssClass="form-control required" MaxLength="20" MinLength="6" Style="margin-top: 10px" placeholder="Ingresá nuevamente tu contraseña"></asp:TextBox>
                    <%--<asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control required" MaxLength="30" MinLength="6" Style="margin-top: 10px" placeholder="(0011) 1234-5678"></asp:TextBox>--%>
                    <div class="form-group">
                        <asp:TextBox runat="server" ID="txtArea" CssClass="form-control" MaxLength="5" MinLength="2" Style="margin-top: 10px;margin-right: 5%;width: 30%; float:left" placeholder="Área"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control required" MaxLength="15" MinLength="6" Style="margin-top: 10px;width: 65%; float:left" placeholder="Teléfono"></asp:TextBox>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlActividad" CssClass="form-control required" Style="margin-top: 10px">
                       <asp:ListItem Text="Seleccione su actividad" Value=""></asp:ListItem>
                        <asp:ListItem Text="Vendo productos" Value="Vendo productos"></asp:ListItem>
                        <asp:ListItem Text="Vendo servicios" Value="Vendo servicios"></asp:ListItem>
                        <asp:ListItem Text="Vendo productos y servicios" Value="Vendo productos y servicios"></asp:ListItem>
                        <asp:ListItem Text="Soy contador" Value="Soy contador"></asp:ListItem>
                        <asp:ListItem Text="Otro" Value="Otro"></asp:ListItem>
                    </asp:DropDownList>
                    <small>¿Tenés un código de promoción? <a href="javascript:$('#txtCodigoPromocion').slideToggle();"><strong>Ingresalo aquí</strong></a></small>
                    <asp:TextBox runat="server" ID="txtCodigoPromocion" CssClass="form-control" MaxLength="150" Style="margin-top: 10px; display: none" placeholder="Ingresá el código de promoción"></asp:TextBox>

                    <br />
                    <small style="float: left; margin-top: 10px;">
                        <input type="checkbox" id="chkTyC" runat="server" />&nbsp;&nbsp;Acepto los <a href="http://www.contabilium.com/terminos-de-uso/" target="_blank">Términos y Condiciones</a></small>
                    <br />
                    <br />
                    <a class="btn btn-success" id="lnkRegistrarme" onclick="grabar();">Empezar a probarlo <br /><small>(No requiere tarjeta de crédito)</small></a>
                    <br />
                    <a href="login.aspx"><p>volver</p></a>
                </form>

            </div>
        </div>

    </section>

    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/js/jquery-ui-1.10.3.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/modernizr.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/jquery.numericInput.min.js"></script>
    <script src="/js/views/common.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <script src="/js/jquery.maskMoney.min.js"></script>
    <script src="/js/views/registro.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>
    <script src="/js/placeholders.jquery.min.js"></script>
</body>
</html>

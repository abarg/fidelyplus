﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Services;
using System.Data;
using System.IO;
using System.Web.Services;
using ACHE.Negocio.Common;


public partial class alertas : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            using (var dbContext = new ACHEEntities()) {
                var TieneDatos = dbContext.Alertas.Any(x => x.IDUsuario == CurrentUser.IDUsuario);
                if (TieneDatos) {
                    divConDatos.Visible = true;
                    divSinDatos.Visible = false;
                }
                else {
                    divConDatos.Visible = false;
                    divSinDatos.Visible = true;
                }
            }

        }
    }

    [WebMethod(true)]
    public static void delete(int id) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            AlertasCommon.EliminarAlerta(id, usu.IDUsuario);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosAlertasViewModel getResults(string avisoAlertas, int page, int pageSize) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            return AlertasCommon.ObtenerAlertas(avisoAlertas, page, pageSize, usu.IDUsuario);

        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static AlertasViewModel cargarEntidad(int id) {
        return AlertasCommon.ObtenerAlerta(id);
    }

    [WebMethod(true)]
    public static void guardarAlertas(int id, string importe, string avisos, string condiciones) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        AlertasCommon.GuardarAlertas(id, importe, avisos, condiciones, usu.IDUsuario);

    }

    [WebMethod(true)]
    public static void esconderAlertaGenerada(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        using (var dbContext = new ACHEEntities()) {
            AlertasCommon.OcultasAlertasGeneradas(id, usu.IDUsuario);
        }

    }
}
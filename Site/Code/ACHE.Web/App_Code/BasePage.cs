﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PaginaBase
/// </summary>
public class BasePage : System.Web.UI.Page {
    public WebUser CurrentUser {
        get { return (Session["CurrentUser"] != null) ? (WebUser)Session["CurrentUser"] : null; }
        set { Session["CurrentUser"] = value; }
    }

    private void ValidateUser() {
        string pageName = Request.FilePath.Substring(Request.FilePath.LastIndexOf(@"/") + 1).ToLower();
        string pageFrom = Request.FilePath;
        if (pageName != "login.aspx") {
            if (CurrentUser == null) {
                if (Request.QueryString.AllKeys.Contains("code")) {
                    Response.Redirect("/registroIntegracion.aspx?" +
                    String.Join("&", Request.QueryString.AllKeys
                        .SelectMany(key => Request.QueryString.GetValues(key)
                            .Select(
                                value => String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value))))
                        .ToArray()));
                }
                else {
                    var qs = Request.QueryString.AllKeys
                        .SelectMany(key => Request.QueryString.GetValues(key)
                            .Select(
                                value =>
                                    String.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value))))
                        .ToList();
                    qs.Add(String.Format("{0}={1}", HttpUtility.UrlEncode("fromPage"), HttpUtility.UrlEncode(pageFrom)));
                    Response.Redirect("/login.aspx?" + String.Join("&", qs));
                }

            }
            else {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                //if (!usu.SetupFinalizado && pageName != "finregistro.aspx")// && pageName != "faq.aspx" && pageName != "tutoriales.aspx" 
                ////&& pageName != "personase.aspx" 
                ////&& pageName != "mis-datos.aspx" && pageName != "importar.aspx" && pageName != "elegir-plan.aspx" 
                ////&& pageName != "pagodeplanes.aspx"// && pageName != "comprasMercadopago.aspx"
                ////&& pageName != "cambiar-pwd.aspx")// && pageName != "usuarios.aspx" && pageName != "usuariose.aspx")
                //{
                //    Response.Redirect("/finRegistro.aspx");
                //}
                if (!Request.FilePath.ToLower().Contains("addins")) {
                    if (CurrentUser.PlanPendienteConfirmacion) {
                        RecheckUserPlan();
                    }

                    if (!PermisosModulos.tienePlan(pageName))
                        Response.Redirect("/modulos/seguridad/elegir-plan.aspx?upgrade=4&tipo=" + pageName.Replace(".aspx", ""));


                    if ((CurrentUser.TipoUsuario == "B") && (pageName != "home.aspx" && pageName != "referidos.aspx" && pageName != "cronograma.aspx")) {
                        if (!PermisosModulos.tieneAccesoAlModulo(pageName))
                            Response.Redirect("/home.aspx");
                    }
                    if ((CurrentUser.TipoUsuario == "C") && (pageName != "home.aspx" && pageName != "referidos.aspx" && pageName != "cronograma.aspx")) {
                        if (!PermisosModulos.tieneAccesoAlModuloPersonalizado(pageName))
                            Response.Redirect("/home.aspx");
                    }
                    if (!CurrentUser.TieneDebitoAutomatico) {
                        if (CurrentUser.PlanPendienteConfirmacion && pageName != "pago-realizado.aspx" && pageName != "home.aspx") {
                            Response.Redirect("/home.aspx");
                        }
                        if (!CurrentUser.PlanVigente && !CurrentUser.PlanPendienteConfirmacion && pageName != "elegir-plan.aspx" && pageName != "pagodeplanes.aspx" && pageName != "pago-realizado.aspx")// && !CurrentUser.TieneDebitoAutomatico)
                    {
                            Response.Redirect("/modulos/seguridad/elegir-plan.aspx");
                        }
                    }
                }
            }
        }

    }

    private void RecheckUserPlan() {
        using (var dbContext = new ACHEEntities()) {
            var plan = PermisosModulos.ObtenerPlanActual(dbContext, CurrentUser.IDUsuario);
            if (plan != null) {
                var usu = CurrentUser;
                HttpContext.Current.Session["CurrentUser"] = new WebUser(
                              usu.IDUsuario, usu.IDUsuarioAdicional, usu.TipoUsuario, usu.RazonSocial, usu.CUIT,
                              usu.CondicionIVA,
                              usu.Email, usu.EmailDisplayFrom, "", usu.Domicilio, usu.Pais, usu.IDProvincia,
                              usu.IDCiudad, usu.Telefono, usu.TieneFE, usu.IIBB, usu.FechaInicio,
                              usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupFinalizado, usu.TieneMultiEmpresa,
                              usu.ModoQA, plan.IDPlan,
                              usu.EmailAlerta, usu.Provincia, usu.Ciudad, usu.AgentePercepcionIVA,
                              usu.AgentePercepcionIIBB, usu.AgenteRetencion,
                              true, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA,
                              usu.FechaAlta,
                              usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion,
                              usu.UsaPlanCorporativo,
                              usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenes,
                              usu.MostrarBonificacionFc, usu.PorcentajeDescuento, false);
            }

        }
    }

    protected override void OnInit(EventArgs e) {
        if (CurrentUser == null)//Esto es para evitar CSRF Attacks
        {
            ViewStateUserKey = Session.SessionID;
        }
        base.OnInit(e);
    }

    public string GetUserIP() {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (!string.IsNullOrEmpty(ipList)) {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }

    protected override void OnPreInit(EventArgs e) {
        ValidateUser();
    }
}
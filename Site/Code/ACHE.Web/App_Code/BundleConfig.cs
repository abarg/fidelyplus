﻿using System.Configuration;
using System.Web.Optimization;


public class BundleConfig
{
    public static void RegisterBundles(BundleCollection bundles)
    {
        bundles.Add(new StyleBundle("~/css/global").Include(
            "~/css/bootstrap.min.css",
            "~/css/bootstrap-overrideFidely.css",
            "~/css/jquery-ui-1.10.3.css",
            "~/css/font-awesome.min.css",
            "~/css/animate.min.css",
            "~/css/animate.delay.css",
            "~/css/toggles.css",
            "~/css/select2.css",
            "~/css/style.defaultFidely.css",
            "~/css/meganav.css",
            "~/css/style-fixFidely.css"
        ));


        bundles.Add(new ScriptBundle("~/scripts/global").Include(
            "~/js/jquery-1.11.1.min.js",
            "~/js/jquery-migrate-1.2.1.min.js",
            "~/js/jquery-ui-1.10.3.min.js",
            "~/js/bootstrap.min.js",
            "~/js/modernizr.min.js",
            "~/js/jquery.numericInput.min.js",
            "~/js/toggles.min.js",
            "~/js/jquery.cookies.js",
            "~/js/custom.js",
            "~/js/bootbox.js",
            "~/js/meganav.js",
            "~/js/print.min.js",
            "~/js/jquery.tmpl.min.js",
            "~/js/jquery.validate.min.js",
            //"~/js/select2.min.js",
            "~/js/jquery.maskedinput.min.js",
            "~/js/jquery.maskMoney.min.js",
            "~/js/views/common.js",
            "~/js/views/alertas.js"

        ));
    }
}

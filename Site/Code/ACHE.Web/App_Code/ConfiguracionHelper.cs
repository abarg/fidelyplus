﻿using ACHE.Model;
using ACHE.Negocio.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConfiguracionHelper
/// </summary>
public static class ConfiguracionHelper
{
    public static byte ObtenerCantidadDecimales()
    {
        //Buscamos primero el dato en sesión
        if (HttpContext.Current.Session["CantidadDecimales"] != null)
        {
            var retorno = default(byte);
            byte.TryParse(HttpContext.Current.Session["CantidadDecimales"].ToString(), out retorno);
            return retorno;
        }

        //Si no existe en sesión, entonces buscamos en la base
        if (HttpContext.Current.Session["CurrentUser"] != null)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var retorno = UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            //Grabamos el dato en sesión
            HttpContext.Current.Session["CantidadDecimales"] = retorno;

            return retorno;
        }

        //No hay usuario logeado
        throw new Exception("Por favor, vuelva a iniciar sesión");
    }
}
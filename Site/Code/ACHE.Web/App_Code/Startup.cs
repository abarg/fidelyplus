﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Hangfire;
using Hangfire.SqlServer;
using Owin;

/// <summary>
/// Summary description for Startup
/// </summary>
public class Startup
{
    public void Configuration(IAppBuilder app)
    {
        var serveroptions = new BackgroundJobServerOptions
        {
            ServerName = "App"
        };
        var bdoptions = new SqlServerStorageOptions
        {
            PrepareSchemaIfNecessary = false
        };
        GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["hangfirebd"].ConnectionString,bdoptions);
        app.UseHangfireServer(serveroptions);
    }
}
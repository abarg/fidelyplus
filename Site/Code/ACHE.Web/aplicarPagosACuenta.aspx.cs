﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Common;

public partial class aplicarPagosACuenta : BasePage {

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            //TODO: pasar a businnes
            using (var dbContext = new ACHEEntities()) {
                var cant = dbContext.PagosDetalle.Where(x => !x.IDCompra.HasValue && x.Pagos.IDUsuario == CurrentUser.IDUsuario).Select(x => x.IDPago).Distinct().Count();
                if (cant > 0) {
                    divAlert.Visible = true;
                    litMsj.Text = "Usted tiene <a style='color: #8a6d3b;font-weight: bold;text-decoration: underline;' href='javascript:Common.verPagosACuenta();'>" + cant + " pago(s)</a> a cuenta pendiente(s) de imputación";
                }
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static ResultadosPagosViewModel getResults(int idPersona, int page, int pageSize) {
        try {
            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            return PagosCommon.ObtenerPagosACuenta(idPersona, page, pageSize, usu.IDUsuario);


        }
        catch (Exception e) {
            var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
            BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
            throw e;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static void AplicarPagosACuenta(int idPersona, AsignacionPagoACuentaDto[] pagosAcuenta) {
        try {

            if (!UsuarioCommon.VerificarWebUserSession())
                throw new CustomException("Por favor, vuelva a iniciar sesión");

            var pagosAAplicar = new Dictionary<int, List<int>>();

            foreach (var pago in pagosAcuenta.Where(pago => pago.IdCompra != -1)) {
                if (!pagosAAplicar.ContainsKey(pago.IdCompra))
                    pagosAAplicar.Add(pago.IdCompra, new List<int>());
                pagosAAplicar[pago.IdCompra].Add(pago.IdPagoACuenta);
            }

            PagosCommon.AplicarPagosACuenta(idPersona, pagosAAplicar);
        }
        catch (CustomException ex) {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
    }
}
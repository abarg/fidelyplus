﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="conceptos.aspx.cs" Inherits="conceptos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .modal.modal-wide .modal-dialog {
            width: 90%;
            max-width: 900px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="pageheader">
        <h2><i class="fa fa-file-text"></i>Productos y servicios <span>Administración</span></h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li class="active">Productos y servicios</li>
            </ol>
        </div>
    </div>
    <div id="divConDatos" runat="server">
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12 col-md-12 table-results">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <form runat="server" id="frmSearch">
                                <input type="hidden" id="hdnPage" runat="server" value="1" />
                                <div class="col-sm-6 col-md-2 form-group">
                                    <select id="ddlTipo" class="form-control" onchange="filtrar();">
                                        <option value="">Todos los tipos</option>
                                        <option value="P">Productos</option>
                                        <option value="S">Servicios</option>
                                        
                                    </select>
                                </div>
                                <div class="col-sm-12 col-md-2 form-group">
                                    <input type="text" class="form-control" id="txtCodigo" maxlength="128" placeholder="Código (SKU)" />
                                </div>
                                <div class="col-sm-12 col-md-4 form-group">
                                    <input type="text" class="form-control" id="txtCondicion" maxlength="128" placeholder="Ingresá el nombre o descripción" />
                                </div>
                                <div class="col-sm-12 col-md-4 form-group">
                                    <div class="btn-group" id="btnAcciones">
                                        <a class="btn btn-warning mr10" onclick="nuevo();">
                                            <i class="fa fa-plus"></i>&nbsp;Nuevo Producto o servicio
                                        </a>
                                    </div>
                                    <div class="btn-group dropdown" id="Div1">
                                        <button type="button" class="btn btn-btn-default"><i class="fa fa-list"></i>&nbsp;Otras acciones</button>
                                        <button class="btn btn-btn-default dropdown-toggle" data-toggle="dropdown"><span class="fa fa-caret-down"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:importar('Productos')">Importación masiva de productos</a></li>
                                            <li><a href="javascript:importar('Servicios')">Importación masiva de servicios</a></li>
                                            <li><a href="/importar.aspx?tipo=Stock">Aumento masivo de stock</a></li>
                                            <li><a href="/modulos/ventas/aumentoMasivoPrecios.aspx">Aumento masivo de precios</a></li>
                                            <li><a href="/modulos/inventarios/modificacionStock.aspx">Modificación de stock</a></li>
                                            <li><a href="/modulos/ventas/listaPrecios.aspx" data-formulario="listaPrecios" id="liListaPreciosConceptos" runat="server">Listas de precios</a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </form>
                            <div class="col-sm-12"><hr class="mt0" /></div>
                            <div class="row">
                                <div class="pull-right">
                                    <div class="btn-group mr10" id="divExportar" runat="server">
                                        <div class="btn btn-white tooltips">
                                            <a id="divIconoDescargar" href="javascript:exportar();">
                                                <i class="glyphicon glyphicon-save"></i>&nbsp;Exportar
                                            </a>
                                            <img alt="" src="/images/loaders/loader1.gif" id="imgLoading" style="display: none" />
                                            <a href="" id="lnkDownload" onclick="resetearExportacion();" download="Productos" style="display: none">Descargar</a>
                                        </div>
                                    </div>

                                    <div class="btn-group mr10" id="divPagination" style="display: none">
                                        <a class="btn btn-white" id="lnkPrevPage" style="cursor: pointer" onclick="mostrarPagAnterior();"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</a>
                                        <a class="btn btn-white" id="lnkNextPage" style="cursor: pointer" onclick="mostrarPagProxima();">Siguiente <i class="glyphicon glyphicon-chevron-right"></i></a>
                                    </div>
                                </div>

                                <h4 class="panel-title" style="clear: left; padding-left: 20px">Resultados</h4>
                                <p id="msjResultados" style="padding-left: 20px"></p>
                            </div>
                        </div>
                        <!-- panel-heading -->
                        <div class="panel-body">
                            <div class="alert alert-danger" id="divError" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Lo sentimos!</strong> <span id="msgError"></span>
                            </div>
                            <div class="table-responsive">
                                <table class="table mb30">
                                    <thead>
                                        <tr>
                                            <th>Tipo</th>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                            <th>Precio Unitario</th>
                                            <th>Costo Interno s/IVA</th>
                                            <th>IVA %</th>
                                            <th>Stock Total</th>
                                            
                                            <th>Estado</th>
                                            <th class="columnIcons" style="min-width: 110px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="resultsContainer">
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script id="resultTemplate" type="text/x-jQuery-tmpl">
            {{each results}}
            <tr>
                <td>${Tipo}</td>
                <td>${Codigo}</td>
                <td>${Nombre}</td>
                <td>${Precio}</td>
                <td>${CostoInterno}</td>
                <td>${Iva}</td>
                <td>
                    {{if $value.Tipo == "Producto"}}
                        <a onclick="verStock(${ID},'${Codigo} - ${Nombre}');" style="cursor: pointer;" title="Ver stock">${Stock}</a>
                    {{else}} 
                        ${Stock}
                    {{/if}} 
                </td>
                
                <td>${Estado}</td>
                <td class="table-action">
                    <a onclick="editar(${ID});" style="cursor: pointer; font-size: 16px" title="Editar"><i class="fa fa-pencil"></i></a>
                    {{if $value.Tipo == "Producto"}}
                    <a onclick="obtenerHistorial(${ID},'${Nombre}');" style="cursor: pointer; font-size: 16px" title="Historial de precios"><i class="fa fa-book"></i></a>
                    {{/if}} 
                    <a onclick="duplicar(${ID});" style="cursor: pointer; font-size: 16px" class="delete-row" title="Duplicar"><i class="fa fa-files-o"></i></a>
                    <a onclick='eliminar(${ID},"${Nombre}");' style="cursor: pointer; font-size: 16px" class="delete-row" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            {{/each}}
        </script>

        <script id="noResultTemplate" type="text/x-jQuery-tmpl">
            <tr>
                <td colspan="9">No se han encontrado resultados</td>
            </tr>
        </script>
    </div>

    <div id="divSinDatos" runat="server">
        <div class="panel-heading" style="background-color: white; height: 30%; width: 100%; border-radius: 4px; text-align: center;">
            <h2 id="hTitulo">Aún no has creado ningún producto o servicio</h2>
            <br />
            <a class="btn btn-warning" onclick="nuevo();" id="btnNuevoSinDatos">Crea un producto o servicio</a>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalHistorial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalTitle">Historial de precios</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Proveedor</th>
                                        <th>Costo Interno s/IVA</th>
                                        <th>Rentabilidad</th>
                                        <th>Precio</th>
                                    </tr>
                                </thead>
                                <tbody id="bodyDetalle">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalReservas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalReservasTitle">Reservas del producto</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr id="headerReservas">
                                        <th>Fecha</th>
                                        <th>Nro Orden</th>
                                        <th>Origen</th>
                                        <th style="max-width:280px">Cliente</th>
                                        <th>Cantidad</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="bodyReservas">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal modal-wide fade" id="modalCombo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalComboTitle">Reservas del producto</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="table-responsive">
                            <table class="table mb30">
                                <thead>
                                    <tr id="headerReservas">
                                        <th>Cantidad</th>
                                        <th>SKU</th>
                                        <th>Nombre</th>
                                    </tr>
                                </thead>
                                <tbody id="bodyCombo">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="margin-left: 20px" href="#" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="Server">
    <script src="/js/views/conceptos.js?v=<%= ConfigurationManager.AppSettings["JS.Version"] %>"></script>

    <script>
        jQuery(document).ready(function () {
            if ($('#divConDatos').is(":visible")) {
                filtrar();
                configFilters();
            }
        });
    </script>
</asp:Content>

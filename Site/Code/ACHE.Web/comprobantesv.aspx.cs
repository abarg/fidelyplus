﻿using ACHE.Extensions;
using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACHE.FacturaElectronica;
using System.Configuration;
using ACHE.FacturaElectronicaModelo;
using ACHE.Negocio.Common;
using ACHE.Negocio.Facturacion;

public partial class comprobantesv : BasePage {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            if (!String.IsNullOrEmpty(Request.QueryString["ID"])) {
                hdnID.Value = Request.QueryString["ID"];
                if (hdnID.Value != "0") {
                    cargarEntidad(int.Parse(hdnID.Value));
                }
                else
                    Response.Redirect("/comprobantes.aspx");
            }
        }
    }

    private void cargarEntidad(int id) {
        using (var dbContext = new ACHEEntities()) {
            var entity = dbContext.Comprobantes.Include("PuntosDeVenta").Include("Personas").Include("ComprobantesDetalle")
                .Include("ComprobantesTributos")
                .Where(x => x.IDUsuario == CurrentUser.IDUsuario && x.IDComprobante == id).FirstOrDefault();
            if (entity != null) {
                if (entity.FechaCAE.HasValue) {
                    hdnIDUsuario.Value = CurrentUser.IDUsuario.ToString();
                    //Configuro botones
                    var fileName = entity.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_" + entity.Tipo + "-" + entity.PuntosDeVenta.Punto.ToString("#0000") + "-" + entity.Numero.ToString("#00000000") + ".pdf";

                    var pathComprobante = HttpContext.Current.Server.MapPath("~/files/explorer/" + CurrentUser.IDUsuario.ToString() + "/Comprobantes/" + entity.FechaAlta.Year.ToString() + "/" + fileName);
                    if (!System.IO.File.Exists(pathComprobante))
                        fileName = GenerarPDF(entity);

                    hdnTipoComprobante.Value = entity.Tipo;
                    hdnRazonSocial.Value = entity.Personas.RazonSocial;
                    txtEnvioPara.Value = string.IsNullOrEmpty(entity.Personas.EmailsEnvioFc) ? entity.Personas.Email : entity.Personas.EmailsEnvioFc;
                    hdnFileName.Value = fileName;
                    hdnFileYear.Value = entity.FechaComprobante.Year.ToString();
                    //ifrPdf.Attributes.Add("src", "/files/explorer/" + CurrentUser.IDUsuario.ToString() + "/Comprobantes/" + entity.FechaAlta.Year.ToString() + "/" + fileName + "#zoom=100&view=FitH,top");
                    lnkDescargar2.NavigateUrl = lnkDescargar.NavigateUrl = "/pdfGenerator.ashx?file=" + fileName + "&year=" + entity.FechaComprobante.Year;
                    lnkPreview.Attributes.Add("onclick", "Common.previsualizarComprobanteGeneradoPorAnio('" + fileName + "','" + entity.FechaComprobante.Year + "');return false;");
                    lnkPrint.Attributes.Add("onclick", "Common.imprimirArchivoDesdeIframe('" + fileName + "');return false;");
                    lnkPrint2.Attributes.Add("onclick", "Common.imprimirArchivoDesdeIframe('" + fileName + "');return false;");

                    litRazonSocial.Text = CurrentUser.RazonSocial + " - " + CurrentUser.CUIT;
                    litDomicilio.Text = CurrentUser.Domicilio;
                    litPaisCiudad.Text = CurrentUser.Provincia + ", " + CurrentUser.Ciudad;
                    litTelefono.Text = CurrentUser.Telefono;
                    litEmail.Text = CurrentUser.Email;

                    //Datos de la fc
                    litComprobante.Text = entity.Tipo + " " + entity.PuntosDeVenta.Punto.ToString("#0000") + "-" + entity.Numero.ToString("#00000000");
                    litPersonaRazonSocial.Text = entity.Personas.RazonSocial;
                    litPersonaDomicilio.Text = entity.Personas.Domicilio;
                    litPersonaPaisCiudad.Text = entity.Personas.Provincias.Nombre + ", " + entity.Personas.Ciudades.Nombre;
                    //litPersonaEmail.Text = entity.Personas.Email;
                    //litPersonaTelefono.Text = entity.Personas.Telefono;
                    litFecha.Text = entity.FechaComprobante.ToLongDateString();
                    //litFechaVencimiento.Text = entity.FechaVencimiento.ToLongDateString();
                    litPersonaCondicionIva.Text = UsuarioCommon.GetCondicionIvaDesc(entity.Personas.CondicionIva);

                    var detalle = entity.ComprobantesDetalle.Select(x => new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales()) {
                        Codigo = (x.Conceptos == null) ? "" : x.Conceptos.Codigo,
                        Concepto = x.Concepto,
                        Cantidad = x.Cantidad,
                        PrecioUnitario = x.PrecioUnitario,
                        Bonificacion = x.Bonificacion,
                        Iva = x.Iva
                    });

                    rptDetalle.DataSource = detalle;
                    rptDetalle.DataBind();

                    //Precio Neto = Es el precio del artículo más todos los gravamenes o impuestos que este genere. 
                    //Precio Bruto= Es el precio base, sin incluir los gravamenes o impuestos.


                    litSubtotal.Text = detalle.Where(x => x.Iva > 0).Sum(x => x.PrecioUnitarioSinIVA * x.Cantidad).ToMoneyFormat(2);
                    litNoGravado.Text = detalle.Where(x => x.Iva == 0).Sum(x => x.PrecioUnitarioSinIVA * x.Cantidad).ToMoneyFormat(2);
                    litImpuestosInternos.Text = entity.ComprobantesTributos.Sum(x => x.Importe).ToMoneyFormat(2);

                    litIva.Text = (entity.ImporteTotalNeto - entity.ImporteTotalBruto).ToMoneyFormat(2);
                    litTotal.Text = entity.ImporteTotalNeto.ToMoneyFormat(2);

                    if (entity.Observaciones != string.Empty)
                        litObservaciones.Text = entity.Observaciones;
                    else
                        litObservaciones.Text = "El comprobante no tiene observaciones";

                    litUsuarioAlta.Text = entity.UsuarioAlta;
                    litUsuarioModifica.Text = entity.UsuarioModifica;

                    if (!entity.ComprobantesDetalle.Any(x => !x.IDConcepto.HasValue || (x.IDConcepto.HasValue && x.Conceptos.Tipo != "S")))
                        lnkDescargarRemito.Visible = false;
                }
                else
                    Response.Redirect("/comprobantese.aspx?ID=" + id);
            }
            else
                Response.Redirect("/error.aspx");
        }
    }

    [WebMethod(true)]
    public static string previsualizar(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

             var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            var pathPdf = HttpContext.Current.Server.MapPath("/files/facturas/" + usu.IDUsuario + "_prev.pdf");
            if (System.IO.File.Exists(pathPdf))
                System.IO.File.Delete(pathPdf);

            using (var dbContext = new ACHEEntities()) {
                Comprobantes entity = dbContext.Comprobantes.Include("Personas").Include("ComprobantesDetalle").Include("PuntosDeVenta")
                    .Where(x => x.IDComprobante == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                if (entity != null) {

                    FEFacturaElectronica fe = new FEFacturaElectronica();
                    FEComprobante comprobante = new FEComprobante();

                    comprobante.TipoComprobante = ComprobantesCommon.ObtenerTipoComprobante(entity.Tipo);

                    comprobante.Cuit = long.Parse(usu.CUIT);
                    comprobante.PtoVta = entity.PuntosDeVenta.Punto;
                    comprobante.Concepto = (FEConcepto)entity.TipoConcepto;
                    comprobante.Fecha = entity.FechaComprobante;
                    comprobante.FchServDesde = null;
                    comprobante.FchServHasta = null;
                    comprobante.FchVtoPago = entity.FechaVencimiento;
                    comprobante.CodigoMoneda = "PES";
                    comprobante.CotizacionMoneda = 1;

                    //Seteo los datos del facturante
                    comprobante.CondicionVenta = usu.CondicionIVA;
                    comprobante.Domicilio = usu.Domicilio;
                    comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;
                    comprobante.RazonSocial = usu.RazonSocial;
                    comprobante.Telefono = usu.Telefono;
                    comprobante.IIBB = usu.IIBB;
                    if (usu.CondicionIVA == "MO")
                        comprobante.CondicionIva = "Monotributista";
                    else if (usu.CondicionIVA == "RI")
                        comprobante.CondicionIva = "IVA Responsable Inscripto";
                    else if (usu.CondicionIVA == "EX")
                        comprobante.CondicionIva = "IVA Exento";
                    else
                        comprobante.CondicionIva = "Responsable No Inscripto";
                    comprobante.FechaInicioActividades = usu.FechaInicio.HasValue ? usu.FechaInicio.Value.ToString("dd/MM/yyyy") : "";

                    //Seteo los datos de la persona
                    comprobante.DocTipo = entity.Personas.TipoDocumento == "DNI" ? 96 : 80;
                    comprobante.DocNro = long.Parse(entity.Personas.NroDocumento.ToNumericOnly());
                    comprobante.CondicionVenta = entity.CondicionVenta;
                    comprobante.ClienteNombre = entity.Personas.RazonSocial;
                    if (entity.Personas.CondicionIva == "MO")
                        comprobante.ClienteCondiionIva = "Monotributista";
                    else if (entity.Personas.CondicionIva == "RI")
                        comprobante.ClienteCondiionIva = "Responsable Inscripto";
                    else if (entity.Personas.CondicionIva == "EX")
                        comprobante.ClienteCondiionIva = "Exento";
                    else
                        comprobante.ClienteCondiionIva = "Responsable No Inscripto";

                    comprobante.ClienteDomicilio = entity.Personas.Domicilio + " " + entity.Personas.PisoDepto;
                    comprobante.ClienteLocalidad = entity.Personas.Ciudades.Nombre + ", " + entity.Personas.Provincias.Nombre;
                    comprobante.Observaciones = entity.Observaciones;
                    comprobante.NumeroComprobante = entity.Numero;


                    //Seteo los datos de la factura
                    //comprobante.ImpTotal = 2;
                    comprobante.ImpTotConc = 0;
                    comprobante.ImpOpEx = 0;
                    comprobante.DetalleIva = new List<FERegistroIVA>();
                    comprobante.Tributos = new List<FERegistroTributo>();
                    comprobante.ItemsDetalle.Add(new FEItemDetalle() { Cantidad = 1, Descripcion = "Producto 1", Precio = 1, Codigo = "55422" });
                    comprobante.ItemsDetalle.Add(new FEItemDetalle() { Cantidad = 1, Descripcion = "Producto 2", Precio = 1, Codigo = "554223" });

                    //var pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["CertificadoAFIP"]);
                    var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.Template"]);
                    var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usu.IDUsuario + ".png");
                    fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, usu.MostrarBonificacionFc, true, pathMarcaAgua);

                    return usu.IDUsuario + "_prev.pdf";
                }
                else
                    throw new Exception("Comprobante inválido");
            }

    }

    [WebMethod(true)]
    public static string GenerarRemito(int id) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        using (var dbContext = new ACHEEntities()) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var entity = dbContext.Comprobantes.Where(x => x.IDComprobante == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            var fileNameRemito = entity.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_" + "R-" + entity.PuntosDeVenta.Punto.ToString("#0000") + "-" + entity.Numero.ToString("#00000000") + ".pdf";

            var pathRemito = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/remitos/" + fileNameRemito);
            if (System.IO.File.Exists(pathRemito))
                System.IO.File.Delete(pathRemito);

            if (!System.IO.File.Exists(pathRemito)) {
                ComprobanteCart.Retrieve().Items.Clear();
                var contador = 0;
                foreach (var item in entity.ComprobantesDetalle) {
                    contador++;
                    var idconcepto = (item.IDConcepto == null) ? null : item.IDConcepto;
                    var tipoConcepto = (item.IDConcepto == null) ? null : item.Conceptos.Tipo;
                    AgregarItem(contador, idconcepto, item.Concepto, item.Iva, item.PrecioUnitario, item.Bonificacion, item.Cantidad, tipoConcepto);
                }
                Common.GenerarRemito(usu, entity.IDPersona, entity.FechaComprobante.ToString("dd/MM/yyyy"), entity.IDPuntoVenta, entity.Numero.ToString("#00000000"), entity.CondicionesVentas.Nombre, usu.ObservacionesRemito, null, FETipoComprobante.REMITO, usu.MostrarProductosEnRemito);
            }
            return fileNameRemito;
        }

    }

    private static void AgregarItem(int id, int? idConcepto, string concepto, decimal iva, decimal precio, decimal bonif, decimal cantidad, string tipoConcepto) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        if (id != 0) {
            var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            ComprobanteCart.Retrieve().Items.Remove(aux);
        }

        var tra = new ComprobantesDetalleViewModel(ConfiguracionHelper.ObtenerCantidadDecimales());
        tra.ID = ComprobanteCart.Retrieve().Items.Count() + 1;
        tra.Concepto = concepto;
        tra.Iva = iva;
        tra.Bonificacion = bonif;
        tra.PrecioUnitario = precio;
        tra.Cantidad = cantidad;
        tra.IDConcepto = idConcepto;
        tra.TipoConcepto = tipoConcepto;
        ComprobanteCart.Retrieve().Items.Add(tra);

    }

    private static string GenerarPDF(Comprobantes comprobante) {
        if (!UsuarioCommon.VerificarWebUserSession())
            throw new CustomException("Por favor, vuelva a iniciar sesión");

        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
        var nroComprobante = comprobante.Numero.ToString("#00000000");
        ComprobanteCart.Retrieve().Items.Clear();
        var aux = 0;
        foreach (var item in comprobante.ComprobantesDetalle) {
            AgregarItem(0, item.IDConcepto, item.Concepto, item.Iva, item.PrecioUnitario, item.Bonificacion, item.Cantidad, "");
            aux++;
        }

        string afipObs = "";
        Common.CrearComprobante(usu, comprobante.IDComprobante, comprobante.IDPersona, comprobante.Tipo, comprobante.Modo, comprobante.FechaComprobante.ToString("dd/MM/yyy"), comprobante.CondicionesVentas.Nombre, comprobante.TipoConcepto, comprobante.FechaVencimiento.ToString("dd/MM/yyy"), comprobante.IDPuntoVenta, ref nroComprobante, comprobante.Observaciones, Common.ComprobanteModo.GenerarPDF, ref afipObs);
        var numero = comprobante.PuntosDeVenta.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
        var pathPdf = comprobante.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_" + comprobante.Tipo + "-" + numero + ".pdf";
        return pathPdf;
    }
}
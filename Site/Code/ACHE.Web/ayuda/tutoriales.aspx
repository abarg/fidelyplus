﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Front.master" AutoEventWireup="true" CodeFile="tutoriales.aspx.cs" Inherits="ayuda_tutoriales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .fm-title
        {
            font-size: 14px;
            min-height: 50px;
            
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="pageheader">
        <h2><i class="glyphicon glyphicon-question-sign"></i>Centro de ayuda <span>Manuales</span></h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Estás aquí:</span>
            <ol class="breadcrumb">
                <li><a href="/home.aspx"><i class="fa fa-home faHomeCustom"></i></a></li>
                <li class="active">Ayuda</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">

        <div class="row mb15" align="center">
            <div class="row filemanager" style="width:80%" >
                <a class="col-xs-6 col-sm-4 col-md-2" href="/ayuda/manuales/Guia-paso-a-paso-para-configurar-nuevo-punto-de-venta-AFIP.pdf" download>
                    <div class="thmb">
                        <div class="thmb-prev">
                            <img src="/images/filetypes/pdf.png" class="img-responsive" alt="" />
                        </div>
                        <h5 class="fm-title">Alta de punto de venta en AFIP</h5>
                    </div>  
                </a>
                <a class="col-xs-6 col-sm-4 col-md-2" href="/ayuda/manuales/Guia-paso-a-paso-para-configurar-FE-en-Contabilium.pdf" download>
                    <div class="thmb">
                        <div class="thmb-prev">
                            <img src="/images/filetypes/pdf.png" class="img-responsive" alt="" />
                        </div>
                        <h5 class="fm-title">Habilitar Factura Electrónica</h5>
                    </div>  
                </a>
                <a class="col-xs-6 col-sm-4 col-md-2" href="/ayuda/manuales/Guia-paso-a-paso-para-crear-una-venta.pdf" download>
                    <div class="thmb">
                        <div class="thmb-prev">
                            <img src="/images/filetypes/pdf.png" class="img-responsive" alt="" />
                        </div>
                        <h5 class="fm-title">Crear una factura, nota de débito o crédito de venta</h5>
                    </div>  
                </a>
                <a class="col-xs-6 col-sm-4 col-md-2" href="/ayuda/manuales/Guia-paso-a-paso-para-crear-una-venta.pdf" download>
                    <div class="thmb">
                        <div class="thmb-prev">
                            <img src="/images/filetypes/pdf.png" class="img-responsive" alt="" />
                        </div>
                        <h5 class="fm-title">Crear una cobranza</h5>
                    </div>  
                </a>
                <a class="col-xs-6 col-sm-4 col-md-2" href="/ayuda/manuales/Guia-paso-a-paso-para-carga-factura-compra.pdf" download>
                    <div class="thmb">
                        <div class="thmb-prev">
                            <img src="/images/filetypes/pdf.png" class="img-responsive" alt="" />
                        </div>
                        <h5 class="fm-title">Cargar factura, nota de débito o crédito de compra</h5>
                    </div>  
                </a>
                <a class="col-xs-6 col-sm-4 col-md-2" href="/ayuda/manuales/Guia-paso-a-paso-pago-de-una-factura-de-compra.pdf" download>
                    <div class="thmb">
                        <div class="thmb-prev">
                            <img src="/images/filetypes/pdf.png" class="img-responsive" alt="" />
                        </div>
                        <h5 class="fm-title">Registrar un pago</h5>
                    </div>  
                </a>
                <a class="col-xs-6 col-sm-4 col-md-2" href="/ayuda/manuales/Guia-paso-a-paso-creacion-de-abonos.pdf" download>
                    <div class="thmb">
                        <div class="thmb-prev">
                            <img src="/images/filetypes/pdf.png" class="img-responsive" alt="" />
                        </div>
                        <h5 class="fm-title">Cargar un abono</h5>
                    </div>  
                </a>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" Runat="Server">    
</asp:Content>


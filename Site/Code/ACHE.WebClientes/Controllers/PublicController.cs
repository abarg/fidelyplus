﻿using ACHE.Extensions;
using ACHE.MercadoPago;
using ACHE.Model;
using ACHE.Negocio.Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ACHE.WebClientes.Controllers
{
    [AllowAnonymous]
    public class PublicController : BaseController
    {
        public ActionResult Factura(string comprobante)
        {
            var model = new ComprobantesViewModel();
            using (var dbContext = new ACHEEntities())
            {
                //comprobante = Cryptography.Encrypt(comprobante, true);
                int idcomprobante = 0;

                if (string.IsNullOrEmpty(comprobante))
                    return RedirectToAction("Error", "Public");

                try
                {
                    comprobante = comprobante.ReplaceAll(" ", "+");
                    idcomprobante = int.Parse(Cryptography.Decrypt(comprobante, true));
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Public");
                }

                var entity = dbContext.Comprobantes.Include("PuntosDeVenta").Include("Personas").Include("ComprobantesDetalle")
                    .Include("Usuarios").Include("Usuarios.Integraciones")
                    .Where(x => x.IDComprobante == idcomprobante).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.FechaCAE.HasValue)
                    {

                        model.IDComprobante = entity.IDComprobante;
                        model.Tipo = entity.Tipo;
                        model.RazonSocial = entity.Personas.RazonSocial;
                        var urlApi = ConfigurationManager.AppSettings["Api.Path"];
                        var token = Common.generarToken(dbContext, entity.IDPersona);

                        model.RutaDescarga = urlApi + "/request/getFactura/" + token + "/" + entity.IDComprobante + "/" + entity.IDPersona;
                        PersonasViewModel u = new PersonasViewModel();
                        u.RazonSocial = entity.Usuarios.RazonSocial + " - " + entity.Usuarios.CUIT;
                        u.Domicilio = entity.Usuarios.Domicilio;
                        u.Provincia = entity.Usuarios.Provincias.Nombre;
                        u.Ciudad = entity.Usuarios.Ciudades.Nombre;
                        u.Telefono = entity.Usuarios.Telefono;
                        u.Email = entity.Usuarios.Email;
                        model.Usuario = u;
                        //Datos de la fc
                        model.Numero = entity.Tipo + " " + entity.PuntosDeVenta.Punto.ToString("#0000") + "-" + entity.Numero.ToString("#00000000");
                        PersonasViewModel p = new PersonasViewModel();
                        p.RazonSocial = entity.Personas.RazonSocial + " - " + entity.NroDocumento;
                        p.Domicilio = entity.Personas.Domicilio;
                        p.Provincia = entity.Personas.Provincias.Nombre;
                        p.Ciudad = entity.Personas.Ciudades.Nombre;
                        p.Email = entity.Personas.Email;
                        model.Fecha = entity.FechaComprobante.ToLongDateString();
                        model.FechaVencimiento = entity.FechaVencimiento;
                        p.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(entity.Personas.CondicionIva);
                        model.Persona = p;
                        var detalle = entity.ComprobantesDetalle.OrderBy(x => x.Concepto).Select(x => new ComprobantesDetalleViewModel(2)
                        {
                            ID = x.IDDetalle,
                            Codigo = (x.Conceptos == null) ? "" : x.Conceptos.Codigo,
                            Concepto = x.Concepto,
                            Cantidad = x.Cantidad,
                            PrecioUnitario = x.PrecioUnitario,
                            Bonificacion = x.Bonificacion,
                            Iva = x.Iva
                        });

                        model.Detalle = detalle.ToList();

                        model.ImporteTotalBruto = entity.ImporteTotalBruto.ToString();
                        model.Iva = (entity.ImporteTotalNeto - entity.ImporteTotalBruto);
                        model.ImporteTotalNeto = entity.ImporteTotalNeto.ToString();
                        model.Saldo = entity.Saldo;

                        if (entity.Observaciones != string.Empty)
                            model.Observaciones = entity.Observaciones;
                        else
                            model.Observaciones = "El comprobante no tiene observaciones";

                        //mercado pago
                        if (entity.Tipo != "NCA" && entity.Tipo != "NCB" && entity.Tipo != "NCC")
                        {

                            if (model.Saldo > 0)
                            {
                                API.ClientIDMercadoPago = entity.Usuarios.MercadoPagoClientID;
                                API.PasswordMercadoPago = entity.Usuarios.MercadoPagoClientSecret;
                                int planesPagos = dbContext.PlanesPagos.Where(x => x.IDUsuario == entity.IDUsuario && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).Select(x => x.IDPlan).FirstOrDefault();

                                if (!string.IsNullOrEmpty(API.ClientIDMercadoPago) && !string.IsNullOrEmpty(API.PasswordMercadoPago) && entity.Saldo > 0 && planesPagos >= 3)
                                {
                                    var factura = entity.Tipo + "-" + entity.PuntosDeVenta.Punto.ToString("#0000") + "-" + entity.Numero.ToString("#00000000");
                                    var mpRef = API.AddPreferenceClient(entity.IDComprobante.ToString() + "|" + entity.IDUsuario.ToString(), "Pago de factura " + factura, string.Empty, 1, entity.Saldo, API.Moneda.ARS, string.Empty, entity.Personas.Email, entity.Personas.Telefono, entity.Personas.RazonSocial, entity.Personas.NroDocumento, entity.Personas.Domicilio, "", entity.Personas.FechaAlta);

                                    model.RutaMercadoPago = mpRef;
                                }

                                //chequeo payu
                                var integraPayu = entity.Usuarios.Integraciones.FirstOrDefault(x => x.Tipo == "PayU" && !x.FechaEliminacion.HasValue);
                                if (integraPayu != null)
                                {
                                    model.PayUForm = new PayUForm();
                                    model.PayUForm.AccountId = integraPayu.ParametrosIntegracion.Where(x => x.Clave == "AccountId").First().Valor;//"512322";//
                                    model.PayUForm.MerchantId = integraPayu.ParametrosIntegracion.Where(x => x.Clave == "MerchantId").First().Valor;//"508029";//
                                    model.PayUForm.ReferenceCode = model.Numero + " | " + DateTime.Now.ToString("ddMMyyyyHHmmss");

                                    var apiKey = integraPayu.ParametrosIntegracion.Where(x => x.Clave == "ApiKey").First().Valor;

                                    //“ApiKey~merchantId~referenceCode~amount~currency”.
                                    var signature = apiKey + "~" + model.PayUForm.MerchantId + "~" + model.PayUForm.ReferenceCode + "~" + entity.Saldo.ToString("#0.00").Replace(",", ".") + "~ARS";
                                    var hashResult = MD5Hash(signature);

                                    model.PayUForm.Signature = hashResult;
                                }
                            }
                        }
                    }
                    else
                        return RedirectToAction("Error", "Public");
                }
                else
                    return RedirectToAction("Error", "Public");
            }
            return View(model);
        }

        public string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public ActionResult PagoFin(int estado)
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }


    }
}
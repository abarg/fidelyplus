﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ACHE.Model;
using ACHE.Extensions;
using ACHE.WebClientes.Models;
using System.Configuration;
using ACHE.MercadoPago;

namespace ACHE.WebClientes.Controllers
{
    public class HomeController : BaseController
    {

        private HtmlString integracionMercadoPago(Comprobantes comprobante)
        {
            var btnMP = string.Empty;
            try
            {
                //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["MP.LogError"]), "boton pagar entro ","");
                if (comprobante.Saldo > 0)
                {
                    if (comprobante.Tipo != "NCA" && comprobante.Tipo != "NCB" && comprobante.Tipo != "NCC")
                    {

                        API.ClientIDMercadoPago = comprobante.Usuarios.MercadoPagoClientID;
                        API.PasswordMercadoPago = comprobante.Usuarios.MercadoPagoClientSecret;

                        if (!string.IsNullOrEmpty(API.ClientIDMercadoPago) && !string.IsNullOrEmpty(API.PasswordMercadoPago))
                        {
                            var factura = comprobante.Tipo + "-" + comprobante.PuntosDeVenta.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
                            var mpRef = API.AddPreferenceClient(comprobante.IDComprobante.ToString() + "|" + comprobante.IDUsuario.ToString(), "Pago de la Factura: " + factura, string.Empty, 1, comprobante.Saldo, API.Moneda.ARS, string.Empty, comprobante.Personas.Email, comprobante.Personas.Telefono, comprobante.Personas.RazonSocial, comprobante.Personas.NroDocumento, comprobante.Personas.Domicilio, "", comprobante.Personas.FechaAlta);
                            //btnMP = "<a class='btn btn-success' id='lnkComprar' href='" + mpRef + "&payer.name=" + comprobante.Personas.RazonSocial + "&payer.surname=" + comprobante.Personas.RazonSocial + "&payer.email=" + comprobante.Personas.Email + "' name='MP-Checkout' mp-mode='modal' onreturn='pagoCompleto'>Pagar</a>";
                            btnMP = "<a class='btn btn-success' id='lnkComprar' href='" + mpRef + "' name='MP-Checkout' mp-mode='modal' onreturn='pagoCompleto'>Pagar</a>";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new HtmlString(btnMP);
        }

        public ActionResult Index()
        {
            var model = new FacturasViewModel();
            var token = User.Token;
            using (var dbContext = new ACHEEntities())
            {
                var pwd = dbContext.PersonasPwd.Where(x => x.IDPersonaPwd == User.IDPersonaPwd).FirstOrDefault();
                if (pwd != null)
                {
                    if (!pwd.CambioPwd)
                        return RedirectToAction("PrimerLogin", "Account");
                }

                var urlApi = ConfigurationManager.AppSettings["Api.Path"];

                //

                var listaAux = dbContext.Comprobantes.Include("Personas").Include("PuntosDeVenta")
                    .Where(x => x.NroDocumento == User.Documento && x.TipoDocumento == User.TipoDocumento && x.Usuarios.PortalClientes)
                    .OrderByDescending(x => x.FechaComprobante).ToList()
                    .Select(x => new FacturaViewModel()
                    {
                        IDUsuario = x.IDUsuario,
                        IDFactura = x.IDComprobante,
                        Emisor = x.Usuarios.RazonSocial,
                        NroComprobante = x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                        TipoComprobante = x.Tipo,
                        CAE = x.CAE,
                        FechaFacturacion = x.FechaComprobante,
                        Importe = x.ImporteTotalBruto,
                        Iva = x.ImporteTotalBruto - x.ImporteTotalNeto,
                        Total = x.ImporteTotalNeto,
                        Saldo = (x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC") ? x.Saldo : 0,
                        Modo = x.Modo,
                        Descargar = x.FechaCAE.HasValue ? urlApi + "/request/getFactura/" + token + "/" + x.IDComprobante + "/" + x.IDPersona : "",
                        BtnPago = integracionMercadoPago(x)
                    }).ToList();

                if (listaAux.Any())//Remuevo el boton de MP si NO tiene Plan Pyme o superior
                {
                    var dtHoy = DateTime.Now;
                    var idUsuario = listaAux[0].IDUsuario;

                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    if (usu.IDUsuarioPadre.HasValue)
                        idUsuario = usu.IDUsuarioPadre.Value;//se usa el id usuariopadre para validar

                    var planesPagos = dbContext.PlanesPagos.Where(x => x.IDUsuario == idUsuario && x.FechaInicioPlan <= dtHoy && x.FechaFinPlan >= dtHoy).Select(x => x.IDPlan).FirstOrDefault();
                    //if (auxPlan != null)
                    //{
                    //int planesPagos = auxPlan.IDPlan;
                    if (planesPagos < 3)
                    {
                        foreach (var item in listaAux)
                        {
                            item.BtnPago = new HtmlString("");
                        }
                    }
                    //}
                }


                model.listaFacturas = ValidarPlan(dbContext, listaAux);
            }
            return View(model);
        }

        private List<FacturaViewModel> ValidarPlan(ACHEEntities dbContext, List<FacturaViewModel> listAux)
        {
            List<int> listUsuarios = new List<int>();
            foreach (var fc in listAux)
            {
                var UsuAdicional = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == fc.IDUsuario).FirstOrDefault();
                var Empresa = dbContext.Usuarios.Where(x => x.IDUsuarioPadre != null && x.IDUsuario == fc.IDUsuario).FirstOrDefault();
                PlanesPagos planesPagos = new PlanesPagos();

                if (UsuAdicional != null)
                    planesPagos = dbContext.PlanesPagos.Where(x => x.IDUsuario == UsuAdicional.IDUsuario && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();
                else if (Empresa != null)
                    planesPagos = dbContext.PlanesPagos.Where(x => x.IDUsuario == Empresa.IDUsuarioPadre && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();
                else
                    planesPagos = dbContext.PlanesPagos.Where(x => x.IDUsuario == fc.IDUsuario && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now).FirstOrDefault();

                if (planesPagos != null && planesPagos.FechaFinPlan >= DateTime.Now.Date && planesPagos.IDPlan < 3)//Remuevo las fc de usuarios del plan pyme para abajo
                    listUsuarios.Add(fc.IDUsuario);
            }

            foreach (var item in listUsuarios)
                listAux.RemoveAll(x => x.IDUsuario == item);

            return listAux;
        }
    }
}
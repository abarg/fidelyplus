﻿using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;
using ACHE.Negocio.Presupuesto;
using ACHE.Negocio.Productos;
using ACHE.WebClientes.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Services;

namespace ACHE.WebClientes.Controllers
{
    public class PedidosController : BaseController
    {
        public static int IDUsuarioSeleccionado;

        //
        // GET: /Pedidos/
        public ActionResult Index()
        {
            using (var dbContext = new ACHEEntities())
            {
                var persona = dbContext.Personas.Where(x => x.NroDocumento == User.Documento && x.TipoDocumento == User.TipoDocumento && x.Usuarios.Activo).ToList();
                if (persona.Count > 1 && (IDUsuarioSeleccionado == null || IDUsuarioSeleccionado == 0))
                    return RedirectToAction("SeleccionarUsuario", "Pedidos");
                else if (persona.Count == 1)
                {
                    IDUsuarioSeleccionado = dbContext.Personas.Where(x => x.NroDocumento == User.Documento && x.TipoDocumento == User.TipoDocumento && x.Usuarios.Activo).FirstOrDefault().IDUsuario;
                }
                @ViewBag.IDUsuario = IDUsuarioSeleccionado;
            }
            return View();
        }

        [HttpPost]
        public ActionResult getResults(string concepto, string idRubro, string idSubrubro, int page, int pageSize)
        {
            using (var dbContext = new ACHEEntities())
            {

                var persona = dbContext.Personas.Where(x => x.NroDocumento == User.Documento && x.TipoDocumento == User.TipoDocumento && x.IDUsuario == IDUsuarioSeleccionado).FirstOrDefault();
                var aux = ConceptosCommon.ObtenerConceptosPorPersona(concepto, idRubro, idSubrubro, page, pageSize, IDUsuarioSeleccionado, persona.IDPersona);
                var urlWeb = ConfigurationManager.AppSettings["Web.Path"];

                foreach (var item in aux.Items)
                {
                    item.Nombre = (item.Nombre != null && item.Nombre != string.Empty ? item.Nombre.Length > 68 ? System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.Nombre.Substring(0, 68).ToLower()) + "..." : System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.Nombre.ToLower()) : string.Empty);
                    if (!string.IsNullOrEmpty(item.Foto))
                        item.Foto = urlWeb + "/files/explorer/" + IDUsuarioSeleccionado + "/Productos-Servicios/" + item.Foto;
                    else
                        item.Foto = urlWeb + "/files/explorer/no-photo.png";

                }
                return Json(aux, JsonRequestBehavior.AllowGet);

            }
            //   return Json(result, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        public JsonResult AddItem(int id, int cantidad, decimal precio)
        {
            var dto = new ErrorViewModel();
            try
            {
                ShoppingCart.RetrieveShoppingCart().AddItem(id, cantidad, precio);
                dto.TieneError = false;
            }
            catch (Exception ex)
            {
                dto.TieneError = true;
                dto.Mensaje = ex.Message;
            }
            return Json(new { success = true, result = dto }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckOut()
        {
            PedidoViewModel model = new PedidoViewModel();

            if (ShoppingCart.RetrieveShoppingCart().Conceptos != null)
            {
                if (ShoppingCart.RetrieveShoppingCart().Conceptos.Count() > 0)
                    model.Conceptos = ShoppingCart.RetrieveShoppingCart().Conceptos;
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult RemoveItem(int id)
        {
            var dto = new ErrorViewModel();
            try
            {
                ShoppingCart.RetrieveShoppingCart().RemoveItem(id);
                dto.TieneError = false;
            }
            catch (Exception ex)
            {
                dto.TieneError = true;
                dto.Mensaje = ex.Message;
            }
            return Json(new { success = true, result = dto }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Confirmar()
        {
            var dto = new ErrorViewModel();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var persona = dbContext.Personas.Where(x => x.NroDocumento == User.Documento && x.TipoDocumento == User.TipoDocumento && x.IDUsuario == IDUsuarioSeleccionado).FirstOrDefault();
                    PresupuestoCartDto comprobanteCart = new PresupuestoCartDto();
                    comprobanteCart.Items = ShoppingCart.RetrieveShoppingCart().Conceptos;

                    comprobanteCart.IDPresupuesto = 0;
                    comprobanteCart.IDPersona = persona.IDPersona;
                    comprobanteCart.Fecha = DateTime.Now.ToString("dd/MM/yyyy");
                    comprobanteCart.FechaValidez = DateTime.Now.AddDays(15).ToString("dd/MM/yyyy");
                    comprobanteCart.Nombre = "";
                    var numero = dbContext.Presupuestos.Where(x =>/* x.IDPersona == persona.IDPersona &&*/ x.IDUsuario == persona.IDUsuario).OrderByDescending(x => x.Numero).Select(x => x.Numero).FirstOrDefault();
                    if (numero == null)
                        comprobanteCart.Numero = 1;
                    else
                        comprobanteCart.Numero = numero + 1;
                    numero = comprobanteCart.Numero;
                    comprobanteCart.CondicionesPago = "Cuenta corriente";
                    comprobanteCart.Observaciones = "";
                    comprobanteCart.Estado = "B";

                    var id = PresupuestosCommon.GuardarPresupuesto(comprobanteCart, persona.IDUsuario, persona.Usuarios.EmailAlertas);
                    if (id > 0)
                    {

                        ListDictionary data = new ListDictionary();
                        data.Add("<USUARIO>", persona.Usuarios.RazonSocial);
                        data.Add("<NOTIFICACION>", "Ha recibido el pedido #" + numero.ToString("#00000000") + " desde el acceso clientes");
                        bool send = EmailCommon.SendMessage(EmailTemplate.Notificacion.ToString(), data, persona.Usuarios.EmailAlertas, "Nuevo pedido recibido desde Contabilium");
                        dto.Mensaje = numero.ToString();
                        ShoppingCart.RetrieveShoppingCart().Conceptos.Clear();
                        Session["ASPNETShoppingCart"] = null;
                    }

                }
                dto.TieneError = false;
            }
            catch (Exception ex)
            {
                dto.TieneError = true;
                dto.Mensaje = ex.Message;
            }
            return Json(new { success = true, result = dto }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateItem(int id, int cantidad)
        {
            var dto = new ErrorViewModel();
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    ShoppingCart.RetrieveShoppingCart().SetItemQuantity(id, cantidad);//1, idTalle2);

                }
                dto.TieneError = false;
            }
            catch (Exception ex)
            {
                dto.TieneError = true;
                dto.Mensaje = ex.Message;
            }
            return Json(new { success = true, result = dto }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PedidoFin(int numero)
        {
            ViewBag.Numero = numero;
            return View();
        }

        [HttpPost]
        public JsonResult GetTotales()
        {
            var dto = new ErrorViewModel();
            try
            {

                if (ShoppingCart.RetrieveShoppingCart().Conceptos != null)
                {
                    if (ShoppingCart.RetrieveShoppingCart().Conceptos.Count() > 0)
                        dto.Mensaje = ShoppingCart.RetrieveShoppingCart().GetSubTotal().ToString("N2") + "#" + ShoppingCart.RetrieveShoppingCart().GetTotal().ToString("N2");
                }
                dto.TieneError = false;
            }
            catch (Exception ex)
            {
                dto.TieneError = true;
                dto.Mensaje = ex.Message;
            }
            return Json(new { success = true, result = dto }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SeleccionarUsuario()
        {
            ResultadosUsuariosViewModel model = new ResultadosUsuariosViewModel();
            using (var dbContext = new ACHEEntities())
            {

                var list = dbContext.Personas.Where(x => x.NroDocumento == User.Documento && x.TipoDocumento == User.TipoDocumento)
                    .Select(x => new UsuariosViewModel()
                    {
                        Activo = x.Usuarios.Activo ? "SI" : "NO",
                        Email = x.Usuarios.EmailAlertas,
                        ID = x.IDUsuario,
                        Tipo = x.Tipo,
                        RazonSocial = x.Usuarios.RazonSocial
                    }).OrderBy(x => x.RazonSocial).ToList();

                var listFinal = new List<UsuariosViewModel>();
                foreach (var item in list)
                {
                    if (!listFinal.Any(x => x.ID == item.ID))
                        listFinal.Add(item);
                }
                model.Items = listFinal;
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult GuardarUsuario(int id)
        {
            var dto = new ErrorViewModel();
            IDUsuarioSeleccionado = id;
            ShoppingCart.RetrieveShoppingCart().Conceptos.Clear();
            Session["ASPNETShoppingCart"] = null;
            return Json(new { success = true, result = dto }, JsonRequestBehavior.AllowGet);
        }

    }
}
﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.WebClientes.Models
{
    /*public class CartItem
    {
        public int IDProducto { get; set; }
 
        public string Nombre { get; set; }
        public string Codigo { get; set; }
   
        public int Cantidad { get; set; }
        public decimal Iva { get; set; }
  
        public decimal StockActual { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string Imagen { get; set; }
        public decimal TotalNeto
        {
            get { return Cantidad *( PrecioUnitario+(PrecioUnitario *Iva /100)); }

        }
        public decimal Total
        {
            get { return Cantidad * PrecioUnitario; }
        }
        public CartItem(int productId, int cantidad, decimal precio)//, int idTalle2)
        {
            this.IDProducto = productId;
            this.Cantidad = cantidad;
            this.PrecioUnitario = precio;

        }
    }*/

    public class ShoppingCart
    {
        #region Properties
        public List<ComprobantesDetalleViewModel> Conceptos { get; set; }
        public int IDPedido { get; set; }
        public int IDUsuario { get; set; }
        public string ObservacionesEntrega { get; set; }
        public decimal Total { get; set; }
        #endregion

        public static ShoppingCart Instance;

        public static ShoppingCart RetrieveShoppingCart()
        {
            if (HttpContext.Current.Session["ASPNETShoppingCart"] == null)
            {
                Instance = new ShoppingCart();
                Instance.Conceptos = new List<ComprobantesDetalleViewModel>();
                HttpContext.Current.Session["ASPNETShoppingCart"] = Instance;
            }
            else
                Instance = (ShoppingCart)HttpContext.Current.Session["ASPNETShoppingCart"];
            return Instance;
        }

        protected ShoppingCart() { }

        #region Item Modification Methods

        public void AddItem(int productId, int cantidad, decimal precio)
        {
            if (ShoppingCart.Instance.Conceptos == null)
                ShoppingCart.Instance.Conceptos = new List<ComprobantesDetalleViewModel>();
            var items = ShoppingCart.Instance.Conceptos;
            if (items.Exists(x => x.IDConcepto == productId))
            {
                foreach (ComprobantesDetalleViewModel item in items)
                {
                    if (item.IDConcepto == productId)
                    {
                        item.Cantidad = cantidad;
                        return;
                    }
                }
            }
            else
            {
                ComprobantesDetalleViewModel newItem = new ComprobantesDetalleViewModel(2);
                using (var dbContext = new ACHEEntities())
                {
                    var prod = dbContext.Conceptos.Where(x => x.IDConcepto == productId).First();
                    newItem.Concepto = prod.Nombre;
                    newItem.PrecioUnitario = precio;
                    newItem.Iva = prod.Iva;
                    newItem.IDConcepto = productId;
                    newItem.Cantidad = cantidad;
                    newItem.Bonificacion = 0;
                    newItem.Codigo = prod.Codigo;
                    

                    newItem.Codigo = prod.Codigo;

                    newItem.Imagen = "/files/productos/" + prod.Foto;
                }
                ShoppingCart.Instance.Conceptos.Add(newItem);
            }
        }

        public void SetItemQuantity(int productId, int quantity)
        {
            foreach (ComprobantesDetalleViewModel item in ShoppingCart.RetrieveShoppingCart().Conceptos)
            {
                if (item.IDConcepto == productId)
                {
                    item.Cantidad = quantity;
                    return;
                }
            }
        }

        public void RemoveItem(int productId)
        {
            var removedItem = ShoppingCart.RetrieveShoppingCart().Conceptos.Where(x => x.IDConcepto == productId).First();
            ShoppingCart.RetrieveShoppingCart().Conceptos.Remove(removedItem);
        }
        #endregion Item Modification Methods

        #region Reporting Methods
        public decimal GetSubTotal()
        {
            decimal subTotal = 0;
            foreach (ComprobantesDetalleViewModel item in ShoppingCart.RetrieveShoppingCart().Conceptos)
                subTotal += item.TotalSinIva;
            return subTotal;
        }

        public decimal GetTotalProducto(int productId)
        {
            return ShoppingCart.RetrieveShoppingCart().Conceptos.Where(x => x.IDConcepto == productId).First().TotalConIva;
        }

        public decimal GetTotal()
        {
            decimal total = 0;
            foreach (ComprobantesDetalleViewModel item in ShoppingCart.RetrieveShoppingCart().Conceptos)
                total += item.TotalConIva;
            return total;
        }

        public decimal GetCantidadTotal()
        {
            return ShoppingCart.RetrieveShoppingCart().Conceptos.Sum(x => x.Cantidad);
        }
        #endregion Reporting Methods
    }

    
}
﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.WebClientes.Models
{
    public class PedidoViewModel
    {
        public List<ComprobantesDetalleViewModel> Conceptos { get; set; }
    }
    public class ConceptoViewModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public string CodigoOem { get; set; }
        public string CodigoBarras { get; set; }
        public string Descripcion { get; set; }
        public string Precio { get; set; }
        public string Stock { get; set; }
        public string Estado { get; set; }
        public string Iva { get; set; }
        public string Tipo { get; set; }
        public int idPlanDeCuentaCompra { get; set; }
        public int idPlanDeCuentaVenta { get; set; }

        public string CostoInterno { get; set; }
    }
    public class ResultadosConceptosViewModel
    {
        public IList<ConceptoViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}
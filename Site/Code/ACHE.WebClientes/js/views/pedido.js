﻿var PAGE_SIZE = 50;
var pedido = {
    /*** SEARCH ***/
    configFilters: function () {
        pedido.obtenerRubros("ddlRubros", true);
        pedido.obtenerSubRubros("", "ddlSubRubros");
        $(".select2").select2({ width: '100%', allowClear: true });
        $("#txtConcepto, #ddlRubros, #ddlSubRubros").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                pedido.resetearPagina();
                pedido.filtrar();
                return false;
            }
        });
        pedido.filtrar();
    },
    mostrarPagAnterior: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual--;
        $("#hdnPage").val(paginaActual);
        pedido.filtrar();
    },
    mostrarPagProxima: function () {
        var paginaActual = parseInt($("#hdnPage").val());
        paginaActual++;
        $("#hdnPage").val(paginaActual);
        pedido.filtrar();
    },
    filtrar: function () {
        $("#divError").hide();
        if ($('#frmSearch').valid()) {
            $("#resultsContainer").html("");

            var currentPage = parseInt($("#hdnPage").val());

            var info = "{ concepto: '" + $("#txtConcepto").val()
                       + "', idRubro: '" + $("#ddlRubros").val()
                       + "', idSubrubro: '" + $("#ddlSubRubros").val()
                       + "', page: " + currentPage + ", pageSize: " + PAGE_SIZE
                       + "}";


            $.ajax({
                type: "POST",
                url:  "/Pedidos/getResults",
                data: info,
              //  data: JSON.stringify(info),
                contentType: "application/json; charset=utf-8",
                dataType: "json",

                success: function (data) {
                    $("#resultsContainer").empty();

                    if (data.TotalPage > 0) {
                        $("#divPagination").show();

                        $("#lnkNextPage, #lnkPrevPage").removeAttr('disabled')
                        if (data.TotalPage == 1)
                            $("#lnkNextPage, #lnkPrevPage").attr('disabled', "disabled")
                        else if (currentPage == data.TotalPage)
                            $("#lnkNextPage").attr("disabled", "disabled");
                        else if (currentPage == 1)
                            $("#lnkPrevPage").attr("disabled", "disabled");

                        var aux = (currentPage * PAGE_SIZE);
                        if (aux > data.TotalItems)
                            aux = data.TotalItems;
                        $("#msjResultados").html("Mostrando " + ((currentPage * PAGE_SIZE) - PAGE_SIZE + 1) + " - " + aux + " de " + data.TotalItems);
                    }
                    else {
                        $("#divPagination").hide();
                        $("#msjResultados").html("");
                    }

                    // Render using the template
                    if (data.Items.length > 0)
                        $("#resultTemplate").tmpl({ results: data.Items }).appendTo("#resultsContainer");
                    else
                        $("#noResultTemplate").tmpl({ results: data.Items }).appendTo("#resultsContainer");
                },
                error: function (response) {
                    var r = jQuery.parseJSON(response.responseText);
                    alert(r.Message);
                }
            });
        }
    },
    verTodos: function () {
        $("#txtConcepto").val("");
        $("#ddlRubros").val("").trigger("change");
        $("#ddlSubRubros").val("").trigger("change");
        pedido.filtrar();
    },
    resetearExportacion: function () {
        $("#imgLoading, #lnkDownload").hide();
        $("#divIconoDescargar").show();
    },
    resetearPagina: function () {
        $("#hdnPage").val("1");
    },
    obtenerRubros: function (controlName, showEmpty) {
        $.ajax({
            type: "POST",
            url: "/common.aspx/obtenerRubros",
            data: "{ idUsuario :'" + $("#hdnIDUsuario").val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data != null) {

                    $("#" + controlName).html("");
                    if (showEmpty)
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

                    for (var i = 0; i < data.d.length; i++) {
                        $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                    }



                }
            }
        });
    },
    obtenerSubRubros: function (idPadre, controlName) {
        if (idPadre != "" && idPadre != "0") {
            $.ajax({
                type: "POST",
                url: "/common.aspx/obtenerSubRubros",
                data: "{ idPadre :" + idPadre + ",idUsuario: " + $("#hdnIDUsuario").val() + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data != null) {
                        $("#" + controlName).html("");
                        $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));


                        for (var i = 0; i < data.d.length; i++) {
                            $("<option/>").attr("value", data.d[i].ID).text(data.d[i].Nombre).appendTo($("#" + controlName));
                        }


                    }
                }
            });
        } else {
            $("#" + controlName).html("");
            $("<option/>").attr("value", "").text("").appendTo($("#" + controlName));

        }
    },
    agregar: function (idConcepto, precio) {
        var cantidad = $("#txtCantidad-" + idConcepto).val();
        precio = precio.replace(".", "");
        precio = precio.replace(",", ".");

        if (cantidad == 0) {
            alert("Debe ingresar una cantidad");
        } else {            var info = "{ id: " + parseInt(idConcepto) + ", cantidad: " + parseInt(cantidad) + ",precio:" + precio + "}"
            $.ajax({
                type: "POST",
                async: false,
                url: '/Pedidos/AddItem',
                data: info,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, text) {
                    pedido.getSubTotal();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + ":" + thrownError);
                    $("#lnkComprar").attr("disabled", false);
                    $("#imgLoading").hide();
                    return false;
                }
            });

            $("#lbl-" + idConcepto).show().delay(1000).fadeOut();;
            
        }
    },
    remove: function (idProducto, index) {
        $.ajax({
            type: "POST",
            url:  '/Pedidos/RemoveItem',
            data: "{ id: " + idProducto + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {

                $("#tr_" + index).remove();
              
                var rowCount = 0;
                $("tbody").after(function () {
                    rowCount = $('tr', this).length;
                });          
                if ((rowCount) == 1) {
                    $("#divContinuarCarrito").hide();
                    $("#divSinElementos").show();
                } else
                    pedido.UpdateTotal();



            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });

    },
    confirmar: function () {
        $.ajax({
            type: "POST",
            url: '/Pedidos/Confirmar',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/Pedidos/PedidoFin?Numero=" + data.result.Mensaje;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status + ":" + thrownError);
            }
        });
    },
    update: function (idProducto, index, iva) {
        var cantidad = $.trim($("#txtCantidad_" + index).val());
        $.ajax({
            type: "POST",
            url: '/Pedidos/UpdateItem',
            data: "{ id: " + idProducto + ",cantidad:"+cantidad+"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                var precio = $("#spnPrecio_" + index).html().replace(".", "").replace(",", ".");
                var ivaDecimal = iva.replace(".", "").replace(",", ".");
                precioConIva = parseFloat(precio) + ((parseFloat(precio) * parseFloat(iva)) / 100);
                $("#spnSubtotal_" + index).html((cantidad * parseFloat(precio)).toFixed(2).replace(".", ","));
    
                $("#spnSubtotalNeto_" + index).html((cantidad * parseFloat(precioConIva)).toFixed(2).replace(".", ","));
                pedido.UpdateTotal();

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });

    },
    UpdateTotal: function () {
        $.ajax({
            type: "POST",
            url:  '/Pedidos/GetTotales',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.result.Mensaje != null) {
                    var total = data.result.Mensaje;
                    $("#spnSubTotal").html("$" + total.split("#")[0]);
                    $("#spnTotal").html("$" + total.split("#")[1]);
                } else {
                    $("#divContinuarCarrito").hide();
                    $("#divSinElementos").show();
                }
            },
            error: function (data) {
                alert(data.Message + "\n" + data.StackTrace);
            }
        });
    },
    guardarUsuario: function () {
      
        $.ajax({
            type: "POST",
            url: '/Pedidos/GuardarUsuario',
            data: "{ id: " + $("#ddlUsuario").val() + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, text) {
                window.location.href = "/Pedidos/Index";


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ":" + thrownError);
            }
        });

    },
    getSubTotal: function () {
        $.ajax({
            type: "POST",
            url: '/Pedidos/GetTotales',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.result.Mensaje != null) {
                    var total = data.result.Mensaje;
                    $("#spnTotal").html("Total: $" + total.split("#")[0]);
                 //   $("#spnTotal").html("$" + total.split("#")[1]);
                } 
                
            },
            error: function (data) {
                alert(data.Message + "\n" + data.StackTrace);
            }
        });
    }
}
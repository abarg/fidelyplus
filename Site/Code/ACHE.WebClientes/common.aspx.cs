﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ACHE.WebClientes
{
    public partial class common : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod(true)]
        public static List<Combo2ViewModel> obtenerSubRubros(int idPadre,string idUsuario)
        {
           
                using (var dbContext = new ACHEEntities())
                {         
                  //  var persona = dbContext.Personas.Where(x => x.NroDocumento == "" && x.TipoDocumento == "").FirstOrDefault();
                    int id = int.Parse(idUsuario);

                    return dbContext.Rubros.Where(x => x.IDUsuario == id && x.IDRubroPadre == idPadre).OrderBy(x => x.Nombre)
                        .Select(x => new Combo2ViewModel()
                        {
                            ID = x.IDRubro,
                            Nombre = x.Nombre
                        }).ToList();
                }
           
            
        }
        [WebMethod(true)]
        public static List<Combo2ViewModel> obtenerRubros(string idUsuario)
        {
            
                using (var dbContext = new ACHEEntities())
                {
                   int id = int.Parse(idUsuario);

                   return dbContext.Rubros.Where(x => x.IDUsuario == id && !x.IDRubroPadre.HasValue).OrderBy(x => x.Nombre)
                        .Select(x => new Combo2ViewModel()
                        {
                            ID = x.IDRubro,
                            Nombre = x.Nombre
                        }).ToList();
                }
            
        }
    }

}
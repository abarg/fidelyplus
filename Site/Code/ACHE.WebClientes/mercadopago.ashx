﻿<%@ WebHandler Language="C#" Class="mercadopago" %>

using ACHE.WebClientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Model;
using ACHE.Extensions;
using System.Web.Security;
using System.Collections.Specialized;
using ACHE.MercadoPago;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;

public class mercadopago : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.QueryString["id"] != "null" && context.Request.QueryString["token"] != "null")
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    string idMP = context.Request.QueryString["id"];
                    string token = context.Request.QueryString["token"];
                    string log = "idMP : " + idMP + "token: " + token;
                    log = (log == null) ? "" : log;
                    BasicLogClientes.AppendToFile(HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["MP.LogError"]), "parametros de entrada ", log);

                    var usu = dbContext.Usuarios.Where(x => x.ApiKey == token).FirstOrDefault();
                    if (usu != null)
                    {
                        API.ClientIDMercadoPago = usu.MercadoPagoClientID;
                        API.PasswordMercadoPago = usu.MercadoPagoClientSecret;

                        if (API.ClientIDMercadoPago != null || API.PasswordMercadoPago != null)
                        {
                            string infoPago = API.GetPago(idMP);
                            var dynamicObject = System.Web.Helpers.Json.Decode(infoPago);

                            var referencia = dynamicObject.collection.external_reference.ToString();
                            if (!referencia.Contains("-") && !referencia.Contains("_"))
                            {
                                string[] refAux = referencia.Split('|');//viene como IDComprobante|IDUsuario
                                if (refAux.Length > 1)
                                    referencia = refAux[0];
                                int idComprobante = int.Parse(referencia);
                                if (idComprobante > 0)
                                {
                                    //Valido que el id sea del usuario por si agregaron la integracion de MSHOPS ya que nos informan todos los pagos.
                                    var comprobante = dbContext.Comprobantes
                                        .Include("Usuarios").Include("Personas")
                                        .Where(x => x.IDComprobante == idComprobante && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                                    if (comprobante != null)
                                    {
                                        var usuarioMP = comprobante.Usuarios;

                                        var MetodoDePago = dynamicObject.collection.payment_type;
                                        var EstadoMP = dynamicObject.collection.status;

                                        if (EstadoMP == "approved")
                                        {
                                            var IDCobranza = guardarCobranza(comprobante, dbContext, idMP, MetodoDePago, EstadoMP);
                                            if (TienePlan(dbContext, usu.IDUsuario))
                                                EnviarEmail(comprobante, usu, IDCobranza);
                                        }
                                        //}
                                        //else
                                        //{
                                        //    string logCliente = "idMP : " + idMP + "token: " + token + ". IDUsuario=" + usuarioMP.IDUsuario + ".IDComprobante=" + idComprobante;
                                        //    BasicLogClientes.AppendToFile(HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["MP.LogError"]), "Error pago recibido", logCliente);
                                        //}
                                    }
                                }
                            }
                        }
                        else
                        {
                            BasicLogClientes.AppendToFile(HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["MP.LogError"]), "no se encontro clientID o ClientSecret ", "no se encotro clientID o ClientSecret ");
                        }
                    }
                    else
                    {
                        BasicLogClientes.AppendToFile(HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["MP.LogError"]), "Token Incorrecto", "Token Incorrecto");
                    }
                }
                context.Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                BasicLogClientes.AppendToFile(HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["MP.LogError"]), "Error procesar el pago ", msg);
            }
        }
    }

    public static bool TienePlan(ACHEEntities dbContext, int idUsuario)
    {
        var plan = new PlanesPagos();
        var usu = dbContext.UsuariosView.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
        var Empresa = dbContext.Usuarios.Where(x => x.IDUsuarioPadre != null && x.IDUsuario == idUsuario).FirstOrDefault();

        if (usu.IDUsuarioAdicional > 0)
            plan = dbContext.PlanesPagos.Where(x => x.IDUsuario == usu.IDUsuario && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now && x.Estado == "Aceptado").FirstOrDefault();
        else if (Empresa != null)
            plan = dbContext.PlanesPagos.Where(x => x.IDUsuario == Empresa.IDUsuarioPadre && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now && x.Estado == "Aceptado").FirstOrDefault();
        else
            plan = dbContext.PlanesPagos.Where(x => x.IDUsuario == idUsuario && x.FechaInicioPlan <= DateTime.Now && x.FechaFinPlan >= DateTime.Now && x.Estado == "Aceptado").FirstOrDefault();

        if (plan.IDPlan >= 3)//Plan PYME
            return true;
        else
            return false;
    }

    private static int guardarCobranza(Comprobantes comprobante, ACHEEntities dbContext, string idMP, string MetodoDePago, string EstadoMP)
    {
        var punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == comprobante.IDPuntoVenta).FirstOrDefault();
        WebUser usu = ACHE.Negocio.Common.TokenCommon.ObtenerWebUser(comprobante.IDUsuario);

        CobranzaCartDto cobrCartdto = new CobranzaCartDto();
        cobrCartdto.IDCobranza = 0;
        cobrCartdto.IDPersona = comprobante.IDPersona;
        cobrCartdto.Tipo = "RC";
        cobrCartdto.Fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
        cobrCartdto.IDPuntoVenta = comprobante.IDPuntoVenta;
        cobrCartdto.NumeroCobranza = CobranzasCommon.obtenerProxNroCobranza(cobrCartdto.Tipo, comprobante.IDUsuario);
        cobrCartdto.Observaciones = "Comprobante generado automáticamente desde cliente, por una cobranza de MercadoPago - Numero de Ref: " + idMP;

        //Determinamos la cantidad de decimales configurado para el usuario
        //var cantidadDecimales = 2; ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

        CobranzasDetalleViewModel item = new CobranzasDetalleViewModel(2);
        cobrCartdto.Items = new List<CobranzasDetalleViewModel>();
        item.ID = 1;
        item.Comprobante = comprobante.Tipo + " " + punto.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
        item.Importe = comprobante.Saldo;
        item.IDComprobante = comprobante.IDComprobante;
        cobrCartdto.Items.Add(item);

        //var auxFormaDepago = MetodoDePago.ToLower();
        //if (auxFormaDepago == "credit_card")
        //    auxFormaDepago = "Tarjeta de credito";
        //else if (auxFormaDepago == "debit_card")
        //    auxFormaDepago = "Tarjeta de debito";
        //else if (auxFormaDepago == "ticket")
        //    auxFormaDepago = "Ticket";
        //else
        //    auxFormaDepago = "Transferencia";

        CobranzasFormasDePagoViewModel formasDePago = new CobranzasFormasDePagoViewModel();
        cobrCartdto.FormasDePago = new List<CobranzasFormasDePagoViewModel>();
        formasDePago.ID = 1;
        formasDePago.Importe = comprobante.Saldo;
        formasDePago.FormaDePago = "Transferencia";
        formasDePago.IDBanco = dbContext.Bancos.Where(x => x.IDUsuario == comprobante.IDUsuario && x.IDBancoBase == 80).First().IDBanco;
        formasDePago.NroReferencia = idMP;
        cobrCartdto.FormasDePago.Add(formasDePago);
        cobrCartdto.Retenciones = new List<CobranzasRetencionesViewModel>();

        var cobranza = CobranzasCommon.Guardar(dbContext, cobrCartdto, usu, false);
        generarAlertas(dbContext, cobranza);

        return cobranza.IDCobranza;
    }

    //todo: pasar a commmon
    private static void generarAlertas(ACHEEntities dbContext, Cobranzas cobranzas)
    {
        var listaAlertas = dbContext.Alertas.Where(x => x.IDUsuario == cobranzas.IDUsuario).ToList();

        foreach (var alertas in listaAlertas)
        {
            if (alertas.AvisoAlerta == "El cobro a un cliente es")
            {
                switch (alertas.Condicion)
                {
                    case "Mayor o igual que":
                        if (cobranzas.ImporteTotal >= alertas.Importe)
                        {
                            insertarAlerta(dbContext, cobranzas, alertas);
                        }
                        break;
                    case "Menor o igual que":
                        if (cobranzas.ImporteTotal <= alertas.Importe)
                        {
                            insertarAlerta(dbContext, cobranzas, alertas);
                        }
                        break;
                }
            }
        }
    }

    private static void insertarAlerta(ACHEEntities dbContext, Cobranzas cobranzas, Alertas alertas)
    {

        AlertasGeneradas entity = new AlertasGeneradas();

        entity.IDAlerta = alertas.IDAlerta;
        entity.IDUsuario = cobranzas.IDUsuario;
        entity.IDPersona = cobranzas.IDPersona;

        entity.ImportePagado = cobranzas.ImporteTotal;
        entity.Visible = true;
        entity.Fecha = DateTime.Now.Date;
        entity.IDCobranzas = cobranzas.IDCobranza;

        entity.NroComprobante = "MercadoPago: Se cobró el Comprobante: ";
        foreach (var item in cobranzas.CobranzasDetalle)
        {
            var comprobante = dbContext.Comprobantes.Where(x => x.IDComprobante == item.IDComprobante).FirstOrDefault();
            entity.NroComprobante += comprobante.Tipo + " " + comprobante.PuntosDeVenta.Punto.ToString("#0000") + "-" + cobranzas.Numero.ToString("#00000000") + " ; ";
        }

        entity.NroComprobante = entity.NroComprobante.Substring(0, entity.NroComprobante.Length - 3);
        entity.NroComprobante += ".";

        dbContext.AlertasGeneradas.Add(entity);
        dbContext.SaveChanges();

        var alerta = alertas.AvisoAlerta + " - " + alertas.Condicion + " - $" + alertas.Importe;
        var descripcion = entity.NroComprobante;
    }

    private static void EnviarEmail(Comprobantes comprobante, Usuarios usu, int idCobranza)
    {
        var factura = comprobante.Tipo + "-" + comprobante.PuntosDeVenta.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
        var persona = comprobante.Personas;
        var cliente = persona.RazonSocial; 
        if(!string.IsNullOrEmpty(persona.NombreFantansia))
            cliente = persona.NombreFantansia; 
        
        ListDictionary replacements = new ListDictionary();
        replacements.Add("<USUARIO>", comprobante.Usuarios.RazonSocial);
        replacements.Add("<NOTIFICACION>", "Hemos recibido el pago de la factura " + factura + " desde el acceso clientes del cliente " + cliente);

        bool send = EmailCommon.SendMessage(EmailTemplate.Notificacion.ToString(), replacements, comprobante.Usuarios.EmailAlertas, "Pago recibido");
        //CobranzasCommon.EnviarCobranzaAutomaticamente(idCobranza, nombre, usu);
        
        
        var mailPersona = string.IsNullOrEmpty(persona.EmailsEnvioFc) ? persona.Email : persona.EmailsEnvioFc;
        if (!string.IsNullOrEmpty(mailPersona))
        {
            string emailFrom = comprobante.Usuarios.EmailFrom ?? comprobante.Usuarios.RazonSocial;

            System.Net.Mail.MailAddressCollection listTo = new System.Net.Mail.MailAddressCollection();
            foreach (var mail in mailPersona.Split(','))
            {
                if (mail != string.Empty)
                {
                    listTo.Add(new System.Net.Mail.MailAddress(mail));
                }
            }
            
            send = EmailCommon.SaveMessage(comprobante.IDUsuario, EmailTemplate.PagoRecibido, replacements,
                listTo, System.Configuration.ConfigurationManager.AppSettings["Email.Notifications"], "", "Pago recibido", null, emailFrom);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}

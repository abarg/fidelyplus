﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Extensions
{
    public static class DecimalExtensions
    {
        public static string ToMoneyFormat(this decimal target, byte cantidadDecimales)
        {
            //string strValue = target.ToString("#,0.0000"); //Get the stock string
            var format = "#,0.";
            format = format.PadRight(format.Length + cantidadDecimales, '0');
            string strValue = target.ToString(format); //Get the stock string

            //If there is a decimal point present
            /*if (strValue.Contains(","))
            {
                //Remove all trailing zeros
                strValue = strValue.TrimEnd('0');

                //If all we are left with is a decimal point
                if (strValue.EndsWith(",")) //then remove it
                    strValue = strValue.TrimEnd(',');
            }*/

            //if (strValue.EndsWith(",00")) //then remove it
            //    strValue = strValue.Replace(",00","");

            return strValue;
        }

        public static string ToMoneyFormatForEdit(this decimal target, byte cantidadDecimales)
        {
            string strValue = target.ToString(String.Format("N{0}", cantidadDecimales)); //Get the stock string

            ////If there is a decimal point present
            //if (strValue.Contains(","))
            //{
            //    //Remove all trailing zeros
            //    strValue = strValue.TrimEnd('0');

            //    //If all we are left with is a decimal point
            //    if (strValue.EndsWith(",")) //then remove it
            //        strValue = strValue.TrimEnd(',');
            //}

            return strValue.Replace(".", "").Replace(",", ".");
        }
    }
}

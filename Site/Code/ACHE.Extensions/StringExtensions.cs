﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.ComponentModel;

namespace ACHE.Extensions
{
    public static class StringExtensions
    {

        public static string SplitCamelCase(this string str)
        {
            return Regex.Replace(
                Regex.Replace(
                    str,
                    @"(\P{Ll})(\P{Ll}\p{Ll})",
                    "$1 $2"
                ),
                @"(\p{Ll})(\P{Ll})",
                "$1 $2"
            );
        }

        public static string RemoverAcentosHTML(this string textoOriginal)
        {
            return textoOriginal.Replace("&aacute;", "á").Replace("&eacute;", "é")
                .Replace("&iacute;", "í").Replace("&oacute;", "ó").Replace("&úacute;", "ó").Replace("&ntilde;", "ñ").Trim();
        }

        public static string RemoverAcentos(this string textoOriginal)
        {
            return textoOriginal.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").Replace("ü", "u").Replace("ö", "o").Replace("ä", "a")
                .Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                .Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U").Replace("Ü", "U").Replace("º", "").Replace("ï", "i")
                .Replace("Ç", "C").Replace("ç", "c").Replace("Ñ", "N").Replace("ñ", "n").Replace('"', ' ').Replace("´", "").Replace("ê", "e")
                .Replace("°", "").Replace(",", "").Replace(".", "").Replace('“', ' ').Trim();

            //.Replace("\"", "\\\"");
        }

        public static string RemoverAcentosUnicode(this string textoOriginal)
        {
            return textoOriginal.Replace("\\u00C1", "Á").Replace("\\u00E1", "á")
                .Replace("\\u00C9", "É").Replace("\\u00E9", "é")
                .Replace("\\u00CD", "Í").Replace("\\u00ED", "í")
                .Replace("\\u00D3", "Ó").Replace("\\u00F3", "ó")
                .Replace("\\u00DA", "Ú").Replace("\\u00FA", "ú")
                .Replace("\\u00D1", "Ñ").Replace("\\u00F1", "ñ");

            //.Replace("\"", "\\\"");
        }

        private static readonly Regex cleanWhitespace = new Regex(@"\s+", RegexOptions.Compiled | RegexOptions.Singleline);

        public static bool IsValidEmailAddress(this string s)
        {
            return new System.ComponentModel.DataAnnotations.EmailAddressAttribute().IsValid(s);
            //Regex rx = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$");
            //return rx.IsMatch(s);
        }

        public static bool IsValidMultiEmailAddress(this string s)
        {
            var isValid = true;
            foreach (var mail in s.Split(","))
            {
                if (mail.Trim() != "")
                {
                    if (!new System.ComponentModel.DataAnnotations.EmailAddressAttribute().IsValid(mail.Trim()))
                        isValid = false;
                }
            }

            return isValid;

            //return new System.ComponentModel.DataAnnotations.EmailAddressAttribute().IsValid(s);
            //Regex rx = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$");
            //return rx.IsMatch(s);
        }

        public static bool IsValidUrl(this string text)
        {
            Regex rx = new Regex(@"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?");
            return rx.IsMatch(text);
        }

        public static string CleanHtmlTags(this string s)
        {
            Regex exp = new Regex("<[^<>]*>", RegexOptions.Compiled);

            return exp.Replace(s, "").ReplaceAll("&nbsp;", " ").RemoverAcentosHTML();
        }

        public static bool ContainsWords(this string phrase, string[] words)
        {
            //The staring returnValue is false, but changes if all words are found
            bool returnValue = false;
            //Placeholder for the amount of words found
            int wordsFound = 0;
            //Loop through all of the words we are trying to find
            for (int w = 0; w < words.Length; w++)
            {
                //Loop through all of the words in the phrase
                for (int i = 0; i < phrase.Split(' ').Length; i++)
                {
                    //Get the current word in the phrase
                    String word = phrase.Split(' ')[i];
                    //If the current word is in our list of words to find
                    if (word == words[w])
                        //Add 1 to the wordsFound count
                        wordsFound++;
                }
            }
            //If all of the words are found
            if (wordsFound == words.Length)
                //Set the returnValue to true
                returnValue = true;

            //Return true or false depending on how many words were found
            return returnValue;
        }

        public static bool ContainsAll<T>(this IEnumerable<T> source, IEnumerable<T> values)
        {
            return values.All(value => source.Contains(value));
        }

        //todo: (nheskew) rename to something more generic (CleanAttributeALittle?) because not everything needs
        // the cleaning power of CleanAttribute (everything should but AntiXss.HtmlAttributeEncode encodes 
        // *everyting* incl. white space :|) so attributes can get really long...but then my only current worry is around
        // the description meta tag. Attributes from untrusted sources *do* need the current CleanAttribute...
        public static string CleanWhitespace(this string s)
        {
            return cleanWhitespace.Replace(s, " ");
        }

        public static string RemoveLineBreaks(this string lines)
        {
            return lines.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Replace("\t", "").Trim();
        }

        public static string ReplaceLineBreaks(this string lines, string replacement)
        {
            return lines.Replace("\r\n", replacement)
                        .Replace("\r", replacement)
                        .Replace("\n", replacement)
                        .Replace("\t", replacement)
                        .Trim();
        }

        public static string Truncate(this string text, int maxLength, string suffix)
        {
            // replaces the truncated string to a ...
            string truncatedString = text;

            if (maxLength <= 0) return truncatedString;
            int strLength = maxLength - suffix.Length;

            if (strLength <= 0) return truncatedString;

            if (text == null || text.Length <= maxLength) return truncatedString;

            truncatedString = text.Substring(0, strLength);
            truncatedString = truncatedString.TrimEnd();
            truncatedString += suffix;
            return truncatedString;
        }

        public static bool IsNotNullOrEmpty(this string input)
        {
            return !String.IsNullOrEmpty(input);
        }

        public static bool IsStrongPassword(this string s)
        {
            bool isStrong = Regex.IsMatch(s, @"[\d]");
            if (isStrong) isStrong = Regex.IsMatch(s, @"[a-z]");
            if (isStrong) isStrong = Regex.IsMatch(s, @"[A-Z]");
            if (isStrong) isStrong = Regex.IsMatch(s, @"[\s~!@#\$%\^&\*\(\)\{\}\|\[\]\\:;'?,.`+=<>\/]");
            if (isStrong) isStrong = s.Length > 7;
            return isStrong;
        }

        public static string ToProperCase(this string text)
        {
            System.Globalization.CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Globalization.TextInfo textInfo = cultureInfo.TextInfo;
            return textInfo.ToTitleCase(text);
        }

        public static string ToDescription(this Enum en) //ext method
        {

            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {

                object[] attrs = memInfo[0].GetCustomAttributes(
                typeof(DescriptionAttribute),

                false);

                if (attrs != null && attrs.Length > 0)

                    return ((DescriptionAttribute)attrs[0]).Description;

            }

            return en.ToString();

        }

        public static string ToNumericOnly(this string input)
        {
            Regex rgx = new Regex("[^0-9]");
            return rgx.Replace(input, "");
        }

        public static string ReplaceAll(this string input, string pattern, string replacement)
        {
            return Regex.Replace(input, pattern, replacement);
        }

        public static string[] Split(this string input, string pattern)
        {
            return Regex.Split(input, pattern);
        }

        public static string[] Split(this string input, string pattern, System.Text.RegularExpressions.RegexOptions options)
        {
            return Regex.Split(input, pattern, options);
        }

        public static string Join(this string[] input)
        {
            return string.Join("", input);
        }

        public static string Join(this string[] input, string seperator)
        {
            return string.Join(seperator, input);
        }

        public static string MakeNotNull(this string input)
        {
            if (input == null)
                return string.Empty;
            return input;
        }

        public static string GenerateRandom(this string input, int lenght)
        {

            string _characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz/////*****";

            Random random = new Random();
            StringBuilder buffer = new StringBuilder(lenght);
            for (int i = 0; i < lenght; i++)
            {
                int randomNumber = random.Next(0, _characters.Length);
                buffer.Append(_characters, randomNumber, 1);
            }
            return buffer.ToString();
        }

        public static string EncryptToSHA1(this string str)
        {
            string rethash = "";
            try
            {

                System.Security.Cryptography.SHA1 hash = System.Security.Cryptography.SHA1.Create();
                System.Text.ASCIIEncoding encoder = new System.Text.ASCIIEncoding();
                byte[] combined = encoder.GetBytes(str);
                hash.ComputeHash(combined);
                rethash = Convert.ToBase64String(hash.Hash);
            }
            catch (Exception ex)
            {
                string strerr = "Error in HashCode : " + ex.Message;
            }
            return rethash;
        }

        public static bool ExtensionIsDangerous(this string extension)
        {
            return (extension.Equals(".REG") || extension.Equals(".DOCM") || extension.Equals(".DOTM") || extension.Equals(".XLSM")
                || extension.Equals(".XLTM") || extension.Equals(".XLAM") || extension.Equals(".PPTM") || extension.Equals(".POTM")
                || extension.Equals(".PPAM") || extension.Equals(".PPSM") || extension.Equals(".SLDM") || extension.Equals(".PS1")
                || extension.Equals(".PS1XML") || extension.Equals(".PS2") || extension.Equals(".PS2XML") || extension.Equals(".PSC1")
                || extension.Equals(".PSC2") || extension.Equals(".INF") || extension.Equals(".MSH") || extension.Equals(".MSH1")

                || extension.Equals(".PSC2") || extension.Equals(".INF") || extension.Equals(".MSH") || extension.Equals(".MSH1")
                || extension.Equals(".MSH2") || extension.Equals(".MSHXML") || extension.Equals(".MSH1XML") || extension.Equals(".MSH2XML")
                || extension.Equals(".WS") || extension.Equals(".WSF") || extension.Equals(".WSC") || extension.Equals(".WSH")

                || extension.Equals(".JS") || extension.Equals(".JSE") || extension.Equals(".VBE") || extension.Equals(".VB")
                || extension.Equals(".WS") || extension.Equals(".WSF") || extension.Equals(".WSC") || extension.Equals(".WSH")
                || extension.Equals(".VBS") || extension.Equals(".CMD") || extension.Equals(".BAT") || extension.Equals(".HTA")
                || extension.Equals(".EXE") || extension.Equals(".COM") || extension.Equals(".MSI") || extension.Equals(".APPLICATION")

                || extension.Equals(".PIF") || extension.Equals(".JAR") || extension.Equals(".CPL") || extension.Equals(".SCR")
                || extension.Equals(".MSP") || extension.Equals(".ASPX") || extension.Equals(".ASHX") || extension.Equals(".CS")
                || extension.Equals(".SYS") || extension.Equals(".DRV")
                );
        }

        public static bool ExtensionIsOK(this string extension)
        {
            return (extension.Equals(".PDF")
                 || extension.Equals(".DOC") || extension.Equals(".DOCX") || extension.Equals(".RTF") || extension.Equals(".WTX") || extension.Equals(".TXT")
                 || extension.Equals(".JPG") || extension.Equals(".JPEG") || extension.Equals(".PNG") || extension.Equals(".GIF") || extension.Equals(".PIC") || extension.Equals(".TIF")
                 || extension.Equals(".XLS") || extension.Equals(".XLSX")
                 || extension.Equals(".CSV")
                );
        }

        public static bool IsValidCUIT(this string cuit)
        {
            if (cuit == null)
            {
                return false;
            }

            cuit = cuit.Replace("-", string.Empty);
            if (cuit.Length != 11)
            {
                return false;
            }
            else
            {
                int calculado = CalcularDigitoCuil(cuit);
                int digito = int.Parse(cuit.Substring(10));
                return calculado == digito;
            }
        }

        private static int CalcularDigitoCuil(string cuil)
        {
            int[] mult = new[] { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            char[] nums = cuil.ToCharArray();
            int total = 0;
            for (int i = 0; i < mult.Length; i++)
            {
                total += int.Parse(nums[i].ToString()) * mult[i];
            }
            var resto = total % 11;
            return resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
        }

        public static string RemoverCaracteresParaPDF(this string textoOriginal)
        {
            textoOriginal = textoOriginal.Replace("\r\n", " ")
                        .Replace("\r", " ")
                        .Replace("\n", " ")
                        .Replace("\t", " ")
                        .Trim();

            return textoOriginal.RemoverAcentos().ReplaceAll("#", "").ReplaceAll("@", "").ReplaceAll("$", "").ReplaceAll(" ", "_").Replace(",", "").Replace("\"", "").Replace("/", "").Replace(":", "")
                                                 .Replace("*", "").Replace("?", "").Replace("<", "").Replace(">", "").Replace("|", "").Replace(".", "").Replace("&", "").Trim();
            // \/ : * ? " <> | .
        }
    }
}

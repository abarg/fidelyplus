﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Collections;
using System.Configuration;
using ACHE.Extensions;

namespace ACHE.Model
{
    public enum EmailTemplateApp
    {
        NotificacionSinSetup,
        Notificacion2Dias,
        Notificacion4Dias,
        Notificacion4DiasSinUso,
        Notificacion6Dias,
        Notificacion7Dias,
        Notificacion8Dias,
        Notificacion9Dias,
        Notificacion10Dias,
        Notificacion5DiasVencimiento,
        Notificacion1DiasVencimiento,
        Notificacion0DiasVencimiento,
        NotificacionVencido,
        NotificacionAdministrador,
        Alertas,
        AlertasCheque,
        AlertasChequePropio,
        NotificacionReactivar,
        AvisosVencimiento,
        AvisosVencimientoConFoto,
        NotificacionCapacitacion,
        NotificacionPrimerPago,
        NotificacionPrimerPago28Dias,
        NotificacionPrimerPago20Dias,
        NotificacionPrimerPago15Dias,
        NotificacionPrimerPago7Dias,
        NotificacionPrevioDA
    }

    public static class EmailHelperApp
    {
        public static bool SendMessage(string template, ListDictionary replacements, string to, string subject)
        {
            string emailFrom = ConfigurationManager.AppSettings["Email.From"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, subject, "");

            return SendMailMessage(mailMessage);
        }

        public static bool SendMessage(string template, ListDictionary replacements, string to, string subject, string from, string displayFromName, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "", bool enableSsl = false)
        {
            string emailFrom = from == string.Empty ? ConfigurationManager.AppSettings["Email.From"] : from;
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, subject, displayFromName);

            return SendMailMessage(mailMessage, smtphost, smtpPuerto, smtpUsuario, smtpContrasenia);
        }

        public static bool SendMessage(string template, ListDictionary replacements, string to, string cc, string replyTo, string subject, string displayFromName, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "", bool enableSsl = false)
        {
            string emailFrom = smtpUsuario == string.Empty ? ConfigurationManager.AppSettings["Email.From"] : smtpUsuario;
            MailMessage mailMessage = CreateMessage(template, replacements, to, cc, replyTo, emailFrom, subject, displayFromName);

            return SendMailMessage(mailMessage, smtphost, smtpPuerto, smtpUsuario, smtpContrasenia);
        }

        private static MailMessage CreateMessage(string template, ListDictionary replacements, string to, string cc, string replyTo, string from, string subject, string displayFromName)
        {
            var pathBase = ConfigurationManager.AppSettings["PathBaseWeb"];

            string physicalPath = (Path.Combine(pathBase + "App_Data\\EmailTemplates\\") + template.ToString() + ".txt");
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid EMail Template Passed into the NotificationManager: " + template, "template");

            string html = File.ReadAllText(physicalPath);
            foreach (DictionaryEntry param in replacements)
                html = html.Replace(param.Key.ToString(), param.Value.ToString());

            MailMessage mailMessage = new MailMessage();
            if (from != string.Empty)
                mailMessage.From = new MailAddress(from, displayFromName);
            mailMessage.Subject = subject;

            foreach (var mail in to.Split(','))
            {
                if (mail != string.Empty)
                    mailMessage.To.Add(mail);
            }
            //mailMessage.To.Add(to);

            //string cc = ConfigurationManager.AppSettings["Email.CC"];
            if (!string.IsNullOrEmpty(cc))
            {
                foreach (var mail in cc.Split(','))
                {
                    if (mail != string.Empty)
                        mailMessage.CC.Add(mail);
                }
                //mailMessage.CC.Add(new MailAddress(cc));
            }

            //var replyTo = ConfigurationManager.AppSettings["Email.ReplyTo"];
            if (!string.IsNullOrEmpty(replyTo))
            {
                foreach (var mail in replyTo.Split(','))
                {
                    if (mail != string.Empty)
                        mailMessage.ReplyToList.Add(mail);
                }

                //mailMessage.ReplyToList.Add(new MailAddress(replyTo));
            }

            string emailBCC = ConfigurationManager.AppSettings["Email.BCC"];
            if (!string.IsNullOrEmpty(emailBCC))
                mailMessage.Bcc.Add(new MailAddress(emailBCC));

            mailMessage.IsBodyHtml = true;
            mailMessage.Body = html;

            return mailMessage;
        }

        private static MailMessage CreateMessage(string template, ListDictionary replacements, string to, string from, string subject, string displayFromName)
        {
            var pathBase = ConfigurationManager.AppSettings["PathBaseWeb"];

            string physicalPath = (Path.Combine(pathBase + "App_Data\\EmailTemplates\\") + template + ".txt");
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid EMail Template Passed into the NotificationManager: " + template, "template");

            string html = File.ReadAllText(physicalPath);
            foreach (DictionaryEntry param in replacements)
                html = html.Replace(param.Key.ToString(), param.Value.ToString());

            MailMessage mailMessage = new MailMessage();
            if (from != string.Empty)
                mailMessage.From = new MailAddress(from, displayFromName);
            mailMessage.Subject = subject;

            foreach (var mail in to.Split(','))
            {
                if (mail != string.Empty)
                    mailMessage.To.Add(mail);
            }
            //mailMessage.To.Add(to);

            string cc = ConfigurationManager.AppSettings["Email.CC"];
            if (!string.IsNullOrEmpty(cc))
            {
                foreach (var mail in cc.Split(','))
                {
                    if (mail != string.Empty)
                        mailMessage.CC.Add(mail);
                }
            }

            var replyTo = ConfigurationManager.AppSettings["Email.ReplyTo"];
            if (!string.IsNullOrEmpty(replyTo))
                mailMessage.ReplyToList.Add(new MailAddress(replyTo));

            string emailBCC = ConfigurationManager.AppSettings["Email.BCC"];
            if (!string.IsNullOrEmpty(emailBCC))
                mailMessage.Bcc.Add(new MailAddress(emailBCC));

            mailMessage.IsBodyHtml = true;
            mailMessage.Body = html;

            return mailMessage;
        }

        private static bool SendMailMessage(MailMessage mailMessage, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "", bool enableSsl = false)
        {
            bool send = true;

            try
            {
                SmtpClient client = new SmtpClient();
                if (!string.IsNullOrEmpty(smtpUsuario) && !string.IsNullOrEmpty(smtpContrasenia))
                {
                    NetworkCredential basicCredential = new NetworkCredential(smtpUsuario, smtpContrasenia);
                    client.UseDefaultCredentials = false;
                    client.Credentials = basicCredential;
                    client.EnableSsl = enableSsl;
                }
                if (!string.IsNullOrEmpty(smtphost))
                    client.Host = smtphost;
                if (!string.IsNullOrEmpty(smtpPuerto))
                    client.Port = int.Parse(smtpPuerto);

                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                send = false;
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                throw new Exception(msg);
            }
            return send;
        }

        public static bool SendMessage(string realTemplate, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName, string smtphost, string smtpPuerto, string smtpUsuario, string smtpContrasenia, bool enableSsl)
        {
            string emailFrom = from == string.Empty ? ConfigurationManager.AppSettings["Email.From"] : from;
            MailMessage mailMessage = CreateMessage(realTemplate, replacements, to, from, cc, subject, attachments, fromDisplayName);

            return SendMailMessage(mailMessage, smtphost, smtpPuerto, smtpUsuario, smtpContrasenia, enableSsl);
        }

        private static MailMessage CreateMessage(string template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName)
        {
            var pathBase = ConfigurationManager.AppSettings["PathBaseWeb"];

            string physicalPath = (Path.Combine(pathBase + "App_Data\\EmailTemplates\\") + template + ".txt");
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid EMail Template Passed into the NotificationManager: " + template, "template");

            string html = File.ReadAllText(physicalPath);
            foreach (DictionaryEntry param in replacements)
                html = html.Replace(param.Key.ToString(), param.Value.ToString());

            MailMessage mailMessage = new MailMessage();
            if (from != string.Empty)
                mailMessage.From = new MailAddress(from, fromDisplayName);
            mailMessage.Subject = subject;
            foreach (var mail in to)
                mailMessage.To.Add(mail);
            //if (!string.IsNullOrEmpty(cc))
            //    mailMessage.CC.Add(new MailAddress(cc));
            if (!string.IsNullOrEmpty(cc))
            {
                foreach (var mail in cc.Split(','))
                {
                    if (mail != string.Empty)
                        mailMessage.CC.Add(mail);
                }
            }

            //string emailReply = ConfigurationManager.AppSettings["Email.ReplyTo"];
            string emailReply = cc;
            if (!string.IsNullOrEmpty(emailReply))
            {
                foreach (var mail in emailReply.Split(','))
                {
                    if (mail != string.Empty)
                        mailMessage.ReplyToList.Add(mail);
                }
            }
            
            //if (!string.IsNullOrEmpty(emailReply))
            //    mailMessage.ReplyToList.Add(new MailAddress(emailReply));

            if (attachments != null)
            {
                foreach (string attach in attachments)
                {
                    if (attach != "")
                    {
                        mailMessage.Attachments.Add(new Attachment(attach));
                    }
                }
            }
            mailMessage.IsBodyHtml = template.ToString() != "EnvioComprobanteSinFormato" ? true : false;
            mailMessage.Body = template.ToString() != "EnvioComprobanteSinFormato" ? html : html.CleanHtmlTags();

            //if (template.ToString() == "EnvioComprobanteSinFormato")
            //    mailMessage.Bcc.Add(new MailAddress("leandrohalfon@gmail.com"));

            if (from != string.Empty)
            {
                mailMessage.Headers.Add("Disposition-Notification-To", from);
                mailMessage.Headers.Add("Read-Receipt-To", from);
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess | DeliveryNotificationOptions.OnFailure;
            }

            return mailMessage;
        }
    }
}
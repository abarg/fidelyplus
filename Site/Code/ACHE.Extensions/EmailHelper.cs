﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using ACHE.Extensions;

namespace ACHE.Model
{
    public enum EmailTemplate
    {
        RecuperoPwd,
        EnvioComprobante,//Se usa para tracking horas
        EnvioComprobanteContabilium,
        EnvioComprobanteConFoto,
        EnvioComprobanteSinFormato,
        EnvioCobranza,
        Ayuda,
        Notificacion,
        NotificacionDeContabilium,
        Bienvenido,
        ModificacionPwd,
        Compras,
        Alertas,
        PagoPlanes,
        EnvioInvitacionReferidos,
        DebitoAutomatico,
        NotificacionPrevioDebitoAutomatico,
        NotificacionPrevioDA,
        NotificacionDASuscripcion,
        NotificacionDebitoAutomatico,
        PagoRecibido,
        ConsultaDePago    }

    public static class EmailHelper
    {
        //public static readonly string HOST = ConfigurationManager.AppSettings["Email.Host"] ?? "No hay host definido";
        //public static readonly int PORT = int.Parse(ConfigurationManager.AppSettings["Email.Port"]);

        public static bool SendMessage(string template, ListDictionary replacements, string to, string subject)
        {
            string emailFrom = ConfigurationManager.AppSettings["Email.From"];
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject);

            return SendMailMessage(mailMessage);
        }

        public static bool SendMessage(string template, ListDictionary replacements, string from, string to, string subject, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "", bool enableSsl = false)
        {
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            string emailFrom = from == string.Empty ? ConfigurationManager.AppSettings["Email.From"] : from;
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject);

            return SendMailMessage(mailMessage, smtphost, smtpPuerto, smtpUsuario, smtpContrasenia, enableSsl);
        }

        /*public static bool SendMessage(EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string subject)
        {
            string emailFrom = ConfigurationManager.AppSettings["Email.From"];
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject);

            return SendMailMessage(mailMessage);
        }*/

        public static bool SendMessage(string template, ListDictionary replacements, MailAddressCollection to, string from, string subject, List<string> attachments, string fromDisplayName, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "", bool enableSsl = false)
        {
            string emailFrom = from == string.Empty ? ConfigurationManager.AppSettings["Email.From"] : from;
            string emailCC = ConfigurationManager.AppSettings["Email.CCFc"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject, attachments, fromDisplayName);

            return SendMailMessage(mailMessage, smtphost, smtpPuerto, smtpUsuario, smtpContrasenia, enableSsl);
        }

        public static bool SendMessage(string template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "", bool enableSsl = false)
        {
            string emailFrom = from == string.Empty ? ConfigurationManager.AppSettings["Email.From"] : from;
            //string emailCC = ConfigurationManager.AppSettings["Email.CCFc"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, cc, subject, attachments, fromDisplayName);

            return SendMailMessage(mailMessage, smtphost, smtpPuerto, smtpUsuario, smtpContrasenia, enableSsl);
        }


        /*public static bool SendMessage(EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string subject, List<string> attachments)
        {
            string emailFrom = ConfigurationManager.AppSettings["Email.From"];
            string emailCC = ConfigurationManager.AppSettings["Email.CC"];
            MailMessage mailMessage = CreateMessage(template, replacements, to, emailFrom, emailCC, subject, attachments);

            return SendMailMessage(mailMessage);
        }*/

        private static MailMessage CreateMessage(EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, string fromDisplayName)
        {
            string physicalPath = HttpContext.Current.Server.MapPath("~/App_Data/EmailTemplates/" + template.ToString() + ".txt");
            physicalPath = physicalPath.Replace("rest", "app");//TODO: FIx para WebApi. Mejorar
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid EMail Template Passed into the NotificationManager: " + template, "template");

            string html = File.ReadAllText(physicalPath);
            foreach (DictionaryEntry param in replacements)
                html = html.Replace(param.Key.ToString(), param.Value.ToString());

            MailMessage mailMessage = new MailMessage();
            if (from != string.Empty)
                mailMessage.From = new MailAddress(from, fromDisplayName);
            mailMessage.Subject = subject;
            foreach (var mail in to)
                mailMessage.To.Add(mail);
            if (!string.IsNullOrEmpty(cc))
                mailMessage.CC.Add(new MailAddress(cc));

            if (subject != "Contabilium: Recuperación de contraseña" && subject != "Contabilium: Hemos recibido su sugerencia.")
            {
                string emailBCC = ConfigurationManager.AppSettings["Email.BCC"];
                if (!string.IsNullOrEmpty(emailBCC))
                    mailMessage.Bcc.Add(new MailAddress(emailBCC));
            }

            mailMessage.IsBodyHtml = template.ToString() != "EnvioComprobanteSinFormato" ? true : false;
            mailMessage.Body = template.ToString() != "EnvioComprobanteSinFormato" ? html : html.CleanHtmlTags();

            //if (template.ToString() == "EnvioComprobanteSinFormato")
            //    mailMessage.Bcc.Add(new MailAddress("leandrohalfon@gmail.com"));

            return mailMessage;
        }

        private static MailMessage CreateMessage(string template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName)
        {
            string physicalPath = HttpContext.Current.Server.MapPath("~/App_Data/EmailTemplates/" + template.ToString() + ".txt");
            physicalPath = physicalPath.Replace("rest", "app");//TODO: FIx para WebApi. Mejorar
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid EMail Template Passed into the NotificationManager: " + template, "template");

            string html = File.ReadAllText(physicalPath);
            foreach (DictionaryEntry param in replacements)
                html = html.Replace(param.Key.ToString(), param.Value.ToString());

            MailMessage mailMessage = new MailMessage();
            if (from != string.Empty)
                mailMessage.From = new MailAddress(from, fromDisplayName);
            mailMessage.Subject = subject;
            foreach (var mail in to)
                mailMessage.To.Add(mail);
            if (!string.IsNullOrEmpty(cc))
                mailMessage.CC.Add(new MailAddress(cc));
            //string emailReply = ConfigurationManager.AppSettings["Email.ReplyTo"];
            string emailReply = cc;
            if (!string.IsNullOrEmpty(emailReply))
                mailMessage.ReplyToList.Add(new MailAddress(emailReply));

            if (attachments != null)
            {
                foreach (string attach in attachments)
                {
                    mailMessage.Attachments.Add(new Attachment(attach));
                }
            }
            mailMessage.IsBodyHtml = template.ToString() != "EnvioComprobanteSinFormato" ? true : false;
            mailMessage.Body = template.ToString() != "EnvioComprobanteSinFormato" ? html : html.CleanHtmlTags();

            //if (template.ToString() == "EnvioComprobanteSinFormato")
            //    mailMessage.Bcc.Add(new MailAddress("leandrohalfon@gmail.com"));

            if (from != string.Empty)
            {
                mailMessage.Headers.Add("Disposition-Notification-To", from);
                mailMessage.Headers.Add("Read-Receipt-To", from);
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess | DeliveryNotificationOptions.OnFailure;
            }

            return mailMessage;
        }

        private static MailMessage CreateMessage(string template, ListDictionary replacements, string to, string from, string cc, string subject)
        {
            string physicalPath = HttpContext.Current.Server.MapPath("~/App_Data/EmailTemplates/" + template.ToString() + ".txt");
            physicalPath = physicalPath.Replace("rest", "app");//TODO: FIx para WebApi. Mejorar
            if (!File.Exists(physicalPath))
                throw new ArgumentException("Invalid EMail Template Passed into the NotificationManager: " + template, "template");

            string html = File.ReadAllText(physicalPath);
            if (replacements != null)
            {
                foreach (DictionaryEntry param in replacements)
                {
                    string val = param.Value == null ? "" : param.Value.ToString();
                    html = html.Replace(param.Key.ToString(), val);
                }
            }

            MailMessage mailMessage = new MailMessage();
            if (from != string.Empty)
                mailMessage.From = new MailAddress(from);
            mailMessage.Subject = subject;
            mailMessage.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(cc))
                mailMessage.CC.Add(new MailAddress(cc));

            if (subject != "Contabilium: Recuperación de contraseña" && subject != "Recibimos tu sugerencia!")
            {
                string emailBCC = ConfigurationManager.AppSettings["Email.BCC"];
                if (!string.IsNullOrEmpty(emailBCC))
                    mailMessage.Bcc.Add(new MailAddress(emailBCC));
            }

            mailMessage.IsBodyHtml = template.ToString() != "EnvioComprobanteSinFormato" ? true : false;
            mailMessage.Body = template.ToString() != "EnvioComprobanteSinFormato" ? html : html.CleanHtmlTags();

            //if (template.ToString() == "EnvioComprobanteSinFormato")
            //    mailMessage.Bcc.Add(new MailAddress("leandrohalfon@gmail.com"));

            return mailMessage;
        }

        private static bool SendMailMessage(MailMessage mailMessage, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "", bool enableSsl = false)
        {
            bool send = true;

            try
            {
                SmtpClient client = new SmtpClient();
                if (!string.IsNullOrEmpty(smtpUsuario) && !string.IsNullOrEmpty(smtpContrasenia))
                {
                    NetworkCredential basicCredential = new NetworkCredential(smtpUsuario, smtpContrasenia);
                    client.UseDefaultCredentials = false;
                    client.Credentials = basicCredential;
                    client.EnableSsl = enableSsl;
                }
                if (!string.IsNullOrEmpty(smtphost))
                    client.Host = smtphost;
                if (!string.IsNullOrEmpty(smtpPuerto))
                    client.Port = int.Parse(smtpPuerto);

                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                send = false;
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, ex.ToString());
            }

            return send;
        }
    }


}
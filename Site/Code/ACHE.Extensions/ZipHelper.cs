﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;

namespace ACHE.Extensions
{
    public static class ZipHelper
    {
        public static void ZipFiles(string path, IEnumerable<string> files,
               CompressionOption compressionLevel = CompressionOption.Normal)
        {
            using (FileStream fileStream = new FileStream(path, FileMode.Create))
            {
                ZipHelper.ZipFilesToStream(fileStream, files, compressionLevel);
            }
        }

        public static byte[] ZipFilesToByteArray(IEnumerable<string> files,
               CompressionOption compressionLevel = CompressionOption.Normal)
        {
            byte[] zipBytes = default(byte[]);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                ZipHelper.ZipFilesToStream(memoryStream, files, compressionLevel);
                memoryStream.Flush();

                zipBytes = memoryStream.ToArray();
            }

            return zipBytes;
        }
        /*
        private static void AddFolder(string Folder, Stream destination)
        {
            if (string.IsNullOrEmpty(Folder))
                throw new ArgumentNullException("Folder");
            if (Folder.EndsWith(@"\"))
                Folder = Folder.Remove(Folder.Length - 1);
            using (Package Package = ZipPackage.Open(destination, FileMode.OpenOrCreate))
            {
                //List<FileInfo> Files = Utilities.IO.FileManager.FileList(Folder, true);
                foreach (FileInfo File in Directory.GetFiles(Folder))
                {
                    string FilePath = File.FullName.Replace(Folder, "");
                    AddFile(FilePath, destination);
                }
            }
        }

        private static void AddFile(string File, Stream destination)
        {
            if (string.IsNullOrEmpty(File))
                throw new ArgumentNullException("File");
            //if (!Utilities.IO.FileManager.FileExists(File))
            //    throw new ArgumentException("File");
            using (Package Package = ZipPackage.Open(destination))
            {
                AddFile(File, new FileInfo(File), Package);
            }
        }

        private static void AddFile(string File, FileInfo FileInfo, Package Package)
        {
            if (string.IsNullOrEmpty(File))
                throw new ArgumentNullException("File");
            //if (!Utilities.IO.FileManager.FileExists(FileInfo.FullName))
            //    throw new ArgumentException("File");
            Uri UriPath = PackUriHelper.CreatePartUri(new Uri(File, UriKind.Relative));
            PackagePart PackagePart = Package.CreatePart(UriPath, System.Net.Mime.MediaTypeNames.Text.Xml, CompressionOption.Maximum);
            byte[] Data = null;
            Utilities.IO.FileManager.GetFileContents(FileInfo.FullName, out Data);
            PackagePart.GetStream().Write(Data, 0, Data.Count());
            Package.CreateRelationship(PackagePart.Uri, TargetMode.Internal, "http://schemas.microsoft.com/opc/2006/sample/document");
        }
        */
        public static byte[] ZipFilesToByteArray(IEnumerable<string> files, IEnumerable<string> directories,
               CompressionOption compressionLevel = CompressionOption.Normal)
        {
            byte[] zipBytes = default(byte[]);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                ZipHelper.ZipFilesToStream(memoryStream, files, compressionLevel);
                //ZipHelper.DirectoriesToStream(memoryStream, directories, compressionLevel);

                memoryStream.Flush();

                zipBytes = memoryStream.ToArray();
            }

            return zipBytes;
        }

        public static void Unzip(string zipPath, string baseFolder)
        {
            using (FileStream fileStream = new FileStream(zipPath, FileMode.Open))
            {
                ZipHelper.UnzipFilesFromStream(fileStream, baseFolder);
            }
        }

        public static void UnzipFromByteArray(byte[] zipData, string baseFolder)
        {
            using (MemoryStream memoryStream = new MemoryStream(zipData))
            {
                ZipHelper.UnzipFilesFromStream(memoryStream, baseFolder);
            }
        }
        /*
        public static void BuildList(string SearchPath, int RecursionLevel, ArrayList _Files)
        {
            DirectoryInfo ThisLevel = new DirectoryInfo(SearchPath);
            DirectoryInfo[] ChildLevel = ThisLevel.GetDirectories();
            if (RecursionLevel != 1)
            {
                foreach (DirectoryInfo Child in ChildLevel)
                {
                    BuildList(Child.FullName, RecursionLevel - 1, _Files);
                }
            }
            FileInfo[] ChildFiles = ThisLevel.GetFiles();
            foreach (FileInfo ChildFile in ChildFiles)
            {
                _Files.Add(ChildFile.FullName);
            }
        }

        public static void zipFolder(string basePath)
        {
            zipFolder(basePath, false);
        }

        public static void zipFolder(string basePath, bool deleteAfter)
        {
            string zipFileName = basePath + ".zip";
            Stream os = new Stream(File.OpenWrite(zipFileName));

            FileStream fs;

            var _Files = new ArrayList();
            BuildList(basePath, 0, _Files);

            foreach (string s in _Files)
            {
                string sourceFileName = s;

                //string path = Path.GetFileName(sourceFileName);
                string path = Path.GetFullPath(sourceFileName).Replace(basePath, "");
                //string path1 = Path.GetExtension(path2);

                ZipEntry ze = new ZipEntry(path);
                ze.CompressionMethod = CompressionMethod.Deflated;
                os.PutNextEntry(ze);

                fs = File.OpenRead(sourceFileName);

                byte[] buff = new byte[1024];
                int n = 0;
                while ((n = fs.Read(buff, 0, buff.Length)) > 0)
                {
                    os.Write(buff, 0, n);

                }
                fs.Close();
            }

            os.CloseEntry();
            os.Close();

            if (deleteAfter)
            {
                Directory.Delete(basePath, true);
            }
        }
        */

        private static void ZipFilesToStream(Stream destination,
                IEnumerable<string> files, CompressionOption compressionLevel)
        {
            using (Package package = Package.Open(destination, FileMode.Create))
            {
                foreach (string path in files)
                {
                    // fix for white spaces in file names (by ErrCode)
                    Uri fileUri = PackUriHelper.CreatePartUri(new Uri(@"/" +
                                  Path.GetFileName(path), UriKind.Relative));
                    string contentType = @"data/" + ZipHelper.GetFileExtentionName(path);

                    using (Stream zipStream =
                            package.CreatePart(fileUri, contentType, compressionLevel).GetStream())
                    {
                        using (FileStream fileStream = new FileStream(path, FileMode.Open))
                        {
                            fileStream.CopyTo(zipStream);
                        }
                    }
                }
            }
        }

        /*private static void DirectoriesToStream(Stream destination,
                IEnumerable<string> directories, CompressionOption compressionLevel)
        {
            using (Package package = Package.Open(destination, FileMode.Create))
            {
                foreach (string path in directories)
                {
                    // fix for white spaces in file names (by ErrCode)
                    //Uri fileUri = PackUriHelper.CreatePartUri(new Uri(@"/" +
                    //              Path.GetFileName(path), UriKind.Relative));
                    //string contentType = @"data/" + ZipHelper.GetFileExtentionName(path);

                    using (Stream zipStream =
                            package.CreatePart(fileUri, contentType, compressionLevel).GetStream())
                    {
                        using (FileStream fileStream = new FileStream(path, FileMode.Open))
                        {
                            fileStream.CopyTo(zipStream);
                        }
                    }
                }
            }
        }*/

        private static void UnzipFilesFromStream(Stream source, string baseFolder)
        {
            if (!Directory.Exists(baseFolder))
            {
                Directory.CreateDirectory(baseFolder);
            }

            using (Package package = Package.Open(source, FileMode.Open))
            {
                foreach (PackagePart zipPart in package.GetParts())
                {
                    // fix for white spaces in file names (by ErrCode)
                    string path = Path.Combine(baseFolder,
                         Uri.UnescapeDataString(zipPart.Uri.ToString()).Substring(1));

                    using (Stream zipStream = zipPart.GetStream())
                    {
                        using (FileStream fileStream = new FileStream(path, FileMode.Create))
                        {
                            zipStream.CopyTo(fileStream);
                        }
                    }
                }
            }
        }

        private static string GetFileExtentionName(string path)
        {
            string extention = Path.GetExtension(path);
            if (!string.IsNullOrWhiteSpace(extention) && extention.StartsWith("."))
            {
                extention = extention.Substring(1);
            }

            return extention;
        }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class MovimientosDeStockDetalle
    {
        public int IDMovimientoDeStockDetalle { get; set; }
        public int IDMovimiento { get; set; }
        public int IDConcepto { get; set; }
        public decimal CantidadTransferida { get; set; }
        public Nullable<decimal> StockAnterior { get; set; }
    
        public virtual MovimientosDeStock MovimientosDeStock { get; set; }
        public virtual Conceptos Conceptos { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class LogExternos
    {
        public int IDLog { get; set; }
        public int IDUsuario { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Tipo { get; set; }
        public string IDPublicacionIntegracion { get; set; }
        public string IDVariantePublicacionIntegracion { get; set; }
        public Nullable<int> IDIntegracion { get; set; }
        public string Estado { get; set; }
        public string InfoEnviada { get; set; }
        public string InfoRecibida { get; set; }
        public string RespuestaAMostrar { get; set; }
    
        public virtual Integraciones Integraciones { get; set; }
        public virtual Usuarios Usuarios { get; set; }
    }
}

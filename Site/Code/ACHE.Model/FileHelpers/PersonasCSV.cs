﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    [DelimitedRecord(";")]
    [IgnoreFirst(1)]
    public class PersonasCSV 
    {
        public string Codigo;
        public string RazonSocial;
        public string NombreFantansia;
        public string TipoDocumento;
        public string NroDocumento;
        public string CondicionIva;
        public string Telefono;
        public string Celular;
        public string Web;
        public string Email;
        public string Observaciones;
        public string Provincia;
        public string Ciudad;
        public string Domicilio;
        public string PisoDepto;
        public string CodigoPostal;
        public string EmailsEnvioFc;
        public string Personeria;
        public string AlicuotaIvaDefecto;
        public string TipoComprobanteDefecto;
        public string CBU;
        public string Banco;

        public string Contacto;
        public string Vendedor;

        [FieldOptional]
        public string[] Values;

    }

    public class PersonasCSVTmp : PersonasCSV
    {
        public string Tipo { get; set; }
        public string resultados { get; set; }
        public DateTime fechaAlta { get; set; }
        public string Estado{ get; set; }
        public int IDUsuario { get; set; }
    }
}

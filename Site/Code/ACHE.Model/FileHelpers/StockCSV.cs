﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHE.Model
{
    [DelimitedRecord(";")]
    [IgnoreFirst(1)]
    public class StockCSV
    {
        public string Codigo;
        public string Stock;
        public string Inventario;

        [FieldOptional]
        public string[] Values;
    }

    public class StockCSVTmp : StockCSV
    {
        public int IDInventario { get; set; }
        public string resultados { get; set; }
        public string Estado { get; set; }
        public int IDUsuario { get; set; }
    }
}

﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
    [DelimitedRecord(";")]
    [IgnoreFirst(1)]
    public class FacturasCSV
    {
        public string RazonSocial;
        public string NombreFantasia;
        public string Email;
        public string TipoDocumento;
        public string NroDocumento;
        public string CondicionIva;
        public string Provincia;
        public string Ciudad;
        public string Domicilio;
        public string Web;
        public string Concepto;
        public string Fecha;
        public string Modo;
        public string TipoComprobante;
        public string PuntoDeVenta;
        public string NroComprobante;
        public string CAE;

        public string ImporteNeto;
        public string ImporteNoGrabado;
        public string IVA2700;
        public string IVA2100;
        public string IVA1005;
        public string IVA0500;
        public string IVA0205;
        public string IVA0000;
        public string PercepcionesIVA;
        public string PercepcionesIIBB;
        public string Total;
        public string FormaDePago;
        public string Banco;
        public string Caja;
        public string FechaDePago;
        public string MontoPagado;
        public string CodigoCuentaContable;
        public string Observaciones;
        [FieldOptional]
        public string[] Values;
    }

    public class FacturasCSVTmp : FacturasCSV
    {
        public string Tipo { get; set; }
        public int IDUsuario { get; set; }
        public int IDPersona { get; set; }
        public int IDProvincia { get; set; }
        public int? IDCiudad { get; set; }
        public string Personeria { get; set; }
        public string resultados { get; set; }
        public DateTime fechaAlta { get; set; }
        public string FechaCAE { get; set; }
        public string Estado { get; set; }
        public string Modo { get; set; }
        public int idPlanDeCuenta { get; set; }
        public int IDPuntoDeVenta { get; set; }
        public List<ComprobantesDetalleViewModel> Items { get; set; }
    }

    [DelimitedRecord(";")]
    [IgnoreFirst(1)]
    public class FacturasCompraCSV
    {
        public string RazonSocial;
        public string NombreFantasia;
        public string Email;
        public string TipoDocumento;
        public string NroDocumento;
        public string CondicionIva;
        public string Provincia;
        public string Ciudad;
        public string Domicilio;
        public string Web;

        public string FechaEmision;
        public string FechaContable;
        public string TipoComprobante;
        public string PuntoDeVenta;
        public string NroComprobante;
        
        //public string ImporteNeto;
        //public string ImporteNoGrabado;
        public string ImporteMonotributo;
        public string Gravado2700;
        public string Gravado2100;
        public string Gravado1005;
        public string NoGravado;
        public string Exento;
        public string PercepcionesIVA;
        public string ImpuestosNacionales;
        public string ImpuestosMunicipales;
        public string ImpuestosInternos;
        public string Otros;
        public string Categoria;
        public string Rubro;
        public string CodigoCuentaContable;
        public string Observaciones;
        public string FormaDePago;
        public string Banco;
        public string Caja;
        public string FechaDePago;
        public string MontoPagado;
        [FieldOptional]
        public string[] Values;
        
    }

    public class FacturasCompraCSVTmp : FacturasCompraCSV
    {
        public string Tipo { get; set; }
        public int IDUsuario { get; set; }
        public int IDPersona { get; set; }
        public int IDProvincia { get; set; }
        public int? IDCiudad { get; set; }
        public string Personeria { get; set; }
        public string resultados { get; set; }
        public DateTime fechaAlta { get; set; }
        public int IDPuntoDeVenta { get; set; }
        public string Estado { get; set; }
        public List<ComprobantesDetalleViewModel> Items { get; set; }
    }

    [DelimitedRecord(";")]
    [IgnoreFirst(1)]
    public class ComprobantesDetalleCSV
    {
      
        public string Numero;
        public string Tipo;   
        public string PuntoDeVenta;

        public string Fecha;
        public string PeriodoDesde;
        public string PeriodoHasta;
        public string FechaVencimiento;
        public string CUIT;     
        public string RazonSocial;

        public string CondicionVenta;
        public string Codigo;
        public string Concepto;
        public string Cantidad;
        public string PrecioUnitario;

        public string Bonificacion;
        public string IVA;
        [FieldOptional]
        public string[] Values;

    }
    public class ComprobantesDetalleCSVTmp : ComprobantesDetalleCSV
    {
        public int IDUsuario { get; set; }
        public int IDPuntoDeVenta { get; set; }
        public int IDCondicionDeVenta { get; set; }

        public int IDPersona { get; set; }
        public int IDConcepto { get; set; }

        public string resultados { get; set; }
        public string Estado { get; set; }
        public int IDCuentaVenta { get; set; }

      /*  public string Numero { get; set; }
        public string PuntoDeVenta { get; set; }
        public string CUIT { get; set; }
        public string Tipo { get; set; }
        public string Fecha { get; set; }
        public string PeriodoDesde { get; set; }
        public string PeriodoHasta { get; set; }
        public string FechaVencimiento { get; set; }
        public string RazonSocial { get; set; }
        public string CondicionVenta { get; set; }*/
        public List<ComprobantesDetalleViewModel> Items { get; set; }


    }
  /*  public class ComprobantesImportacionIDS
    {
        public int IDUsuario { get; set; }
        public List<int> ids { get; set; }
    }*/

}

//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Estudios
    {
        public Estudios()
        {
            this.Usuarios = new HashSet<Usuarios>();
        }
    
        public int IDEstudio { get; set; }
        public string Usuario { get; set; }
        public string Pwd { get; set; }
        public string Email { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }
        public System.DateTime FechaUltLogin { get; set; }
    
        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}

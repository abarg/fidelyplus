//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class AsientosModelo
    {
        public AsientosModelo()
        {
            this.AsientoModeloDetalle = new HashSet<AsientoModeloDetalle>();
        }
    
        public int IDAsientoModelo { get; set; }
        public int IDUsuario { get; set; }
        public string Leyenda { get; set; }
    
        public virtual Usuarios Usuarios { get; set; }
        public virtual ICollection<AsientoModeloDetalle> AsientoModeloDetalle { get; set; }
    }
}

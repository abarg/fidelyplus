//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Comprobantes
    {
        public Comprobantes()
        {
            this.CobranzasDetalle = new HashSet<CobranzasDetalle>();
            this.CobranzasFormasDePago = new HashSet<CobranzasFormasDePago>();
            this.PlanesPagos = new HashSet<PlanesPagos>();
            this.Asientos = new HashSet<Asientos>();
            this.ComprobantesEnviados = new HashSet<ComprobantesEnviados>();
            this.ComprobantesDetalle = new HashSet<ComprobantesDetalle>();
            this.ComprobantesTributos = new HashSet<ComprobantesTributos>();
            this.OrdenVenta1 = new HashSet<OrdenVenta>();
            this.SuscripcionesPagos = new HashSet<SuscripcionesPagos>();
        }
    
        public int IDComprobante { get; set; }
        public int IDUsuario { get; set; }
        public int IDPuntoVenta { get; set; }
        public int IDPersona { get; set; }
        public string TipoDestinatario { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string Modo { get; set; }
        public string Tipo { get; set; }
        public System.DateTime FechaComprobante { get; set; }
        public System.DateTime FechaVencimiento { get; set; }
        public decimal ImporteTotalNeto { get; set; }
        public decimal ImporteTotalBruto { get; set; }
        public string CondicionVenta { get; set; }
        public int Numero { get; set; }
        public int TipoConcepto { get; set; }
        public string CAE { get; set; }
        public Nullable<System.DateTime> FechaCAE { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public string Error { get; set; }
        public Nullable<System.DateTime> FechaProceso { get; set; }
        public Nullable<System.DateTime> FechaError { get; set; }
        public bool Enviada { get; set; }
        public Nullable<System.DateTime> FechaEnvio { get; set; }
        public string EnvioError { get; set; }
        public Nullable<System.DateTime> FechaRecibida { get; set; }
        public string Observaciones { get; set; }
        public decimal Saldo { get; set; }
        public decimal ImporteNoGrabado { get; set; }
        public decimal PercepcionIVA { get; set; }
        public decimal PercepcionIIBB { get; set; }
        public Nullable<int> IDJurisdiccion { get; set; }
        public string UsuarioAlta { get; set; }
        public string UsuarioModifica { get; set; }
        public string IDVentaIntegracion { get; set; }
        public Nullable<int> IDIntegracion { get; set; }
        public Nullable<int> IDInventario { get; set; }
        public Nullable<int> NroRemito { get; set; }
        public Nullable<int> NroOrdenCompra { get; set; }
        public Nullable<int> IDUsuarioAdicional { get; set; }
        public Nullable<decimal> Comision { get; set; }
        public Nullable<int> IDOrdenVenta { get; set; }
        public int IDCondicionVenta { get; set; }
    
        public virtual ICollection<CobranzasDetalle> CobranzasDetalle { get; set; }
        public virtual Personas Personas { get; set; }
        public virtual PuntosDeVenta PuntosDeVenta { get; set; }
        public virtual ICollection<CobranzasFormasDePago> CobranzasFormasDePago { get; set; }
        public virtual ICollection<PlanesPagos> PlanesPagos { get; set; }
        public virtual ICollection<Asientos> Asientos { get; set; }
        public virtual ICollection<ComprobantesEnviados> ComprobantesEnviados { get; set; }
        public virtual Usuarios Usuarios { get; set; }
        public virtual Provincias Provincias { get; set; }
        public virtual ICollection<ComprobantesDetalle> ComprobantesDetalle { get; set; }
        public virtual Integraciones Integraciones { get; set; }
        public virtual Inventarios Inventarios { get; set; }
        public virtual UsuariosAdicionales UsuariosAdicionales { get; set; }
        public virtual ICollection<ComprobantesTributos> ComprobantesTributos { get; set; }
        public virtual OrdenVenta OrdenVenta { get; set; }
        public virtual ICollection<OrdenVenta> OrdenVenta1 { get; set; }
        public virtual CondicionesVentas CondicionesVentas { get; set; }
        public virtual ICollection<SuscripcionesPagos> SuscripcionesPagos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class DetalleCart
    {
        #region Properties

        public List<DetalleViewModel> Items { get; set; }

        public decimal PercepcionIVA { get; set; }
        public decimal PercepcionIIBB { get; set; }
        public decimal ImporteNoGrabado { get; set; }
        public int IDJuresdiccion { get; set; }
        

        #endregion

        public static DetalleCart Instance;

        public static DetalleCart Retrieve()
        {
            if (HttpContext.Current.Session["ASPNETDetalleCart"] == null)
            {
                Instance = new DetalleCart();
                Instance.Items = new List<DetalleViewModel>();
                HttpContext.Current.Session["ASPNETDetalleCart"] = Instance;
            }
            else
            {
                Instance = (DetalleCart)HttpContext.Current.Session["ASPNETDetalleCart"];
            }

            return Instance;
        }

        // A protected constructor ensures that an object can't be created from outside  
        protected DetalleCart() { }

        #region Reporting Methods

        public decimal GetSubTotal()
        {
            return DetalleCart.Retrieve().Items.Sum(x => x.TotalSinIva);
        }

        public decimal GetIva()
        {            
            return DetalleCart.Retrieve().Items.Where(x => x.Iva > 0).Sum(x => (x.TotalConIva - x.TotalSinIva));
        }

        public decimal GetTotal()
        {
            return (GetIva() + GetSubTotal() + GetImporteNoGrabado() + GetPercepcionIVA() + GetPercepcionIIBB());
        }

        #endregion

        #region PERCEPCIONES

        public decimal GetPercepcionIVA()
        {
            return ((PercepcionIVA > 0) ? ((GetSubTotal() * PercepcionIVA) / 100) : 0);
        }
        
        public decimal GetPercepcionIIBB()
        {
            return ((PercepcionIIBB > 0) ? ((GetSubTotal() * PercepcionIIBB) / 100) : 0);
        }
        
        public decimal GetImporteNoGrabado()
        {
            return ImporteNoGrabado;
        }

        #endregion
    }
}
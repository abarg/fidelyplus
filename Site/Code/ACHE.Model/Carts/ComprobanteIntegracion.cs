﻿using System.Web;


namespace ACHE.Model
{
    public class ComprobanteIntegracion
    {

        public static Comprobantes Retrieve()
        {
            return (Comprobantes)HttpContext.Current.Session["ASPNETComprobanteIntegracion"]; ;
        }

        public static void Dispose()
        {
              HttpContext.Current.Session["ASPNETComprobanteIntegracion"]=null;
        }

        public ComprobanteIntegracion(Comprobantes comprobante)
        {
            HttpContext.Current.Session["ASPNETComprobanteIntegracion"] = comprobante;
        }
    }
}
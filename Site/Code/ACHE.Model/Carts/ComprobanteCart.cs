﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Model.Negocio;

namespace ACHE.Model
{
    public class ComprobanteCart
    {
        #region Properties
        public int IDComprobante { get; set; }
        public int IDUsuario { get; set; }
        public int IDPersona { get; set; }
        public int IDPuntoVenta { get; set; }
        public string Modo { get; set; }
        public string TipoComprobante { get; set; }
        public DateTime FechaComprobante { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string CondicionVenta { get; set; }
        public string TipoConcepto { get; set; }
        public string Numero { get; set; }
        public string Observaciones { get; set; }
        public List<ComprobantesDetalleViewModel> Items { get; set; }
        public List<ComprobantesTributosViewModel> Tributos { get; set; }

        public decimal PercepcionIVA { get; set; }
        public decimal PercepcionIIBB { get; set; }
        public string JurisdiccionIIBB { get; set; }
        public decimal ImporteNoGrabado { get; set; }
        public int IDJuresdiccion { get; set; }
        public int? IDIntegracion { get; set; }
        public string IDVentaIntegracion { get; set; }
        public string NombrePersona { get; set; }
        public string EmailPersona { get; set; }
        public string NroDocumentoPersona { get; set; }
        public string TipoDocumentoPersona { get; set; }
        public string RazonSocialPersona { get; set; }
        public string CondicionIvaPersona { get; set; }
        public string PuntoDeVentaStr { get; set; }
        public int IDCondicionVenta { get; set; }
        public int? IdOrdenVenta { get; set; }

        #endregion

        public static ComprobanteCart Instance;

        public static void Dispose()
        {
            HttpContext.Current.Session["ASPNETComprobanteCart"] = null;
        }

        public static ComprobanteCart Retrieve()
        {
            if (HttpContext.Current.Session["ASPNETComprobanteCart"] == null)
            {
                Instance = new ComprobanteCart();
                Instance.Items = new List<ComprobantesDetalleViewModel>();
                Instance.Tributos = new List<ComprobantesTributosViewModel>();
                HttpContext.Current.Session["ASPNETComprobanteCart"] = Instance;
            }
            else
            {
                Instance = (ComprobanteCart)HttpContext.Current.Session["ASPNETComprobanteCart"];
            }

            return Instance;
        }

        // A protected constructor ensures that an object can't be created from outside  
        public ComprobanteCart() { }

        #region Reporting Methods

        public decimal GetSubTotal()
        {
            return ComprobanteCart.Retrieve().Items.Sum(x => x.TotalSinIva);
        }

        public decimal GetGravado()
        {
            return ComprobanteCart.Retrieve().Items.Where(x => x.Iva > 0).Sum(x => x.TotalSinIva);
        }

        public decimal GetIva()
        {
            //return ComprobanteCart.Retrieve().Items.Where(x => x.Iva > 0).Sum(x => (x.TotalSinIva * x.Iva / 100));
            return ComprobanteCart.Retrieve().Items.Where(x => x.Iva > 0).Sum(x => (x.TotalConIva - x.TotalSinIva));
        }

        public decimal GetTotal()
        {
            return (GetIva() + GetSubTotal() + GetPercepcionIVA() + GetPercepcionIIBB());//+ GetImporteNoGrabado()
        }

        #endregion

        #region PERCEPCIONES

        public decimal GetPercepcionIVA()
        {
            return ((PercepcionIVA > 0) ? ((GetSubTotal() * PercepcionIVA) / 100) : 0);
        }

        public decimal GetPercepcionIIBB()
        {
            return ((PercepcionIIBB > 0) ? ((GetSubTotal() * PercepcionIIBB) / 100) : 0);
        }

        public decimal GetImporteNoGrabado()
        {
            return ComprobanteCart.Retrieve().Items.Where(x => x.Iva == 0).Sum(x => x.TotalSinIva);
        }

        #endregion

        public ComprobanteCartDto GetComprobanteCartDto()
        {
            return new ComprobanteCartDto()
            {
                IDUsuario = IDUsuario,
                IDComprobante = IDComprobante,
                Modo = Modo,
                TipoComprobante = TipoComprobante,
                CondicionVenta = CondicionVenta,
                IDCondicionVenta=IDCondicionVenta,
                TipoConcepto = TipoConcepto,
                Numero = Numero,
                Observaciones = Observaciones,
                FechaComprobante = FechaComprobante,
                FechaVencimiento = FechaVencimiento,
                IDJuresdiccion = IDJuresdiccion,
                IDPersona = IDPersona,
                IDPuntoVenta = IDPuntoVenta,
                IdIntegracion = IDIntegracion,
                Items = Items,
                PercepcionIVA = PercepcionIVA,
                PercepcionIIBB = PercepcionIIBB,
                ImporteNoGrabado = ImporteNoGrabado,
                NombrePersona = NombrePersona,
                EmailPersona = EmailPersona,
                RazonSocialPersona = RazonSocialPersona,
                NroDocumentoPersona = NroDocumentoPersona,
                TipoDocumentoPersona = TipoDocumentoPersona,
                CondicionIvaPersona = CondicionIvaPersona,
                IdVentaIntegracion = IDVentaIntegracion,
                PuntoDeVentaStr = PuntoDeVentaStr,
                IdOrdenVenta = IdOrdenVenta
            };
        }

        public void Init(Comprobantes comprobante)
        {
            IDComprobante = comprobante.IDComprobante;
            IDUsuario = comprobante.IDUsuario;
            IDPersona = comprobante.IDPersona;
            IDPuntoVenta = comprobante.IDPuntoVenta;
            Modo = comprobante.Modo;
            TipoComprobante = comprobante.Tipo;
            IDVentaIntegracion = comprobante.IDVentaIntegracion;
            FechaComprobante = comprobante.FechaComprobante;
            FechaVencimiento = comprobante.FechaVencimiento;
            CondicionVenta = comprobante.CondicionVenta;
            IDCondicionVenta = comprobante.IDCondicionVenta;
            TipoConcepto = comprobante.TipoConcepto.ToString();
            Numero = comprobante.Numero.ToString();
            Observaciones = comprobante.Observaciones;
            IdOrdenVenta = comprobante.IDOrdenVenta;
            Items = comprobante.ComprobantesDetalle.Select(c => new ComprobantesDetalleViewModel(2)
            {
                ID = c.IDDetalle,
                Concepto = c.Concepto,
                Cantidad = c.Cantidad,
                IDConcepto = c.IDConcepto,
                IDAbonos = c.IDAbono,
                IDPlanDeCuenta = c.IDPlanDeCuenta,
                PrecioUnitario = c.PrecioUnitario,
                Iva = c.Iva,
                Bonificacion = c.Bonificacion,
                Codigo = c.IDConcepto.HasValue && c.Conceptos != null ? c.Conceptos.Codigo : "",
            }).ToList();
            Tributos = comprobante.ComprobantesTributos.Select(i => new ComprobantesTributosViewModel()
            {
                Tipo = i.Tipo,
                Descripcion = i.Descripcion,
                Alicuota = i.Iva,
                BaseImp = i.BaseImp,
                Importe = i.Importe
            }).ToList();
            PercepcionIVA = comprobante.PercepcionIVA;
            PercepcionIIBB = comprobante.PercepcionIIBB;
            ImporteNoGrabado = comprobante.ImporteNoGrabado;
            IDJuresdiccion = comprobante.IDJurisdiccion ?? 0;
            IDIntegracion = comprobante.IDIntegracion;
            PuntoDeVentaStr = ((comprobante.PuntosDeVenta != null) ? comprobante.PuntosDeVenta.Punto : 0).ToString("#000");
            NombrePersona = (comprobante.Personas != null) ? comprobante.Personas.NroDocumento + " - " + comprobante.Personas.RazonSocial : "";
            EmailPersona = (comprobante.Personas != null) ? comprobante.Personas.Email : "";
            RazonSocialPersona = (comprobante.Personas != null) ? comprobante.Personas.RazonSocial : "";
            NroDocumentoPersona = (comprobante.Personas != null) ? comprobante.Personas.NroDocumento : "";
            TipoDocumentoPersona = (comprobante.Personas != null) ? comprobante.Personas.TipoDocumento : "";
            CondicionIvaPersona = (comprobante.Personas != null) ? comprobante.Personas.CondicionIva : "";
        }
    }
}
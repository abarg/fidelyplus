﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model {

  public class MovimientosStockCart 
    {
        #region Properties
        public int ID { get; set; }
        public int IDUsuario { get; set; }
        public int IDInventarioOrigen { get; set; }
        public int IDInventarioDestino { get; set; }
        public DateTime FechaMovimiento { get; set; }
        public string Observaciones { get; set; }
        public List<MovimientoDetalleViewModel> Items { get; set; }
        #endregion

        public static MovimientosStockCart Instance;

        public static void Dispose() {
            HttpContext.Current.Session["ASPNETMovimientoStockCart"] = null;
        }

        public static MovimientosStockCart Retrieve() {
            if (HttpContext.Current.Session["ASPNETMovimientoStockCart"] == null) {
                Instance = new MovimientosStockCart();
                Instance.Items = new List<MovimientoDetalleViewModel>();
                HttpContext.Current.Session["ASPNETMovimientoStockCart"] = Instance;
            }
            else {
                Instance = (MovimientosStockCart)HttpContext.Current.Session["ASPNETMovimientoStockCart"];
            }

            return Instance;
        }

    }
}

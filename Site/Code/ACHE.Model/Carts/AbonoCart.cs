﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    public class AbonoCart
    {
        #region Properties

        public List<AbonosDetalleViewModel> Items { get; set; }

        public decimal PercepcionIVA { get; set; }
        public decimal PercepcionIIBB { get; set; }
        public decimal ImporteNoGrabado { get; set; }
        public int IDJuresdiccion { get; set; }
        

        #endregion

        public static AbonoCart Instance;

        public static AbonoCart Retrieve()
        {
            if (HttpContext.Current.Session["ASPNETAbonoCart"] == null)
            {
                Instance = new AbonoCart();
                Instance.Items = new List<AbonosDetalleViewModel>();
                HttpContext.Current.Session["ASPNETAbonoCart"] = Instance;
            }
            else
            {
                Instance = (AbonoCart)HttpContext.Current.Session["ASPNETAbonoCart"];
            }

            return Instance;
        }

        // A protected constructor ensures that an object can't be created from outside  
        protected AbonoCart() { }

        #region Reporting Methods

        public decimal GetSubTotal()
        {
            return AbonoCart.Retrieve().Items.Sum(x => x.TotalSinIva);
        }

        public decimal GetIva()
        {            
            return AbonoCart.Retrieve().Items.Where(x => x.Iva > 0).Sum(x => (x.TotalConIva - x.TotalSinIva));
        }

        public decimal GetTotal()
        {
            return (GetIva() + GetSubTotal() + GetImporteNoGrabado() + GetPercepcionIVA() + GetPercepcionIIBB());
        }

        #endregion

        #region PERCEPCIONES

        public decimal GetPercepcionIVA()
        {
            return ((PercepcionIVA > 0) ? ((GetSubTotal() * PercepcionIVA) / 100) : 0);
        }
        
        public decimal GetPercepcionIIBB()
        {
            return ((PercepcionIIBB > 0) ? ((GetSubTotal() * PercepcionIIBB) / 100) : 0);
        }
        
        public decimal GetImporteNoGrabado()
        {
            return ImporteNoGrabado;
        }

        #endregion
    }
}
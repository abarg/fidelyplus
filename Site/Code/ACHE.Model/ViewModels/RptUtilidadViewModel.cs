﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for RptProductosPorClinteViewModel
    /// </summary>
    public class RptUtilidadViewModel
    {
        public decimal MontoVenta { get; set; }

        public string MontoVentaStr {
            get { return MontoVenta.ToString("N2");}
        }
        public decimal IvaVenta { get; set; }

        public string IvaVentaStr
        {
            get { return IvaVenta.ToString("N2"); }
        }
        public decimal MontoCompra { get; set; }

        public string MontoCompraStr
        {
            get { return MontoCompra.ToString("N2"); }
        }
        public decimal IvaCompra { get; set; }

        public string IvaCompraStr
        {
            get { return IvaCompra.ToString("N2"); }
        }
        public int Mes { get; set; }

        public decimal TotalVenta { get; set; }
        

        public string TotalVentaStr
        {
            get { return TotalVenta.ToString("N2"); }
        }
        public decimal TotalCompra { get; set; }
        public string TotalCompraStr
        {
            get { return TotalCompra.ToString("N2"); }
        }
        public decimal Total
        {
            get { return TotalVenta - TotalCompra; }
        }
        public string TotalStr
        {
            get { return Total.ToString("N2"); }
        }

        public decimal TotalIva
        {
            get { return IvaVenta - IvaCompra; }
        }
        public string TotalIvaStr
        {
            get { return TotalIva.ToString("N2"); }
        }
        public string Nombre
        {
            get
            {
                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                return dtinfo.GetMonthName(Mes);
            }
        }
    }

    public class ResultadoRptUtilidadViewModel
    {
        public List<RptUtilidadViewModel> Items;

        private int CantItemsCompra { get { return Items.Where(i => i.MontoCompra != 0).Count(); } }

        private int CantItemsIvaCompra { get { return Items.Where(i => i.IvaCompra != 0).Count(); } }
        private int CantItemsVenta { get { return Items.Where(i => i.MontoVenta != 0).Count(); } }
        private int CantItemsIvaVenta { get { return Items.Where(i => i.IvaVenta != 0).Count(); } }
        private int CantItemsTotal
        {
            get { return Items.Where(i => i.Total != 0).Count(); }
        }
        private int CantItemsTotalIva
        {
            get { return Items.Where(i => i.TotalIva != 0).Count(); }
        }

        public decimal TotalMontoVenta { get; set; }
        public string TotalMontoVentaPromedio
        {
            get
            {
                if (CantItemsVenta == 0) return 0.ToString("N2");
                return (TotalMontoVenta / CantItemsVenta).ToString("N2");
            }
        }
        public string TotalMontoVentaStr {
            get { return TotalMontoVenta.ToString("N2"); }
        }
        public decimal TotalIvaVenta { get; set; }
        public string TotalIvaVentaStr {
            get { return TotalIvaVenta.ToString("N2"); }
        }
        public string TotalIvaVentaPromedio
        {
            get
            {
                if (CantItemsIvaVenta == 0) return 0.ToString("N2");
                return (TotalIvaVenta / CantItemsIvaVenta).ToString("N2");
            }
        }
        public decimal TotalVenta { get; set; }
        public string TotalVentaPromedio
        {
            get
            {
                if (CantItemsVenta == 0) return 0.ToString("N2");
                return (TotalVenta / CantItemsVenta).ToString("N2");
            }
        }
        public string TotalVentaStr
        {
            get { return TotalVenta.ToString("N2"); }
        }
        public decimal TotalMontoCompra { get; set; }
        public string TotalMontoCompraPromedio
        {
            get
            {
                if (CantItemsCompra == 0) return 0.ToString("N2");
                return (TotalMontoCompra / CantItemsCompra).ToString("N2");
            }
        }
        public string TotalMontoCompraStr
        {
            get { return TotalMontoCompra.ToString("N2"); }
        }
        public decimal TotalIvaCompra { get; set; }
        public string TotalIvaCompraPromedio
        {
            get
            {
                if (CantItemsIvaCompra == 0) return 0.ToString("N2");
                return (TotalIvaCompra / CantItemsIvaCompra).ToString("N2");
            }
        }
        public string TotalIvaCompraStr
        {
            get { return TotalIvaCompra.ToString("N2"); }
        }
        public decimal TotalCompra { get; set; }
        public string TotalCompraPromedio
        {
            get
            {
                if (CantItemsCompra == 0) return 0.ToString("N2");
                return (TotalCompra / CantItemsCompra).ToString("N2");
            }
        }
        public string TotalCompraStr
        {
            get { return TotalCompra.ToString("N2"); }
        }
        public decimal TotalIva {
            get { return TotalIvaVenta - TotalIvaCompra; }
        }
        public string TotalIvaPromedio
        {
            get
            {
                if (CantItemsTotalIva == 0) return 0.ToString("N2");
                return (TotalIva / CantItemsTotalIva).ToString("N2");
            }
        }
        public string TotalIvaStr
        {
            get { return TotalIva.ToString("N2"); }
        }
        public decimal TotalGanancia {
            get { return TotalVenta-TotalCompra;}
        }
        public string TotalGananciaPromedio
        {
            get
            {
                if (CantItemsTotal == 0) return 0.ToString("N2");
                return (TotalGanancia / CantItemsTotal).ToString("N2");
            }
        }
        public string TotalGananciaStr
        {
            get { return TotalGanancia.ToString("N2"); }
        }
    }

}
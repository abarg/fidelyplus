﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RptAltasMensuales
    {
        public int Cantidad { get; set; }
        public int Mes { get; set; }
        public int Totales { get; set; }
        public int Bajas { get; set; }
        public int Pagos { get; set; }
        public int Registros { get; set; }
        public decimal Conversiones { get; set; }
        public int Impagos { get; set; }
        public string NombreMes
        {
            get
            {
                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                return dtinfo.GetMonthName(Mes);
            }
        }
    }

    public class ResultadosRptAltasMensuales
    {
        public IList<RptAltasMensuales> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

        public int TotalAltas { get; set; }
        public int TotalBajas { get; set; }
        public int TotalImpagos { get; set; }
    }

    public class RptDetalleAltasMensuales
    {
        public int IDUsuario { get; set; }
        public string RazonSocial { get; set; }
        public string Motivo { get; set; }
        public System.DateTime FechaUltLogin { get; set; }
        public System.DateTime FechaVencimiento { get; set; }
        public string FechaVenc
        {
            get
            {
                return FechaVencimiento.ToString("dd/MM/yyyy");
            }
        }
        public string FechaUltimoLogin
        {
            get
            {
                return FechaUltLogin.ToString("dd/MM/yyyy");
            }
        }


        public string NombrePlan { get; set; }

        public string CUIT { get; set; }
        public string Email { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public System.DateTime FechaDePago { get; set; }
        public string Fecha
        {
            get
            {
                return FechaDePago.ToString("dd/MM/yyyy");
            }
        }
        public string FechaRegistro
        {
            get
            {
                return FechaAlta.ToString("dd/MM/yyyy");
            }
        }
        public System.DateTime FechaBaja { get; set; }
        public string FechaDeBaja {
            get {
                return FechaBaja.ToString("dd/MM/yyyy");
            }
        }
        public string Nombre { get; set; }
        public decimal ImportePagado { get; set; }
        public string CodigoPromo { get; set; }
        public int MesesPagados { get; set; }



    }

    public class ResultadosRptDetalleAltasMensuales
    {
        public IList<RptDetalleAltasMensuales> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public decimal ImporteTotal {
            get {
                return Items.Sum(x=>x.ImportePagado);
            }
        }

    }

}

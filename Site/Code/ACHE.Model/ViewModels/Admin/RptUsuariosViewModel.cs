﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels {
    public class RptUsuariosViewModel {
        public int ID { get; set; }
        public int IDPadre { get; set; }
        public string RazonSocial { get; set; }
        public string Email { get; set; }
        public int AntiguedadMeses { get; set; }
        public string CodigoPromo { get; set; }
        public string PlanActual { get; set; }
        public string IntegracionesActivas { get; set; }
        public string TipoUsuario { get; set; }
        public string FechaBaja { get; set; }
        public string MotivoBaja { get; set; }


        //public string Rentabilidad {
        //    get {
        //        if (Pagos > 0) {
        //            //return decimal.Parse((Pagos * 100 / Registros).ToString()).ToString("0.00");
        //            return (Pagos * 100 / decimal.Parse(Registros.ToString())).ToString("0.00");
        //        }
        //        return "0";
        //    }
        //}


    }
    public class ResultadosUsuariosViewModel {
        public IList<RptUsuariosViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RptUsuariosPlanes
    {
        public int IDUsuario { get; set; }
        public string RazonSocial { get; set; }
        public string CUIT { get; set; }
        public string Email { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public string FechaRegistro {
            get {
                return FechaAlta.ToString("dd/MM/yyyy");
            }
        }
        public System.DateTime FechaFinPlan { get; set; }
        public string FechaVencimiento {
            get {
                return FechaFinPlan.ToString("dd/MM/yyyy");
            }
        }
        public int MesesPagados { get; set; }
        public string FormaDePago { get; set; }


    }





    public class ResultadosRptUsuariosPlanes
    {
        public IList<RptUsuariosPlanes> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

}

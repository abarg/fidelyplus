﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RptPromocionesViewModel
    {
        public string CodigoPromo { get; set; }
        public int Registros { get; set; }
        public int Pagos {get;set;}
        public int Bajas { get; set; }
        public int Impagos { get; set; }
        public string Rentabilidad {
            get {
                if (Pagos > 0) {
                    //return decimal.Parse((Pagos * 100 / Registros).ToString()).ToString("0.00");
                    return (Pagos * 100 / decimal.Parse(Registros.ToString())).ToString("0.00");
                }
                return "0";
            }
        }


    }
    public class ResultadosPromocionesViewModel
    {
        public IList<RptPromocionesViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RptRealtrendsViewModel
    {
        public int IDUsuario { get; set; }
        public string RazonSocial { get; set; }
        public string FechaDePago { get; set; }
        public string Nombre { get; set; }
        public decimal Precio { get; set; }
        public decimal ImportePagado { get; set; }
        public string ApodoML { get; set; }
        public decimal ComisionRT { get; set; }
        public decimal ComisionInLat { get; set; }
    }
    public class ResultadosRptRealtrendsViewModel
    {
        public IList<RptRealtrendsViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

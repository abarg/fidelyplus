﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for AbonosDetalleViewModel
    /// </summary>
    public class AbonosDetalleViewModel
    {
        #region Atributos privados
        private byte CantidadDecimales { get; set; }
        #endregion

        #region Constructores
        public AbonosDetalleViewModel(byte cantidadDecimales)
        {
            this.CantidadDecimales = cantidadDecimales;
        }
        #endregion

        public int ID { get; set; }
        public string Concepto { get; set; }
        public decimal Cantidad { get; set; }
        public int? IDConcepto { get; set; }     
        public int IDAbono { get; set; }
        public int? IDPlanDeCuenta { get; set; }
        public string NombreCuenta { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Iva { get; set; }
        public decimal Bonificacion { get; set; }
        public string Codigo { get; set; }
        public string CodigoPlanCta { get; set; }
        public decimal PrecioUnitarioSinIVA
        {
            get
            {
                decimal subTotal = PrecioUnitario;

                if (Bonificacion > 0)
                    subTotal = subTotal - ((subTotal * Bonificacion) / 100);
               
                return subTotal;
            }
        }

        public decimal PrecioUnitarioConIva
        {
            get
            {
                decimal subTotal = PrecioUnitario;

                if (Bonificacion > 0)
                    subTotal = subTotal - ((subTotal * Bonificacion) / 100);

                if (Iva > 0)
                    subTotal = ((subTotal) + ((subTotal * Iva) / 100));
               
                return subTotal;
            }
        }

        public decimal TotalSinIva
        {
            get
            {
                decimal subTotal = Cantidad * PrecioUnitarioSinIVA;
                return Math.Round(subTotal, this.CantidadDecimales);
            }
        }

        public decimal TotalConIva
        {
            get
            {  
                decimal subTotal = Cantidad * PrecioUnitarioConIva;    
                return Math.Round(subTotal, this.CantidadDecimales);
            }
        }
    }
}
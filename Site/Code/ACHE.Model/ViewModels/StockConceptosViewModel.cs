﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ACHE.Model
{
    public class StockConceptosViewModel
    {
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public decimal Cantidad { get; set; }
    }

    public class ResultadoslistaStockConceptosViewModel
    {
        public IList<StockConceptosViewModel> Conceptos;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

    public class DetalleReservasViewModel
    {

        public int IDUsuario { get; set; }
        public int IDConcepto { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int IDOrdenVenta { get; set; }
        public int IDIntegracion { get; set; }
        public string NroOrden { get; set; }
        public string Fecha { get; set; }
        public string Estado { get; set; }
        public string Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Bonificacion { get; set; }
    }

    public class ResultadosDetalleReservasViewModel
    {
        public IList<DetalleReservasViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

    //public class StockDto {
    //    public decimal StockActual { get; set; }
    //    public decimal StockReservado { get; set; }

    //    public decimal StockConReservas
    //    {
    //        get { return StockActual - StockReservado; }
    //    }
    //}
}

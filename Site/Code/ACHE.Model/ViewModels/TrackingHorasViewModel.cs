﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for BancosViewModel
    /// </summary>
    public class TrackingHorasViewModel
    {
        public int ID { get; set; }
        public string RazonSocial { get; set; }
        public string Fecha { get; set; }
        public string DesdeHasta { get; set; }
        public string Duracion { get; set; }
        public string ValorHora { get; set; }
        public string Tarea { get; set; }
        public string Observaciones { get; set; }
        public string Estado { get; set; }
        public string IncluirEnAbono { get; set; }
        public string NombreUsuario { get; set; }

        public decimal CantHorasFacturables { get; set; }
        public decimal CantHorasNOFacturables { get; set; }
        public decimal SubTotal { get; set; }
        public string SubTotalStr { get { return SubTotal.ToString("N2"); } }
      
    }

    public class ResultadosTrackingHorasViewModel
    {
        public IList<TrackingHorasViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

        public decimal TotalHorasFacturables { get { return (Items.Sum(x => x.CantHorasFacturables)); } }
        public decimal TotalHorasNoFacturables { get { return (Items.Sum(x => x.CantHorasNOFacturables)); } }

        public decimal Total { get { return (Items.Sum(x => x.SubTotal)); } }
        public string TotalStr { get { return Total.ToString("N2"); } }
        
    }
}
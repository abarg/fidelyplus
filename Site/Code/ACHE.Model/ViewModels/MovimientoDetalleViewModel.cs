﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ACHE.Model {
    public class MovimientoDetalleViewModel {
        public int IDMovimientoDeStockDetalle { get; set; }
        public int IDMovimiento { get; set; }
        public int IDConcepto { get; set; }
        public decimal CantidadTransferida { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public string StockActual { get; set; }
    }
}

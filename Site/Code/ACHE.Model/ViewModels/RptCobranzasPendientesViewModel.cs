﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;

namespace ACHE.Model.ViewModels
{
    public class RptCobranzasPendientesViewModel
    {
        public string Fecha { get; set; }
        public string FechaVencimiento { get; set; }
        public string RazonSocial { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string NroFactura { get; set; }
        public string CondicionIVA { get; set; }
        public string Iva { get; set; }
        public string importeTotal { get; set; }
        public string Importe { get; set; }
        public string ACuenta { get; set; }
        public string Saldo { get; set; }
        public string Estado { get; set; }
        public int IDComprobante { get; set; }
        public int IDPersona { get; set; }
    }

    public class ResultadosRptCobranzasPendientesViewModel
    {
        public IList<RptCobranzasPendientesViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

        public string TotalGeneral { get { return Items.Sum(x => decimal.Parse(x.Saldo)).ToMoneyFormat(2); } }
    }

    public class RptSaldosViewModel
    {
        public int IDPersona { get; set; }
        public int IDUsuario { get; set; }
        public string RazonSocial { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string CondicionIVA { get; set; }
        public decimal SaldoInicial { get; set; }
        public decimal Saldo { get; set; }
        public string SaldoDesc { get; set; }
        public decimal CotNC { get; set; }
        public decimal CobranzasACuenta { get; set; }
        public decimal NCSinAplicar { get; set; }
    }

    public class ResultadosRptSaldosViewModel
    {
        public IList<RptSaldosViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string TotalGeneral { get; set; }
        //public string TotalGeneral { get { return Items.Sum(x => decimal.Parse(x.Saldo)).ToMoneyFormat(2); } } No funciona en paginacion
    }

    public class RptSaldosProvViewModel
    {
        public int IDPersona { get; set; }
        public int IDUsuario { get; set; }
        public string RazonSocial { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string CondicionIVA { get; set; }
        public decimal SaldoInicial { get; set; }
        public decimal Saldo { get; set; }
        public string SaldoDesc { get; set; }
        public decimal CotNC { get; set; }
        public decimal PagosACuenta { get; set; }
        public decimal NCSinAplicar { get; set; }
    }

}
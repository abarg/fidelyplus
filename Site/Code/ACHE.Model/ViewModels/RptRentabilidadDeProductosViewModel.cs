﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Extensions;
namespace ACHE.Model.ViewModels
{
    /// <summary>
    /// Summary description for RptProductosPorClienteViewModel
    /// </summary>
    public class RptRentabilidadDeProductosViewModel
    {
        public decimal Rentabilidad { get; set; }
        public int IDConcepto { get; set; }
        public string Codigo { get; set; }
        public string Concepto { get; set; }
        public int CantidadVendidas { get; set; }
        public int CantidadComprados { get; set; }
        public decimal PromPrecioVenta { get; set; }
        public decimal PromPrecioCompra { get; set; }
        public decimal Iva { get; set; }
        public string TotalCompraStr
        {
            get { return PromPrecioCompra.ToString("N2"); }
        }

        public string TotalVentaStr
        {
            get { return PromPrecioVenta.ToString("N2"); }
        }
        public string RentabilidadPromedio
        {
            get { return Rentabilidad.ToString("N2"); }
        }
        public string RentabilidadPesos
        {
            get { return ((PromPrecioVenta - PromPrecioCompra) * CantidadVendidas).ToString("N2"); }
        }

        public decimal RentabilidadPesosDecimal
        {
            get { return ((PromPrecioVenta - PromPrecioCompra) * CantidadVendidas); }
        }
    }

    /// <summary>
    /// Summary description for RptProductosPorClienteViewModel
    /// </summary>
    public class RptRentabilidadDeProductosDetViewModel
    {
        public decimal Rentabilidad { get; set; }
        public int IDConcepto { get; set; }
        public string Codigo { get; set; }
        public string Concepto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal PrecioUnit { get; set; }
        public decimal PrecioCompra { get; set; }
        public decimal Iva { get; set; }

        public string FechaStr
        {
            get { return Fecha.ToString("dd/MM/yyyy"); }
        }
        public string PrecioCompraStr
        {
            get { return PrecioCompra.ToString("N2"); }
        }

        public string PrecioUnitStr
        {
            get { return PrecioUnit.ToString("N2"); }
        }
        public string RentabilidadPromedio
        {
            get { return Rentabilidad.ToString("N2"); }
        }
        public string RentabilidadPesos { get; set; }
        public decimal RentabilidadPesosDecimal { get; set; }
        //get { return ((PrecioUnit - PrecioCompra) * Cantidad).ToString("N2"); }
        //public decimal RentabilidadPesosDecimal
        //{
        //    get { return ((PrecioUnit - PrecioCompra) * Cantidad); }
        //}
        public DateTime Fecha { get; set; }
        public string Comprobante { get; set; }
        public string Observaciones { get; set; }
    }


    /// <summary>
    /// Summary description for ResultadosRptProductosPorClienteViewModel
    /// </summary>
    public class ResultadosRptRentabilidadDeProductosViewModel
    {
        public IList<RptRentabilidadDeProductosViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string TotalRentabilidad { get; set; }
        public string TotalGeneral { get { return Items.Sum(x => x.RentabilidadPesosDecimal).ToMoneyFormat(2); } }
    }

    public class ResultadosRptRentabilidadDeProductosDetViewModel
    {
        public IList<RptRentabilidadDeProductosDetViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string TotalRentabilidad { get; set; }
        public string TotalGeneral { get { return Items.Sum(x => x.RentabilidadPesosDecimal).ToMoneyFormat(2); } }
    }

}

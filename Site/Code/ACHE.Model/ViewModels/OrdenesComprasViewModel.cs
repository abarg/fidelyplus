﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class OrdenesComprasViewModel
    {
        public int ID { get; set; }
        public int IDPersona { get; set; }
        public string Fecha { get; set; }
        public string FechaEntrega { get; set; }
        public string Total { get; set; }
        public string Proveedor { get; set; }
        public string Area { get; set; }
        public string Solicitante { get; set; }
        public string NumeroOrden { get; set; }
        public string Estado { get; set; }
        public string TotalNeto { get; set; }

    }
    public class ResultadosOrdenesViewModel
    {
        public IList<OrdenesComprasViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

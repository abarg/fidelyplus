﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class ConfiguracionViewModel
    {
        public string Nombre { get; set; }
        public string Valor { get; set; }

    }
}

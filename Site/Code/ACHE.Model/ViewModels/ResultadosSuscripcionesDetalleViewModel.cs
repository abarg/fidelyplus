﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;

namespace ACHE.Model.ViewModels {

    //public class ResultadosSuscripcionesDetalleViewModel {
    //    public List<SuscripcionDetalleViewModel> Suscripcion;
    //    public int TotalPage { get; set; }
    //    public int TotalItems { get; set; }
    //}

    //public class SuscripcionDetalleViewModel {
    //    public int IDSuscripcion { get; set; }
    //    public string NombreSuscripcion { get; set; }
    //    public int CantidadPersonas { get; set; }
    //    public string ProximaFechaV { get; set; }
    //    public string TotalDebitar { get; set; }
    //    public string TotalDebitado { get; set; }
    //    public string Estado { get; set; }
    //    public List<SuscripcionesPersonasDetalleViewModel> Personas { get; set; }
    //}

    //public class SuscripcionesPersonasDetalleViewModel {
    //    public int IDSuscripcion { get; set; }
    //    public int IDSuscripcionPersona { get; set; }
    //    public string NombrePersona { get; set; }
    //    public int CantidadDebitos { get; set; }
    //    public string Estado { get; set; }
    //    public List<SuscripcionesPagosDetalleViewModel> Pagos { get; set; }
    //}

    //public class SuscripcionesPagosDetalleViewModel {
    //    public int IDSuscripcion { get; set; }
    //    public int IDSuscripcionPersona { get; set; }
    //    public int IDSuscripcionPago { get; set; }
    //    public string PeriodoPago { get; set; }
    //    public string TotalADebitar{ get; set; }
    //    public string TotalDebitado { get; set; }
    //    public string Estado { get; set; }
    //}

    public class ResultadosSuscripcionesDetalleViewModel {
        public List<SuscripcionDetalleViewModel> Suscripcion;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

    public class SuscripcionDetalleViewModel {
        public string Periodo { get; set; }
        public string TotalDebitar { get; set; }
        public string TotalDebitado { get; set; }
        public string PersonasDebitar { get; set; }
        public string PeriodoExport { get; set; }
        public string AnoExport { get; set; }
        public string Estado { get; set; }
        public string EstadoStyle { get; set; }


        public List<SuscripcionesPersonasDetalleViewModel> Pagos { get; set; }
    }

    public class SuscripcionesPersonasDetalleViewModel {
        public int IDSuscripcionPago { get; set; }
        public string Periodo { get; set; }
        public string NombrePersona { get; set; }
        public string TotalDebitar { get; set; }
        public string TotalDebitado { get; set; }
        public string Estado { get; set; }
    }

    public class SuscripcionesPersonasExportDetalleViewModel {
        public string Periodo { get; set; }
        public string NombrePersona { get; set; }
        public string TotalDebitar { get; set; }
        public string TotalDebitado { get; set; }
        public string Estado { get; set; }
    }








}
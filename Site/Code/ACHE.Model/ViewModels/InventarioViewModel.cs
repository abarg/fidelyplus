﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model.ViewModels {
    public class InventarioViewModel {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public int IDProvincia { get; set; }
        public int IDCiudad { get; set; }
        public string Direccion { get; set; }
        public string Activo { get; set; }
        public string PorDefectoReservas { get; set; }
        public string Telefono { get; set; }
        public string FechaUltimoInventario { get; set; }
        public string FechaAlta { get; set; }
        public int IDUsuario { get; set; }
    }

    public class ResultadosInventarioViewModel {
        public IList<InventarioViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

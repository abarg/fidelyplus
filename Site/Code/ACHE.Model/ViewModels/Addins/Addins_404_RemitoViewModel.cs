﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for Addins_404_RemitoViewModel
    /// </summary>
    public class Addins_404_RemitoViewModel
    {
        public int ID { get; set; }
        public string RazonSocial { get; set; }
        public string Nombre { get; set; }
        public string Fecha { get; set; }
        public string Numero { get; set; }
        public string ActoContractual { get; set; }
        public string OrdenDeCompra { get; set; }
    }

    /// <summary>
    /// Summary description for ResultadosAddins_404_RemitoViewModel
    /// </summary>
    public class ResultadosAddins_404_RemitoViewModel
    {
        public IList<Addins_404_RemitoViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}
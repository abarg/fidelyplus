﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for RptCcViewModel
    /// </summary>
    public class RptCcViewModel
    {
        public int IDPersona { get; set; }
        public string FechaUltPago { get; set; }
        public string Estado { get; set; }
        public string RazonSocial { get; set; }
        public string Importe { get; set; }
        public string Abonado { get; set; }
        public string Saldo { get; set; }
    }

    /// <summary>
    /// Summary description for ResultadosRptIvaViewModel
    /// </summary>
    public class ResultadosRptCcViewModel
    {
        public IList<RptCcViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

    public class CtaCteProveedorDto
    {
        public string Tipo { get; set; }
        public decimal TotalImpuestos { get; set; }
        public decimal Total { get; set; }
        public decimal Iva { get; set; }
        public string NroFactura { get; set; }
        public int NroPago { get; set; }
        public DateTime Fecha { get; set; }
        public int IDCompra { get; set; }
        public decimal Importe { get; set; }
        public decimal Saldo { get; set; }
        public string Observaciones { get; set; }
    }

    public class CtaCteClienteDto
    {
        public string Tipo { get; set; }
        public string Observaciones { get; set; }
        public int PuntoVenta { get; set; }
        public int Numero { get; set; }
        public decimal ImporteTotalNeto { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Saldo { get; set; }
        public int IDComprobante { get; set; }
        public decimal Importe { get; set; }
    }
}
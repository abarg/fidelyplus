﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for ComprobantesTributosViewModel
    /// </summary>
    public class ComprobantesTributosViewModel
    {
        #region Constructores

        public ComprobantesTributosViewModel()
        {
        }

        #endregion

        public int ID { get; set; }
        public int Tipo { get; set; }
        public string Descripcion { get; set; }
        public decimal BaseImp { get; set; }
        public decimal Alicuota { get; set; }
        public decimal Importe { get; set; }
    }
}
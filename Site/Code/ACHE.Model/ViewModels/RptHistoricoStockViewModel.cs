﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model
{
   public class RptHistoricoStockViewModel
    {
        public string Tipo { get; set; }
        //public string RazonSocial { get; set; }
        //public string TipoDocumento { get; set; }
        //public string NroDocumento { get; set; }
        public string Factura { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public string Numero { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Cantidad { get; set; }
        public decimal StockActualizado { get; set; }
        public string CantModificada { get; set; }
        //public decimal CostoInterno { get; set; }
        //public decimal Bonificacion { get; set; }
        //public decimal PrecioUnitario { get; set; }
        public string FechaStr { get { return Fecha.ToString("dd/MM/yyyy HH:mm").Replace("00:00",""); } }
        public int IdFactura { get; set; }
        /*public decimal Total
        {
            get { return ((PrecioUnitario- ((PrecioUnitario * Bonificacion)/100)) * Cantidad ); }
        }
        public string TotalStr
        {
            get { return Total.ToString("N2"); }
        }
        public string PrecioUnitarioStr
        {
            get { return PrecioUnitario.ToString("N2"); }
        }
        public string CostoInternoStr
        {
            get { return CostoInterno.ToString("N2"); }
        }*/
        public string Deposito { get; set; }
        public string Observaciones { get; set; }
    }

   public class ResultadosRptHistoricoStockViewModel
   {
       public IList<RptHistoricoStockViewModel> Items;
       public int TotalPage { get; set; }
       public int TotalItems { get; set; }

       
   }
}

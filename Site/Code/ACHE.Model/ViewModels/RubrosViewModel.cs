﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RubrosViewModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string RubroPadre { get; set; }
        public int? IDRubroPadre { get; set; }

    }
    public class ResultadosRubrosViewModel
    {
        public IList<RubrosViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

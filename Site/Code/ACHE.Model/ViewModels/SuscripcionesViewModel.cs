﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model {
    /// <summary>
    /// Summary description for AbonosViewModel
    /// </summary>
    public class SuscripcionesViewModel {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Frecuencia { get; set; }
        public string Precio { get; set; }
        public string Iva { get; set; }
        public string Total { get; set; }
        public string UltimoPeriodo { get; set; }
        //public string UltimoPeriodoAno { get; set; }
        //public string FechaProximoVencimiento { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Estado { get; set; }
        public string CantClientes { get; set; }
    }

    public class ResultadosSuscripcionesViewModel {
        public IList<SuscripcionesViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

    public class SuscripcionesPersonasViewModel {
        public int IDSuscripcion { get; set; }
        public int IDPersona { get; set; }
        //public string Cantidad { get; set; }
        public string RazonSocial { get; set; }
        public string Total { get; set; }
        public string CBU { get; set; }
        public string Estado { get; set; }
    }

    public class SuscripcionesPersonasModeloViewModel {
        public int IDSuscripcion { get; set; }
        public int IDPersona { get; set; }
        public string Cantidad { get; set; }
        public string RazonSocial { get; set; }
        public string Total { get; set; }
        public string Estado { get; set; }
        public string CBU { get; set; }
    }
    public class SuscripcionesDetalleModeloViewModel {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Frecuencia { get; set; }
        public string Precio { get; set; }
        public string Iva { get; set; }
        public string Total { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Estado { get; set; }
        public string CantClientes { get; set; }
        public string Observaciones { get; set; }
        public int IDCondicionVenta { get; set; }
        public int IDCuentaContable { get; set; }
        public bool Detalles { get; set; }
    }

    public class SuscripcionViewModel {
        public int IDSuscripcion { get; set; }
        public int? IDAbono { get; set; }
        public int IDUsuario { get; set; }
        public string Integracion { get; set; }
        public int DiaVencimiento { get; set; }
        public bool UsaFacturaElectronica { get; set; }
        public string Nombre { get; set; }
        public string Frecuencia { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Estado { get; set; }
        public string Precio { get; set; }
        public string Iva { get; set; }
        public string Observaciones { get; set; }
        public int IDCondicionVenta { get; set; }
        public int IDPlanDeCuenta { get; set; }
        public List<SuscripcionesPersonasViewModel> Personas { get; set; }
    }


    public class SuscripcionesModeloViewModel {
        public SuscripcionesDetalleModeloViewModel Detalles { get; set; }
        public IEnumerable<SuscripcionesPersonasModeloViewModel> Personas { get; set; }
    }


    //public class OrdenPPT {
    //    public int idSuscripcionPago { get; set; }
    //    public string estado { get; set; }
    //    public string fechaPago { get; set; }
    //}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Extensions;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for RptProductosPorClienteViewModel
    /// </summary>
    public class RptProductosPorClienteViewModel
    {
        public string Cliente { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Total { get; set; }
        public int IDCliente { get; set; }
        public string Concepto { get; set; }

        public string TotalStr
        {
            get { return Total.ToString("N2"); }
        }
    }
  

    /// <summary>
    /// Summary description for ResultadosRptProductosPorClienteViewModel
    /// </summary>
    public class ResultadosRptProductosPorClienteViewModel
    {
        public IList<RptProductosPorClienteViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

        public string TotalGeneral { get { return Items.Sum(x => x.Total).ToMoneyFormat(4); } }
    }

    public class RptProductosPorClienteDetalleViewModel
    {
        public string RazonSocial { get; set; }
        public string Fecha { get; set; }
        public string Comprobante { get; set; }
        public decimal Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Total { get; set; }
    }
}
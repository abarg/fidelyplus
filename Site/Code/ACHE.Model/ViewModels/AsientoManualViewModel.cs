﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{

    public class AsientoManualViewModel {
        public int IDAsiento { get; set; }
        public string Fecha { get; set; }
        public string NroAsiento { get; set; }
        public string Leyenda { get; set; }
        public string AsientoDebe { get; set; }
        public string AsientoHaber { get; set; }
        public string TotalDebe { get; set; }
        public string TotalHaber { get; set; }
    }

    public class ResultadosAsientoManualViewModel
    {
        public IList<AsientoManualViewModel> Asientos;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string TotalDebe { get; set; }
        public string TotalHaber { get; set; }
    }

    public class AsientoManualExportacionViewModel
    {
        public string NroAsiento { get; set; }
        public string Fecha { get; set; }
        public string Leyenda { get; set; }
        public string Debe { get; set; }
        public string Haber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class rptVentasDelMesViewModel
    {
        public decimal MontoVenta { get; set; }

        public string MontoVentaStr
        {
            get { return MontoVenta.ToString("N2"); }
        }
        public decimal IvaVenta { get; set; }

        public string IvaVentaStr
        {
            get { return IvaVenta.ToString("N2"); }
        }
        public decimal TotalVenta { get; set; }


        public string TotalVentaStr
        {
            get { return TotalVenta.ToString("N2"); }
        }
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Anio { get; set; }
        public string Nombre
        {
            get
            {
                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                DateTime day = new DateTime(Anio, Mes, Dia);
                DayOfWeek aux = day.DayOfWeek;
                return dtinfo.GetDayName(aux) + " - " + day.ToString("dd/MM");
            }
        }
    }
    public class ResultadoRptVentasDelMesViewModel
    {
        public List<rptVentasDelMesViewModel> Items;


        private int CantItemsVenta { get { return Items.Where(i => i.MontoVenta != 0).Count(); } }
        private int CantItemsIvaVenta { get { return Items.Where(i => i.IvaVenta != 0).Count(); } }

        public decimal TotalMontoVenta { get; set; }
        public string TotalMontoVentaPromedio
        {
            get
            {
                if (CantItemsVenta == 0) return 0.ToString("N2");
                return (TotalMontoVenta / CantItemsVenta).ToString("N2");
            }
        }
        public string TotalMontoVentaStr
        {
            get { return TotalMontoVenta.ToString("N2"); }
        }
        public decimal TotalIvaVenta { get; set; }
        public string TotalIvaVentaStr
        {
            get { return TotalIvaVenta.ToString("N2"); }
        }
        public string TotalIvaVentaPromedio
        {
            get
            {
                if (CantItemsIvaVenta == 0) return 0.ToString("N2");
                return (TotalIvaVenta / CantItemsIvaVenta).ToString("N2");
            }
        }
        public decimal TotalVenta { get; set; }
        public string TotalVentaPromedio
        {
            get
            {
                if (CantItemsVenta == 0) return 0.ToString("N2");
                return (TotalVenta / CantItemsVenta).ToString("N2");
            }
        }
        public string TotalVentaStr
        {
            get { return TotalVenta.ToString("N2"); }
        }
    }

}

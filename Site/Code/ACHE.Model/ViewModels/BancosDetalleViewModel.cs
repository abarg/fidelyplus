﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for BancosDetalleViewModel
    /// </summary>
    public class BancosDetalleViewModel
    {
        public int ID { get; set; }
        public string tipoMovimiento { get; set; }
        public string Concepto { get; set; }
        public decimal Importe { get; set; }
        public string Observaciones { get; set; }
        public string Fecha { get; set; }
        public string Ingreso { get; set; }
        public string Egreso { get; set; }
        public string Ticket { get; set; }
        public string PuedeEditar { get; set; }
        public int IDPlanDeCuenta { get; set; }
        public int IDBanco { get; set; }
        public string NombreBanco { get; set; }
        public string idCategoria { get; set; }
    }

    public class ResultadosBancosDetalleViewModel
    {
        public IList<BancosDetalleViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}
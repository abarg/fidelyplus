﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for AbonosViewModel
    /// </summary>
    public class AbonosAGenerarViewModel
    {
        #region Atributos privados
        private byte CantidadDecimales { get; set; }
        #endregion

        #region Constructores
        public AbonosAGenerarViewModel(byte cantidadDecimales)
        {
            this.CantidadDecimales = cantidadDecimales;
        }
        #endregion

        public int IDPersona { get; set; }
        public string RazonSocial { get; set; }
        public string ClienteEmail { get; set; }
        //public string Observaciones { get; set; }
        public string Cuit { get; set; }
        public string CondicionIva { get; set; }
        public decimal Importe { get; set; }
        public decimal Iva { get; set; }
        public string ivaCalculado { get { return ((((Importe * Iva) / 100))).ToString(String.Format("N{0}", this.CantidadDecimales)); } }
        public string nroRegistro { get; set; }
        public string nroComprobante { get; set; }
        public string Estado { get; set; }
        public string FEGenerada { get; set; }
        public string FEGeneradaObs { get; set; }
        public string URL { get; set; }
        public string EnvioFE { get; set; }
        public decimal Total { get { return ((Importe + ((Importe * Iva) / 100)) * Cantidad); } }
        public string FrecuenciaAbono { get; set; }
        public string CondicionVenta { get; set; }
        public DateTime FechaInicio { get; set; }
        public string ImportaPantalla { get { return Importe.ToString(String.Format("N{0}", this.CantidadDecimales)); } }
        public string TotalPantalla { get { return Total.ToString(String.Format("N{0}", this.CantidadDecimales)); } }
        public int IDCondicionVenta { get; set; }
        public int Cantidad { get; set; }
    }

    public class AbonosAGenerarGrupoViewModel
    {
        #region Atributos privados
        private byte CantidadDecimales { get; set; }
        #endregion

        #region Constructores
        public AbonosAGenerarGrupoViewModel(byte cantidadDecimales)
        {
            this.CantidadDecimales = cantidadDecimales;
        }
        #endregion

        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Observaciones { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime? FechaFin { get; set; }
        public int tipoConcepto { get; set; }
        public int Cantidad { get { return (Items.Count); } }
        public int? IDPlanDeCuenta { get; set; }
        public string TotalCant { get { return (Items.Sum(x => x.Total)).ToString(String.Format("N{0}", this.CantidadDecimales)); } }

        public IList<AbonosDetalleViewModel> Detalles;

        public IList<AbonosAGenerarViewModel> Items;
    }

    public class ResultadosAbonosAGenerarViewModel
    {
        #region Atributos privados
        private byte CantidadDecimales { get; set; }
        #endregion

        #region Constructores
        public ResultadosAbonosAGenerarViewModel(byte cantidadDecimales)
        {
            this.CantidadDecimales = cantidadDecimales;
        }
        #endregion

        public IList<AbonosAGenerarGrupoViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

        public string TotalIva { get { return (Items.Sum(x => x.Items.Sum(y => Convert.ToDecimal(y.ivaCalculado) * y.Cantidad))).ToString(String.Format("N{0}", this.CantidadDecimales)); } }
        public string Total { get { return (Items.Sum(x => x.Items.Sum(y => y.Total))).ToString(String.Format("N{0}", this.CantidadDecimales)); } }
        public string totalImporte { get { return (Items.Sum(x => x.Items.Sum(y => y.Importe * y.Cantidad))).ToString(String.Format("N{0}", this.CantidadDecimales)); } }
    }
}
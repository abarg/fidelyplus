﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class ContenidoArchivoViewModel
    {
        public string Logo { get; set; }
        public List<Total> Totales { get; set; }
        public List<DetalleCabecera> Cabecera { get; set; }
        public string TituloReporte { get; set; }
        public List<string> UltimaFila { get; set; }
        public float[] Widths { get; set; } 

    }
    public class Total
    {
        public string Nombre{get;set;}
        public string Importe{get;set;}

    }
    public class DetalleCabecera {
        public string Titulo{get;set;}
        public string Valor{get;set;}
    }

}

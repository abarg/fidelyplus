﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public  class SicoreViewModel
    {
        public string NombreArchivo { get; set; }
        public string URL { get; set; }
        public string Errores { get; set; }
        public string Observaciones { get; set; }
        public string Nombre { get; set; }
    }

   
    public class ResultadosSicoreViewModel
    {
        public IList<SicoreViewModel> Items;
    }



    public class RegInfoSicore
    {
        public string CodigoComprobante { get; set; }
        public string FechaEmisionComprobante { get; set; }
        public string NumeroComprobante { get; set; }
        public string ImporteComprobante { get; set; }
        public string CodigoImpuesto { get; set; }
        public string CodigoRegimen { get; set; }
        public string CodigoOperacion { get; set; }
        public string BaseCalculo { get; set; }
        public string FechaEmisionRetencion { get; set; }
        public string CodigoCondicion { get; set; }
        public string RetencionPracticadaSegun { get; set; }
        public string ImporteRetencion { get; set; }
        public string PorcentajeExclusion { get; set; }
        public string FechaEmisionBoletin { get; set; }
        public string TipoDocumentoDelRetenido { get; set; }
        public string NumeroDocumentoDelRetenido { get; set; }
        public string NumeroCertificadoOriginal { get; set; }
        public string DenominacionDelOrdenante { get; set; }
        public string Acrecentamiento { get; set; }
        public string CuitPaisRetenido { get; set; }
        public string CuitOrdenante { get; set; }
    }

    public class RegInfoArciba
    {
        public string TipoOperacion { get; set; }
        public string CodigoNorma { get; set; }
        public string FechaRetPers { get; set; }
        public string TipoComp { get; set; }
        public string Letra { get; set; }
        public string NroComprobante { get; set; }
        public string FechaComprobante { get; set; }
        public string MontoComprobante { get; set; }
        public string NroCertificado { get; set; }
        public string TipoDocRetenido { get; set; }
        public string NroDocRetenido { get; set; }
        public string SituacionIB { get; set; }
        public string NroInscripcion { get; set; }
        public string SituacionFrenteALIVA { get; set; }
        public string RazonSocialRetenido { get; set; }
        public string ImporteOtrosConceptos { get; set; }
        public string ImporteIVA { get; set; }
        public string MontoSujetoRetPer { get; set; }
        public string Alicuota { get; set; }
        public string RetPercPracticada { get; set; }
        public string MontoTotalRetPerc { get; set; }
    }
    public class RegInfoArcibaNC
    {
        public string TipoOperacion { get; set; }
        public string NroNC { get; set; }
        public string FechaNC { get; set; }
        public string MontoNC { get; set; }
        public string NroCertificadoPropio { get; set; }
        public string TipoComprobanteOrigen { get; set; }
        public string LetraComprobante { get; set; }
        public string NroComprobante { get; set; }
        public string NroDocumentoRetenido { get; set; }
        public string CodigoNorma { get; set; }
        public string FechaRetencion { get; set; }
        public string RetPercepADeducir { get; set; }
        public string Alicuota { get; set; }

    }
}

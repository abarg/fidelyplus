﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for AbonosViewModel
    /// </summary>
    public class ComprobantesDescargasViewModel
    {
        public string Comprobante { get; set; }
        public string Remito { get; set; }
        public string Observaciones { get; set; }
        public int IDComprobante { get; set; }
    }
}
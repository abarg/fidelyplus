﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for RptIvaVentasViewModel
    /// </summary>
    public class RptIvaVentasViewModel
    {
        public string Fecha { get; set; }
        public string NroFactura { get; set; }
        public string RazonSocial { get; set; }
        public string Cuit { get; set; }
        public string CondicionIVA { get; set; }
        public string Iva { get; set; }
        public string Importe { get; set; }
        public string Total { get; set; }
        public string NoGrav { get; set; }
        public string IVA27 { get; set; }
        public string IVA21 { get; set; }
        public string IVA10 { get; set; }
        public string IVA5 { get; set; }
        public string IVA2 { get; set; }
        public string IIBB { get; set; }
        public string OtrosImp { get; set; }

        public string Servicios { get; set; }
        public string Productos { get; set; }
    }

    /// <summary>
    /// Summary description for ResultadosRptIvaVentasViewModel
    /// </summary>
    public class ResultadosRptIvaVentasViewModel
    {
        public IList<RptIvaVentasViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }

        public string TotalIva { get; set; }
        public string TotalImporte { get; set; }
        public string TotalTotal { get; set; }
        public string TotalNoGrav { get; set; }
        public string TotalIVA27 { get; set; }
        public string TotalIVA21 { get; set; }
        public string TotalIVA10 { get; set; }
        public string TotalIVA5 { get; set; }
        public string TotalIVA2 { get; set; }
        public string TotalIIBB { get; set; }
        public string TotalOtrosImp { get; set; }

        public string TotalServicios { get; set; }
        public string TotalProductos { get; set; }

        public string TotalRI { get; set; }
        public string TotalMO { get; set; }
        public string TotalEX { get; set; }
        public string TotalCF { get; set; }
    }
}
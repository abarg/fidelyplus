﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Model.ViewModels;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for RptIvaVentasViewModel
    /// </summary>
    public class VentaIntegracionViewModel
    {
        public string Id { get; set; }
        public DateTime? Fecha { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public string Estado { get; set; }
        public string EstadoEnvio { get; set; }
        public string NombreComprador { get; set; }
        public string ApellidoComprador { get; set; }
        //public string Usuario { get; set; }
        public string Tel { get; set; }
        public string Apodo { get; set; }
        public string PrecioTotal { get; set; }
        public string PrecioEnvio { get; set; }
        public string DescuentoTotal { get; set; }
        public string FechaStr { get { return Fecha.HasValue?Fecha.Value.ToString("dd/MM/yyyy"):""; } }
        public string Email { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public List<VentaIntegracionDetalleViewModel> Items { get; set; }
        public long? IdComprador { get; set; }
        public int IDIntegracion { get; set; }
        public string CiudadEnvioNombre { get; set; }
        public string ProvinciaEnvioId { get; set; }
        public string Domicilio { get; set; }
        public string PisoDepto { get; set; }
        public string CodigoPostal { get; set; }
        public bool FacturadaElectronica { get; set; }
        public bool FacturadaBorrador { get; set; }
        public string MetodoPago { get; set; }
        //public string TipoPago { get; set; }
        public int IDComprobante { get; set; }
        public string EstadoFacturacion { get; set; }
        public string IDOrdenVenta { get; set; }
        public string Origen { get; set; }
        public long? IDShipping { get; set; }
        public long? IDPack { get; set; }
      
    }
    public class VentaIntegracionDetalleViewModel
    {
        public string Concepto { get; set; }
        public double PrecioUnitario { get; set; }
        public int Cantidad { get; set; }
        public string Codigo { get; set; }
        public double Bonificacion { get; set; }
      
    }
    /// <summary>
    /// Summary description for ResultadosRptIvaVentasViewModel
    /// </summary>
    public class ResultadosVentaIntegracionViewModel
    {
        public IList<VentaIntegracionViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
    public class ConfiguracionPorIntegracionViewModel
    {
        public int IDIntegracion { get; set; }
        public IList<ConfiguracionViewModel> configuraciones { get; set; }
    }
}
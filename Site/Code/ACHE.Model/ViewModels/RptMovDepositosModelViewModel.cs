﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model.ViewModels {
    public class RptMovDepositosModelViewModel
    {
        public int ID { get; set; }
        public string Fecha { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public string Producto { get; set; }
        public string Cantidad { get; set; }
        public int IDUsuario { get; set; }
    }

    public class ResultadosRptMovDepositosModelViewModel
    {
        public IList<RptMovDepositosModelViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

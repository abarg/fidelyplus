﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RptComprasPorCategoriaMensualViewModel
    {
        public decimal TotalCompra { get; set; }
        public int  IDCategoria { get; set; }
        public string Categoria { get; set; }

        public string TotalCompraStr
        {
            get { return TotalCompra.ToString("N2"); }
        }
        public int Mes { get; set; }

        public string Nombre
        {
            get
            {
                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                return dtinfo.GetMonthName(Mes);
            }
        }
    }
}

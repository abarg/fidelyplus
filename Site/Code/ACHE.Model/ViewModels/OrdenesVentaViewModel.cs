﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class OrdenesVentaViewModel
    {
        public int ID { get; set; }
        public int IDPersona { get; set; }
        public int IDComprobante { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaVencimiento { get; set; }
        public string Total { get; set; }
        public string Comprador { get; set; }
        public string NumeroOrden { get; set; }
        public string Estado { get; set; }
        public string TotalNeto { get; set; }
        public string Integracion { get; set; }

    }
    public class ResultadosOrdenesVentaViewModel
    {
        public IList<OrdenesVentaViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

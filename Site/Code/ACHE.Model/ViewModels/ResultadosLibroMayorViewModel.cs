﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;

namespace ACHE.Model.ViewModels
{
    public class DetalleComprobantesMayorViewModel
    {
        public string Fecha { get; set; }
        public string Leyenda { get; set; }
        //public string DebeString { get { return this.Debe.ToMoneyFormat(4); } }
        //public string HaberString { get { return this.Haber.ToMoneyFormat(4); } }
        //public string SaldoString { get { return this.Saldo.ToMoneyFormat(4); } }

        public string Debe { get; set; }
        public string Haber { get; set; }
        public string Saldo { get; set; }
    }

    public class DetalleComprobantesMayorAuxViewModel
    {
        public string Fecha { get; set; }
        public string Leyenda { get; set; }
        //public string DebeString { get { return this.Debe.ToMoneyFormat(4); } }
        //public string HaberString { get { return this.Haber.ToMoneyFormat(4); } }
        //public string SaldoString { get { return this.Saldo.ToMoneyFormat(4); } }

        public decimal Debe { get; set; }
        public decimal Haber { get; set; }
        public decimal Saldo { get; set; }
        public decimal Total { get; set; }
    }


    public class CuentasViewModel
    {
        public bool MostrarResultado { get; set; }
        public int IDPlanDeCuenta { get; set; }
        public string IDPadre { get; set; }
        public string nroCuenta { get; set; }
        public string NombreCuenta { get; set; }
        public string TotalDebe { get; set; }
        public string TotalHaber { get; set; }
        public string TotalDeudor { get; set; }
        public string TotalAcreedor { get; set; }
        public string TotalActivo { get; set; }
        public string TotalPasivo { get; set; }
        public string TotalPerdidas { get; set; }
        public string TotalPN { get; set; }
        public string TotalGanancias { get; set; }
        public string Saldo { get; set; }
        public string TipoDeCuenta { get; set; }
        public bool EsPadre { get; set; }


        public IList<DetalleComprobantesMayorViewModel> Items;
    }

    public class ResultadosLibroMayorViewModel
    {
        public IList<CuentasViewModel> Asientos;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string TotalDebe { get; set; }
        public string TotalHaber { get; set; }
        public string TotalDeudor { get; set; }
        public string TotalAcreedor { get; set; }
        public string TotalActivo { get; set; }
        public string TotalPasivo { get; set; }
        public string TotalPerdidas { get; set; }
        public string TotalGanancias { get; set; }
        public string TotalSaldo { get; set; }
    }

    public class ResultadosBalanceResultadosViewModel
    {
        public IList<CuentasViewModel> Asientos;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public decimal TotalDebe { get; set; }
        public decimal TotalHaber { get; set; }
        public decimal TotalDeudor { get; set; }
        public decimal TotalAcreedor { get; set; }
        public decimal TotalActivo { get; set; }
        public decimal TotalPasivo { get; set; }
        public decimal TotalPN { get; set; }
        public decimal TotalPerdidas { get; set; }
        public decimal TotalGanancias { get; set; }
        public decimal TotalSaldo { get; set; }
    }

    public class ResultadosBalanceGeneralViewModel
    {
        public IList<CuentasViewModel> Asientos;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string TotalDebe { get; set; }
        public string TotalHaber { get; set; }
        public string TotalDeudor { get; set; }
        public string TotalAcreedor { get; set; }
        public string TotalActivo { get; set; }
        public string TotalPasivo { get; set; }
        public string TotalPN { get; set; }
        public string TotalPerdidas { get; set; }
        public string TotalGanancias { get; set; }
        public string TotalSaldo { get; set; }
    }

    public class LibroMayorExport
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Fecha { get; set; }
        public string Leyenda { get; set; }
        public string Debe { get; set; }
        public string Haber { get; set; }
        public string Saldo { get; set; }
    }
}
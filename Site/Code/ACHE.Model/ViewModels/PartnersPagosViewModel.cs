﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class PartnersPagosViewModel
    {
        public string Usuario {get;set;}
        public string Fecha {get;set;}
        public string Plan {get;set;}
        public string Total {get;set;}
        public string Porcentaje {get;set;}
        public string Comision {get;set;}
        public string Saldo {get;set;}
        public string FechaPago {get;set;}

    }
}

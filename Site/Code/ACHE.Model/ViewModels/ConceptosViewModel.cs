﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for ProductosViewModel
    /// </summary>
    public class ConceptosViewModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public string CodigoOem { get; set; }
        public string CodigoBarras { get; set; }
        public string Descripcion { get; set; }
        public string Precio { get; set; }
        public string Stock { get; set; }
        public string StockReservado { get; set; }
        public string StockConReservas { get; set; }
        //public string Provincia { get; set; }
        public string Estado { get; set; }
        public string Iva { get; set; }
        public string Tipo { get; set; }
        public int idPlanDeCuentaCompra { get; set; }
        public int idPlanDeCuentaVenta { get; set; }
        public string Foto { get; set; }
        public string CostoInterno { get; set; }
    }

    public class ResultadosProductosViewModel
    {
        public IList<ConceptosViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }


    /// <summary>
    /// Summary description for ProductosViewModel
    /// </summary>
    public class PreciosHistorialViewModel
    {
        public int ID { get; set; }
        public string Proveedor { get; set; }
        public string Precio { get; set; }
        public string Fecha { get; set; }
        public string CostoInterno { get; set; }
        public string Rentabilidad { get; set; }
    }

    public class ConceptosReservadosViewModel
    {
        public int ID { get; set; }
        public string NroOrden { get; set; }
        public decimal Cantidad { get; set; }
        public string Origen { get; set; }
        public string Cliente { get; set; }
        public DateTime Fecha { get; set; }
        public bool PuedeEliminar { get; set; }
        public string ComboDesc { get; set; }
        public int IDCombo { get; set; }
    }

    public class ResultadosPreciosHistorialViewModel
    {
        public IList<PreciosHistorialViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}
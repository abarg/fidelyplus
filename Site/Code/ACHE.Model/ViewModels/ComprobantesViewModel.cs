﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for ComprobantesViewModel
    /// </summary>
    public class ComprobantesViewModel
    {
        public int ID { get; set; }
        public string RazonSocial { get; set; }
        public string Fecha { get; set; }
        public string Numero { get; set; }
        public string Tipo { get; set; }
        public string Modo { get; set; }
        public string ImporteTotalNeto { get; set; }
        public string ImporteTotalBruto { get; set; }
        public string PuedeAdm { get; set; }
        public string PuedeAnular { get; set; }
        public string Resultado { get; set; }
        public string MensajeResultado { get; set; }
        public string CondicionVenta { get; set; }
        public int IdPersona { get; set; }
        public int TipoConcepto { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string Observaciones { get; set; }
        public int IdPuntoVenta { get; set; }
        public string Cae { get; set; }
        public decimal Saldo { get; set; }
        public decimal Iva { get;set;}
        public PersonasViewModel Persona { get; set; }
        public PersonasViewModel Usuario { get; set; }
        public string RutaMercadoPago { get; set; }
        public PayUForm PayUForm { get; set; }
        public List<ComprobantesDetalleViewModel> Detalle { get; set; }
        public string Estado { get; set; }
        public string RutaDescarga { get; set; }
    
        public string MetodoPago { get; set; }
        public int IDComprobante { get; set; }
        public string EstadoFacturacion { get; set; }
        public int IDCondicionVenta { get; set; }
    }

    /// <summary>
    /// Summary description for ResultadosComprobantesViewModel
    /// </summary>
    public class ResultadosComprobantesViewModel
    {
        public IList<ComprobantesViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

    public class PayUForm
    {
        public string MerchantId { get; set; }
        public string AccountId { get; set; }
        public string Signature { get; set; }
        public string ReferenceCode { get; set; }
        
    }


    public class ResultadosImportacionesComprobantesViewModel
    {
       
        public IList<ComprobantesViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }

   

    /// <summary>
    /// Summary description for ComprobantesEditViewModel
    /// </summary>
    public class ComprobantesEditViewModel
    {
        public int ID { get; set; }
        public int IDPersona { get; set; }
        public string Fecha { get; set; }
        public string FechaVencimiento { get; set; }
        public string Numero { get; set; }
        public string Tipo { get; set; }
        public int TipoConcepto { get; set; }
        public string Modo { get; set; }
        public string CondicionVenta { get; set; }
        public int? IDCondicionVenta { get; set; }

        public int IDPuntoVenta { get; set; }
        public string Observaciones { get; set; }
        public int IDInventario { get; set; }
        public int IDOrdenVenta { get; set; }
        //public int? IDIntegracion { get; set; }
        //public string IDVentaIntegracion{ get; set; }
        public PersonasEditViewModel Personas { get; set; }
    }

    public class PersonasEditViewModel
    {
        public int ID { get; set; }
        public string RazonSocial { get; set; }
        public string NombreFantasia { get; set; }
        public string CondicionIva { get; set; }
        public string TipoDoc { get; set; }
        public string NroDoc { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string Domicilio { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public bool? esAgenteRetencion { get; set; }

        public string PercepcionIIBB { get; set; }
        public int IDJuresdiccion { get; set; }
        public string PercepcionIVA { get; set; }
        public string ImporteNoGrabado { get; set; }
    }
}
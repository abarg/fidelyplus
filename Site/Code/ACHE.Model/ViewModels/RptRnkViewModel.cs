﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for RptRnkViewModel
    /// </summary>
    public class RptRnkViewModel
    {
        public string Valor1 { get; set; }
        public string Valor2 { get; set; }
        public string Cantidad { get; set; }
        public string Cantidad2 { get; set; }
        public string Cantidad3 { get; set; }
        public string Precio { get; set; }
        public string Total { get; set; }
        public string CostoInterno { get; set; }
        public string CostoTotal { get; set; }
        public string Inventario { get; set; }
        public string Rubro { get; set; }
        public string SubRubro { get; set; }
        public string TipoConcepto { get; set; }
    }

    public class RptRnkViewModelItems {
        public string Codigo { get; set; }
        public string Tipo { get; set; }
        public string Estado { get; set; }
        public string Descripcion { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Importe { get; set; }        
        public string Rubro { get; set; }
        public int IDRubro { get; set; }
        public string SubRubro { get; set; }
        public int IDSubRubro { get; set; }
    }

    /// <summary>
    /// Summary description for ResultadosRptRnkViewModel
    /// </summary>
    public class ResultadosRptRnkViewModel
    {
        public IList<RptRnkViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}
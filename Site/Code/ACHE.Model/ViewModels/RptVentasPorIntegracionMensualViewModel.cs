﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RptVentasPorIntegracionMensualViewModel
    {
        public decimal ImporteTotalBruto { get; set; }
        public decimal ImporteTotalNeto { get; set; }
        public decimal EnvioBruto { get; set; }
        public decimal EnvioNeto { get; set; }
        public int IDIntegracion { get; set; }
        public int Cantidad { get; set; }
        public string Integracion { get; set; }

        public string ImporteTotalBrutoStr
        {
            get { return (ImporteTotalBruto-EnvioBruto).ToString("N2"); }
        }
        public string ImporteTotalNetoStr
        {
            get { return (ImporteTotalNeto-EnvioNeto).ToString("N2"); }
        }
        public int Mes { get; set; }

        public string Nombre
        {
            get
            {
                DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
                return dtinfo.GetMonthName(Mes);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Extensions;

namespace ACHE.Model
{
    /// <summary>
    /// Summary description for RptPagoProvViewModel
    /// </summary>
    public class RptPagoProvViewModel
    {
        public int IDPago { get; set; }
        public int IDCompra { get; set; }
        public string NroPago { get; set; }
        public string Fecha { get; set; }
        public string Proveedor { get; set; }
        public string TipoDocumento { get; set; }
        public string NroDocumento { get; set; }
        public string NroFactura { get; set; }
        public string CondicionIVA { get; set; }
        public string Iva { get; set; }
        public string Importe { get; set; }
        public string Total { get; set; }
        public string TotalAbonado { get; set; }
        public string FechaEmision { get; set; }
        public string FechaPrimerVencimiento { get; set; }
        public string FechaSegundoVencimiento { get; set; }
        public string Saldo { get; set; }
        public string Retenciones { get; set; }
    }

    /// <summary>
    /// Summary description for ResultadosRptPagoProvViewModel
    /// </summary>
    public class ResultadosRptPagoProvViewModel
    {
        public IList<RptPagoProvViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string TotalGeneral { get { return Items.Sum(x => decimal.Parse(x.TotalAbonado)).ToMoneyFormat(2); } }
        public string SaldoGeneral { get { return Items.Sum(x => decimal.Parse(x.Saldo)).ToMoneyFormat(2); } }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Model.ViewModels;

namespace ACHE.Model
{

    public class PublicacionIntegracionViewModel
    {
        public string IdProducto { get; set; }
        public string IdVariante { get; set; }
        public string Titulo { get; set; }
        public string Precio { get; set; }
        public string SKUActual { get; set; }
        public decimal? Stock { get; set; }

        public string Estado { get; set; }

        public int? IdConfiguracionPublicacion { get; set; }
        public int? IdConceptoConfiguracionPublicacion { get; set; }
        public bool? ActualizaPrecio { get; set; }
        public bool? ActualizaStock { get; set; }
        public decimal? PorcentajeAumentoPrecio { get; set; }
        public decimal? PrecioAdicional { get; set; }

        public string StockMinimo { get; set; }
        public string ConceptoContabilium { get; set; }
        public string PrecioContabilium { get; set; }
        public string StockContabilium { get; set; }
        public string FechaSync { get; set; }
    }

    public class ResultadosPublicacionIntegracionViewModel
    {
        public IList<PublicacionIntegracionViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
        public string ScrollId { get; set; }
        public string TipoIntegracion { get; set; }
    }

    public class ActualizarPublicacionDTO
    {
        public string idPublicacion { get; set; }
        public string app { get; set; }
        public string idVariante { get; set; }
        public string estado { get; set; }
        public int cuentaId { get; set; }
        public decimal? stockNuevo { get; set; }
        public decimal? stockPublicacion { get; set; }
        public decimal? precioNuevo { get; set; }
        public decimal precioPublicacion { get; set; }

        public int? stockMinimo { get; set; }
        public string InfoEnviada { get; set; }
        public string InfoRecibida { get; set; }
        public string RespuestaAMostrar { get; set; }
        public string HttpStatus { get; set; }
    }

    public class ActualizarSkuDTO
    {
        public string idPublicacion { get; set; }
        public string app { get; set; }
        public string idVariante { get; set; }
        public string sku { get; set; }
        public int cuentaId { get; set; }
    }

    public class ParametrosActualizarPublicacionDTO
    {
        public string app { get; set; }
        public int cuentaId { get; set; }
        public string idPublicacion { get; set; }
        public ActualizarPublicacionDTO actualizarPublicacion { get; set; }
    }

}
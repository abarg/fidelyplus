﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.ViewModels
{
    public class RptComisionesViewModel
    {
        public int IDUsuarioAdicional { get; set; }
        public string UsuarioAdicional { get; set; }
        public decimal Total { get; set; }
        public decimal Comision { get; set; }
        public string Tipo { get; set; }
        public string Numero { get; set; }
        public string TotalString { get; set; }
        public string Fecha { get; set; }
        public string ComisionString { get; set; }
        public decimal  Porcentaje { get; set; }


    }
    public class ResultadosRptComisionesViewModel
    {
        public IList<RptComisionesViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

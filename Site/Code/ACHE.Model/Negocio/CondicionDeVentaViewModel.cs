﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Negocio
{
    public class CondicionDeVentaViewModel
    {
        public int ID {get;set;}
        public string Nombre { get; set; }
        public string FechaDeAlta { get; set; }
        public string Activa { get; set; }
        public string CobranzaAutomatica { get; set; }
        public string Banco { get; set; }
        public string Caja { get; set; }
        public string FormaDePago { get; set; }
    }
    public class ResultadosCondicionDeVentaViewModel
    {
        public IList<CondicionDeVentaViewModel> Items;
        public int TotalPage { get; set; }
        public int TotalItems { get; set; }
    }
}

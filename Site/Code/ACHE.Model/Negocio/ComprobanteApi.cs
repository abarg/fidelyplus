﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model.Negocio
{
    public class ComprobanteApi
    {
        #region Properties
        
        public string Token { get; set; }
        public int IDComprobante { get; set; }
        public int IDPersona { get; set; }
        public int IDUsuario { get; set; }
        public int IDPuntoVenta { get; set; }
        public string Modo { get; set; }
        public string TipoComprobante { get; set; }
        public DateTime FechaComprobante { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string CondicionVenta { get; set; }
        public string TipoConcepto { get; set; }
        public string Numero { get; set; }
        public string Observaciones { get; set; }
        public List<ComprobantesDetalleViewModel> Items { get; set; }

        public decimal PercepcionIVA { get; set; }
        public decimal PercepcionIIBB { get; set; }
        public decimal ImporteNoGrabado { get; set; }
        public int IDJuresdiccion { get; set; }
        public decimal Tributos { get; set; }

        #endregion

        // A protected constructor ensures that an object can't be created from outside  
        //protected ComprobanteCartDto() { }

        #region Reporting Methods

        public decimal GetSubTotal()
        {
            return Items.Sum(x => x.TotalSinIva);
        }

        public decimal GetIva()
        {
            return Items.Where(x => x.Iva > 0).Sum(x => (x.TotalConIva - x.TotalSinIva));
        }

        public decimal GetTotal()
        {
            return (GetIva() + GetSubTotal() + GetImporteNoGrabado() + GetPercepcionIVA() + GetPercepcionIIBB());
        }
        #endregion

        #region PERCEPCIONES
        public decimal GetPercepcionIVA()
        {
            return ((PercepcionIVA > 0) ? ((GetSubTotal() * PercepcionIVA) / 100) : 0);
        }
        public decimal GetPercepcionIIBB()
        {
            return ((PercepcionIIBB > 0) ? ((GetSubTotal() * PercepcionIIBB) / 100) : 0);
        }
        public decimal GetImporteNoGrabado()
        {
            return ImporteNoGrabado;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ACHE.Model.Negocio
{
    public class SmtpSettings
    {
        public bool TieneAddinMails { get; set; }
        public string Usuario { get; set; }
        public string Contrasenia { get; set; }
        public string Host { get; set; }
        public string Puerto { get; set; }
        public string EmailFrom { get; set; }
        public bool EnableSsl { get; set; }
        public Dictionary<string, string> UserTemplates { get; set; }
    }
}

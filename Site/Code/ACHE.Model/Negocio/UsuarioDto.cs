﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Negocio
{
    public class UsuarioDto
    {
        public int IDUsuarioAdicional { get; set; }
        public string TipoUsuario { get; set; }
        public string Descuento { get; set; }
        public string RazonSocial { get; set; }
        public string Personeria { get; set; }
        public string CUIT { get; set; }
        public string Pwd { get; set; }
        public string Actividad { get; set; }
        public string CodigoPromocion { get; set; }

        public string CondicionIVA { get; set; }
        public string Email { get; set; }
        public string EmailDisplayFrom { get; set; }
        public string Theme { get; set; }
        public string Domicilio { get; set; }
        public string Pais { get; set; }
        public int? IDProvincia { get; set; }
        public int? IDCiudad { get; set; }
        public string Telefono { get; set; }
        public string IIBB { get; set; }
        public string Logo { get; set; }
        public string TemplateFc { get; set; }
        public DateTime? FechaInicio { get; set; }
        public bool TieneFE { get; set; }
        public bool SetupFinalizado { get; set; }
        public int? IDUsuarioPadre { get; set; }
        public bool TieneMultiEmpresa { get; set; }
        public bool ModoQA { get; set; }
        public int IDPlan { get; set; }
        public string EmailAlerta { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public bool? AgentePercepcionIVA { get; set; }
        public bool? AgentePercepcionIIBB { get; set; }
        public bool? AgenteRetencion { get; set; }
        public bool PlanVigente { get; set; }
        public bool UsaFechaFinPlan { get; set; }
        public string ApiKey { get; set; }
        public bool? ExentoIIBB { get; set; }
        public bool UsaPrecioFinalConIVA { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool EnvioAutomaticoComprobante { get; set; }
        public bool EnvioAutomaticoRecibo { get; set; }
        public string IDJurisdiccion { get; set; }
        public bool UsaPlanCorporativo { get; set; }
        public string ObservacionesFc { get; set; }
        public string ObservacionesRemito { get; set; }
        public string ObservacionesPresupuestos { get; set; }
        public string ObservacionesOrdenes { get; set; }
        public decimal? PorcentajeDescuento { get; set; }
        public bool PlanPendienteConfirmacion { get; set; }
        public bool MostrarBonificacionFc { get; set; }
        public bool? MostrarMenuML { get; set; }
        public bool MostrarProductosEnRemito { get; set; }
        public string CodigoPostal { get; set; }
        public string Codigo { get; set; }
        public string TipoIntegracion { get; set; }
        public string CBK { get; set; }

    }
}

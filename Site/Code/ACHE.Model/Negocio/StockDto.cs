﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Model.Negocio
{
    public class StockDto
    {
        public decimal StockActual { get; set; }
        public decimal StockReservado { get; set; }
        public decimal StockConReservas
        {
            get { return (StockActual - StockReservado) < 0 ? 0 : (StockActual - StockReservado); }
        }
    }
}

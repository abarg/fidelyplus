﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model.Negocio
{
    public class ComprobanteCartDto
    {
        #region Properties
        public string Token { get; set; }
        public int IDComprobante { get; set; }
        public int IDPersona { get; set; }
        public int IDUsuario { get; set; }
        public int IDPuntoVenta { get; set; }
        public string Modo { get; set; }
        public string TipoComprobante { get; set; }
        public DateTime FechaComprobante { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string CondicionVenta { get; set; }
        public string TipoConcepto { get; set; }
        public string Numero { get; set; }
        public string Observaciones { get; set; }
        public List<ComprobantesDetalleViewModel> Items { get; set; }
        public List<ComprobantesTributosViewModel> Tributos { get; set; }

        public decimal PercepcionIVA { get; set; }
        public decimal PercepcionIIBB { get; set; }
        public decimal ImporteNoGrabado { get; set; }
        public int IDJuresdiccion { get; set; }
        //public decimal Tributos { get; set; }
        public string IdVentaIntegracion { get; set; }
        public int? IdIntegracion { get; set; }
        public string NombrePersona { get; set; }
        public string EmailPersona { get; set; }
        public string NroDocumentoPersona { get; set; }
        public string TipoDocumentoPersona { get; set; }
        public string RazonSocialPersona { get; set; }
        public string CondicionIvaPersona { get; set; }
        public string PuntoDeVentaStr { get; set; }
        public int? IdInventario { get;set;}

        public int? IDCondicionVenta { get; set; }

        public int? IdOrdenVenta { get; set; }

        public int IDUsuarioAdicional { get; set; }
        public string EmailUsuario { get; set; }

        #endregion

        public ComprobanteCartDto()
        {}
        // A protected constructor ensures that an object can't be created from outside  
        //protected ComprobanteCartDto() { }
        public ComprobanteCartDto(Comprobantes c)
        {
            IDUsuario = c.IDUsuario;
            IDComprobante = c.IDComprobante;
            Modo = c.Modo;
            TipoComprobante = c.Tipo;
            CondicionVenta = c.CondicionVenta;
            IDCondicionVenta = c.IDCondicionVenta;
            TipoConcepto = c.TipoConcepto.ToString();
            Numero = c.Numero.ToString();
            Observaciones = c.Observaciones;
            FechaComprobante = c.FechaComprobante;
            FechaVencimiento = c.FechaVencimiento;
            IDJuresdiccion = c.IDJurisdiccion ?? 0;
            IDPersona = c.IDPersona;
            IDPuntoVenta = c.IDPuntoVenta;
            IdIntegracion = c.IDIntegracion;
            Items = c.ComprobantesDetalle.Select(x => new ComprobantesDetalleViewModel(2)
            {
                ID = x.IDDetalle,
                Concepto = x.Concepto,
                Cantidad = x.Cantidad,
                IDConcepto = x.IDConcepto,
                IDAbonos = x.IDAbono,
                IDPlanDeCuenta = x.IDPlanDeCuenta,
                PrecioUnitario = x.PrecioUnitario,
                Iva = x.Iva,
                Bonificacion = x.Bonificacion,
                Codigo = x.IDConcepto.HasValue && x.Conceptos != null ? x.Conceptos.Codigo : "",
            }).ToList();
            Tributos = c.ComprobantesTributos.Select(x => new ComprobantesTributosViewModel()
            {
                ID = x.IDTributo,
                Tipo = x.Tipo,
                Descripcion = x.Descripcion,
                Alicuota = x.Iva,
                BaseImp = x.BaseImp,
                Importe = x.Importe
            }).ToList();
            PercepcionIVA = c.PercepcionIVA;
            PercepcionIIBB = c.PercepcionIIBB;
            ImporteNoGrabado = c.ImporteNoGrabado;
            NombrePersona = (c.Personas != null) ? c.Personas.NroDocumento + " - " + c.Personas.RazonSocial : "";
            IdVentaIntegracion = c.IDVentaIntegracion;
            PuntoDeVentaStr = ((c.PuntosDeVenta != null) ? c.PuntosDeVenta.Punto : 0).ToString("#000");
            IdInventario = c.IDInventario;
        }

        #region Reporting Methods

        public decimal GetSubTotal()
        {
            return Items.Sum(x => x.TotalSinIva);
        }

        public decimal GetIva()
        {
            return Items.Where(x => x.Iva > 0).Sum(x => (x.TotalConIva - x.TotalSinIva));
        }

        public decimal GetTotal()
        {
            return (GetIva() + GetSubTotal() + GetImporteNoGrabado() + GetPercepcionIVA() + GetPercepcionIIBB());
        }
        #endregion

        public decimal TotalComprobante { get {return GetTotal();} }
        public decimal SubTotalComprobante { get { return GetSubTotal(); } }
        public decimal IvaComprobante { get { return GetIva(); } }

        public string ModoStr { get { return Modo == "E" ? "Electrónica" : Modo; } }
        public string TipoStr { get { return TipoComprobante.StartsWith("FC") ? "Factura " + TipoComprobante.Substring(2) : TipoComprobante; } }
        public string FechaComprobanteStr { get { return (FechaComprobante != null || FechaComprobante!=DateTime.MinValue) ? FechaComprobante.ToString("dd/MM/yyyy") : ""; } }

        #region PERCEPCIONES
        public decimal GetPercepcionIVA()
        {
            return ((PercepcionIVA > 0) ? ((GetSubTotal() * PercepcionIVA) / 100) : 0);
        }
        public decimal GetPercepcionIIBB()
        {
            return ((PercepcionIIBB > 0) ? ((GetSubTotal() * PercepcionIIBB) / 100) : 0);
        }
        public decimal GetImporteNoGrabado()
        {
            return ImporteNoGrabado;
        }

        #endregion
    }

    public class ComprobanteCompletoCartDto
    {
        #region Properties
        public string Token { get; set; }
        public int IDComprobante { get; set; }
        public Personas cliente { get; set; }
        public int IDUsuario { get; set; }
        public int IDPuntoVenta { get; set; }
        public string Modo { get; set; }
        public string TipoComprobante { get; set; }
        public DateTime FechaComprobante { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string CondicionVenta { get; set; }
        public string TipoConcepto { get; set; }
        public string Numero { get; set; }
        public string Observaciones { get; set; }
        public List<ComprobantesDetalleViewModel> Items { get; set; }

        public decimal PercepcionIVA { get; set; }
        public decimal PercepcionIIBB { get; set; }
        public decimal ImporteNoGrabado { get; set; }
        public int IDJuresdiccion { get; set; }
        public decimal Tributos { get; set; }
        #endregion

        // A protected constructor ensures that an object can't be created from outside  
        //protected ComprobanteCartDto() { }

        #region Reporting Methods

        public decimal GetSubTotal()
        {
            return Items.Sum(x => x.TotalSinIva);
        }

        public decimal GetIva()
        {
            return Items.Where(x => x.Iva > 0).Sum(x => (x.TotalConIva - x.TotalSinIva));
        }

        public decimal GetTotal()
        {
            return (GetIva() + GetSubTotal() + GetImporteNoGrabado() + GetPercepcionIVA() + GetPercepcionIIBB());
        }
        #endregion

        #region PERCEPCIONES
        public decimal GetPercepcionIVA()
        {
            return ((PercepcionIVA > 0) ? ((GetSubTotal() * PercepcionIVA) / 100) : 0);
        }
        public decimal GetPercepcionIIBB()
        {
            return ((PercepcionIIBB > 0) ? ((GetSubTotal() * PercepcionIIBB) / 100) : 0);
        }
        public decimal GetImporteNoGrabado()
        {
            return ImporteNoGrabado;
        }

        #endregion
    }
}

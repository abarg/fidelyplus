﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ACHE.Model.Negocio
{
    public class SuscripcionCartDto
    {       
        #region Properties
        public int IDSuscripcion { get; set; }   
        public int? IDAbono { get; set; }      
        public int IDUsuario { get; set; }
        public int IDIntegracion { get; set; }
        public int DiaVencimiento { get; set; }
        public bool UsaFacturaElectronica { get; set; }
        public string Nombre { get; set; }
        public string Frecuencia { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Estado { get; set; }
        public string Precio { get; set; }
        public string Iva { get; set; }
        public string Observaciones { get; set; }
        public int IDCondicionVenta { get; set; }
        public int IDPlanDeCuenta { get; set; }
        public List<SuscripcionesPersonasViewModel> Personas { get; set; }
        //public int Tipo { get; set; }
        //public List<SuscripcionesDetalleViewModel> Items { get; set; }
        //public decimal PercepcionIVA { get; set; }
        //public decimal PercepcionIIBB { get; set; }
        //public decimal ImporteNoGrabado { get; set; }
        //public bool Detallado { get; set; }
        //public string CondicionVenta { get; set; }


   
        //#endregion

        //// A protected constructor ensures that an object can't be created from outside  
        ////protected ComprobanteCartDto() { }

        //#region Reporting Methods

        //public decimal GetSubTotal()
        //{
        //    return Items.Sum(x => x.TotalSinIva);
        //}

        //public decimal GetIva()
        //{
        //    return Items.Where(x => x.Iva > 0).Sum(x => (x.TotalConIva - x.TotalSinIva));
        //}

        //public decimal GetTotal()
        //{
        //    return (GetIva() + GetSubTotal() + GetImporteNoGrabado() + GetPercepcionIVA() + GetPercepcionIIBB());
        //}
        //#endregion

        //#region PERCEPCIONES
        //public decimal GetPercepcionIVA()
        //{
        //    return ((PercepcionIVA > 0) ? ((GetSubTotal() * PercepcionIVA) / 100) : 0);
        //}
        //public decimal GetPercepcionIIBB()
        //{
        //    return ((PercepcionIIBB > 0) ? ((GetSubTotal() * PercepcionIIBB) / 100) : 0);
        //}
        //public decimal GetImporteNoGrabado()
        //{
        //    return ImporteNoGrabado;
        //}

        #endregion
    }

   
}

//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class CobranzasRetenciones
    {
        public int IDRetencion { get; set; }
        public int IDCobranza { get; set; }
        public string Tipo { get; set; }
        public decimal Importe { get; set; }
        public string NroReferencia { get; set; }
        public Nullable<int> IDJurisdiccion { get; set; }
        public Nullable<System.DateTime> FechaRetencion { get; set; }
    
        public virtual Cobranzas Cobranzas { get; set; }
        public virtual Provincias Provincias { get; set; }
    }
}

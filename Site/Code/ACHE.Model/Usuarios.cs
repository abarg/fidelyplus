//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Usuarios
    {
        public Usuarios()
        {
            this.Abonos = new HashSet<Abonos>();
            this.Alertas = new HashSet<Alertas>();
            this.AlertasGeneradas = new HashSet<AlertasGeneradas>();
            this.Asientos = new HashSet<Asientos>();
            this.AuthenticationToken = new HashSet<AuthenticationToken>();
            this.AvisosVencimiento = new HashSet<AvisosVencimiento>();
            this.Bancos = new HashSet<Bancos>();
            this.BancosPlanDeCuenta = new HashSet<BancosPlanDeCuenta>();
            this.Categorias = new HashSet<Categorias>();
            this.ChequeAccion = new HashSet<ChequeAccion>();
            this.Cheques = new HashSet<Cheques>();
            this.Cobranzas = new HashSet<Cobranzas>();
            this.Comprobantes = new HashSet<Comprobantes>();
            this.ComprobantesEnviados = new HashSet<ComprobantesEnviados>();
            this.ConceptosCaja = new HashSet<ConceptosCaja>();
            this.ConceptosTmp = new HashSet<ConceptosTmp>();
            this.Empleados = new HashSet<Empleados>();
            this.ListaPrecios = new HashSet<ListaPrecios>();
            this.ObservacionesUsuario = new HashSet<ObservacionesUsuario>();
            this.Personas = new HashSet<Personas>();
            this.PlanDeCuentas = new HashSet<PlanDeCuentas>();
            this.PlanesPagos = new HashSet<PlanesPagos>();
            this.Presupuestos = new HashSet<Presupuestos>();
            this.PuntosDeVenta = new HashSet<PuntosDeVenta>();
            this.TrackingHoras = new HashSet<TrackingHoras>();
            this.UsuariosAdicionales = new HashSet<UsuariosAdicionales>();
            this.UsuariosEmpresa = new HashSet<UsuariosEmpresa>();
            this.Caja = new HashSet<Caja>();
            this.MovimientoDeFondos = new HashSet<MovimientoDeFondos>();
            this.Compras = new HashSet<Compras>();
            this.LoginUsuarios = new HashSet<LoginUsuarios>();
            this.Addins_404_Remito = new HashSet<Addins_404_Remito>();
            this.Cajas = new HashSet<Cajas>();
            this.Integraciones = new HashSet<Integraciones>();
            this.GastosBancarios = new HashSet<GastosBancarios>();
            this.PreciosHistorial = new HashSet<PreciosHistorial>();
            this.UsuarioAddins = new HashSet<UsuarioAddins>();
            this.ConfiguracionPlanDeCuenta = new HashSet<ConfiguracionPlanDeCuenta>();
            this.UsuariosReferidos = new HashSet<UsuariosReferidos>();
            this.CajasPlanDeCuenta = new HashSet<CajasPlanDeCuenta>();
            this.MovimientosDeStock = new HashSet<MovimientosDeStock>();
            this.Inventarios = new HashSet<Inventarios>();
            this.OrdenesCompras = new HashSet<OrdenesCompras>();
            this.Conceptos = new HashSet<Conceptos>();
            this.Pagos = new HashSet<Pagos>();
            this.EnvioMails = new HashSet<EnvioMails>();
            this.Rubros = new HashSet<Rubros>();
            this.OrdenVenta = new HashSet<OrdenVenta>();
            this.StockConceptosReserva = new HashSet<StockConceptosReserva>();
            this.CondicionesVentas = new HashSet<CondicionesVentas>();
            this.ConfiguracionPublicacionIntegracion = new HashSet<ConfiguracionPublicacionIntegracion>();
            this.AsientosModelo = new HashSet<AsientosModelo>();
            this.Suscripciones = new HashSet<Suscripciones>();
            this.LogProcesosMasivos = new HashSet<LogProcesosMasivos>();
            this.BancosDetalle = new HashSet<BancosDetalle>();
            this.LogExternos = new HashSet<LogExternos>();
            this.PartnersPagos = new HashSet<PartnersPagos>();
        }
    
        public int IDUsuario { get; set; }
        public Nullable<int> IDEstudio { get; set; }
        public string RazonSocial { get; set; }
        public string Personeria { get; set; }
        public string Email { get; set; }
        public string Pwd { get; set; }
        public string CUIT { get; set; }
        public string IIBB { get; set; }
        public Nullable<System.DateTime> FechaInicioActividades { get; set; }
        public string CondicionIva { get; set; }
        public int IDProvincia { get; set; }
        public int IDCiudad { get; set; }
        public string Domicilio { get; set; }
        public string PisoDepto { get; set; }
        public string CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string EmailAlertas { get; set; }
        public string Logo { get; set; }
        public string Theme { get; set; }
        public int IDPlan { get; set; }
        public bool Activo { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public System.DateTime FechaUltLogin { get; set; }
        public string Contacto { get; set; }
        public bool TieneFacturaElectronica { get; set; }
        public string TemplateFc { get; set; }
        public string Pais { get; set; }
        public bool SetupRealizado { get; set; }
        public bool UsaProd { get; set; }
        public bool CorreoPortal { get; set; }
        public bool PortalClientes { get; set; }
        public Nullable<int> IDUsuarioPadre { get; set; }
        public string ApiKey { get; set; }
        public string CodigoPromo { get; set; }
        public Nullable<bool> EsAgentePercepcionIVA { get; set; }
        public Nullable<bool> EsAgentePercepcionIIBB { get; set; }
        public Nullable<bool> EsAgenteRetencion { get; set; }
        public string MercadoPagoClientID { get; set; }
        public string MercadoPagoClientSecret { get; set; }
        public System.DateTime FechaFinPlan { get; set; }
        public bool UsaFechaFinPlan { get; set; }
        public string Observaciones { get; set; }
        public int CantidadEmpresas { get; set; }
        public Nullable<bool> ExentoIIBB { get; set; }
        public bool UsaPrecioFinalConIVA { get; set; }
        public Nullable<System.DateTime> FechaBaja { get; set; }
        public string MotivoBaja { get; set; }
        public Nullable<System.DateTime> FechaCierreContable { get; set; }
        public bool EstaBloqueado { get; set; }
        public int CantIntentos { get; set; }
        public bool EnvioAutomaticoComprobante { get; set; }
        public bool EnvioAutomaticoRecibo { get; set; }
        public string IDJurisdiccion { get; set; }
        public bool EsContador { get; set; }
        public bool UsaPlanCorporativo { get; set; }
        public string ObservacionesFc { get; set; }
        public string ObservacionesRemito { get; set; }
        public string ObservacionesPresupuestos { get; set; }
        public Nullable<byte> CantidadDecimales { get; set; }
        public decimal PorcentajeDescuento { get; set; }
        public bool MostrarBonificacionFc { get; set; }
        public bool MostrarProductosEnRemito { get; set; }
        public string Actividad { get; set; }
        public Nullable<System.DateTime> FechaLimiteCarga { get; set; }
        public string EmailFrom { get; set; }
        public bool VenderSinStock { get; set; }
        public string EmailStock { get; set; }
        public string ObservacionesOrdenesPago { get; set; }
        public bool TieneDebitoAutomatico { get; set; }
    
        public virtual ICollection<Abonos> Abonos { get; set; }
        public virtual ICollection<Alertas> Alertas { get; set; }
        public virtual ICollection<AlertasGeneradas> AlertasGeneradas { get; set; }
        public virtual ICollection<Asientos> Asientos { get; set; }
        public virtual ICollection<AuthenticationToken> AuthenticationToken { get; set; }
        public virtual ICollection<AvisosVencimiento> AvisosVencimiento { get; set; }
        public virtual ICollection<Bancos> Bancos { get; set; }
        public virtual ICollection<BancosPlanDeCuenta> BancosPlanDeCuenta { get; set; }
        public virtual ICollection<Categorias> Categorias { get; set; }
        public virtual ICollection<ChequeAccion> ChequeAccion { get; set; }
        public virtual ICollection<Cheques> Cheques { get; set; }
        public virtual Ciudades Ciudades { get; set; }
        public virtual ICollection<Cobranzas> Cobranzas { get; set; }
        public virtual ICollection<Comprobantes> Comprobantes { get; set; }
        public virtual ICollection<ComprobantesEnviados> ComprobantesEnviados { get; set; }
        public virtual ICollection<ConceptosCaja> ConceptosCaja { get; set; }
        public virtual ICollection<ConceptosTmp> ConceptosTmp { get; set; }
        public virtual ICollection<Empleados> Empleados { get; set; }
        public virtual Estudios Estudios { get; set; }
        public virtual ICollection<ListaPrecios> ListaPrecios { get; set; }
        public virtual ICollection<ObservacionesUsuario> ObservacionesUsuario { get; set; }
        public virtual ICollection<Personas> Personas { get; set; }
        public virtual ICollection<PlanDeCuentas> PlanDeCuentas { get; set; }
        public virtual Planes Planes { get; set; }
        public virtual ICollection<PlanesPagos> PlanesPagos { get; set; }
        public virtual ICollection<Presupuestos> Presupuestos { get; set; }
        public virtual Provincias Provincias { get; set; }
        public virtual ICollection<PuntosDeVenta> PuntosDeVenta { get; set; }
        public virtual ICollection<TrackingHoras> TrackingHoras { get; set; }
        public virtual ICollection<UsuariosAdicionales> UsuariosAdicionales { get; set; }
        public virtual ICollection<UsuariosEmpresa> UsuariosEmpresa { get; set; }
        public virtual ICollection<Caja> Caja { get; set; }
        public virtual ICollection<MovimientoDeFondos> MovimientoDeFondos { get; set; }
        public virtual ICollection<Compras> Compras { get; set; }
        public virtual ICollection<LoginUsuarios> LoginUsuarios { get; set; }
        public virtual ICollection<Addins_404_Remito> Addins_404_Remito { get; set; }
        public virtual ICollection<Cajas> Cajas { get; set; }
        public virtual ICollection<Integraciones> Integraciones { get; set; }
        public virtual ICollection<GastosBancarios> GastosBancarios { get; set; }
        public virtual ICollection<PreciosHistorial> PreciosHistorial { get; set; }
        public virtual ICollection<UsuarioAddins> UsuarioAddins { get; set; }
        public virtual ICollection<ConfiguracionPlanDeCuenta> ConfiguracionPlanDeCuenta { get; set; }
        public virtual ICollection<UsuariosReferidos> UsuariosReferidos { get; set; }
        public virtual ICollection<CajasPlanDeCuenta> CajasPlanDeCuenta { get; set; }
        public virtual ICollection<MovimientosDeStock> MovimientosDeStock { get; set; }
        public virtual ICollection<Inventarios> Inventarios { get; set; }
        public virtual ICollection<OrdenesCompras> OrdenesCompras { get; set; }
        public virtual ICollection<Conceptos> Conceptos { get; set; }
        public virtual ICollection<Pagos> Pagos { get; set; }
        public virtual ICollection<EnvioMails> EnvioMails { get; set; }
        public virtual ICollection<Rubros> Rubros { get; set; }
        public virtual ICollection<OrdenVenta> OrdenVenta { get; set; }
        public virtual ICollection<StockConceptosReserva> StockConceptosReserva { get; set; }
        public virtual ICollection<CondicionesVentas> CondicionesVentas { get; set; }
        public virtual ICollection<ConfiguracionPublicacionIntegracion> ConfiguracionPublicacionIntegracion { get; set; }
        public virtual ICollection<AsientosModelo> AsientosModelo { get; set; }
        public virtual ICollection<Suscripciones> Suscripciones { get; set; }
        public virtual ICollection<LogProcesosMasivos> LogProcesosMasivos { get; set; }
        public virtual ICollection<BancosDetalle> BancosDetalle { get; set; }
        public virtual ICollection<LogExternos> LogExternos { get; set; }
        public virtual ICollection<PartnersPagos> PartnersPagos { get; set; }
    }
}

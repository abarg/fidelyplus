//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ACHE.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ComprobantesTributos
    {
        public int IDTributo { get; set; }
        public int IDComprobante { get; set; }
        public int Tipo { get; set; }
        public string Descripcion { get; set; }
        public decimal BaseImp { get; set; }
        public decimal Iva { get; set; }
        public decimal Importe { get; set; }
    
        public virtual Comprobantes Comprobantes { get; set; }
    }
}

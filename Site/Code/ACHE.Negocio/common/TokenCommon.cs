﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Negocio.Contabilidad;

namespace ACHE.Negocio.Common
{
    public class TokenCommon
    {
        public static int validarToken(string token)
        {
            int id = 0;

            using (var dbContext = new ACHEEntities())
            {
                var auth = dbContext.AuthenticationToken.Where(x => x.Token == token).FirstOrDefault();
                if (auth != null)
                {
                    var fecha = DateTime.Now;
                    if (auth.FechaExpiracion > fecha)
                        id = auth.IDUsuario;
                }
            }

            return id;
        }

        public static WebUser ObtenerWebUser(int idusuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                return ObtenerWebUser(dbContext, idusuario);
            }
        }
        public static WebUser ObtenerWebUser(int idusuario, int idusuarioAdicional)
        {
            using (var dbContext = new ACHEEntities())
            {
                return ObtenerWebUser(dbContext, idusuario, idusuarioAdicional);
            }
        }

        public static WebUser ObtenerWebUser(ACHEEntities dbContext, int idusuario, int idusuarioAdicional)
        {
            UsuariosView usu = null;
            usu = idusuarioAdicional>0 ? dbContext.UsuariosView.FirstOrDefault(x => x.IDUsuario == idusuario  && x.IDUsuarioAdicional==idusuarioAdicional) : dbContext.UsuariosView.FirstOrDefault(x => x.IDUsuario == idusuario);
            if(usu==null && idusuarioAdicional>0)
                usu = dbContext.UsuariosView.FirstOrDefault(x => x.IDUsuario == idusuario);
            if (usu == null)///PARA CUANDO FUE CAMBIO DE EMPRESA
                throw new CustomException("Por favor, revisele la configuracion del usuario. No se encontró la cuenta correcta");

            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, usu.IDUsuario, usu.CondicionIva);

            bool tieneMultiempresa = (usu.IDUsuarioAdicional != 0 && usu.IDUsuarioPadre == null) ? dbContext.UsuariosEmpresa.Any(x => x.IDUsuarioAdicional == usu.IDUsuarioAdicional) : true;

            return new WebUser(usu.IDUsuario, idusuarioAdicional, usu.Tipo, usu.RazonSocial, usu.CUIT, usu.CondicionIva,
                               usu.Email, usu.EmailFrom, "", usu.Domicilio + " " + usu.PisoDepto, usu.Pais, usu.IDProvincia,
                               usu.IDCiudad, usu.Telefono, usu.TieneFacturaElectronica, usu.IIBB, usu.FechaInicioActividades,
                               usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupRealizado, tieneMultiempresa, !usu.UsaProd,
                               4, usu.EmailAlertas, usu.Provincia, usu.Ciudad, usu.EsAgentePercepcionIVA, usu.EsAgentePercepcionIIBB,
                               usu.EsAgenteRetencion, true/*planVigente*/, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA,
                               usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
                               usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, "", usu.MostrarBonificacionFc, usu.PorcentajeDescuento);

        }

        public static WebUser ObtenerWebUser(ACHEEntities dbContext, int idusuario)
        {
            var usu = dbContext.UsuariosView.Where(x => x.IDUsuario == idusuario).FirstOrDefault();

            if (usu == null)
                throw new CustomException("Por favor, revisele la configuracion del usuario. No se encontró la cuenta correcta");

            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, usu.IDUsuario, usu.CondicionIva);

            bool tieneMultiempresa = (usu.IDUsuarioAdicional != 0 && usu.IDUsuarioPadre == null) ? dbContext.UsuariosEmpresa.Any(x => x.IDUsuarioAdicional == usu.IDUsuarioAdicional) : true;

            return new WebUser(usu.IDUsuario, usu.IDUsuarioAdicional, usu.Tipo, usu.RazonSocial, usu.CUIT, usu.CondicionIva,
                               usu.Email, usu.EmailFrom, "", usu.Domicilio + " " + usu.PisoDepto, usu.Pais, usu.IDProvincia,
                               usu.IDCiudad, usu.Telefono, usu.TieneFacturaElectronica, usu.IIBB, usu.FechaInicioActividades,
                               usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupRealizado, tieneMultiempresa, !usu.UsaProd,
                               4, usu.EmailAlertas, usu.Provincia, usu.Ciudad, usu.EsAgentePercepcionIVA, usu.EsAgentePercepcionIIBB,
                               usu.EsAgenteRetencion, true/*planVigente*/, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA,
                               usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
                               usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, "", usu.MostrarBonificacionFc, usu.PorcentajeDescuento);

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Facturacion;
using ACHE.Model.Negocio;
using System.Configuration;
using System.Web;
using System.IO;
using System.Data;

namespace ACHE.Negocio.Common {
    public static class PersonasCommon {
        public static Personas GuardarDesdeIDUsuario(ACHEEntities dbContext, int idUsuario, int idCliente, string tipo) {
            try {
                Personas persona = new Personas();
                var usuContabilium = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                var Usuario = dbContext.Usuarios.Where(x => x.IDUsuario == idCliente).FirstOrDefault();
                var cliente = dbContext.Personas.Where(x => x.NroDocumento == Usuario.CUIT && x.IDUsuario == usuContabilium.IDUsuario).FirstOrDefault();

                if (cliente != null)
                    persona = cliente;
                else if (usuContabilium != null && Usuario != null) {
                    var entity = new Personas();
                    entity.IDUsuario = usuContabilium.IDUsuario;
                    entity.FechaAlta = DateTime.Now.Date;

                    entity.Tipo = tipo;
                    entity.RazonSocial = Usuario.RazonSocial != null ? Usuario.RazonSocial.ToUpper() : "";
                    entity.NombreFantansia = Usuario.RazonSocial != null ? Usuario.RazonSocial.ToUpper() : "";
                    entity.CondicionIva = Usuario.CondicionIva;
                    entity.TipoDocumento = "CUIT";
                    entity.NroDocumento = Usuario.CUIT;
                    entity.Personeria = Usuario.Personeria;
                    entity.Email = Usuario.Email != null ? Usuario.Email.ToLower() : "";
                    //entity.EmailsEnvioFc = usuContabilium.EmailAlertas != null ? usuContabilium.EmailAlertas.ToLower():"";
                    entity.Telefono = Usuario.Telefono;
                    entity.Observaciones = Usuario.Observaciones;
                    entity.EmailsEnvioFc = Usuario.Email != null ? Usuario.Email.ToLower() : "";
                    entity.TipoComprobanteDefecto = "";
                    entity.AlicuotaIvaDefecto = "";
                    entity.Codigo = "";
                    //Domicilio
                    entity.IDPais = 10;//ARG
                    entity.IDProvincia = Usuario.IDProvincia;
                    entity.IDCiudad = Usuario.IDCiudad;
                    entity.Domicilio = Usuario.Domicilio;
                    entity.PisoDepto = Usuario.PisoDepto;
                    entity.CodigoPostal = Usuario.CodigoPostal;
                    entity.SaldoInicial = 0;
                    //lista Precio
                    entity.IDListaPrecio = null;

                    dbContext.Personas.Add(entity);
                    dbContext.SaveChanges();
                    persona = entity;
                }
                return persona;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static Personas ObtenerPersonaPorNroDoc(string nroDoc, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    return dbContext.Personas.Include("Provincias").Include("Ciudades")
                        .Where(x => x.IDUsuario == idUsuario && x.NroDocumento == nroDoc).FirstOrDefault();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static Personas ObtenerPersonaPorTipoYNroDoc(string tipo, string nroDoc, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    return dbContext.Personas.Include("Paises").Include("Provincias").Include("Ciudades")
                        .Where(x => x.IDUsuario == idUsuario && x.NroDocumento == nroDoc && x.TipoDocumento == tipo).FirstOrDefault();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static Personas ObtenerPersonaPorID(int id, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    return dbContext.Personas.Include("Paises").Include("Provincias").Include("Ciudades")
                        .Where(x => x.IDUsuario == idUsuario && x.IDPersona == id).FirstOrDefault();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static PersonasViewModel ObtenerDatos(int id, int idUsuario) {

            using (var dbContext = new ACHEEntities()) {
                Personas entity = dbContext.Personas.Where(x => x.IDPersona == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null) {
                    PersonasViewModel result = new PersonasViewModel();
                    result.ID = id;
                    result.RazonSocial = entity.RazonSocial.ToUpper();
                    //result.Email = entity.Email.ToLower();
                    if (!string.IsNullOrEmpty(entity.EmailsEnvioFc) || !string.IsNullOrEmpty(entity.Email))
                        result.Email = !string.IsNullOrEmpty(entity.EmailsEnvioFc) ? entity.EmailsEnvioFc.ToLower() : entity.Email.ToLower(); //personas.Email.ToLower();
                    result.CondicionIva = entity.CondicionIva;
                    result.Domicilio = entity.Domicilio.ToUpper() + " " + entity.PisoDepto;
                    result.Ciudad = (entity.Ciudades == null) ? "" : entity.Ciudades.Nombre.ToUpper();
                    result.Provincia = (entity.Provincias == null) ? "" : entity.Provincias.Nombre;
                    result.TipoDoc = entity.TipoDocumento;
                    result.NroDoc = entity.NroDocumento;

                    return result;
                }
                else
                    throw new Exception("Error al obtener los datos");
            }

        }

        public static List<Combo2ViewModel> ObtenerComprobantes(int id, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.Comprobantes.Where(x => x.IDPersona == id && x.IDUsuario == idUsuario).OrderByDescending(x => x.FechaComprobante)
                    .Select(x => new Combo2ViewModel() {
                        ID = x.IDComprobante,
                        Nombre = x.Tipo + " " + x.Numero.ToString("#00000000").ToUpper()
                    }).ToList();
            }
        }

        public static ResultadosPersonasViewModel ObtenerPersonas(string condicion, string tipo, int page, int pageSize, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var results = dbContext.Personas.Where(x => x.IDUsuario == idUsuario && x.Tipo == tipo.ToUpper()).AsQueryable();
                if (!string.IsNullOrWhiteSpace(condicion)) {
                    condicion = condicion.Trim().ToLower();
                    results = results.Where(x => x.RazonSocial.ToLower().Contains(condicion) || x.NombreFantansia.ToLower().Contains(condicion) || x.NroDocumento.Contains(condicion) || x.Codigo.ToLower().Contains(condicion));
                }

                var list = results.Select(x => new PersonasViewModel() {
                    ID = x.IDPersona,
                    RazonSocial = (x.RazonSocial != null) ? x.RazonSocial.ToUpper() : "",
                    NombreFantasia = x.NombreFantansia.ToUpper(),
                    NroDoc = x.NroDocumento,
                    CondicionIva = x.CondicionIva,
                    Telefono = x.Telefono,
                    Email = x.Email == null ? x.Email.ToLower() : "",
                    TieneFoto = (x.Foto == null || x.Foto == "") ? "0" : "1",
                    Codigo = x.Codigo,
                    Provincia = (x.Provincias != null) ? x.Provincias.Nombre : "",
                    Pais = (x.Paises != null) ? x.Paises.Nombre : "",
                    Ciudad = (x.Ciudades != null) ? x.Ciudades.Nombre : "",
                    Domicilio = x.Domicilio,
                    Personeria = x.Personeria,
                    PisoDepto = x.PisoDepto,
                    CodigoPostal = x.CodigoPostal,
                    Observaciones = x.Observaciones,
                    IDCiudad = x.IDCiudad,
                    IDPais = x.IDPais,
                    IDProvincia = x.IDProvincia
                });

                page--;

                ResultadosPersonasViewModel resultado = new ResultadosPersonasViewModel();
                resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = list.Count();
                resultado.Items = list.OrderBy(x => x.RazonSocial).Skip(page * pageSize).Take(pageSize).ToList();

                return resultado;
            }
        }

        public static bool EliminarPersona(int id, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                if (dbContext.AbonosPersona.Any(x => x.IDPersona == id))
                    throw new CustomException("No se puede eliminar por tener Abonos asociados.");
                else if (dbContext.TrackingHoras.Any(x => x.IDPersona == id && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener Tracking de horas asociadas.");
                else if (dbContext.Presupuestos.Any(x => x.IDPersona == id && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener Presupuestos asociados.");
                else if (dbContext.Comprobantes.Any(x => x.IDPersona == id && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener Ventas asociadas.");
                else if (dbContext.Compras.Any(x => x.IDPersona == id && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener Compras asociados.");
                else if (dbContext.Pagos.Any(x => x.IDPersona == id))
                    throw new CustomException("No se puede eliminar por tener Pagos asociados.");
                else if (dbContext.Cobranzas.Any(x => x.IDPersona == id && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener Cobranzas asociadas.");
                else if (dbContext.OrdenVenta.Any(x => x.IDPersonaComprador == id && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener Ordenes de Venta asociadas.");
                else {
                    var entity = dbContext.Personas.Where(x => x.IDPersona == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null) {
                        dbContext.Database.ExecuteSqlCommand("update Conceptos set IDPersona=null where idpersona = " + id);

                        dbContext.Personas.Remove(entity);
                        dbContext.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
        }

        public static void EliminarFoto(int idPersona, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {

                var entity = dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null) {
                    string Serverpath = HttpContext.Current.Server.MapPath("~/files/explorer/" + idUsuario + "/Contactos/" + entity.Foto);

                    if (File.Exists(Serverpath)) {
                        File.Delete(Serverpath);

                        entity.Foto = "";
                        dbContext.SaveChanges();
                    }
                    else {
                        throw new Exception("El cheque no tiene una imagen guardada");
                    }
                }
            }
        }

        private static String convertirMayusculasYAcentos(string original) {
            original =
                original.ReplaceAll("á", "a")
                    .ReplaceAll("é", "e")
                    .ReplaceAll("í", "i")
                    .ReplaceAll("ó", "o")
                    .ReplaceAll("ú", "u");
            original = original.ReplaceAll("ü", "u");
            original = original.ReplaceAll("ñ", "n");
            original = original.ReplaceAll("\\.", "").Replace("-", " ");
            original = original.ToUpper();
            return original;
        }
        
        public static string ExportarPersonas(string condicion, string tipo, int idUsuario) {
            string fileName = "ClientesProv_" + idUsuario + "_";
            string path = "~/tmp/";
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var results = dbContext.Personas.Where(x => x.IDUsuario == idUsuario && x.Tipo == tipo.ToUpper()).AsQueryable();

                    if (condicion != string.Empty)
                        results = results.Where(x => x.RazonSocial.Contains(condicion) || x.NombreFantansia.Contains(condicion) || x.NroDocumento.Contains(condicion));


                    dt = results.OrderBy(x => x.RazonSocial).ToList().Select(x => new {
                        Personeria = x.Personeria == "F" ? "Fisica" : "Juridica",
                        RazonSocial = x.RazonSocial.ToUpper(),
                        NombreFantasia = x.NombreFantansia.ToUpper(),
                        Documento = x.NroDocumento,
                        CategoriaImpositiva = x.CondicionIva,
                        Telefono = x.Telefono ?? "",
                        Celular = x.Celular ?? "",
                        Email = x.Email ?? "",//.ToLower(),
                        Web = x.Web ?? "",
                        Observaciones = x.Observaciones ?? "",
                        Provincia = x.Provincias.Nombre,
                        Ciudad = x.Ciudades.Nombre,
                        Domicilio = x.Domicilio ?? "",
                        PisoDepto = x.PisoDepto ?? "",
                        CodigoPostal = x.CodigoPostal ?? "",
                        EmailsEnvioFc = x.EmailsEnvioFc ?? ""

                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        #region Crear datos para el cliente

        public static void CrearDatosParaElCliente(string idMP, int IdUsuario, ACHEEntities dbContext, PlanesPagos planesPagos, Comprobantes comprobante, string nroComprobante) {
            // ** CREACION DE PROVEEDORES FACTURA Y COBRANZA ** //
            var idContabilium = Convert.ToInt32(ConfigurationManager.AppSettings["Usu.idContabilium"]);
            var usuario = dbContext.Usuarios.Where(x => x.IDUsuario == IdUsuario).FirstOrDefault();

            // ** CREACION DE USUARIO ** //
            var proveedor = PersonasCommon.GuardarDesdeIDUsuario(dbContext, IdUsuario, idContabilium, "P");
            // ** CREACION DE COMPROBANTE DE COMPRA ** //
            var compra = CrearCompra(dbContext, comprobante, nroComprobante, planesPagos, proveedor, usuario);
            // ** CREACION DE PAGO ** //
            CrearPago(dbContext, compra, proveedor, idMP, planesPagos, usuario.IDUsuario);
        }

        private static Compras CrearCompra(ACHEEntities dbContext, Comprobantes comprobante, string nroComprobante, PlanesPagos planesPagos, Personas proveedor, Usuarios usuario) {
            var precioUnitario = Math.Round(comprobante.ImporteTotalBruto, 2);

            int id = 0;
            int idPersona = proveedor.IDPersona;
            string fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
            string fechaPrimerVencimiento = DateTime.Now.AddDays(15).Date.ToString("dd/MM/yyyy");
            string fechaSegundoVencimiento = DateTime.Now.AddDays(30).Date.ToString("dd/MM/yyyy");
            string nroFactura = nroComprobante;
            string iva = "0";
            string importe2 = "0";
            string importe5 = "0";
            string importe10 = "0";
            string importe21 = "0";
            string importe27 = "0";
            string noGrav = "0";
            string importeMon = "0";
            string impNacional = "0";
            string impMunicipal = "0";
            string impInterno = "0";

            string percepcionIva = "0";
            string otros = "0";
            string obs = "Compra generada automáticamente por Contabilium, " + "Plan: " + planesPagos.Planes.Nombre;
            string tipo = comprobante.Tipo;
            string idCategoria = "";
            string rubro = "Servicios";
            string exento = "0";
            string FechaEmision = DateTime.Now.Date.ToString("dd/MM/yyyy");
            int idUsuario = usuario.IDUsuario;
            int idPlanDeCuenta = 0;
            List<JurisdiccionesViewModel> Jurisdicciones = new List<JurisdiccionesViewModel>();
            switch (usuario.CondicionIva) {
                case "RI":
                    importe21 = precioUnitario.ToString();
                    iva = Math.Round(((precioUnitario * 21) / 100), 2).ToString();
                    break;
                default:
                    importeMon = (precioUnitario + ((precioUnitario * 21) / 100)).ToString();
                    break;
            }

            var compras = ComprasCommon.Guardar(id, idPersona, fecha, nroFactura, iva, importe2, importe5, importe10, importe21, importe27, noGrav, importeMon,
            impNacional, impMunicipal, impInterno, percepcionIva, otros, obs, tipo, idCategoria, rubro, exento, FechaEmision,
            idPlanDeCuenta, idUsuario, Jurisdicciones, fechaPrimerVencimiento, fechaSegundoVencimiento, false, new List<DetalleViewModel>(), id, 0, usuario.Email);

            return compras;
        }

        private static void CrearPago(ACHEEntities dbContext, Compras compra, Personas Proveedor, string idMp, PlanesPagos planesPagos, int idUsuario) {
            var usu = TokenCommon.ObtenerWebUser(dbContext, idUsuario);//TODO: Mejorar esto

            //Determinamos la cantidad de decimales que posee configurado el usuario
            //var cantidadDecimales = 2;// ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            PagosCartDto PagosCart = new PagosCartDto();
            PagosCart.IDPago = 0;
            PagosCart.IDPersona = Proveedor.IDPersona;
            PagosCart.Observaciones = "Pago generado automaticamente desde Contabilium";
            PagosCart.FechaPago = DateTime.Now.Date.ToString("dd/MM/yyyy");

            PagosCart.Items = new List<PagosDetalleViewModel>();
            PagosCart.FormasDePago = new List<PagosFormasDePagoViewModel>();
            PagosCart.Retenciones = new List<PagosRetencionesViewModel>();

            PagosCart.Items.Add(new PagosDetalleViewModel(2) {
                ID = 1,
                IDCompra = compra.IDCompra,
                Importe = Convert.ToDecimal(planesPagos.ImportePagado),
                nroFactura = compra.NroFactura
            });
            PagosCart.FormasDePago.Add(new PagosFormasDePagoViewModel() {
                ID = 1,
                Importe = Convert.ToDecimal(planesPagos.ImportePagado),
                NroReferencia = idMp,
                FormaDePago = planesPagos.FormaDePago
            });

            PagosCommon.Guardar(dbContext, PagosCart, usu, false);
        }

        public static Personas ActualizarPersonaPorCodigo(int idIntegracion, long idComprador, string nombre, string apellido, string email, string tel, string tipoDocumento, string nroDocumento, int idUsuario, string ciudadEnvioNombre, string provinciaEnvioId, string domicilio, string pisoDepto, string codigoPostal) {
            var comprador = idComprador.ToString();
            //Si no existe lo crea, si existe actualiza nro doc y direccion

            using (var dbContext = new ACHEEntities()) {
                string codigo = idComprador.ToString();

                var persona = dbContext.Personas.FirstOrDefault(p => p.IDUsuario == idUsuario
                    && ((nroDocumento != "" && p.NroDocumento == nroDocumento)
                    || (email != null && email != "" && p.Email.ToLower() == email.ToLower())
                    || (idComprador != null && idComprador > 0 && p.Codigo == codigo)));
                bool checkAfip = false;
                if (persona == null) {
                    var razonSocial = nombre + " " + apellido;
                    persona = dbContext.Personas.Where(p => p.IDUsuario == idUsuario
                        && p.RazonSocial.ToLower() == razonSocial.ToLower()).FirstOrDefault();//Valido por las dudas

                    if (persona == null) {

                        checkAfip = true;

                        persona = new Personas();
                        persona.NroDocumento = "";
                        //persona.Codigo = idComprador;
                        persona.IDUsuario = idUsuario;
                        persona.FechaAlta = DateTime.Now.Date;
                        persona.Tipo = "C";
                        persona.RazonSocial = razonSocial;
                        persona.NombreFantansia = "";
                        persona.CondicionIva = (tipoDocumento != "CUIT" ? "CF" : "RI");
                        persona.TipoDocumento = tipoDocumento;
                        persona.Personeria = "F";
                        persona.Email = email;
                        persona.EmailsEnvioFc = email;
                        persona.Telefono = tel;
                        persona.Observaciones = "";
                        //persona.EmailsEnvioFc = email;
                        persona.TipoComprobanteDefecto = "";
                        persona.AlicuotaIvaDefecto = "";
                        persona.Codigo = codigo;
                        persona.IDPais = 10;// dbContext.Paises.FirstOrDefault(p => p.Nombre == "Argentina").IDPais;
                        persona.Domicilio = domicilio;
                        persona.PisoDepto = pisoDepto;
                        persona.CodigoPostal = codigoPostal;
                        persona.SaldoInicial = 0;
                        persona.NroDocumento = "";
                        dbContext.Personas.Add(persona);
                    }
                }
                //if (checkAfip) {
                //    if (string.IsNullOrEmpty(persona.NroDocumento) && !string.IsNullOrEmpty(nroDocumento)) {//trata de obtener los datos de afio
                //        var datosAfip = PersonasCommon.ObtenerDatosAfip(nroDocumento);
                //        if (datosAfip != null) {
                //            persona.NroDocumento = datosAfip.NroDoc;
                //            persona.TipoDocumento = datosAfip.TipoDoc;
                //            persona.CondicionIva = datosAfip.CategoriaImpositiva;
                //        }
                //        else {
                //            persona.NroDocumento = nroDocumento;
                //        }
                //    }
                //}

                if (string.IsNullOrEmpty(persona.Email) && !string.IsNullOrEmpty(email))
                    persona.Email = email;

               
                if (persona.Domicilio == null)
                    persona.Domicilio = "";
                else if (persona.Domicilio != null && persona.Domicilio.Length > 100)
                    persona.Domicilio = persona.Domicilio.Substring(0, 100);

                if (persona.PisoDepto != null && persona.PisoDepto.Length > 10)
                    persona.PisoDepto = persona.PisoDepto.Substring(0, 10);


                dbContext.SaveChanges();
                return persona;
            }
        }

        public static int GuardarPersonas(ACHEEntities dbContext, int id, string razonSocial, string nombreFantasia, string condicionIva, string personeria, string tipoDoc, string nroDoc,
            string telefono, string email, string emailFc, string tipo,
            int idPais, int idProvincia, int idCiudad, string domicilio, string pisoDepto, string cp, string obs, int listaPrecio, string codigo, decimal saldoInicial, int idUsuario, string idUsuarioAdicional) {
            try {
                if (tipoDoc == "CUIT" && dbContext.Personas.Any(x => x.IDUsuario == idUsuario && x.NroDocumento == nroDoc && x.IDPersona != id && x.NroDocumento != ""))
                    throw new CustomException("El CUIT ingresado ya se encuentra registrado.");
                else if (tipoDoc != "CUIT" && nroDoc != "" && condicionIva == "CF" && dbContext.Personas.Any(x => x.IDUsuario == idUsuario && x.CondicionIva == "CF" && x.RazonSocial.ToLower() == razonSocial.ToLower() && x.NroDocumento == nroDoc && x.IDPersona != id))
                    throw new Exception("El cliente ya se encuentra registrado.");
                else if (!string.IsNullOrEmpty(codigo) && dbContext.Personas.Any(x => x.Codigo.ToUpper() == codigo.ToUpper() && x.Codigo != "" && x.IDPersona != id && x.IDUsuario == idUsuario && x.Tipo == tipo))
                    throw new CustomException("El Código ingresado ya se encuentra registrado.");
                else if (tipoDoc == "CUIT" && !nroDoc.IsValidCUIT())
                    throw new CustomException("El CUIT es inválido.");

                Personas entity;
                if (id > 0) {
                    entity = dbContext.Personas.Where(x => x.IDPersona == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity == null)
                        throw new CustomException("El ID informado es inexistente");
                }
                else {
                    entity = new Personas();
                    entity.FechaAlta = DateTime.Now;
                    entity.IDUsuario = idUsuario;
                }

                var usuarioAdicional = 0;
                if(idUsuarioAdicional!="")
                    usuarioAdicional = int.Parse(idUsuarioAdicional);
                if (usuarioAdicional > 0)
                    entity.IDUsuarioAdicional = usuarioAdicional;
                else
                    entity.IDUsuarioAdicional = null;

                entity.Tipo = tipo.ToUpper();
                entity.RazonSocial = razonSocial != null ? razonSocial.ToUpper() : "";
                entity.NombreFantansia = nombreFantasia != null ? nombreFantasia.ToUpper() : "";
                entity.CondicionIva = condicionIva;
                entity.TipoDocumento = tipoDoc;

                if (condicionIva == "RI" || condicionIva == "EX" || condicionIva == "MO")
                    entity.TipoDocumento = "CUIT";

                entity.NroDocumento = nroDoc;
                entity.Personeria = personeria != null ? personeria : "";
                entity.Email = email != null ? email.ToLower() : "";
                entity.Telefono = telefono;
                entity.Observaciones = obs;
                entity.EmailsEnvioFc = emailFc != null ? emailFc.ToLower() : "";
                entity.TipoComprobanteDefecto = "";
                entity.AlicuotaIvaDefecto = "";
                entity.Codigo = (codigo == "") ? null : codigo;
                //Domicilio
                entity.IDPais = (idPais == 0) ? 10 : idPais;
                entity.IDProvincia = (idProvincia == 0) ? 2 : idProvincia;

                if (idCiudad == 0) {
                    var ciudad = dbContext.Ciudades.Where(x => x.IDProvincia == entity.IDProvincia && x.Nombre.ToLower() == "sin identificar").First();
                    entity.IDCiudad = ciudad.IDCiudad;
                }
                else
                    entity.IDCiudad = idCiudad;

                entity.Domicilio = domicilio != null ? domicilio.ToUpper() : "";
                entity.PisoDepto = pisoDepto;
                entity.CodigoPostal = cp;
                entity.SaldoInicial = saldoInicial;
                //lista Precio
                if (listaPrecio > 0 && entity.Tipo == "C")
                    entity.IDListaPrecio = listaPrecio;
                else
                    entity.IDListaPrecio = null;

                if (id > 0) {
                    dbContext.SaveChanges();
                }
                else {
                    dbContext.Personas.Add(entity);
                    dbContext.SaveChanges();
                }

                return entity.IDPersona;

            }
            catch (CustomException ex) {
                throw new Exception(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static int GuardarPersonas(int id, string razonSocial, string nombreFantasia, string condicionIva, string personeria, string tipoDoc, string nroDoc,
            string telefono, string email, string emailFc, string tipo,
            int idPais, int idProvincia, int idCiudad, string domicilio, string pisoDepto, string cp, string obs, int listaPrecio, string codigo, decimal saldoInicial, int idUsuario, string idUsuarioAdicional) {
            try {
                using (var dbContext = new ACHEEntities()) {

                    return GuardarPersonas(dbContext, id, razonSocial, nombreFantasia, condicionIva, personeria, tipoDoc, nroDoc, telefono, email, emailFc, tipo, idPais, idProvincia, idCiudad, domicilio, pisoDepto, cp, obs, listaPrecio, codigo, saldoInicial, idUsuario, idUsuarioAdicional);
                }

            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}
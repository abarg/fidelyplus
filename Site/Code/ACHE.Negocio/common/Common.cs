﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using ACHE.FacturaElectronica;
using System.Configuration;
using System.Security.Cryptography;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Common;
using ACHE.Extensions;
using System.Net.Mail;
using System.Collections.Specialized;
using ACHE.FacturaElectronicaModelo;
using ACHE.Negocio.Helpers;
using ACHE.Negocio.Ventas;

/// <summary>
/// Descripción breve de Common
/// </summary>
/// 
public static class Common {

    public enum ComprobanteModo {
        Previsualizar = 1,
        Generar = 2,
        GenerarRemito = 3,
        GenerarPDF = 4
    }

    public static void CrearComprobante(WebUser usu, int id, int idPersona, string tipo, string modo, string fecha, string condicionVenta,
        int tipoConcepto, string fechaVencimiento, int idPuntoVenta, ref string nroComprobante, string obs, ComprobanteModo accion, ref string afipObs) {
        //Determinamos la cantidad de decimales configurada para el usuario
        //var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);
        var aux = DateTime.Parse(fecha);

        var pathPdf = "";
        //var pathCertificado = "";

        if (tipoConcepto == 0)//fix
            tipoConcepto = 3;

        if (accion == ComprobanteModo.Previsualizar) {
            pathPdf = HttpContext.Current.Server.MapPath("~/files/comprobantes/" + usu.IDUsuario + "_prev.pdf");
            if (System.IO.File.Exists(pathPdf))
                System.IO.File.Delete(pathPdf);
        }
        else {
            pathPdf = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/Comprobantes/" + aux.Year.ToString() + "/");

            if (!Directory.Exists(pathPdf))
                Directory.CreateDirectory(pathPdf);
        }

        Personas persona = null;
        PuntosDeVenta punto = null;
        FEFacturaElectronica fe = new FEFacturaElectronica();
        FEComprobante comprobante = new FEComprobante();
        int puntoVenta = 0;

        using (var dbContext = new ACHEEntities()) {
            //armo el nombre del pdf
            try {
                persona = dbContext.Personas.Include("Provincias").Include("Ciudades").Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (persona == null)
                    throw new Exception("El cliente/proveedor es inexistente");

                if (ComprobanteModo.Generar == accion || ComprobanteModo.GenerarPDF == accion)
                    pathPdf = pathPdf + persona.RazonSocial.RemoverCaracteresParaPDF() + "_";

                punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == idPuntoVenta && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (punto != null)
                    puntoVenta = punto.Punto;

                if (puntoVenta == 0)
                    throw new Exception("Punto de venta inválido");
            }
            catch (Exception ex) {
                throw new CustomException("Error al setear al obtener los datos principales en metodo CrearComprobante");
            }

            #region Datos del emisor

            try {

                comprobante.TipoComprobante = ComprobantesCommon.ObtenerTipoComprobante(tipo);

                //var feMode = ConfigurationManager.AppSettings["FE.Modo"];
                if (accion == ComprobanteModo.Previsualizar) {
                    if (usu.CUIT != "")
                        comprobante.Cuit = long.Parse(usu.CUIT.ReplaceAll("-", ""));
                    comprobante.PtoVta = puntoVenta;
                }
                else {
                    if (usu.ModoQA) {
                        comprobante.Cuit = long.Parse(ConfigurationManager.AppSettings["FE.QA.Cuit"]);
                        comprobante.PtoVta = int.Parse(ConfigurationManager.AppSettings["FE.QA.Punto"]);
                        //pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.CertificadoAFIP.QA"]);
                    }
                    else {
                        if (usu.CUIT != "")
                            comprobante.Cuit = long.Parse(usu.CUIT.ReplaceAll("-", ""));
                        comprobante.PtoVta = puntoVenta;
                        //pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.CertificadoAFIP.PROD"]);
                    }
                }

                comprobante.Concepto = (FEConcepto)tipoConcepto;
                comprobante.Fecha = DateTime.Parse(fecha);

            }
            catch (Exception ex) {
                throw new CustomException("Error al setear los datos del emisor en metodo CrearComprobante");
            }

            try {
                //si el tipo de factura es C solo se tiene que setear las fechas si el tipo de concepto es 2 o 3 (productos o productos y servicios)
                if (tipoConcepto != 1) {
                    comprobante.FchServDesde = DateTime.Parse(fecha);
                    comprobante.FchServHasta = DateTime.Parse(fechaVencimiento);
                    comprobante.FchVtoPago = DateTime.Parse(fechaVencimiento);
                }

                comprobante.CodigoMoneda = "PES";
                comprobante.CotizacionMoneda = 1;
                if (nroComprobante != string.Empty)
                    comprobante.NumeroComprobante = int.Parse(nroComprobante);
                comprobante.CondicionVenta = usu.CondicionIVA;

                if (string.IsNullOrEmpty(punto.Domicilio)) {
                    comprobante.Domicilio = usu.Domicilio;
                    comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;
                }
                else {// Si el punto de venta tiene un domicilio dif cargado, el mismo debe estar con dividido com |
                    var auxDom = punto.Domicilio.Split('|');
                    if (auxDom.Length == 3) {
                        comprobante.Domicilio = auxDom[0];
                        comprobante.CiudadProvincia = auxDom[1] + ", " + auxDom[2];
                    }
                    else {
                        comprobante.Domicilio = "";
                        comprobante.CiudadProvincia = "";
                    }
                }
                comprobante.RazonSocial = usu.RazonSocial;
                comprobante.Telefono = usu.Telefono;
                if (usu.ExentoIIBB != null)
                    comprobante.IIBB = ((bool)usu.ExentoIIBB) ? "Exento" : usu.IIBB;
                comprobante.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usu.CondicionIVA);
                comprobante.FechaInicioActividades = usu.FechaInicio.HasValue ? usu.FechaInicio.Value.ToString("dd/MM/yyyy") : "";

            }
            catch (Exception ex) {
                throw new CustomException("Error al setear los datos del comprobante en metodo CrearComprobante");
            }

            #endregion

            #region Datos del cliente

            try {
                if (persona.CondicionIva == "CF") {
                    comprobante.DocTipo = 99; // Consumidor final
                    if (persona.NroDocumento != string.Empty) {
                        comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                        comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
                    }
                }
                else {
                    comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                    comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
                }


                comprobante.CondicionVenta = condicionVenta;


                comprobante.ClienteNombre = persona.RazonSocial;
                comprobante.ClienteCondiionIva = UsuarioCommon.GetCondicionIvaDesc(persona.CondicionIva);
                comprobante.ClienteDomicilio = persona.Domicilio + " " + persona.PisoDepto;
                if (persona.IDPais == 10)
                    comprobante.ClienteLocalidad = persona.Ciudades.Nombre + ", " + persona.Provincias.Nombre;
                else
                    comprobante.ClienteLocalidad = persona.Paises.Nombre;
                comprobante.Observaciones = obs;
                comprobante.Original = (accion == ComprobanteModo.Previsualizar ? false : true);
                if (tipo == "PRE")
                    comprobante.Original = true;

            }
            catch (Exception ex) {
                throw new CustomException("Error al setear los datos del receptor en metodo CrearComprobante");
            }

            #endregion

            #region Seteo los datos de la factura

            if (accion == ComprobanteModo.GenerarPDF) {
                var c = dbContext.Comprobantes.Where(x => x.IDComprobante == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (c != null && !string.IsNullOrWhiteSpace(c.CAE)) {
                    comprobante.CAE = c.CAE;
                    comprobante.FechaVencCAE = Convert.ToDateTime(c.FechaCAE);
                }
            }
            comprobante.ImpTotConc = 0;
            //comprobante.ImpOpEx = 0;
            //comprobante.ImpTotConc = (double)ComprobanteCart.Retrieve().GetImporteNoGrabado();
            comprobante.DetalleIva = new List<FERegistroIVA>();
            comprobante.Tributos = new List<FERegistroTributo>();

            //List<ComprobantesDetalleViewModel> list = new List<ComprobantesDetalleViewModel>();
            //if (ComprobanteCart.Instance != null)
            var list = ComprobanteCart.Retrieve().Items.ToList();// .OrderBy(x => x.Concepto).ToList();

            foreach (var det in ComprobanteCart.Retrieve().Tributos.ToList()) {
                FERegistroTributo ct = new FERegistroTributo();
                ct.Descripcion = det.Descripcion;
                ct.Alicuota = double.Parse(det.Alicuota.ToString());
                ct.Importe = double.Parse(Math.Round(det.Importe, 2).ToString());
                ct.BaseImp = double.Parse(det.BaseImp.ToString());
                if (det.Tipo == 1)
                    ct.Tipo = FETipoTributo.ImpuestosNacionales;
                else if (det.Tipo == 2)
                    ct.Tipo = FETipoTributo.ImpuestosProvinciales;
                else if (det.Tipo == 3)
                    ct.Tipo = FETipoTributo.ImpuestosMunicipales;
                else if (det.Tipo == 4)
                    ct.Tipo = FETipoTributo.ImpuestosInternos;
                else if (det.Tipo == 99)
                    ct.Tipo = FETipoTributo.Otro;
                comprobante.Tributos.Add(ct);
            }

            //else
            //{
            //    ComprobanteCart.Instance = new ComprobanteCart();
            //    ComprobanteCart.Instance.Items = new List<ComprobantesDetalleViewModel>();
            //    foreach (var det in dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == id).ToList())
            //    {
            //        ComprobantesDetalleViewModel cd = new ComprobantesDetalleViewModel(cantidadDecimales);
            //        cd.Concepto = det.Concepto;
            //        cd.Iva = det.Iva;
            //        cd.PrecioUnitario = det.PrecioUnitario;
            //        cd.Bonificacion = det.Bonificacion;
            //        cd.Cantidad = det.Cantidad;
            //        cd.IDConcepto = det.IDConcepto;
            //        list.Add(cd);
            //        ComprobanteCart.Instance.Items.Add(cd);
            //    }
            //}

            //DESCUENTO
            /*list.Add(new ComprobantesDetalleViewModel()
            {
                Codigo = "1111",
                Cantidad = 1,
                Bonificacion = 0,
                Concepto = "Descuento global",
                PrecioUnitario = -121,
                Iva = 0
            });*/

            if (list.Any()) {
                try {
                    foreach (var detalle in list) {
                        if (usu.CondicionIVA == "RI") {
                            comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                //Precio = Math.Round(double.Parse(detalle.PrecioUnitarioSinIVA.ToString()), cantidadDecimales),
                                Precio = double.Parse(detalle.PrecioUnitarioSinIVA.ToString()),
                                //PrecioParaPDF = persona.CondicionIva == "RI" ? Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales) : Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales),
                                PrecioParaPDF = persona.CondicionIva == "RI" ? double.Parse(detalle.PrecioUnitario.ToString()) : double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()),
                                Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                Bonificacion = detalle.Bonificacion,
                                Iva = detalle.Iva
                            });

                            //totalizar importe e IVA
                            switch (detalle.Iva.ToString("#0.00")) {
                                case "21,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva21);
                                    break;
                                case "27,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva27);
                                    break;
                                case "10,50":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva10_5);
                                    break;
                                case "5,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva5);
                                    break;
                                case "2,50":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva2_5);
                                    break;
                                case "0,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva0);
                                    break;
                            }
                        }
                        else {
                            comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                //Precio = Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales),
                                //PrecioParaPDF = Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales),
                                Precio = double.Parse(detalle.PrecioUnitarioConIva.ToString()),
                                PrecioParaPDF = double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()),
                                Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                Bonificacion = detalle.Bonificacion,
                                Iva = detalle.Iva
                            });

                            if (comprobante.DetalleIva.Any(x => x.TipoIva == FETipoIva.Iva0)) {
                                comprobante.DetalleIva.Where(x => x.TipoIva == FETipoIva.Iva0).FirstOrDefault().BaseImp += double.Parse(detalle.TotalConIva.ToString());
                            }
                            else {
                                if (tipo != "FCC" && tipo != "NDC" && tipo != "NCC" && tipo != "RCC")
                                    comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = double.Parse(detalle.TotalConIva.ToString()), TipoIva = FETipoIva.Iva0 });
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    throw new CustomException("Error al setear los datos del iva en metodo CrearComprobante");
                }
            }

            try {
                //Remplazar el IDConcepto por codigo 
                foreach (var item in comprobante.ItemsDetalle) {
                    if (item.Codigo != "") {
                        int idConceto = Convert.ToInt32(item.Codigo);
                        item.Codigo = dbContext.Conceptos.Where(x => x.IDConcepto == idConceto).FirstOrDefault().Codigo.ToString();
                    }
                }
            }
            catch (Exception ex) {
                throw new CustomException("Error al setear los datos de los codigos en metodo CrearComprobante");
            }

            #endregion

            if (usu.CondicionIVA == "RI")
                comprobante.ImpTotConc = (double)ComprobanteCart.Retrieve().GetImporteNoGrabado();
            if (usu.CondicionIVA == "RI" && (bool)usu.AgentePercepcionIIBB) {
                if ((double)ComprobanteCart.Retrieve().GetPercepcionIIBB() > 0) {
                    comprobante.Tributos.Add(new FERegistroTributo() {
                        BaseImp = Math.Round(comprobante.ItemsDetalle.Sum(x => x.Total), 2),
                        Alicuota = (double)ComprobanteCart.Retrieve().PercepcionIIBB,
                        Importe = Math.Round((double)ComprobanteCart.Retrieve().GetPercepcionIIBB(), 2),
                        //Importe = (double)ComprobanteCart.Retrieve().GetPercepcionIIBB(),
                        Descripcion = "Percepción IIBB " + ComprobanteCart.Retrieve().JurisdiccionIIBB.Replace("Ciudad de Buenos Aires", "CABA"),
                        Tipo = FETipoTributo.ImpuestosNacionales
                    });
                }
            }

            if (usu.CondicionIVA == "RI" && (bool)usu.AgentePercepcionIVA) {
                if ((double)ComprobanteCart.Retrieve().GetPercepcionIVA() > 0) {
                    comprobante.Tributos.Add(new FERegistroTributo() {
                        BaseImp = Math.Round(comprobante.ItemsDetalle.Sum(x => x.Total), 2),
                        Alicuota = (double)ComprobanteCart.Retrieve().PercepcionIVA,
                        Importe = Math.Round((double)ComprobanteCart.Retrieve().GetPercepcionIVA(), 2),
                        //Importe = (double)ComprobanteCart.Retrieve().GetPercepcionIVA(),
                        Descripcion = "Percepción IVA",
                        Tipo = FETipoTributo.ImpuestosNacionales
                    });
                }
            }
            var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.Template"] + usu.TemplateFc + ".pdf");
            var imgLogo = "logo-fc-" + usu.TemplateFc + ".png";
            if (!string.IsNullOrEmpty(usu.Logo)) {
                if (File.Exists(HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
                    imgLogo = usu.Logo;
            }

            var pathLogo = HttpContext.Current.Server.MapPath("~/files/usuarios/" + imgLogo);
            var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usu.IDUsuario + ".png");

            if (accion == ComprobanteModo.Previsualizar)
                fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, true, pathMarcaAgua);
            else if (accion == ComprobanteModo.GenerarPDF) {
                var numero = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                pathPdf = pathPdf + tipo + "-" + numero + ".pdf";
                fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, true, pathMarcaAgua);
                afipObs = comprobante.Observaciones;
            }
            else//SI no se previsualiza se genera electronicamente el comprobante
            {
                Comprobantes cmp = dbContext.Comprobantes.Where(x => x.IDComprobante == id).FirstOrDefault();
                if (cmp != null) {
                    cmp.FechaProceso = DateTime.Now;
                    try {
                        //fe.GenerarComprobante(comprobante, ConfigurationManager.AppSettings["FE.PROD.wsaa"], pathCertificado, pathTemplateFc, pathLogo);
                        fe.GenerarComprobante(comprobante, pathTemplateFc, pathLogo, (usu.ModoQA ? "QA" : "PROD"), usu.MostrarBonificacionFc, pathMarcaAgua);
                        string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                        pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

                        cmp.FechaCAE = comprobante.FechaVencCAE;
                        cmp.CAE = comprobante.CAE;
                        cmp.Numero = comprobante.NumeroComprobante;
                        cmp.Error = null;
                        cmp.FechaError = null;

                        //nroComprobante = tipo + "-" + numeroComprobante;
                        nroComprobante = numeroComprobante;
                        dbContext.SaveChanges();

                        afipObs = comprobante.Observaciones;
                    }
                    catch (Exception ex) {
                        cmp.Error = ex.Message;
                        cmp.FechaError = DateTime.Now;
                        dbContext.SaveChanges();

                        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                        msg = msg + "- USUARIO: " + cmp.IDUsuario;
                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FELogError"]), msg, ex.ToString());

                        //if (ex.Message.Contains("###Error###"))
                        //    throw new CustomException("El servicio de la AFIP se encuentra inaccesible momentáneamente.");
                        ////else 
                        if (ex.Message.Contains("El CEE ya posee un TA valido para el acceso al WSN solicitado"))
                            throw new CustomException("El servicio de la AFIP se encuentra sobrecargado en este momento. Intente nuevamente en unos minutos.");
                        else
                            throw new CustomException(cmp.Error, ex.InnerException);
                    }
                    try {
                        fe.GrabarEnDisco(comprobante.ArchivoFactura, pathPdf, pathTemplateFc, pathLogo);
                    }
                    catch (Exception) {
                        throw new Exception("El comprobante fue emitido correctamente pero no pudo ser generado el archivo PDF. Contáctese con soporte para obtener más información.");
                    }
                }
            }
        }
    }

    public static void CrearFEDesdeWebApi(WebUser usu, int id, int idPersona, string tipo, string modo, string fecha, string condicionVenta,
        int tipoConcepto, string fechaVencimiento, int idPuntoVenta, ref string nroComprobante, string obs, ComprobanteModo accion, ref string afipObs, string basePathReplace, string basePath) {
        //Determinamos la cantidad de decimales configurada para el usuario
        var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

        var pathPdf = "";
        //var pathCertificado = "";

        if (tipoConcepto == 0)//fix
            tipoConcepto = 3;

        if (accion == ComprobanteModo.Previsualizar) {
            pathPdf = basePathReplace + "/files/comprobantes/" + usu.IDUsuario + "_prev.pdf";//.Replace(basePathReplace, basePath);
            if (System.IO.File.Exists(pathPdf))
                System.IO.File.Delete(pathPdf);
        }
        else
            pathPdf = basePathReplace + "/files/explorer/" + usu.IDUsuario + "/Comprobantes/" + DateTime.Now.Year.ToString() + "/";//.Replace(basePathReplace, basePath);


        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "PathPDF", pathPdf);
        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "PathPDF 2", pathPdf.Replace(basePathReplace, basePath));


        using (var dbContext = new ACHEEntities()) {
            Personas persona = dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            if (persona == null)
                throw new Exception("El cliente/proveedor es inexistente");

            if (ComprobanteModo.Generar == accion || ComprobanteModo.GenerarPDF == accion)
                pathPdf = pathPdf + persona.RazonSocial.RemoverCaracteresParaPDF() + "_";

            int puntoVenta = 0;
            PuntosDeVenta punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == idPuntoVenta && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            if (punto != null)
                puntoVenta = punto.Punto;

            if (puntoVenta == 0)
                throw new Exception("Punto de venta inválido");

            FEFacturaElectronica fe = new FEFacturaElectronica();
            FEComprobante comprobante = new FEComprobante();

            #region Datos del emisor


            comprobante.TipoComprobante = ComprobantesCommon.ObtenerTipoComprobante(tipo);

            //usu.ModoQA = true;
            if (usu.ModoQA) {
                comprobante.Cuit = long.Parse(ConfigurationManager.AppSettings["FE.QA.Cuit"]);
                comprobante.PtoVta = int.Parse(ConfigurationManager.AppSettings["FE.QA.Punto"]);
                //pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.CertificadoAFIP.QA"]);
            }
            else {
                comprobante.Cuit = long.Parse(usu.CUIT.ReplaceAll("-", ""));
                comprobante.PtoVta = puntoVenta;
                //pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.CertificadoAFIP.PROD"]);
            }

            comprobante.Concepto = (FEConcepto)tipoConcepto;
            comprobante.Fecha = DateTime.Parse(fecha);


            //si el tipo de factura es C solo se tiene que setear las fechas si el tipo de concepto es 2 o 3 (productos o productos y servicios)
            if (tipoConcepto != 1) {
                comprobante.FchServDesde = DateTime.Parse(fecha);
                comprobante.FchServHasta = DateTime.Parse(fechaVencimiento);
                comprobante.FchVtoPago = DateTime.Parse(fechaVencimiento);
            }

            comprobante.CodigoMoneda = "PES";
            comprobante.CotizacionMoneda = 1;
            if (nroComprobante != string.Empty)
                comprobante.NumeroComprobante = int.Parse(nroComprobante);

            comprobante.CondicionVenta = usu.CondicionIVA;
            comprobante.Domicilio = usu.Domicilio;
            comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;

            if (string.IsNullOrEmpty(punto.Domicilio)) {
                comprobante.Domicilio = usu.Domicilio;
                comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;
            }
            else {// Si el punto de venta tiene un domicilio dif cargado, el mismo debe estar con dividido com |
                var auxDom = punto.Domicilio.Split('|');
                if (auxDom.Length == 3) {
                    comprobante.Domicilio = auxDom[0];
                    comprobante.CiudadProvincia = auxDom[1] + ", " + auxDom[2];
                }
                else {
                    comprobante.Domicilio = "";
                    comprobante.CiudadProvincia = "";
                }
            }

            comprobante.RazonSocial = usu.RazonSocial;
            comprobante.Telefono = usu.Telefono;
            if (usu.ExentoIIBB != null)
                comprobante.IIBB = ((bool)usu.ExentoIIBB) ? "Exento" : usu.IIBB;
            comprobante.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usu.CondicionIVA);
            comprobante.FechaInicioActividades = usu.FechaInicio.HasValue ? usu.FechaInicio.Value.ToString("dd/MM/yyyy") : "";

            #endregion

            #region Datos del cliente

            if (persona.CondicionIva == "CF") {
                comprobante.DocTipo = 99; // Consumidor final
                if (persona.NroDocumento != string.Empty) {
                    comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                    comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
                }
            }
            else {
                comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
            }

            comprobante.CondicionVenta = condicionVenta;
            comprobante.ClienteNombre = persona.RazonSocial;
            comprobante.ClienteCondiionIva = UsuarioCommon.GetCondicionIvaDesc(persona.CondicionIva);
            comprobante.ClienteDomicilio = persona.Domicilio + " " + persona.PisoDepto;
            if (persona.IDPais == 10)
                comprobante.ClienteLocalidad = persona.Ciudades.Nombre + ", " + persona.Provincias.Nombre;
            else
                comprobante.ClienteLocalidad = persona.Paises.Nombre;
            comprobante.Observaciones = obs;
            comprobante.Original = (accion == ComprobanteModo.Previsualizar ? false : true);
            if (tipo == "PRE")
                comprobante.Original = true;

            #endregion

            #region Seteo los datos de la factura

            if (accion == ComprobanteModo.GenerarPDF) {
                var c = dbContext.Comprobantes.Where(x => x.IDComprobante == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (c != null && !string.IsNullOrWhiteSpace(c.CAE)) {
                    comprobante.CAE = c.CAE;
                    comprobante.FechaVencCAE = Convert.ToDateTime(c.FechaCAE);
                }
            }
            //comprobante.ImpTotConc = 0;
            //comprobante.ImpOpEx = 0;
            comprobante.DetalleIva = new List<FERegistroIVA>();
            comprobante.Tributos = new List<FERegistroTributo>();

            List<ComprobantesDetalleViewModel> list = new List<ComprobantesDetalleViewModel>();
            foreach (var det in dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == id).ToList()) {
                ComprobantesDetalleViewModel cd = new ComprobantesDetalleViewModel(cantidadDecimales);
                cd.Concepto = det.Concepto;
                cd.Iva = det.Iva;
                cd.PrecioUnitario = det.PrecioUnitario;
                cd.Bonificacion = det.Bonificacion;
                cd.Cantidad = det.Cantidad;
                cd.IDConcepto = det.IDConcepto;
                list.Add(cd);
            }
            //List<ComprobantesTributosViewModel> tributos = new List<ComprobantesTributosViewModel>();
            foreach (var det in dbContext.ComprobantesTributos.Where(x => x.IDComprobante == id).ToList()) {
                FERegistroTributo ct = new FERegistroTributo();
                ct.Descripcion = det.Descripcion;
                ct.Alicuota = double.Parse(det.Iva.ToString());
                ct.Importe = double.Parse(Math.Round(det.Importe, 2).ToString());
                ct.BaseImp = double.Parse(det.BaseImp.ToString());
                if (det.Tipo == 1)
                    ct.Tipo = FETipoTributo.ImpuestosNacionales;
                else if (det.Tipo == 2)
                    ct.Tipo = FETipoTributo.ImpuestosProvinciales;
                else if (det.Tipo == 3)
                    ct.Tipo = FETipoTributo.ImpuestosMunicipales;
                else if (det.Tipo == 4)
                    ct.Tipo = FETipoTributo.ImpuestosInternos;
                else if (det.Tipo == 99)
                    ct.Tipo = FETipoTributo.Otro;
                comprobante.Tributos.Add(ct);
            }

            //DESCUENTO
            /*list.Add(new ComprobantesDetalleViewModel()
            {
                Codigo = "1111",
                Cantidad = 1,
                Bonificacion = 0,
                Concepto = "Descuento global",
                PrecioUnitario = -121,
                Iva = 0
            });*/

            if (list.Any()) {
                foreach (var detalle in list) {
                    if (usu.CondicionIVA == "RI")// && persona.CondicionIva == "RI")
                    {
                        comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                            Cantidad = detalle.Cantidad,
                            Descripcion = detalle.Concepto,
                            //Precio = Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales), 
                            Precio = Math.Round(double.Parse(detalle.PrecioUnitarioSinIVA.ToString()), cantidadDecimales),
                            PrecioParaPDF = persona.CondicionIva == "RI" ? Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales) : Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales),
                            Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                            Bonificacion = detalle.Bonificacion,
                            Iva = detalle.Iva
                        });
                        //BasicLog.AppendToFile(ConfigurationManager.AppSettings["ApiError"], "iva", detalle.Iva.ToString("#0.00"));
                        //totalizar importe e IVA
                        switch (detalle.Iva.ToString("#0.00")) {
                            case "21,00":
                            case "21.00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva21);
                                break;
                            case "27,00":
                            case "27.00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva27);
                                break;
                            case "10,50":
                            case "10.50":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva10_5);
                                break;
                            case "5,00":
                            case "5.00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva5);
                                break;
                            case "2,50":
                            case "2.50":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva2_5);
                                break;
                            case "0,00":
                            case "0.00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva0);
                                break;
                        }
                    }
                    else {
                        comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                            Cantidad = detalle.Cantidad,
                            Descripcion = detalle.Concepto,
                            Precio = Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales),
                            PrecioParaPDF = Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales),
                            Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                            Bonificacion = detalle.Bonificacion,
                            Iva = detalle.Iva
                        });

                        if (comprobante.DetalleIva.Any(x => x.TipoIva == FETipoIva.Iva0)) {
                            comprobante.DetalleIva.Where(x => x.TipoIva == FETipoIva.Iva0).FirstOrDefault().BaseImp += double.Parse(detalle.TotalConIva.ToString());
                        }
                        else {
                            if (tipo != "FCC" && tipo != "NDC" && tipo != "NCC" && tipo != "RCC")
                                comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = double.Parse(detalle.TotalConIva.ToString()), TipoIva = FETipoIva.Iva0 });
                        }
                    }
                }
            }

            //Remplazar el IDConcepto por codigo 
            foreach (var item in comprobante.ItemsDetalle) {
                if (item.Codigo != "") {
                    int idConceto = Convert.ToInt32(item.Codigo);
                    item.Codigo = dbContext.Conceptos.Where(x => x.IDConcepto == idConceto).FirstOrDefault().Codigo.ToString();
                }
            }

            #endregion

            comprobante.ImpTotConc = 0;// (double)ComprobanteCart.Retrieve().GetImporteNoGrabado(); //se saca para webapi por ahora
            //BasicLog.AppendToFile(ConfigurationManager.AppSettings["ApiError"], "ImpNeto", comprobante.ImpNeto.ToString());
            if (usu.CondicionIVA == "RI")
                comprobante.ImpTotConc = comprobante.ItemsDetalle.Where(x => x.Iva == 0).Sum(x => x.Precio);


            //LH: esto no deberia estar comentado pero en rest no hay sesion
            if (usu.CondicionIVA == "RI" && (bool)usu.AgentePercepcionIIBB) {
                //if ((double)ComprobanteCart.Retrieve().GetPercepcionIIBB() > 0)//se saca para webapi por ahora
                //{
                //    comprobante.Tributos.Add(new FERegistroTributo()
                //    {
                //        BaseImp = comprobante.ItemsDetalle.Sum(x => x.Total),
                //        Alicuota = (double)ComprobanteCart.Retrieve().PercepcionIIBB,
                //        Importe = Math.Round((double)ComprobanteCart.Retrieve().GetPercepcionIIBB(), cantidadDecimales),
                //        Decripcion = "Percepción IIBB " + ComprobanteCart.Retrieve().JurisdiccionIIBB,
                //        Tipo = FETipoTributo.ImpuestosNacionales
                //    });
                //}
            }

            if (usu.CondicionIVA == "RI" && (bool)usu.AgentePercepcionIVA) {
                //if ((double)ComprobanteCart.Retrieve().GetPercepcionIVA() > 0)//se saca para webapi por ahora
                //{
                //    comprobante.Tributos.Add(new FERegistroTributo()
                //    {
                //        BaseImp = comprobante.ItemsDetalle.Sum(x => x.Total),
                //        Alicuota = (double)ComprobanteCart.Retrieve().PercepcionIVA,
                //        Importe = Math.Round((double)ComprobanteCart.Retrieve().GetPercepcionIVA(), cantidadDecimales),
                //        Decripcion = "Percepción IVA",
                //        Tipo = FETipoTributo.ImpuestosNacionales
                //    });
                //}
            }
            var pathTemplateFc = basePathReplace + "/fe/TemplateFactura_" + usu.TemplateFc + ".pdf";//.Replace(basePathReplace, basePath);
            var imgLogo = "logo-fc-" + usu.TemplateFc + ".png";
            if (!string.IsNullOrEmpty(usu.Logo)) {
                if (File.Exists(basePathReplace + "/files/usuarios/" + usu.Logo))//.Replace(basePathReplace, basePath)))
                    imgLogo = usu.Logo;
            }
            var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usu.IDUsuario + ".png");

            var pathLogo = basePathReplace + "/files/usuarios/" + imgLogo;//.Replace(basePathReplace, basePath);
            if (accion == ComprobanteModo.Previsualizar)
                fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, true, pathMarcaAgua);
            else if (accion == ComprobanteModo.GenerarPDF) {
                var numero = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                pathPdf = pathPdf + tipo + "-" + numero + ".pdf";
                fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, true, pathMarcaAgua);
                afipObs = comprobante.Observaciones;
            }
            else//SI no se previsualiza se genera electronicamente el comprobante
            {
                Comprobantes cmp = dbContext.Comprobantes.Where(x => x.IDComprobante == id).FirstOrDefault();
                if (cmp != null) {
                    cmp.FechaProceso = DateTime.Now;
                    try {
                        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "pathTemplateFc", pathTemplateFc);
                        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "pathPdf pre", pathPdf);
                        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "pathLogo", pathLogo);
                        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "usu.ModoQA", (usu.ModoQA ? "QA" : "PROD"));

                        //fe.GenerarComprobante(comprobante, ConfigurationManager.AppSettings["FE.PROD.wsaa"], pathCertificado, pathTemplateFc, pathLogo);
                        fe.GenerarComprobante(comprobante, pathTemplateFc, pathLogo, (usu.ModoQA ? "QA" : "PROD"), usu.MostrarBonificacionFc, pathMarcaAgua);

                        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "pathPdf post", pathPdf);

                        string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                        pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

                        cmp.FechaCAE = comprobante.FechaVencCAE;
                        cmp.CAE = comprobante.CAE;
                        cmp.Numero = comprobante.NumeroComprobante;
                        cmp.Error = null;
                        cmp.FechaError = null;

                        //nroComprobante = tipo + "-" + numeroComprobante;
                        nroComprobante = numeroComprobante;
                        dbContext.SaveChanges();



                        afipObs = comprobante.Observaciones;
                    }
                    catch (Exception ex) {
                        cmp.Error = ex.Message;
                        cmp.FechaError = DateTime.Now;
                        dbContext.SaveChanges();

                        /*var msg = "API REST" + (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                        msg = msg + "- USUARIO: " + cmp.IDUsuario;
                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), msg, ex.ToString());
                        */
                        if (ex.Message.Contains("###Error###"))
                            throw new CustomException("El servicio de la AFIP se encuentra inaccesible momentáneamente.");
                        else
                            throw new CustomException(cmp.Error, ex.InnerException);
                    }
                    try {

                        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "pathPdf", pathPdf);
                        //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApiError"]), "pathTemplateFc", pathTemplateFc);
                        fe.GrabarEnDisco(comprobante.ArchivoFactura, pathPdf, pathTemplateFc, pathLogo);
                    }
                    catch (Exception) {
                        //LH: Se saca esto para que por API informe OK el numero.
                        //throw new Exception("El comprobante fue emitido correctamente pero no pudo ser generado el archivo PDF en " + pathPdf + "-" + pathTemplateFc + "-" + pathLogo + ". Contáctese con soporte para obtener más información.");
                    }
                }
            }
        }
    }

    /// <summary>
    /// Se usa para remitos o presupuestos
    /// </summary>
    /// <param name="usu"></param>
    /// <param name="idPersona"></param>
    /// <param name="fecha"></param>
    /// <param name="idPunto"></param>
    /// <param name="numero"></param>
    /// <param name="condicionVenta"></param>
    /// <param name="obs"></param>
    /// <param name="tipoComprobante"></param>
    public static void GenerarRemito(WebUser usu, int idPersona, string fecha, int idPunto, string numero, string condicionVenta, string obs, DateTime? fechaValidez, FETipoComprobante tipoComprobante, bool mostrarProductos) {
        //Determinamos la cantidad de decimales configurada para el usuario
        var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

        using (var dbContext = new ACHEEntities()) {
            Personas persona = dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            FEFacturaElectronica fe = new FEFacturaElectronica();
            FEComprobante comprobante = new FEComprobante();
            string numeroComprobanteRemito = string.Empty;

            #region Datos del emisor

            PuntosDeVenta punto = null;
            int puntoVenta = 0;
            if (idPunto != 0) {
                punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == idPunto && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                puntoVenta = punto.Punto;
                comprobante.PtoVta = puntoVenta;
            }

            var pathRemito = string.Empty;
            var pathTemplateFc = string.Empty;
            string letra = "P";
            bool imprimirEncabezado = true;
            switch (tipoComprobante) {
                case FETipoComprobante.PRESUPUESTO:
                    comprobante.TipoComprobante = tipoComprobante;
                    pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.Template"] + usu.TemplateFc + ".pdf");
                    pathRemito = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/Presupuestos/");
                    break;
                case FETipoComprobante.REMITO:
                    comprobante.TipoComprobante = tipoComprobante;
                    letra = "R";
                    pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.TemplateRemito"]);
                    if (File.Exists(pathTemplateFc.Replace(".pdf", "_" + usu.IDUsuario + ".pdf"))) {
                        pathTemplateFc = pathTemplateFc.Replace(".pdf", "_" + usu.IDUsuario + ".pdf");
                        imprimirEncabezado = false;
                    }
                    pathRemito = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/Remitos/");
                    break;
                case FETipoComprobante.ORDEN_COMPRAS:
                    comprobante.TipoComprobante = tipoComprobante;
                    letra = "OC";
                    pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.TemplateOrden"]);
                    pathRemito = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/OrdenCompra/");
                    break;
                case FETipoComprobante.ORDEN_VENTA:
                    comprobante.TipoComprobante = tipoComprobante;
                    letra = "OV";
                    pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.TemplateOrden"]);
                    pathRemito = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/OrdenVenta/");
                    break;
            }

            if (usu.CUIT != "")
                comprobante.Cuit = long.Parse(usu.CUIT.ReplaceAll("-", ""));
            comprobante.Fecha = DateTime.Parse(fecha);

            comprobante.CodigoMoneda = "PES";
            comprobante.CotizacionMoneda = 1;
            if (numero != string.Empty)
                comprobante.NumeroComprobante = int.Parse(numero);

            comprobante.CondicionVenta = usu.CondicionIVA;
            if (punto == null || (punto != null && string.IsNullOrEmpty(punto.Domicilio))) {
                comprobante.Domicilio = usu.Domicilio;
                comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;
            }
            else {// Si el punto de venta tiene un domicilio dif cargado, el mismo debe estar con dividido com |
                var auxDom = punto.Domicilio.Split('|');
                if (auxDom.Length == 3) {
                    comprobante.Domicilio = auxDom[0];
                    comprobante.CiudadProvincia = auxDom[1] + ", " + auxDom[2];
                }
                else {
                    comprobante.Domicilio = "";
                    comprobante.CiudadProvincia = "";
                }
            }
            comprobante.RazonSocial = usu.RazonSocial;
            comprobante.Telefono = usu.Telefono;
            if (usu.ExentoIIBB != null)
                comprobante.IIBB = ((bool)usu.ExentoIIBB) ? "Exento" : usu.IIBB;
            comprobante.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usu.CondicionIVA);
            comprobante.FechaInicioActividades = usu.FechaInicio.HasValue ? usu.FechaInicio.Value.ToString("dd/MM/yyyy") : "";

            #endregion

            #region Datos del cliente

            /*Int32 nroDNI = 0;
            
            if (Int32.TryParse(persona.NroDocumento, out nroDNI))
                comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
            else
            {*/
            if (persona.CondicionIva == "CF") {
                comprobante.DocTipo = 99; // Consumidor final
                if (persona.NroDocumento != string.Empty) {
                    comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                    comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
                }
            }
            else {
                comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
            }
            //}


            //if (persona.CondicionIva == "CF")
            //    comprobante.DocTipo = 99; // Consumidor final
            //else
            //    comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
            //comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento);

            comprobante.CondicionVenta = condicionVenta;

            comprobante.FchVtoPago = fechaValidez;
            comprobante.ClienteNombre = persona.RazonSocial;
            comprobante.ClienteCondiionIva = UsuarioCommon.GetCondicionIvaDesc(persona.CondicionIva);
            comprobante.ClienteDomicilio = persona.Domicilio + " " + persona.PisoDepto;
            if (persona.IDPais == 10)
                comprobante.ClienteLocalidad = persona.Ciudades.Nombre + ", " + persona.Provincias.Nombre;
            else
                comprobante.ClienteLocalidad = persona.Paises.Nombre;
            comprobante.Observaciones = obs;
            comprobante.Original = true;
            #endregion

            #region Seteo los datos de la factura

            comprobante.DetalleIva = new List<FERegistroIVA>();
            comprobante.Tributos = new List<FERegistroTributo>();

            if (usu.CondicionIVA == "RI")
                comprobante.ImpTotConc = (double)ComprobanteCart.Retrieve().GetImporteNoGrabado();

            var list = ComprobanteCart.Retrieve().Items.ToList();

            if (list.Any()) {
                foreach (var detalle in list) {
                    if (comprobante.TipoComprobante == FETipoComprobante.ORDEN_COMPRAS || comprobante.TipoComprobante == FETipoComprobante.ORDEN_VENTA) {
                        comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                            Cantidad = detalle.Cantidad,
                            Descripcion = detalle.Concepto,
                            Precio = Math.Round(double.Parse((detalle.PrecioUnitarioConIva).ToString()), cantidadDecimales),
                            PrecioParaPDF = Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales),
                            //PrecioParaPDF = Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales),
                            Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                            Bonificacion = detalle.Bonificacion,
                            Iva = detalle.Iva
                        });

                        switch (detalle.Iva.ToString("#0.00")) {
                            case "21,00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva21);
                                break;
                            case "27,00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva27);
                                break;
                            case "10,50":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva10_5);
                                break;
                            case "5,00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva5);
                                break;
                            case "2,50":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva2_5);
                                break;
                            case "0,00":
                                ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva0);
                                break;
                        }
                    }
                    else {
                        if (detalle.TipoConcepto == "S" && !mostrarProductos)
                            continue;

                        if (usu.CondicionIVA == "RI") {
                            comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                //Precio = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales) : 0,
                                //PrecioParaPDF = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales) : 0,
                                Precio = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse((detalle.PrecioUnitarioSinIVA).ToString()), cantidadDecimales) : 0,
                                PrecioParaPDF = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? (persona.CondicionIva == "RI" ? double.Parse(detalle.PrecioUnitario.ToString()) : double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString())) : 0,

                                //PrecioParaPDF = Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales),
                                Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                Bonificacion = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? detalle.Bonificacion : 0,
                                Iva = detalle.Iva
                            });

                            //totalizar importe e IVA
                            switch (detalle.Iva.ToString("#0.00")) {
                                case "21,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva21);
                                    break;
                                case "27,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva27);
                                    break;
                                case "10,50":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva10_5);
                                    break;
                                case "5,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva5);
                                    break;
                                case "2,50":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva2_5);
                                    break;
                                case "0,00":
                                    ComprobantesCommon.AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva0);
                                    break;
                            }
                        }
                        else {
                            comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                //Precio = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales) : 0,
                                Precio = Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales),
                                //PrecioParaPDF = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales) : 0,
                                //PrecioParaPDF = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales) : 0,
                                PrecioParaPDF = Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales),
                                Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                //Bonificacion = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? detalle.Bonificacion : 0,
                                Bonificacion = detalle.Bonificacion,
                                Iva = detalle.Iva
                            });

                            if (comprobante.DetalleIva.Any(x => x.TipoIva == FETipoIva.Iva0)) {
                                comprobante.DetalleIva.Where(x => x.TipoIva == FETipoIva.Iva0).FirstOrDefault().BaseImp += double.Parse(detalle.TotalConIva.ToString());
                            }
                            else {
                                if (usu.CondicionIVA == "RI")
                                    comprobante.DetalleIva.Add(new FERegistroIVA() { BaseImp = double.Parse(detalle.TotalConIva.ToString()), TipoIva = FETipoIva.Iva0 });
                            }
                        }
                    }
                }
            }


            /*if (list.Any())
            {
                foreach (var detalle in list)
                {
                    comprobante.ItemsDetalle.Add(new FEItemDetalle()
                    {
                        Cantidad = detalle.Cantidad,
                        Descripcion = detalle.Concepto,
                        Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",

                        //Precio = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), 2) : 0,
             * Precio = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales) : 0,
                        Bonificacion = (tipoComprobante == FETipoComprobante.PRESUPUESTO) ? detalle.Bonificacion : 0,
                        Iva = detalle.Iva
                    });
                }
            }*/

            foreach (var item in comprobante.ItemsDetalle) {
                if (item.Codigo != "") {
                    int idConceto = Convert.ToInt32(item.Codigo);
                    item.Codigo = dbContext.Conceptos.Where(x => x.IDConcepto == idConceto).FirstOrDefault().Codigo.ToString();
                }
            }

            #endregion

            var imgLogo = "logo-fc-default.png";
            if (!string.IsNullOrEmpty(usu.Logo)) {
                if (File.Exists(HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
                    imgLogo = usu.Logo;
            }
            var pathLogo = HttpContext.Current.Server.MapPath("~/files/usuarios/" + imgLogo);


            if (puntoVenta > 0)
                numeroComprobanteRemito = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + letra + "-" + puntoVenta.ToString("#0000") + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
            else
                numeroComprobanteRemito = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + letra + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');

            pathRemito = pathRemito + numeroComprobanteRemito + ".pdf";
            var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usu.IDUsuario + ".png");

            fe.GrabarEnDisco(comprobante, pathRemito, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, imprimirEncabezado, pathMarcaAgua);
        }
    }

    /// <summary>
    /// Solo se usa para recibos de cobranza X
    /// </summary>
    /// <param name="usu"></param>
    /// <param name="id"></param>
    /// <param name="idPersona"></param>
    /// <param name="tipo"></param>
    /// <param name="modo"></param>
    /// <param name="fecha"></param>
    /// <param name="idPuntoVenta"></param>
    /// <param name="nroComprobante"></param>
    /// <param name="obs"></param>
    /// <param name="accion"></param>
    public static void CrearCobranza(WebUser usu, int id, int idPersona, string tipo, string fecha, ref string nroComprobante, string obs, ComprobanteModo accion) {
        //Determinamos la cantidad de decimales configurada para el usuario
        var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

        var pathPdf = "";

        if (accion == ComprobanteModo.Previsualizar) {
            pathPdf = HttpContext.Current.Server.MapPath("~/files/comprobantes/" + usu.IDUsuario + "_prev.pdf");
            if (System.IO.File.Exists(pathPdf))
                System.IO.File.Delete(pathPdf);
        }
        else
            pathPdf = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/comprobantes/" + DateTime.Now.Year.ToString() + "/");

        using (var dbContext = new ACHEEntities()) {
            Personas persona = dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            if (persona == null)
                throw new Exception("El cliente/proveedor es inexistente");

            if (ComprobanteModo.Generar == accion)
                pathPdf = pathPdf + persona.RazonSocial.RemoverCaracteresParaPDF() + "_";

            FEFacturaElectronica fe = new FEFacturaElectronica();
            FEComprobante comprobante = new FEComprobante();

            #region Datos del emisor
            comprobante.TipoComprobante = FETipoComprobante.SIN_DEFINIR_RECIBO;

            var feMode = ConfigurationManager.AppSettings["FE.Modo"];
            if (feMode == "QA") {
                comprobante.Cuit = 30714075159;//long.Parse(usu.CUIT);
                comprobante.PtoVta = 5;// puntoVenta;
            }
            else {
                if (usu.CUIT != "")
                    comprobante.Cuit = long.Parse(usu.CUIT.ReplaceAll("-", ""));
                comprobante.PtoVta = 1;//puntoVenta;
            }
            comprobante.Concepto = FEConcepto.ProductoYServicio;
            comprobante.Fecha = DateTime.Parse(fecha);

            comprobante.FchServDesde = null;
            comprobante.FchServHasta = null;
            comprobante.CodigoMoneda = "PES";
            comprobante.CotizacionMoneda = 1;
            if (nroComprobante != string.Empty && nroComprobante != "-")
                comprobante.NumeroComprobante = int.Parse(nroComprobante);

            comprobante.CondicionVenta = usu.CondicionIVA;
            comprobante.Domicilio = usu.Domicilio;
            comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;
            comprobante.RazonSocial = usu.RazonSocial;
            comprobante.Telefono = usu.Telefono;
            if (usu.ExentoIIBB != null)
                comprobante.IIBB = ((bool)usu.ExentoIIBB) ? "Exento" : usu.IIBB;
            comprobante.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usu.CondicionIVA);
            comprobante.FechaInicioActividades = usu.FechaInicio.HasValue ? usu.FechaInicio.Value.ToString("dd/MM/yyyy") : "";

            #endregion

            #region Datos del cliente

            if (persona.CondicionIva == "CF") {
                comprobante.DocTipo = 99; // Consumidor final
                if (persona.NroDocumento != string.Empty) {
                    comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                    comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
                }
            }
            else {
                comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
            }

            comprobante.CondicionVenta = "";
            comprobante.ClienteNombre = persona.RazonSocial;
            comprobante.ClienteCondiionIva = UsuarioCommon.GetCondicionIvaDesc(persona.CondicionIva);
            comprobante.ClienteDomicilio = persona.Domicilio + " " + persona.PisoDepto;
            if (persona.IDPais == 10)
                comprobante.ClienteLocalidad = persona.Ciudades.Nombre + ", " + persona.Provincias.Nombre;
            else
                comprobante.ClienteLocalidad = persona.Paises.Nombre;
            comprobante.Observaciones = obs;
            comprobante.Original = true;//accion == ComprobanteModo.Previsualizar ? false : 

            #endregion

            //Determinamos el formato decimal
            var decimalFormat = DecimalFormatter.GetDecimalStringFormat(usu.IDUsuario);

            #region Seteo los datos de la factura

            comprobante.ImpTotConc = 0;
            comprobante.ImpOpEx = 0;
            comprobante.DetalleIva = new List<FERegistroIVA>();
            comprobante.Tributos = new List<FERegistroTributo>();

            var list = CobranzaCart.Retrieve().Items.OrderBy(x => x.Comprobante).ToList();
            var retenciones = CobranzaCart.Retrieve().Retenciones.OrderBy(x => x.Tipo).ToList();
            var formasCobro = CobranzaCart.Retrieve().FormasDePago.OrderBy(x => x.ID).ToList();
            if (list.Any() || retenciones.Any()) {
                comprobante.ConceptoRecibo = "El recibo contempla los siguientes items: |";

                foreach (var detalle in list) {
                    comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                        Cantidad = 1,
                        Descripcion = detalle.Comprobante,
                        Precio = Math.Round(double.Parse(detalle.Importe.ToString()), cantidadDecimales),
                    });
                    comprobante.ConceptoRecibo += detalle.Comprobante + ": $" + detalle.Importe.ToString(decimalFormat);
                    comprobante.ConceptoRecibo += "|";
                }

                if (retenciones.Any())
                    comprobante.ConceptoRecibo += "|El recibo contempla las siguientes retenciones: ||";
                foreach (var detalle in retenciones) {
                    /*comprobante.ItemsDetalle.Add(new FEItemDetalle()
                    {
                        Cantidad = 1,
                        Descripcion = "Retención " + detalle.Tipo + " Nro. " + detalle.NroReferencia,
                        Precio = Math.Round(double.Parse(detalle.Importe.ToString()), cantidadDecimales),
                    });*/
                    comprobante.ConceptoRecibo += detalle.Tipo + " Nro. " + detalle.NroReferencia + ": $" + detalle.Importe.ToString(decimalFormat);
                    comprobante.ConceptoRecibo += "|";
                }
                if (formasCobro.Any())
                    comprobante.ConceptoRecibo += "|El recibo contempla las siguientes cobranzas: ||";
                foreach (var detalle in formasCobro) {
                    comprobante.ConceptoRecibo += detalle.FormaDePago.Replace("de Terceros", "").Replace(" Propio", "").Replace("banco Banco", "Banco");
                    if (detalle.IDCheque.HasValue) {
                        var cheques = dbContext.Cheques.Include("BancosBase").Where(x => x.IDCheque == detalle.IDCheque.Value).First();
                        comprobante.ConceptoRecibo += " del banco " + cheques.BancosBase.Nombre;
                        comprobante.ConceptoRecibo += " emitido el " + cheques.FechaEmision.ToString("dd/MM/yyyy");
                        if (cheques.FechaCobro.HasValue)
                            comprobante.ConceptoRecibo += " con vto " + cheques.FechaCobro.Value.ToString("dd/MM/yyyy");
                    }
                    else if (detalle.IDBanco.HasValue) {
                        var banco = dbContext.Bancos.Include("BancosBase").Where(x => x.IDBanco == detalle.IDBanco.Value).First();
                        comprobante.ConceptoRecibo += " a " + banco.BancosBase.Nombre;
                        if (banco.NroCuenta != string.Empty)
                            comprobante.ConceptoRecibo += " Cta " + banco.NroCuenta;
                    }


                    comprobante.ConceptoRecibo += " - Nro Ref. " + detalle.NroReferencia + ": $" + detalle.Importe.ToString(decimalFormat);
                    comprobante.ConceptoRecibo += "|";
                }
            }

            #endregion

            var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.TemplateRecibo"]);
            var imgLogo = "logo-fc-default.png";
            if (!string.IsNullOrEmpty(usu.Logo)) {
                if (File.Exists(HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
                    imgLogo = usu.Logo;
            }
            var pathLogo = HttpContext.Current.Server.MapPath("~/files/usuarios/" + imgLogo);
            var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usu.IDUsuario + ".png");

            string numeroComprobante = nroComprobante.ToString().PadLeft(8, '0');

            if (accion != ComprobanteModo.Previsualizar)
                pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

            fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, true, pathMarcaAgua);
            nroComprobante = tipo + "-" + numeroComprobante;
        }
    }

    public static bool AlertaPlan(ACHEEntities dbContext, int idUsuario, ref string nombre, ref int cant) {
        var plan = PermisosModulos.ObtenerPlanActual(dbContext, idUsuario);

        if (plan == null)
            return false;

        var planes = dbContext.PlanesPagos.Where(x => x.IDUsuario == plan.IDUsuario && x.FechaInicioPlan >= plan.FechaFinPlan).ToList();
        if (planes.Count() > 0)
            return false;


        var diferencia = (Convert.ToDateTime(plan.FechaFinPlan) - DateTime.Now.Date).Days;
        if (diferencia <= 5) {
            nombre = plan.Planes.Nombre;
            cant = diferencia;
            return true;
        }
        else
            return false;
    }

    /*
    public static void EnviarComprobantePorMail(string nombre, string para, string asunto, string mensaje, string file, string logo)
    {
        var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

        MailAddressCollection listTo = new MailAddressCollection();
        foreach (var mail in para.Split(','))
        {
            if (mail != string.Empty)
                listTo.Add(new MailAddress(mail));
        }

        ListDictionary replacements = new ListDictionary();
        replacements.Add("<NOTIFICACION>", mensaje);
        if (nombre.Trim() != string.Empty)
            replacements.Add("<USUARIO>", nombre);
        else
            replacements.Add("<USUARIO>", "usuario");
        if (logo != string.Empty)
            replacements.Add("<LOGOEMPRESA>", "/files/usuarios/" + usu.Logo);
        else
            replacements.Add("<LOGOEMPRESA>", "/images/logo-mail-gris.png");


        List<string> attachments = new List<string>();
        attachments.Add(HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/comprobantes/" + DateTime.Now.Year.ToString() + "/" + file));

        bool send = EmailHelper.SendMessage(EmailTemplate.EnvioComprobanteConFoto, replacements, listTo, usu.Email, usu.Email, asunto, attachments);
        if (!send)
            throw new Exception("El mensaje no pudo ser enviado. Por favor, intente nuevamente. En caso de continuar el error, escribenos a <a href='" + ConfigurationManager.AppSettings["Email.Ayuda"] + "'>" + ConfigurationManager.AppSettings["Email.Ayuda"] + "</a>");
    }*/

    public static object generarToken(ACHEEntities dbContext, int idPersona) {
        var entity = dbContext.AuthenticationTokenClientes.Where(x => x.IDPersona == idPersona && x.FechaExpiracion < DateTime.Now).FirstOrDefault();
        if (entity == null || entity.FechaExpiracion < DateTime.Now) {
            string token = Guid.NewGuid().ToString();
            entity = new AuthenticationTokenClientes();
            entity.IDPersona = idPersona;
            entity.Token = token;
            entity.FechaExpiracion = DateTime.Now.AddHours(1);

            dbContext.AuthenticationTokenClientes.Add(entity);
            dbContext.SaveChanges();
            return token;
        }
        return entity.Token;
    }


    public static void GenerarPago(WebUser usu, int id, int idPersona, string tipo, string fecha, ref string nroComprobante, string obs, ComprobanteModo accion) {
        var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);
        var pathPdf = "";

        if (accion == ComprobanteModo.Previsualizar) {
            pathPdf = HttpContext.Current.Server.MapPath("~/files/pagos/" + usu.IDUsuario + "_prev.pdf");
            if (System.IO.File.Exists(pathPdf))
                System.IO.File.Delete(pathPdf);
        }
        else
            pathPdf = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/pagos/" + DateTime.Now.Year.ToString() + "/");

        using (var dbContext = new ACHEEntities()) {
            Personas persona = dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            if (persona == null)
                throw new Exception("El cliente/proveedor es inexistente");

            if (ComprobanteModo.Generar == accion)
                pathPdf = pathPdf + persona.RazonSocial.RemoverCaracteresParaPDF() + "_";

            FEFacturaElectronica fe = new FEFacturaElectronica();
            FEComprobante comprobante = new FEComprobante();

            #region Datos del emisor
            comprobante.TipoComprobante = FETipoComprobante.PAGO;

            var feMode = ConfigurationManager.AppSettings["FE.Modo"];
            if (feMode == "QA") {
                comprobante.Cuit = 30714075159;//long.Parse(usu.CUIT);
                comprobante.PtoVta = 5;// puntoVenta;
            }
            else {
                if (usu.CUIT != "")
                    comprobante.Cuit = long.Parse(usu.CUIT.ReplaceAll("-", ""));
                comprobante.PtoVta = 1;//puntoVenta;
            }
            comprobante.Concepto = FEConcepto.ProductoYServicio;
            comprobante.Fecha = DateTime.Parse(fecha);

            comprobante.FchServDesde = null;
            comprobante.FchServHasta = null;
            comprobante.CodigoMoneda = "PES";
            comprobante.CotizacionMoneda = 1;
            if (nroComprobante != string.Empty && nroComprobante != "-")
                comprobante.NumeroComprobante = int.Parse(nroComprobante);

            comprobante.CondicionVenta = usu.CondicionIVA;
            comprobante.Domicilio = usu.Domicilio;
            comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;
            comprobante.RazonSocial = usu.RazonSocial;
            comprobante.Telefono = usu.Telefono;
            if (usu.ExentoIIBB != null)
                comprobante.IIBB = ((bool)usu.ExentoIIBB) ? "Exento" : usu.IIBB;
            comprobante.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usu.CondicionIVA);
            comprobante.FechaInicioActividades = usu.FechaInicio.HasValue ? usu.FechaInicio.Value.ToString("dd/MM/yyyy") : "";

            #endregion

            #region Datos del cliente

            if (persona.CondicionIva == "CF") {
                comprobante.DocTipo = 99; // Consumidor final
                if (persona.NroDocumento != string.Empty) {
                    comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                    comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
                }
            }
            else {
                comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;
                comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento.ToNumericOnly());
            }

            comprobante.CondicionVenta = "";
            comprobante.ClienteNombre = persona.RazonSocial;
            comprobante.ClienteCondiionIva = UsuarioCommon.GetCondicionIvaDesc(persona.CondicionIva);
            comprobante.ClienteDomicilio = persona.Domicilio + " " + persona.PisoDepto;
            if (persona.IDPais == 10)
                comprobante.ClienteLocalidad = persona.Ciudades.Nombre + ", " + persona.Provincias.Nombre;
            else
                comprobante.ClienteLocalidad = persona.Paises.Nombre;
            comprobante.Observaciones = obs;
            comprobante.Original = true;//accion == ComprobanteModo.Previsualizar ? false : 

            #endregion

            //Determinamos el formato decimal
            var decimalFormat = DecimalFormatter.GetDecimalStringFormat(usu.IDUsuario);

            #region Seteo los datos de la factura

            comprobante.ImpTotConc = 0;
            comprobante.ImpOpEx = 0;
            comprobante.DetalleIva = new List<FERegistroIVA>();
            comprobante.Tributos = new List<FERegistroTributo>();

            var list = PagosCart.Retrieve().Items.OrderBy(x => x.nroFactura).ToList();
            var formasPagos = PagosCart.Retrieve().FormasDePago.OrderBy(x => x.FormaDePago).ToList();
            var retenciones = PagosCart.Retrieve().Retenciones.OrderBy(x => x.Tipo).ToList();
            //var formasCobro = CobranzaCart.Retrieve().FormasDePago.OrderBy(x => x.ID).ToList();
            if (list.Any() || formasPagos.Any()) {
                comprobante.ConceptoRecibo = "El pago contempla los siguientes items: |";

                foreach (var detalle in list) {
                    int index = 1;
                    comprobante.ItemsDetalle.Add(new FEItemDetalle() {
                        Cantidad = 1,
                        Descripcion = detalle.nroFactura,
                        Precio = Math.Round(double.Parse(detalle.Importe.ToString()), cantidadDecimales),
                    });
                    comprobante.ConceptoRecibo += detalle.nroFactura + ": $" + detalle.Importe.ToString(decimalFormat);
                    comprobante.ConceptoRecibo += "|";
                }

                if (formasPagos.Any())
                    comprobante.ConceptoRecibo += "|El pago contempla las siguientes formas de pagos: ||";

                foreach (var detalle in formasPagos) {
                    comprobante.ConceptoRecibo += detalle.FormaDePago.Replace(" de Terceros", "").Replace(" Propio", "").Replace("banco Banco", "Banco");
                    if (detalle.IDCheque.HasValue) {
                        var cheques = dbContext.Cheques.Include("BancosBase").Where(x => x.IDCheque == detalle.IDCheque.Value).First();
                        comprobante.ConceptoRecibo += " del banco " + cheques.BancosBase.Nombre;
                        comprobante.ConceptoRecibo += " emitido el " + cheques.FechaEmision.ToString("dd/MM/yyyy");
                        if (cheques.FechaCobro.HasValue)
                            comprobante.ConceptoRecibo += " con vto " + cheques.FechaCobro.Value.ToString("dd/MM/yyyy");
                    }
                    else if (detalle.IDBanco.HasValue) {
                        var banco = dbContext.Bancos.Include("BancosBase").Where(x => x.IDBanco == detalle.IDBanco.Value).First();
                        comprobante.ConceptoRecibo += " del " + banco.BancosBase.Nombre;
                        if (banco.NroCuenta != string.Empty)
                            comprobante.ConceptoRecibo += " Cta " + banco.NroCuenta;
                    }


                    comprobante.ConceptoRecibo += " - Nro. " + detalle.NroReferencia + ": $" + detalle.Importe.ToString(decimalFormat);
                    comprobante.ConceptoRecibo += "|";
                }

                //foreach (var detalle in formasPagos)
                //{


                //    /*comprobante.ItemsDetalle.Add(new FEItemDetalle()
                //    {
                //        Cantidad = 1,
                //        Descripcion = "Retención " + detalle.Tipo + " Nro. " + detalle.NroReferencia,
                //        Precio = Math.Round(double.Parse(detalle.Importe.ToString()), cantidadDecimales),
                //    });*/
                //    comprobante.ConceptoRecibo += detalle.FormaDePago + " Nro. " + detalle.NroReferencia + ": $" + detalle.Importe.ToString(decimalFormat);
                //    comprobante.ConceptoRecibo += "|";
                //}
                if (retenciones.Any())
                    comprobante.ConceptoRecibo += "|El pago contempla las siguientes retenciones: ||";
                foreach (var detalle in retenciones) {
                    comprobante.ConceptoRecibo += detalle.Tipo + " Nro. " + detalle.NroReferencia + ": $" + detalle.Importe.ToString(decimalFormat);
                    comprobante.ConceptoRecibo += "|";
                }
            }

            #endregion

            var pathTemplateFc = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.TemplateRecibo"]);
            var imgLogo = "logo-fc-default.png";
            if (!string.IsNullOrEmpty(usu.Logo)) {
                if (File.Exists(HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
                    imgLogo = usu.Logo;
            }
            var pathLogo = HttpContext.Current.Server.MapPath("~/files/usuarios/" + imgLogo);

            string numeroComprobante = "0001-" + nroComprobante.ToString().PadLeft(8, '0');

            if (accion != ComprobanteModo.Previsualizar)
                pathPdf = pathPdf + numeroComprobante + ".pdf";
            var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usu.IDUsuario + ".png");

            fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, true, pathMarcaAgua);
            nroComprobante = numeroComprobante;
        }
    }


}

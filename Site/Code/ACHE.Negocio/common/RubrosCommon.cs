﻿using ACHE.Model;
using ACHE.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ACHE.Negocio.Common {
    public static class RubrosCommon {
        public static ResultadosRubrosViewModel ObtenerRubros(string nombre, int page, int pageSize, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var results = dbContext.Rubros.Where(x => x.IDUsuario == idUsuario).AsQueryable();
                if (!string.IsNullOrWhiteSpace(nombre)) {
                    nombre = nombre.Trim().ToLower();
                    results = results.Where(x => x.Nombre.ToLower().Contains(nombre));
                }

                var list = results.Select(x => new RubrosViewModel() {
                    ID = x.IDRubro,
                    IDRubroPadre = x.IDRubroPadre,
                    Nombre = x.Nombre,
                    RubroPadre = (x.IDRubroPadre.HasValue) ? x.Rubros2.Nombre : ""

                });

                page--;

                ResultadosRubrosViewModel resultado = new ResultadosRubrosViewModel();
                resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = list.Count();
                resultado.Items = list.OrderBy(x => x.Nombre).Skip(page * pageSize).Take(pageSize).ToList();

                return resultado;
            }
        }

        public static bool EliminarRubro(int id, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                if (dbContext.Rubros.Any(x => x.IDRubroPadre == id && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener subros asociados.");
                else if (dbContext.Conceptos.Any(x => (x.IDSubRubro == id || x.IDRubro == id) && x.IDUsuario == idUsuario))
                    throw new CustomException("No se puede eliminar por tener productos asociados.");
                else {
                    var entity = dbContext.Rubros.Where(x => x.IDRubro == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null) {
                        dbContext.Rubros.Remove(entity);
                        dbContext.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
        }

        public static int Guardar(int id, string nombre, string idRubroPadre, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var idPadre = 0;
                if (idRubroPadre != "") {
                    idPadre = int.Parse(idRubroPadre);
                    if (dbContext.Rubros.Any(x => x.IDRubroPadre == idPadre && (x.Nombre.ToLower() == nombre.ToLower()) && (x.IDUsuario == idUsuario) && (x.IDRubro != id)))
                        throw new Exception("Ya existe un rubro con nombre " + nombre);
                }
                else {
                    if (dbContext.Rubros.Any(x => (x.Nombre.ToLower() == nombre.ToLower()) && (x.IDUsuario == idUsuario) && (x.IDRubro != id)))
                        throw new Exception("Ya existe un rubro con nombre " + nombre);
                }

                Rubros entity;
                if (id > 0)
                    entity = dbContext.Rubros.Where(x => x.IDRubro == id && x.IDUsuario == idUsuario).FirstOrDefault();
                else
                    entity = new Rubros();

                entity.Nombre = nombre;
                entity.IDUsuario = idUsuario;
                if (idPadre > 0)
                    entity.IDRubroPadre = idPadre;
                else
                    entity.IDRubroPadre = null;

                if (id == 0)
                    dbContext.Rubros.Add(entity);

                dbContext.SaveChanges();
                return entity.IDRubro;
            }
        }

        public static string ExportarRubros(string nombre, WebUser usu) {
            try {
                string fileName = "Rubros" + usu.IDUsuario + "_";
                string path = "~/tmp/";
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var results = dbContext.Rubros.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                    if (nombre != string.Empty)
                        results = results.Where(x => x.Nombre.Contains(nombre));


                    dt = results.OrderBy(x => x.Nombre).ToList().Select(x => new {
                        Nombre = x.Nombre,
                        RubroPadre = (x.IDRubroPadre.HasValue) ? x.Rubros2.Nombre : ""


                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }

        }
    }
}

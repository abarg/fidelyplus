﻿using ACHE.Model;
using ACHE.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ACHE.Extensions;

namespace ACHE.Negocio.Common {
    public static class AlertasCommon {

        public static void OcultasAlertasGeneradas(int id, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var alerta = dbContext.AlertasGeneradas.Where(x => x.IDAlertasGeneradas == id && x.IDUsuario == idUsuario).FirstOrDefault();
                alerta.Visible = false;
                dbContext.SaveChanges();
            }
        }

        public static void GuardarAlertas(int id, string importe, string avisos, string condiciones, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var existe = dbContext.Alertas.Any(x => x.Condicion == condiciones && x.AvisoAlerta == avisos && x.IDUsuario == idUsuario && x.IDAlerta != id);
                if (existe)
                    throw new Exception("Ya existe una alerta con estas caracteristicas.");

                Alertas entity;
                if (id > 0) {
                    entity = dbContext.Alertas.Where(x => x.IDAlerta == id & x.IDUsuario == idUsuario).FirstOrDefault();
                }
                else {
                    entity = new Alertas();
                }

                entity.Importe = (importe != string.Empty) ? Convert.ToDecimal(importe.Replace(".", ",")) : 0;
                entity.AvisoAlerta = avisos;
                entity.Condicion = condiciones;
                entity.IDUsuario = idUsuario;


                if (id == 0)
                    dbContext.Alertas.Add(entity);

                dbContext.SaveChanges();
            }
        
        }

        public static AlertasViewModel ObtenerAlerta(int id){
            using (var dbContext = new ACHEEntities()) {
                AlertasViewModel AlertasViewModel = new AlertasViewModel();
                var entity = dbContext.Alertas.Where(x => x.IDAlerta == id).FirstOrDefault();
                if (entity != null) {
                    AlertasViewModel.ID = entity.IDAlerta;
                    AlertasViewModel.Condicion = entity.Condicion;
                    AlertasViewModel.AvisoAlerta = entity.AvisoAlerta;
                    AlertasViewModel.Importe = entity.Importe.ToString().Replace(",", ".");
                }
                return AlertasViewModel;
            }        
        
        }

        public static ResultadosAlertasViewModel ObtenerAlertas(string avisoAlertas, int page, int pageSize, int idUsuario) {

            using (var dbContext = new ACHEEntities()) {
                var results = dbContext.Alertas.Where(x => x.IDUsuario == idUsuario).AsQueryable();

                if (avisoAlertas != string.Empty)
                    results = results.Where(x => x.AvisoAlerta == avisoAlertas);


                page--;
                ResultadosAlertasViewModel resultado = new ResultadosAlertasViewModel();

                var list = results.OrderByDescending(x => x.IDAlerta).Skip(page * pageSize).Take(pageSize).ToList()
                 .Select(x => new AlertasViewModel() {
                     ID = x.IDAlerta,
                     AvisoAlerta = x.AvisoAlerta,
                     Condicion = x.Condicion,
                     Importe = x.Importe.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario))
                 });

                resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = list.Count();
                resultado.Items = list.ToList();
                return resultado;
            }
        }

        public static void EliminarAlerta(int id, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var entity = dbContext.Alertas.Where(x => x.IDAlerta == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null) {
                    dbContext.Alertas.Remove(entity);
                    dbContext.SaveChanges();
                }
            }
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Net.Mail;
using System.Web;
using ACHE.Model;
using ACHE.Model.Negocio;
using System.Linq;


namespace ACHE.Negocio.Common
{
    public static class EmailCommon
    {
        public static SmtpSettings getSmtpSettings(int idUsuario)
        {

            if (HttpContext.Current.Session == null)//Para API REST
                return new SmtpSettings { TieneAddinMails = false };

            if (HttpContext.Current.Session["ASPNETSmtpSettings"] == null)
            {
                HttpContext.Current.Session["ASPNETSmtpSettings"] = AddinsCommon.ObtenerSmtpSettings(idUsuario, ConfigurationManager.AppSettings["Addins.MailsCustomizable"]);
            }
            return (SmtpSettings)HttpContext.Current.Session["ASPNETSmtpSettings"];
        }


        public static bool SendMessage(int idUsuario, EmailTemplateApp template, ListDictionary replacements, string to, string subject, string emailFrom, string displayFromName)
        {
            SmtpSettings smtpSettings = getSmtpSettings(idUsuario);
            return SendMessage(idUsuario, template, replacements, to, subject, emailFrom, displayFromName, smtpSettings);
        }

        public static bool SendMessage(int idUsuario, EmailTemplateApp template, ListDictionary replacements, string to, string subject, string emailFrom, string displayFromName, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
                ? smtpSettingsConfig
                : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();
            return EmailHelperApp.SendMessage(realTemplate, replacements, to, subject,
                smtpSettings != null ? (smtpSettings.EmailFrom ?? emailFrom) : emailFrom, displayFromName, smtpSettings != null ? smtpSettings.Host : "",
                smtpSettings != null ? smtpSettings.Puerto : "", smtpSettings != null ? smtpSettings.Usuario : "",
                smtpSettings != null ? smtpSettings.Contrasenia : "", smtpSettings != null ? smtpSettings.EnableSsl : false);
        }

        public static bool SendMessage(int idUsuario, EmailTemplateApp template, ListDictionary replacements, string to, string cc, string replyTo, string subject, string displayFromName)
        {
            SmtpSettings smtpSettings = getSmtpSettings(idUsuario);
            return SendMessage(idUsuario, template, replacements, to, cc, replyTo, subject, displayFromName, smtpSettings);
        }

        public static bool SendMessage(int idUsuario, EmailTemplateApp template, ListDictionary replacements, string to, string cc, string replyTo, string subject, string displayFromName, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
                ? smtpSettingsConfig
                : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();
            return EmailHelperApp.SendMessage(realTemplate, replacements, to, cc, replyTo, subject, displayFromName, smtpSettings != null ? smtpSettings.Host : "", smtpSettings != null ? smtpSettings.Puerto : "", smtpSettings != null ? smtpSettings.Usuario : "", smtpSettings != null ? smtpSettings.Contrasenia : "");
        }

        public static bool SendMessage(string template, ListDictionary replacements, string to, string subject)
        {
            return EmailHelperApp.SendMessage(template, replacements, to, subject);
        }

        public static bool SendMessage(int idUsuario, string template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName, string smtphost = "", string smtpPuerto = "", string smtpUsuario = "", string smtpContrasenia = "")
        {
            SmtpSettings smtpSettings = getSmtpSettings(idUsuario);
            return SendMessage(idUsuario, template, replacements, to, from, cc, subject, attachments, fromDisplayName,
                smtphost, smtpPuerto, smtpUsuario, smtpContrasenia, smtpSettings);
        }
        
        public static bool SendMessage(int idUsuario, string template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName, string smtphost, string smtpPuerto, string smtpUsuario, string smtpContrasenia, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
               ? smtpSettingsConfig
               : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();
            return EmailHelper.SendMessage(realTemplate, replacements, to, smtpSettings != null ? (smtpSettings.EmailFrom ?? from) : from, cc, subject, attachments, fromDisplayName,
                smtpSettings != null ? (smtpSettings.Host ?? smtphost) : smtphost, smtpSettings != null ? (smtpSettings.Puerto ?? smtpPuerto) : smtpPuerto, smtpSettings != null ? (smtpSettings.Usuario ?? smtpUsuario) : smtpUsuario, smtpSettings != null ? (smtpSettings.Contrasenia ?? smtpContrasenia) : smtpContrasenia);
        }

        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName)
        {
            SmtpSettings smtpSettings = getSmtpSettings(idUsuario);
            return SendMessage(idUsuario, template, replacements,
                to, from, cc, subject, attachments,
                fromDisplayName, smtpSettings);
        }

        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements,
            MailAddressCollection to, string from, string cc, string subject, List<string> attachments,
            string fromDisplayName, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
               ? smtpSettingsConfig
               : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();

            return EmailHelper.SendMessage(realTemplate, replacements, to, smtpSettings != null ? (smtpSettings.EmailFrom ?? from) : from, cc, subject, attachments,
                fromDisplayName, smtpSettings != null ? smtpSettings.Host : "", smtpSettings != null ? smtpSettings.Puerto : "", smtpSettings != null ? smtpSettings.Usuario : "", smtpSettings != null ? smtpSettings.Contrasenia : "", smtpSettings != null ? smtpSettings.EnableSsl : false);
        }


        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements, string emailFrom, string to, string subject)
        {
            SmtpSettings smtpSettings = getSmtpSettings(idUsuario);
            return SendMessage(idUsuario, template, replacements,
                emailFrom, to, subject, smtpSettings);
        }
        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements, string emailFrom, string to, string subject, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
               ? smtpSettingsConfig
               : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();

            return EmailHelper.SendMessage(realTemplate, replacements, smtpSettings != null ? (smtpSettings.EmailFrom ?? emailFrom) : emailFrom, to, subject, smtpSettings != null ? smtpSettings.Host : "", smtpSettings != null ? smtpSettings.Puerto : "", smtpSettings != null ? smtpSettings.Usuario : "", smtpSettings != null ? smtpSettings.Contrasenia : "", smtpSettings != null ? smtpSettings.EnableSsl : false);
        }

        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements, string to, string subject)
        {
            SmtpSettings smtpSettings = getSmtpSettings(idUsuario);

            return SendMessage(idUsuario, template, replacements, to, subject, smtpSettings);
        }
        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements, string to, string subject, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
              ? smtpSettingsConfig
              : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();

            return EmailHelper.SendMessage(realTemplate, replacements, smtpSettings != null ? smtpSettings.EmailFrom : "", to, subject, smtpSettings != null ? smtpSettings.Host : "", smtpSettings != null ? smtpSettings.Puerto : "", smtpSettings != null ? smtpSettings.Usuario : "", smtpSettings != null ? smtpSettings.Contrasenia : "");
        }

        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements,
            MailAddressCollection to, string from, string subject, List<string> attachments, string fromDisplayName)
        {
            SmtpSettings smtpSettings = getSmtpSettings(idUsuario);
            return SendMessage(idUsuario, template, replacements,
                to, from, subject, attachments, fromDisplayName, smtpSettings);
        }

        public static bool SendMessage(int idUsuario, EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string from, string subject, List<string> attachments, string fromDisplayName, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
              ? smtpSettingsConfig
              : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();

            return EmailHelper.SendMessage(realTemplate, replacements, to, smtpSettings != null ? (smtpSettings.EmailFrom ?? from) : from, subject, attachments,
                fromDisplayName, smtpSettings != null ? smtpSettings.Host : "", smtpSettings != null ? smtpSettings.Puerto : "", smtpSettings != null ? smtpSettings.Usuario : "", smtpSettings != null ? smtpSettings.Contrasenia : "", smtpSettings != null ? smtpSettings.EnableSsl : false);
        }

        public static bool SendMessageBatch(int idUsuario, EmailTemplate template, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName, SmtpSettings smtpSettingsConfig)
        {
            var smtpSettings = smtpSettingsConfig != null && smtpSettingsConfig.TieneAddinMails
              ? smtpSettingsConfig
              : null;
            var realTemplate = smtpSettings != null ? (smtpSettings.UserTemplates.ContainsKey(template.ToString()) ? smtpSettings.UserTemplates[template.ToString()] : template.ToString()) : template.ToString();

            return EmailHelperApp.SendMessage(realTemplate, replacements, to, smtpSettings != null ? (smtpSettings.EmailFrom ?? from) : from, cc, subject, attachments,
                fromDisplayName, smtpSettings != null ? smtpSettings.Host : "", smtpSettings != null ? smtpSettings.Puerto : "", smtpSettings != null ? smtpSettings.Usuario : "", smtpSettings != null ? smtpSettings.Contrasenia : "", smtpSettings != null ? smtpSettings.EnableSsl : false);
        }

        public static bool SaveMessage(int idUsuario, EmailTemplate template, ListDictionary replacements,
          MailAddressCollection to, string from, string cc, string subject, List<string> attachments,
          string fromDisplayName)
        {
            return GuardarEnvioMail(idUsuario, template.ToString(), replacements, to, from, cc, subject, attachments,
                fromDisplayName);
        }

        private static bool GuardarEnvioMail(int idUsuario, string realTemplate, ListDictionary replacements, MailAddressCollection to, string from, string cc, string subject, List<string> attachments, string fromDisplayName)
        {
            using (var dbContext = new ACHEEntities())
            {
                var envioMail = new EnvioMails();
                envioMail.FechaCreacion = DateTime.Now;
                envioMail.IDUsuario = idUsuario;
                envioMail.Template = realTemplate;
                string replacementsStr = string.Join("^", replacements.Cast<DictionaryEntry>().Select(param => param.Key.ToString() + "=" + param.Value.ToString()).ToList());
                envioMail.Reemplazos = replacementsStr;
                envioMail.Receptor = string.Join(",", to);
                envioMail.Emisor = from;
                envioMail.Cc = cc;
                envioMail.Asunto = subject;
                if (attachments != null)
                    envioMail.Adjuntos = string.Join(",", attachments);
                envioMail.EmisorNombre = fromDisplayName;
                dbContext.EnvioMails.Add(envioMail);
                dbContext.SaveChanges();
            }
            return true;
        }

    }

}


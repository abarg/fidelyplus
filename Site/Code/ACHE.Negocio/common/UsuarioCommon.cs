﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Negocio.Facturacion;
using System.IO;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Web;
using System.Configuration;
using ACHE.Model.Negocio;
using ACHE.Negocio.Contabilidad;

namespace ACHE.Negocio.Common {
    public static class UsuarioCommon {
        public static List<Combo2ViewModel> ObtenerPuntosDeVenta(int idUsuario, bool considerarIntegracion = false) {
            try {
                var listaPorDefectoIntegracion = new List<Combo2ViewModel>();
                using (var dbContext = new ACHEEntities()) {
                    if (considerarIntegracion) {
                        listaPorDefectoIntegracion = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario && !x.FechaBaja.HasValue && x.PorDefectoIntegraciones == true).ToList()
                        .Select(x => new Combo2ViewModel() {
                            ID = x.IDPuntoVenta,
                            Nombre = x.Punto.ToString("#0000")
                        }).OrderBy(x => x.Nombre).ToList();
                    }
                    var listaPorDefecto = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario && !x.FechaBaja.HasValue && x.PorDefecto == true).ToList()
                        .Select(x => new Combo2ViewModel() {
                            ID = x.IDPuntoVenta,
                            Nombre = x.Punto.ToString("#0000")
                        }).OrderBy(x => x.Nombre).ToList();

                    var listaDemasElementos = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario && !x.FechaBaja.HasValue && x.PorDefecto != true).ToList()
                        .Select(x => new Combo2ViewModel() {
                            ID = x.IDPuntoVenta,
                            Nombre = x.Punto.ToString("#0000")
                        }).OrderBy(x => x.Nombre).ToList();

                    return listaPorDefectoIntegracion.Union(listaPorDefecto).Union(listaDemasElementos).ToList();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static List<PuntosDeVenta> ObtenerPuntosDeVentaList(int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var listaPorDefecto = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario && !x.FechaBaja.HasValue && x.PorDefecto == true).ToList()
                        .OrderBy(x => x.Punto).ToList();

                    var listaDemasElementos = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == idUsuario && !x.FechaBaja.HasValue && x.PorDefecto != true).ToList()
                        .OrderBy(x => x.Punto).ToList();

                    return listaPorDefecto.Union(listaDemasElementos).ToList();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static List<Combo2ViewModel> ObtenerCondicionesDeVenta(int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var lista = dbContext.CondicionesVentas.Where(x => x.IDUsuario == idUsuario && x.Activa).ToList().OrderBy(x => x.Nombre).Select(x => new Combo2ViewModel() {
                        ID = x.IDCondicionVenta,
                        Nombre = x.Nombre
                    })
                      .ToList();

                    return lista;
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static List<PuntosDeVenta> ObtenerPuntosDeVentaIntegracionesList(int idUsuario, int idUsuarioAdicional) {
            try {
                using (var dbContext = new ACHEEntities()) {

                    var listaPorDefectoIntegracion =
                        dbContext.PuntosDeVenta.Where(
                            x =>
                                x.IDUsuario == idUsuario && !x.FechaBaja.HasValue &&
                                x.PorDefectoIntegraciones)
                            .OrderBy(x => x.Punto).ToList();

                    var listaPorDefecto =
                        dbContext.PuntosDeVenta.Where(
                            x => x.IDUsuario == idUsuario && !x.FechaBaja.HasValue && x.PorDefecto == true)
                            .OrderBy(x => x.Punto).ToList();

                    var listaDemasElementos =
                        dbContext.PuntosDeVenta.Where(
                            x => x.IDUsuario == idUsuario && !x.FechaBaja.HasValue && x.PorDefecto != true)
                            .OrderBy(x => x.Punto).ToList();

                    if (idUsuarioAdicional == 0) {
                        return listaPorDefectoIntegracion.Union(listaPorDefecto).Union(listaDemasElementos).ToList();
                    }
                    else {
                        var listaIdsAdic = dbContext.PuntosDeVentaUsuariosAdicionales.Where(
                            x => x.IDUsuarioAdicional == idUsuarioAdicional).Select(x => x.IDPuntoVenta).ToList();
                        return listaPorDefectoIntegracion.Union(listaPorDefecto).Union(listaDemasElementos).Where(p => listaIdsAdic.Contains(p.IDPuntoVenta)).ToList();
                    }
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static List<Combo2ViewModel> ObtenerPuntosDeVenta(WebUser usu, Boolean puntosDeVentaFromParent = false, bool considerarIntegracion = false) {
            List<Combo2ViewModel> list = new List<Combo2ViewModel>();
            try {
                using (var dbContext = new ACHEEntities()) {
                    if (usu.IDUsuarioAdicional != 0 && puntosDeVentaFromParent == false) {
                        List<Combo2ViewModel> listaPorDefecto = new List<Combo2ViewModel>();
                        List<Combo2ViewModel> listaPorDefectoIntegracion = new List<Combo2ViewModel>();
                        List<Combo2ViewModel> listaDemasElementos = new List<Combo2ViewModel>();

                        var usuAD = dbContext.UsuariosAdicionales
                            .SingleOrDefault(x => x.IDUsuarioAdicional == usu.IDUsuarioAdicional);

                        foreach (var item in usuAD.PuntosDeVentaUsuariosAdicionales) {
                            var puntoDeVenta = dbContext.PuntosDeVenta.SingleOrDefault(x => x.IDPuntoVenta == item.IDPuntoVenta);
                            if (puntoDeVenta == null)
                                continue;

                            //Verificamos que el punto de venta pertenezca al usuario identificado
                            if (puntoDeVenta.IDUsuario != usu.IDUsuario)
                                continue;

                            var itemToAdd = new Combo2ViewModel() {
                                ID = puntoDeVenta.IDPuntoVenta,
                                Nombre = puntoDeVenta.Punto.ToString("#0000")
                            };
                            if (considerarIntegracion) {
                                if (!puntoDeVenta.FechaBaja.HasValue && puntoDeVenta.PorDefectoIntegraciones)
                                    listaPorDefectoIntegracion.Add(itemToAdd);
                            }
                            if (!puntoDeVenta.FechaBaja.HasValue && puntoDeVenta.PorDefecto)
                                listaPorDefecto.Add(itemToAdd);
                            else if (!puntoDeVenta.FechaBaja.HasValue && puntoDeVenta.PorDefecto != true)
                                listaDemasElementos.Add(itemToAdd);
                        }


                        listaPorDefecto = listaPorDefecto.OrderBy(x => x.Nombre).ToList();
                        listaDemasElementos = listaDemasElementos.OrderBy(x => x.Nombre).ToList();

                        list = listaPorDefectoIntegracion.Union(listaPorDefecto).Union(listaDemasElementos).ToList();
                    }
                    else {
                        list = UsuarioCommon.ObtenerPuntosDeVenta(usu.IDUsuario, considerarIntegracion);
                    }
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            return list;
        }

        public static List<Combo2ViewModel> ObtenerCategorias(int idUsuario) {

            using (var dbContext = new ACHEEntities()) {
                return dbContext.Categorias.Where(x => x.IDUsuario == idUsuario).ToList()
                    .Select(x => new Combo2ViewModel() {
                        ID = x.IDCategoria,
                        Nombre = x.Nombre
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }

        public static List<Combo2ViewModel> ObtenerConceptos(int idUsuario, string tipo) {
            using (var dbContext = new ACHEEntities()) {
                return dbContext.Conceptos.Where(x => x.Estado == "A" && x.IDUsuario == idUsuario && (tipo == "" || x.Tipo == tipo))
                    .Select(x => new Combo2ViewModel() {
                        ID = x.IDConcepto,
                        Nombre = x.Codigo
                    }).OrderBy(x => x.Nombre).ToList();
            }
        }

        public static List<EmpresasViewModel> ListaEmpresasDisponibles(WebUser usu, ACHEEntities dbContext) {
            List<EmpresasViewModel> list = new List<EmpresasViewModel>();
            if (usu.IDUsuarioAdicional != 0) {
                //Carga los datos si es una usuario Adicional
                var results = dbContext.UsuariosEmpresa.Include("Usuarios").Where(x => x.IDUsuarioAdicional == usu.IDUsuarioAdicional && x.Usuarios.Activo).ToList();
                Usuarios usuarioPadre;
                if (string.IsNullOrWhiteSpace(usu.IDUsuarioPadre.ToString())) {
                    usuarioPadre = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario && x.IDUsuarioPadre == null).FirstOrDefault();
                }
                else {
                    usuarioPadre = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuarioPadre).FirstOrDefault();
                }
                list = esUsuAdicional(list, results, usuarioPadre);
            }
            else {
                IList<Usuarios> results = null;
                if (string.IsNullOrWhiteSpace(usu.IDUsuarioPadre.ToString())) {
                    //Soy un usuario Padre y busco todas mis empresas
                    results = dbContext.Usuarios.Where(x => x.IDUsuarioPadre == usu.IDUsuario && x.Activo).ToList();
                    list = esUsuarioPadre(list, results, usu);
                }
                else {
                    //Soy un usuario Padre pero estoy logiado con otra de mis  empresas y busco todas mis empresas
                    results = dbContext.Usuarios.Where(x => x.IDUsuarioPadre == usu.IDUsuarioPadre && x.Activo).ToList();
                    var usuarioPadre = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuarioPadre).FirstOrDefault();
                    list = esCambioSesion(list, results, usuarioPadre);
                }
            }
            return list;
        }

        private static List<EmpresasViewModel> esCambioSesion(List<EmpresasViewModel> list, IList<Usuarios> results, Usuarios usuarioPadre) {
            list = results.Select(x => new EmpresasViewModel() {
                ID = x.IDUsuario,
                RazonSocial = x.RazonSocial.ToUpper(),
                CUIT = x.CUIT,
                CondicionIva = UsuarioCommon.GetCondicionIvaDesc(x.CondicionIva),
                Email = x.Email.ToLower(),
                Domicilio = x.Domicilio,
                Ciudad = x.Ciudades.Nombre,
                Provincia = x.Provincias.Nombre,
                Logo = (string.IsNullOrEmpty(x.Logo)) ? "/files/usuarios/" + "no-photo.png" : "/files/usuarios/" + usuarioPadre.Logo
            }).ToList();

            var empresa = new EmpresasViewModel();
            empresa.ID = usuarioPadre.IDUsuario;
            empresa.RazonSocial = usuarioPadre.RazonSocial.ToUpper();
            empresa.CUIT = usuarioPadre.CUIT;
            empresa.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usuarioPadre.CondicionIva);
            empresa.Email = usuarioPadre.Email.ToLower();
            empresa.Domicilio = usuarioPadre.Domicilio;
            empresa.Ciudad = usuarioPadre.Ciudades.Nombre;
            empresa.Provincia = usuarioPadre.Provincias.Nombre;
            empresa.Logo = (string.IsNullOrEmpty(usuarioPadre.Logo)) ? "/files/usuarios/" + "no-photo.png" : "/files/usuarios/" + usuarioPadre.Logo;
            list.Add(empresa);

            return list;
        }

        private static List<EmpresasViewModel> esUsuarioPadre(List<EmpresasViewModel> list, IList<Usuarios> results, WebUser usu) {
            list = results.Select(x => new EmpresasViewModel() {
                ID = x.IDUsuario,
                RazonSocial = x.RazonSocial.ToUpper(),
                CUIT = x.CUIT,
                CondicionIva = UsuarioCommon.GetCondicionIvaDesc(x.CondicionIva),
                Email = x.Email.ToLower(),
                Domicilio = x.Domicilio,
                Ciudad = x.Ciudades.Nombre,
                Provincia = x.Provincias.Nombre,
                Logo = (string.IsNullOrEmpty(x.Logo)) ? "/files/usuarios/" + "no-photo.png" : "/files/usuarios/" + x.Logo
            }).ToList();


            //Agrego la empresa actual
            var empresa = new EmpresasViewModel();
            empresa.ID = usu.IDUsuario;
            empresa.RazonSocial = usu.RazonSocial.ToUpper();
            empresa.CUIT = usu.CUIT;
            empresa.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usu.CondicionIVA);
            empresa.Email = usu.Email.ToLower();
            empresa.Domicilio = usu.Domicilio.ToLower();
            empresa.Ciudad = usu.Ciudad.ToLower();
            empresa.Provincia = usu.Provincia.ToLower();
            empresa.Logo = (string.IsNullOrEmpty(usu.Logo)) ? "/files/usuarios/" + "no-photo.png" : "/files/usuarios/" + usu.Logo;
            list.Add(empresa);

            return list;
        }

        private static List<EmpresasViewModel> esUsuAdicional(List<EmpresasViewModel> list, List<UsuariosEmpresa> results, Usuarios usuarioPadre) {
            list = results.Select(x => new EmpresasViewModel() {
                ID = x.IDUsuario,
                RazonSocial = x.Usuarios.RazonSocial.ToUpper(),
                CUIT = x.Usuarios.CUIT,
                CondicionIva = UsuarioCommon.GetCondicionIvaDesc(x.Usuarios.CondicionIva),
                Email = x.Usuarios.Email.ToLower(),
                Domicilio = x.Usuarios.Domicilio,
                Ciudad = x.Usuarios.Ciudades.Nombre,
                Provincia = x.Usuarios.Provincias.Nombre,
                Logo = (string.IsNullOrWhiteSpace(x.Usuarios.Logo)) ? "/files/usuarios/" + "no-photo.png" : "/files/usuarios/" + usuarioPadre.Logo
            }).ToList();
            return list;
        }



        public static void CreateFolders(int idUsuario, string basePath) {
            Directory.CreateDirectory(basePath + "//" + idUsuario);
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Contactos/");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Empleados/");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Compras/");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Comprobantes/");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Productos-Servicios");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Cheques");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Caja");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Presupuestos");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Remitos");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/OrdenCompra");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/MovimientosDeFondos");
        }

        public static void CreateFoldersRI(int idUsuario, string basePath) {
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Balances");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Cargas-Sociales");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Impuestos");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Impuestos/Bienes-Acciones-Participaciones");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Impuestos/IB");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Impuestos/Iva");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Impuestos/Ganancias");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Impuestos/Monotributo");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Impuestos/Sicore");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Societarios");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Societarios/Estatutos");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Societarios/Socios");
            Directory.CreateDirectory(basePath + "//" + idUsuario + "/Societarios/Apoderados");
        }

        public static string GetCondicionIvaDesc(string tipo) {
            if (tipo == "RI")
                return "Responsable Inscripto";
            else if (tipo == "MO")
                return "Monotributo";
            else if (tipo == "EX")
                return "Exento";
            else if (tipo == "NI")
                return "Responsable NO inscripto";
            else
                return "Consumidor final";
        }

        public static bool TienePersonas(int idUsuario) {
            try {
                bool tiene = false;

                using (var dbContext = new ACHEEntities()) {
                    tiene = dbContext.Personas.Any(x => x.IDUsuario == idUsuario);
                }

                return tiene;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        internal static Usuarios ObtenerUsuarioPorNroDoc(string nroCUIT) {
            try {
                using (var dbContext = new ACHEEntities())
                    return dbContext.Usuarios.Include("Provincias").Include("Ciudades").Where(x => x.CUIT == nroCUIT).FirstOrDefault();
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static void CambiarPassword(string passwordActual, string PasswordNuevo, string Passwordverificado, WebUser usu) {
            try {
                if (PasswordNuevo != Passwordverificado)
                    throw new CustomException("El password ingresado no coinciden entre si");

                using (var dbContext = new ACHEEntities()) {
                    bool esAdicional = usu.IDUsuarioAdicional != 0;
                    if (!esAdicional) {
                        var entity = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (entity.Pwd != passwordActual)
                            throw new CustomException("La contraseña actual ingresada es incorrecta");
                        else
                            entity.Pwd = PasswordNuevo;

                        dbContext.SaveChanges();
                        avisarCambioDePwd(entity.Email, entity.RazonSocial);
                    }
                    else {
                        var entity = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == usu.IDUsuarioAdicional).First();
                        if (entity.Pwd != passwordActual)
                            throw new CustomException("La contraseña actual ingresada es incorrecta");
                        else
                            entity.Pwd = PasswordNuevo;

                        dbContext.SaveChanges();
                        avisarCambioDePwd(entity.Email, "usuario/a");
                    }
                }
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        private static void avisarCambioDePwd(string email, string nombre) {
            try {
                if (!email.IsValidEmailAddress())
                    throw new CustomException("Email incorrecto.");

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<USUARIO>", nombre);

                bool send = EmailCommon.SendMessage(EmailTemplate.ModificacionPwd.ToString(), replacements, email, "Contabilium: Modificación de contraseña");
                if (!send)
                    throw new Exception("El email avisando el cambio de contraseña no pudo ser enviado");
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static void GuardarConfiguracion(string razonSocial, string condicionIva, string cuit, string iibb, string fechaInicio, string personeria, string emailFrom, string emailAlertas, string telefono, string celular, string contacto, string idProvincia, string idCiudad, string domicilio, string pisoDepto, string cp, bool esAgentePersepcionIVA, bool esAgentePersepcionIIBB, bool esAgenteRetencion, bool exentoIIBB, string fechaCierreContable, string fechaLimiteCarga, string idJurisdiccion, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();

                    if (string.IsNullOrWhiteSpace(idProvincia) || idProvincia == "null")
                        throw new CustomException("Debe ingresar la provincia del domicilio fiscal.");
                    if (string.IsNullOrWhiteSpace(idCiudad) || idCiudad == "null")
                        throw new CustomException("Debe ingresar la ciudad del domicilio fiscal.");
                    if (dbContext.Usuarios.Any(x => x.CUIT == cuit && x.IDUsuario != idUsuario))
                        throw new CustomException("El CUIT ingresado ya se encuentra registrado.");
                    if (!cuit.IsValidCUIT())
                        throw new CustomException("El CUIT es invalido.");
                    else {
                        entity.RazonSocial = razonSocial;
                        if (!string.IsNullOrWhiteSpace(fechaInicio))
                            entity.FechaInicioActividades = DateTime.Parse(fechaInicio);
                        else
                            entity.FechaInicioActividades = null;

                        entity.CondicionIva = condicionIva;
                        entity.CUIT = cuit;
                        entity.IIBB = iibb;
                        entity.Personeria = personeria;
                        entity.EmailFrom = emailFrom;
                        entity.EmailAlertas = emailAlertas;

                        entity.Telefono = telefono;
                        entity.Celular = celular;
                        entity.Contacto = contacto;
                        entity.ExentoIIBB = exentoIIBB;
                        //Domicilio
                        entity.IDProvincia = Convert.ToInt32(idProvincia);
                        entity.IDCiudad = Convert.ToInt32(idCiudad);
                        entity.Domicilio = domicilio;
                        entity.PisoDepto = pisoDepto;
                        entity.CodigoPostal = cp;

                        entity.EsAgentePercepcionIVA = esAgentePersepcionIVA;
                        entity.EsAgentePercepcionIIBB = esAgentePersepcionIIBB;
                        entity.EsAgenteRetencion = esAgenteRetencion;
                        entity.IDJurisdiccion = idJurisdiccion.Trim();

                        if (!string.IsNullOrWhiteSpace(fechaCierreContable))
                            entity.FechaCierreContable = Convert.ToDateTime(fechaCierreContable);
                        else
                            entity.FechaCierreContable = null;

                        if (!string.IsNullOrWhiteSpace(fechaLimiteCarga))
                            entity.FechaLimiteCarga = Convert.ToDateTime(fechaLimiteCarga);
                        else
                            entity.FechaLimiteCarga = null;

                        //if (entity.CondicionIva == "MO" && !entity.SetupRealizado)
                        if (entity.CondicionIva == "MO" && !entity.SetupRealizado)
                            entity.UsaPrecioFinalConIVA = true;

                        if (!entity.SetupRealizado) {
                            if (entity.CUIT != string.Empty && entity.Domicilio != null && entity.Domicilio.Trim() != "")
                                entity.SetupRealizado = true;
                        }

                        dbContext.SaveChanges();
                    }
                }
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static UsuariosAPIViewModel ObtenerConfiguracion(WebUser usu) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    UsuariosAPIViewModel usuv = new UsuariosAPIViewModel();

                    usuv.RazonSocial = entity.RazonSocial;
                    usuv.CondicionIva = entity.CondicionIva;
                    usuv.CUIT = entity.CUIT;
                    usuv.Email = entity.Email;
                    usuv.EmailAlertas = entity.EmailAlertas;
                    usuv.FechaInicioActividades = entity.FechaInicioActividades;
                    usuv.Personeria = entity.Personeria;
                    usuv.ExentoIIBB = entity.ExentoIIBB;
                    usuv.IIBB = entity.IIBB;
                    usuv.Contacto = entity.Contacto;
                    usuv.Celular = entity.Celular;

                    usuv.IDProvincia = entity.IDProvincia;
                    usuv.IDCiudad = entity.IDCiudad;
                    usuv.Domicilio = entity.Domicilio;
                    usuv.PisoDepto = entity.PisoDepto;
                    usuv.CodigoPostal = entity.CodigoPostal;
                    usuv.Telefono = entity.Telefono;

                    usuv.EsAgentePercepcionIVA = entity.EsAgentePercepcionIVA;
                    usuv.EsAgentePercepcionIIBB = entity.EsAgentePercepcionIIBB;
                    usuv.EsAgenteRetencion = entity.EsAgenteRetencion;
                    usuv.IDJurisdiccion = entity.IDJurisdiccion;

                    return usuv;
                }
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static void GuardarLoginUsuarios(string email, int? idUsuario, int? idUsuarioAdicional, string Observaciones) {
            using (var dbCotnext = new ACHEEntities()) {
                var entity = new LoginUsuarios();
                entity.EmailLogin = email;
                entity.Observacion = Observaciones;
                entity.FechaLogin = DateTime.Now;

                if (idUsuario != 0)
                    entity.IDUsuario = idUsuario;
                else
                    entity.IDUsuario = null;
                if (idUsuarioAdicional != 0)
                    entity.IDUsuarioAdicional = idUsuarioAdicional;
                else
                    entity.IDUsuarioAdicional = null;

                dbCotnext.LoginUsuarios.Add(entity);
                dbCotnext.SaveChanges();

            }
        }

        public static byte ObtenerCantidadDecimales(int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Usuarios.FirstOrDefault(x => x.IDUsuario == idUsuario);
                    if (entity != null)
                        return entity.CantidadDecimales ?? 2;

                    //Retornamos un valor default
                    return 2;
                }
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static void cambiarEmpresa(int idEmpresa) {
            using (var dbContext = new ACHEEntities()) {
                var usuLogin = (WebUser)HttpContext.Current.Session["CurrentUser"];
                var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idEmpresa && x.Activo).FirstOrDefault();

                if (usu != null) {
                    bool tieneMultiempresa = false;
                    if (usuLogin.IDUsuarioAdicional != 0 && usu.IDUsuarioPadre != null)
                        tieneMultiempresa = dbContext.UsuariosEmpresa.Any(x => x.IDUsuarioAdicional == usuLogin.IDUsuarioAdicional);
                    else
                        tieneMultiempresa = true;

                    var UsaFechaFinPlan = true;
                    bool PlanVigente;

                    var idPlan = PermisosModulos.ObtenerTodosLosFormularios(dbContext, usu.IDUsuario);
                    PlanVigente = PermisosModulos.PlanVigente(dbContext, usu.IDUsuario);

                    PermisosModulos.obtenerRolesUsuarioAdicional(dbContext, usuLogin.IDUsuarioAdicional, usuLogin.TipoUsuario);
                    PermisosModulos.obtenerPermisosUsuarioAdicional(dbContext, usuLogin.IDUsuarioAdicional, usuLogin.TipoUsuario);
                    //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, usu.IDUsuario, usu.CondicionIva);
                    HttpContext.Current.Session["CurrentUser"] = new WebUser(
                        usu.IDUsuario, usuLogin.IDUsuarioAdicional, usuLogin.TipoUsuario, usu.RazonSocial, usu.CUIT, usu.CondicionIva,
                        usu.Email, usu.EmailFrom, "", usu.Domicilio + " " + usu.PisoDepto, usu.Pais, usu.IDProvincia,
                        usu.IDCiudad, usu.Telefono, usu.TieneFacturaElectronica, usu.IIBB, usu.FechaInicioActividades,
                        usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupRealizado, tieneMultiempresa, !usu.UsaProd, idPlan, usu.EmailAlertas, usu.Provincias.Nombre, usu.Ciudades.Nombre,
                        usu.EsAgentePercepcionIVA, usu.EsAgentePercepcionIIBB, usu.EsAgenteRetencion, PlanVigente, usu.TieneDebitoAutomatico, UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB,
                        usu.UsaPrecioFinalConIVA, usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
                        usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenesPago, usu.MostrarBonificacionFc, usu.PorcentajeDescuento);

                    var usuOrig = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                    if (usuOrig.Activo) {
                        HttpCookie cookieRememberMe = new HttpCookie("ContabiliumRecordarme");
                        cookieRememberMe.Value = "F";
                        cookieRememberMe.Expires = DateTime.Now.AddDays(14);
                        HttpContext.Current.Response.Cookies.Add(cookieRememberMe);

                        usuOrig.FechaUltLogin = DateTime.Now;
                        dbContext.SaveChanges();

                        HttpContext.Current.Response.Cookies.Remove("ContabiliumUsuario");
                        HttpContext.Current.Response.Cookies.Remove("ContabiliumPwd");

                    }
                    else
                        throw new Exception("El usuario no se encuentra Activo");
                }
                else
                    throw new Exception("Usuario y/o contraseña incorrecta.");

            }
        }

        public static async Task<IEnumerable<UsuariosAdicionales>> ObtenerUsuariosAdicionales(ACHEEntities dbContext, int idUsuario, string rol) {
            try {
                return await dbContext.UsuariosAdicionales.Include("RolesUsuariosAdicionales").Where(x => x.IDUsuario == idUsuario && x.RolesUsuariosAdicionales.Any(r => r.Roles.Nombre == rol)).ToListAsync();
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static void RecuperarPassword(string email) {
            if (!email.IsValidEmailAddress())
                throw new Exception("Email incorrecto.");

            using (var dbContext = new ACHEEntities()) {
                var usuView = dbContext.UsuariosView.Where(x => x.Email.ToLower() == email.ToLower() && x.Activo).FirstOrDefault();
                if (usuView != null) {
                    string newPwd = string.Empty;
                    newPwd = newPwd.GenerateRandom(10);

                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<PASSWORD>", newPwd);
                    if (usuView.IDUsuarioAdicional == 0)
                        replacements.Add("<USUARIO>", usuView.RazonSocial);
                    else
                        replacements.Add("<USUARIO>", "usuario");

                    bool send = EmailCommon.SendMessage(usuView.IDUsuario, EmailTemplate.RecuperoPwd, replacements, usuView.Email, "Contabilium: Recuperación de contraseña");

                    if (!send)
                        throw new Exception("El email con tu nueva contraseña no pudo ser enviado.");
                    else {
                        if (usuView.IDUsuarioAdicional == 0) {
                            var usu = dbContext.Usuarios.Where(x => x.IDUsuario == usuView.IDUsuario).FirstOrDefault();
                            usu.CantIntentos = 0;
                            usu.EstaBloqueado = false;
                            usu.Pwd = newPwd;
                        }
                        else {
                            var usu =
                                dbContext.UsuariosAdicionales.Where(
                                    x => x.IDUsuarioAdicional == usuView.IDUsuarioAdicional).FirstOrDefault();
                            usu.Pwd = newPwd;
                            usu.CantIntentos = 0;
                            usu.EstaBloqueado = false;
                        }
                        dbContext.SaveChanges();
                    }
                }
                else {
                    throw new Exception("No existe un usuario registrado con el mail ingresado.");
                }
            }
        }

        public static void Registrar(UsuarioDto usu) {
            using (var dbContext = new ACHEEntities()) {
                if (esMailFalso(usu.Email.ToLower()))//mails temporarios
                    throw new Exception("El E-mail ingresado es inválido.");
                if (dbContext.Usuarios.Any(x => x.Email == usu.Email))
                    throw new Exception("El E-mail ingresado ya se encuentra registrado.");
                if (!string.IsNullOrEmpty(usu.CUIT) && dbContext.Usuarios.Any(x => x.CUIT == usu.CUIT))
                    throw new Exception("El CUIT ingresado ya se encuentra registrado.");
                if (!string.IsNullOrEmpty(usu.CUIT) && !usu.CUIT.IsValidCUIT())
                    throw new Exception("El CUIT es inválido.");
                else {
                    Usuarios entity = new Usuarios();

                    entity.RazonSocial = usu.RazonSocial; //razonSocial;
                    entity.EmailFrom = usu.RazonSocial;
                    entity.FechaInicioActividades = null;
                    entity.CondicionIva = usu.CondicionIVA ?? "MO"; //condicionIva;
                    entity.CUIT = usu.CUIT ?? "";// cuit;
                    entity.IIBB = string.Empty;
                    entity.Personeria = usu.Personeria ?? ""; //personeria;
                    entity.Email = usu.Email;
                    entity.EmailAlertas = usu.Email;
                    entity.EmailStock = usu.Email;
                    entity.Pwd = usu.Pwd;
                    entity.Telefono = usu.Telefono;
                    entity.Celular = string.Empty;
                    entity.Contacto = string.Empty;
                    entity.Actividad = usu.Actividad;

                    //Domicilio
                    entity.Pais = "Argentina";
                    entity.IDProvincia = usu.IDProvincia ?? 2;//Ciudad de Buenos Aires
                    entity.IDCiudad = usu.IDCiudad ?? 5003;//SIN IDENTIFICAR
                    entity.Domicilio = usu.Domicilio ?? string.Empty;
                    entity.PisoDepto = string.Empty;
                    entity.CodigoPostal = usu.CodigoPostal ?? string.Empty;

                    entity.Theme = "default";
                    entity.TemplateFc = "default";
                    entity.FechaAlta = DateTime.Now;
                    entity.FechaUltLogin = DateTime.Now;
                    entity.TieneFacturaElectronica = false;
                    entity.Logo = null;
                    entity.Activo = true;
                    entity.IDPlan = 4;
                    entity.FechaFinPlan = DateTime.Now.AddDays(10);
                    entity.UsaFechaFinPlan = true;
                    entity.SetupRealizado = false;
                    entity.UsaProd = false;

                    entity.IDUsuarioPadre = null;
                    entity.CodigoPromo = usu.CodigoPromocion;

                    if (!string.IsNullOrEmpty(usu.Descuento))
                        entity.PorcentajeDescuento = decimal.Parse(usu.Descuento);
                    else
                        entity.PorcentajeDescuento = 0;

                    entity.ApiKey = Guid.NewGuid().ToString().Replace("-", "");

                    entity.EsAgentePercepcionIVA = false;
                    entity.EsAgentePercepcionIIBB = false;
                    entity.IDJurisdiccion = entity.IDProvincia.ToString();
                    entity.EsAgenteRetencion = false;
                    entity.CantidadEmpresas = 1;
                    entity.ExentoIIBB = false;
                    entity.UsaPrecioFinalConIVA = true;
                    entity.EnvioAutomaticoComprobante = false;
                    entity.EnvioAutomaticoRecibo = false;
                    entity.EsContador = false;
                    entity.UsaPlanCorporativo = false;
                    entity.CantidadDecimales = 2;
                    entity.MostrarBonificacionFc = true;
                    entity.VenderSinStock = true;

                    try {
                        PlanesPagos p = new PlanesPagos();
                        p.IDUsuario = entity.IDUsuario;
                        p.IDPlan = 6;
                        p.ImportePagado = 0;
                        p.PagoAnual = false;
                        p.FormaDePago = "-";
                        p.NroReferencia = "";
                        p.Estado = "Aceptado";
                        p.FechaDeAlta = DateTime.Now.Date;
                        p.FechaDePago = DateTime.Now.Date;
                        p.FechaInicioPlan = p.FechaDePago;
                        p.FechaFinPlan = DateTime.Now.AddDays(int.Parse(ConfigurationManager.AppSettings["Registro.DiasPrueba"]));

                        dbContext.PlanesPagos.Add(p);

                        PuntosDeVenta punto = new PuntosDeVenta();
                        punto.FechaAlta = DateTime.Now;
                        punto.Punto = 1;
                        punto.PorDefecto = true;
                        entity.PuntosDeVenta.Add(punto);

                        Inventarios inventario = new Inventarios();
                        inventario.IDPuntoVenta = punto.IDPuntoVenta;
                        inventario.FechaAlta = DateTime.Now;
                        inventario.Nombre = "Principal";
                        inventario.IDProvincia = 2;
                        inventario.IDCiudad = 5003;
                        inventario.Direccion = string.Empty;
                        inventario.Telefono = usu.Telefono;
                        inventario.FechaUltimoInventarioFisico = DateTime.Now;
                        inventario.Activo = true;
                        inventario.PorDefectoReservas = true;
                        entity.Inventarios.Add(inventario);

                        dbContext.Inventarios.Add(inventario);

                        Categorias cat = new Categorias();
                        cat.Nombre = "General";
                        entity.Categorias.Add(cat);

                        Categorias cat2 = new Categorias();
                        cat2.Nombre = "Honorarios";
                        entity.Categorias.Add(cat2);

                        Categorias cat3 = new Categorias();
                        cat3.Nombre = "Gastos varios";
                        entity.Categorias.Add(cat3);

                        Categorias cat4 = new Categorias();
                        cat4.Nombre = "Nafta";
                        entity.Categorias.Add(cat4);

                        Categorias cat5 = new Categorias();
                        cat5.Nombre = "Equipamiento";
                        entity.Categorias.Add(cat5);

                        Bancos banco = new Bancos();
                        banco.Moneda = "Pesos Argentinos";
                        banco.SaldoInicial = 0;
                        banco.NroCuenta = "";
                        banco.IDBancoBase = dbContext.BancosBase.Where(x => x.Nombre == "Default").FirstOrDefault().IDBancoBase;
                        banco.FechaAlta = DateTime.Now;
                        banco.Activo = true;
                        entity.Bancos.Add(banco);

                        Bancos banco2 = new Bancos();
                        banco2.Moneda = "Pesos Argentinos";
                        banco2.SaldoInicial = 0;
                        banco2.NroCuenta = "";
                        banco2.IDBancoBase = 80;//MercadoPago dbContext.BancosBase.Where(x => x.Nombre == "Default").FirstOrDefault().IDBancoBase;
                        banco2.FechaAlta = DateTime.Now;
                        banco2.Activo = true;
                        entity.Bancos.Add(banco2);

                        Cajas caj = new Cajas();
                        caj.Nombre = "Caja en pesos";
                        entity.Cajas.Add(caj);// = new List<Cajas> { new Cajas { Nombre = "Caja en pesos" } };

                        #region cond ventas

                        CondicionesVentas condVenta1 = new CondicionesVentas();
                        condVenta1.Activa = true;
                        condVenta1.FechaAlta = DateTime.Now;
                        condVenta1.Nombre = "Efectivo";
                        condVenta1.CobranzaAutomatica = true;
                        condVenta1.FormaDePago = "Efectivo";
                        condVenta1.Cajas = caj;
                        entity.CondicionesVentas.Add(condVenta1);

                        CondicionesVentas condVenta2 = new CondicionesVentas();
                        condVenta2.Activa = true;
                        condVenta2.FechaAlta = DateTime.Now;
                        condVenta2.Nombre = "Cheque";
                        condVenta2.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta2);

                        CondicionesVentas condVenta3 = new CondicionesVentas();
                        condVenta3.Activa = true;
                        condVenta3.FechaAlta = DateTime.Now;
                        condVenta3.Nombre = "Cuenta corriente";
                        condVenta3.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta3);

                        CondicionesVentas condVenta4 = new CondicionesVentas();
                        condVenta4.Activa = true;
                        condVenta4.FechaAlta = DateTime.Now;
                        condVenta4.Nombre = "MercadoPago";
                        condVenta4.CobranzaAutomatica = true;
                        condVenta4.FormaDePago = "Transferencia";
                        condVenta4.Bancos = banco2;//AVISAR QUE NO SE CREA EL PLAN DE CUENTA
                        entity.CondicionesVentas.Add(condVenta4);

                        CondicionesVentas condVenta5 = new CondicionesVentas();
                        condVenta5.Activa = true;
                        condVenta5.FechaAlta = DateTime.Now;
                        condVenta5.Nombre = "Tarjeta de debito";
                        condVenta5.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta5);

                        CondicionesVentas condVenta6 = new CondicionesVentas();
                        condVenta6.Activa = true;
                        condVenta6.FechaAlta = DateTime.Now;
                        condVenta6.Nombre = "Tarjeta de credito";
                        condVenta6.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta6);

                        CondicionesVentas condVenta7 = new CondicionesVentas();
                        condVenta7.Activa = true;
                        condVenta7.FechaAlta = DateTime.Now;
                        condVenta7.Nombre = "Ticket";
                        condVenta7.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta7);

                        CondicionesVentas condVenta8 = new CondicionesVentas();
                        condVenta8.Activa = true;
                        condVenta8.FechaAlta = DateTime.Now;
                        condVenta8.Nombre = "PayU";
                        condVenta8.CobranzaAutomatica = false;
                        entity.CondicionesVentas.Add(condVenta8);

                        #endregion

                        dbContext.Usuarios.Add(entity);

                        if (usu.Actividad == "Soy contador")
                            entity.UsaPlanCorporativo = true;

                        dbContext.SaveChanges();
                        int idCaja = dbContext.Cajas.Where(x => x.IDUsuario == entity.IDUsuario).First().IDCaja;

                        PermisosModulos.ObtenerTodosLosFormularios(dbContext, entity.IDUsuario);

                        //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, entity.IDUsuario, entity.CondicionIva);
                        
                        
                        HttpContext.Current.Session["CurrentUser"] = new WebUser(entity.IDUsuario, 0, "A", entity.RazonSocial, entity.CUIT, entity.CondicionIva,
                            entity.Email, entity.EmailFrom, "", entity.Domicilio + " " + entity.PisoDepto, entity.Pais, entity.IDProvincia, entity.IDCiudad, entity.Telefono,
                            entity.TieneFacturaElectronica, entity.IIBB, entity.FechaInicioActividades, "", entity.TemplateFc, entity.IDUsuarioPadre, entity.SetupRealizado, false,
                            !entity.UsaProd, entity.IDPlan, entity.EmailAlertas, "", "", entity.EsAgentePercepcionIVA, entity.EsAgentePercepcionIIBB,
                            entity.EsAgenteRetencion, true, entity.TieneDebitoAutomatico, entity.UsaFechaFinPlan, entity.ApiKey, entity.ExentoIIBB, entity.UsaPrecioFinalConIVA,
                            entity.FechaAlta, entity.EnvioAutomaticoComprobante, entity.EnvioAutomaticoRecibo, entity.IDJurisdiccion, entity.UsaPlanCorporativo,
                            entity.ObservacionesFc, entity.ObservacionesRemito, entity.ObservacionesPresupuestos, entity.ObservacionesOrdenesPago, entity.MostrarBonificacionFc, entity.PorcentajeDescuento, false);

                        if (usu.Actividad == "Soy contador") {
                            dbContext.ConfigurarPlanCorporativo(entity.IDUsuario);
                            var listaBancos = dbContext.Bancos.Where(x => x.IDUsuario == entity.IDUsuario).ToList();
                            foreach (var item in listaBancos)
                                ContabilidadCommon.CrearCuentaBancos(item.IDBanco, null, (WebUser)HttpContext.Current.Session["CurrentUser"]);

                            ContabilidadCommon.CrearCuentaCaja(idCaja, (WebUser)HttpContext.Current.Session["CurrentUser"]);
                        }

                        CrearDatosPrincipales(idCaja);

                        ListDictionary replacements = new ListDictionary();
                        replacements.Add("<USUARIO>", entity.RazonSocial);
                        try {
                            bool send = EmailCommon.SendMessage(entity.IDUsuario, EmailTemplate.Bienvenido, replacements, entity.Email, "¡Bienvenido a Contabilium! Te cuento cómo empezar a usarlo");

                        }
                        catch (Exception e) {
                            //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "Fallo en envío de mails", e.ToString());
                        }
                        //try
                        //{
                        //    bool send2 = EmailCommon.SendMessage(entity.IDUsuario, EmailTemplate.Notificacion, replacements, "roni@inlat.biz", "Contabilium: Nuevo registro " + entity.CodigoPromo + " " + entity.Email);
                        //}
                        //catch (Exception e)
                        //{
                        //    //BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "Fallo en envío de mails", e.ToString());
                        //}
                    }
                    catch (Exception e) {
                        var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                        BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                        throw e;
                    }
                }
            }
        }

        private static bool esMailFalso(string email) {
            if (email.Contains("mvrht.com") || email.Contains("mvrht.net") || email.Contains("trbvn.com") || email.Contains("my10minutemail.com")
                || email.Contains("20email.eu") || email.Contains("20mail.it") || email.Contains("yopmail.com")
                || email.Contains("mailinator.com") || email.Contains("haribu.com") || email.Contains("nowmymail.com")
                || email.Contains("clrmail.com") || email.Contains("@guerrillamail") || email.Contains("throwam.com")
                || email.Contains("stexsy.com") || email.Contains("lackmail.ru") || email.Contains("dispostable.com")
                || email.Contains("spamgourmet.com") || email.Contains("@trashmail") || email.Contains("inboxproxy.com")
                || email.Contains("instantemailaddress.com") || email.Contains("boun.cr") || email.Contains("mailforspam.com")
                || email.Contains("mailnull.com") || email.Contains("mailnesia.com") || email.Contains("maildrop.cc")
                || email.Contains("33mail.com") || email.Contains("zasod.com") || email.Contains("dayrep.com")
                || email.Contains("posdz.com") || email.Contains("tiapz.com") || email.Contains("isosq.com")
                || email.Contains("yuiop.com")
                )

                return true;
            else
                return false;
        }

        private static void CrearDatosPrincipales(int idCaja) {
            try {
                if (HttpContext.Current.Session["CurrentUser"] != null) {
                    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                    List<AbonosPersonasViewModel> personas = new List<AbonosPersonasViewModel>();
                    var idClientes = DatosInicialesCommon.CrearDatosClientes(usu);
                    var idProveedor = DatosInicialesCommon.CrearDatosProveedores(usu);
                    var comprobante = DatosInicialesCommon.CrearDatosVentas(usu, idClientes);
                    var compra = DatosInicialesCommon.CrearDatosCompras(usu, idProveedor);

                    personas.Add(new AbonosPersonasViewModel() { IDPersona = idClientes, Cantidad = "1" });
                    personas.Add(new AbonosPersonasViewModel() { IDPersona = idProveedor, Cantidad = "1" });
                    var idRubro = DatosInicialesCommon.CrearDatosRubros(usu.IDUsuario);
                    DatosInicialesCommon.CrearDatosConceptos(usu.IDUsuario, idRubro);
                    var idPago = DatosInicialesCommon.CrearDatosPagos(usu, compra, idCaja);
                    var idCobranza = DatosInicialesCommon.CrearDatosCobranzas(usu, comprobante, idCaja);
                    DatosInicialesCommon.CrearDatosPresupuestos(usu, idClientes);
                    //DatosInicialesCommon.CrearDatosAbonos(usu.IDUsuario, personas);

                    if (usu.UsaPlanCorporativo && usu.CondicionIVA == "RI") {
                        ContabilidadCommon.AgregarAsientoDeCompra(compra.IDCompra, usu);
                        ContabilidadCommon.AgregarAsientoDeVentas(usu, comprobante.IDComprobante);
                        ContabilidadCommon.AgregarAsientoDePago(usu, idPago);
                        ContabilidadCommon.AgregarAsientoDeCobranza(usu, idCobranza);
                    }
                }
                else
                    throw new Exception("Por favor, vuelva a iniciar sesión");
            }
            catch (Exception ex) {
                //throw new Exception(ex.Message);
            }
        }

        public static void EnviarContactoShopping(WebUser usu, string mensaje, string tipoPaquete) {

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<MENSAJE>", mensaje);
            replacements.Add("<USUARIO>", usu.RazonSocial);
            replacements.Add("<ID>", usu.IDUsuario);
            replacements.Add("<EMAIL>", usu.Email);
            replacements.Add("<PAQUETE>", tipoPaquete);
            replacements.Add("<NOTIFICACION>", "Hemos recibido su mensaje. Le responderemos a la brevedad");

            bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Compras, replacements, ConfigurationManager.AppSettings["Email.Compras"], "Contabilium: Solicitud de ayuda");
            if (!send) {
                throw new Exception("El mensaje no pudo ser enviado. Por favor, escribenos a <a href='mailto:ayuda@contabilium.com'>ayuda@contabilium.com</a>");
            }

            send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Notificacion, replacements, usu.Email, "Contabilium: Hemos recibido su consulta.");

        }

        public static void EnviarContacto(ListDictionary replacements, string email) {

            bool send = EmailHelper.SendMessage("Notificacion", replacements, email, "info@contabilium.com", "[Landing] Consulta");


        }

        public static async Task<IEnumerable<Combo4ViewModel>> ObtenerUsuariosPOS(int idUsuario, int idUsuarioAdicional) {
            using (var dbContext = new ACHEEntities()) {
                var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                List<int> list = new List<int>();
                if (idUsuarioAdicional != 0) {
                    //Carga los datos si es una usuario Adicional
                    var results = dbContext.UsuariosEmpresa.Include("Usuarios").Where(x => x.IDUsuarioAdicional == idUsuarioAdicional && x.Usuarios.Activo).ToList();
                    list = results.Select(x => x.IDUsuario).ToList();

                }
                else {
                    IList<Usuarios> results = null;
                    if (string.IsNullOrWhiteSpace(usu.IDUsuarioPadre.ToString())) {
                        //Soy un usuario Padre y busco todas mis empresas
                        results = dbContext.Usuarios.Where(x => x.IDUsuarioPadre == usu.IDUsuario && x.Activo).ToList();
                        list = results.Select(x => x.IDUsuario).ToList();


                        list.Add(usu.IDUsuario);
                    }
                    else {
                        //Soy un usuario Padre pero estoy logiado con otra de mis  empresas y busco todas mis empresas
                        results = dbContext.Usuarios.Where(x => x.IDUsuarioPadre == usu.IDUsuarioPadre && x.Activo).ToList();
                        var usuarioPadre = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuarioPadre).FirstOrDefault();
                        list = results.Select(x => x.IDUsuario).ToList();


                        list.Add(usuarioPadre.IDUsuario);
                    }
                }
                var aux = dbContext.Usuarios.Where(x => list.Contains(x.IDUsuario)).ToList();
                var lista = aux.Select(x => new Combo4ViewModel() {
                    ID = x.Email + "#" + x.Pwd,
                    Nombre = x.RazonSocial
                })
                  .ToList();

                return lista;
            }
        }

        public static bool VerificarWebUserSession() {
            // SI NO HAY SESSION, TRATO DE RENOVARLA. SI NO PUEDO TRAER UN WEB USER(YA QUE NO TODOS UTILIZAN COOKIES), RESPONDO FALSE PARA MOSTRAR MENSAJE DE VOLVER A INICIAR SESIóN
            var respuesta = true;
            if (HttpContext.Current.Session["CurrentUser"] == null) {
                UsuarioCommon.RenovarWebUserSession();
                if (HttpContext.Current.Session["CurrentUser"] == null)
                    respuesta = false;
            }
            return respuesta;
        }

        private static void RenovarWebUserSession() {
            string username = "";
            string password = "";
            bool tieneMultiempresa = false;

            HttpCookie cookieUser = HttpContext.Current.Request.Cookies.Get("ContabiliumUsuario");
            HttpCookie cookiePass = HttpContext.Current.Request.Cookies.Get("ContabiliumPwd");

            if (cookieUser != null)
                username = HttpUtility.UrlDecode(cookieUser.Value);

            if (cookiePass != null) {
                password = HttpUtility.UrlDecode(cookiePass.Value);
                password = Encriptar.DesencriptarCadena(password).Replace("\0", "");
            }

            if (username != "" && password != "") {
                using (var dbContext = new ACHEEntities()) {
                    var usu = dbContext.UsuariosView.Where(x => x.Email.ToLower() == username.ToLower() && x.Activo).FirstOrDefault();

                    if (usu != null) {
                        if (usu.Pwd == password && usu.Tipo != "T") {

                            if (usu.IDUsuarioAdicional != 0 && usu.IDUsuarioPadre == null)
                                tieneMultiempresa = dbContext.UsuariosEmpresa.Any(x => x.IDUsuarioAdicional == usu.IDUsuarioAdicional);
                            else
                                tieneMultiempresa = true;

                            var idPlan = PermisosModulos.ObtenerTodosLosFormularios(dbContext, usu.IDUsuario);

                            var PlanVigente = PermisosModulos.PlanVigente(dbContext, usu.IDUsuario);


                            var tipo = PermisosModulos.ObtenerTipoSegunPLan(dbContext, usu.Tipo, idPlan);

                            HttpContext.Current.Session["CurrentUser"] = new WebUser(
                                            usu.IDUsuario, usu.IDUsuarioAdicional, tipo, usu.RazonSocial, usu.CUIT, usu.CondicionIva,
                                            usu.Email, usu.EmailFrom, "", usu.Domicilio + " " + usu.PisoDepto, usu.Pais, usu.IDProvincia,
                                            usu.IDCiudad, usu.Telefono, usu.TieneFacturaElectronica, usu.IIBB, usu.FechaInicioActividades,
                                            usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupRealizado, tieneMultiempresa, !usu.UsaProd,
                                            idPlan, usu.EmailAlertas, usu.Provincia, usu.Ciudad, usu.EsAgentePercepcionIVA, usu.EsAgentePercepcionIIBB,
                                            usu.EsAgenteRetencion, PlanVigente, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB, usu.UsaPrecioFinalConIVA,
                                            usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo, usu.IDJurisdiccion, usu.UsaPlanCorporativo,
                                            usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, usu.ObservacionesOrdenesPago, usu.MostrarBonificacionFc, usu.MostrarProductosEnRemito, usu.PorcentajeDescuento);
                        }
                    }
                }
            }
        }//



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Model.ViewModels;
using ClosedXML.Excel;

namespace ACHE.Negocio.Common
{
    public static class AddinsCommon
    {
        public static List<UsuarioAddins> ObtenerAddins(int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    return
                         dbContext.UsuarioAddins.Include("Addins").Where(
                             a =>
                                 a.IDUsuario == idUsuario && a.Addins.Mensual && a.Activo && a.FechaDesde <= DateTime.Now &&
                                 (a.FechaHasta == null || a.FechaHasta.Value >= DateTime.Now)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool TieneAddin(int idUsuario, string nombreAddin)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    return
                         dbContext.UsuarioAddins.Include("Usuarios").Any(
                             a =>
                                !a.Usuarios.FechaBaja.HasValue && a.IDUsuario == idUsuario && a.Addins.Nombre == nombreAddin && a.Activo && a.FechaDesde <= DateTime.Now &&
                                 (a.FechaHasta == null || a.FechaHasta.Value >= DateTime.Now));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static decimal ObtenerPrecioAddinsMensuales(int idUsuario)
        {
            try
            {
                decimal importeAddins = 0;
                var addinsUsuario = ObtenerAddins(idUsuario);//.ForEach(a => importeAddins += a.Addins.Precio);
                foreach (var a in addinsUsuario) {
                    var total = (a.Addins.Precio * a.Cantidad);
                    if(a.Bonificacion > 0)
                    total -= ((total * a.Bonificacion.Value)/100);
                    importeAddins += total;
                    
                }
                return importeAddins;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string ObtenerValorPropiedadAddin(int idUsuario, string nombreAddin, string nombrePropiedad)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usuarioAddin =
                         dbContext.UsuarioAddins.Include("ParametrosAddin").FirstOrDefault(a => a.IDUsuario == idUsuario && a.Addins.Nombre == nombreAddin && a.Activo && a.FechaDesde <= DateTime.Now &&
                                 (a.FechaHasta == null || a.FechaHasta.Value >= DateTime.Now) && a.ParametrosAddin.Any(y => y.Clave == nombrePropiedad));
                    if (usuarioAddin != null)
                        return
                            usuarioAddin.ParametrosAddin.Where(p => p.Clave == nombrePropiedad)
                                .Select(x => x.Valor)
                                .FirstOrDefault();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static SmtpSettings ObtenerSmtpSettings(int idUsuario, string nombreAddin)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usuarioAddin =
                         dbContext.UsuarioAddins.Include("ParametrosAddin").FirstOrDefault(a => a.IDUsuario == idUsuario && a.Addins.Nombre == nombreAddin && a.Activo && a.FechaDesde <= DateTime.Now &&
                                 (a.FechaHasta == null || a.FechaHasta.Value >= DateTime.Now));
                    if (usuarioAddin != null)
                    {
                        var user = usuarioAddin.ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpUser");
                        var pass = usuarioAddin.ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpPassword");
                        var host = usuarioAddin.ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpHost");
                        var port = usuarioAddin.ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpPort");
                        var from = usuarioAddin.ParametrosAddin.FirstOrDefault(x => x.Clave == "EmailFrom");
                        var ssl = usuarioAddin.ParametrosAddin.FirstOrDefault(x => x.Clave == "EnableSsl");

                        var templates =
                            usuarioAddin.ParametrosAddin.Where(x => x.Clave.Contains("Template")).ToDictionary(x => x.Clave.Replace("Template", ""), x => x.Valor);

                        var smtpSettings = new SmtpSettings
                        {
                            TieneAddinMails = true,
                            Usuario = user != null ? user.Valor : "",
                            Contrasenia = pass != null ? pass.Valor : "",
                            Host = host != null ? host.Valor : "",
                            Puerto = port != null ? port.Valor : "",
                            EmailFrom = from != null ? from.Valor : "",
                            UserTemplates = templates,
                            EnableSsl = ssl != null ? bool.Parse(ssl.Valor) : false,
                        };
                        return smtpSettings;
                    }
                    return new SmtpSettings { TieneAddinMails = false };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static Dictionary<int, SmtpSettings> ObtenerSmtpSettings(string nombreAddin)
        {
            try
            {
                Dictionary<int, SmtpSettings> result = new Dictionary<int, SmtpSettings>();
                using (var dbContext = new ACHEEntities())
                {
                    var usuariosAddin =
                         dbContext.UsuarioAddins.Include("ParametrosAddin").Where(a => a.Addins.Nombre == nombreAddin && a.Activo && a.FechaDesde <= DateTime.Now &&
                                 (a.FechaHasta == null || a.FechaHasta.Value >= DateTime.Now)).GroupBy(x => x.IDUsuario);

                    foreach (var usuarioAddin in usuariosAddin)
                    {

                        var user = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpUser");
                        var pass = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpPassword");
                        var host = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpHost");
                        var port = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpPort");
                        var from = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "EmailFrom");
                        var ssl = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "EnableSsl");
                        var templates =
                             usuarioAddin.FirstOrDefault().ParametrosAddin.Where(x => x.Clave.Contains("Template")).ToDictionary(x => x.Clave.Replace("Template", ""), x => x.Valor);

                        var smtpSettings = new SmtpSettings
                        {
                            TieneAddinMails = true,
                            Usuario = user != null ? user.Valor : "",
                            Contrasenia = pass != null ? pass.Valor : "",
                            Host = host != null ? host.Valor : "",
                            Puerto = port != null ? port.Valor : "",
                            EmailFrom = from != null ? from.Valor : "",
                            UserTemplates = templates,
                            EnableSsl = ssl != null ? bool.Parse(ssl.Valor) : false,
                        };
                        result.Add(usuarioAddin.Key, smtpSettings);

                    }
                    return result.Count > 0 ? result : null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static Dictionary<int, SmtpSettings> ObtenerSmtpSettings(string nombreAddin, List<int> usuarioIds)
        {
            try
            {
                Dictionary<int, SmtpSettings> result = new Dictionary<int, SmtpSettings>();
                using (var dbContext = new ACHEEntities())
                {
                    var usuariosAddin =
                         dbContext.UsuarioAddins.Include("ParametrosAddin").Where(a => a.Addins.Nombre == nombreAddin && usuarioIds.Contains(a.IDUsuario) && a.Activo && a.FechaDesde <= DateTime.Now &&
                                 (a.FechaHasta == null || a.FechaHasta.Value >= DateTime.Now)).GroupBy(x => x.IDUsuario);

                    foreach (var usuarioAddin in usuariosAddin)
                    {

                        var user = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpUser");
                        var pass = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpPassword");
                        var host = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpHost");
                        var port = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "SmtpPort");
                        var from = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "EmailFrom");
                        var ssl = usuarioAddin.FirstOrDefault().ParametrosAddin.FirstOrDefault(x => x.Clave == "EnableSsl");
                        var templates =
                             usuarioAddin.FirstOrDefault().ParametrosAddin.Where(x => x.Clave.Contains("Template")).ToDictionary(x => x.Clave.Replace("Template", ""), x => x.Valor);

                        var smtpSettings = new SmtpSettings
                        {
                            TieneAddinMails = true,
                            Usuario = user != null ? user.Valor : "",
                            Contrasenia = pass != null ? pass.Valor : "",
                            Host = host != null ? host.Valor : "",
                            Puerto = port != null ? port.Valor : "",
                            EmailFrom = from != null ? from.Valor : "",
                            UserTemplates = templates,
                            EnableSsl = ssl != null ? bool.Parse(ssl.Valor) : false,
                        };
                        result.Add(usuarioAddin.Key, smtpSettings);

                    }
                    return result.Count > 0 ? result : null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

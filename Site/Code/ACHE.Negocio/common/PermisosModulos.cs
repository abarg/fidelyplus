﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Configuration;
using System.Reflection;


/// <summary>
/// Summary description for PermisosModulos
/// </summary>
/// 
public enum PermisosEnum
{
    Exportacion
}
public static class PermisosModulos
{
    ////////////////////////////////////////// PLANES //////////////////////////

    public static bool tienePlan(string formulario)
    {
        var listaFormularios = (List<Formularios>)HttpContext.Current.Session["ASPNETListaFormularios"];
        bool tieneAcceso = false;
        using (var dbContext = new ACHEEntities())
            tieneAcceso = listaFormularios.Any(x => x.Nombre.ToUpper() == formulario.Replace(".aspx", "").ToUpper());
        return tieneAcceso;
    }

    public static int ObtenerTodosLosFormularios(ACHEEntities dbContext, int idUsuario)
    {
        var idPlan = 0;
        var plan = ObtenerPlanActual(dbContext, idUsuario);

        if (plan != null)
            idPlan = plan.IDPlan;
        else
            idPlan = 1;

        var listaFormularios = new List<Formularios>();
        if (HttpContext.Current.Session["ASPNETListaFormularios"] == null)
        {
            listaFormularios = dbContext.Formularios.Where(x => x.Planes.IDPlan <= idPlan).ToList();
            HttpContext.Current.Session["ASPNETListaFormularios"] = listaFormularios;
        }
        else
            listaFormularios = (List<Formularios>)HttpContext.Current.Session["ASPNETListaFormularios"];

        return idPlan;
    }

    public static string ObtenerTipoSegunPLan(ACHEEntities dbContext, string tipoActual, int idPlanActual)
    {
       //Si es personalizado y el plan es < Empresa, entonces se le da acceso total
        if (tipoActual == "C")
        {
            var planEmpresa = dbContext.Planes.FirstOrDefault(p => p.Nombre.ToUpper() == "EMPRESA");
            if (planEmpresa != null)
            {
                var idPlanEmpresa = planEmpresa.IDPlan;
                if (idPlanActual < idPlanEmpresa)
                    return "C";
            }
        }
        return tipoActual;
    }
    
    public static bool PlanVigente(ACHEEntities dbContext, int idUsuario)
    {
        var plan = ObtenerPlanActual(dbContext, idUsuario);
        if (plan != null && plan.FechaFinPlan >= DateTime.Now.Date)
            return true;
        else
            return false;
    }
    
    public static PlanesPagos ObtenerPlanActual(ACHEEntities dbContext, int idUsuario)
    {
        PlanesPagos plan;
        if (HttpContext.Current.Session["ASPNETPlanActual"] == null)
        {
            plan = PermisosModulosCommon.ObtenerPlanActual(dbContext, idUsuario);
            HttpContext.Current.Session["ASPNETPlanActual"] = plan;
        }
        else
        {
            plan = (PlanesPagos)HttpContext.Current.Session["ASPNETPlanActual"];
        }
        return plan;
    }

    //////////////// ROLES///////////////////////

    public static bool tieneAccesoARol(ACHEEntities dbContext, string nombreRol, int idUsuarioAdicional, string tipo)
    {
        
            if (tipo == "A")
                return true;
            if (tipo == "B") //restringido
                return
                    dbContext.RolesUsuariosAdicionales.Any(
                        r => r.IDUsuarioAdicional == idUsuarioAdicional && r.Roles.Nombre == nombreRol);
            if (tipo == "C") //personalizado
            {
                return dbContext.RolesPersonalizadosUsuariosAdicionales.Any(
                    r => r.IDUsuarioAdicional == idUsuarioAdicional && r.Roles.Nombre == nombreRol);
            }
        
        return false;
    }
    
    public static bool tieneAccesoAlModulo(string formulario)
    {
        bool tieneAcceso = false;
        var listaRoles = (List<RolesUsuariosAdicionales>)HttpContext.Current.Session["ASPNETListaRoles"];

        using (var dbContext = new ACHEEntities())
        {
            foreach (var item in listaRoles)
            {
                tieneAcceso = item.Roles.Formularios.Any(x => x.Nombre.ToUpper() == formulario.Replace(".aspx", "").ToUpper());
                if (tieneAcceso)
                    break;
            }
        }
        return tieneAcceso;
    }
    
    public static bool tieneAccesoAlModuloPersonalizado(string formulario)
    {
        bool tieneAcceso = false;
        var listaRolesPersonalizados = (List<RolesPersonalizadosUsuariosAdicionales>)HttpContext.Current.Session["ASPNETListaRolesPersonalizados"];
        return listaRolesPersonalizados.Any(x => x.Formularios.Nombre.ToUpper() == formulario.Replace(".aspx", "").ToUpper());
    }

    public static bool ocultarHeader(int idRol)
    {
        var listaRoles = (List<RolesUsuariosAdicionales>)HttpContext.Current.Session["ASPNETListaRoles"];
        bool ocultarHeader = true;
        if (listaRoles.Any(x => x.IDRol == idRol))
            ocultarHeader = false;
        return ocultarHeader;

    }

    public static bool ocultarHeaderPersonalizado(int idRol)
    {
        var listaRoles =
            (List<RolesPersonalizadosUsuariosAdicionales>)HttpContext.Current.Session["ASPNETListaRolesPersonalizados"];
        bool ocultarHeader = true;
        if (listaRoles.Any(x => x.IDRol == idRol))
            ocultarHeader = false;
        return ocultarHeader;

    }
    
    public static bool mostrarHeaderFormularioPersonalizado(int idRol, string formulario)
    {
        var listaRoles =
           (List<RolesPersonalizadosUsuariosAdicionales>)HttpContext.Current.Session["ASPNETListaRolesPersonalizados"];
        var tienePermiso = listaRoles.Any(x => x.IDRol == idRol && x.Formularios.Nombre.ToUpper() == formulario.ToUpper());
        return tienePermiso;
    }

    public static bool mostrarPersonaSegunPermiso(string tipo)
    {
        bool tieneAcceso = false;
        if (tipo == "c")
        {
            if (!ocultarHeader(2))
                tieneAcceso = true;
        }
        else if (tipo == "p")
        {
            if (!ocultarHeader(1))
                tieneAcceso = true;
        }
        return tieneAcceso;
    }
    
    public static void obtenerRolesUsuarioAdicional(ACHEEntities dbContext, int idUsuAdic, string tipo)
    {
        if (tipo == "B") //acceso restringido
        {
            var listaRoles = new List<RolesUsuariosAdicionales>();
            if (HttpContext.Current.Session["ASPNETListaRoles"] == null)
            {
                listaRoles =
                    dbContext.RolesUsuariosAdicionales.Include("Roles.Formularios")
                        .Where(x => x.IDUsuarioAdicional == idUsuAdic)
                        .ToList();
                HttpContext.Current.Session["ASPNETListaRoles"] = listaRoles;
            }
            else
                listaRoles = (List<RolesUsuariosAdicionales>)HttpContext.Current.Session["ASPNETListaRoles"];
        }
        else if (tipo == "C") //acceso personalizado
        {
            if (HttpContext.Current.Session["ASPNETListaRolesPersonalizados"] == null)
            {
                HttpContext.Current.Session["ASPNETListaRolesPersonalizados"] = dbContext.RolesPersonalizadosUsuariosAdicionales.Include("Formularios")
                        .Where(x => x.IDUsuarioAdicional == idUsuAdic)
                        .ToList();
                var asd= dbContext.RolesPersonalizadosUsuariosAdicionales.Include("Formularios")
        .Where(x => x.IDUsuarioAdicional == idUsuAdic)
        .ToList();

            }
        }

    }

    public static void obtenerPermisosUsuarioAdicional(ACHEEntities dbContext, int idUsuAdic, string tipo)
    {
        if (HttpContext.Current.Session["ASPNETListaPermisos"] == null)
        {
            var usuAd = dbContext.UsuariosAdicionales.Include("Permisos").FirstOrDefault(x => x.IDUsuarioAdicional == idUsuAdic);
            if (usuAd != null)
                HttpContext.Current.Session["ASPNETListaPermisos"] = usuAd.Permisos.ToList();
        }
    }

    public static bool tieneAccesoAPermiso(string permisoNombre, string tipoUsuario)
    {
        if (tipoUsuario == "A" || tipoUsuario == "B") return true;

        var listaPermisos = (List<ACHE.Model.Permisos>)HttpContext.Current.Session["ASPNETListaPermisos"];

        if (listaPermisos == null) return false;

        return listaPermisos.Any(p => p.Nombre.ToUpper() == permisoNombre.ToUpper());
    }
}

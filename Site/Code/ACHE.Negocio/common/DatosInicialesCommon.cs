﻿using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Abono;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Presupuesto;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Ventas;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ACHE.Negocio.Common
{
    public class DatosInicialesCommon
    {
        public static int CrearDatosClientes(WebUser usu)
        {
            return PersonasCommon.GuardarPersonas(0, "Cliente Demo", "", "CF", "F", "", "", "4251108", "correo@correo.com", "correo@correo.com", "C", 10, 2, 5003, "algun lugar", "7", "1414", "alguna observación", 0, "101", 0, usu.IDUsuario, "0");
        }

        public static int CrearDatosProveedores(WebUser usu)
        {
            return PersonasCommon.GuardarPersonas(0, "Proveedor Demo", "Nombre de fantasia del Proveedor", "MO", "F", "DNI", "12345678", "4251108", "correo@correo.com", "correo@correo.com", "P", 10, 2, 5003, "algun lugar", "7", "1414", "alguna observación", 0, "1001", 0, usu.IDUsuario, "0");
        }
        public static int CrearDatosRubros(int idUsuario)
        {
            return RubrosCommon.Guardar(0, "General", "", idUsuario);
        }
        public static void CrearDatosConceptos(int idUsuario, int idRubro)
        {
            //CAMBIAR ACA

            var idConcepto = ConceptosCommon.GuardarConcepto(0, "Producto DEMO", "1", "P", "Alguna descripción", "A", "100", "0,00", "100", "Alguna observación del producto", "10", "10", "", 0,"0", idUsuario,"",idRubro.ToString(),"","","",false);
        }

        public static void CrearDatosPresupuestos(WebUser usu, int idPersona)
        {
            PresupuestoCartDto comprobanteCart = new PresupuestoCartDto();
            comprobanteCart.Items = new List<ComprobantesDetalleViewModel>();
            comprobanteCart.Items.Add(new ComprobantesDetalleViewModel(UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario))
            {
                ID = 1,
                Cantidad = 1,
                Concepto = "Concepto DEMO ",
                PrecioUnitario = 100,
                Bonificacion = 0,
                Iva = 0
            });

            comprobanteCart.IDPresupuesto = 0;
            comprobanteCart.IDPersona = idPersona;
            comprobanteCart.Fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
            comprobanteCart.Nombre = "presupuesto demo";
            comprobanteCart.Numero = 2;
            comprobanteCart.CondicionesPago = "A convenir";
            comprobanteCart.Observaciones = "Alguna observacion";
            comprobanteCart.Estado = "B";

            var idPresupuesto = PresupuestosCommon.GuardarPresupuesto(comprobanteCart, usu.IDUsuario, usu.Email);
        }

        public static Compras CrearDatosCompras(WebUser usu, int idPersona)
        {
            var idCuenta = 0;
            if (usu.UsaPlanCorporativo)
            {
                using (var dbContext = new ACHEEntities())
                    idCuenta = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.Codigo == "1.4.1").FirstOrDefault().IDPlanDeCuenta;
            }

            List<JurisdiccionesViewModel> Jurisdicciones = new List<JurisdiccionesViewModel>();
            return ComprasCommon.Guardar(0, idPersona, DateTime.Now.Date.ToString("dd/MM/yyyy"), "0001-99999999", "0", "0", "0", "0", "0", "0", "0", "100", "0", "0", "0", "0", "0",
                                        "Alguna observación de la compra", "FCC", "", "", "0", DateTime.Now.Date.ToString("dd/MM/yyyy"), idCuenta, usu.IDUsuario, Jurisdicciones,
                                        DateTime.Now.AddDays(15).Date.ToString("dd/MM/yyyy"), DateTime.Now.AddDays(30).Date.ToString("dd/MM/yyyy"), false, new List<DetalleViewModel>(),0,0,usu.Email);

        }

        public static int CrearDatosPagos(WebUser usu, Compras compra, int idCaja)
        {
            //Determinamos la cantidad de decimales que posee configurado el usuario
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            PagosCartDto PagosCart = new PagosCartDto();
            PagosCart.IDPago = 0;
            PagosCart.IDPersona = compra.IDPersona;
            PagosCart.Observaciones = "Alguna observación del pago";
            PagosCart.FechaPago = DateTime.Now.Date.ToString("dd/MM/yyyy");

            PagosCart.Items = new List<PagosDetalleViewModel>();
            PagosCart.FormasDePago = new List<PagosFormasDePagoViewModel>();
            PagosCart.Retenciones = new List<PagosRetencionesViewModel>();

            PagosCart.Items.Add(new PagosDetalleViewModel(cantidadDecimales)
            {
                IDCompra = compra.IDCompra,
                Importe = Convert.ToDecimal(compra.Total),
                nroFactura = compra.NroFactura
            });

            PagosCart.FormasDePago.Add(new PagosFormasDePagoViewModel()
            {
                Importe = Convert.ToDecimal(compra.Total),
                NroReferencia = "",
                FormaDePago = "Efectivo",
                IDCaja = idCaja
            });
            var pago = PagosCommon.Guardar(PagosCart, usu,false);
            return pago.IDPago;
        }

        public static Comprobantes CrearDatosVentas(WebUser usu, int idPersona)
        {
            var idCuenta = 0;
            var idCondVenta = 0;
            if (usu.UsaPlanCorporativo)
            {
                using (var dbContext = new ACHEEntities())
                {
                    idCuenta = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.Codigo == "4.1.1").FirstOrDefault().IDPlanDeCuenta;
                    idCondVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, usu.IDUsuario, "Efectivo").IDCondicionVenta;
                }
            }

            ComprobanteCartDto compCart = new ComprobanteCartDto();
            compCart.Items = new List<ComprobantesDetalleViewModel>();
            compCart.Items.Add(new ComprobantesDetalleViewModel(UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario))
            {
                IDConcepto = null,
                Codigo = "",
                Concepto = "Servicios de venta DEMO",
                Bonificacion = 0,
                Cantidad = 1,
                Iva = (usu.CondicionIVA == "RI") ? 21 : 0,
                PrecioUnitario = 100,
                IDPlanDeCuenta = idCuenta
            });

            compCart.IDComprobante = 0;
            compCart.TipoComprobante = (usu.CondicionIVA == "RI") ? "FCB" : "FCC";
            compCart.Numero = "00000001";
            compCart.Modo = "T";
            compCart.FechaComprobante = DateTime.Now.Date;
            compCart.CondicionVenta = "Efectivo";
          //JC COBRANZA AUTOMATICA
            compCart.IDCondicionVenta = idCondVenta;
            compCart.TipoConcepto = "2";
            compCart.FechaVencimiento = DateTime.Now.AddDays(30).Date;
            //compCart.IDPuntoVenta = UsuarioCommon.ObtenerPuntosDeVenta(usu.IDUsuario).FirstOrDefault().ID;
            compCart.IDPuntoVenta = UsuarioCommon.ObtenerPuntosDeVenta(usu).FirstOrDefault().ID;
            compCart.Observaciones = "Alguna observación de la factura";

            compCart.IDPersona = idPersona;
            compCart.IDUsuario = usu.IDUsuario;
            using (var dbContext = new ACHEEntities())
            {
                return ComprobantesCommon.GuardarComprobante(dbContext, compCart);
            }
        }

        public static int CrearDatosCobranzas(WebUser usu, Comprobantes comprobante, int idCaja)
        {
            //Determinamos cantidad de decimales que posee configurado el usuario
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            CobranzaCartDto cobrCartdto = new CobranzaCartDto();
            cobrCartdto.IDCobranza = 0;
            cobrCartdto.IDPersona = comprobante.IDPersona;
            cobrCartdto.Tipo = "RC";
            cobrCartdto.Fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
            cobrCartdto.IDPuntoVenta = comprobante.IDPuntoVenta;
            cobrCartdto.NumeroCobranza = "00000001";
            cobrCartdto.Observaciones = "Alguna observación de la cobranza";

            cobrCartdto.Items = new List<CobranzasDetalleViewModel>();
            cobrCartdto.Retenciones = new List<CobranzasRetencionesViewModel>();
            cobrCartdto.FormasDePago = new List<CobranzasFormasDePagoViewModel>();

            cobrCartdto.Items.Add(new CobranzasDetalleViewModel(cantidadDecimales)
            {
                Comprobante = comprobante.Tipo + " 0001-" + comprobante.Numero.ToString("#00000000"),
                Importe = comprobante.ImporteTotalNeto,
                IDComprobante = comprobante.IDComprobante,
            });
            cobrCartdto.FormasDePago.Add(new CobranzasFormasDePagoViewModel()
            {
                Importe = comprobante.ImporteTotalNeto,
                FormaDePago = "Efectivo",
                NroReferencia = "",
                IDCaja = idCaja
            });

            var cobranza = CobranzasCommon.Guardar(cobrCartdto, usu,false);
            return cobranza.IDCobranza;
        }

        public static void CrearDatosAbonos(int idUsuario, List<AbonosPersonasViewModel> personas)
        {
            int idCondVenta = 0;
            using (var dbContext = new ACHEEntities())
                {
                    idCondVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, idUsuario, "Efectivo").IDCondicionVenta;
                }
            AbonosCommon.GuardarAbono(0, "Abono por servicios prestados DEMO", "M", DateTime.Now.Date.ToString("dd/MM/yyyy"), DateTime.Now.AddDays(30).Date.ToString("dd/MM/yyyy"),
                                             "A", "100", "0,00", "Alguna observación del abono", personas, 1, idUsuario, 0, false, new List<AbonosDetalleViewModel>(), idCondVenta);
}
    }
}
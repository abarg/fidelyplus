﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ACHE.Model.ViewModels;

namespace ACHE.Model {
    public class CommonModel {
        public static void GenerarArchivo(DataTable dt, string path, string fileName) {
            var wb = new ClosedXML.Excel.XLWorkbook();
            var ws = wb.Worksheets.Add(fileName);
            //ws.Cell("A1").InsertData(dt.AsEnumerable());

            //Establezco el header
            int index = 1;
            foreach (var dc in dt.Columns) {
                ws.Cell(1, index).Value = dt.Columns[index - 1].Caption.SplitCamelCase();
                ws.Cell(1, index).Style.Fill.BackgroundColor = ClosedXML.Excel.XLColor.FromHtml("#4F81BD");// .FromTheme(ClosedXML.Excel.XLThemeColor.Accent1);
                ws.Cell(1, index).Style.Font.FontColor = ClosedXML.Excel.XLColor.White;
                index++;
            }

            //Inserto los resultados
            ws.Cell(2, 1).Value = dt.AsEnumerable();
            ws.RangeUsed().SetAutoFilter();

            //wb.Worksheets.Add(dt, fileName);
            ws.Columns().AdjustToContents();
            if (path.EndsWith("_"))
                path = path.Substring(0, path.Length - 1);
            wb.SaveAs(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx");//HHmmss
        }

        public static void GenerarArchivoPDF(DataTable dataTable, string path, string fileName, ContenidoArchivoViewModel contenidosExtra, bool vertical, bool tieneCabeceras) {
            Document document = new Document();
            if (!vertical)
                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            if (path.EndsWith("_"))
                path = path.Substring(0, path.Length - 1);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(path + "_" + DateTime.Now.ToString("yyyyMMdd") + ".pdf", FileMode.Create));
            document.Open();

            PdfPTable table = new PdfPTable(dataTable.Columns.Count);


            if (contenidosExtra.Widths != null) {
                table.SetWidths(contenidosExtra.Widths);
            }
            #region cabecera
            if (contenidosExtra != null) {
                if (!string.IsNullOrEmpty(contenidosExtra.Logo) && System.IO.File.Exists(HttpContext.Current.Server.MapPath(contenidosExtra.Logo))) {
                    iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(contenidosExtra.Logo), true);
                    logo.Alignment = iTextSharp.text.Image.MIDDLE_ALIGN;
                    logo.ScaleAbsolute(200f, 60f);
                    //logo.Width = 200;
                    //logo.ScalePercent(50f);
                    document.Add(logo);
                }

                if (!string.IsNullOrEmpty(contenidosExtra.TituloReporte)) {
                    Paragraph p = new Paragraph(contenidosExtra.TituloReporte, new Font(null, 16F, Font.BOLD));
                    p.Alignment = 1;//0=Left, 1=Centre, 2=Right
                    document.Add(p);
                }
                document.Add(Chunk.NEWLINE);

                if (contenidosExtra.Cabecera != null && contenidosExtra.Cabecera.Any()) {
                    foreach (var item in contenidosExtra.Cabecera) {
                        Paragraph p = new Paragraph(item.Titulo + ": " + item.Valor, new Font(null, 9F, Font.BOLD));
                        p.Alignment = 0;//0=Left, 1=Centre, 2=Right
                        document.Add(p);

                    }
                }

                document.Add(Chunk.NEWLINE);
            }

            #endregion

            #region detalle

            table.WidthPercentage = 100;

            //Set columns names in the pdf file
            for (int k = 0; k < dataTable.Columns.Count; k++) {
                dataTable.Columns[k].ColumnName = ACHE.Extensions.StringExtensions.SplitCamelCase(dataTable.Columns[k].ColumnName);
                PdfPCell cell = new PdfPCell(new Phrase(dataTable.Columns[k].ColumnName, new Font(null, 7F, Font.BOLD)));

                cell.BackgroundColor = new BaseColor(204, 204, 204);

                cell.HorizontalAlignment = 1;
                table.AddCell(cell);
            }

            //Add values of DataTable in pdf file
            for (int i = 0; i < dataTable.Rows.Count; i++) {
                for (int j = 0; j < dataTable.Columns.Count; j++) {
                    bool esDecimal = false;
                    decimal number;
                    if (Decimal.TryParse(dataTable.Rows[i][j].ToString(), out number))
                        esDecimal = true;

                    PdfPCell cell = new PdfPCell(new Phrase(dataTable.Rows[i][j].ToString(), new Font(null, 7F, Font.NORMAL)));

                    if (tieneCabeceras) { 
                    if (dataTable.Rows[i][0] != null && dataTable.Rows[i][0].ToString() != "")
                        cell.BackgroundColor = new BaseColor(204, 204, 204);
                    }


                    if (!esDecimal) {
                        cell.HorizontalAlignment = 0;
                    }
                    else {
                        cell.HorizontalAlignment = 2;

                    }
                    table.AddCell(cell);

                }
            }

            if (contenidosExtra != null && contenidosExtra.UltimaFila != null) {
                foreach (var item in contenidosExtra.UltimaFila) {
                    bool esDecimal = false;
                    decimal number;
                    if (Decimal.TryParse(item, out number))
                        esDecimal = true;

                    PdfPCell cellfinal = new PdfPCell(new Phrase(item, new Font(null, 7F, Font.BOLD)));
                    cellfinal.BackgroundColor = new BaseColor(204, 204, 204);

                    if (!esDecimal) {
                        cellfinal.HorizontalAlignment = 0;
                    }
                    else {
                        cellfinal.HorizontalAlignment = 2;
                    }

                    table.AddCell(cellfinal);


                }

            }

            document.Add(table);

            #endregion

            #region total
            if (contenidosExtra != null && contenidosExtra.Totales != null && contenidosExtra.Totales.Any()) {
                foreach (var item in contenidosExtra.Totales) {
                    Paragraph p = new Paragraph(item.Nombre + " $ " + item.Importe, new Font(null, 11F, Font.BOLD));
                    p.Alignment = 0;//0=Left, 1=Centre, 2=Right
                    document.Add(p);

                }
            }
            #endregion

            document.Close();
        }

        private void AddPageNumber(string fileIn, string fileOut) {
            byte[] bytes = File.ReadAllBytes(fileIn);
            Font blackFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, BaseColor.BLACK);
            using (MemoryStream stream = new MemoryStream()) {
                PdfReader reader = new PdfReader(bytes);
                using (PdfStamper stamper = new PdfStamper(reader, stream)) {
                    int pages = reader.NumberOfPages;
                    for (int i = 1; i <= pages; i++) {
                        ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(i.ToString(), blackFont), 568f, 15f, 0);
                    }
                }
                bytes = stream.ToArray();
            }
            File.WriteAllBytes(fileOut, bytes);
        }

        public static List<Combo2ViewModel> ObtenerPaises() {
            try {
                using (var dbContext = new ACHEEntities()) {
                    return dbContext.Paises.ToList()
                           .Select(x => new Combo2ViewModel() {
                               ID = x.IDPais,
                               Nombre = x.Nombre
                           }).OrderBy(x => x.Nombre).ToList();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static List<Combo2ViewModel> ObtenerProvincias(int idPais) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    return dbContext.Provincias.Where(x => x.IDPais == idPais).ToList()
                           .Select(x => new Combo2ViewModel() {
                               ID = x.IDProvincia,
                               Nombre = x.Nombre
                           }).ToList();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static List<Combo2ViewModel> ObtenerCuidades(int idProvincia) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    return dbContext.Ciudades.Where(x => x.IDProvincia == idProvincia).OrderBy(x => x.Nombre)
                        .Select(x => new Combo2ViewModel() {
                            ID = x.IDCiudad,
                            Nombre = x.Nombre
                        }).ToList();
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static ResultadosCombo2ViewModel ObtenerCuidadesPaginadas(int page, int pageSize) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    ResultadosCombo2ViewModel resultado = new ResultadosCombo2ViewModel();
                    page--;
                    var list = dbContext.Ciudades.OrderBy(x => x.Nombre).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new Combo2ViewModel() {
                            ID = x.IDCiudad,
                            Nombre = x.Nombre
                        }).ToList();

                    resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = list.Count();
                    resultado.Items = list.ToList();
                    return resultado;
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }
    }
}
﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ACHE.Extensions;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Web;
using System.Configuration;
using ACHE.Model.Negocio;
using System.Net.Mail;

namespace ACHE.Negocio.Common
{
    public static class LogCommon
    {
        public static int IniciarProceso(int idUsuario, int idIntegracion, string tipoProceso)
        {
            using (var dbContext = new ACHEEntities())
            {

                if (idIntegracion > 0)
                {
                    if (dbContext.LogProcesosMasivos.Any(x => x.IDUsuario == idUsuario && x.Proceso == tipoProceso && x.IDIntegracion == idIntegracion && !x.Fin.HasValue))
                        throw new CustomException("Hay otra acción similar a la que está intentando realizar para esta cuenta, que aún se encuentra pendiente. Por favor, aguarde a que esta finalice.");
                }
                else
                {
                    if (dbContext.LogProcesosMasivos.Any(x => x.IDUsuario == idUsuario && x.Proceso == tipoProceso && !x.Fin.HasValue))
                        throw new CustomException("Hay otra acción similar a la que está intentando realizar, que aún se encuentra pendiente. Por favor, aguarde a que esta finalice.");
                }

                var entity = new LogProcesosMasivos();
                entity.IDUsuario = idUsuario;

                if (idIntegracion > 0)
                    entity.IDIntegracion = idIntegracion;

                entity.Proceso = tipoProceso;
                entity.Inicio = DateTime.Now;
                dbContext.LogProcesosMasivos.Add(entity);
                dbContext.SaveChanges();
                return entity.IDProceso;
            }
        }

        public static void FinalizarProceso(int idUsuario, int idProceso, bool tieneError)
        {//todas las llamadas a este metodo, envian: tieneError = false; 22/06/18
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.LogProcesosMasivos.Include("Usuarios").Where(x => x.IDUsuario == idUsuario && x.IDProceso == idProceso).FirstOrDefault();
                if (entity != null)
                {
                    entity.Fin = DateTime.Now;
                    dbContext.SaveChanges();

                    string asunto = "";
                    string mensaje = "";


                    if (!tieneError)
                    {
                        asunto = "El proceso ha finalizado";
                        mensaje = "El proceso de ";
                        mensaje += tipoProceso(entity.Proceso);
                        mensaje += " acaba de finalizar. Los datos ya se encuentran actualizados.";
                    }
                    else
                    {
                        asunto = "El proceso ha fnalizado con errores";
                        mensaje = "Hubo un error al procesar la solicitud de ";
                        mensaje += tipoProceso(entity.Proceso);
                        mensaje += ". Por favor, reintente nuevamente y en caso de persistir comuniquese con soporte.";
                    }

                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<NOTIFICACION>", mensaje);
                    replacements.Add("<USUARIO>", entity.Usuarios.RazonSocial);

                    var emailUsuario = entity.Usuarios.Email;
                    if (entity.Usuarios.EmailAlertas != string.Empty)
                        emailUsuario = entity.Usuarios.EmailAlertas;
                    
                    MailAddressCollection listTo = new MailAddressCollection();
                    listTo.Add(emailUsuario);

                    EmailCommon.SaveMessage(idUsuario, EmailTemplate.NotificacionDeContabilium, replacements, listTo, "alertas@contabilium.com", "", asunto, null, "Contabiliium");//ENVIO MAIL A USUARIO CON LOS PAGOS GENERADOS Y SI SE GENERARON BIEN

                    //bool send = EmailCommon.SendMessage(entity.Usuarios.IDUsuario, EmailTemplate.NotificacionDeContabilium, replacements, ConfigurationManager.AppSettings["Email.Alertas"], entity.Usuarios.Email, asunto);

                }
            }
        }

        private static string tipoProceso(string tipo)
        {
            string mensaje = "";
            switch (tipo)
            {
                case "ActualizarMasivamentePublicaciones":
                    mensaje = "actualización masiva de publicaciones";
                    break;
                case "ImportarPublicaciones":
                    mensaje = "importación masiva de publicaciones";
                    break;
                case "ActualizarMasivamentePrecios":
                    mensaje = "actualización masiva de precios";
                    break;
                case "ActualizarMasivamenteStock":
                    mensaje = "actualización masiva de stock";
                    break;
            }

            return mensaje;

        }

    }

}

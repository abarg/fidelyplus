﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ACHE.Extensions;
using System.Text.RegularExpressions;
using FileHelpers;
using System.Data;
using System.Data.SqlClient;
using ACHE.Negocio.Contabilidad;
using ACHE.Model.Negocio;
using ACHE.Negocio.Facturacion;
using System.Configuration;
using ACHE.Negocio.Inventario;
using ACHE.Negocio.Helpers;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Abono;
using ACHE.Negocio.Common;
using Hangfire;
using ACHE.Negocio.Ventas;


/// <summary>
/// Summary description for ImportacionMasiva
/// </summary>
public static class ImportacionMasiva
{
    #region PRODUCTOS

    public static List<ProductosCSVTmp> LeerArchivoCSVProductos(string tipo, int idUsuario, string path)
    {
        List<ProductosCSVTmp> listaproductosCSV = new List<ProductosCSVTmp>();
        var engine = new DelimitedFileEngine<ProductosCSV>();
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //engine.Options.Delimiter=";";
        ProductosCSV[] res = (ProductosCSV[])engine.ReadFile(path);

        if (res.Count() < 1)
        {
            engine.Options.Delimiter = ",";
            ProductosCSV[] resaux = (ProductosCSV[])engine.ReadFile(path);
            if (resaux.Count() > 0)
                res = resaux;
        }

        ProductosCSVTmp prodCSV;

        List<Inventarios> inventarios = new List<Inventarios>();
        List<Rubros> rubros = new List<Rubros>();
        List<Conceptos> conceptos = new List<Conceptos>();
        using (var dbContext = new ACHEEntities())
        {
            inventarios = dbContext.Inventarios.Where(x => x.IDUsuario == idUsuario).ToList();
            rubros = dbContext.Rubros.Where(x => x.IDUsuario == idUsuario).ToList();
            conceptos = dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario && x.Codigo != "").ToList();
        }

        foreach (var productos in res)
        {
            prodCSV = new ProductosCSVTmp();
            prodCSV.IDUsuario = idUsuario;
            prodCSV.nombre = productos.nombre.ToUpper().Trim();
            prodCSV.descripcion = productos.descripcion.Trim();
            prodCSV.codigo = productos.codigo.ToUpper().Trim();
            prodCSV.observaciones = productos.observaciones.Trim();
            prodCSV.precioUnitario = productos.precioUnitario.Replace(".", ",");
            prodCSV.stock = productos.stock;
            prodCSV.rentabilidad = productos.rentabilidad.Replace(".", ",");
            prodCSV.iva = productos.iva.Replace(".", ",");
            prodCSV.tipo = (tipo == "Productos") ? "P" : "S";
            prodCSV.fechaAlta = DateTime.Now;
            prodCSV.CostoInterno = productos.CostoInterno;
            prodCSV.StockMinimo = (prodCSV.tipo == "P") ? productos.StockMinimo : "0";
            prodCSV.CodigoProveedor = productos.CodigoProveedor;
            prodCSV.Deposito = productos.Deposito.ToUpper().Trim();
            prodCSV.CodigoBarra = productos.CodigoBarra;
            prodCSV.Rubro = productos.Rubro.ToUpper().Trim();
            prodCSV.SubRubro = productos.SubRubro.ToUpper().Trim();

            prodCSV.resultados = ImportacionMasiva.ValidarProducto(prodCSV, inventarios, rubros, conceptos);

            if (listaproductosCSV.Any(x => x.codigo == productos.codigo))
                prodCSV.resultados = "El Código se encuentra repetido en la lista, ";

            //if (productos.StockMinimo != "")
            //{
            //    int valueStockMin;
            //    if (!int.TryParse(productos.StockMinimo, out valueStockMin))
            //    {
            //        prodCSV.resultados += "El Stock mínimo debe ser un valor entero, ";
            //    }
            //}
            //if (productos.stock != "")
            //{
            //    int valueStock;
            //    if (!int.TryParse(productos.stock, out valueStock))
            //    {
            //        prodCSV.resultados += "El Stock debe ser un valor entero, ";
            //    }
            //}

            prodCSV.Estado = prodCSV.resultados == string.Empty ? "A" : "I";

            if (prodCSV.resultados == string.Empty)
                prodCSV.resultados = "<span class='label label-success'>OK</span>";
            else
                prodCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + prodCSV.resultados.Substring(0, prodCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

            listaproductosCSV.Add(prodCSV);
        }

        return listaproductosCSV.ToList();
    }

    public static string ValidarProducto(ProductosCSVTmp productos, List<Inventarios> inventarios, List<Rubros> rubros, List<Conceptos> conceptos)
    {
        string resultado = string.Empty;

        if (string.IsNullOrEmpty(productos.nombre))
            resultado += "El campo Nombre es obligatorio , ";

        //if (string.IsNullOrEmpty(productos.descripcion))
        //    resultado += "El campo Descripción es obligatorio, ";

        if (string.IsNullOrEmpty(productos.codigo))
            resultado += "El campo Código es obligatorio, ";

        if (string.IsNullOrEmpty(productos.iva))
            resultado += "El campo Iva es obligatorio, ";

        if (productos.nombre.Length > 500)
            resultado += "El campo Nombre supera los 500 carácteres, ";

        if (productos.descripcion.Length > 500)
            resultado += "El campo Descripción supera los 500 carácteres, ";

        if (productos.codigo.Length > 50)
            resultado += "El campo Código supera los 50 carácteres, ";

        //if (string.IsNullOrEmpty(productos.stock))
        //    resultado += "El campo Stock es obligatorio, ";

        if (string.IsNullOrEmpty(productos.precioUnitario))
            resultado += "El campo Precio Unitario es obligatorio, ";

        if (string.IsNullOrEmpty(productos.Rubro))
            resultado += "El campo rubro es obligatorio, ";

        decimal Stock = 0;
        if (string.IsNullOrEmpty(productos.stock))
            productos.stock = "0";

        decimal.TryParse(productos.stock, out Stock);
        if (Stock == 0 && productos.stock != "0")
            resultado += "El campo Stock contiene carácteres inválidos, ";

        int StockMinimo = 0;
        int.TryParse(productos.StockMinimo, out StockMinimo);
        if (Stock == 0 && productos.stock != "0" && productos.stock != "")
            resultado += "El campo StockMinimo contiene carácteres inválidos, ";

        double PrecioUnitario = 0;
        double.TryParse(productos.precioUnitario, out PrecioUnitario);

        if (PrecioUnitario == 0 && productos.precioUnitario != "0")
            resultado += "El campo Precio Unitario contiene carácteres inválidos (NO utilice punto para separador de miles), ";

        double iva = 0;
        double.TryParse(productos.iva, out iva);

        if (iva != 0 && iva != 2.5 && iva != 5 && iva != 10.5 && iva != 21 && iva != 27)
            resultado += "El campo Iva sólo puede contener los siguientes valores '0','2,5','10,5','21','27', ";

        using (var dbContext = new ACHEEntities())
        {
            var results = conceptos.Where(x => x.Codigo.ToUpper() == productos.codigo.ToUpper()).FirstOrDefault();
            if (results != null)
            {
                productos.Existe = true;
                productos.IDConcepto = results.IDConcepto;
            }
            else
                productos.Existe = false;
            //resultado += (results) ? "El Código ingresado ya existe, " : "";


            if (productos.CodigoProveedor != "0" && productos.CodigoProveedor != "")
            {
                var persona = dbContext.Personas.Where(x => x.IDUsuario == productos.IDUsuario && x.Codigo == productos.CodigoProveedor).FirstOrDefault();

                if (persona != null)
                    productos.IDPersona = persona.IDPersona;
                else
                    resultado += "No se encontro ningun proveedor con el codigo asociado. Puede dejar en blanco este campo si lo desea, ";
            }
            if (productos.Deposito != "")
            {
                var inventario = inventarios.Where(x => x.Nombre.ToUpper() == productos.Deposito).FirstOrDefault();

                if (inventario != null)
                    productos.IDInventario = inventario.IDInventario;
                else
                    resultado += "No se encontro ningun déposito con el nombre asociado. Puede dejar en blanco este campo si lo desea, ";
            }

            if (productos.Rubro != "")
            {
                var rubro = rubros.Where(x => x.Nombre.ToUpper().Trim() == productos.Rubro).FirstOrDefault();

                if (rubro != null)
                    productos.IDRubro = rubro.IDRubro;
                else
                    resultado += "No se encontro ningun rubro con el nombre asociado, ";
            }
            if (productos.SubRubro != "")
            {
                var subrubro = rubros.Where(x => x.Nombre.ToUpper().Trim() == productos.SubRubro).FirstOrDefault();

                if (subrubro != null)
                    productos.IDSubRubro = subrubro.IDRubro;
                else
                    resultado += "No se encontro ningun subrubro con el nombre asociado. Puede dejar en blanco este campo si lo desea, ";
            }
            //VER SI FALTA VALIDACION DE QUE EL SUBRUBRO SEA DE ESE PADRE
        }
        return resultado;
    }

    public static void RealizarImportacionProductos(List<ProductosCSVTmp> lista, string ACHEString)
    {
        if (lista != null && lista.Any(x => x.Estado == "A"))
        {

            //IMPORTAR
            DataTable dt = new DataTable();
            dt.Columns.Add("IDUsuario", typeof(Int32));//0
            dt.Columns.Add("Nombre", typeof(string));//1
            dt.Columns.Add("Codigo", typeof(string));//2
            dt.Columns.Add("Descripcion", typeof(string));//3
            dt.Columns.Add("Stock", typeof(Int32));//4
            dt.Columns.Add("PrecioUnitario", typeof(decimal));//5
            dt.Columns.Add("FechaAlta", typeof(DateTime));//6
            dt.Columns.Add("Estado", typeof(string));//7
            dt.Columns.Add("Observaciones", typeof(string));//8
            dt.Columns.Add("Rentabilidad", typeof(decimal));//9
            dt.Columns.Add("Iva", typeof(decimal));//10
            dt.Columns.Add("Tipo", typeof(string));//11
            dt.Columns.Add("CostoInterno", typeof(decimal));//12
            dt.Columns.Add("StockMinimo", typeof(Int32));//13
            dt.Columns.Add("IDPersona", typeof(Int32));//14
            dt.Columns.Add("IDInventario", typeof(Int32));//15
            dt.Columns.Add("Existe", typeof(bool));//16
            dt.Columns.Add("CodigoBarra", typeof(string));//17
            dt.Columns.Add("IDRubro", typeof(Int32));//18
            dt.Columns.Add("IDSubRubro", typeof(Int32));//19
            dt.Columns.Add("IDConcepto", typeof(Int32));//20
            dt.Columns.Add("ActualiceStock", typeof(bool));//21

            int idUsuario = 0;
            foreach (var prod in lista.Where(x => x.Estado == "A").ToList())
            {
                #region Armo DataRow
                try
                {
                    DataRow drNew = dt.NewRow();
                    drNew[0] = prod.IDUsuario;
                    idUsuario = prod.IDUsuario;
                    drNew[1] = prod.nombre;
                    drNew[2] = prod.codigo;
                    drNew[3] = prod.descripcion;
                    drNew[4] = prod.stock;
                    drNew[5] = prod.precioUnitario;
                    drNew[6] = prod.fechaAlta;
                    drNew[7] = prod.Estado;
                    drNew[8] = prod.observaciones;
                    if (prod.rentabilidad != "")
                        drNew[9] = prod.rentabilidad;
                    else
                        drNew[9] = 0;
                    drNew[10] = prod.iva;
                    drNew[11] = prod.tipo;
                    if (prod.CostoInterno != "")
                        drNew[12] = prod.CostoInterno;
                    else
                        drNew[12] = 0;
                    if (prod.StockMinimo != "")
                        drNew[13] = prod.StockMinimo;
                    else
                        drNew[13] = 0;
                    drNew[14] = prod.IDPersona;
                    drNew[15] = prod.IDInventario;
                    drNew[16] = prod.Existe;
                    drNew[17] = prod.CodigoBarra;

                    drNew[18] = prod.IDRubro;
                    drNew[19] = prod.IDSubRubro;

                    drNew[20] = prod.IDConcepto;
                    drNew[21] = false;

                    dt.Rows.Add(drNew);

                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
                #endregion
            }

            if (dt.Rows.Count > 0)
            {
                EliminarConceptosTmp(idUsuario);

                using (var sqlConnection = new SqlConnection(ACHEString))
                {
                    sqlConnection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = "TempConceptos";
                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
                ProcesarConceptosTmp(idUsuario);

                /*
                                var listaProd = lista.Where(x => x.Estado == "A").ToList();
                                //TODO: Mejorar esto
                                using (var dbContext = new ACHEEntities())
                                {
                                    int idInventario = dbContext.Inventarios.Where(x => x.IDUsuario == idUsuario).First().IDInventario;

                                    foreach (var item in listaProd)
                                    {
                                        if (item.stock != "")
                                        {
                                            var concepto = dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario && x.Codigo == item.codigo).FirstOrDefault();
                                            if (concepto != null)
                                        }
                                    }

                                    dbContext.SaveChanges();
                                }*/
            }
        }
        else
            throw new Exception("no se encontraron datos para importar");
    }

    private static void ProcesarConceptosTmp(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 120;
                dbContext.ProcesarConceptosImportacionMasiva(idUsuario);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar los conceptos. Error: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
        }
    }

    private static void EliminarConceptosTmp(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 120;
                dbContext.deleteTempConceptos(idUsuario);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar los datos de la tabla temporal");
        }
    }


    #endregion

    #region PERSONAS

    public static List<PersonasCSVTmp> LeerArchivoCSVPersonas(string tipo, int idUsuario, string path)
    {
        List<PersonasCSVTmp> listaPersonasCSV = new List<PersonasCSVTmp>();
        //FileHelperEngine engine = new FileHelperEngine(typeof(PersonasCSV));
        //engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //PersonasCSV[] res = (PersonasCSV[])engine.ReadFile(path);

        var engine = new DelimitedFileEngine<PersonasCSV>();
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //engine.Options.Delimiter=";";
        PersonasCSV[] res = (PersonasCSV[])engine.ReadFile(path);

        if (res.Count() < 1)
        {
            engine.Options.Delimiter = ",";
            PersonasCSV[] resaux = (PersonasCSV[])engine.ReadFile(path);
            if (resaux.Count() > 0)
                res = resaux;
        }

        PersonasCSVTmp personaCSV;
        foreach (var Personas in res)
        {
            try
            {
                personaCSV = new PersonasCSVTmp();
                personaCSV.IDUsuario = idUsuario;
                personaCSV.Codigo = Personas.Codigo;
                personaCSV.Tipo = (tipo == "Clientes") ? "C" : "P";
                personaCSV.RazonSocial = Personas.RazonSocial.Trim();
                personaCSV.TipoDocumento = Personas.TipoDocumento.ToUpper();
                personaCSV.NroDocumento = Personas.NroDocumento.Replace("-", "").ReplaceAll(" ", "").Trim();
                personaCSV.CondicionIva = Personas.CondicionIva.ToUpper();

                personaCSV.Telefono = Personas.Telefono.Trim();
                personaCSV.Celular = Personas.Celular.Trim();
                personaCSV.Web = Personas.Web.Trim();
                personaCSV.Email = Personas.Email.Trim();

                if (Personas.Provincia.ToUpper().Contains("CIUDAD") || Personas.Provincia.ToUpper().Contains("CABA")
                    || Personas.Provincia.ToUpper().Contains("C.A.B.A") || Personas.Provincia.ToUpper().Contains("FEDERAL"))
                    personaCSV.Provincia = "Ciudad de Buenos Aires";
                else
                    personaCSV.Provincia = Personas.Provincia;

                //personaCSV.Provincia = Personas.Provincia.Trim();
                personaCSV.Ciudad = Personas.Ciudad.Trim();
                personaCSV.Domicilio = Personas.Domicilio.Trim();
                personaCSV.PisoDepto = Personas.PisoDepto.Trim();
                personaCSV.CodigoPostal = Personas.CodigoPostal.Trim();

                personaCSV.EmailsEnvioFc = Personas.EmailsEnvioFc;
                personaCSV.Personeria = Personas.Personeria.ToUpper();
                personaCSV.AlicuotaIvaDefecto = Personas.AlicuotaIvaDefecto;
                personaCSV.TipoComprobanteDefecto = Personas.TipoComprobanteDefecto;
                personaCSV.CBU = Personas.CBU;

                personaCSV.Banco = Personas.Banco.Trim();
                personaCSV.NombreFantansia = Personas.NombreFantansia.Trim();
                personaCSV.fechaAlta = DateTime.Now;
                personaCSV.Observaciones = Personas.Observaciones.Trim();
                personaCSV.Contacto = Personas.Contacto.Trim();
                personaCSV.Vendedor = (tipo == "Clientes") ? Personas.Vendedor.Trim().ToUpper() : "";

                personaCSV.resultados = ImportacionMasiva.ValidarPersona(personaCSV);

                if (listaPersonasCSV.Any(x => Personas.NroDocumento != "" && x.NroDocumento == Personas.NroDocumento))
                    personaCSV.resultados += "El Nro de Documento se encuentra repetido en la lista, ";


                if (listaPersonasCSV.Any(x => x.Codigo == Personas.Codigo))
                    personaCSV.resultados += "El Código se encuentra repetido en la lista, ";


                personaCSV.Estado = personaCSV.resultados == string.Empty ? "A" : "I";

                if (personaCSV.resultados == string.Empty)
                    personaCSV.resultados = "<span class='label label-success'>OK</span>";
                else
                    personaCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + personaCSV.resultados.Substring(0, personaCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

                listaPersonasCSV.Add(personaCSV);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }

        return listaPersonasCSV.ToList();
    }

    public static string ValidarPersona(PersonasCSVTmp Personas)
    {
        string resultado = string.Empty;

        /*Obligatorios*/
        resultado += (string.IsNullOrEmpty(Personas.RazonSocial)) ? "El campo Razón Social es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(Personas.TipoDocumento)) ? "El campo Tipo Documento es obligatorio , " : "";
        if (Personas.CondicionIva != "CF")
            resultado += (string.IsNullOrEmpty(Personas.NroDocumento)) ? "El campo Nro Documento es obligatorio , " : "";

        resultado += (string.IsNullOrEmpty(Personas.CondicionIva)) ? "El campo Condición Iva es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(Personas.Provincia)) ? "El campo Provincia es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(Personas.Personeria)) ? "El campo Personería es obligatorio , " : "";


        /*longitud  obligatoria*/
        resultado += (Personas.RazonSocial.Length > 128) ? "El campo Razón Social supera los 128 carácteres , " : "";
        resultado += (Personas.TipoDocumento.Length > 15) ? "El campo Tipo Documento supera los 15 carácteres , " : "";
        resultado += (Personas.NroDocumento.Length > 20) ? "El campo Nro Documento supera los 20 carácteres , " : "";

        resultado += (Personas.CondicionIva.Length > 50) ? "El campo Condición Iva supera los 50 carácteres , " : "";
        resultado += (Personas.Provincia.Length > 50) ? "El campo Provincia supera los 50 carácteres , " : "";
        resultado += (Personas.Ciudad.Length > 100) ? "El campo Ciudad supera los 100 carácteres , " : "";

        resultado += (Personas.Personeria.Length > 50) ? "El campo Personería supera los 50 carácteres , " : "";
        resultado += (Personas.AlicuotaIvaDefecto.Length > 10) ? "El campo Alicuota Iva Defecto supera los 10 carácteres , " : "";
        resultado += (Personas.TipoComprobanteDefecto.Length > 10) ? "El campo Tipo Comprobante Defecto supera los 10 carácteres , " : "";

        /*longitud no obligatoria*/
        resultado += (Personas.Telefono.Length > 50) ? "El campo Teléfono supera los 50 carácteres , " : "";
        resultado += (Personas.Celular.Length > 50) ? "El campo Celular supera los 50 carácteres , " : "";
        resultado += (Personas.Web.Length > 255) ? "El campo Web supera los 255 carácteres , " : "";
        resultado += (Personas.Email.Length > 128) ? "El campo Email supera los 128 carácteres , " : "";

        resultado += (Personas.Domicilio.Length > 100) ? "El campo Domicilio supera los 100 carácteres , " : "";
        resultado += (Personas.PisoDepto.Length > 10) ? "El campo Piso Depto supera los 10 carácteres , " : "";
        resultado += (Personas.CodigoPostal.Length > 10) ? "El campo Código Postal supera los 10 carácteres , " : "";

        resultado += (Personas.CBU.Length > 50) ? "El campo CBU supera los 50 carácteres , " : "";
        resultado += (Personas.Banco.Length > 50) ? "El campo Banco supera los 50 carácteres , " : "";
        resultado += (Personas.NombreFantansia.Length > 128) ? "El campo Nombre Fantasía supera los 128 carácteres , " : "";
        resultado += (Personas.Contacto.Length > 150) ? "El campo Contacto supera los 150 carácteres , " : "";
        resultado += (Personas.Codigo.Length > 50) ? "El campo Código supera 50 carácteres , " : "";

        /*solo determinados valores*/
        resultado += (Personas.CondicionIva.ToUpper() != "RI" && Personas.CondicionIva.ToUpper() != "CF" && Personas.CondicionIva.ToUpper() != "MO" && Personas.CondicionIva.ToUpper() != "EX") ? "El campo Condición Iva sólo puede contener los siguientes valores: RI,CF,MO,EX, " : "";
        resultado += (Personas.Personeria.ToUpper() != "F" && Personas.Personeria.ToUpper() != "J") ? "El campo Personería sólo puede contener los siguientes valores F,J, " : "";
        resultado += (Personas.TipoDocumento.ToUpper().Trim() != "DNI" && Personas.TipoDocumento.ToUpper().Trim() != "CUIT") ? "El campo Tipo Documento sólo puede contener los siguientes valores: DNI, CUIT, " : "";

        /*otras validaciones*/
        resultado += (!Personas.NroDocumento.IsValidCUIT() && Personas.TipoDocumento.Trim() == "CUIT") ? "El CUIT ingresado es inválido , " : "";

        if (Personas.Email.Trim() != "")
            resultado += (!Personas.Email.IsValidEmailAddress()) ? "El Email ingresado es inválido , " : "";
        if (Personas.EmailsEnvioFc.Trim() != "")
            resultado += (!Personas.EmailsEnvioFc.IsValidMultiEmailAddress()) ? "El Email de Envio FC ingresado es inválido , " : "";

        using (var dbContext = new ACHEEntities())
        {
            if (Personas.TipoDocumento.ToUpper().Trim() != "DNI" || (Personas.CondicionIva.ToUpper().Trim() == "CF" && Personas.NroDocumento.Trim() != ""))
            {
                var documento = dbContext.Personas.Where(x => x.NroDocumento.ToUpper() == Personas.NroDocumento.ToUpper() && x.IDUsuario == Personas.IDUsuario).Any();
                resultado += (documento) ? "El Nro Documento ingresado ya existe, " : "";
            }

            if (Personas.Vendedor != "")
            {
                var vendedor = dbContext.UsuariosAdicionales.Where(x => x.IDUsuario == Personas.IDUsuario && x.Email.ToUpper() == Personas.Vendedor).FirstOrDefault();
                resultado += vendedor != null ? "" : "El usuario vendedor no existe, ";
            }

            if (Personas.Codigo.Trim() != "")
            {
                var codigo = dbContext.Personas.Where(x => x.Codigo.ToUpper() == Personas.Codigo.Trim().ToUpper() && x.Codigo != "" && x.IDUsuario == Personas.IDUsuario).Any();
                resultado += (codigo) ? "El Código ingresado ya existe, " : "";
            }

            var Provincia = dbContext.Provincias.Any(x => x.Nombre.ToUpper() == Personas.Provincia.ToUpper());
            resultado += (!Provincia) ? "La Provincia ingresada no es correcta, " : "";
        }
        return resultado;
    }

    public static void RealizarImportacionPersonas(List<PersonasCSVTmp> lista, string ACHEString)
    {
        if (lista != null && lista.Any(x => x.Estado == "A"))
        {
            //IMPORTAR
            DataTable dt = new DataTable();

            dt.Columns.Add("Tipo", typeof(string));//0

            dt.Columns.Add("IDUsuario", typeof(Int32));//1
            dt.Columns.Add("RazonSocial", typeof(string));//2
            dt.Columns.Add("TipoDocumento", typeof(string));//3
            dt.Columns.Add("NroDocumento", typeof(string));//4
            dt.Columns.Add("CondicionIva", typeof(string));//5

            dt.Columns.Add("Telefono", typeof(string));//6
            dt.Columns.Add("Celular", typeof(string));//7
            dt.Columns.Add("Web", typeof(string));//8
            dt.Columns.Add("Email", typeof(string));//9
            dt.Columns.Add("Observaciones", typeof(string));//10
            dt.Columns.Add("IDProvincia", typeof(string));//11

            dt.Columns.Add("IDCiudad", typeof(string));//12
            dt.Columns.Add("Domicilio", typeof(string));//13
            dt.Columns.Add("PisoDepto", typeof(string));//14
            dt.Columns.Add("CodigoPostal", typeof(string));//15
            dt.Columns.Add("EmailsEnvioFc", typeof(string));//16

            dt.Columns.Add("Personeria", typeof(string));//17
            dt.Columns.Add("AlicuotaIvaDefecto", typeof(string));//18
            dt.Columns.Add("TipoComprobanteDefecto", typeof(string));//19
            dt.Columns.Add("FechaAlta", typeof(DateTime));//20
            dt.Columns.Add("CBU", typeof(string));//21

            dt.Columns.Add("Banco", typeof(string));//22
            dt.Columns.Add("NombreFantansia", typeof(string));//23
            dt.Columns.Add("Contacto", typeof(string));//24
            dt.Columns.Add("Codigo", typeof(string));//25
            dt.Columns.Add("SaldoInicial", typeof(string));//26
            dt.Columns.Add("IDPais", typeof(string));//27
            dt.Columns.Add("IDUsuarioAdicional", typeof(string));//28


            foreach (var personas in lista.Where(x => x.Estado == "A").ToList())
            {
                #region Armo DataRow
                try
                {
                    ObtenerIDProvinciayCiudad(personas);
                    ObtenerIDUsuarioAdicional(personas, personas.IDUsuario);
                    DataRow drNew = dt.NewRow();

                    drNew[0] = personas.Tipo;

                    drNew[1] = personas.IDUsuario;
                    drNew[2] = personas.RazonSocial;
                    drNew[3] = personas.TipoDocumento;
                    drNew[4] = personas.NroDocumento;
                    drNew[5] = personas.CondicionIva;

                    drNew[6] = personas.Telefono;
                    drNew[7] = personas.Celular;
                    drNew[8] = personas.Web;
                    drNew[9] = personas.Email;
                    drNew[10] = personas.Observaciones;
                    drNew[11] = personas.Provincia;

                    drNew[12] = personas.Ciudad;
                    drNew[13] = personas.Domicilio;
                    drNew[14] = personas.PisoDepto;
                    drNew[15] = personas.CodigoPostal;
                    drNew[16] = personas.EmailsEnvioFc;

                    drNew[17] = personas.Personeria;
                    drNew[18] = personas.AlicuotaIvaDefecto;
                    drNew[19] = personas.TipoComprobanteDefecto;
                    drNew[20] = personas.fechaAlta;
                    drNew[21] = personas.CBU;

                    drNew[22] = personas.Banco;
                    drNew[23] = personas.NombreFantansia;
                    drNew[24] = personas.Contacto;
                    drNew[25] = personas.Codigo;
                    drNew[26] = 0;
                    drNew[27] = 10;
                    drNew[28] = personas.Vendedor;


                    dt.Rows.Add(drNew);

                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
                #endregion
            }

            if (dt.Rows.Count > 0)
            {

                using (var sqlConnection = new SqlConnection(ACHEString))
                {
                    sqlConnection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = "Personas";
                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
            }
        }
        else
            throw new Exception("No se encontraron datos para importar");
    }

    private static void ObtenerIDProvinciayCiudad(PersonasCSVTmp personas)
    {
        using (var dbConcext = new ACHEEntities())
        {
            if (personas.Ciudad.ToUpper().Contains("CABA") || personas.Ciudad.ToUpper().Contains("C.A.B.A.") || personas.Ciudad.ToUpper().Contains("CAPITAL FEDERAL"))
            {
                personas.Provincia = "2";
                personas.Ciudad = "5003";
            }
            else
            {
                var provincia = dbConcext.Provincias.Where(x => x.Nombre.ToUpper() == personas.Provincia.ToUpper()).FirstOrDefault();
                if (provincia != null)
                    personas.Provincia = provincia.IDProvincia.ToString();
                else
                    personas.Provincia = "2";//Ciudad de buenos aires

                var ciudad = dbConcext.Ciudades.Where(x => x.Nombre.ToUpper() == personas.Ciudad.ToUpper() && x.IDProvincia == provincia.IDProvincia).FirstOrDefault();
                if (ciudad != null)
                    personas.Ciudad = ciudad.IDCiudad.ToString();
                else
                    personas.Ciudad = dbConcext.Ciudades.Where(x => x.IDProvincia == provincia.IDProvincia && x.Nombre == "SIN IDENTIFICAR").FirstOrDefault().IDCiudad.ToString();//SIN IDENTIFICAR
            }
        }
    }

    private static void ObtenerIDUsuarioAdicional(PersonasCSVTmp personas, int IDUsuario)
    {
        using (var dbConcext = new ACHEEntities())
        {
            var vendedor = dbConcext.UsuariosAdicionales.Where(x => x.IDUsuario == IDUsuario && x.Email.ToUpper() == personas.Vendedor).FirstOrDefault();

            personas.Vendedor = vendedor != null ? vendedor.IDUsuarioAdicional.ToString() : null;
        }
    }

    #endregion

    #region LISTA DE PRECIOS

    public static void RealizarImportacionListaPrecios(int idUsuario, List<ProductosPreciosCSVTmp> lista, string ACHEString, int idProceso)
    {
        int idLista = 0;
        List<string> codigosConceptos = new List<string>();
        if (lista != null && lista.Any(x => x.Estado == "A"))
        {
            //IMPORTAR
            DataTable dt = new DataTable();
            dt.Columns.Add("Codigo", typeof(string));//0
            dt.Columns.Add("Precio", typeof(decimal));//1
            dt.Columns.Add("IDUsuario", typeof(int));//2
            dt.Columns.Add("Stock", typeof(string));//3
            dt.Columns.Add("Costo", typeof(decimal));//4
            dt.Columns.Add("Rentabilidad", typeof(decimal));//5

            foreach (var prod in lista.Where(x => x.Estado == "A").ToList())
            {
                idLista = prod.IDLista;

                #region Armo DataRow
                try
                {
                    DataRow drNew = dt.NewRow();
                    drNew[0] = prod.Codigo;
                    drNew[1] = prod.Precio;
                    drNew[2] = prod.IDUsuario;
                    drNew[3] = "0";// prod.Stock;
                    drNew[4] = prod.Costo;
                    drNew[5] = prod.Rentabilidad;
                    dt.Rows.Add(drNew);
                    codigosConceptos.Add(prod.Codigo);
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
                #endregion
            }

            if (dt.Rows.Count > 0)
            {
                EliminarTmp(idUsuario);
                using (var sqlConnection = new SqlConnection(ACHEString))
                {
                    sqlConnection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = "ConceptosTmp";
                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
                if (idLista > 0)
                    ProcesarListaTmp(idUsuario, idLista, idProceso);
                else
                {
                    ActualizarPrecios(idUsuario);

                    //Modifico abonosdetalles que tengan este concepto. Tambien modifico los totales de los abonos con el nuevo precio
                    BackgroundJob.Enqueue(() => AbonosCommon.ActualizarPreciosAbonos(codigosConceptos, idUsuario));

                    //Se notifican los cambios de todos los productos
                    ConceptosCommon.NotificacionCambioConcepto(idUsuario, codigosConceptos,true, idProceso);
                }

            }
        }
        else
            throw new Exception("no se encontraron datos para importar");
    }

    public static string ValidarProductosPrecios(ProductosPreciosCSVTmp productos, List<Conceptos> Conceptos)
    {
        string resultado = string.Empty;

        resultado += (string.IsNullOrEmpty(productos.Codigo)) ? "El campo Código es obligatorio, " : "";
        resultado += (string.IsNullOrEmpty(productos.Precio)) ? "El campo Precio es obligatorio, " : "";
        resultado += (string.IsNullOrEmpty(productos.Costo)) ? "El campo Costo interno es obligatorio, " : "";

        resultado += (productos.Codigo.Length > 50) ? "El campo Código supera los 50 carácteres , " : "";
        resultado += (productos.Precio.Length > 10) ? "El campo Precio supera los 10 carácteres , " : "";
        resultado += (productos.Costo.Length > 10) ? "El campo Costo interno supera los 10 carácteres , " : "";
        resultado += (productos.Rentabilidad.Length > 10) ? "El campo Costo interno supera los 10 carácteres , " : "";


        decimal n;
        decimal.TryParse(productos.Precio, out n);
        resultado += (n == 0) ? "El campo Precio debe ser numérico y mayor a 0, " : "";

        /*if (!string.IsNullOrWhiteSpace(productos.Stock))
        {
            int Stock;
            int.TryParse(productos.Stock, out Stock);
            if (Stock == 0 && productos.Stock != "0")
                resultado += (n == 0) ? "El campo Stock debe ser mayor o igual 0, " : "";
        }*/

        decimal Costo;
        decimal.TryParse(productos.Costo, out Costo);
        if (Costo == 0 && productos.Costo != "0")
            resultado += (Costo == 0) ? "El campo Costo debe ser mayor o igual 0, " : "";

        //using (var dbContext = new ACHEEntities())
        //{
        //if (productos.IDLista > 0)
        //{
        //    var lista = dbContext.PreciosConceptos.Any(x => x.Conceptos.Codigo.ToUpper() == productos.Codigo.ToUpper() && x.IDListaPrecios == productos.IDLista && x.Conceptos.IDUsuario == productos.IDUsuario);
        //    resultado += (!lista) ? "El Código ingresado no pertenece a la lista seleccionada, " : "";
        //}
        var codigo = Conceptos.Any(x => x.Codigo.ToUpper() == productos.Codigo.ToUpper().Trim());
        resultado += (!codigo) ? "El Código ingresado no existe, " : "";
        //}
        return resultado;
    }

    public static List<ProductosPreciosCSVTmp> LeerArchivoCSVListaPrecios(string idLista, int idUsuario, string path)
    {
        List<ProductosPreciosCSVTmp> listaPreciosCSV = new List<ProductosPreciosCSVTmp>();

        //FileHelperEngine engine = new FileHelperEngine(typeof(ProductosPreciosCSV));
        //engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //ProductosPreciosCSV[] res = (ProductosPreciosCSV[])engine.ReadFile(path);
        var engine = new DelimitedFileEngine<ProductosPreciosCSV>();
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //engine.Options.Delimiter=";";
        ProductosPreciosCSV[] res = (ProductosPreciosCSV[])engine.ReadFile(path);

        if (res.Count() < 1)
        {
            engine.Options.Delimiter = ",";
            ProductosPreciosCSV[] resaux = (ProductosPreciosCSV[])engine.ReadFile(path);
            if (resaux.Count() > 0)
                res = resaux;
        }

        List<Conceptos> conceptos = new List<Conceptos>();
        using (var dbContext = new ACHEEntities())
        {
            conceptos = dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario && x.Codigo != "").ToList();
        }

        ProductosPreciosCSVTmp prodCSV;
        foreach (var productos in res)
        {
            prodCSV = new ProductosPreciosCSVTmp();
            prodCSV.IDUsuario = idUsuario;
            prodCSV.Codigo = productos.Codigo;
            prodCSV.Precio = productos.Precio.Replace(".", ",");
            prodCSV.IDLista = Convert.ToInt32(idLista);
            prodCSV.Rentabilidad = prodCSV.IDLista == 0 ? (productos.Rentabilidad != "" ? productos.Rentabilidad.Replace(".", ",") : "0") : "0";


            //prodCSV.Stock = productos.Stock;
            prodCSV.Costo = productos.Costo;

            prodCSV.resultados = ImportacionMasiva.ValidarProductosPrecios(prodCSV, conceptos);

            if (listaPreciosCSV.Any(x => x.Codigo == productos.Codigo))
                prodCSV.resultados = "El código se encuentra repetido en la lista, ";


            prodCSV.Estado = prodCSV.resultados == string.Empty ? "A" : "I";

            if (prodCSV.resultados == string.Empty)
                prodCSV.resultados = "<span class='label label-success'>OK</span>";
            else
                prodCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + prodCSV.resultados.Substring(0, prodCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

            listaPreciosCSV.Add(prodCSV);
        }

        return listaPreciosCSV;
    }

    private static void EliminarTmp(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 120;
                dbContext.Database.ExecuteSqlCommand("delete ConceptosTmp where IDUsuario= " + idUsuario, new object[] { });

                //dbContext.deleteConceptosTmp(idUsuario);
            }
        }
        catch (Exception)
        {
            throw new Exception("Error al eliminar los datos de la tabla temporal");
        }
    }

    private static void ProcesarListaTmp(int idUsuario, int idLista, int idProceso)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 120;
                dbContext.Database.ExecuteSqlCommand("exec ProcesarListaPrecios " + idUsuario + "," + idLista, new object[] { });

                //dbContext.ProcesarListaPrecios(idUsuario);
                LogCommon.FinalizarProceso(idUsuario, idProceso, false);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar los precios. Error: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
        }
    }

    private static void ActualizarPrecios(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 120;
                dbContext.ProcesarConceptos(idUsuario);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar los precios. Error: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
        }
    }

    #endregion

    #region STOCK

    public static void RealizarImportacionStock(int idUsuario, List<StockCSVTmp> lista, string ACHEString, int idProceso)
    {
        List<string> codigosConceptos = new List<string>();
        if (lista != null && lista.Any(x => x.Estado == "A"))
        {
            //IMPORTAR
            DataTable dt = new DataTable();
            dt.Columns.Add("Codigo", typeof(string));//1
            dt.Columns.Add("IDUsuario", typeof(int));//2
            dt.Columns.Add("IDInventario", typeof(int));//3
            dt.Columns.Add("IDConcepto", typeof(int));//4
            dt.Columns.Add("Stock", typeof(decimal));//5

            foreach (var prod in lista.Where(x => x.Estado == "A" && x.IDInventario > 0).ToList())
            {
                #region Armo DataRow
                try
                {
                    DataRow drNew = dt.NewRow();
                    drNew[0] = prod.Codigo;
                    drNew[1] = idUsuario;
                    drNew[2] = prod.IDInventario;
                    drNew[3] = -1;
                    drNew[4] = prod.Stock;
                    dt.Rows.Add(drNew);
                    codigosConceptos.Add(prod.Codigo);
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
                #endregion
            }

            if (dt.Rows.Count > 0)
            {
                EliminarStockTmp(idUsuario);
                using (var sqlConnection = new SqlConnection(ACHEString))
                {
                    sqlConnection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = "StockTmp";
                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }

                ProcesarStockTmp(idUsuario);
                ConceptosCommon.NotificacionCambioConcepto(idUsuario, codigosConceptos, false, idProceso);
            }
        }
        else
            throw new Exception("no se encontraron datos para importar");
    }

    public static string ValidarStock(StockCSVTmp productos, ref int idInventario, List<Inventarios> inventarios, List<Conceptos> conceptos)
    {
        string resultado = string.Empty;

        resultado += (string.IsNullOrEmpty(productos.Codigo)) ? "El campo Código es obligatorio, " : "";
        resultado += (string.IsNullOrEmpty(productos.Inventario)) ? "El campo Inventario es obligatorio, " : "";

        resultado += (productos.Codigo.Length > 50) ? "El campo Código supera los 50 carácteres , " : "";

        //if (!productos.Stock.Contains(".") && !productos.Stock.Contains(","))
        //{

        if (!string.IsNullOrWhiteSpace(productos.Stock))
        {
            decimal Stock;
            decimal.TryParse(productos.Stock.Replace(".", ","), out Stock);
            if (Stock == 0 && productos.Stock != "0")
                resultado += (Stock == 0) ? "El campo Stock debe ser mayor o igual 0 (NO utilice punto para separador de miles), " : "";
        }
        else
            resultado += "El campo Stock debe ser mayor o igual 0, ";
        //}
        //else
        //    resultado += "El campo Stock debe ser numérico. Sin comas ni puntos, ";

        using (var dbContext = new ACHEEntities())
        {
            var codigo = conceptos.Any(x => x.Codigo.ToUpper() == productos.Codigo.ToUpper());
            resultado += (!codigo) ? "El Código ingresado no existe, " : "";

            var inventario = inventarios.Where(x => x.Nombre.ToUpper() == productos.Inventario.ToUpper()).FirstOrDefault();
            if (inventario != null)
                idInventario = inventario.IDInventario;
            else
                resultado += "El Inventario ingresado no existe, ";
        }
        return resultado;
    }

    public static List<StockCSVTmp> LeerArchivoCSVStock(int idUsuario, string path)
    {
        List<StockCSVTmp> listaStockCSV = new List<StockCSVTmp>();

        var engine = new DelimitedFileEngine<StockCSV>();
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //engine.Options.Delimiter=";";
        StockCSV[] res = (StockCSV[])engine.ReadFile(path);

        if (res.Count() < 1)
        {
            engine.Options.Delimiter = ",";
            StockCSV[] resaux = (StockCSV[])engine.ReadFile(path);
            if (resaux.Count() > 0)
                res = resaux;
        }


        StockCSVTmp stockCSV;

        int idInventario = 0;

        List<Inventarios> inventarios = new List<Inventarios>();
        List<Conceptos> conceptos = new List<Conceptos>();
        using (var dbContext = new ACHEEntities())
        {
            inventarios = dbContext.Inventarios.Where(x => x.IDUsuario == idUsuario).ToList();
            conceptos = dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario && x.Codigo != "").ToList();
        }

        foreach (var productos in res)
        {
            stockCSV = new StockCSVTmp();
            stockCSV.IDUsuario = idUsuario;
            stockCSV.Codigo = productos.Codigo;
            stockCSV.Inventario = productos.Inventario;
            stockCSV.Stock = productos.Stock;

            idInventario = 0;

            stockCSV.resultados = ImportacionMasiva.ValidarStock(stockCSV, ref idInventario, inventarios, conceptos);
            stockCSV.IDInventario = idInventario;

            if (listaStockCSV.Any(x => x.Codigo.Trim().ToLower() == productos.Codigo.Trim().ToLower() && x.Inventario == productos.Inventario))
                stockCSV.resultados += "El producto se encuentra repetido en la lista, ";

            stockCSV.Estado = stockCSV.resultados == string.Empty ? "A" : "I";
            if (stockCSV.resultados == string.Empty)
                stockCSV.resultados = "<span class='label label-success'>OK</span>";
            else
                stockCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + stockCSV.resultados.Substring(0, stockCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

            listaStockCSV.Add(stockCSV);
        }

        return listaStockCSV;
    }

    private static void EliminarStockTmp(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
                dbContext.deleteStockTmp(idUsuario);
        }
        catch (Exception)
        {
            throw new Exception("Error al eliminar los datos de la tabla temporal");
        }
    }

    private static void ProcesarStockTmp(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.CommandTimeout = 120;
                dbContext.ProcesarStock(idUsuario);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar los precios. Error: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
        }
    }

    #endregion

    #region PLAN DE CUENTAS NEGOCIO

    public static List<PlanDeCuentasCSVTmp> LeerArchivoCSVPlanDeCuentas(int idUsuario, string path)
    {
        List<PlanDeCuentasCSVTmp> listaPlanDeCuentasCSV = new List<PlanDeCuentasCSVTmp>();
        FileHelperEngine engine = new FileHelperEngine(typeof(PlanDeCuentasCSV));
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        // to Read use:
        PlanDeCuentasCSV[] res = (PlanDeCuentasCSV[])engine.ReadFile(path);

        PlanDeCuentasCSVTmp pcCSV;
        foreach (var pc in res)
        {
            pcCSV = new PlanDeCuentasCSVTmp();
            pcCSV.IDUsuario = idUsuario;
            pcCSV.Codigo = pc.Codigo;
            pcCSV.Nombre = pc.Nombre;
            pcCSV.AdmiteAsientoManual = pc.AdmiteAsientoManual;
            pcCSV.TipoDeCuenta = (pc.TipoDeCuenta.ToUpper() == "PN") ? "RESULTADO" : pc.TipoDeCuenta.ToUpper();
            pcCSV.CodigoPadre = pc.CodigoPadre;

            pcCSV.resultados = ValidarPlanDeCuentas(pcCSV);

            if (listaPlanDeCuentasCSV.Any(x => x.Codigo == pc.Codigo))
                pcCSV.resultados = "El Código se encuentra repetido en la lista, ";

            pcCSV.Estado = pcCSV.resultados == string.Empty ? "A" : "I";

            if (pcCSV.resultados == string.Empty)
                pcCSV.resultados = "<span class='label label-success'>OK</span>";
            else
                pcCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + pcCSV.resultados.Substring(0, pcCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

            listaPlanDeCuentasCSV.Add(pcCSV);
        }

        return listaPlanDeCuentasCSV.ToList();
    }

    public static string ValidarPlanDeCuentas(PlanDeCuentasCSVTmp planDeCuentas)
    {
        string resultado = string.Empty;
        resultado += (string.IsNullOrEmpty(planDeCuentas.Codigo)) ? "El campo Codigo es obligatorio, " : "";
        resultado += (string.IsNullOrEmpty(planDeCuentas.Nombre)) ? "El campo Nombre es obligatorio, " : "";
        resultado += (string.IsNullOrEmpty(planDeCuentas.AdmiteAsientoManual)) ? "El campo Admite Asiento Manual es obligatorio, " : "";
        resultado += (string.IsNullOrEmpty(planDeCuentas.TipoDeCuenta)) ? "El campo Tipo de Cuenta es obligatorio, " : "";
        resultado += ContabilidadCommon.VerificarCodigo(planDeCuentas.Codigo) ? "El código contiene caracteres inválidos, " : "";
        if (planDeCuentas.CodigoPadre != "")
            resultado += ContabilidadCommon.VerificarCodigo(planDeCuentas.CodigoPadre) ? "El código padre contiene caracteres inválidos, " : "";

        resultado += (planDeCuentas.Nombre.Length > 100) ? "El campo Nombre supera los 100 carácteres, " : "";
        resultado += (planDeCuentas.TipoDeCuenta.Length > 100) ? "El campo Tipo De Cuenta supera los 100 carácteres, " : "";
        resultado += (planDeCuentas.Codigo.Length > 100) ? "El campo Código supera los 100 carácteres, " : "";
        resultado += (planDeCuentas.AdmiteAsientoManual != "SI" && planDeCuentas.AdmiteAsientoManual != "NO") ? "El campo admite asiento manual sólo puede contener el valor SI o NO, " : "";
        resultado += (planDeCuentas.TipoDeCuenta.ToUpper() != "ACTIVO" && planDeCuentas.TipoDeCuenta.ToUpper() != "PASIVO" && planDeCuentas.TipoDeCuenta.ToUpper() != "RESULTADO" && planDeCuentas.TipoDeCuenta.ToUpper() != "PN") ? "El campo admite asiento manual sólo puede contener el valor Activo, Pasivo, Resultado o PN, " : "";

        return resultado;
    }

    public static void RealizarImportacionPlanDeCuentas(List<PlanDeCuentasCSVTmp> lista, string ACHEString, int idusuario)
    {
        try
        {
            ContabilidadCommon.EliminarPlanDeCuentasActual(idusuario);
            ContabilidadCommon.EliminarConfiguracionPlanDeCuenta(idusuario);

            if (lista != null && lista.Any(x => x.Estado == "A"))
            {
                //IMPORTAR
                DataTable dt = new DataTable();
                dt.Columns.Add("IDUsuario", typeof(Int32));//0

                dt.Columns.Add("Nombre", typeof(string));//1
                dt.Columns.Add("Codigo", typeof(string));//2
                dt.Columns.Add("AdminiteAsientoManual", typeof(Boolean));//3
                dt.Columns.Add("TipoDeCuenta", typeof(string));//4

                foreach (var prod in lista.Where(x => x.Estado == "A").ToList())
                {
                    #region Armo DataRow
                    try
                    {
                        DataRow drNew = dt.NewRow();
                        drNew[0] = idusuario;
                        drNew[1] = prod.Nombre;
                        drNew[2] = prod.Codigo;
                        drNew[3] = (prod.AdmiteAsientoManual == "SI") ? 1 : 0;
                        drNew[4] = prod.TipoDeCuenta;

                        dt.Rows.Add(drNew);

                    }
                    catch (Exception ex)
                    {
                        var msg = ex.Message;
                    }
                    #endregion
                }

                if (dt.Rows.Count > 0)
                {
                    using (var sqlConnection = new SqlConnection(ACHEString))
                    {
                        sqlConnection.Open();
                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                        {
                            bulkCopy.DestinationTableName = "PlanDeCuentas";
                            foreach (DataColumn col in dt.Columns)
                                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                            bulkCopy.WriteToServer(dt);
                        }
                    }
                }
            }
            else
                throw new CustomException("No se encontraron datos para importar.");
        }
        catch (CustomException e)
        {
            throw new Exception(e.Message);
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

    public static void ReferenciarPlanDeCuentas(int idUsuario, List<PlanDeCuentasCSVTmp> lista)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var planDeCuenta = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == idUsuario).OrderBy(x => x.Codigo).ToList();
                List<PlanDeCuentasCSVTmp> listaAux = new List<PlanDeCuentasCSVTmp>();
                foreach (var item in planDeCuenta)
                {
                    var CtasHijas = lista.Where(x => x.CodigoPadre == item.Codigo).ToList();
                    foreach (var Hijas in CtasHijas)
                    {
                        if (Hijas != null)
                        {
                            Hijas.IDPadre = item.IDPlanDeCuenta;
                            listaAux.Add(Hijas);
                        }
                    }
                }
                foreach (var item in listaAux)
                {
                    var CtasHijas = planDeCuenta.Where(x => x.Codigo == item.Codigo).FirstOrDefault();
                    CtasHijas.IDPadre = item.IDPadre;
                }
                dbContext.SaveChanges();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region FACTURAS VENTA

    public static List<FacturasCSVTmp> LeerArchivoCSVFacturas(WebUser usu, string path)
    {
        List<FacturasCSVTmp> listaFacturasCSV = new List<FacturasCSVTmp>();
        //FileHelperEngine engine = new FileHelperEngine(typeof(FacturasCSV));
        //engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //FacturasCSV[] res = (FacturasCSV[])engine.ReadFile(path);
        var engine = new DelimitedFileEngine<FacturasCSV>();
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //engine.Options.Delimiter=";";
        FacturasCSV[] res = (FacturasCSV[])engine.ReadFile(path);

        if (res.Count() < 1)
        {
            engine.Options.Delimiter = ",";
            FacturasCSV[] resaux = (FacturasCSV[])engine.ReadFile(path);
            if (resaux.Count() > 0)
                res = resaux;
        }


        FacturasCSVTmp facturasCSV;
        foreach (var FC in res)
        {
            if (FC.RazonSocial != string.Empty)
            {
                facturasCSV = new FacturasCSVTmp();
                facturasCSV.IDUsuario = usu.IDUsuario;
                facturasCSV.Tipo = "C";
                facturasCSV.RazonSocial = FC.RazonSocial;
                facturasCSV.NombreFantasia = FC.NombreFantasia;
                facturasCSV.Email = FC.Email;
                facturasCSV.TipoDocumento = FC.TipoDocumento;
                facturasCSV.NroDocumento = FC.NroDocumento.ReplaceAll("-", "").ReplaceAll(" ", "").Trim();
                facturasCSV.CondicionIva = FC.CondicionIva;
                if (facturasCSV.NroDocumento.Length > 8)
                    facturasCSV.Personeria = (facturasCSV.NroDocumento.Substring(0, 2) == "30") ? "J" : "F";
                else
                    facturasCSV.Personeria = "F";

                if (FC.Provincia.ToUpper().Contains("CIUDAD") || FC.Provincia.ToUpper().Contains("CABA")
                     || FC.Provincia.ToUpper().Contains("C.A.B.A") || FC.Provincia.ToUpper().Contains("FEDERAL"))
                    facturasCSV.Provincia = "Ciudad de Buenos Aires";
                else
                    facturasCSV.Provincia = FC.Provincia;
                facturasCSV.Ciudad = FC.Ciudad;
                facturasCSV.Domicilio = FC.Domicilio;
                facturasCSV.Concepto = FC.Concepto;
                facturasCSV.Web = FC.Web;

                facturasCSV.Fecha = FC.Fecha;
                facturasCSV.TipoComprobante = FC.TipoComprobante.ToUpper();
                facturasCSV.PuntoDeVenta = FC.PuntoDeVenta;
                facturasCSV.NroComprobante = FC.NroComprobante;
                facturasCSV.CAE = FC.CAE;
                facturasCSV.Modo = (string.IsNullOrWhiteSpace(FC.Modo)) ? "T" : FC.Modo;
                //facturasCSV.Modo = (string.IsNullOrWhiteSpace(FC.CAE)) ? "T" : "T";
                if (!string.IsNullOrWhiteSpace(facturasCSV.Fecha))
                    facturasCSV.FechaCAE = (string.IsNullOrWhiteSpace(FC.CAE)) ? Convert.ToDateTime(facturasCSV.Fecha).AddDays(30).ToString("dd/MM/yyyy") : null;

                facturasCSV.ImporteNeto = FC.ImporteNeto;
                facturasCSV.ImporteNoGrabado = FC.ImporteNoGrabado;
                facturasCSV.IVA2700 = FC.IVA2700;
                facturasCSV.IVA2100 = FC.IVA2100;
                facturasCSV.IVA1005 = FC.IVA1005;
                facturasCSV.IVA0205 = FC.IVA0205;
                facturasCSV.IVA0500 = FC.IVA0500;
                facturasCSV.IVA0000 = FC.IVA0000;
                facturasCSV.PercepcionesIVA = FC.PercepcionesIVA;
                facturasCSV.PercepcionesIIBB = FC.PercepcionesIIBB;
                facturasCSV.Total = FC.Total;

                facturasCSV.FormaDePago = FC.FormaDePago;
                facturasCSV.Banco = FC.Banco;
                facturasCSV.Caja = FC.Caja;
                facturasCSV.MontoPagado = FC.MontoPagado;
                facturasCSV.FechaDePago = FC.FechaDePago;

                facturasCSV.CodigoCuentaContable = FC.CodigoCuentaContable;
                facturasCSV.Observaciones = FC.Observaciones;

                facturasCSV.resultados = ValidarFacturas(facturasCSV, usu);
                facturasCSV.resultados = ObtenerConceptos(facturasCSV, usu, facturasCSV.resultados);

                if (FC.Modo == "T" && listaFacturasCSV.Any(x => x.NroComprobante == FC.NroComprobante && x.TipoComprobante == FC.TipoComprobante && x.PuntoDeVenta == FC.PuntoDeVenta))
                    facturasCSV.resultados += "El Comprobante se encuentra repetido en la lista, ";
                facturasCSV.Estado = facturasCSV.resultados == string.Empty ? "A" : "I";

                if (facturasCSV.resultados == string.Empty)
                    facturasCSV.resultados = "<span class='label label-success'>OK</span>";
                else
                    facturasCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + facturasCSV.resultados.Substring(0, facturasCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

                listaFacturasCSV.Add(facturasCSV);
            }
        }

        return listaFacturasCSV.ToList();
    }

    private static string ValidarFacturas(FacturasCSVTmp facturas, WebUser usu)
    {
        string resultado = string.Empty;

        #region/*Obligatorios*/
        //resultado += (string.IsNullOrEmpty(facturas.RazonSocial)) ? "El campo Razón Social es obligatorio , " : "";
        if (facturas.CondicionIva != "CF")
            resultado += (string.IsNullOrEmpty(facturas.NroDocumento)) ? "El campo Nro Documento es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.CondicionIva)) ? "El campo Condición Iva es obligatorio , " : "";

        resultado += (string.IsNullOrEmpty(facturas.Fecha)) ? "El campo Fecha es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.TipoComprobante)) ? "El campo Tipo Comprobante es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.PuntoDeVenta)) ? "El campo Punto de Venta es obligatorio , " : "";
        resultado += (facturas.Modo != "E" && string.IsNullOrEmpty(facturas.NroComprobante)) ? "El campo Nro Comprobante es obligatorio , " : "";
        if (facturas.MontoPagado != "" && facturas.MontoPagado != "0")
            resultado += (string.IsNullOrEmpty(facturas.FechaDePago)) ? "El campo Fecha de Pago es obligatorio , " : "";

        var fecha = new DateTime();
        if (!DateTime.TryParse(facturas.Fecha, out fecha))
            resultado += "El campo Fecha no tiene un formato válido , ";

        if (facturas.MontoPagado.Trim() != "")
        {
            var FechaDePago = new DateTime();
            if (!DateTime.TryParse(facturas.FechaDePago, out FechaDePago))
                resultado += "El campo fecha de pago no tiene un formato válido , ";
        }
        #endregion

        #region  /*longitud  obligatoria*/
        resultado += (facturas.TipoComprobante.Length > 3) ? "El campo Tipo Comprobante supera los 3 carácteres , " : "";
        resultado += (facturas.PuntoDeVenta.Length > 4) ? "El campo Punto de Venta supera los 8 carácteres , " : "";
        resultado += (facturas.NroComprobante.Length > 8) ? "El campo Nro Comprobante supera los 8 carácteres , " : "";

        resultado += (facturas.ImporteNeto.Length > 18) ? "El campo Importe Neto supera los 18 carácteres , " : "";
        resultado += (facturas.ImporteNoGrabado.Length > 18) ? "El campo Importe No Grabado supera los 18 carácteres , " : "";
        resultado += (facturas.PercepcionesIVA.Length > 18) ? "El campo Percepciones IVA supera los 18 carácteres , " : "";
        resultado += (facturas.PercepcionesIIBB.Length > 18) ? "El campo Percepciones IIBB supera los 18 carácteres , " : "";

        resultado += (facturas.IVA2700.Length > 18) ? "El campo IVA 27,00% supera los 18 carácteres , " : "";
        resultado += (facturas.IVA2100.Length > 18) ? "El campo IVA 21,00% supera los 18 carácteres , " : "";
        resultado += (facturas.IVA1005.Length > 18) ? "El campo IVA 10,50% supera los 18 carácteres , " : "";
        resultado += (facturas.IVA0205.Length > 18) ? "El campo IVA 02,50% supera los 18 carácteres , " : "";
        resultado += (facturas.IVA0500.Length > 18) ? "El campo IVA 00,50% supera los 18 carácteres , " : "";
        resultado += (facturas.IVA0000.Length > 18) ? "El campo IVA 00,00% supera los 18 carácteres , " : "";
        #endregion

        #region/* Numeros*/
        decimal ImporteNeto = 0;
        if (facturas.ImporteNeto.Trim() != "")
        {
            decimal.TryParse(facturas.ImporteNeto.Replace(".", ","), out ImporteNeto);
            if (ImporteNeto == 0 && facturas.ImporteNeto != "0")
                resultado += "El campo Importe Neto contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal Total = 0;
        if (facturas.Total.Trim() != "")
        {
            decimal.TryParse(facturas.Total.Replace(".", ","), out Total);
            if (Total == 0 && facturas.Total != "0")
                resultado += "El campo Total contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal ImporteNoGrabado = 0;
        if (facturas.ImporteNoGrabado.Trim() != "")
        {
            decimal.TryParse(facturas.ImporteNoGrabado.Replace(".", ","), out ImporteNoGrabado);
            if (ImporteNoGrabado == 0 && facturas.ImporteNoGrabado != "0")
                resultado += "El campo Importe No Grabado contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal PercepcionesIVA = 0;
        if (facturas.PercepcionesIVA.Trim() != "")
        {
            decimal.TryParse(facturas.PercepcionesIVA.Replace(".", ","), out PercepcionesIVA);
            if (PercepcionesIVA == 0 && facturas.PercepcionesIVA != "0")
                resultado += "El campo Percepciones IVA contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal PercepcionesIIBB = 0;
        if (facturas.PercepcionesIIBB.Trim() != "")
        {
            decimal.TryParse(facturas.PercepcionesIIBB.Replace(".", ","), out PercepcionesIIBB);
            if (PercepcionesIIBB == 0 && facturas.PercepcionesIIBB != "0")
                resultado += "El campo Percepciones IIBB contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }


        decimal IVA2700 = 0;
        if (facturas.IVA2700.Trim() != "")
        {
            decimal.TryParse(facturas.IVA2700.Replace(".", ","), out IVA2700);
            if (IVA2700 == 0 && facturas.IVA2700 != "0")
                resultado += "El campo IVA 27,00% contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal IVA2100 = 0;
        if (facturas.IVA2100.Trim() != "")
        {
            decimal.TryParse(facturas.IVA2100.Replace(".", ","), out IVA2100);
            if (IVA2100 == 0 && facturas.IVA2100 != "0")
                resultado += "El campo IVA 21,00% contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal IVA1005 = 0;
        if (facturas.IVA1005.Trim() != "")
        {
            decimal.TryParse(facturas.IVA1005.Replace(".", ","), out IVA1005);
            if (IVA1005 == 0 && facturas.IVA1005 != "0")
                resultado += "El campo IVA 10,50% contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal IVA0500 = 0;
        if (facturas.IVA0500.Trim() != "")
        {
            decimal.TryParse(facturas.IVA0500.Replace(".", ","), out IVA0500);
            if (IVA0500 == 0 && facturas.IVA0500 != "0")
                resultado += "El campo IVA 05,00% contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal IVA0205 = 0;
        if (facturas.IVA0205.Trim() != "")
        {
            decimal.TryParse(facturas.IVA0205.Replace(".", ","), out IVA0205);
            if (IVA0205 == 0 && facturas.IVA0205 != "0")
                resultado += "El campo IVA 02,05% contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal IVA0000 = 0;
        if (facturas.IVA0000.Trim() != "")
        {
            decimal.TryParse(facturas.IVA0000.Replace(".", ","), out IVA0000);
            if (IVA0000 == 0 && facturas.IVA0000 != "0")
                resultado += "El campo IVA 00,00% contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal MontoPagado = 0;
        if (facturas.MontoPagado.Trim() != "")
        {
            decimal.TryParse(facturas.MontoPagado.Replace(".", ","), out MontoPagado);
            if (MontoPagado == 0 && facturas.MontoPagado != "0")
                resultado += "El campo monto pagado contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }
        #endregion

        #region otras logicas
        switch (facturas.CondicionIva.Trim())
        {
            case "EX":
            case "CF":
            case "MO":
                resultado += (facturas.TipoComprobante != "FCC" && facturas.TipoComprobante != "FCB" && facturas.TipoComprobante != "NCC" && facturas.TipoComprobante != "NCB" && facturas.TipoComprobante != "NDC" && facturas.TipoComprobante != "NDB" && facturas.TipoComprobante != "COT") ? "El campo Tipo Comprobante sólo puede tener los siguientes valores: FCC,FCB,NCC,NCB,NDC,NDB,COT carácteres , " : "";
                break;
            case "RI":
                if (usu.CondicionIVA == "RI")
                    resultado += (facturas.TipoComprobante != "FCA" && facturas.TipoComprobante != "FCB" && facturas.TipoComprobante != "FCE" && facturas.TipoComprobante != "NCA" && facturas.TipoComprobante != "NCB" && facturas.TipoComprobante != "NCE" && facturas.TipoComprobante != "NDA" && facturas.TipoComprobante != "NDB" && facturas.TipoComprobante != "NDE" && facturas.TipoComprobante != "COT") ? "El campo Tipo Comprobante sólo puede tener los siguientes valores: FCA,FCB,NCA,NCB,NDA,NDB,FCE,NCE,NDE,COT carácteres , " : "";
                else if (usu.CondicionIVA == "MO" || usu.CondicionIVA == "EX")
                    resultado += (facturas.TipoComprobante != "FCC" && facturas.TipoComprobante != "NCC" && facturas.TipoComprobante != "NDC" && facturas.TipoComprobante != "COT") ? "El campo Tipo Comprobante sólo puede tener los siguientes valores: FCC,NCC,NDC,COT carácteres , " : "";
                break;
        }

        if (facturas.FormaDePago != "")
        {
            string forma = facturas.FormaDePago.Trim().ToLower();
            if (forma != "transferencia" && forma != "efectivo" && forma != "deposito")
                resultado += "La forma de pago no es válida, ";
        }

        //Se elimina validacion porque hay medicos que son exentos de IVA

        //if (usu.CondicionIVA == "RI")
        //    resultado += (IVA0000 == 0 && IVA0205 == 0 && IVA0500 == 0 && IVA1005 == 0 && IVA2700 == 0 && IVA2100 == 0) ? "Su condición frente al IVA no admite productos sin IVA, " : "";
        //else
        //{
        //    resultado += (IVA0000 > 0 || IVA0205 > 0 || IVA0500 > 0 || IVA1005 > 0 || IVA2700 > 0 || IVA2100 > 0) ? "Su condición frente al IVA no admite productos con IVA, " : "";
        //    resultado += (PercepcionesIIBB > 0 || PercepcionesIIBB > 0 || ImporteNoGrabado > 0) ? "Su condición frente al IVA no admite comprobantes con percepciones, " : "";
        //}
        #endregion

        #region/*validaciones contra la db*/
        using (var dbContext = new ACHEEntities())
        {
            var numero = facturas.Modo != "E" ? Convert.ToInt32(facturas.NroComprobante) : 0;
            var punto = Convert.ToInt32(facturas.PuntoDeVenta);

            var puntoValido = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == usu.IDUsuario && x.Punto == punto).FirstOrDefault();
            if (puntoValido == null)
                resultado += "El punto de venta ingresado no es correcto, ";
            else
            {
                var documento = (facturas.Tipo == "T" ? dbContext.Comprobantes.Where(x => x.Tipo.ToUpper() == facturas.TipoComprobante.ToUpper() && x.PuntosDeVenta.Punto == punto && x.Numero == numero && x.IDUsuario == usu.IDUsuario).Any() : false);
                var Provincia = dbContext.Provincias.Where(x => x.Nombre.ToUpper() == facturas.Provincia.ToUpper()).FirstOrDefault();
                var ciudad = dbContext.Ciudades.Where(x => x.Nombre.ToUpper() == facturas.Ciudad.ToUpper()).FirstOrDefault();

                PlanDeCuentas planDeCuenta = new PlanDeCuentas();
                if (usu.UsaPlanCorporativo)
                {
                    planDeCuenta = dbContext.PlanDeCuentas.Where(x => x.Codigo == facturas.CodigoCuentaContable && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                    var configPlanDeCuenta = dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == usu.IDUsuario);
                    resultado += (!configPlanDeCuenta) ? "No tiene configurado el plan de cuentas, " : "";
                }

                resultado += (documento) ? "El Comprobante ingresado ya existe, " : "";

                facturas.IDPuntoDeVenta = puntoValido.IDPuntoVenta;

                if (Provincia == null)
                    resultado += "La Provincia ingresada no es correcta, ";
                else
                    facturas.IDProvincia = Provincia.IDProvincia;

                if (ciudad != null)
                    facturas.IDCiudad = ciudad.IDCiudad;
                else
                {
                    try
                    {
                        facturas.IDCiudad = dbContext.Ciudades.Where(x => x.Nombre.ToUpper() == "SIN IDENTIFICAR" && x.IDProvincia == facturas.IDProvincia).FirstOrDefault().IDCiudad;
                    }
                    catch (Exception ex)
                    {
                        var msg = ex.Message;
                    }
                }

                if (!string.IsNullOrEmpty(facturas.Banco))
                {
                    var banco = dbContext.Bancos.Any(x => x.IDUsuario == usu.IDUsuario && x.NroCuenta == facturas.Banco);
                    resultado += (!banco) ? "No existe el banco, " : "";
                }
                if (!string.IsNullOrEmpty(facturas.Caja))
                {
                    var caja = dbContext.Cajas.Any(x => x.IDUsuario == usu.IDUsuario && x.Nombre == facturas.Caja);
                    resultado += (!caja) ? "No existe la caja, " : "";
                }

                if (usu.UsaPlanCorporativo)
                {
                    if (planDeCuenta == null)
                        resultado += "El código de la cuenta contable ingresado no es correcta, ";
                    else
                        facturas.idPlanDeCuenta = planDeCuenta.IDPlanDeCuenta;
                }

                decimal totalFc = 0;

                if (!string.IsNullOrEmpty(facturas.ImporteNeto))
                    totalFc += decimal.Parse(facturas.ImporteNeto.Replace(".", ","));
                if (!string.IsNullOrEmpty(facturas.ImporteNoGrabado))
                    totalFc += decimal.Parse(facturas.ImporteNoGrabado.Replace(".", ","));

                if (totalFc == 0)
                    resultado += "El Total de la factura no puede ser 0, ";
            }
        }
        #endregion
        return resultado;
    }

    public static void RealizarImportacionFacturas(List<FacturasCSVTmp> lista, string ACHEString, int idUsuario)
    {
        if (lista != null && lista.Any(x => x.Estado == "A"))
        {
            //IMPORTAR
            DataTable dt = new DataTable();

            dt.Columns.Add("IDUsuario", typeof(Int32));//0
            dt.Columns.Add("Tipo", typeof(string));//0

            dt.Columns.Add("RazonSocial", typeof(string));//1
            dt.Columns.Add("NombreFantasia", typeof(string));//2
            dt.Columns.Add("Email", typeof(string));//3
            dt.Columns.Add("TipoDocumento", typeof(string));//4
            dt.Columns.Add("NroDocumento", typeof(string));//5

            dt.Columns.Add("CondicionIva", typeof(string));//6
            dt.Columns.Add("Provincia", typeof(Int32));//7
            dt.Columns.Add("Ciudad", typeof(Int32));//8
            dt.Columns.Add("Domicilio", typeof(string));//9
            dt.Columns.Add("Web", typeof(string));//10
            dt.Columns.Add("Personeria", typeof(string));//10

            foreach (var facturas in lista.Where(x => x.Estado == "A").ToList())
            {
                #region Armo DataRow
                try
                {
                    DataRow drNew = dt.NewRow();
                    drNew[0] = facturas.IDUsuario;
                    drNew[1] = facturas.Tipo;

                    drNew[2] = facturas.RazonSocial;
                    drNew[3] = facturas.NombreFantasia;
                    drNew[4] = facturas.Email;
                    drNew[5] = facturas.TipoDocumento;
                    drNew[6] = facturas.NroDocumento;

                    drNew[7] = facturas.CondicionIva;
                    drNew[8] = facturas.IDProvincia;
                    drNew[9] = facturas.IDCiudad;
                    drNew[10] = facturas.Domicilio;
                    drNew[11] = facturas.Web;
                    drNew[12] = facturas.Personeria;
                    dt.Rows.Add(drNew);

                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
                #endregion
            }

            if (dt.Rows.Count > 0)
            {
                using (var sqlConnection = new SqlConnection(ACHEString))
                {
                    sqlConnection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = "PersonasTemp";
                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
            }

            using (var dbContext = new ACHEEntities())
            {
                dbContext.ObtenerDatosPersonasTmp(idUsuario);
            }
        }
        else
            throw new Exception("No se encontraron datos para importar");
    }

    public static void ReferenciarFacturasClientes(List<FacturasCSVTmp> listaFacturas, WebUser usu)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                List<FacturasCSVTmp> facturas = new List<FacturasCSVTmp>();
                var listaPersonas = dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario).ToList();
                var condVentaList = dbContext.CondicionesVentas.Where(x => x.IDUsuario == usu.IDUsuario).ToList();

                foreach (var item in listaFacturas.Where(x => x.Estado == "A"))
                {
                    Personas persona = null;

                    if (!string.IsNullOrWhiteSpace(item.NroDocumento))
                        persona = listaPersonas.Where(x => x.TipoDocumento == item.TipoDocumento && x.NroDocumento == item.NroDocumento).FirstOrDefault();
                    else
                        persona = listaPersonas.Where(x => x.RazonSocial.ToUpper() == item.RazonSocial.ToUpper()).FirstOrDefault();

                    if (persona != null)
                        item.IDPersona = persona.IDPersona;

                    facturas.Add(item);
                }

                try
                {
                    GuardarFacturas(dbContext, facturas, usu, condVentaList);
                }
                catch (Exception ex) { }

                /*var listaPersonas = dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario).ToList();
                List<FacturasCSVTmp> facturas = new List<FacturasCSVTmp>();
                foreach (var item in listaPersonas)
                {
                    if (string.IsNullOrWhiteSpace(item.NroDocumento))
                        facturas = listaFacturas.Where(x => x.RazonSocial.ToUpper() == item.RazonSocial.ToUpper() && x.TipoDocumento == item.TipoDocumento && x.NroDocumento == item.NroDocumento && x.Estado == "A").ToList();
                    else
                        facturas = listaFacturas.Where(x => x.TipoDocumento == item.TipoDocumento && x.NroDocumento == item.NroDocumento && x.Estado == "A").ToList();

                    foreach (var fc in facturas)
                        fc.IDPersona = item.IDPersona;

                    if (facturas.Any())
                    {
                        try
                        {
                            GuardarFacturas(dbContext, facturas, usu);
                        }
                        catch (Exception ex) { }
                    }
                }*/
            }
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    private static void GuardarFacturas(ACHEEntities dbContext, List<FacturasCSVTmp> listaFacturas, WebUser usu, List<CondicionesVentas> condiciones)
    {
        try
        {
            var TipoConcepto = "2";
            var IDJuresdiccion = 0;
            var CondicionVenta = "Cuenta Corriente";


            var listaAImportar = listaFacturas.Where(x => x.Estado == "A").ToList();
            int index = 0;

            foreach (var item in listaAImportar)
            {
                index++;
                ComprobanteCartDto compCart = new ComprobanteCartDto();
                compCart.Items = new List<ComprobantesDetalleViewModel>();
                compCart.IDComprobante = -1;
                compCart.IDPersona = item.IDPersona;
                compCart.TipoComprobante = item.TipoComprobante;
                compCart.TipoConcepto = TipoConcepto;
                compCart.IDUsuario = item.IDUsuario;
                compCart.Modo = item.Modo;
                compCart.FechaComprobante = Convert.ToDateTime(item.Fecha);
                compCart.FechaVencimiento = Convert.ToDateTime(item.Fecha).AddDays(30).Date;
                compCart.IDPuntoVenta = item.IDPuntoDeVenta;
                compCart.Numero = !string.IsNullOrEmpty(item.NroComprobante) ? item.NroComprobante : "0";
                compCart.Observaciones = item.Observaciones;
                compCart.CondicionVenta = CondicionVenta;

                if (item.MontoPagado != string.Empty && Convert.ToDecimal(item.MontoPagado) > 0 && item.Caja != string.Empty)
                    compCart.CondicionVenta = "Efectivo";

                compCart.EmailUsuario = usu.Email;
                //JC COBRANZA AUTOMATICA
                var condicion = condiciones.First(x => x.Nombre.ToLower() == compCart.CondicionVenta.ToLower());
                if (condicion != null)
                    compCart.IDCondicionVenta = condicion.IDCondicionVenta;
                else
                    compCart.IDCondicionVenta = condiciones.First(x => x.Nombre.ToLower() == "efectivo").IDCondicionVenta;
                compCart.IDJuresdiccion = IDJuresdiccion;
                compCart.Items = new List<ComprobantesDetalleViewModel>();
                compCart.Tributos = new List<ComprobantesTributosViewModel>();
                compCart.ImporteNoGrabado = 0;// item.ImporteNoGrabado != string.Empty ? Math.Abs(Convert.ToDecimal(item.ImporteNoGrabado.Replace(".", ","))) : 0;
                compCart.PercepcionIVA = item.PercepcionesIVA != string.Empty ? Math.Abs(Convert.ToDecimal(item.PercepcionesIVA.Replace(".", ","))) : 0;
                compCart.PercepcionIIBB = item.PercepcionesIIBB != string.Empty ? Math.Abs(Convert.ToDecimal(item.PercepcionesIIBB.Replace(".", ","))) : 0;
                compCart.Items = item.Items;

                if (item.ImporteNoGrabado != string.Empty)
                {
                    var importeNoGravado = Math.Abs(Convert.ToDecimal(item.ImporteNoGrabado.Replace(".", ",")));
                    if (importeNoGravado > 0)
                    {
                        if (compCart.Items.Any() && compCart.Items.Any(x => x.Iva == 0))//si viene un solo item, lo remuevo y vuelvo a crear
                            compCart.Items.Remove(compCart.Items.First(x => x.Iva == 0));

                        var itemNoGravado = new ComprobantesDetalleViewModel();
                        itemNoGravado.Cantidad = 1;
                        itemNoGravado.Bonificacion = 0;
                        itemNoGravado.Concepto = item.Concepto;
                        itemNoGravado.Iva = 0;
                        itemNoGravado.PrecioUnitario = importeNoGravado;
                        itemNoGravado.IDPlanDeCuenta = item.idPlanDeCuenta;

                        compCart.Items.Add(itemNoGravado);
                    }
                }

                if (compCart.Items.Any())//TODO: VER PROQUE ALGUNAS VECES VIENE VACIO
                {
                    compCart.Items[0].Concepto = item.Concepto;
                    compCart.Items[0].TipoConcepto = "S";

                    if (!string.IsNullOrEmpty(item.CodigoCuentaContable))
                    {
                        var planDeCuentas = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.Codigo == item.CodigoCuentaContable).FirstOrDefault();
                        if (planDeCuentas != null)
                            compCart.Items[0].IDPlanDeCuenta = planDeCuentas.IDPlanDeCuenta;
                    }

                    foreach (var itemDet in compCart.Items)
                    {
                        if (itemDet.Concepto == string.Empty)
                            itemDet.Concepto = "Concepto generado automáticamente por la importación masiva";
                    }


                    try
                    {
                        var entity = ComprobantesCommon.Guardar(dbContext, compCart);
                        if (compCart.Items[0].IDPlanDeCuenta.HasValue && compCart.Items[0].IDPlanDeCuenta.Value > 0)
                        {

                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeVentas(usu, entity.IDComprobante);
                            }
                            catch (Exception ex)
                            { }
                        }

                        if (item.MontoPagado != string.Empty && Convert.ToDecimal(item.MontoPagado) > 0)
                            GuardarCobranza(dbContext, entity, item, usu);
                    }
                    catch (Exception ex)
                    {
                        var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                    }
                }
            }
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;

            //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PathBaseWeb"] + ConfigurationManager.AppSettings["BasicLogError"].Replace("~", ""), "GuardarFacturas" + msg, ex.ToString());
            throw new Exception(ex.Message);
        }
    }

    private static void GuardarCobranza(ACHEEntities dbContext, Comprobantes comp, FacturasCSVTmp csvTemp, WebUser usu)
    {
        try
        {
            //Determinamos cantidad de decimales que posee configurado el usuario
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            var punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == comp.IDPuntoVenta).FirstOrDefault();
            CobranzaCartDto cobrCartdto = new CobranzaCartDto();
            cobrCartdto.IDCobranza = 0;
            cobrCartdto.IDPersona = csvTemp.IDPersona;
            cobrCartdto.Tipo = "RC";
            cobrCartdto.Fecha = csvTemp.FechaDePago != "" ? csvTemp.FechaDePago : DateTime.Now.Date.ToString("dd/MM/yyyy");
            cobrCartdto.IDPuntoVenta = comp.IDPuntoVenta;
            cobrCartdto.NumeroCobranza = CobranzasCommon.obtenerProxNroCobranza(cobrCartdto.Tipo, usu.IDUsuario);
            cobrCartdto.Observaciones = "";
            cobrCartdto.Items = new List<CobranzasDetalleViewModel>();
            cobrCartdto.FormasDePago = new List<CobranzasFormasDePagoViewModel>();
            cobrCartdto.Retenciones = new List<CobranzasRetencionesViewModel>();

            cobrCartdto.Items.Add(new CobranzasDetalleViewModel(cantidadDecimales)
            {
                ID = 1,
                Comprobante = comp.Tipo + " " + punto.Punto.ToString("#0000") + "-" + comp.Numero.ToString("#00000000"),
                Importe = Convert.ToDecimal(csvTemp.MontoPagado.Replace(".", ",")),
                IDComprobante = comp.IDComprobante
            });

            CobranzasFormasDePagoViewModel formaPago = new CobranzasFormasDePagoViewModel();
            formaPago.ID = 1;
            formaPago.Importe = Convert.ToDecimal(csvTemp.MontoPagado.Replace(".", ","));
            if (csvTemp.FormaDePago.Trim() == "deposito")
                formaPago.FormaDePago = "Depósito";
            else if (csvTemp.FormaDePago.Trim() == "debito")
                formaPago.FormaDePago = "Débito";
            else
                formaPago.FormaDePago = csvTemp.FormaDePago.Trim();
            formaPago.NroReferencia = "";
            if (csvTemp.FormaDePago.Trim().ToLower() == "transferencia" || csvTemp.FormaDePago.Trim().ToLower() == "deposito")
                formaPago.IDBanco = dbContext.Bancos.First(x => x.IDUsuario == usu.IDUsuario && x.NroCuenta == csvTemp.Banco).IDBanco;
            else if (csvTemp.FormaDePago.Trim().ToLower() == "efectivo")
                formaPago.IDCaja = dbContext.Cajas.First(x => x.IDUsuario == usu.IDUsuario && x.Nombre == csvTemp.Caja).IDCaja;

            cobrCartdto.FormasDePago.Add(formaPago);

            var entity = CobranzasCommon.Guardar(cobrCartdto, usu, false);

            if (csvTemp.idPlanDeCuenta > 0)
                ContabilidadCommon.AgregarAsientoDeCobranza(usu, entity.IDCobranza);
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            //BasicLog.AppendToFile(ConfigurationManager.AppSettings["BasicLogError"], "GuardarCobranza" + msg, ex.ToString());
            throw new Exception(ex.Message);
        }
    }

    private static string ObtenerConceptos(FacturasCSVTmp facturas, WebUser usu, string resultado)
    {
        /* Items */
        if (string.IsNullOrWhiteSpace(resultado))
        {
            decimal importeNeto = 0;
            decimal importeTotal = Convert.ToDecimal(facturas.Total.Replace(".", ","));
            if (facturas.ImporteNeto.Trim() != "")
                importeNeto = Convert.ToDecimal(facturas.ImporteNeto.Replace(".", ","));

            var listaItems = new List<ComprobantesDetalleViewModel>();
            if (usu.CondicionIVA == "MO" || usu.CondicionIVA == "EX")
            {
                listaItems.Add(AgregarItem(importeNeto, importeNeto, 0, facturas.idPlanDeCuenta));
            }
            else if (usu.CondicionIVA == "RI")
            {
                var iva2700 = facturas.IVA2700 != string.Empty ? Convert.ToDecimal(facturas.IVA2700.Replace(".", ",")) : 0;
                var iva2100 = facturas.IVA2100 != string.Empty ? Convert.ToDecimal(facturas.IVA2100.Replace(".", ",")) : 0;
                var iva1050 = facturas.IVA1005 != string.Empty ? Convert.ToDecimal(facturas.IVA1005.Replace(".", ",")) : 0;
                var iva0500 = facturas.IVA0500 != string.Empty ? Convert.ToDecimal(facturas.IVA0500.Replace(".", ",")) : 0;
                var iva2050 = facturas.IVA0205 != string.Empty ? Convert.ToDecimal(facturas.IVA0205.Replace(".", ",")) : 0;
                var iva0000 = facturas.IVA0000 != string.Empty ? Convert.ToDecimal(facturas.IVA0000.Replace(".", ",")) : 0;
                var ivaNoGrav = facturas.ImporteNoGrabado != string.Empty ? Convert.ToDecimal(facturas.ImporteNoGrabado.Replace(".", ",")) : 0;

                //decimal importeParaCalc = (importeNeto - ivaNoGrav);

                if (iva2700 > 0)
                    listaItems.Add(AgregarItem((importeTotal / 1.27M), iva2700, 27, facturas.idPlanDeCuenta));
                if (iva2100 > 0)
                    listaItems.Add(AgregarItem((importeTotal / 1.21M), iva2100, 21, facturas.idPlanDeCuenta));
                if (iva1050 > 0)
                    listaItems.Add(AgregarItem((importeTotal / 1.105M), iva1050, 10.5M, facturas.idPlanDeCuenta));
                if (iva0500 > 0)
                    listaItems.Add(AgregarItem((importeTotal / 1.05M), iva0500, 5, facturas.idPlanDeCuenta));
                if (iva2050 > 0)
                    listaItems.Add(AgregarItem((importeTotal / 1.27M), iva2050, 2.5M, facturas.idPlanDeCuenta));
                if (iva0000 > 0)
                    listaItems.Add(AgregarItem(importeTotal, iva0000, 0, facturas.idPlanDeCuenta));
                if (ivaNoGrav > 0)
                    listaItems.Add(AgregarItem(importeNeto, ivaNoGrav, 0, facturas.idPlanDeCuenta));
            }

            /* percepciones */
            decimal PercepcionesIIBB = facturas.PercepcionesIIBB != string.Empty ? Convert.ToDecimal(facturas.PercepcionesIIBB.Replace(".", ",")) : 0;
            decimal PercepcionesIVA = facturas.PercepcionesIVA != string.Empty ? Convert.ToDecimal(facturas.PercepcionesIVA.Replace(".", ",")) : 0;
            decimal ImporteNoGrabado = facturas.ImporteNoGrabado != string.Empty ? Convert.ToDecimal(facturas.ImporteNoGrabado.Replace(".", ",")) : 0;


            decimal PIIBB = (importeNeto * PercepcionesIIBB) / 100;
            decimal PIVA = (importeNeto * PercepcionesIVA) / 100;
            decimal Tributos = PIIBB + PIVA + ImporteNoGrabado;

            //if (listaItems.Sum(x => x.TotalConIva) + Tributos != Convert.ToDecimal(facturas.Total))
            //    resultado += "No coinciden el importe total Con la suma de los conceptos, ";
            //else
            facturas.Items = listaItems;
        }
        return resultado;
    }

    private static ComprobantesDetalleViewModel AgregarItem(decimal importeTotal, decimal importeConIVA, decimal IVA, int idPlanDeCuenta)
    {
        decimal importe = Math.Abs(importeTotal);

        //if (IVA > 0)
        //    importe = importeConIVA * 100 / IVA;
        //else
        //    importe = Math.Abs(importeConIVA);

        var Item = new ComprobantesDetalleViewModel(2)
        {
            Codigo = "",
            PrecioUnitario = Math.Abs(importe),
            Iva = IVA,
            Concepto = "Concepto generado automáticamente por la importación masiva",
            Cantidad = 1,
            Bonificacion = 0,
            IDPlanDeCuenta = idPlanDeCuenta
        };

        return Item;
    }
    #endregion

    #region FACTURAS COMPRA

    public static List<FacturasCompraCSVTmp> LeerArchivoCSVFacturasCompra(WebUser usu, string path)
    {
        List<FacturasCompraCSVTmp> listaFacturasCSV = new List<FacturasCompraCSVTmp>();
        //FileHelperEngine engine = new FileHelperEngine(typeof(FacturasCompraCSV));
        //engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //FacturasCompraCSV[] res = (FacturasCompraCSV[])engine.ReadFile(path);
        var engine = new DelimitedFileEngine<FacturasCompraCSV>();
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        //engine.Options.Delimiter=";";
        FacturasCompraCSV[] res = (FacturasCompraCSV[])engine.ReadFile(path);

        if (res.Count() < 1)
        {
            engine.Options.Delimiter = ",";
            FacturasCompraCSV[] resaux = (FacturasCompraCSV[])engine.ReadFile(path);
            if (resaux.Count() > 0)
                res = resaux;
        }

        FacturasCompraCSVTmp facturasCSV;
        foreach (var FC in res)
        {
            if (FC.RazonSocial != string.Empty)
            {
                try
                {
                    facturasCSV = new FacturasCompraCSVTmp();
                    facturasCSV.IDUsuario = usu.IDUsuario;
                    facturasCSV.Tipo = "P";
                    facturasCSV.RazonSocial = FC.RazonSocial;
                    facturasCSV.NombreFantasia = FC.NombreFantasia;
                    facturasCSV.Email = FC.Email;
                    facturasCSV.TipoDocumento = FC.TipoDocumento;
                    facturasCSV.NroDocumento = FC.NroDocumento.ReplaceAll("-", "").ReplaceAll(" ", "").Trim();
                    facturasCSV.CondicionIva = FC.CondicionIva;
                    if (facturasCSV.NroDocumento.Length > 8)
                        facturasCSV.Personeria = (facturasCSV.NroDocumento.Substring(0, 2) == "30") ? "J" : "F";
                    else
                        facturasCSV.Personeria = "F";

                    if (FC.Provincia.ToUpper().Contains("CIUDAD") || FC.Provincia.ToUpper().Contains("CABA")
                         || FC.Provincia.ToUpper().Contains("C.A.B.A") || FC.Provincia.ToUpper().Contains("FEDERAL"))
                        facturasCSV.Provincia = "Ciudad de Buenos Aires";
                    else
                        facturasCSV.Provincia = FC.Provincia;
                    facturasCSV.Ciudad = FC.Ciudad;
                    facturasCSV.Domicilio = FC.Domicilio;

                    facturasCSV.Web = FC.Web;

                    facturasCSV.FechaEmision = FC.FechaEmision;
                    facturasCSV.FechaContable = FC.FechaContable;
                    facturasCSV.TipoComprobante = FC.TipoComprobante.ToUpper();
                    facturasCSV.PuntoDeVenta = FC.PuntoDeVenta;
                    facturasCSV.NroComprobante = FC.NroComprobante;

                    facturasCSV.ImporteMonotributo = FC.ImporteMonotributo;
                    facturasCSV.Gravado2700 = FC.Gravado2700;
                    facturasCSV.Gravado2100 = FC.Gravado2100;
                    facturasCSV.Gravado1005 = FC.Gravado1005;
                    facturasCSV.NoGravado = FC.NoGravado;
                    facturasCSV.Exento = FC.Exento;
                    facturasCSV.PercepcionesIVA = FC.PercepcionesIVA;
                    facturasCSV.ImpuestosNacionales = FC.ImpuestosNacionales;
                    facturasCSV.ImpuestosMunicipales = FC.ImpuestosMunicipales;
                    facturasCSV.ImpuestosInternos = FC.ImpuestosInternos;
                    facturasCSV.Otros = FC.Otros;
                    facturasCSV.Categoria = FC.Categoria;
                    facturasCSV.Rubro = FC.Rubro;

                    facturasCSV.FormaDePago = FC.FormaDePago;
                    facturasCSV.Banco = FC.Banco;
                    facturasCSV.Caja = FC.Caja;

                    facturasCSV.MontoPagado = FC.MontoPagado;
                    facturasCSV.FechaDePago = FC.FechaDePago;
                    facturasCSV.CodigoCuentaContable = FC.CodigoCuentaContable;
                    facturasCSV.Observaciones = FC.Observaciones;

                    facturasCSV.resultados = ValidarFacturasCompras(facturasCSV, usu);
                    //facturasCSV.resultados = ObtenerConceptos(facturasCSV, usu, facturasCSV.resultados);

                    if (listaFacturasCSV.Any(x => x.NroComprobante == FC.NroComprobante && x.TipoComprobante == FC.TipoComprobante && x.PuntoDeVenta == FC.PuntoDeVenta && x.NroDocumento == FC.NroDocumento))
                        facturasCSV.resultados += "El Comprobante se encuentra repetido en la lista, ";
                    facturasCSV.Estado = facturasCSV.resultados == string.Empty ? "A" : "I";

                    if (facturasCSV.resultados == string.Empty)
                        facturasCSV.resultados = "<span class='label label-success'>OK</span>";
                    else
                        facturasCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + facturasCSV.resultados.Substring(0, facturasCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

                    listaFacturasCSV.Add(facturasCSV);
                }
                catch (Exception ex)
                {
                    var msg = ex.InnerException;
                }
            }
        }

        return listaFacturasCSV.ToList();
    }

    private static string ValidarFacturasCompras(FacturasCompraCSVTmp facturas, WebUser usu)
    {
        string resultado = string.Empty;

        #region/*Obligatorios*/
        //resultado += (string.IsNullOrEmpty(facturas.RazonSocial)) ? "El campo Razón Social es obligatorio , " : "";
        if (facturas.CondicionIva != "CF")
            resultado += (string.IsNullOrEmpty(facturas.NroDocumento)) ? "El campo Nro Documento es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.CondicionIva)) ? "El campo Condición Iva es obligatorio , " : "";

        resultado += (string.IsNullOrEmpty(facturas.FechaEmision)) ? "El campo Fecha Emisión es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.FechaContable)) ? "El campo Fecha Contable es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.TipoComprobante)) ? "El campo Tipo de Comprobante es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.PuntoDeVenta)) ? "El campo Puntode Venta es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(facturas.NroComprobante)) ? "El campo Nro Comprobante es obligatorio , " : "";
        if (facturas.MontoPagado != "" && facturas.MontoPagado != "0")
            resultado += (string.IsNullOrEmpty(facturas.FechaDePago)) ? "El campo Fecha de Pago es obligatorio , " : "";

        var fecha = new DateTime();
        if (!DateTime.TryParse(facturas.FechaEmision, out fecha))
            resultado += "El campo Fecha Emisión no tiene un formato válido , ";

        if (!DateTime.TryParse(facturas.FechaContable, out fecha))
            resultado += "El campo Fecha Contable no tiene un formato válido , ";

        if (facturas.MontoPagado.Trim() != "")
        {
            var FechaDePago = new DateTime();
            if (!DateTime.TryParse(facturas.FechaDePago, out FechaDePago))
                resultado += "El campo fecha de pago no tiene un formato válido , ";
        }
        #endregion

        #region  /*longitud  obligatoria*/
        resultado += (facturas.TipoComprobante.Length > 3) ? "El campo Tipo Comprobante supera los 3 carácteres , " : "";
        resultado += (facturas.PuntoDeVenta.Length > 4) ? "El campo Punto de Venta supera los 8 carácteres , " : "";
        resultado += (facturas.NroComprobante.Length > 8) ? "El campo Nro Comprobante supera los 8 carácteres , " : "";

        resultado += (facturas.ImporteMonotributo.Length > 18) ? "El campo Importe Monotributo supera los 18 carácteres , " : "";
        resultado += (facturas.Gravado2700.Length > 18) ? "El campo Gravado al 27 supera los 18 carácteres , " : "";
        resultado += (facturas.Gravado2100.Length > 18) ? "El campo Gravado al 21 supera los 18 carácteres , " : "";
        resultado += (facturas.Gravado1005.Length > 18) ? "El campo Gravado al 10.5 supera los 18 carácteres , " : "";
        resultado += (facturas.NoGravado.Length > 18) ? "El campo No Gravado supera los 18 carácteres , " : "";
        resultado += (facturas.Exento.Length > 18) ? "El campo Exento supera los 18 carácteres , " : "";
        resultado += (facturas.PercepcionesIVA.Length > 18) ? "El campo Percepciones IVA supera los 18 carácteres , " : "";
        resultado += (facturas.ImpuestosNacionales.Length > 18) ? "El campo Impuestos Nacionales supera los 18 carácteres , " : "";
        resultado += (facturas.ImpuestosMunicipales.Length > 18) ? "El campo Impuestos Municipales supera los 18 carácteres , " : "";
        resultado += (facturas.ImpuestosInternos.Length > 18) ? "El campo Impuestos Internos supera los 18 carácteres , " : "";
        resultado += (facturas.Otros.Length > 18) ? "El campo Otros supera los 18 carácteres , " : "";
        #endregion

        #region/* Numeros*/
        decimal importeMon = 0;
        if (facturas.ImporteMonotributo.Trim() != "")
        {
            decimal.TryParse(facturas.ImporteMonotributo.Replace(".", ","), out importeMon);
            if (importeMon == 0 && facturas.ImporteMonotributo != "0")
                resultado += "El campo Importe Monotributo contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }


        decimal Gravado2700 = 0;
        if (facturas.Gravado2700.Trim() != "")
        {
            decimal.TryParse(facturas.Gravado2700.Replace(".", ","), out Gravado2700);
            if (Gravado2700 == 0 && facturas.Gravado2700 != "0")
                resultado += "El campo Gravado al 27 contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal Gravado2100 = 0;
        if (facturas.Gravado2100.Trim() != "")
        {
            decimal.TryParse(facturas.Gravado2100.Replace(".", ","), out Gravado2100);
            if (Gravado2100 == 0 && facturas.Gravado2100 != "0")
                resultado += "El campo Gravado al 21 contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal Gravado1005 = 0;
        if (facturas.Gravado1005.Trim() != "")
        {
            decimal.TryParse(facturas.Gravado1005.Replace(".", ","), out Gravado1005);
            if (Gravado1005 == 0 && facturas.Gravado1005 != "0")
                resultado += "El campo Gravado al 10.5 contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal NoGravado = 0;
        if (facturas.NoGravado.Trim() != "")
        {
            decimal.TryParse(facturas.NoGravado.Replace(".", ","), out NoGravado);
            if (NoGravado == 0 && facturas.NoGravado != "0")
                resultado += "El campo No Gravado contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }


        decimal Exento = 0;
        if (facturas.Exento.Trim() != "")
        {
            decimal.TryParse(facturas.Exento.Replace(".", ","), out Exento);
            if (Exento == 0 && facturas.Exento != "0")
                resultado += "El campo Exento contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal PercepcionesIVA = 0;
        if (facturas.PercepcionesIVA.Trim() != "")
        {
            decimal.TryParse(facturas.PercepcionesIVA.Replace(".", ","), out PercepcionesIVA);
            if (PercepcionesIVA == 0 && facturas.PercepcionesIVA != "0")
                resultado += "El campo PercepcionesIVA contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal ImpuestosNacionales = 0;
        if (facturas.ImpuestosNacionales.Trim() != "")
        {
            decimal.TryParse(facturas.ImpuestosNacionales.Replace(".", ","), out ImpuestosNacionales);
            if (ImpuestosNacionales == 0 && facturas.ImpuestosNacionales != "0")
                resultado += "El campo Impuestos Nacionales contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal ImpuestosMunicipales = 0;
        if (facturas.ImpuestosMunicipales.Trim() != "")
        {
            decimal.TryParse(facturas.ImpuestosMunicipales.Replace(".", ","), out ImpuestosMunicipales);
            if (ImpuestosMunicipales == 0 && facturas.ImpuestosMunicipales != "0")
                resultado += "El campo Impuestos Municipales contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal ImpuestosInternos = 0;
        if (facturas.ImpuestosInternos.Trim() != "")
        {
            decimal.TryParse(facturas.ImpuestosInternos.Replace(".", ","), out ImpuestosInternos);
            if (ImpuestosInternos == 0 && facturas.ImpuestosInternos != "0")
                resultado += "El campo Impuestos Internos contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal Otros = 0;
        if (facturas.Otros.Trim() != "")
        {
            decimal.TryParse(facturas.Otros.Replace(".", ","), out Otros);
            if (Otros == 0 && facturas.Otros != "0")
                resultado += "El campo Otros contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal MontoPagado = 0;
        if (facturas.MontoPagado.Trim() != "")
        {
            decimal.TryParse(facturas.MontoPagado.Replace(".", ","), out MontoPagado);
            if (MontoPagado == 0 && facturas.MontoPagado != "0")
                resultado += "El campo Monto pagado contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }
        #endregion

        #region otras logicas
        switch (facturas.CondicionIva.Trim())
        {
            case "EX":
            case "CF":
            case "MO":
                resultado += (facturas.TipoComprobante != "TIC" && facturas.TipoComprobante != "FCC" && facturas.TipoComprobante != "FCB" && facturas.TipoComprobante != "NCC" && facturas.TipoComprobante != "NCB" && facturas.TipoComprobante != "NDC" && facturas.TipoComprobante != "NDB" && facturas.TipoComprobante != "COT") ? "El campo Tipo Comprobante sólo puede tener los siguientes valores: FCC,FCB,NCC,NCB,NDC,NDB,TIC,COT, " : "";
                break;
            case "RI":
                if (usu.CondicionIVA == "RI")
                    resultado += (facturas.TipoComprobante != "TIC" && facturas.TipoComprobante != "FCA" && facturas.TipoComprobante != "FCB" && facturas.TipoComprobante != "NCA" && facturas.TipoComprobante != "NCB" && facturas.TipoComprobante != "NDA" && facturas.TipoComprobante != "NDB" && facturas.TipoComprobante != "COT") ? "El campo Tipo Comprobante sólo puede tener los siguientes valores: FCA,FCB,NCA,NCB,NDA,NDB,TIC,COT, " : "";
                else if (usu.CondicionIVA == "MO" || usu.CondicionIVA == "EX")
                    resultado += (facturas.TipoComprobante != "TIC" && facturas.TipoComprobante != "FCC" && facturas.TipoComprobante != "NCC" && facturas.TipoComprobante != "NDC" && facturas.TipoComprobante != "COT") ? "El campo Tipo Comprobante sólo puede tener los siguientes valores: FCC,NCC,NDC,TIC,COT, " : "";
                break;
        }

        if (facturas.FormaDePago != "")
        {
            string forma = facturas.FormaDePago.Trim().ToLower();
            if (forma != "transferencia" && forma != "efectivo" && forma != "deposito")
                resultado += "La forma de pago no es válida, ";
        }

        //Se elimina validacion porque hay medicos que son exentos de IVA

        //if (usu.CondicionIVA == "RI")
        //    resultado += (IVA0000 == 0 && IVA0205 == 0 && IVA0500 == 0 && IVA1005 == 0 && IVA2700 == 0 && IVA2100 == 0) ? "Su condición frente al IVA no admite productos sin IVA, " : "";
        //else
        //{
        //    resultado += (IVA0000 > 0 || IVA0205 > 0 || IVA0500 > 0 || IVA1005 > 0 || IVA2700 > 0 || IVA2100 > 0) ? "Su condición frente al IVA no admite productos con IVA, " : "";
        //    resultado += (PercepcionesIIBB > 0 || PercepcionesIIBB > 0 || ImporteNoGrabado > 0) ? "Su condición frente al IVA no admite comprobantes con percepciones, " : "";
        //}
        #endregion

        #region/*validaciones contra la db*/
        using (var dbContext = new ACHEEntities())
        {
            var numero = Convert.ToInt32(facturas.NroComprobante);
            var punto = Convert.ToInt32(facturas.PuntoDeVenta);

            //var puntoValido = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == usu.IDUsuario && x.Punto == punto && x.FechaBaja == null).FirstOrDefault();
            //var documento = dbContext.Comprobantes.Where(x => x.Tipo.ToUpper() == facturas.TipoComprobante.ToUpper() && x.PuntosDeVenta.Punto == punto && x.Numero == numero && x.IDUsuario == usu.IDUsuario).Any();
            var Provincia = dbContext.Provincias.Where(x => x.Nombre.ToUpper() == facturas.Provincia.ToUpper()).FirstOrDefault();
            var ciudad = dbContext.Ciudades.Where(x => x.Nombre.ToUpper() == facturas.Ciudad.ToUpper()).FirstOrDefault();

            /*PlanDeCuentas planDeCuenta = new PlanDeCuentas();
            if (usu.UsaPlanCorporativo)
            {
                planDeCuenta = dbContext.PlanDeCuentas.Where(x => x.Codigo == facturas.CodigoCuentaContable && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                var configPlanDeCuenta = dbContext.ConfiguracionPlanDeCuenta.Any(x => x.IDUsuario == usu.IDUsuario);
                resultado += (!configPlanDeCuenta) ? "No tiene configurado el plan de cuentas, " : "";
            }*/

            //resultado += (documento) ? "El Comprobante ingresado ya existe, " : "";

            /*if (puntoValido == null)
                resultado += "El punto de venta ingresado no es correcto, ";
            else
                facturas.IDPuntoDeVenta = puntoValido.IDPuntoVenta;*/

            if (Provincia == null)
                resultado += "La Provincia ingresada no es correcta, ";
            else
                facturas.IDProvincia = Provincia.IDProvincia;

            if (ciudad != null)
                facturas.IDCiudad = ciudad.IDCiudad;

            if (facturas.Banco != "")
            {
                if (!dbContext.Bancos.Any(x => x.IDUsuario == usu.IDUsuario && x.NroCuenta.Trim().ToLower() == facturas.Banco.Trim().ToLower()))
                    resultado += "El Banco ingresado no es correcto, ";
            }

            if (facturas.Caja != "")
            {
                if (!dbContext.Cajas.Any(x => x.IDUsuario == usu.IDUsuario && x.Nombre.Trim().ToLower() == facturas.Caja.Trim().ToLower()))
                    resultado += "La Caja ingresada no es correcta, ";
            }

            /*if (usu.UsaPlanCorporativo)
            {
                if (planDeCuenta == null)
                    resultado += "El código de la cuenta contable ingresado no es correcta, ";
                else
                    facturas.idPlanDeCuenta = planDeCuenta.IDPlanDeCuenta;
            }*/
        }
        #endregion
        return resultado;
    }

    public static void RealizarImportacionFacturasCompra(List<FacturasCompraCSVTmp> lista, string ACHEString, int idUsuario)
    {
        if (lista != null && lista.Any(x => x.Estado == "A"))
        {
            //IMPORTAR
            DataTable dt = new DataTable();

            dt.Columns.Add("IDUsuario", typeof(Int32));//0
            dt.Columns.Add("Tipo", typeof(string));//0

            dt.Columns.Add("RazonSocial", typeof(string));//1
            dt.Columns.Add("NombreFantasia", typeof(string));//2
            dt.Columns.Add("Email", typeof(string));//3
            dt.Columns.Add("TipoDocumento", typeof(string));//4
            dt.Columns.Add("NroDocumento", typeof(string));//5

            dt.Columns.Add("CondicionIva", typeof(string));//6
            dt.Columns.Add("Provincia", typeof(Int32));//7
            dt.Columns.Add("Ciudad", typeof(Int32));//8
            dt.Columns.Add("Domicilio", typeof(string));//9
            dt.Columns.Add("Web", typeof(string));//10
            dt.Columns.Add("Personeria", typeof(string));//10

            foreach (var facturas in lista.Where(x => x.Estado == "A").ToList())
            {
                #region Armo DataRow
                try
                {
                    DataRow drNew = dt.NewRow();
                    drNew[0] = facturas.IDUsuario;
                    drNew[1] = facturas.Tipo;

                    drNew[2] = facturas.RazonSocial;
                    drNew[3] = facturas.NombreFantasia;
                    drNew[4] = facturas.Email;
                    drNew[5] = facturas.TipoDocumento;
                    drNew[6] = facturas.NroDocumento;

                    drNew[7] = facturas.CondicionIva;
                    drNew[8] = facturas.IDProvincia;
                    drNew[9] = facturas.IDCiudad;
                    drNew[10] = facturas.Domicilio;
                    drNew[11] = facturas.Web;
                    drNew[12] = facturas.Personeria;
                    dt.Rows.Add(drNew);

                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
                #endregion
            }

            if (dt.Rows.Count > 0)
            {
                using (var sqlConnection = new SqlConnection(ACHEString))
                {
                    sqlConnection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = "PersonasTemp";
                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
            }

            using (var dbContext = new ACHEEntities())
            {
                dbContext.ObtenerDatosPersonasTmp(idUsuario);

            }
        }
        else
            throw new Exception("No se encontraron datos para importar");
    }

    public static void ReferenciarFacturasComprasClientes(List<FacturasCompraCSVTmp> listaFacturas, WebUser usu)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                var listaPersonas = dbContext.Personas.Where(x => x.IDUsuario == usu.IDUsuario).ToList();
                List<FacturasCompraCSVTmp> facturas = new List<FacturasCompraCSVTmp>();
                foreach (var item in listaPersonas)
                {
                    if (!string.IsNullOrWhiteSpace(item.NroDocumento))
                        facturas = listaFacturas.Where(x => x.TipoDocumento.ToUpper() == item.TipoDocumento && x.NroDocumento == item.NroDocumento && x.Estado == "A").ToList();
                    else
                        facturas = listaFacturas.Where(x => x.RazonSocial.ToUpper() == item.RazonSocial.ToUpper() && x.Estado == "A").ToList();

                    foreach (var fc in facturas)
                        fc.IDPersona = item.IDPersona;

                    if (facturas.Any())
                        GuardarFacturasCompras(dbContext, facturas, usu);
                }
            }
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    private static void GuardarFacturasCompras(ACHEEntities dbContext, List<FacturasCompraCSVTmp> listaFacturas, WebUser usu)
    {
        try
        {
            foreach (var item in listaFacturas.Where(x => x.Estado == "A"))
            {
                ComprasDto compCart = new ComprasDto();
                compCart.IDCompra = 0;

                compCart.IDPersona = item.IDPersona;
                compCart.Tipo = item.TipoComprobante;
                compCart.IDUsuario = item.IDUsuario;
                compCart.FechaEmision = item.FechaEmision;
                compCart.Fecha = item.FechaContable;
                compCart.FechaPrimerVencimiento = item.FechaEmision;
                compCart.NroFactura = int.Parse(item.PuntoDeVenta).ToString("#0000") + "-" + int.Parse(item.NroComprobante).ToString("#00000000");
                compCart.Obs = item.Observaciones;
                compCart.EmailUsuario = usu.Email;
                if (compCart.Tipo != "FCC" && compCart.Tipo != "NCC" && compCart.Tipo != "NDC")
                {
                    compCart.Importe27 = item.Gravado2700 != string.Empty ? item.Gravado2700.Replace(".", ",") : "0";
                    compCart.Importe21 = item.Gravado2100 != string.Empty ? item.Gravado2100.Replace(".", ",") : "0";
                    compCart.Importe10 = item.Gravado1005 != string.Empty ? item.Gravado1005.Replace(".", ",") : "0";
                    //compCart.Importe5 = item.Gravado1005 != string.Empty ? item.Gravado1005.Replace(".", ",") : "0";
                    //compCart.Importe = item.Gravado1005 != string.Empty ? item.Gravado1005.Replace(".", ",") : "0";
                    compCart.ImporteMon = "0";
                }
                else if (compCart.Tipo == "COT")
                {
                    compCart.Importe27 = item.Gravado2700 != string.Empty ? item.Gravado2700.Replace(".", ",") : "0";
                    compCart.Importe21 = item.Gravado2100 != string.Empty ? item.Gravado2100.Replace(".", ",") : "0";
                    compCart.Importe10 = item.Gravado1005 != string.Empty ? item.Gravado1005.Replace(".", ",") : "0";
                    compCart.ImporteMon = item.ImporteMonotributo != string.Empty ? item.ImporteMonotributo.Replace(".", ",") : "0";
                }
                else
                {
                    compCart.Importe27 = "0";
                    compCart.Importe21 = "0";
                    compCart.Importe10 = "0";
                    compCart.ImporteMon = item.ImporteMonotributo != string.Empty ? item.ImporteMonotributo.Replace(".", ",") : "0";
                }

                compCart.Importe5 = "0";
                compCart.Importe2 = "0";
                compCart.NoGrav = item.NoGravado != string.Empty ? item.NoGravado.Replace(".", ",") : "0";
                compCart.Exento = item.Exento != string.Empty ? item.Exento.Replace(".", ",") : "0";
                compCart.PercepcionIva = item.PercepcionesIVA != string.Empty ? item.PercepcionesIVA.Replace(".", ",") : "0";
                compCart.ImpNacional = item.ImpuestosNacionales != string.Empty ? item.ImpuestosNacionales.Replace(".", ",") : "0";
                compCart.ImpMunicipal = item.ImpuestosMunicipales != string.Empty ? item.ImpuestosMunicipales.Replace(".", ",") : "0";
                compCart.ImpInterno = item.ImpuestosInternos != string.Empty ? item.ImpuestosInternos.Replace(".", ",") : "0";
                compCart.Otros = item.Otros != string.Empty ? item.Otros.Replace(".", ",") : "0";
                compCart.Detallado = false;
                compCart.IdCategoria = "0";
                compCart.Rubro = "";
                compCart.IdPlanDeCuenta = 0;

                if (!string.IsNullOrEmpty(item.CodigoCuentaContable))
                {
                    var planDeCuentas = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.Codigo == item.CodigoCuentaContable).FirstOrDefault();
                    if (planDeCuentas != null)
                        compCart.IdPlanDeCuenta = planDeCuentas.IDPlanDeCuenta;
                }

                decimal iva = 0;
                if (compCart.Importe27 != "0")
                    iva += decimal.Parse(compCart.Importe27) * 0.27M;
                if (compCart.Importe21 != "0")
                    iva += decimal.Parse(compCart.Importe21) * 0.21M;
                if (compCart.Importe10 != "0")
                    iva += decimal.Parse(compCart.Importe10) * 0.105M;

                compCart.Iva = iva.ToString();

                //compCart.Items = item.Items;

                var compraExiste = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.Tipo == item.TipoComprobante && x.IDPersona == item.IDPersona && x.NroFactura == compCart.NroFactura).FirstOrDefault();
                if (compraExiste != null)
                    compCart.IDCompra = compraExiste.IDCompra;

                try
                {
                    var entity = ComprasCommon.Guardar(compCart);
                    if (compCart.IdPlanDeCuenta > 0)
                        ContabilidadCommon.AgregarAsientoDeCompra(entity.IDCompra, usu);

                    if (item.MontoPagado != string.Empty && Convert.ToDecimal(item.MontoPagado) > 0)
                    {
                        GuardarPago(dbContext, entity, item, usu);
                    }
                }
                catch (Exception ex)
                { }
            }
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;

            //BasicLog.AppendToFile(ConfigurationManager.AppSettings["PathBaseWeb"] + ConfigurationManager.AppSettings["BasicLogError"].Replace("~", ""), "GuardarFacturas" + msg, ex.ToString());
            throw new Exception(ex.Message);
        }
    }

    private static void GuardarPago(ACHEEntities dbContext, Compras comp, FacturasCompraCSVTmp csvTemp, WebUser usu)
    {
        try
        {
            //Determinamos cantidad de decimales que posee configurado el usuario
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            PagosCartDto pagCartdto = new PagosCartDto();
            pagCartdto.IDPago = 0;
            pagCartdto.IDPersona = csvTemp.IDPersona;
            pagCartdto.FechaPago = csvTemp.FechaDePago != "" ? csvTemp.FechaDePago : DateTime.Now.Date.ToString("dd/MM/yyyy");
            pagCartdto.Observaciones = "";


            pagCartdto.Items = new List<PagosDetalleViewModel>();
            pagCartdto.FormasDePago = new List<PagosFormasDePagoViewModel>();
            pagCartdto.Retenciones = new List<PagosRetencionesViewModel>();

            pagCartdto.Items.Add(new PagosDetalleViewModel(cantidadDecimales)
            {
                ID = 1,
                nroFactura = comp.NroFactura,
                Importe = Convert.ToDecimal(csvTemp.MontoPagado.Replace(".", ",")),
                IDCompra = comp.IDCompra
            });

            PagosFormasDePagoViewModel formaPago = new PagosFormasDePagoViewModel();
            formaPago.ID = 1;
            formaPago.Importe = Convert.ToDecimal(csvTemp.MontoPagado.Replace(".", ","));
            formaPago.FormaDePago = csvTemp.FormaDePago.Trim();
            formaPago.NroReferencia = "";
            if (csvTemp.FormaDePago.Trim().ToLower() == "transferencia" || csvTemp.FormaDePago.Trim().ToLower() == "deposito")
                formaPago.IDBanco = dbContext.Bancos.First(x => x.IDUsuario == usu.IDUsuario && x.NroCuenta == csvTemp.Banco).IDBanco;
            else if (csvTemp.FormaDePago.Trim().ToLower() == "efectivo")
                formaPago.IDCaja = dbContext.Cajas.First(x => x.IDUsuario == usu.IDUsuario && x.Nombre == csvTemp.Caja).IDCaja;

            pagCartdto.FormasDePago.Add(formaPago);

            if (comp.IDCompra > 0)
            {
                PagosCommon.Eliminar(comp.IDCompra);
            }

            var entity = PagosCommon.Guardar(pagCartdto, usu, false);
            if (comp.IDPlanDeCuenta > 0)
                ContabilidadCommon.AgregarAsientoDePago(usu, entity.IDPago);
        }
        catch (CustomException ex)
        {
            throw new CustomException(ex.Message);
        }
        catch (Exception ex)
        {
            var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            //BasicLog.AppendToFile(ConfigurationManager.AppSettings["BasicLogError"], "GuardarPago" + msg, ex.ToString());
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region COMPROBANTESDETALLE
    public static List<ComprobantesDetalleCSVTmp> LeerArchivoCSVComprobantesDetalle(WebUser usu, string path)
    {
        List<ComprobantesDetalleCSVTmp> listaComprobantesCSV = new List<ComprobantesDetalleCSVTmp>();
        FileHelperEngine engine = new FileHelperEngine(typeof(ComprobantesDetalleCSV));
        engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
        ComprobantesDetalleCSV[] res = (ComprobantesDetalleCSV[])engine.ReadFile(path);
        ComprobantesDetalleCSVTmp compCSV;

        foreach (var CD in res)
        {

            if (CD.RazonSocial != string.Empty)
            {
                try
                {


                    compCSV = new ComprobantesDetalleCSVTmp();


                    compCSV.IDUsuario = usu.IDUsuario;
                    compCSV.Tipo = CD.Tipo;
                    compCSV.Fecha = CD.Fecha;
                    compCSV.PeriodoDesde = CD.PeriodoDesde;
                    compCSV.PeriodoHasta = CD.PeriodoHasta;
                    compCSV.FechaVencimiento = CD.FechaVencimiento;
                    compCSV.PuntoDeVenta = CD.PuntoDeVenta;
                    compCSV.Numero = CD.Numero;
                    compCSV.RazonSocial = CD.RazonSocial;
                    compCSV.CondicionVenta = CD.CondicionVenta;

                    compCSV.CUIT = CD.CUIT;
                    //    ComprobantesDetalleViewModel item = new ComprobantesDetalleViewModel();
                    compCSV.Bonificacion = CD.Bonificacion;
                    compCSV.Cantidad = CD.Cantidad;
                    compCSV.Codigo = CD.Codigo;
                    compCSV.Concepto = CD.Concepto;
                    compCSV.PrecioUnitario = CD.PrecioUnitario.Replace(".", ",");
                    compCSV.IVA = CD.IVA;
                    //  items.Add(item);
                    compCSV.resultados = ValidarComprobantesDetalle(compCSV, usu);
                    //facturasCSV.resultados = ObtenerConceptos(facturasCSV, usu, facturasCSV.resultados);


                    compCSV.Estado = compCSV.resultados == string.Empty ? "A" : "I";

                    if (compCSV.resultados == string.Empty)
                        compCSV.resultados = "<span class='label label-success'>OK</span>";
                    else
                        compCSV.resultados = "<span class='label label-danger' data-toggle='tooltip' title='" + compCSV.resultados.Substring(0, compCSV.resultados.Length - 2) + ".'>ERROR. Ver detalle</span>";

                    listaComprobantesCSV.Add(compCSV);
                }
                catch (Exception ex)
                {
                    var msg = ex.InnerException;
                }

            }
        }




        return listaComprobantesCSV.ToList();
    }

    private static string ValidarComprobantesDetalle(ComprobantesDetalleCSVTmp comp, WebUser usu)
    {
        string resultado = string.Empty;

        #region/*Obligatorios*/
        //resultado += (string.IsNullOrEmpty(facturas.RazonSocial)) ? "El campo Razón Social es obligatorio , " : "";




        resultado += (string.IsNullOrEmpty(comp.Numero)) ? "El campo Numero es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.Tipo)) ? "El campo Tipo Comprobante es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.PuntoDeVenta)) ? "El campo Tipo Comprobante es obligatorio , " : "";

        resultado += (string.IsNullOrEmpty(comp.Fecha)) ? "El campo Fecha es obligatorio , " : "";

        resultado += (string.IsNullOrEmpty(comp.PeriodoDesde)) ? "El campo periodo desde es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.PeriodoHasta)) ? "El campo periodo hasta es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.FechaVencimiento)) ? "El campo Fecha de vencimiento es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.CUIT)) ? "El campo cuit es obligatorio , " : "";

        resultado += (string.IsNullOrEmpty(comp.RazonSocial)) ? "El campo razon social es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.CondicionVenta)) ? "El campo coondicion de venta es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.Codigo)) ? "El campo codigo es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.Concepto)) ? "El campo concepto es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.Cantidad)) ? "El campo cantidad es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.PrecioUnitario)) ? "El campo Precio unitario es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.Bonificacion)) ? "El campo Bonificacion es obligatorio , " : "";
        resultado += (string.IsNullOrEmpty(comp.IVA)) ? "El campo IVA es obligatorio , " : "";
        var fecha = new DateTime();
        if (!DateTime.TryParse(comp.Fecha, out fecha))
            resultado += "El campo Fecha no tiene un formato válido , ";


        var periodoD = new DateTime();
        if (!DateTime.TryParse(comp.PeriodoDesde, out periodoD))
            resultado += "El campo periodo desde no tiene un formato válido , ";

        var periodoH = new DateTime();
        if (!DateTime.TryParse(comp.PeriodoHasta, out periodoH))
            resultado += "El campo periodo hasta no tiene un formato válido , ";

        var fechaVenc = new DateTime();
        if (!DateTime.TryParse(comp.FechaVencimiento, out fechaVenc))
            resultado += "El campo Fecha vencimiento no tiene un formato válido , ";

        decimal iva;
        if (!Decimal.TryParse(comp.IVA, out iva))
            resultado += "El campo IVA vencimiento no tiene un formato válido , ";
        #endregion


        #region  /*longitud  obligatoria*/
        resultado += (comp.Tipo.Length > 3) ? "El campo Tipo Comprobante supera los 3 carácteres , " : "";
        resultado += (comp.PuntoDeVenta.Length > 4) ? "El campo Punto de Venta supera los 8 carácteres , " : "";
        resultado += (comp.Numero.Length > 8) ? "El campo Nro Comprobante supera los 8 carácteres , " : "";

        resultado += (comp.PrecioUnitario.Length > 18) ? "El campo precio unitario supera los 18 carácteres , " : "";

        resultado += (comp.IVA.Length > 18) ? "El campo IVA  supera los 18 carácteres , " : "";

        #endregion

        #region/* Numeros*/
        decimal Numero = 0;
        if (comp.Numero.Trim() != "")
        {
            decimal.TryParse(comp.Numero.Replace(".", ","), out Numero);
            if (Numero == 0 && comp.Numero != "0")
                resultado += "El campo Importe Neto contiene carácteres inválidos, ";
        }

        decimal Cantidad = 0;
        if (comp.Cantidad.Trim() != "")
        {
            decimal.TryParse(comp.Cantidad.Replace(".", ","), out Cantidad);
            if (Cantidad == 0 && comp.Cantidad != "0")
                resultado += "El campo Cantidad contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal PrecioUnitario = 0;
        if (comp.PrecioUnitario.Trim() != "")
        {
            decimal.TryParse(comp.PrecioUnitario.Replace(".", ","), out PrecioUnitario);
            if (PrecioUnitario == 0 && comp.PrecioUnitario != "0")
                resultado += "El campo PrecioUnitario No Grabado contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal Bonificacion = 0;
        if (comp.Bonificacion.Trim() != "")
        {
            decimal.TryParse(comp.Bonificacion.Replace(".", ","), out Bonificacion);
            if (Bonificacion == 0 && comp.Bonificacion != "0")
                resultado += "El campo Bonificacion contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }

        decimal IVA = 0;
        if (comp.IVA.Trim() != "")
        {
            decimal.TryParse(comp.IVA.Replace(".", ","), out IVA);
            if (IVA == 0 && comp.IVA != "0")
                resultado += "El campo IVA  contiene carácteres inválidos (NO utilice punto para separador de miles), ";
        }


        #endregion
        #region /* otras logicas*/
        if (comp.IVA != "")
        {
            var ivaAux = comp.IVA.Trim().Replace(".", ",");
            if (ivaAux != "0" && ivaAux != "2,5" && ivaAux != "5" && ivaAux != "10,5" && ivaAux != "21" && ivaAux != "27")
                resultado += "El IVA no es válido, ";
        }
        if (comp.Tipo != "")
        {
            var auxTipo = comp.Tipo.Trim();
            if (auxTipo.ToUpper() != "FCA" && auxTipo.ToUpper() != "FCB")
                resultado += "El Tipo de factura no es válido, ";
        }
        /* JC COBRANZA AUTOMATICA
         * if (comp.CondicionVenta != "")
         {
             var auxCondVenta = comp.CondicionVenta.Trim();
             if (auxCondVenta.ToLower() != "efectivo" && auxCondVenta.ToLower() != "cheque" && auxCondVenta.ToLower() != "cuenta corriente" && auxCondVenta.ToLower() != "mercado pago" && auxCondVenta.ToLower() != "tarjeta de debito" && auxCondVenta.ToLower() != "tarjeta de credito" && auxCondVenta.ToLower() != "ticket" && auxCondVenta.ToLower() != "payu" && auxCondVenta.ToLower() != "otro")
                 resultado += "La condicion de venta no es válido, ";
         }*/
        #endregion

        #region/*validaciones contra la db*/
        using (var dbContext = new ACHEEntities())
        {
            var punto = Convert.ToInt32(comp.PuntoDeVenta);
            var puntoValido = dbContext.PuntosDeVenta.Where(x => x.IDUsuario == usu.IDUsuario && x.Punto == punto && x.FechaBaja == null).FirstOrDefault();
            if (puntoValido == null)
                resultado += "El punto de venta ingresado no es correcto, ";
            else
                comp.IDPuntoDeVenta = puntoValido.IDPuntoVenta;

            var concepto = (comp.Codigo);
            var conceptoValido = dbContext.Conceptos.Where(x => x.IDUsuario == usu.IDUsuario && x.Codigo == concepto).FirstOrDefault();
            if (conceptoValido == null)
                resultado += "El concepto  no es correcto, ";
            else
            {
                comp.IDConcepto = conceptoValido.IDConcepto;
                comp.IDCuentaVenta = conceptoValido.IDPlanDeCuentaVentas ?? 0;
            }

            var cuit = comp.CUIT;
            var prov = dbContext.Personas.Where(x => x.NroDocumento == cuit && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

            if (prov == null)
                resultado += "El cliente no existe, ";
            else
                comp.IDPersona = prov.IDPersona;



            decimal precioUnit = 0;

            if (!string.IsNullOrEmpty(comp.PrecioUnitario))
                precioUnit = decimal.Parse(comp.PrecioUnitario.Replace(".", ","));

            if (precioUnit == 0)
                resultado += "El precio unitario no puede ser 0, ";

            var condValida = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, usu.IDUsuario, comp.CondicionVenta);
            if (condValida == null)
                resultado += "La condicion de venta ingresada no es correcta, ";
            else
                comp.IDCondicionDeVenta = condValida.IDCondicionVenta;

        }
        #endregion
        return resultado;
    }


    public static void RealizarImportacionComprobantes(List<ComprobantesDetalleCSVTmp> lista, string ACHEString)
    {

        if (lista != null && lista.Any(x => x.Estado == "A"))
        {
            int idusu = 0;
            //IMPORTAR
            DataTable dt = new DataTable();

            dt.Columns.Add("IDUsuario", typeof(Int32));//0
            dt.Columns.Add("NroFactura", typeof(Int32));//1
            dt.Columns.Add("TipoFactura", typeof(string));//1

            dt.Columns.Add("IDPuntoVenta", typeof(Int32));//2
            dt.Columns.Add("Fecha", typeof(string));//3
            dt.Columns.Add("PeriodoDesde", typeof(string));//4
            dt.Columns.Add("PeriodoHasta", typeof(string));//5
            dt.Columns.Add("FechaVencimiento", typeof(string));//6
            dt.Columns.Add("IDPersona", typeof(Int32));//7
            dt.Columns.Add("Cuit", typeof(string));//8

            dt.Columns.Add("RazonSocial", typeof(string));//9
            dt.Columns.Add("CondicionVenta", typeof(int));//10
            dt.Columns.Add("Codigo", typeof(string));//11
            dt.Columns.Add("Concepto", typeof(string));//12
            dt.Columns.Add("Cantidad", typeof(decimal));//13
            dt.Columns.Add("PrecioUnitario", typeof(decimal));//14
            dt.Columns.Add("Bonificacion", typeof(decimal));//15
            dt.Columns.Add("Iva", typeof(decimal));//16
            dt.Columns.Add("IDConcepto", typeof(decimal));//16
            dt.Columns.Add("IDCuentaVenta", typeof(int));//16

            foreach (var comp in lista.Where(x => x.Estado == "A").ToList())
            {
                #region Armo DataRow
                try
                {
                    DataRow drNew = dt.NewRow();
                    drNew[0] = comp.IDUsuario;
                    idusu = comp.IDUsuario;
                    drNew[1] = comp.Numero;
                    drNew[2] = comp.Tipo;

                    drNew[3] = comp.IDPuntoDeVenta;
                    drNew[4] = comp.Fecha;
                    drNew[5] = comp.PeriodoDesde;
                    drNew[6] = comp.PeriodoHasta;
                    drNew[7] = comp.FechaVencimiento;
                    drNew[8] = comp.IDPersona;
                    drNew[9] = comp.CUIT;

                    drNew[10] = comp.RazonSocial;
                    //JC COBRTANZA AUTOMATICA
                    drNew[11] = comp.IDCondicionDeVenta;
                    drNew[12] = comp.Codigo;
                    drNew[13] = comp.Concepto;
                    drNew[14] = comp.Cantidad.Replace(".", ",");
                    drNew[15] = comp.PrecioUnitario.Replace(".", ",");
                    drNew[16] = comp.Bonificacion.Replace(".", ",");
                    drNew[17] = comp.IVA.Replace(".", ",");
                    drNew[18] = comp.IDConcepto;
                    drNew[19] = comp.IDCuentaVenta;
                    dt.Rows.Add(drNew);

                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
                #endregion
            }

            if (dt.Rows.Count > 0)
            {
                using (var sqlConnection = new SqlConnection(ACHEString))
                {
                    sqlConnection.Open();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnection))
                    {
                        bulkCopy.DestinationTableName = "TempComprobantes";
                        foreach (DataColumn col in dt.Columns)
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                        bulkCopy.WriteToServer(dt);
                    }
                }
                ProcesarComprobantesTmp(idusu);
            }

        }
        else
            throw new Exception("No se encontraron datos para importar");

    }
    private static void ProcesarComprobantesTmp(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.InsertarComprobantes(idUsuario);
                EliminarComprobantesTmp(idUsuario);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al actualizar los comprobantes. Error: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
        }
    }
    private static void EliminarComprobantesTmp(int idUsuario)
    {
        try
        {
            using (var dbContext = new ACHEEntities())
                dbContext.deleteComprobantesDetalleTmp(idUsuario);
        }
        catch (Exception)
        {
            throw new Exception("Error al eliminar los datos de la tabla temporal");
        }
    }


    #endregion
}

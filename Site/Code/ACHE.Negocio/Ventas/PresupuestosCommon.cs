﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Model.Negocio;
using System.IO;
using System.Configuration;
using ACHE.Negocio.Helpers;
using ACHE.Negocio.Ventas;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Common;
using ACHE.FacturaElectronica;
using ACHE.FacturaElectronicaModelo;
using System.Data;
using System.Data.Entity;
using System.Web;


namespace ACHE.Negocio.Presupuesto
{
    public class PresupuestosCommon
    {
        public static int GuardarPresupuesto(PresupuestoCartDto comprobanteCart, int idUsuario, string emailUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.Presupuestos.Any(x => x.IDUsuario == idUsuario && x.Numero == comprobanteCart.Numero && x.IDPresupuesto != comprobanteCart.IDPresupuesto))
                        throw new CustomException("El Numero ingresado ya se encuentra registrado.");

                    Personas persona = dbContext.Personas.Where(x => x.IDPersona == comprobanteCart.IDPersona && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (persona == null)
                        throw new CustomException("El cliente/proveedor es inexistente");

                    Presupuestos entity;
                    if (comprobanteCart.IDPresupuesto > 0)
                    {
                        entity = dbContext.Presupuestos.Where(x => x.IDPresupuesto == comprobanteCart.IDPresupuesto && x.IDUsuario == idUsuario).FirstOrDefault();
                        entity.UsuarioModifica = emailUsuario;
                    }
                    else
                    {
                        entity = new Presupuestos();
                        //entity.FechaAlta = DateTime.Parse(comprobanteCart.Fecha;
                        entity.IDUsuario = idUsuario;
                        entity.Estado = "B";
                        entity.UsuarioAlta = emailUsuario;
                    }

                    entity.IDPersona = comprobanteCart.IDPersona;
                    entity.FechaValidez = DateTime.Parse(comprobanteCart.FechaValidez);
                    entity.FechaAlta = DateTime.Parse(comprobanteCart.Fecha);
                    entity.Nombre = comprobanteCart.Nombre.ToUpper();
                    entity.Estado = comprobanteCart.Estado;
                    entity.Descripcion = "";
                    entity.Numero = comprobanteCart.Numero;
                    entity.FormaDePago = ""; //sacar dsps
                    entity.IDCondicionVenta = comprobanteCart.IDCondicionVenta;

                    if (entity.IDCondicionVenta == null || entity.IDCondicionVenta == 0)
                    {
                        var condicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, idUsuario, comprobanteCart.CondicionesPago);
                        if (condicionVenta != null)
                            entity.IDCondicionVenta = condicionVenta.IDCondicionVenta;
                        else//fix
                            entity.IDCondicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, idUsuario, "Cuenta Corriente").IDCondicionVenta;
                    }

                    entity.Observaciones = comprobanteCart.Observaciones;
                    entity.Tipo = comprobanteCart.Tipo;

                    if (entity.PresupuestoDetalle.Any())
                        dbContext.PresupuestoDetalle.RemoveRange(entity.PresupuestoDetalle);

                    decimal totalNeto = 0;
                    decimal totalBruto = 0;

                    if (comprobanteCart.Items.Count == 0)
                        throw new CustomException("Ingrese al menos un producto o servicio, para realizar el presupuesto.");
                    foreach (var det in comprobanteCart.Items)
                    {
                        PresupuestoDetalle presupuestoDet = new PresupuestoDetalle();
                        presupuestoDet.IDPresupuesto = entity.IDPresupuesto;
                        presupuestoDet.PrecioUnitario = det.PrecioUnitario;
                        presupuestoDet.Iva = det.Iva;
                        presupuestoDet.Concepto = det.Concepto;
                        presupuestoDet.Cantidad = det.Cantidad;
                        presupuestoDet.Bonificacion = det.Bonificacion;
                        presupuestoDet.IDConcepto = det.IDConcepto;

                        totalNeto += det.TotalConIva;
                        totalBruto += det.TotalSinIva;

                        entity.PresupuestoDetalle.Add(presupuestoDet);
                    }

                    //TODO DECIMALES: SE REDONDEA A 2 DECIMALES
                    entity.ImporteTotalNeto = Math.Round(totalNeto, 2);
                    entity.ImporteTotalBruto = Math.Round(totalBruto, 2);

                    if (comprobanteCart.IDPresupuesto > 0)
                        dbContext.SaveChanges();
                    else
                    {
                        dbContext.Presupuestos.Add(entity);
                        dbContext.SaveChanges();
                    }

                    return entity.IDPresupuesto;
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool EliminarPresupuesto(int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Presupuestos.Where(x => x.IDPresupuesto == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        var archivo = entity.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_R-" + entity.Numero.ToString("#00000000") + ".pdf";
                        dbContext.Presupuestos.Remove(entity);
                        dbContext.SaveChanges();

                        var path = Path.Combine(ConfigurationManager.AppSettings["PathBaseWeb"] + "~/files/Presupuestos/" + idUsuario.ToString() + "/" + archivo);
                        if (File.Exists(path))
                            File.Delete(path);
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarItem(PresupuestoItemDto item, WebUser usu, byte cantDecimales)
        {
            int index = item.ID;
            if (item.ID != 0)
            {
                var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == item.ID).FirstOrDefault();
                ComprobanteCart.Retrieve().Items.Remove(aux);
            }
            else
                index = ComprobanteCart.Retrieve().Items.Count() + 1;

            var tra = new ComprobantesDetalleViewModel(cantDecimales);
            tra.ID = index;// ComprobanteCart.Retrieve().Items.Count() + 1;
            tra.Codigo = item.Codigo;
            tra.Concepto = item.Concepto;
            if (item.Iva != string.Empty && item.Iva != "null")
                tra.Iva = decimal.Parse(item.Iva);
            else
                tra.Iva = 0;
            if (item.Bonificacion != string.Empty)
                tra.Bonificacion = decimal.Parse(item.Bonificacion.Replace(".", ","));
            else
                tra.Bonificacion = 0;

            tra.PrecioUnitario = decimal.Parse(item.Precio.Replace(".", ","));
            tra.Cantidad = decimal.Parse(item.Cantidad.Replace(".", ",")); ;
            if (!string.IsNullOrEmpty(item.IDConcepto))
            {
                var prod = ConceptosCommon.GetConcepto(int.Parse(item.IDConcepto), usu.IDUsuario);

                tra.IDConcepto = int.Parse(item.IDConcepto);
                tra.TipoConcepto = prod.Tipo;
            }
            else
                tra.IDConcepto = null;

            tra.PrecioUnitario = ObtenerPrecioFinal(tra.PrecioUnitario, item.Iva, cantDecimales, usu.UsaPrecioFinalConIVA);//, idPersona);

            if (item.ID != 0)
                ComprobanteCart.Retrieve().Items.Insert(item.ID - 1, tra);
            else
                ComprobanteCart.Retrieve().Items.Add(tra);
        }

        private static decimal ObtenerPrecioFinal(decimal PrecioUnitario, string iva, byte cantDecimales, bool usaPrecioFinalConIva)
        {
            //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            //var UsaPrecioConIva = dbContext.Personas.Any(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersona && (x.CondicionIva == "MO" || x.CondicionIva == "CF"));
            //if (usu.UsaPrecioFinalConIVA || UsaPrecioConIva)
            if (usaPrecioFinalConIva)
                return ConceptosCommon.ObtenerPrecioFinal(PrecioUnitario, iva, cantDecimales);
            else
                return PrecioUnitario;
        }

        public static void EliminarItem(int id)
        {
            var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            if (aux != null)
                ComprobanteCart.Retrieve().Items.Remove(aux);
        }

        public static TotalesViewModel ObtenerTotales(byte cantDecimales)
        {
            TotalesViewModel totales = new TotalesViewModel();
            totales.Iva = ComprobanteCart.Retrieve().GetIva().ToMoneyFormat(cantDecimales);
            totales.Subtotal = ComprobanteCart.Retrieve().GetGravado().ToMoneyFormat(cantDecimales);
            totales.Total = ComprobanteCart.Retrieve().GetTotal().ToMoneyFormat(cantDecimales);
            totales.ImporteNoGrabado = ComprobanteCart.Retrieve().GetImporteNoGrabado().ToMoneyFormat(cantDecimales);

            return totales;

        }

        public static string ObtenerItems(byte cantDecimales, bool usaPrecioFinalConIva)
        {
            var html = string.Empty;
            using (var dbContext = new ACHEEntities())
            {
                var list = ComprobanteCart.Retrieve().Items.ToList();
                if (list.Any())
                {
                    int index = 1;
                    foreach (var detalle in list)
                    {
                        html += "<tr>";
                        html += "<td>" + index + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(cantDecimales) + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                        html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
                        html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(cantDecimales) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Bonificacion.ToMoneyFormat(cantDecimales) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.Iva.ToMoneyFormat(cantDecimales) + "</td>";
                        html += "<td style='text-align:right'>" + detalle.TotalConIva.ToMoneyFormat(cantDecimales) + "</td>";

                        decimal preciFinal = 0;
                        //var UsaPrecioConIva = dbContext.Personas.Any(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersona && (x.CondicionIva == "MO" || x.CondicionIva == "CF"));
                        //if (usu.UsaPrecioFinalConIVA || UsaPrecioConIva)
                        if (usaPrecioFinalConIva)
                            preciFinal = detalle.PrecioUnitarioConIvaSinBonificacion;
                        else
                            preciFinal = detalle.PrecioUnitario;
                        html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:Presupuestos.eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                        html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:Presupuestos.modificarItem("
                            + detalle.ID
                            + ", '" + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
                            + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(cantDecimales)
                            + "','" + detalle.Concepto
                            + "','" + preciFinal.ToMoneyFormatForEdit(cantDecimales)
                            + "','" + detalle.Iva.ToString("#0.00")
                            + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(cantDecimales)
                            + "');\"><i class='fa fa-edit'></i></a></td>";
                        html += "</tr>";

                        index++;
                    }
                }
            }
            if (html == "")
                html = "<tr><td colspan='8' style='text-align:center'>No tienes items agregados</td></tr>";

            return html;

        }

        public static string Previsualizar(PresupuestoPrevisualizarDto pre, WebUser usu)
        {

            var tipo = "PRE";
            var modo = "";
            var tipoConcepto = 1;
            //var fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
            var nroPresupuesto = pre.Numero.ToString("#00000000");

            //var punto = UsuarioCommon.ObtenerPuntosDeVenta(usu.IDUsuario).FirstOrDefault();
            var punto = UsuarioCommon.ObtenerPuntosDeVenta(usu).FirstOrDefault();
            if (!ComprobanteCart.Retrieve().Items.Any())
                throw new Exception("Debe Ingresar al menos un Items a presupuestar.");

            string afipObs = "";
            int idcondventa = int.Parse(pre.CondicionesPago);
            string condVenta = "";
            using (var dbContext = new ACHEEntities())
            {
                condVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, idcondventa).Nombre;
            }

            global::Common.CrearComprobante(usu, pre.ID, pre.IDPersona, tipo, modo, pre.Fecha, condVenta, tipoConcepto, pre.FechaVencimiento, punto.ID, ref nroPresupuesto, pre.Observaciones, global::Common.ComprobanteModo.Previsualizar, ref afipObs);

            return usu.IDUsuario + "_prev.pdf";

        }

        public static string GenerarRemito(WebUser usu, int id)
        {

            using (var dbContext = new ACHEEntities())
            {

                var entity = dbContext.Presupuestos.Include("PresupuestoDetalle").Include("Personas").Where(x => x.IDPresupuesto == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                var fileNameRemito = entity.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_" + "R-" + entity.Numero.ToString("#00000000") + ".pdf";
                var formadepago = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, entity.IDCondicionVenta).Nombre;
                //var pathRemito = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/remitos/" + fileNameRemito);

                //if (!System.IO.File.Exists(pathRemito))
                //{
                ComprobanteCart.Retrieve().Items.Clear();
                var contador = 0;
                foreach (var item in entity.PresupuestoDetalle)
                {
                    contador++;
                    var idconcepto = (item.IDConcepto == null) ? null : item.IDConcepto;
                    var tipoConcepto = (item.IDConcepto == null) ? null : item.Conceptos.Tipo;
                    AgregarItem(contador, idconcepto, item.Concepto, item.Iva, item.PrecioUnitario, item.Bonificacion, item.Cantidad, tipoConcepto, usu.IDUsuario);
                }
                global::Common.GenerarRemito(usu, entity.IDPersona, entity.FechaAlta.ToString("dd/MM/yyyy"), 0, entity.Numero.ToString(), formadepago, usu.ObservacionesRemito, null, FETipoComprobante.REMITO, usu.MostrarProductosEnRemito);
                //}

                return fileNameRemito;
            }
        }

        public static string GenerarPresupuesto(int id, WebUser usu)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Presupuestos.Include("PresupuestoDetalle").Include("Personas").Where(x => x.IDPresupuesto == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                var fileNamePresup = entity.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_" + "P-" + entity.Numero.ToString("#00000000") + ".pdf";

                //var pathRemito = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/presupuestos/" + fileNameRemito);
                //if (!System.IO.File.Exists(pathRemito))
                //{
                ComprobanteCart.Retrieve().Items.Clear();
                var contador = 0;
                foreach (var item in entity.PresupuestoDetalle)
                {
                    contador++;
                    var idconcepto = (item.IDConcepto == null) ? null : item.IDConcepto;
                    AgregarItem(contador, idconcepto, item.Concepto, item.Iva, item.PrecioUnitario, item.Bonificacion, item.Cantidad, "", usu.IDUsuario);
                }

                //var tipo = "PRE";
                //var modo = "";
                //var tipoConcepto = 1;
                var fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
                var nroPresupuesto = entity.Numero.ToString("#00000000");

                //var punto = UsuarioCommon.ObtenerPuntosDeVenta(usu.IDUsuario).FirstOrDefault();
                var punto = UsuarioCommon.ObtenerPuntosDeVenta(usu).FirstOrDefault();

                //Common.CrearComprobante(usu, id, entity.IDPersona, tipo, modo, fecha, entity.FormaDePago, tipoConcepto, entity.FechaAlta.ToString("dd/MM/yyyy"), punto.ID, ref nroPresupuesto, entity.Observaciones, Common.ComprobanteModo.Previsualizar);
                var formadepago = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, entity.IDCondicionVenta).Nombre;

                global::Common.GenerarRemito(usu, entity.IDPersona, entity.FechaAlta.ToString("dd/MM/yyyy"), 0, entity.Numero.ToString(), formadepago, entity.Observaciones, entity.FechaValidez, FETipoComprobante.PRESUPUESTO, true);
                //string afipObs = "";
                //Common.CrearComprobante(usu, id, idPersona, tipo, modo, fecha, condicionesPago, tipoConcepto, fechaVencimiento, punto.ID, ref nroPresupuesto, obs, Common.ComprobanteModo.Previsualizar, ref afipObs);

                //}
                return fileNamePresup;
            }
        }

        public static string ExportarPresupuestos(int idUsuario, string condicion, string periodo, string fechaDesde, string fechaHasta)
        {
            string fileName = "Presupuestos_" + idUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.Presupuestos.Include("Personas").Where(x => x.IDUsuario == idUsuario).AsQueryable();
                    Int32 numero = 0;
                    if (Int32.TryParse(condicion, out numero))
                    {
                        results = results.Where(x => x.Numero == numero);
                    }
                    else if (condicion != string.Empty)
                    {
                        results = results.Where(x => x.Personas.RazonSocial.Contains(condicion) || x.Personas.NombreFantansia.Contains(condicion));
                    }

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaAlta >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.FechaAlta) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    dt = results.OrderByDescending(x => x.FechaAlta).ToList().Select(x => new
                    {
                        Numero = x.Numero.ToString("#00000000"),
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        Fecha = x.FechaAlta.ToString("dd/MM/yyyy"),
                        Estado = PresupuestosCommon.ObtenerEstadoDesc(x.Estado),// == "A" ? "Aprobado" : (x.Estado == "E" ? "Enviado" : "Borrador"),
                        Nombre = x.Nombre,
                        FechaValidez = x.FechaValidez.ToString("dd/MM/yyyy"),
                        FormaDePago = x.CondicionesVentas.Nombre,
                        Observaciones = x.Observaciones,
                        Total = x.ImporteTotalNeto,
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        private static void AgregarItem(int id, int? idConcepto, string concepto, decimal iva, decimal precio, decimal bonif, decimal cantidad, string tipoConcepto, int idUsuario)
        {
            int index = id;
            if (id != 0)
            {
                var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
                ComprobanteCart.Retrieve().Items.Remove(aux);
            }
            else
                index = ComprobanteCart.Retrieve().Items.Count() + 1;

            var tra = new ComprobantesDetalleViewModel(UsuarioCommon.ObtenerCantidadDecimales(idUsuario));
            tra.ID = index;// ComprobanteCart.Retrieve().Items.Count() + 1;
            tra.Concepto = concepto;
            tra.Iva = iva;
            tra.Bonificacion = bonif;
            tra.PrecioUnitario = precio;
            tra.Cantidad = cantidad;
            tra.IDConcepto = idConcepto;
            tra.TipoConcepto = tipoConcepto;

            if (id != 0)
                ComprobanteCart.Retrieve().Items.Insert(id - 1, tra);
            else
                ComprobanteCart.Retrieve().Items.Add(tra);
        }

        public static ResultadosPresupuestosViewModel ObtenerPresupuesto(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.Presupuestos.Include("Personas").Where(x => x.IDUsuario == idUsuario).AsQueryable();
                    Int32 numero = 0;
                    if (Int32.TryParse(condicion, out numero))
                        results = results.Where(x => x.Numero == numero);
                    else if (!string.IsNullOrWhiteSpace(condicion))
                        results = results.Where(x => x.Personas.RazonSocial.ToLower().Contains(condicion.ToLower()) || x.Personas.NombreFantansia.ToLower().Contains(condicion.ToLower()));

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(fechaDesde))
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaAlta >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta))
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaAlta <= dtHasta);
                    }

                    //Determinamos el formato decimal
                    var decimalFormat = "N2";// DecimalFormatter.GetDecimalStringFormat(idUsuario);

                    page--;
                    ResultadosPresupuestosViewModel resultado = new ResultadosPresupuestosViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.OrderByDescending(x => x.FechaAlta).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new PresupuestosViewModel()
                        {
                            ID = x.IDPresupuesto,
                            RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                            Fecha = x.FechaAlta.ToString("dd/MM/yyyy"),
                            Estado = ObtenerEstadoDesc(x.Estado),// == "A" ? "Aprobado" : (x.Estado == "E" ? "Enviado" : "Borrador"),
                            Nombre = x.Nombre,
                            Numero = x.Numero.ToString("#00000000"),
                            Total = x.ImporteTotalNeto.ToString(decimalFormat),
                            FechaValidez = x.FechaValidez.ToString("dd/MM/yyyy")
                        });
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string ObtenerEstadoDesc(string codigo)
        {
            if (codigo == "A")
                return "Aprobado";
            else if (codigo == "E")
                return "Enviado";
            else if (codigo == "B")
                return "Borrador";
            else if (codigo == "R")
                return "Rechazado";
            else
                return "Facturado";
        }
    }
}
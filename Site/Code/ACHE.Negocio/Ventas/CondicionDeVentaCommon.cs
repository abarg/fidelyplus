﻿using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;
using ACHE.Negocio.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Negocio.Ventas
{
    public static class CondicionDeVentaCommon
    {
        public static bool EliminarCondicionDeVenta(int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    CondicionesVentas entity = dbContext.CondicionesVentas.Where(x => x.IDCondicionVenta == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.Comprobantes.Any())
                            throw new Exception("No se puede eliminar por tener comprobantes asociados");
                        if (entity.Abonos.Any())
                            throw new Exception("No se puede eliminar por tener abonos asociados");
                        if (entity.Presupuestos.Any())
                            throw new Exception("No se puede eliminar por tener presupuestos asociados");
                        dbContext.CondicionesVentas.Remove(entity);
                        dbContext.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<CondicionDeVentaViewModel> ObtenerCondicionesDeVentas(int idUsuario,string nombre)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.CondicionesVentas.Where(x => x.IDUsuario == idUsuario).AsQueryable();
                    if (!string.IsNullOrWhiteSpace(nombre))
                        results = results.Where(x => x.Nombre.ToLower().Contains(nombre.ToLower()));
                    var listaCondicionesDeVentas =  results.OrderBy(x => x.Nombre).ToList() .Select(x => new CondicionDeVentaViewModel()
                    {
                        ID = x.IDCondicionVenta,
                        Nombre = x.Nombre,
                        FechaDeAlta = x.FechaAlta.ToString("dd/MM/yyyy"),
                        Activa = x.Activa?"Si":"No",
                        CobranzaAutomatica = x.CobranzaAutomatica?"Si":"No",
                        Banco = (x.IDBanco.HasValue) ? x.Bancos.BancosBase.Nombre + " - Nro Cuenta:" + x.Bancos.NroCuenta : "",
                        Caja =(x.IDCaja.HasValue)? x.Cajas.Nombre:"",
                        FormaDePago = x.FormaDePago,
                    });
                    return listaCondicionesDeVentas.ToList();
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static ResultadosCondicionDeVentaViewModel ObtenerCondicionesDeVentas(string nombre, int page, int pageSize, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = ObtenerCondicionesDeVentas(idUsuario, nombre).AsQueryable();


                    page--;
                    ResultadosCondicionDeVentaViewModel resultado = new ResultadosCondicionDeVentaViewModel();

                    var list = results.OrderBy(x => x.Nombre).Skip(page * pageSize).Take(pageSize).ToList();

                    resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = list.Count();
                    resultado.Items = list.ToList();
                    return resultado;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public static CondicionesVentas ObtenerCondicionDeVentaPorID(ACHEEntities dbContext,int idUsuario, int id)
        {
            try
            {
                
                    var result = dbContext.CondicionesVentas.Where(x => x.IDUsuario == idUsuario && x.IDCondicionVenta == id).FirstOrDefault();
                    return result;
                
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static CondicionesVentas GuardarCondicionDeVenta(int idCondicion, bool activa, DateTime fechaAlta, string nombre, bool cobranzaAutomatica, int idCaja, int idBanco, string formaDePago, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (cobranzaAutomatica && (string.IsNullOrEmpty(formaDePago) || (idBanco == 0 && idCaja == 0)))
                        throw new Exception("Debe completar los datos obligatorios");

                    if (dbContext.CondicionesVentas.Any(x => x.IDCondicionVenta != idCondicion && x.IDUsuario == idUsuario && x.Nombre==nombre))
                        throw new Exception("Ya existe una condicion de venta con ese nombre");

                    idCondicion = Math.Abs(idCondicion);
                    CondicionesVentas entity;

                    if (dbContext.CondicionesVentas.Any(x => x.IDCondicionVenta == idCondicion && x.IDUsuario == idUsuario))
                        entity = dbContext.CondicionesVentas.Where(x => x.IDCondicionVenta == idCondicion && x.IDUsuario == idUsuario).FirstOrDefault();
                    else
                        entity = new CondicionesVentas();

                    entity.IDUsuario = idUsuario;
                    entity.Activa = activa;
                    entity.FechaAlta = DateTime.Now;
                    entity.Nombre = nombre;
                    entity.CobranzaAutomatica = cobranzaAutomatica;
                    if (formaDePago != "undefined")
                        entity.FormaDePago = formaDePago;
                    if (idBanco > 0)
                        entity.IDBanco = idBanco;
                    else
                        entity.IDBanco = null;

                    //preguntar
                    var usu = TokenCommon.ObtenerWebUser(dbContext, idUsuario); //TODO: Mejorar esto

                    if (idBanco > 0)
                    {
                        if (!dbContext.BancosPlanDeCuenta.Any(x => x.IDBanco == idBanco))
                            ContabilidadCommon.CrearCuentaBancos(idBanco,null, usu);
                    }


                    if (idCaja > 0)
                        entity.IDCaja = idCaja;
                    else
                        entity.IDCaja = null;


                    if (!dbContext.CondicionesVentas.Any(x => x.IDCondicionVenta == idCondicion && x.IDUsuario == idUsuario))
                        dbContext.CondicionesVentas.Add(entity);
                    dbContext.SaveChanges();
                    return entity;
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static CondicionesVentas ObtenerCondicionDeVentaPorNombre(ACHEEntities dbContext, int idUsuario, string nombre)
        {
            try
            {
                
                    var result = dbContext.CondicionesVentas.Where(x => x.IDUsuario == idUsuario && x.Nombre.ToLower() == nombre.ToLower()).FirstOrDefault();
                    return result;
                
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

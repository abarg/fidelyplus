﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model.Negocio;
using ACHE.Extensions;
using System.Web;
using System.IO;
using System.Data;
using ACHE.Negocio.Common;
using ACHE.Negocio.Ventas;
using ACHE.Negocio.Productos;

namespace ACHE.Negocio.Abono {
    public class AbonosCommon {

        public static Abonos GuardarAbono(int id, string nombre, string frecuencia, string fechaInicio, string fechaFin, string estado, string precio, string iva, string obs, List<AbonosPersonasViewModel> personas, int tipo, int idUsuario, int idPlanDeCuenta, bool detallado, List<AbonosDetalleViewModel> items, int condicion) {
            using (var dbContext = new ACHEEntities()) {
                Abonos entity;
                if (id > 0)
                    entity = dbContext.Abonos.Where(x => x.IDAbono == id && x.IDUsuario == idUsuario).FirstOrDefault();
                else {
                    entity = new Abonos();
                    entity.IDUsuario = idUsuario;
                }

                entity.Frecuencia = frecuencia;
                entity.Nombre = nombre.ToUpper();
                entity.Estado = estado;
                entity.FechaInicio = DateTime.Parse(fechaInicio);
                if (fechaFin != string.Empty)
                    entity.FechaFin = DateTime.Parse(fechaFin);
                else
                    entity.FechaFin = null;

                entity.Iva = decimal.Parse(iva);
                entity.Observaciones = obs;
                entity.Tipo = tipo;
                entity.Detallado = detallado;
                entity.CondicionVenta = "";//SACAR DSPS
                entity.IDCondicionVenta = condicion;
                if (idPlanDeCuenta > 0)
                    entity.IDPlanDeCuenta = idPlanDeCuenta;
                else
                    entity.IDPlanDeCuenta = null;

                foreach (var p in personas) {
                    AbonosPersona per = new AbonosPersona();
                    per.IDPersona = p.IDPersona;
                    per.IDAbono = p.IDAbono;
                    per.Cantidad = (p.Cantidad == "" || p.Cantidad == "0") ? 1 : Math.Abs(Convert.ToInt32(p.Cantidad));
                    dbContext.AbonosPersona.Add(per);
                }
                decimal totalNeto = 0;
                decimal totalBruto = 0;

                if (detallado) {
                    foreach (var i in items) {
                        AbonosDetalle ad = new AbonosDetalle();

                        if (id > 0)
                            ad.IDAbono = id;
                        else
                            ad.IDAbono = i.IDAbono;
                        ad.PrecioUnitario = i.PrecioUnitario;
                        ad.Iva = i.Iva;
                        ad.Concepto = i.Concepto;
                        ad.Cantidad = i.Cantidad;
                        ad.Bonificacion = i.Bonificacion;
                        if (i.IDPlanDeCuenta > 0)
                            ad.IDPlanDeCuenta = i.IDPlanDeCuenta;

                        if (i.IDConcepto != null && i.IDConcepto != 0)
                            ad.IDConcepto = i.IDConcepto;

                        totalNeto += i.TotalConIva;
                        totalBruto += i.TotalSinIva;
                        dbContext.AbonosDetalle.Add(ad);
                    }
                }

                entity.PrecioUnitario = decimal.Parse(precio.Replace(".", ","));

                if (entity.PrecioUnitario == 0)
                    throw new CustomException("No puede generar una factura con Importe 0");

                if (id > 0) {
                    dbContext.Database.ExecuteSqlCommand("DELETE AbonosPersona WHERE IDAbono=" + id, new object[] { });
                    dbContext.Database.ExecuteSqlCommand("DELETE AbonosDetalle WHERE IDAbono=" + id, new object[] { });
                    dbContext.SaveChanges();
                }
                else {
                    dbContext.Abonos.Add(entity);
                    dbContext.SaveChanges();
                }
                return entity;
            }
        }

        public static Abonos Guardar(ACHEEntities dbContext, AbonoCartDto compCart) {
            string importe = compCart.Detallado ? compCart.ImporteNoGrabado.ToString("0.00") : compCart.Precio;
            string iva = compCart.Detallado ? compCart.PercepcionIVA.ToString("0.00") : compCart.Iva;

            return GuardarAbono(compCart.IDAbono, compCart.Nombre, compCart.Frecuencia, compCart.FechaInicio, compCart.FechaFin, compCart.Estado, importe, iva, compCart.Observaciones, compCart.Personas, compCart.Tipo, compCart.IDUsuario, compCart.IDPlanDeCuenta, compCart.Detallado, compCart.Items, compCart.IDCondicionVenta);

        }

        public static void BorrarAbono(WebUser usu, int id) {
            using (var dbContext = new ACHEEntities()) {
                //if (dbContext.ComprobantesDetalle.Any(x => x.IDAbono == id && x.Comprobantes.IDUsuario == usu.IDUsuario))
                //    throw new Exception("No se puede eliminar por tener comprobantes asociados");

                if (dbContext.Suscripciones.Any(x => x.IDAbono == id))
                    throw new Exception("No se puede eliminar por tener suscripciones asociadas");

                dbContext.Database.ExecuteSqlCommand("update ComprobantesDetalle set idAbono =null where idAbono=" + id);

                var entity = dbContext.Abonos.Where(x => x.IDAbono == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (entity != null) {
                    dbContext.Abonos.Remove(entity);
                    dbContext.SaveChanges();
                }
            }
        }

        public static ResultadosAbonosViewModel ObtenerAbonos(WebUser usu, string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize) {
            using (var dbContext = new ACHEEntities()) {
                var results = dbContext.Abonos.Include("AbonosPersona").Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                if (condicion != string.Empty)
                    results = results.Where(x => x.Nombre.ToLower().Contains(condicion.ToLower()));

                /*switch (periodo)
                {
                    case "30":
                        fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                        break;
                    case "15":
                        fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                        break;
                    case "7":
                        fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                        break;
                    case "1":
                        fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                        break;
                    case "0":
                        fechaDesde = DateTime.Now.ToShortDateString();
                        break;
                }

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.FechaInicio >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 11:59:59 pm");
                    results = results.Where(x => x.FechaFin <= dtHasta);
                }*/

                page--;
                ResultadosAbonosViewModel resultado = new ResultadosAbonosViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.Nombre).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new AbonosViewModel() {
                        ID = x.IDAbono,
                        Nombre = x.Nombre.ToUpper(),
                        Frecuencia = FrecuenciaCompleto(x.Frecuencia),
                        Estado = x.Estado == "A" ? "Activo" : "Inactivo",
                        Precio = x.PrecioUnitario.ToMoneyFormat(2),
                        Iva = (x.PrecioUnitario + (x.PrecioUnitario * x.Iva) / 100).ToMoneyFormat(2),
                        FechaInicio = x.FechaInicio.ToString("dd/MM/yyyy"),
                        FechaFin = x.FechaFin.HasValue ? " - " + x.FechaFin.Value.ToString("dd/MM/yyyy") : "",
                        CantClientes = x.AbonosPersona.Count().ToString(),
                        Total = (x.PrecioUnitario * x.AbonosPersona.Count()).ToMoneyFormat(2)
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }

        public static string Exportar(WebUser usu, string condicion, string periodo, string fechaDesde, string fechaHasta) {
            using (var dbContext = new ACHEEntities()) {
                string fileName = "Abonos_" + usu.IDUsuario + "_";
                string path = "~/tmp/";
                DataTable dt = new DataTable();

                var results = dbContext.Abonos.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                if (condicion != string.Empty)
                    results = results.Where(x => x.Nombre.ToLower().Contains(condicion.ToLower()));

                /*switch (periodo)
                {
                    case "30":
                        fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                        break;
                    case "15":
                        fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                        break;
                    case "7":
                        fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                        break;
                    case "1":
                        fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                        break;
                    case "0":
                        fechaDesde = DateTime.Now.ToShortDateString();
                        break;
                }
                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.FechaInicio >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 11:59:59 pm");
                    results = results.Where(x => x.FechaFin <= dtHasta);
                }*/

                dt = results.OrderBy(x => x.Nombre).ToList().Select(x => new {
                    ID = x.IDAbono,
                    Nombre = x.Nombre.ToUpper(),
                    Frecuencia = FrecuenciaCompleto(x.Frecuencia),
                    Estado = x.Estado == "A" ? "Activo" : "Inactivo",
                    ImpNetoGrav = x.PrecioUnitario,
                    TotalFact = (x.PrecioUnitario + (x.PrecioUnitario * x.Iva) / 100),
                    FechaInicio = x.FechaInicio.ToString("dd/MM/yyyy"),
                    FechaFin = x.FechaFin.HasValue ? x.FechaFin.Value.ToString("dd/MM/yyyy") : "-",
                    CantClientes = x.AbonosPersona.Count(),
                    Total = (x.PrecioUnitario * x.AbonosPersona.Count())
                }).ToList().ToDataTable();



                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }

        }

        private static string FrecuenciaCompleto(string frecuencia) {
            switch (frecuencia) {
                case "A":
                    return "Anual";
                case "S":
                    return "Semestral";
                case "T":
                    return "Trimestral";
                case "M":
                    return "Mensual";
                case "Q":
                    return "Quincenal";
            }
            return "";
        }

        public static AbonoCartDto ObtenerDatos(int id, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                Abonos entity = dbContext.Abonos.Include("AbonosDetalle").Where(x => x.IDUsuario == idUsuario && x.IDAbono == id).FirstOrDefault();

                if (entity != null) {
                    AbonoCartDto compCart = new AbonoCartDto();
                    compCart.IDAbono = entity.IDAbono;
                    compCart.Nombre = entity.Nombre;
                    compCart.Frecuencia = entity.Frecuencia;
                    compCart.FechaInicio = entity.FechaInicio.ToString();
                    compCart.FechaFin = entity.FechaFin.ToString();
                    compCart.IDUsuario = entity.IDUsuario;
                    compCart.Estado = entity.Estado;
                    compCart.Precio = entity.PrecioUnitario.ToString();
                    compCart.Iva = entity.Iva.ToString("#0.00");
                    compCart.Observaciones = entity.Observaciones;
                    compCart.Tipo = entity.Tipo;
                    compCart.IDPlanDeCuenta = entity.IDPlanDeCuenta.HasValue ? entity.IDPlanDeCuenta.Value : 0;

                    foreach (var det in entity.AbonosDetalle) {
                        var tra = new AbonosDetalleViewModel(UsuarioCommon.ObtenerCantidadDecimales(idUsuario));
                        tra.ID = AbonoCart.Retrieve().Items.Count() + 1;
                        tra.Concepto = det.Concepto;
                        tra.Codigo = (det.Conceptos != null) ? det.Conceptos.Codigo : "";
                        tra.CodigoPlanCta = (det.PlanDeCuentas == null) ? "" : det.PlanDeCuentas.Codigo + " - " + det.PlanDeCuentas.Nombre;
                        tra.Iva = det.Iva;
                        tra.PrecioUnitario = det.PrecioUnitario;
                        tra.Bonificacion = det.Bonificacion;
                        tra.Cantidad = det.Cantidad;
                        tra.IDConcepto = det.IDConcepto;
                        tra.IDPlanDeCuenta = det.IDPlanDeCuenta;

                        AbonoCart.Retrieve().Items.Add(tra);
                    }

                    compCart.Detallado = entity.Detallado;

                    return compCart;


                }
                else
                    throw new Exception("Error al obtener los datos");
            }
        }

        public static List<AbonosPersonasViewModel> ObtenerClientes(int id, int idUsuario) {
            List<AbonosPersonasViewModel> lista = new List<AbonosPersonasViewModel>();

            using (var dbContext = new ACHEEntities()) {
                lista = dbContext.AbonosPersona.Where(x => x.IDAbono == id).ToList()
                                               .Select(x => new AbonosPersonasViewModel() {
                                                   IDAbono = x.IDAbono,
                                                   Cantidad = x.Cantidad.ToString(),
                                                   IDPersona = x.IDPersona,
                                                   RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                                                   Total = (x.Abonos.Iva == 0) ? x.Abonos.PrecioUnitario.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario)) : (x.Cantidad * (x.Abonos.PrecioUnitario + ((x.Abonos.Iva * x.Abonos.PrecioUnitario) / 100))).ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario))
                                               }).ToList();
            }
            return lista;
        }

        public static string ObtenerItems(int idUsuario) {
            var html = string.Empty;
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                using (var dbContext = new ACHEEntities()) {
                    var list = AbonoCart.Retrieve().Items.OrderBy(x => x.Concepto).ToList();
                    if (list.Any()) {
                        int index = 1;
                        foreach (var detalle in list) {
                            html += "<tr>";
                            html += "<td>" + index + "</td>";
                            html += "<td style='text-align:right'>" + detalle.Cantidad.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario)) + "</td>";
                            html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                            html += "<td style='text-align:left'>" + detalle.Concepto + "</td>";
                            html += "<td style='text-align:right'>" + detalle.PrecioUnitario.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario)) + "</td>";
                            html += "<td style='text-align:right'>" + detalle.Bonificacion.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario)) + "</td>";
                            html += "<td style='text-align:right'>" + detalle.Iva.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario)) + "</td>";
                            html += "<td class='divPlanDeCuentas' style='text-align:left'>" + detalle.CodigoPlanCta + "</td>";
                            html += "<td style='text-align:right'>" + detalle.TotalConIva.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario)) + "</td>";

                            decimal preciFinal = 0;

                            if (usu.UsaPrecioFinalConIVA)
                                preciFinal = detalle.PrecioUnitarioConIva;
                            else
                                preciFinal = detalle.PrecioUnitario;

                            html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:eliminarItem(" + detalle.ID + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                            html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificarItem("
                                + detalle.ID
                                + ", '" + (detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "")
                                + "' ,'" + detalle.Cantidad.ToMoneyFormatForEdit(UsuarioCommon.ObtenerCantidadDecimales(idUsuario))
                                + "' ,'" + detalle.Codigo
                                + "','" + detalle.Concepto
                                + "','" + preciFinal.ToMoneyFormatForEdit(UsuarioCommon.ObtenerCantidadDecimales(idUsuario))
                                + "','" + detalle.Iva.ToString("#0.00")
                                + "','" + detalle.Bonificacion.ToMoneyFormatForEdit(UsuarioCommon.ObtenerCantidadDecimales(idUsuario))
                                + "','" + detalle.IDPlanDeCuenta.ToString()
                                + "');\"><i class='fa fa-edit'></i></a></td>";
                            html += "</tr>";

                            index++;
                        }
                    }
                }
                if (html == "")
                    html = "<tr><td colspan='9' style='text-align:center'>No tienes items agregados</td></tr>";

            }
            return html;

        }

        public static TotalesViewModel ObtenerTotales(string percepcionesIIBB, string percepcionesIVA, string importeNoGrabado, byte cantDecimales) {
            TotalesViewModel totales = new TotalesViewModel();

            AbonoCart.Retrieve().ImporteNoGrabado = Convert.ToDecimal(importeNoGrabado.Replace(".", ","));
            AbonoCart.Retrieve().PercepcionIVA = Convert.ToDecimal(percepcionesIVA.Replace(".", ","));
            AbonoCart.Retrieve().PercepcionIIBB = Convert.ToDecimal(percepcionesIIBB.Replace(".", ","));

            totales.Iva = AbonoCart.Retrieve().GetIva().ToMoneyFormat(cantDecimales);
            totales.Subtotal = AbonoCart.Retrieve().GetSubTotal().ToMoneyFormat(cantDecimales);
            totales.ImporteNoGrabado = AbonoCart.Retrieve().GetImporteNoGrabado().ToMoneyFormat(cantDecimales);
            totales.PercepcionIVA = AbonoCart.Retrieve().GetPercepcionIVA().ToMoneyFormat(cantDecimales);
            totales.PercepcionIIBB = AbonoCart.Retrieve().GetPercepcionIIBB().ToMoneyFormat(cantDecimales);
            totales.Total = AbonoCart.Retrieve().GetTotal().ToMoneyFormat(cantDecimales);

            return totales;

        }

        public static IList<AbonosAGenerarGrupoViewModel> ObtenerAbonosParaGenerar(WebUser usu, string fecha, string frecuencia, string nombre, byte cantDecimales) {

            using (var dbContext = new ACHEEntities()) {
                var auxList = dbContext.Abonos.Where(x => x.IDUsuario == usu.IDUsuario && x.Estado == "A").AsQueryable();
                if (frecuencia != string.Empty)
                    auxList = auxList.Where(x => x.Frecuencia == frecuencia).AsQueryable();
                if (nombre != string.Empty)
                    auxList = auxList.Where(x => x.Nombre.ToLower().Contains(nombre.ToLower())).AsQueryable();

                var results = auxList.OrderBy(x => x.Nombre).ToList().Select(x => new AbonosAGenerarGrupoViewModel(cantDecimales) {
                    ID = x.IDAbono,
                    Nombre = x.Nombre,
                    Observaciones = x.Observaciones,
                    tipoConcepto = x.Tipo,
                    Fecha = x.FechaInicio,
                    FechaFin = x.FechaFin,
                    IDPlanDeCuenta = x.IDPlanDeCuenta,
                    Detalles = x.AbonosDetalle.Select(d => new AbonosDetalleViewModel(cantDecimales) {
                        Concepto = d.Concepto,
                        Codigo = (d.Conceptos != null) ? d.Conceptos.Codigo : "",
                        CodigoPlanCta = (d.PlanDeCuentas == null) ? "" : d.PlanDeCuentas.Codigo + " - " + d.PlanDeCuentas.Nombre,
                        Iva = d.Iva,
                        PrecioUnitario = d.PrecioUnitario,
                        Bonificacion = d.Bonificacion,
                        Cantidad = d.Cantidad,
                        IDConcepto = d.IDConcepto,
                        IDPlanDeCuenta = d.IDPlanDeCuenta,
                    }
                    ).ToList()

                }).ToList();

                if (fecha != string.Empty) {
                    DateTime dtDesde = DateTime.Parse(fecha);
                    results = results.Where(x => x.Fecha <= dtDesde && (!x.FechaFin.HasValue || (x.FechaFin.HasValue && x.FechaFin >= dtDesde))).ToList();
                }

                foreach (var abono in results) {
                    abono.Items = dbContext.AbonosPersona.Include("Personas").Where(x => x.IDAbono == abono.ID && x.Abonos.IDUsuario == usu.IDUsuario && x.Personas.IDUsuario == usu.IDUsuario).ToList().Select(x => new AbonosAGenerarViewModel(cantDecimales) {
                        IDPersona = x.IDPersona,
                        RazonSocial = x.Personas.RazonSocial,
                        Cuit = x.Personas.NroDocumento,
                        CondicionIva = x.Personas.CondicionIva,
                        Importe = x.Abonos.PrecioUnitario,
                        Iva = x.Abonos.Iva,
                        nroRegistro = x.IDAbono.ToString() + x.IDPersona.ToString(),
                        FEGenerada = "Pendiente de generación.",
                        ClienteEmail = x.Personas.Email,
                        FrecuenciaAbono = x.Abonos.Frecuencia,
                        Cantidad = x.Cantidad,
                        CondicionVenta = "",
                        IDCondicionVenta = x.Abonos.IDCondicionVenta,
                        FechaInicio = x.Abonos.FechaInicio
                    }).ToList();
                    foreach (var item in abono.Items) {
                        var fechaFrecuenciaDesde = DateTime.Parse(fecha);
                        var fechaFrecuenciaHasta = DateTime.Now.AddDays(1).Date;

                        int difMeses;
                        decimal aux;
                        int result;

                        //switch (item.FrecuenciaAbono)
                        //{
                        //    case "A":
                        //         fechaFrecuenciaDesde = fechaFrecuenciaDesde.AddMonths(-12).GetFirstDayOfMonth();
                        //        fechaFrecuenciaHasta = fechaFrecuenciaDesde.GetLastDayOfMonth();
                        //        break;
                        //    case "S":
                        //         fechaFrecuenciaDesde = fechaFrecuenciaDesde.AddMonths(-6).GetFirstDayOfMonth();
                        //        fechaFrecuenciaHasta = fechaFrecuenciaDesde.GetLastDayOfMonth();
                        //        break;
                        //    case "T":
                        //        fechaFrecuenciaDesde = fechaFrecuenciaDesde.AddMonths(-3).GetFirstDayOfMonth();
                        //        fechaFrecuenciaHasta = fechaFrecuenciaDesde.AddMonths(2).GetLastDayOfMonth();
                        //        break;
                        //    case "M":
                        //        fechaFrecuenciaDesde = fechaFrecuenciaDesde.GetFirstDayOfMonth();
                        //        fechaFrecuenciaHasta = fechaFrecuenciaDesde.GetLastDayOfMonth();
                        //        break;
                        //    case "Q":
                        //        fechaFrecuenciaDesde = fechaFrecuenciaDesde.AddDays(-15).Date;
                        //        break;
                        //}

                        switch (item.FrecuenciaAbono) {
                            case "A":
                                difMeses = ((fechaFrecuenciaDesde.Year - item.FechaInicio.Year) * 12) + fechaFrecuenciaDesde.Month - item.FechaInicio.Month;
                                aux = difMeses / 12;
                                result = Convert.ToInt32(aux);
                                fechaFrecuenciaDesde = item.FechaInicio.AddMonths(result).Date;
                                //fechaFrecuenciaHasta = fechaFrecuenciaDesde.AddYears(1).GetLastDayOfMonth();
                                break;
                            case "S":
                                difMeses = ((fechaFrecuenciaDesde.Year - item.FechaInicio.Year) * 12) + fechaFrecuenciaDesde.Month - item.FechaInicio.Month;
                                aux = difMeses / 6;
                                result = Convert.ToInt32(aux);
                                fechaFrecuenciaDesde = item.FechaInicio.AddMonths(result).Date;
                                //fechaFrecuenciaHasta = fechaFrecuenciaDesde.AddMonths(6).GetLastDayOfMonth();
                                break;
                            case "T":
                                difMeses = ((fechaFrecuenciaDesde.Year - item.FechaInicio.Year) * 12) + fechaFrecuenciaDesde.Month - item.FechaInicio.Month;
                                aux = difMeses / 3;
                                result = Convert.ToInt32(aux);
                                fechaFrecuenciaDesde = item.FechaInicio.AddMonths(result).Date;
                                //fechaFrecuenciaHasta = fechaFrecuenciaDesde.AddMonths(3).GetLastDayOfMonth();
                                break;
                            case "M":
                                difMeses = ((fechaFrecuenciaDesde.Year - item.FechaInicio.Year) * 12) + fechaFrecuenciaDesde.Month - item.FechaInicio.Month;
                                aux = difMeses / 1;
                                result = Convert.ToInt32(aux);
                                fechaFrecuenciaDesde = item.FechaInicio.AddMonths(result).GetFirstDayOfMonth().Date;
                                //fechaFrecuenciaHasta = fechaFrecuenciaDesde.AddMonths(1).GetLastDayOfMonth();
                                break;
                            case "Q":
                                /*
                                difMeses = ((fechaFrecuenciaDesde.Year - item.FechaInicio.Year) * 12) + fechaFrecuenciaDesde.Month - item.FechaInicio.Month;
                                 aux = difMeses / 15;
                                 result = Convert.ToInt32(aux);
                                fechaFrecuenciaDesde = item.FechaInicio.AddMonths(result).Date;
                                 */
                                fechaFrecuenciaDesde = DateTime.Now.AddDays(-15).Date;
                                //fechaFrecuenciaHasta = fechaFrecuenciaDesde.AddMonths(6).GetLastDayOfMonth();
                                break;
                        }

                        var ComprobantePago = dbContext.ComprobantesDetalle.Include("Comprobantes").FirstOrDefault(x =>
                                                                                                        x.Comprobantes.IDUsuario == usu.IDUsuario &&
                                                                                                        x.Comprobantes.IDPersona == item.IDPersona &&
                                                                                                        x.IDAbono == abono.ID &&
                                                                                                        x.Comprobantes.FechaComprobante >= fechaFrecuenciaDesde &&
                                                                                                        x.Comprobantes.FechaComprobante <= fechaFrecuenciaHasta);


                        if (ComprobantePago != null) {
                            abono.Items.Where(x => x.IDPersona == item.IDPersona).FirstOrDefault().FEGenerada = "Comprobante ya emitido.";
                            abono.Items.Where(x => x.IDPersona == item.IDPersona).FirstOrDefault().FEGeneradaObs = "Fecha: " + ComprobantePago.Comprobantes.FechaComprobante.ToString("dd/MM/yyyy");
                        }
                        else {

                            var importe = item.Cantidad * item.Importe;
                            var ComprobanteMismoImporte = dbContext.ComprobantesDetalle.Include("Comprobantes").FirstOrDefault(x =>
                                                                                                        x.Comprobantes.IDUsuario == usu.IDUsuario &&
                                                                                                        x.Comprobantes.IDPersona == item.IDPersona &&
                                                                                                        x.Comprobantes.ImporteTotalNeto == importe &&
                                                                                                        x.Comprobantes.FechaComprobante >= fechaFrecuenciaDesde &&
                                                                                                        x.Comprobantes.FechaComprobante <= fechaFrecuenciaHasta);
                            if (ComprobanteMismoImporte != null) {
                                abono.Items.Where(x => x.IDPersona == item.IDPersona).FirstOrDefault().FEGeneradaObs = "FC Similar: " + ComprobanteMismoImporte.Comprobantes.PuntosDeVenta.Punto.ToString("#0000") + "-" + ComprobanteMismoImporte.Comprobantes.Numero.ToString("#00000000");
                            }
                        }
                        item.CondicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, item.IDCondicionVenta).Nombre;

                    }

                }
                return results;

            }

        }

        public static List<PlanDeCuentas> ObtenerCuentasActivo(int idusuario, ACHEEntities dbContext) {

            var idctas = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == idusuario).FirstOrDefault().CtasFiltroVentas.Split(',');
            List<Int32> cuentas = new List<Int32>();
            for (int i = 0; i < idctas.Length; i++) {
                if (idctas[i] != string.Empty)
                    cuentas.Add(Convert.ToInt32(idctas[i]));
            }

            var listaAux = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == idusuario && cuentas.Contains(x.IDPlanDeCuenta)).ToList();
            return listaAux;
        }

        public static void AgregarItem(AbonoCartItemDto item, byte cantDecimales) {

            if (item.ID != 0) {
                var aux = AbonoCart.Retrieve().Items.Where(x => x.ID == item.ID).FirstOrDefault();
                AbonoCart.Retrieve().Items.Remove(aux);
            }

            var tra = new AbonosDetalleViewModel(cantDecimales);
            tra.ID = AbonoCart.Retrieve().Items.Count() + 1;
            tra.Concepto = item.Concepto;
            tra.Codigo = item.Codigo;
            tra.CodigoPlanCta = item.CodigoCuenta;
            if (item.Iva != string.Empty)
                tra.Iva = decimal.Parse(item.Iva);
            else
                tra.Iva = 0;
            if (item.Bonificacion != string.Empty)
                tra.Bonificacion = decimal.Parse(item.Bonificacion.Replace(".", ","));
            else
                tra.Bonificacion = 0;

            tra.PrecioUnitario = decimal.Parse(item.Precio.Replace(".", ","));
            tra.Cantidad = decimal.Parse(item.Cantidad.Replace(".", ",")); ;
            if (item.IDConcepto != string.Empty)
                tra.IDConcepto = int.Parse(item.IDConcepto);
            else
                tra.IDConcepto = null;

            if (item.IDPlanDeCuenta != "")
                tra.IDPlanDeCuenta = Convert.ToInt32(item.IDPlanDeCuenta);

            tra.PrecioUnitario = ObtenerPrecioFinal(tra.PrecioUnitario, item.Iva, cantDecimales);
            AbonoCart.Retrieve().Items.Add(tra);
        }

        public static void EliminarItem(int id) {
            var aux = AbonoCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
            if (aux != null)
                AbonoCart.Retrieve().Items.Remove(aux);
        }

        public static void ActualizarPreciosAbonos(List<string> codigos, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                foreach (var codigo in codigos) {
                    var abonosConConcepto = dbContext.Abonos.Include("AbonosDetalle").Where(x => x.IDUsuario == idUsuario && x.AbonosDetalle.Any(z => z.Conceptos.Codigo == codigo)).ToList();
                    var concepto = dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario && x.Codigo == codigo).FirstOrDefault();
                    if (abonosConConcepto.Count() > 0) {
                        foreach (var a in abonosConConcepto) {
                            decimal total = 0;

                            foreach (var b in a.AbonosDetalle) {
                                if (b.IDConcepto == concepto.IDConcepto)
                                    b.PrecioUnitario = concepto.PrecioUnitario;

                                total += (b.PrecioUnitario - ((b.PrecioUnitario * b.Bonificacion) / 100)) * b.Cantidad;
                            }

                            a.PrecioUnitario = total;
                        }
                        dbContext.SaveChanges();
                    }
                }
            }

        }

        #region Helpers
        private static decimal ObtenerPrecioFinal(decimal PrecioUnitario, string iva, byte cantDecimales) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            using (var dbContext = new ACHEEntities()) {
                if (usu.UsaPrecioFinalConIVA)
                    return ConceptosCommon.ObtenerPrecioFinal(PrecioUnitario, iva, cantDecimales);
                else
                    return PrecioUnitario;
            }
        }
        #endregion




    }
}
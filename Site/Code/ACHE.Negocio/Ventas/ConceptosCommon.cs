﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using System.IO;
using System.Configuration;
using System.Data.Entity;
using ACHE.Negocio.Inventario;
using ACHE.Model.Negocio;
using Hangfire;
using System.Web;
using ACHE.Negocio.Common;
using System.Data;


namespace ACHE.Negocio.Productos
{
    public class ConceptosCommon
    {
        #region ABM

        public static int GuardarConcepto(int id, string nombre, string codigo, string tipo, string descripcion, string estado, string precio, string iva, string stock, string obs, string costoInterno, string stockMinimo, string codigoOem, int idPersona, string rentabilidad, int idUsuario, string codigoBarra, string idrubro, string idsubRubro, string idPlanDeCuentaCompra, string idPlanDeCuentaVenta, bool precioAutomatico)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.Conceptos.Any(x => x.IDUsuario == idUsuario && x.Codigo == codigo && x.IDConcepto != id && x.Codigo != ""))
                        throw new CustomException("El Código ingresado ya se encuentra registrado.");

                    Conceptos entity;
                    bool hayCambioPrecio = false;
                    var auxCostoInterno = (!string.IsNullOrEmpty(costoInterno)) ? decimal.Parse(costoInterno.Replace(".", ",")) : 0;
                    var auxPrecio = decimal.Parse(precio.Replace(".", ","));
                    var auxRent = rentabilidad == String.Empty ? 0 : decimal.Parse(rentabilidad.Replace(".", ","));

                    if (id > 0)
                    {
                        entity = dbContext.Conceptos.Where(x => x.IDConcepto == id && x.IDUsuario == idUsuario).FirstOrDefault();
                        if (auxCostoInterno != entity.CostoInterno || auxPrecio != entity.PrecioUnitario || entity.Rentabilidad != auxRent)
                            hayCambioPrecio = true;
                    }
                    else
                    {
                        entity = new Conceptos();
                        entity.FechaAlta = DateTime.Now;
                        entity.IDUsuario = idUsuario;
                    }

                    entity.Tipo = tipo;
                    entity.Codigo = codigo.ToUpper();
                    entity.Nombre = nombre.ToUpper();
                    entity.CodigoOEM = codigoOem;
                    entity.Estado = estado;
                    entity.Descripcion = (string.IsNullOrWhiteSpace(descripcion)) ? "" : descripcion;
                    entity.Stock = 0; // decimal.Parse(stock.Replace(".", ",")); ;
                    entity.PrecioUnitario = auxPrecio;
                    entity.Iva = decimal.Parse(iva.Replace(".", ","));
                    entity.Rentabilidad = auxRent;// rentabilidad == String.Empty ? 0 : decimal.Parse(rentabilidad.Replace(".", ","));
                    entity.CostoInterno = auxCostoInterno;
                    entity.Observaciones = (string.IsNullOrWhiteSpace(obs)) ? "" : obs;
                    entity.CodigoBarras = codigoBarra;
                    entity.PrecioAutomatico = precioAutomatico;
                    if (idPersona > 0)
                        entity.IDPersona = idPersona;
                    else
                        entity.IDPersona = null;

                    if (stockMinimo != string.Empty)
                        entity.StockMinimo = int.Parse(stockMinimo);
                    else
                        entity.StockMinimo = null;

                    entity.IDRubro = int.Parse(idrubro);
                    if (!string.IsNullOrEmpty(idsubRubro) && idsubRubro != "0")
                    {
                        entity.IDSubRubro = int.Parse(idsubRubro);
                    }
                    else
                        entity.IDSubRubro = null;

                    if (idPlanDeCuentaCompra != "" && idPlanDeCuentaCompra != "0" && idPlanDeCuentaCompra != null)
                    {
                        entity.IDPlanDeCuentaCompras = int.Parse(idPlanDeCuentaCompra);
                    }
                    else
                        entity.IDPlanDeCuentaCompras = null;

                    if (idPlanDeCuentaVenta != "" && idPlanDeCuentaVenta != "0" && idPlanDeCuentaVenta != null)
                    {
                        entity.IDPlanDeCuentaVentas = int.Parse(idPlanDeCuentaVenta);
                    }
                    else
                        entity.IDPlanDeCuentaVentas = null;
                    


                    if (id > 0)
                    {
                        //esto para que es ?
                        dbContext.Database.ExecuteSqlCommand("DELETE ComboConceptos WHERE IDConceptoCombo=" + id, new object[] { });

                        dbContext.SaveChanges();
                        if (hayCambioPrecio)
                        {
                            PreciosHistorial historial = new PreciosHistorial();
                            historial.IDConcepto = entity.IDConcepto;
                            historial.IDUsuario = entity.IDUsuario;
                            historial.Fecha = DateTime.Now;
                            historial.CostoInterno = entity.CostoInterno;
                            historial.PrecioUnitario = entity.PrecioUnitario;
                            historial.Rentabilidad = entity.Rentabilidad;
                            var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                            historial.UsuarioAlta = usu.Email;

                            dbContext.PreciosHistorial.Add(historial);
                            dbContext.SaveChanges();
                        }
                    }
                    else
                    {
                        dbContext.Conceptos.Add(entity);
                        dbContext.SaveChanges();

                        PreciosHistorial historial = new PreciosHistorial();
                        historial.IDConcepto = entity.IDConcepto;
                        historial.IDUsuario = entity.IDUsuario;
                        historial.Fecha = DateTime.Now;
                        historial.CostoInterno = entity.CostoInterno;
                        historial.PrecioUnitario = entity.PrecioUnitario;
                        historial.Rentabilidad = entity.Rentabilidad;
                        var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                        historial.UsuarioAlta = usu.Email;
                        dbContext.PreciosHistorial.Add(historial);


                        StockConceptosReserva res = new StockConceptosReserva();
                        res.IDConcepto = entity.IDConcepto;
                        res.Reserva = 0;
                        res.IDUsuario = idUsuario;
                        dbContext.StockConceptosReserva.Add(res);

                        dbContext.SaveChanges();
                    }

                    if (DetalleCart.Instance != null)
                        DetalleCart.Retrieve().Items.Clear();

                    //Modifico abonosdetalles que tengan este concepto. Tambien modifico los totales de los abonos con el nuevo precio.
                    // 
                    var abonosConConcepto = dbContext.Abonos.Include("AbonosDetalle").Where(x => x.IDUsuario == idUsuario && x.AbonosDetalle.Any(z => z.IDConcepto == entity.IDConcepto)).ToList();
                    if (abonosConConcepto.Count() > 0)
                    {
                        foreach (var a in abonosConConcepto)
                        {
                            decimal total = 0;

                            foreach (var b in a.AbonosDetalle)
                            {
                                if (b.IDConcepto == entity.IDConcepto)
                                    b.PrecioUnitario = entity.PrecioUnitario;

                                total += (b.PrecioUnitario - ((b.PrecioUnitario * b.Bonificacion) / 100)) * b.Cantidad;
                            }

                            a.PrecioUnitario = total;
                        }
                        dbContext.SaveChanges();
                    }



                    return entity.IDConcepto;
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void ActualizarPrecios(int idUsuario, int idListaPrecios, decimal porcentaje, string actualizarTodos, int idPersona, string email, int idProceso)
        {
            WebUser usu;
            List<string> codigosConceptos = new List<string>();

            using (var dbContext = new ACHEEntities())
            {
                if (actualizarTodos == "1")
                {
                    List<Conceptos> productos;
                    if (idPersona > 0)
                        productos =
                            dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario && x.IDPersona == idPersona).ToList();
                    else
                        productos = dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario).ToList();

                    //TODO: PASAR A SP
                    List<PreciosHistorial> listHist = new List<PreciosHistorial>();
                    foreach (var item in productos)
                    {
                        item.PrecioUnitario = item.PrecioUnitario + ((item.PrecioUnitario * porcentaje) / 100);

                        PreciosHistorial historial = new PreciosHistorial();
                        historial.IDConcepto = item.IDConcepto;
                        historial.IDUsuario = idUsuario;
                        historial.Fecha = DateTime.Now;
                        //historial.CostoInterno = c.CostoInterno;
                        historial.PrecioUnitario = item.PrecioUnitario;
                        historial.Rentabilidad = item.Rentabilidad;
                        if (idPersona > 0)
                            historial.IDPersona = idPersona;
                        historial.UsuarioAlta = email;
                        listHist.Add(historial);

                        codigosConceptos.Add(item.Codigo);
                    }

                    dbContext.SaveChanges();

                    //Como cambió el precio se debe notificar a las integraciones
                    ConceptosCommon.NotificacionCambioConcepto(idUsuario, codigosConceptos, true, idProceso);

                    if (listHist.Any())
                    {
                        dbContext.PreciosHistorial.AddRange(listHist);
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    List<PreciosConceptos> productos;
                    if (idPersona > 0)
                        productos =
                            dbContext.PreciosConceptos.Where(
                                x =>
                                    x.IDListaPrecios == idListaPrecios && x.Conceptos.IDPersona == idPersona &&
                                    x.Conceptos.IDUsuario == idUsuario && x.Precio > 0).ToList();
                    else
                        productos =
                            dbContext.PreciosConceptos.Where(
                                x =>
                                    x.IDListaPrecios == idListaPrecios && x.Conceptos.IDUsuario == idUsuario && x.Precio > 0)
                                .ToList();

                    foreach (var item in productos)
                    {
                        if (item.Precio > 0)
                        {
                            item.Precio = item.Precio + ((item.Precio * porcentaje) / 100);
                            dbContext.SaveChanges();
                        }
                    }

                    //dbContext.SaveChanges();
                }
            }
        }

        public static ResultadosProductosViewModel ObtenerConceptos(string tipo, string codigo, string condicion, int page, int pageSize, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.Conceptos.Include("StockConceptos")
                    .Include("StockConceptos.Inventarios")
                    .Where(x => x.IDUsuario == idUsuario).AsQueryable();

                if (!string.IsNullOrEmpty(tipo))
                {
                    results = results.Where(x => x.Tipo == tipo).AsQueryable();
                }

                if (!string.IsNullOrEmpty(codigo))
                {
                    results = results.Where(x => x.Codigo.ToLower() == codigo.ToLower()).AsQueryable();
                }

                if (!string.IsNullOrEmpty(condicion))
                {
                    var searchWords = condicion.ToLower().Split(' ');
                    var firstWord = searchWords[0];
                    results = results.Where(
                        x => x.Nombre.ToLower().Contains(firstWord.ToLower())
                        || x.Descripcion.ToLower().Contains(firstWord.ToLower())
                        || x.Codigo.ToLower().Contains(firstWord.ToLower())
                        || (x.CodigoOEM != null && x.CodigoOEM.ToLower().Contains(firstWord.ToLower())));

                    if (searchWords.Length > 1)
                    {
                        foreach (var item in searchWords.Skip(1))
                        {
                            results = results.Where(
                            x => x.Nombre.ToLower().Contains(item.ToLower())
                            || x.Codigo.ToLower().Contains(item.ToLower())
                            || x.Descripcion.ToLower().Contains(item.ToLower())
                            || (x.CodigoOEM != null && x.CodigoOEM.ToLower().Contains(item.ToLower())));
                        }
                    }
                }


                //if (!string.IsNullOrEmpty(condicion)) {            
                //List<string> auxList = condicion.ToLower().Split(" ").ToList();
                //string[] auxWords = auxList.ToArray();

                //results = results.Where(
                //    x => (auxWords.Any(w => x.Codigo.ToLower().Contains(w)) 
                //    || auxWords.Any(w => x.Nombre.ToLower().Contains(w)))
                //    || (x.CodigoOEM != null && auxWords.Any(w => x.CodigoOEM.ToLower().Contains(w))));
                //}


                //Determinamos cantidad de decimales que posee configurado el usuario
                var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(idUsuario);

                page--;
                ResultadosProductosViewModel resultado = new ResultadosProductosViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                //TODO: mejorat llamada a obtener stock total
                var list = results.OrderBy(x => x.Nombre).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new ConceptosViewModel()
                    {
                        ID = x.IDConcepto,
                        Tipo = x.Tipo == "S" ? "Servicio" : (x.Tipo == "P" ? "Producto" : "Combo"),
                        Nombre = x.Nombre.ToUpper().Replace("'", ""),
                        Codigo = x.Codigo.ToUpper(),
                        Descripcion = x.Descripcion,
                        Estado = x.Estado == "A" ? "Activo" : "Inactivo",
                        Precio = x.PrecioUnitario.ToMoneyFormat(cantidadDecimales),
                        CostoInterno = x.CostoInterno.HasValue ? x.CostoInterno.Value.ToMoneyFormat(cantidadDecimales) : "",
                        Iva = x.Iva.ToString("#0.00"),

                    }).ToList();

                foreach (var item in list)
                {
                    if (item.Tipo != "S")
                    {
                        var stockAux = InventariosCommon.ObtenerStockTotal(results.First(x => x.IDConcepto == item.ID));
                        item.Stock = stockAux.StockActual.ToMoneyFormat(2);
                        item.StockReservado = stockAux.StockReservado.ToMoneyFormat(2);
                        item.StockConReservas = stockAux.StockConReservas.ToMoneyFormat(2);
                    }
                }

                ////TODO: Mejorar la obtencion del stock de un combo
                //foreach (var item in list)
                //{
                //    if (item.Tipo == "Combo")
                //    {
                //        item.Stock =
                //            (InventariosCommon.ObtenerStockTotal(results.First(x => x.IDConcepto == item.ID))
                //                .StockActual).ToMoneyFormat(cantidadDecimales);
                //        item.Stock =
                //         (InventariosCommon.ObtenerStockTotal(results.First(x => x.IDConcepto == item.ID))
                //             .StockActual).ToMoneyFormat(cantidadDecimales);
                //    }
                //}
                //var auxList = list.ToList();

                resultado.Items = list;

                return resultado;
            }
        }

        public static ResultadosProductosViewModel ObtenerConceptosPorPersona(string condicion, string idRubro, string idSubRubro, int page, int pageSize, int idUsuario, int idPersona)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.Conceptos.Include("StockConceptos").Where(x => x.IDUsuario == idUsuario && x.Estado == "A" && x.Tipo != "S").AsQueryable();

                if (!string.IsNullOrEmpty(condicion))
                    results = results.Where(x => x.Codigo.ToLower().Contains(condicion.ToLower()) || x.Nombre.ToLower().Contains(condicion.ToLower()) || (x.CodigoOEM != null && x.CodigoOEM.ToLower().Contains(condicion.ToLower())));

                if (!string.IsNullOrEmpty(idRubro))
                {
                    int id = int.Parse(idRubro);
                    results = results.Where(x => x.IDRubro == id);
                }
                if (!string.IsNullOrEmpty(idSubRubro))
                {
                    int id = int.Parse(idSubRubro);
                    results = results.Where(x => x.IDSubRubro == id);
                }

                //Determinamos cantidad de decimales que posee configurado el usuario
                var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(idUsuario);

                page--;
                ResultadosProductosViewModel resultado = new ResultadosProductosViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.Nombre).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new ConceptosViewModel()
                    {
                        ID = x.IDConcepto,
                        Tipo = x.Tipo == "S" ? "Servicio" : (x.Tipo == "P" ? "Producto" : "Combo"),
                        Nombre = x.Nombre.ToUpper().Replace("'", ""),
                        Codigo = x.Codigo.ToUpper(),
                        Descripcion = x.Descripcion,
                        Estado = x.Estado == "A" ? "Activo" : "Inactivo",
                        Precio = x.PrecioUnitario.ToMoneyFormat(cantidadDecimales),
                        CostoInterno = x.CostoInterno.HasValue ? x.CostoInterno.Value.ToMoneyFormat(cantidadDecimales) : "",
                        Iva = x.Iva.ToString("#0.00"),
                        //Stock = x.Tipo != "P" ? "" : x.StockConceptos.Sum(y => y.Stock).ToMoneyFormat(cantidadDecimales),
                        Foto = x.Foto
                    }).ToList();
                //var aux = list.ToList();
                foreach (var item in list)
                {
                    if (item.Tipo != "S")
                    {
                        var stockAux = InventariosCommon.ObtenerStockTotal(results.First(x => x.IDConcepto == item.ID));
                        item.Stock = stockAux.StockActual.ToMoneyFormat(2);
                        item.StockReservado = stockAux.StockReservado.ToMoneyFormat(2);
                        item.StockConReservas = stockAux.StockConReservas.ToMoneyFormat(2);
                    }
                }


                if (idPersona > 0)
                {
                    var persona = dbContext.Personas.Where(x => x.IDPersona == idPersona).FirstOrDefault();

                    foreach (var item in list)
                    {

                        if (persona.ListaPrecios != null && persona.ListaPrecios.PreciosConceptos.Where(x => x.IDConceptos == item.ID && x.Precio > 0).Any())
                            item.Precio = persona.ListaPrecios.PreciosConceptos.Where(x => x.IDConceptos == item.ID).FirstOrDefault().Precio.ToMoneyFormat(cantidadDecimales);

                    }
                }
                resultado.Items = list;

                return resultado;
            }
        }

        public static bool EliminarConcepto(int id, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                //else if (dbContext.PreciosConceptos.Any(x => x.IDConceptos == id))
                //    throw new CustomException("No se puede eliminar por estar en una lista de precio");

                if (dbContext.ComprasDetalle.Any(x => x.IDConcepto == id))
                    throw new CustomException("No se puede eliminar por tener compras asociados");
                else if (dbContext.PresupuestoDetalle.Any(x => x.IDConcepto == id))
                    throw new CustomException("No se puede eliminar por tener presupuestos asociados");
                else if (dbContext.ComprobantesDetalle.Any(x => x.IDConcepto == id))
                    throw new CustomException("No se puede eliminar por tener comprobantes asociados");
                else if (dbContext.OrdenVentaDetalle.Any(x => x.IDConcepto == id))
                    throw new CustomException("No se puede eliminar por tener ordenes de venta asociadas");
                else if (dbContext.AbonosDetalle.Any(x => x.IDConcepto == id))
                    throw new CustomException("No se puede eliminar por tener abonos asociados");
                else
                {
                    var entity = dbContext.Conceptos.Where(x => x.IDConcepto == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.Database.ExecuteSqlCommand("update ConfiguracionPublicacionIntegracion set IDConcepto=null where IDConcepto = " + id, new object[] { });

                        var archivo = entity.Foto;
                        dbContext.Conceptos.Remove(entity);
                        dbContext.SaveChanges();
                        var path = Path.Combine(ConfigurationManager.AppSettings["PathBaseWeb"] + "/files/explorer/" + idUsuario.ToString() + "/Productos-Servicios/" + archivo);
                        if (File.Exists(path))
                            File.Delete(path);
                        return true;
                    }
                    else
                        return false;
                }
            }
        }

        public static void EliminarTodos(int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.ExecuteSqlCommand("exec deleteAllConceptos " + idUsuario, new object[] { });
            }
        }

        #endregion

        public static ResultadosPreciosHistorialViewModel ObtenerHistorialPrecios(int idConcepto, int idUsuario, int page, int pageSize)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.PreciosHistorial.Include("Conceptos").Include("Personas").Where(x => x.IDConcepto == idConcepto && x.IDUsuario == idUsuario).AsQueryable();

                page--;
                ResultadosPreciosHistorialViewModel resultado = new ResultadosPreciosHistorialViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                //Determinamos cantidad de decimales que posee configurado el usuario
                //var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(idUsuario);

                var list = results.OrderByDescending(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new PreciosHistorialViewModel()
                    {
                        ID = x.IDHistorial,
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        Proveedor = x.IDPersona.HasValue ? (x.Personas.NombreFantansia != null ? x.Personas.NombreFantansia : x.Personas.RazonSocial) : "",
                        Precio = x.PrecioUnitario.ToMoneyFormat(2),
                        CostoInterno = x.CostoInterno.HasValue ? x.CostoInterno.Value.ToMoneyFormat(2) : "",
                        Rentabilidad = x.Rentabilidad.ToMoneyFormat(2)
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }

        public static Conceptos GetConcepto(int id, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var resultado = dbContext.Conceptos.Include("StockConceptos").Where(x => x.IDConcepto == id && x.IDUsuario == idUsuario).FirstOrDefault();

                return resultado;
            }
        }

        public static Conceptos GetConceptoConIncludes(int id, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {

                var resultado = dbContext.Conceptos
                    .Include("StockConceptos")
                    .Include("StockConceptos.Inventarios")
                    .Include("ComboConceptos").Include("ComboConceptos.Conceptos1")
                    .Include("ComboConceptos.Conceptos1.StockConceptos")
                    .Include("ComboConceptos.Conceptos1.StockConceptos.Inventarios")
                    .Where(x => x.IDConcepto == id && x.IDUsuario == idUsuario).FirstOrDefault();

                return resultado;
            }
        }

        public static Conceptos ObtenerConcepto(string codigo, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var resultado = dbContext.Conceptos.Where(x => x.IDUsuario == idUsuario && x.Codigo == codigo).FirstOrDefault();

                return resultado;
            }
        }

        public static Conceptos ObtenerConceptoConIncludes(string codigo, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var resultado = dbContext.Conceptos
                    .Include("StockConceptos")
                    .Include("StockConceptos.Inventarios")
                    .Include("ComboConceptos").Include("ComboConceptos.Conceptos1")
                    .Include("ComboConceptos.Conceptos1.StockConceptos")
                    .Include("ComboConceptos.Conceptos1.StockConceptos.Inventarios")
                    .Where(x => x.IDUsuario == idUsuario && x.Codigo == codigo).FirstOrDefault();

                return resultado;
            }
        }

        public static decimal ObtenerPrecioFinal(decimal precioUnitario, string iva, byte cantidadDecimales)
        {
            decimal precioFinal = precioUnitario;
            var auxIVA = "1";

            switch (iva)
            {
                case "00,00":
                    auxIVA = "1";
                    break;
                case "02,50":
                    auxIVA = "1,025";
                    break;
                case "05,00":
                    auxIVA = "1,050";
                    break;
                case "10,50":
                    auxIVA = "1,105";
                    break;
                case "21,00":
                    auxIVA = "1,210";
                    break;
                case "27,00":
                    auxIVA = "1,270";
                    break;
            }

            decimal IVA = decimal.Parse(auxIVA);
            precioFinal = precioUnitario / IVA;

            return precioFinal;
            //return Math.Round(precioFinal, cantidadDecimales);
        }

        public static async Task<IEnumerable<Rubros>> ObtenerRubros(ACHEEntities dbContext, int idUsuario)
        {

            return
                await
                    dbContext.Rubros.Where(x => x.IDUsuario == idUsuario && !x.IDRubroPadre.HasValue)
                        .OrderBy(x => x.Nombre)
                        .ToListAsync();

        }

        public static async Task<IEnumerable<Rubros>> ObtenerSubrubros(ACHEEntities dbContext, int idUsuario, int idRubro)
        {

            return
                await
                    dbContext.Rubros.Where(x => x.IDUsuario == idUsuario && x.IDRubroPadre == idRubro)
                        .OrderBy(x => x.Nombre)
                        .ToListAsync();

        }

        internal static void NotificacionCambioConcepto(int idUsuario, List<string> codigosConceptos, bool actualizarCostoCombos, int idProceso)
        {
            
        }

        /*public static void NotificacionCambioMultiplesConceptos(int idUsuario, List<string> codigosConceptos, bool actualizarCostoCombos, int idProceso)
        {
            //try {

            List<Conceptos> conceptos = new List<Conceptos>();
            using (var dbContext = new ACHEEntities())
            {
                conceptos = dbContext.Conceptos.Where(c => c.IDUsuario == idUsuario && codigosConceptos.Contains(c.Codigo)).ToList();
            }
            foreach (var concepto in conceptos)
            {
                //Como cambió el stock se debe notificar a las integraciones
                IntegracionCommon.NotificarCambioConcepto(idUsuario, concepto.IDConcepto, concepto.Tipo, false);
            }

            //if (actualizarCostoCombos)
            //ActualizarCostosInternosDeCombos(idUsuario, codigosConceptos);

            LogCommon.FinalizarProceso(idUsuario, idProceso, false);

            //}
            //catch (Exception) {
            //    LogCommon.FinalizarProceso(idUsuario, idProceso, true);
            //}
        }*/

        //todo: mejoras performance.
        public static void ActualizarCostosInternos(ACHEEntities dbContext, int idUsuario, List<DetalleViewModel> items)
        {
            //conceptos = dbContext.Conceptos.Where(c => c.IDUsuario == idUsuario && codigosConceptos.Contains(c.IDConcepto)).ToList();
            var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
            foreach (var item in items.Where(x => x.IDConcepto.HasValue).ToList())
            {
                var concepto = dbContext.Conceptos.Where(x => x.IDConcepto == item.IDConcepto.Value).FirstOrDefault();
                if (concepto != null)
                {
                    bool hayCambioPrecio = false;
                    if (concepto.CostoInterno != item.PrecioUnitarioSinIVA)
                        hayCambioPrecio = true;

                    if (hayCambioPrecio)
                    {
                        concepto.CostoInterno = item.PrecioUnitarioSinIVA;
                        if (concepto.Rentabilidad > 0 && concepto.CostoInterno > 0)
                            concepto.PrecioUnitario = concepto.CostoInterno.Value + ((concepto.CostoInterno.Value * concepto.Rentabilidad) / 100);

                        PreciosHistorial historial = new PreciosHistorial();
                        historial.IDConcepto = item.IDConcepto.Value;
                        historial.IDUsuario = idUsuario;
                        historial.Fecha = DateTime.Now;
                        historial.CostoInterno = item.PrecioUnitarioSinIVA;
                        historial.PrecioUnitario = concepto.PrecioUnitario;
                        historial.UsuarioAlta = usu.Email;
                        historial.Rentabilidad = concepto.Rentabilidad;
                        dbContext.PreciosHistorial.Add(historial);
                        dbContext.SaveChanges();
                    }
                }
            }
        }

        public static void EliminarFoto(WebUser usu, int idConcepto)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Conceptos.Where(x => x.IDConcepto == idConcepto && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (entity != null)
                {
                    string Serverpath = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/Productos-Servicios/" + entity.Foto);

                    if (File.Exists(Serverpath))
                    {
                        File.Delete(Serverpath);

                        entity.Foto = "";
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("El producto no tiene una imagen guardada");
                    }
                }
            }
        }

        public static ConceptosViewModel ObtenerDatos(int idUsuario, int id, int idPersona)
        {
            using (var dbContext = new ACHEEntities())
            {
                Conceptos entity = dbContext.Conceptos.Where(x => x.IDConcepto == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null)
                {
                    ConceptosViewModel result = new ConceptosViewModel();
                    result.ID = id;
                    result.Nombre = (entity.Descripcion != "") ? entity.Descripcion : entity.Nombre;
                    result.Iva = entity.Iva.ToString("#0.00");
                    result.Codigo = entity.Codigo;
                    result.idPlanDeCuentaCompra = entity.IDPlanDeCuentaCompras ?? 0;
                    result.idPlanDeCuentaVenta = entity.IDPlanDeCuentaVentas ?? 0;

                    if (idPersona > 0)
                    {
                        var persona = dbContext.Personas.Where(x => x.IDPersona == idPersona).FirstOrDefault();
                        if (persona.ListaPrecios != null &&
                            persona.ListaPrecios.PreciosConceptos.Where(x => x.IDConceptos == id && x.Precio > 0).Any())
                            result.Precio =
                                persona.ListaPrecios.PreciosConceptos.Where(x => x.IDConceptos == id)
                                    .FirstOrDefault()
                                    .Precio.ToString()
                                    .Replace(",", ".");
                        else
                        {


                            result.Precio = entity.PrecioUnitario.ToString("").Replace(",", ".");
                        }
                    }
                    else
                        result.Precio = entity.PrecioUnitario.ToString("").Replace(",", ".");

                    var pu = Convert.ToDecimal(result.Precio.Replace(".", ","));

                    result.Precio = ObtenerPrecioFinalDatos(Convert.ToDecimal(pu.ToString("").Replace(".", ",")), result.Iva, idPersona, idUsuario).ToString("").Replace(",", ".");
                    return result;
                }
                else
                    throw new Exception("Error al obtener los datos");
            }
        }

        public static ConceptosViewModel ObtenerDatosParaCompras(int idUsuario, int id)
        {
            using (var dbContext = new ACHEEntities())
            {
                Conceptos entity = dbContext.Conceptos.Where(x => x.IDConcepto == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null)
                {
                    ConceptosViewModel result = new ConceptosViewModel();
                    result.ID = id;
                    result.Nombre = (entity.Descripcion != "") ? entity.Descripcion : entity.Nombre;
                    result.Iva = entity.Iva.ToString("#0.00");
                    result.Codigo = entity.Codigo;
                    result.Precio = entity.CostoInterno.HasValue ? entity.CostoInterno.Value.ToString("").Replace(",", ".") : "0";
                    result.idPlanDeCuentaCompra = entity.IDPlanDeCuentaCompras ?? 0;

                    //result.Precio = ObtenerPrecioFinal(Convert.ToDecimal(result.Precio.Replace(".", ",")), result.Iva, 0).ToString("").Replace(",", ".");
                    return result;
                }
                else
                    throw new Exception("Error al obtener los datos");
            }
        }

        private static decimal ObtenerPrecioFinalDatos(decimal PrecioUnitario, string iva, int idPersona, int idUsuario)
        {
            //var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            decimal resultado = Math.Round(PrecioUnitario, UsuarioCommon.ObtenerCantidadDecimales(idUsuario));

            /*using (var dbContext = new ACHEEntities())
            {
                //var UsaPrecioConIva = dbContext.Personas.Any(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == idPersona && (x.CondicionIva == "MO" || x.CondicionIva == "CF"));
                //if (!usu.UsaPrecioFinalConIVA && UsaPrecioConIva)
                if (usu.UsaPrecioFinalConIVA)
                    resultado = PrecioUnitario - ((PrecioUnitario * Convert.ToDecimal(iva)) / 100);

             * return Math.Round(resultado, ConfiguracionHelper.ObtenerCantidadDecimales());
            }*/
            return resultado;
        }

        public static string ExportarConceptos(int idUsuario, string tipo, string codigo, string condicion)
        {

            string fileName = "Productos_" + idUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.Conceptos.Include("StockConceptos").Include("ComboConceptos")
                        .Include("Personas").Where(x => x.IDUsuario == idUsuario).AsQueryable();

                    if (codigo != string.Empty)
                        results = results.Where(x => x.Codigo.ToLower() == codigo.ToLower());// || x.Nombre.ToLower().Contains(condicion.ToLower()));

                    if (condicion != string.Empty)
                        results = results.Where(x => x.Codigo.ToLower().Contains(condicion.ToLower()) || x.Descripcion.ToLower().Contains(condicion.ToLower()) || x.Nombre.ToLower().Contains(condicion.ToLower()));

                    if (!string.IsNullOrEmpty(tipo))
                        results = results.Where(x => x.Tipo == tipo).AsQueryable();


                    dt = results.OrderBy(x => x.Nombre).ToList().Select(x => new
                    {
                        Tipo = x.Tipo == "S" ? "Servicio" : (x.Tipo == "P" ? "Producto" : "Combo"),
                        Codigo = x.Codigo.ToUpper(),
                        Nombre = x.Nombre.ToUpper(),
                        CodigoBarras = x.Tipo != "S" ? x.CodigoBarras : "",
                        CodigoOem = x.Tipo != "S" ? x.CodigoOEM : "",
                        Descripcion = x.Descripcion,
                        Estado = x.Estado == "A" ? "Activo" : "Inactivo",
                        Precio = x.PrecioUnitario,
                        CostoInterno = x.CostoInterno.HasValue ? x.CostoInterno.Value : 0,
                        Rentabilidad = x.Rentabilidad.ToString("#0.00"),
                        Iva = x.Iva.ToString("#0.00"),
                        //  StockTotal = x.Tipo == "S" ? 0 : (x.Tipo == "P" ? x.StockConceptos.Sum(y => y.Stock) : (InventariosCommon.ObtenerStockTotal(x).StockActual)),
                        Stock = x.Tipo == "S" ? "" : (InventariosCommon.ObtenerStockTotal(x).StockActual).ToMoneyFormat(2),
                        StockMinimo = x.StockMinimo ?? 0,
                        Rubro = x.Tipo != "S" ? x.Rubros.Nombre : "",
                        SubRubro = x.Tipo != "S" && x.IDSubRubro.HasValue ? x.Rubros1.Nombre : "",
                        Proveedor = (x.IDPersona.HasValue && x.Personas != null) ? (x.Personas.RazonSocial ?? "") : "",
                        Observaciones = x.Observaciones,
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }

        }

        public static string ObtenerHistorial(int id, int idUsuario)
        {
            var html = "";
            using (var dbContext = new ACHEEntities())
            {
                var result = ConceptosCommon.ObtenerHistorialPrecios(id, idUsuario, 1, 50);
                if (result.Items.Any())
                {
                    foreach (var detalle in result.Items)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.Fecha + "</td>";
                        html += "<td>" + detalle.Proveedor + "</td>";
                        html += "<td>" + detalle.CostoInterno + "</td>";
                        html += "<td>" + detalle.Rentabilidad + "</td>";
                        html += "<td>" + detalle.Precio + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }
            return html;



        }

        public static string ObtenerReservas(int id, int idUsuario, string tipo)
        {
            var html = "";
            using (var dbContext = new ACHEEntities())
            {
                var result = InventariosCommon.ObtenerReservasDeProducto(idUsuario, id, tipo);
                if (result.Any())
                {
                    foreach (var detalle in result)
                    {
                        html += "<tr id='trReserva_" + detalle.ID + "'>";
                        html += "<td>" + detalle.Fecha.ToString("dd/MM/yyyy HH:mm") + "</td>";
                        html += "<td>" + detalle.NroOrden + "</td>";
                        html += "<td>" + detalle.Origen + "</td>";
                        html += "<td style='max-width:330px'>" + detalle.Cliente + "</td>";
                        html += "<td>" + detalle.Cantidad.ToMoneyFormat(2) + "</td>";
                        if (detalle.PuedeEliminar)
                        {
                            html += "<td><a onclick='anularOrdenVenta(" + detalle.ID + ");' style='cursor: pointer; font-size: 16px' title='Anular'><i class='fa fa-trash-o'></i></a></td>";
                        }
                        else
                            html += "<td><a onclick=\"verDetalleCombo(" + detalle.IDCombo + ",'" + detalle.ComboDesc + "');\" style='cursor: pointer;' title='Ver detalle'>SKU Combo: " + detalle.ComboDesc + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='5'>No hay un detalle disponible</td></tr>";
            }

            return html;
        }

        public static string ObtenerStock(int id)
        {

            var html = "";

            using (var dbContext = new ACHEEntities())
            {
                var result = InventariosCommon.ObtenerStockDetallado(id);
                if (result.Any())
                {
                    foreach (var detalle in result)
                    {
                        html += "<tr>";
                        html += "<td>" + detalle.Nombre + "</td>";
                        html += "<td>" + detalle.Cantidad.ToMoneyFormat(2) + "</td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='2'>No hay un detalle disponible</td></tr>";
            }

            return html;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Facturacion;
using ACHE.Negocio.Common;
using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Negocio.Inventario;

namespace ACHE.Negocio.Ventas
{
    public class FacturaElectronicaCommon
    {
        public static ComprobantesDescargasViewModel generarCae(ComprobanteCartDto comprobante)
        {
            using (var dbContext = new ACHEEntities())
            {
                return generarCae(dbContext, comprobante);
            }

        }
        public static ComprobantesDescargasViewModel generarCae(ACHEEntities dbContext, ComprobanteCartDto comprobante)
        {
            // Guardando el comprobante
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

            if (comprobante == null || comprobante.GetTotal() == 0)
                throw new CustomException("No se puedo generar la factura electrónica, por favor vuelva a iniciar la sesión.");
            if (ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, Convert.ToDateTime(comprobante.FechaComprobante)))
                throw new CustomException("No puede agregar ni modificar una comprobante que se encuentre en un periodo cerrado.");
            if (comprobante.IDJuresdiccion != null && comprobante.IDJuresdiccion == 0 &&
                ComprobanteCart.Retrieve().PercepcionIIBB > 0)
                throw new CustomException("Si informa el importe de IIBB debe informar la jurisdicción.");
            if (comprobante.IDJuresdiccion != null && comprobante.IDJuresdiccion > 0 &&
                ComprobanteCart.Retrieve().PercepcionIIBB == 0)
                throw new CustomException("Si informa la jurisdicción debe informar el IIBB.");

            var archivos = new ComprobantesDescargasViewModel();
            
                var compCartDto = comprobante;
                compCartDto.IDUsuario = usu.IDUsuario;
                compCartDto.EmailUsuario = usu.Email;
                var entity = ComprobantesCommon.Guardar(dbContext, compCartDto);

                string afipObs = "";

                var nroComprobante = entity.Numero.ToString();

            global::Common.CrearComprobante(usu, entity.IDComprobante, entity.IDPersona, entity.Tipo, entity.Modo,
                    entity.FechaComprobante.ToString("dd/MM/yyyy"), entity.CondicionVenta, entity.TipoConcepto,
                    entity.FechaVencimiento.ToString("dd/MM/yyyy"), entity.IDPuntoVenta, ref nroComprobante,
                    entity.Observaciones, global::Common.ComprobanteModo.Generar, ref afipObs);
            
                archivos.Observaciones = afipObs;

                var persona = dbContext.Personas.FirstOrDefault(x => x.IDUsuario == usu.IDUsuario && x.IDPersona == entity.IDPersona);

                archivos.Comprobante = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + entity.Tipo + "-" + nroComprobante + ".pdf";
                archivos.Remito = persona.RazonSocial.RemoverCaracteresParaPDF() + "_" + "R-" + nroComprobante + ".pdf";
                archivos.IDComprobante = entity.IDComprobante;
                if (usu.EnvioAutomaticoComprobante)
                    ComprobantesCommon.EnviarComprobanteAutomaticamente(entity.IDComprobante, usu);
                if (entity.Modo == "E")
                    InventariosCommon.AlertaStock(dbContext, entity.IDComprobante, usu.IDUsuario);

            
            return archivos;

        }
    }
}

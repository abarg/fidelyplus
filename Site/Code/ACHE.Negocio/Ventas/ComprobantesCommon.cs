﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Productos;
using ACHE.Model.Negocio;
using ACHE.FacturaElectronica;
using System.Web;
using System.Configuration;
using System.IO;
using ACHE.Negocio.Common;
using System.Data.Entity.Validation;
using System.Diagnostics;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Banco;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;
using ACHE.FacturaElectronicaModelo;
using ACHE.Negocio.Helpers;
using ACHE.Negocio.Inventario;
using ACHE.Negocio.Ventas;
using System.Data;
using System.Data.Entity;
using iText.Kernel.Pdf;


namespace ACHE.Negocio.Facturacion
{
    public enum ComprobanteModo
    {
        Previsualizar = 1,
        Generar = 2,
        GenerarRemito = 3
    }

    public static class ComprobantesCommon
    {
        public const int BANCO_MP = 80;

        public static FETipoComprobante ObtenerTipoComprobante(string tipo)
        {
            FETipoComprobante tipoComprobante = new FETipoComprobante();
            switch (tipo)
            {
                case "FCA":
                    tipoComprobante = FETipoComprobante.FACTURAS_A;
                    break;
                case "FCB":
                    tipoComprobante = FETipoComprobante.FACTURAS_B;
                    break;
                case "FCC":
                    tipoComprobante = FETipoComprobante.FACTURAS_C;
                    break;
                case "FCE":
                    tipoComprobante = FETipoComprobante.FACTURAS_E;
                    break;
                case "NCA":
                    tipoComprobante = FETipoComprobante.NOTAS_CREDITO_A;
                    break;
                case "NCB":
                    tipoComprobante = FETipoComprobante.NOTAS_CREDITO_B;
                    break;
                case "NCC":
                    tipoComprobante = FETipoComprobante.NOTAS_CREDITO_C;
                    break;
                case "NCE":
                    tipoComprobante = FETipoComprobante.NOTAS_CREDITO_E;
                    break;
                case "NDA":
                    tipoComprobante = FETipoComprobante.NOTAS_DEBITO_A;
                    break;
                case "NDB":
                    tipoComprobante = FETipoComprobante.NOTAS_DEBITO_B;
                    break;
                case "NDC":
                    tipoComprobante = FETipoComprobante.NOTAS_DEBITO_C;
                    break;
                case "NDE":
                    tipoComprobante = FETipoComprobante.NOTAS_DEBITO_E;
                    break;
                case "RCA":
                    tipoComprobante = FETipoComprobante.RECIBO_A;
                    break;
                case "RCB":
                    tipoComprobante = FETipoComprobante.RECIBO_B;
                    break;
                case "RCC":
                    tipoComprobante = FETipoComprobante.RECIBO_C;
                    break;
                case "SIN":
                case "COT":
                    tipoComprobante = FETipoComprobante.SIN_DEFINIR;
                    break;
                case "PRE":
                    tipoComprobante = FETipoComprobante.PRESUPUESTO;
                    break;
            }
            return tipoComprobante;
        }

        #region Guardar

        public static Comprobantes Guardar(ACHEEntities dbContext, ComprobanteCartDto compCart)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(compCart.TipoComprobante))
                    throw new CustomException("El tipo de comprobante es obligatorio");
                else if (string.IsNullOrWhiteSpace(compCart.Modo))
                    throw new CustomException("El modo es obligatorio");
                // else if (string.IsNullOrWhiteSpace(compCart.CondicionVenta))
                // throw new CustomException("La condición de venta es obligatoria");
                else if (string.IsNullOrWhiteSpace(compCart.TipoConcepto))
                    throw new CustomException("El tipo de concepto es obligatorio");
                else if (compCart.TipoComprobante != "COT" && ContabilidadCommon.ValidarFechaLimiteCarga(compCart.IDUsuario, compCart.FechaComprobante))
                    throw new CustomException("No puede agregar ni modificar un comprobante con fecha anterior a la fecha límite de carga.");

                var usu = dbContext.Usuarios.Where(x => x.IDUsuario == compCart.IDUsuario).FirstOrDefault();
                Personas persona =
                    dbContext.Personas.Where(x => x.IDPersona == compCart.IDPersona && x.IDUsuario == usu.IDUsuario)
                        .FirstOrDefault();

                if (persona == null)
                    throw new CustomException("El cliente/proveedor es inexistente");

                bool ComprobanteExistente = false;
                int nro = Convert.ToInt32(compCart.Numero);

                Comprobantes entity;

                //obtengo el prox. nro de cot
                if (nro == 0 && compCart.TipoComprobante == "COT")
                {
                    if (dbContext.Comprobantes.Any(x => x.IDUsuario == usu.IDUsuario && x.Tipo == compCart.TipoComprobante))
                    {
                        var aux =
                            dbContext.Comprobantes.Where(
                                x => x.IDUsuario == usu.IDUsuario && x.Tipo == compCart.TipoComprobante && x.IDPuntoVenta == compCart.IDPuntoVenta)
                                .ToList();
                        if (aux != null && aux.Count() > 0)
                            nro = (aux.Max(x => x.Numero) + 1);
                        else
                            nro = 1;
                    }
                    else
                        nro = 1;


                    compCart.Numero = nro.ToString();
                }
                var ordenVentaAnterior = 0;
                if (compCart.IDComprobante > 0)
                {
                    entity =
                        dbContext.Comprobantes.Include("ComprobantesDetalle")
                            .Where(x => x.IDComprobante == compCart.IDComprobante && x.IDUsuario == usu.IDUsuario)
                            .FirstOrDefault();
                    if (entity == null)
                        throw new CustomException("El ID informado es inexistente");
                    ordenVentaAnterior = entity.IDOrdenVenta ?? 0;

                    entity.UsuarioModifica = compCart.EmailUsuario;
                }
                else
                {
                    //int nro = Convert.ToInt32(compCart.Numero);
                    ComprobanteExistente =
                        dbContext.Comprobantes.Any(
                            x =>
                                x.Tipo == compCart.TipoComprobante && x.Numero == nro && x.IDPersona == compCart.IDPersona &&
                                x.IDPuntoVenta == compCart.IDPuntoVenta && x.IDUsuario == usu.IDUsuario);
                    entity = new Comprobantes();
                    entity.FechaAlta = DateTime.Now;
                    entity.IDUsuario = usu.IDUsuario;
                    entity.UsuarioAlta = compCart.EmailUsuario;
                    if (compCart.IDUsuarioAdicional > 0)//PREGUNTAR SI ESTO ES SOLO EN EL ALTA O SIEMPRE
                    {
                        entity.IDUsuarioAdicional = compCart.IDUsuarioAdicional;
                        var comision = dbContext.UsuariosAdicionales.Where(x => x.IDUsuarioAdicional == compCart.IDUsuarioAdicional).Select(x => x.PorcentajeComision).FirstOrDefault();
                        entity.Comision = comision;
                    }
                }

                if (nro > 0 && ComprobanteExistente && compCart.Modo != "E")
                {
                    if (ComprobanteExistente && compCart.IDComprobante == -1)//es importacion y se actualizan los datos.
                    {
                        entity = dbContext.Comprobantes.First(
                            x =>
                                x.Tipo == compCart.TipoComprobante && x.Numero == nro && x.IDPersona == compCart.IDPersona &&
                                x.IDPuntoVenta == compCart.IDPuntoVenta && x.IDUsuario == usu.IDUsuario);

                        compCart.IDComprobante = entity.IDComprobante;
                    }
                    else
                        throw new Exception("Ya existe el tipo de comprobante y nro comprobante para el proveedor/Cliente seleccionado");
                }

                entity.IDPersona = compCart.IDPersona;
                entity.Tipo = compCart.TipoComprobante;
                entity.Modo = (compCart.TipoComprobante == "COT") ? "O" : compCart.Modo;
                entity.FechaComprobante = compCart.FechaComprobante;
                if (entity.FechaComprobante.Date != compCart.FechaComprobante.Date)//para que agregue la hora si es una edicion/alta
                    entity.FechaComprobante = compCart.FechaComprobante.Date.Add(DateTime.Now.TimeOfDay);
                else
                    entity.FechaComprobante = compCart.FechaComprobante.Date.Add(entity.FechaAlta.TimeOfDay);

                entity.TipoDestinatario = persona.Tipo.ToUpper();
                entity.CondicionVenta = "";//despues sacar ahora por que es obligatorio 
                entity.IDCondicionVenta = compCart.IDCondicionVenta ?? 0;

                CondicionesVentas condicionVenta;
                if (entity.IDCondicionVenta > 0)
                {
                    condicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, entity.IDCondicionVenta);
                    entity.CondicionVenta = condicionVenta.Nombre;//este valor se usa para FCElectronica
                }
                else
                {
                    condicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, usu.IDUsuario, compCart.CondicionVenta);
                    if (condicionVenta != null)
                    {
                        entity.IDCondicionVenta = condicionVenta.IDCondicionVenta;
                        entity.CondicionVenta = condicionVenta.Nombre;
                    }
                    else
                    {//fix
                        condicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, usu.IDUsuario, "Cuenta Corriente");
                        entity.IDCondicionVenta = condicionVenta.IDCondicionVenta;
                        entity.CondicionVenta = condicionVenta.Nombre;
                    }
                }

                entity.NroDocumento = persona.NroDocumento;
                entity.TipoDocumento = persona.TipoDocumento;
                entity.FechaVencimiento = compCart.FechaVencimiento;
                entity.IDPuntoVenta = compCart.IDPuntoVenta;

                entity.PercepcionIVA = compCart.PercepcionIVA;
                entity.PercepcionIIBB = compCart.PercepcionIIBB;
                entity.ImporteNoGrabado = compCart.ImporteNoGrabado;
                entity.IDInventario = compCart.IdInventario;
                entity.IDOrdenVenta = compCart.IdOrdenVenta;

                if (!entity.IDInventario.HasValue || entity.IDInventario.Value == 0)
                {
                    var inventario = dbContext.Inventarios.Where(x => x.IDUsuario == entity.IDUsuario).FirstOrDefault();
                    if (inventario != null)
                    {
                        entity.IDInventario = inventario.IDInventario;
                    }
                }

                if (!entity.IDIntegracion.HasValue && compCart.IdIntegracion > 0)
                    entity.IDIntegracion = compCart.IdIntegracion;
                if (string.IsNullOrEmpty(entity.IDVentaIntegracion))
                    entity.IDVentaIntegracion = compCart.IdVentaIntegracion;

                if (compCart.IDJuresdiccion > 0)
                    entity.IDJurisdiccion = compCart.IDJuresdiccion;

                if (compCart.Modo != "E" || compCart.TipoComprobante == "COT")
                {
                    entity.Numero = int.Parse(compCart.Numero);
                    if (compCart.Modo != "E")
                        entity.FechaProceso = DateTime.Now;
                }
                entity.TipoConcepto = int.Parse(compCart.TipoConcepto);
                entity.Observaciones = compCart.Observaciones;

                

                if (entity.IDComprobante > 0 && entity.ComprobantesDetalle.Any())
                {
                    InventariosCommon.RestaurarStockComprobante(dbContext, entity.Tipo, usu.IDUsuario, entity.ComprobantesDetalle.ToList(), entity.IDInventario ?? 0, entity.IDComprobante);

                }

                if (entity.ComprobantesDetalle.Any())
                    dbContext.ComprobantesDetalle.RemoveRange(entity.ComprobantesDetalle);

                decimal totalNeto = 0;
                decimal totalBruto = 0;

                if (entity.Tipo != "NCA" && entity.Tipo != "NCB" && entity.Tipo != "NCC" && entity.Tipo != "NCE")
                {
                    var stockValido = InventariosCommon.StockValido(dbContext, compCart.Items, usu.IDUsuario,
                        entity.IDInventario ?? 0, compCart);
                    if (stockValido.Any())
                    {
                        if (stockValido.Count() == 1)
                        {
                            throw new Exception("El producto " + stockValido[0] + " no tiene stock suficiente");
                        }
                        else
                        {
                            var result = "";
                            bool first = true;
                            foreach (var prod in stockValido)
                            {
                                if (first)
                                {
                                    result += prod;
                                    first = false;
                                }
                                else
                                    result += "," + prod;
                            }
                            throw new Exception("Los productos " + result + " no tienen stock suficiente");

                        }
                    }
                }

                foreach (var det in compCart.Items)
                {

                    ComprobantesDetalle compDet = new ComprobantesDetalle();
                    compDet.IDComprobante = entity.IDComprobante;
                    compDet.PrecioUnitario = det.PrecioUnitario;
                    compDet.Iva = det.Iva;
                    compDet.Concepto = det.Concepto;
                    compDet.Cantidad = det.Cantidad;
                    compDet.Bonificacion = det.Bonificacion;
                    if (det.IDPlanDeCuenta > 0)
                        compDet.IDPlanDeCuenta = det.IDPlanDeCuenta;

                    if (det.IDConcepto != null && det.IDConcepto != 0)
                        compDet.IDConcepto = det.IDConcepto;
                    if (det.IDAbonos != null && det.IDAbonos != 0)
                        compDet.IDAbono = det.IDAbonos;

                    totalNeto += det.TotalConIva;
                    totalBruto += det.TotalSinIva;
                    entity.ComprobantesDetalle.Add(compDet);
                }
                decimal Tributos = 0;
                if (compCart.Tributos != null)
                {
                    foreach (var det in compCart.Tributos)
                    {

                        ComprobantesTributos compTri = new ComprobantesTributos();
                        compTri.IDComprobante = entity.IDComprobante;
                        compTri.BaseImp = det.BaseImp;
                        compTri.Iva = det.Alicuota;
                        compTri.Descripcion = det.Descripcion ?? "";
                        compTri.Importe = det.Importe;
                        compTri.Tipo = det.Tipo;

                        Tributos += det.Importe;
                        //totalBruto += det.TotalSinIva;
                        entity.ComprobantesTributos.Add(compTri);
                    }
                }


                if (!entity.IDInventario.HasValue && entity.ComprobantesDetalle.Any(x => x.IDConcepto.HasValue))
                {
                    var inventario = dbContext.Inventarios.Where(x => x.IDUsuario == entity.IDUsuario).FirstOrDefault();
                    if (inventario != null)
                        entity.IDInventario = inventario.IDInventario;
                    else
                        throw new Exception("Por favor, configure su inventario principal");
                }

                //Determinamos cantidad de decimales que posee configurado el usuario
                //var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

                //Tributos += compCart.GetImporteNoGrabado() + compCart.GetPercepcionIIBB() + compCart.GetPercepcionIVA();
                Tributos += compCart.GetPercepcionIIBB() + compCart.GetPercepcionIVA();

                //entity.ImporteTotalNeto = totalNeto + Tributos; //Math.Round(totalNeto + Tributos, cantidadDecimales);


                //TODO DECIMALES: SE REDONDEA A 2 DECIMALES
                entity.ImporteTotalNeto = Math.Round(totalNeto + Tributos, 2);
                entity.ImporteTotalBruto = Math.Round(totalBruto, 2);


                //entity.ImporteTotalNeto = totalNeto + Tributos;

                if (entity.ImporteTotalNeto == 0 && entity.ImporteNoGrabado == 0)
                    throw new CustomException("No puede generar una factura con Importe 0");



                if (compCart.IDComprobante > 0)
                {
                    dbContext.SaveChanges();
                    dbContext.ActualizarSaldosPorComprobante(compCart.IDComprobante);
                }
                else
                {
                    entity.Saldo = entity.ImporteTotalNeto;

                    dbContext.Comprobantes.Add(entity);
                    dbContext.SaveChanges();
                }
                //ContabilidadCommon.AgregarAsientoDeVentas(usu, entity.IDComprobante);


               
                //stock
                InventariosCommon.ActualizarStockComprobante(dbContext, entity.Tipo, usu.IDUsuario, entity.ComprobantesDetalle.ToList(), entity.IDInventario ?? 0, entity.IDComprobante);



                //var cond = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, entity.IDUsuario, entity.IDCondicionVenta );
                if (condicionVenta.CobranzaAutomatica && entity.Tipo != "NCA" && entity.Tipo != "NCB" && entity.Tipo != "NCC" && entity.Tipo != "NCE") //Genero cobranza automática
                {
                    ComprobantesCommon.CrearCobranzaPorMPAutomática(dbContext, entity, usu.IDUsuario, entity.IDCondicionVenta);
                }
                dbContext.SaveChanges();


                return entity;
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static Comprobantes GuardarComprobante(ComprobanteCartDto compCart)
        {
            using (var dbContext = new ACHEEntities())
            {
                return Guardar(dbContext, compCart);
            }
        }

        public static Comprobantes GuardarComprobante(ACHEEntities dbContext, ComprobanteCartDto compCart)
        {

            return Guardar(dbContext, compCart);

        }

        public static Comprobantes GuardarComprobante(ComprobanteCartDto compCart, Personas persona)
        {
            using (var dbContext = new ACHEEntities())
            {

                int idPersona = 0;
                var existe =
                    dbContext.Personas.Where(
                        x =>
                            x.IDUsuario == compCart.IDUsuario && x.NroDocumento == persona.NroDocumento &&
                            x.TipoDocumento == persona.TipoDocumento).FirstOrDefault();
                if (existe != null)
                    idPersona = existe.IDPersona;

                idPersona = PersonasCommon.GuardarPersonas(dbContext, idPersona, persona.RazonSocial,
                    persona.NombreFantansia, persona.CondicionIva, persona.Personeria, persona.TipoDocumento,
                    persona.NroDocumento, persona.Telefono,
                    persona.Email, persona.EmailsEnvioFc, persona.Tipo, persona.IDPais, persona.IDProvincia, persona.IDCiudad,
                    persona.Domicilio, persona.PisoDepto, persona.CodigoPostal, persona.Observaciones,
                    persona.IDListaPrecio ?? 0, persona.Codigo, persona.SaldoInicial ?? 0, compCart.IDUsuario, "0");

                compCart.IDPersona = idPersona;
                return Guardar(dbContext, compCart);
            }
        }

        //      public static void GuardarComprobantesEnviados(ACHEEntities dbContext, int? idcomprobante, string mensajeOriginal, int? idCobranza,
        //  string mensaje, bool resultado, WebUser usu, string destinatarios) {
        //          try {
        //              var entity = new ComprobantesEnviados();
        //              entity.IDUsuario = usu.IDUsuario;
        //              entity.Mensaje = mensaje;
        //              entity.Resultado = resultado;
        //              entity.MensajeOriginal = mensajeOriginal;
        //              entity.FechaEnvio = DateTime.Now;
        //              entity.Destinatarios = destinatarios;
        //              if (idcomprobante != null)
        //                  entity.IDComprobante = idcomprobante;
        //              if (idCobranza != null)
        //                  entity.IDCobranza = idCobranza;
        //              dbContext.ComprobantesEnviados.Add(entity);
        //              dbContext.SaveChanges();
        //          }
        //          catch (Exception ex) {
        //              throw new Exception(ex.Message);
        //          }
        //      }

        //      public static void GuardarEnvioComprobanteMail(string nombre, string para, string asunto, string mensaje,
        //int year, string file, string logo, int idUsuario, string emailUsuario, string displayFrom, int idComprobante) {
        //          MailAddressCollection listTo = new MailAddressCollection();
        //          bool esMailDeML = false;

        //          if (para.Contains("mail.mercadolibre.com"))
        //              esMailDeML = true;

        //          foreach (var mail in para.Split(',')) {
        //              if (mail != string.Empty) {
        //                  listTo.Add(new MailAddress(mail));
        //              }
        //          }

        //          if (esMailDeML) {
        //              mensaje = mensaje.CleanHtmlTags();
        //              mensaje = mensaje.RemoverAcentosHTML();
        //              mensaje = mensaje.Replace("<USUARIO>", "");
        //              mensaje = mensaje.Replace("<p>", "");
        //              mensaje = mensaje.Replace("</p>", "");
        //          }
        //          else {
        //              mensaje = mensaje.ReplaceAll("\n", "<br/>");
        //              mensaje = mensaje.Replace("LINKPAGO", "https://clientes.contabilium.com/public/factura?comprobante=" + Cryptography.Encrypt(idComprobante.ToString(), true));
        //          }

        //          ListDictionary replacements = new ListDictionary();
        //          replacements.Add("<NOTIFICACION>", mensaje);
        //          if (nombre.Trim() != string.Empty) {
        //              replacements.Add("<CLIENTE>", nombre);
        //              replacements.Add("<USUARIO>", nombre);
        //          }
        //          else {
        //              replacements.Add("<CLIENTE>", "usuario");
        //              replacements.Add("<USUARIO>", "usuario");
        //          }

        //          if (logo != string.Empty)
        //              replacements.Add("<LOGOEMPRESA>", "/files/usuarios/" + logo);
        //          else
        //              replacements.Add("<LOGOEMPRESA>", "/images/logo-mail-gris.png");


        //          List<string> attachments = new List<string>();
        //          var physicalPath = HttpContext.Current.Server.MapPath("~/files/explorer/" + idUsuario + "/comprobantes/" + year.ToString() + "/" + file);
        //          physicalPath = physicalPath.Replace("rest", "app");//TODO: FIx para WebApi. Mejorar

        //          attachments.Add(physicalPath);

        //          bool send = false;

        //          if (!esMailDeML)
        //              send = EmailCommon.SaveMessage(idUsuario, EmailTemplate.EnvioComprobanteConFoto, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuario, asunto, attachments, displayFrom);
        //          else
        //              send = EmailCommon.SaveMessage(idUsuario, EmailTemplate.EnvioComprobanteSinFormato, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuario, asunto, attachments, displayFrom);

        //          if (!send)
        //              throw new Exception("El mensaje no pudo ser enviado. Por favor, intente nuevamente. En caso de continuar el error, escribenos a <a href='" + ConfigurationManager.AppSettings["Email.Ayuda"] + "'>" + ConfigurationManager.AppSettings["Email.Ayuda"] + "</a>");
        //      }

        #endregion

        #region Obtener

        public static string ObtenerTipoDeFacturaAFacturar(string CondicionIva)
        {
            string tipoDeFactura = string.Empty;
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            if (usu.CondicionIVA == "MO")
            {
                tipoDeFactura = "FCC";
            }
            else
            {
                switch (CondicionIva)
                {
                    case "RI":
                        tipoDeFactura = usu.CondicionIVA == "RI" ? "FCA" : "FCB";
                        break;
                    case "EX":
                        tipoDeFactura = usu.CondicionIVA == "RI" ? "FCB" : "FCC";
                        break;
                    default:
                        tipoDeFactura = usu.CondicionIVA == "RI" ? "FCB" : "FCC";
                        break;
                }
            }
            return tipoDeFactura;

        }

        public static Comprobantes ObtenerPorID(int id, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Comprobantes.Include("PuntosDeVenta").Include("Personas").Include("ComprobantesDetalle")
                    .Include("ComprobantesDetalle.Conceptos").Include("CondicionesVentas").Include("ComprobantesTributos")
                        .Where(x => x.IDUsuario == idUsuario && x.IDComprobante == id).FirstOrDefault();

                return entity;
            }
        }

        public static FEComprobante ObtenerComprobanteElectronica(long cuit, long CbtNro, int PtoVta, string tipoComprobante)
        {
            FEFacturaElectronica fe = new FEFacturaElectronica();
            var tipo = ObtenerTipoComprobante(tipoComprobante);
            FEComprobante comprobante = fe.GetComprobante(cuit, CbtNro, PtoVta, tipo);
            return comprobante;
        }

        public static string ObtenerTipoDeFacturaAFacturar(string SUCondicionIva, string MiCondicionIva)
        {
            string tipoDeFactura = string.Empty;
            if (MiCondicionIva == "MO")
            {
                tipoDeFactura = "FCC";
            }
            else
            {
                switch (SUCondicionIva)
                {
                    case "RI":
                        tipoDeFactura = "FCA";
                        break;
                    default:
                        tipoDeFactura = "FCB";
                        break;
                }
            }
            return tipoDeFactura;
        }

        public static string ObtenerProxNroComprobante(string tipo, int idUsuario, int idPuntoDeVenta)
        {
            try
            {
                var nro = "";
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.Comprobantes.Any(x => x.IDUsuario == idUsuario && x.Tipo == tipo))
                    {
                        var aux =
                            dbContext.Comprobantes.Where(
                                x => x.IDUsuario == idUsuario && x.Tipo == tipo && x.IDPuntoVenta == idPuntoDeVenta)
                                .ToList();
                        if (aux != null && aux.Count() > 0)
                            nro = (aux.Max(x => x.Numero) + 1).ToString("#00000000");
                        else
                            nro = "00000001";
                    }
                    else
                        nro = "00000001";
                }

                return nro;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static ResultadosComprobantesViewModel ObtenerComprobantes(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize, WebUser usu)
        {
            return ObtenerComprobantes(condicion, periodo, fechaDesde, fechaHasta, page, pageSize, usu.IDUsuario);
        }

        public static ResultadosComprobantesViewModel ObtenerComprobantes(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                    var ptosVentaIds = UsuarioCommon.ObtenerPuntosDeVenta(usu).Select(p => p.ID).ToList();

                    condicion = (!string.IsNullOrWhiteSpace(condicion)) ? condicion.ToLower() : "";
                    var results =
                        dbContext.Comprobantes.Include("Personas")
                            .Include("PuntosDeVenta")
                            .Where(x => x.IDUsuario == idUsu && ptosVentaIds.Contains(x.PuntosDeVenta.IDPuntoVenta))
                            .AsQueryable();

                    Int32 numero = 0;

                    if (Int32.TryParse(condicion, out numero))
                        results = results.Where(x => x.Numero == Math.Abs(numero));
                    else if (condicion.Contains("-"))
                    {
                        var punto = (string.IsNullOrWhiteSpace(condicion.Split("-")[0])) ? "" : condicion.Split("-")[0];
                        var nro = (string.IsNullOrWhiteSpace(condicion.Split("-")[1])) ? "" : condicion.Split("-")[1];

                        Int32 ptoAux = 0;
                        Int32 nroAux = 0;

                        if (Int32.TryParse(punto, out ptoAux) || Int32.TryParse(nro, out nroAux))
                        {
                            if (punto != "" && Int32.TryParse(punto, out ptoAux))
                                results = results.Where(x => x.PuntosDeVenta.Punto == ptoAux);

                            if (nro != "" && Int32.TryParse(nro, out nroAux))
                                results = results.Where(x => x.Numero == nroAux);
                        }
                        else
                            results =
                                results.Where(
                                    x =>
                                        x.Personas.RazonSocial.ToLower().ToLower().Contains(condicion) ||
                                        x.Personas.NombreFantansia.ToLower().Contains(condicion) ||
                                        x.Tipo.ToLower().Contains(condicion));
                    }
                    else if (condicion != "")
                        results =
                            results.Where(
                                x =>
                                    x.Personas.RazonSocial.ToLower().Contains(condicion) ||
                                    x.Personas.NombreFantansia.ToLower().Contains(condicion) ||
                                    x.Tipo.ToLower().Contains(condicion));

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(fechaDesde))
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta))
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaComprobante <= dtHasta);
                    }

                    page--;
                    ResultadosComprobantesViewModel resultado = new ResultadosComprobantesViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    //Determinamos el formato decimal
                    var decimalStringFormat = "N2";// DecimalFormatter.GetDecimalStringFormat("N2");

                    var list = results.OrderBy(x => x.FechaComprobante)
                        .ThenBy(x => x.Numero)
                        .Skip(page * pageSize)
                        .Take(pageSize)
                        .ToList()
                        .Select(x => new ComprobantesViewModel()
                        {
                            ID = x.IDComprobante,
                            RazonSocial =
                                (x.Personas.NombreFantansia == ""
                                    ? x.Personas.RazonSocial.ToUpper()
                                    : x.Personas.NombreFantansia.ToUpper()),
                            Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                            Tipo = x.Tipo,
                            Modo = x.Modo == "E" ? "Electrónica" : (x.Modo == "T" ? "Talonario" : "Otro"),
                            Numero = x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                            ImporteTotalNeto =
                                (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE")
                                    ? "-" + x.ImporteTotalNeto.ToString(decimalStringFormat)
                                    : x.ImporteTotalNeto.ToString(decimalStringFormat),
                            ImporteTotalBruto =
                                (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE")
                                    ? "-" + x.ImporteTotalBruto.ToString(decimalStringFormat)
                                    : x.ImporteTotalBruto.ToString(decimalStringFormat),
                            PuedeAdm = x.FechaCAE.HasValue ? "F" : "T",
                            PuedeAnular = (x.Tipo == "FCA" || x.Tipo == "FCB" || x.Tipo == "FCC" || x.Tipo == "FCM" || x.Tipo == "FCE") ? "T" : "F",
                            IdPersona = x.IDPersona,
                            TipoConcepto = x.TipoConcepto,
                            CondicionVenta = x.CondicionesVentas.Nombre,
                            FechaAlta = x.FechaAlta,
                            FechaVencimiento = x.FechaVencimiento,
                            Observaciones = x.Observaciones,
                            IdPuntoVenta = x.IDPuntoVenta,
                            Cae = x.CAE,
                            Saldo = x.Saldo
                        });
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static List<RptComisionesViewModel> ObtenerComprobantesPorUsuarioAdicional(int idUsuarioAdic, string fechaDesde, string fechaHasta, int idUsu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
                    var ptosVentaIds = UsuarioCommon.ObtenerPuntosDeVenta(usu).Select(p => p.ID).ToList();

                    var results =
                      dbContext.Comprobantes.Include("Personas")
                          .Include("PuntosDeVenta")
                          .Where(x => x.IDUsuario == idUsu && ptosVentaIds.Contains(x.PuntosDeVenta.IDPuntoVenta) && x.IDUsuarioAdicional == idUsuarioAdic)
                          .AsQueryable();


                    if (!string.IsNullOrWhiteSpace(fechaDesde))
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta))
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaComprobante <= dtHasta);
                    }


                    //Determinamos el formato decimal
                    var decimalStringFormat = "N2";// DecimalFormatter.GetDecimalStringFormat(idUsu);

                    var list = results.OrderBy(x => x.FechaComprobante)
                        .ThenBy(x => x.Numero)
                        .ToList()
                        .Select(x => new RptComisionesViewModel()
                        {
                            IDUsuarioAdicional = x.IDUsuarioAdicional ?? 0,
                            UsuarioAdicional = x.UsuariosAdicionales.Email,
                            Tipo = x.Tipo,
                            Numero = x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                            Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),

                            TotalString =
                                (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE")
                                    ? "-" + x.ImporteTotalBruto.ToString(decimalStringFormat)
                                    : x.ImporteTotalBruto.ToString(decimalStringFormat),
                            ComisionString =
                                  (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE")
                                      ? "-" + ((x.ImporteTotalBruto * x.Comision ?? 0) / 100).ToString(decimalStringFormat)
                                      : ((x.ImporteTotalBruto * x.Comision ?? 0) / 100).ToString(decimalStringFormat),
                            Porcentaje = x.Comision ?? 0
                        }).ToList();

                    return list;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        #endregion

        #region Generar

        //public static void generarPDF(FEComprobante comprobanteAfip, string cuitEmisor, string tipoComprobante) {
        //    var docNroReceptor = comprobanteAfip.DocNro.ToString();

        //    Usuarios usuario = UsuarioCommon.ObtenerUsuarioPorNroDoc(cuitEmisor);
        //    Personas Cliente = PersonasCommon.ObtenerPersonaPorNroDoc(docNroReceptor, usuario.IDUsuario);

        //    if (Cliente == null)
        //        throw new Exception("Error al recuperar el cliente con documento " + docNroReceptor);

        //    Comprobantes comprobante = null;

        //    using (var dbContext = new ACHEEntities()) {
        //        comprobante = dbContext.Comprobantes.Include("Personas").Include("ComprobantesDetalle").Include("PuntosDeVenta").Include("ComprobantesDetalle.Conceptos")
        //            .Where(x => x.IDPersona == Cliente.IDPersona && x.IDUsuario == usuario.IDUsuario && x.CAE == comprobanteAfip.CAE).FirstOrDefault();
        //    }

        //    if (comprobante == null)
        //        throw new Exception("Error al recuperar el comprobante desde Contabilium");

        //    generarPDF(comprobanteAfip, comprobante, usuario, Cliente, tipoComprobante);
        //}

        //private static void generarPDF(FEComprobante comprobante, Comprobantes comp, Usuarios usuario, Personas Cliente, string tipo) {
        //    comprobante.Cuit = Convert.ToInt64(usuario.CUIT);
        //    comprobante.CiudadProvincia = (usuario.Ciudades == null)
        //        ? ""
        //        : usuario.Ciudades.Nombre + ", " + usuario.Provincias.Nombre;
        //    comprobante.Domicilio = usuario.Domicilio + " " + usuario.PisoDepto;
        //    comprobante.Telefono = usuario.Telefono;
        //    comprobante.ClienteCondiionIva = usuario.CondicionIva;
        //    comprobante.IIBB = usuario.IIBB;
        //    comprobante.FechaInicioActividades = Convert.ToDateTime(usuario.FechaInicioActividades).ToString("dd/MM/yyyy");
        //    comprobante.CondicionVenta = comp.CondicionVenta;
        //    comprobante.RazonSocial = usuario.RazonSocial;
        //    comprobante.ClienteCondiionIva = Cliente.CondicionIva;
        //    comprobante.ClienteDomicilio = Cliente.Domicilio + " " + Cliente.PisoDepto;
        //    comprobante.ClienteLocalidad = (Cliente.Provincias == null) ? "" : Cliente.Provincias.Nombre;
        //    comprobante.ClienteNombre = Cliente.RazonSocial;
        //    comprobante.ImpTotConc = (double)comp.ImporteNoGrabado;

        //    ComprobanteCart.Retrieve().Init(comp);
        //    var compCartDto = ComprobanteCart.Retrieve().GetComprobanteCartDto();
        //    compCartDto.IDUsuario = comp.IDUsuario;

        //    foreach (var item in compCartDto.Items) {
        //        //double precioFinal = item.;
        //        //switch (item.TipoIva)
        //        //{
        //        //    case FETipoIva.Iva10_5:
        //        //        precioFinal = precioFinal * 1.105;
        //        //        break;
        //        //    case FETipoIva.Iva21:
        //        //        precioFinal = precioFinal * 1.21;
        //        //        break;
        //        //    case FETipoIva.Iva27:
        //        //        precioFinal = precioFinal * 1.27;
        //        //        break;
        //        //    case FETipoIva.Iva5:
        //        //        precioFinal = precioFinal * 1.050;
        //        //        break;
        //        //    case FETipoIva.Iva2_5:
        //        //        precioFinal = precioFinal * 1.025;
        //        //        break;
        //        //}

        //        if (comp.Tipo == "FCA") {

        //            comprobante.ItemsDetalle.Add(new FEItemDetalle() {
        //                //Cantidad = item.Cantidad,
        //                //Descripcion = item.Concepto,
        //                //Precio = double.Parse(item.PrecioUnitario.ToString()),
        //                //PrecioParaPDF = double.Parse(item.PrecioUnitario.ToString()),
        //                //Codigo = item.Codigo,
        //                //Bonificacion = item.Bonificacion,
        //                //Iva = item.Iva
        //                Cantidad = item.Cantidad,
        //                Descripcion = item.Concepto,
        //                Precio = Math.Round(double.Parse(item.PrecioUnitarioSinIVA.ToString()), 2),
        //                PrecioParaPDF = Math.Round(double.Parse(item.PrecioUnitarioSinIVA.ToString()), 2),
        //                Codigo = item.IDConcepto.HasValue ? item.Codigo : "",
        //                Bonificacion = item.Bonificacion,
        //                Iva = item.Iva
        //            });
        //        }
        //        else {
        //            comprobante.ItemsDetalle.Add(new FEItemDetalle() {
        //                //Cantidad = item.Cantidad,
        //                //Descripcion = item.Concepto,
        //                //Precio = double.Parse(item.PrecioUnitario.ToString()),
        //                //PrecioParaPDF = double.Parse(item.PrecioUnitario.ToString()),
        //                //Codigo = item.Codigo,
        //                //Bonificacion = item.Bonificacion,
        //                //Iva = item.Iva
        //                Cantidad = item.Cantidad,
        //                Descripcion = item.Concepto,
        //                Precio = Math.Round(double.Parse(item.PrecioUnitarioConIva.ToString()), 2),
        //                PrecioParaPDF = Math.Round(double.Parse(item.PrecioUnitarioConIvaSinBonificacion.ToString()), 2),
        //                Codigo = item.IDConcepto.HasValue ? item.Codigo : "",
        //                Bonificacion = item.Bonificacion,
        //                Iva = item.Iva
        //            });
        //        }
        //    }

        //    var PathBaseWeb = ConfigurationManager.AppSettings["PathBaseWeb"];
        //    var imgLogo = "logo-fc-" + usuario.TemplateFc + ".png";
        //    if (!string.IsNullOrEmpty(usuario.Logo)) {
        //        if (File.Exists(Path.Combine(PathBaseWeb + "/files/usuarios/" + usuario.Logo)))
        //            //HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
        //            imgLogo = usuario.Logo;
        //    }

        //    var pathTemplateFc = Path.Combine(PathBaseWeb + ConfigurationManager.AppSettings["FE.Template"].Replace("~", "") + usuario.TemplateFc + ".pdf");
        //    var pathLogo = Path.Combine(PathBaseWeb + "/files/usuarios/" + imgLogo);
        //    var pathPdf = Path.Combine(PathBaseWeb + "/files/explorer/" + usuario.IDUsuario + "/Comprobantes/" + comprobante.Fecha.Year.ToString() + "/");

        //    if (!Directory.Exists(pathPdf))
        //        Directory.CreateDirectory(pathPdf);

        //    string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
        //    pathPdf = pathPdf + Cliente.RazonSocial.RemoverCaracteresParaPDF() + "_";
        //    pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

        //    FEFacturaElectronica fe = new FEFacturaElectronica();
        //    comprobante.ArchivoFactura = fe.GetStreamPDF(comprobante, pathTemplateFc, pathLogo, usuario.MostrarBonificacionFc, true);
        //    fe.GrabarEnDisco(comprobante.ArchivoFactura, pathPdf, pathTemplateFc, pathLogo);
        //}

        //private static void generarPDF(FEComprobante comprobante, Usuarios usuario, Personas Cliente, string tipo) {
        //    comprobante.Cuit = Convert.ToInt64(usuario.CUIT);
        //    comprobante.CiudadProvincia = (usuario.Ciudades == null)
        //        ? ""
        //        : usuario.Ciudades.Nombre + ", " + usuario.Provincias.Nombre;
        //    comprobante.Domicilio = usuario.Domicilio + " " + usuario.PisoDepto;
        //    comprobante.Telefono = usuario.Telefono;
        //    comprobante.ClienteCondiionIva = usuario.CondicionIva;
        //    comprobante.IIBB = usuario.IIBB;
        //    comprobante.FechaInicioActividades =
        //        Convert.ToDateTime(usuario.FechaInicioActividades).ToString("dd/MM/yyyy");
        //    comprobante.CondicionVenta = "Efectivo";

        //    comprobante.ClienteCondiionIva = Cliente.CondicionIva;
        //    comprobante.ClienteDomicilio = Cliente.Domicilio + " " + Cliente.PisoDepto;
        //    comprobante.ClienteLocalidad = (Cliente.Provincias == null) ? "" : Cliente.Provincias.Nombre;
        //    comprobante.ClienteNombre = Cliente.RazonSocial;


        //    foreach (var item in comprobante.DetalleIva) {
        //        double precioFinal = item.BaseImp;
        //        switch (item.TipoIva) {
        //            case FETipoIva.Iva10_5:
        //                precioFinal = precioFinal * 1.105;
        //                break;
        //            case FETipoIva.Iva21:
        //                precioFinal = precioFinal * 1.21;
        //                break;
        //            case FETipoIva.Iva27:
        //                precioFinal = precioFinal * 1.27;
        //                break;
        //            case FETipoIva.Iva5:
        //                precioFinal = precioFinal * 1.050;
        //                break;
        //            case FETipoIva.Iva2_5:
        //                precioFinal = precioFinal * 1.025;
        //                break;
        //        }

        //        comprobante.ItemsDetalle.Add(new FEItemDetalle() {

        //            Cantidad = 1,
        //            Descripcion = "Item Recuperado",
        //            Precio = item.BaseImp,
        //            PrecioParaPDF = Cliente.CondicionIva == "RI" ? Math.Round(double.Parse(item.BaseImp.ToString()), 2) : Math.Round(double.Parse(precioFinal.ToString()), 2),
        //            Codigo = "",
        //            Bonificacion = 0,
        //            //Iva = item.TipoIva.ToDescription() == "Iva21" ? 21 : decimal.Parse(item.TipoIva.ToDescription())//TODO: FIX ESTO AL RECUPERAR CAE
        //            Iva = decimal.Parse(item.TipoIva.ToDescription().Replace("Iva", "").Replace("_", "."))
        //        });
        //    }

        //    var PathBaseWeb = ConfigurationManager.AppSettings["PathBaseWeb"];
        //    var imgLogo = "logo-fc-" + usuario.TemplateFc + ".png";
        //    if (!string.IsNullOrEmpty(usuario.Logo)) {
        //        if (File.Exists(Path.Combine(PathBaseWeb + "/files/usuarios/" + usuario.Logo)))
        //            //HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
        //            imgLogo = usuario.Logo;
        //    }

        //    var pathTemplateFc = Path.Combine(PathBaseWeb + ConfigurationManager.AppSettings["FE.Template"].Replace("~", "") + usuario.TemplateFc + ".pdf");
        //    var pathLogo = Path.Combine(PathBaseWeb + "/files/usuarios/" + imgLogo);
        //    var pathPdf = Path.Combine(PathBaseWeb + "/files/explorer/" + usuario.IDUsuario + "/Comprobantes/" + comprobante.Fecha.Year.ToString() + "/");

        //    if (!Directory.Exists(pathPdf))
        //        Directory.CreateDirectory(pathPdf);

        //    string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" +
        //                               comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
        //    pathPdf = pathPdf + Cliente.RazonSocial.RemoverCaracteresParaPDF() + "_";
        //    pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

        //    FEFacturaElectronica fe = new FEFacturaElectronica();
        //    comprobante.ArchivoFactura = fe.GetStreamPDF(comprobante, pathTemplateFc, pathLogo,
        //        usuario.MostrarBonificacionFc, true);
        //    fe.GrabarEnDisco(comprobante.ArchivoFactura, pathPdf, pathTemplateFc, pathLogo);
        //}

        #endregion

        public static void CrearComprobanteElectro(WebUser usu, int id, int idPersona, string tipo, string modo, string fecha, int idCondicionVenta,
            int tipoConcepto, string fechaVencimiento, int idPuntoVenta, ref string nroComprobante, string obs, ComprobanteModo accion, ComprobanteCartDto comprCart, string pathBase)
        {
            var pathPdf = "";
            //var pathCertificado = "";
            var aux = DateTime.Parse(fecha);

            if (accion == ComprobanteModo.Previsualizar)
            {
                Path.Combine(pathBase + "/files/comprobantes/" + usu.IDUsuario + "_prev.pdf");
                pathPdf = Path.Combine(pathBase + "/files/comprobantes/" + usu.IDUsuario + "_prev.pdf");
                //HttpContext.Current.Server.MapPath("~/files/comprobantes/" + usu.IDUsuario + "_prev.pdf");
                if (System.IO.File.Exists(pathPdf))
                    System.IO.File.Delete(pathPdf);
            }
            else
            {
                pathPdf = Path.Combine(pathBase + "/files/explorer/" + usu.IDUsuario + "/Comprobantes/" + aux.Year.ToString() + "/");

                if (!Directory.Exists(pathPdf))
                    Directory.CreateDirectory(pathPdf);
            }

            //HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario + "/Comprobantes/" + DateTime.Now.Year.ToString() + "/");

            //Determinamos cantidad de decimales que posee configurado el usuario
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            using (var dbContext = new ACHEEntities())
            {
                Personas persona =
                    dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario)
                        .FirstOrDefault();
                if (persona == null)
                    throw new Exception("El cliente/proveedor es inexistente");

                if (ComprobanteModo.Generar == accion)
                {
                    pathPdf = pathPdf + persona.RazonSocial.RemoverCaracteresParaPDF() + "_";
                }

                int puntoVenta = 0;
                PuntosDeVenta punto =
                    dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == idPuntoVenta && x.IDUsuario == usu.IDUsuario)
                        .FirstOrDefault();
                if (punto != null)
                    puntoVenta = punto.Punto;

                if (puntoVenta == 0)
                    throw new Exception("Punto de venta invalido");

                FEFacturaElectronica fe = new FEFacturaElectronica();
                FEComprobante comprobante = new FEComprobante();

                #region Datos del emisor

                comprobante.TipoComprobante = ComprobantesCommon.ObtenerTipoComprobante(tipo);

                //var feMode = ConfigurationManager.AppSettings["FE.Modo"];
                if (usu.ModoQA)
                {
                    comprobante.Cuit = 30714075159; //long.Parse(usu.CUIT);
                    comprobante.PtoVta = 5; // puntoVenta;
                    //pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.CertificadoAFIP.QA"]);
                }
                else
                {
                    comprobante.Cuit = long.Parse(usu.CUIT);
                    comprobante.PtoVta = puntoVenta;
                    //pathCertificado = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.CertificadoAFIP.PROD"]);
                }

                comprobante.Concepto = (FEConcepto)tipoConcepto;
                comprobante.Fecha = DateTime.Parse(fecha);


                //si el tipo de factura es C solo se tiene que setear las fechas si el tipo de concepto es 2 o 3 (productos o productos y servicios)
                if (tipoConcepto != 1)
                {
                    comprobante.FchServDesde = DateTime.Parse(fecha);
                    comprobante.FchServHasta = DateTime.Parse(fechaVencimiento);
                    comprobante.FchVtoPago = DateTime.Parse(fechaVencimiento);
                }

                comprobante.CodigoMoneda = "PES";
                comprobante.CotizacionMoneda = 1;
                if (nroComprobante != string.Empty)
                    comprobante.NumeroComprobante = int.Parse(nroComprobante);

                comprobante.CondicionVenta = usu.CondicionIVA;
                comprobante.Domicilio = usu.Domicilio;
                comprobante.CiudadProvincia = usu.Ciudad + ", " + usu.Provincia;
                comprobante.RazonSocial = usu.RazonSocial;
                comprobante.Telefono = usu.Telefono;
                if (usu.ExentoIIBB != null)
                    comprobante.IIBB = ((bool)usu.ExentoIIBB) ? "Exento" : usu.IIBB;
                comprobante.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usu.CondicionIVA);
                comprobante.FechaInicioActividades = usu.FechaInicio.HasValue
                    ? usu.FechaInicio.Value.ToString("dd/MM/yyyy")
                    : "";

                #endregion

                #region Datos del cliente

                if (persona.Tipo == "CF")
                    comprobante.DocTipo = 99; // Consumidor final
                else
                    comprobante.DocTipo = persona.TipoDocumento == "DNI" ? 96 : 80;

                comprobante.DocNro = (persona.NroDocumento == "") ? 0 : long.Parse(persona.NroDocumento);
                //JC COBRANZA AUTOMATICA
                var cond = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, usu.IDUsuario, idCondicionVenta);
                if (cond != null)
                    comprobante.CondicionVenta = cond.Nombre;
                comprobante.ClienteNombre = persona.RazonSocial;
                comprobante.ClienteCondiionIva = UsuarioCommon.GetCondicionIvaDesc(persona.CondicionIva);
                comprobante.ClienteDomicilio = persona.Domicilio + " " + persona.PisoDepto;
                comprobante.ClienteLocalidad = persona.Ciudades.Nombre + ", " + persona.Provincias.Nombre;
                comprobante.Observaciones = obs;
                comprobante.Original = (accion == ComprobanteModo.Previsualizar ? false : true);
                if (tipo == "PRE")
                    comprobante.Original = true;

                #endregion


                #region Seteo los datos de la factura

                //comprobante.ImpTotConc = 0;
                //comprobante.ImpOpEx = 0;
                comprobante.DetalleIva = new List<FERegistroIVA>();
                comprobante.Tributos = new List<FERegistroTributo>();


                var list = comprCart.Items.OrderBy(x => x.Concepto).ToList();


                if (list.Any())
                {
                    foreach (var detalle in list)
                    {
                        if (usu.CondicionIVA == "RI")// && persona.CondicionIva == "RI")
                        {
                            comprobante.ItemsDetalle.Add(new FEItemDetalle()
                            {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                //Precio = Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales), 
                                Precio = Math.Round(double.Parse(detalle.PrecioUnitarioSinIVA.ToString()), cantidadDecimales),
                                PrecioParaPDF = persona.CondicionIva == "RI" ? Math.Round(double.Parse(detalle.PrecioUnitario.ToString()), cantidadDecimales) : Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()), cantidadDecimales),
                                Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                Bonificacion = detalle.Bonificacion,
                                Iva = detalle.Iva
                            });

                            //totalizar importe e IVA
                            switch (detalle.Iva.ToString("#0.00"))
                            {
                                case "21,00":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva21);
                                    break;
                                case "27,00":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva27);
                                    break;
                                case "10,50":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva10_5);
                                    break;
                                case "5,00":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva5);
                                    break;
                                case "2,50":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva2_5);
                                    break;
                                case "0,00":
                                    AgregarDetalleIvaTotalizado(ref comprobante, detalle, FETipoIva.Iva0);
                                    break;
                            }
                        }
                        else
                        {
                            comprobante.ItemsDetalle.Add(new FEItemDetalle()
                            {
                                Cantidad = detalle.Cantidad,
                                Descripcion = detalle.Concepto,
                                Precio =
                                    Math.Round(double.Parse(detalle.PrecioUnitarioConIva.ToString()), cantidadDecimales),
                                PrecioParaPDF =
                                    Math.Round(double.Parse(detalle.PrecioUnitarioConIvaSinBonificacion.ToString()),
                                        cantidadDecimales),
                                Codigo = detalle.IDConcepto.HasValue ? detalle.IDConcepto.Value.ToString() : "",
                                Bonificacion = detalle.Bonificacion,
                                Iva = detalle.Iva
                            });

                            if (comprobante.DetalleIva.Any(x => x.TipoIva == FETipoIva.Iva0))
                            {
                                comprobante.DetalleIva.Where(x => x.TipoIva == FETipoIva.Iva0).FirstOrDefault().BaseImp
                                    += Math.Round(double.Parse(detalle.TotalConIva.ToString()), cantidadDecimales);
                            }
                            else
                            {
                                if (tipo != "FCC" && tipo != "NDC" && tipo != "RCC")
                                    comprobante.DetalleIva.Add(new FERegistroIVA()
                                    {
                                        BaseImp =
                                            Math.Round(double.Parse(detalle.TotalConIva.ToString()), cantidadDecimales),
                                        TipoIva = FETipoIva.Iva0
                                    });
                            }
                        }
                    }
                }

                //Remplazar el IDConcepto por codigo 
                foreach (var item in comprobante.ItemsDetalle)
                {
                    if (item.Codigo != "")
                    {
                        int idConceto = Convert.ToInt32(item.Codigo);
                        item.Codigo =
                            dbContext.Conceptos.Where(x => x.IDConcepto == idConceto).FirstOrDefault().Codigo.ToString();
                    }
                }

                #endregion

                comprobante.ImpTotConc = (double)comprCart.GetImporteNoGrabado();
                if (usu.CondicionIVA == "RI" && (bool)usu.AgentePercepcionIIBB)
                {
                    if ((double)comprCart.GetPercepcionIIBB() > 0)
                    {
                        comprobante.Tributos.Add(new FERegistroTributo()
                        {
                            BaseImp = comprobante.ItemsDetalle.Sum(x => x.Total),
                            Alicuota = (double)comprCart.PercepcionIIBB,
                            Importe = Math.Round((double)comprCart.GetPercepcionIIBB(), cantidadDecimales),
                            Descripcion = "Percepción IIBB",
                            Tipo = FETipoTributo.ImpuestosNacionales
                        });
                    }
                }

                if (usu.CondicionIVA == "RI" && (bool)usu.AgentePercepcionIVA)
                {
                    if ((double)comprCart.GetPercepcionIVA() > 0)
                    {
                        comprobante.Tributos.Add(new FERegistroTributo()
                        {
                            BaseImp = comprobante.ItemsDetalle.Sum(x => x.Total),
                            Alicuota = (double)comprCart.PercepcionIVA,
                            Importe = Math.Round((double)comprCart.GetPercepcionIVA(), cantidadDecimales),
                            Descripcion = "Percepción IVA",
                            Tipo = FETipoTributo.ImpuestosNacionales
                        });
                    }
                }

                var pathTemplateFc =
                    Path.Combine(pathBase + ConfigurationManager.AppSettings["FE.Template"].Replace("~", "") +
                                 usu.TemplateFc + ".pdf");
                //HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FE.Template"] + usu.TemplateFc + ".pdf");
                var imgLogo = "logo-fc-" + usu.TemplateFc + ".png";
                if (!string.IsNullOrEmpty(usu.Logo))
                {
                    if (File.Exists(Path.Combine(pathBase + "/files/usuarios/" + usu.Logo)))
                        //HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
                        imgLogo = usu.Logo;
                }

                var pathLogo = Path.Combine(pathBase + "/files/usuarios/" + imgLogo);
                var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usu.IDUsuario + ".png");

                //HttpContext.Current.Server.MapPath("~/files/usuarios/" + imgLogo);
                if (accion == ComprobanteModo.Previsualizar)
                    fe.GrabarEnDisco(comprobante, pathPdf, pathTemplateFc, pathLogo, usu.MostrarBonificacionFc, true, pathMarcaAgua);
                else //SI no se previsualiza se genera electronicamente el comprobante
                {
                    Comprobantes cmp = dbContext.Comprobantes.Where(x => x.IDComprobante == id).FirstOrDefault();
                    if (cmp != null)
                    {
                        cmp.FechaProceso = DateTime.Now;
                        try
                        {
                            //fe.GenerarComprobante(comprobante, ConfigurationManager.AppSettings["FE.PROD.wsaa"], pathCertificado, pathTemplateFc, pathLogo);
                            fe.GenerarComprobante(comprobante, pathTemplateFc, pathLogo, (usu.ModoQA ? "QA" : "PROD"),
                                usu.MostrarBonificacionFc, pathMarcaAgua);
                            string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" +
                                                       comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
                            pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

                            cmp.FechaCAE = comprobante.FechaVencCAE;
                            cmp.CAE = comprobante.CAE;
                            cmp.Numero = comprobante.NumeroComprobante;
                            cmp.Error = null;
                            cmp.FechaError = null;

                            nroComprobante = numeroComprobante;
                            dbContext.SaveChanges();

                            fe.GrabarEnDisco(comprobante.ArchivoFactura, pathPdf, pathTemplateFc, pathLogo);
                        }
                        catch (Exception ex)
                        {
                            cmp.Error = ex.Message;
                            cmp.FechaError = DateTime.Now;
                            dbContext.SaveChanges();

                            throw new CustomException(cmp.Error, ex.InnerException);
                        }
                    }
                }
            }
        }

        public static void AgregarDetalleIvaTotalizado(ref FEComprobante comprobante, ComprobantesDetalleViewModel detalle, FETipoIva tipoiva)
        {
            if (comprobante.DetalleIva.Any(x => x.TipoIva == tipoiva))
            {
                comprobante.DetalleIva.Where(x => x.TipoIva == tipoiva).FirstOrDefault().BaseImp += double.Parse(detalle.TotalSinIva.ToString());
                comprobante.DetalleIva.Where(x => x.TipoIva == tipoiva).FirstOrDefault().Importe += double.Parse(detalle.TotalConIva.ToString()) - double.Parse(detalle.TotalSinIva.ToString());
            }
            else
                comprobante.DetalleIva.Add(new FERegistroIVA()
                {
                    BaseImp = double.Parse(detalle.TotalSinIva.ToString()),
                    TipoIva = tipoiva,
                    Importe = double.Parse(detalle.TotalConIva.ToString()) - double.Parse(detalle.TotalSinIva.ToString())
                });
        }

        public static bool ExisteComprobante(long cuit, long nroComprobante, int PtoVta, string tipoComprobante)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    Usuarios usuario = UsuarioCommon.ObtenerUsuarioPorNroDoc(cuit.ToString());
                    if (usuario == null)
                        throw new Exception("No se encontro el usuario con el cuit seleccionado");

                    var existe =
                        dbContext.Comprobantes.Any(
                            x =>
                                x.IDUsuario == usuario.IDUsuario && x.Numero == nroComprobante &&
                                x.PuntosDeVenta.Punto == PtoVta && x.Tipo == tipoComprobante);
                    return existe;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void InsertarComprobanteRecuperado(FEComprobante comprobante, string tipoComprobante, string cuit)
        {
            try
            {
                Usuarios usuario = UsuarioCommon.ObtenerUsuarioPorNroDoc(cuit);
                Personas Cliente = null;
                if (usuario != null)
                    Cliente = PersonasCommon.ObtenerPersonaPorNroDoc(comprobante.DocNro.ToString(), usuario.IDUsuario);
                else
                    throw new Exception("No se encontro el usuario con el cuit seleccionado");
                if (Cliente == null)
                    throw new Exception("El cliente " + comprobante.ClienteNombre + " (" + comprobante.DocNro +
                                        ")  al que se le emitió la factura no está dado de alta en la cuenta de " +
                                        usuario.RazonSocial);

                //Determinamos cantidad de decimales que posee configurado el usuario
                //var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usuario.IDUsuario);

                using (var dbContext = new ACHEEntities())
                {
                    PuntosDeVenta punto =
                        dbContext.PuntosDeVenta.Where(
                            x => x.Punto == comprobante.PtoVta && x.IDUsuario == usuario.IDUsuario).FirstOrDefault();
                    ComprobanteCartDto compCart = new ComprobanteCartDto();
                    ComprobantesDetalleViewModel comp =
                        new ComprobantesDetalleViewModel(DecimalFormatter.GetCantidadDecimales(usuario.IDUsuario));

                    compCart.IDComprobante = 0;
                    compCart.TipoConcepto = tipoComprobante;
                    compCart.Numero = comprobante.NumeroComprobante.ToString();
                    compCart.Modo = "E";
                    compCart.FechaComprobante = comprobante.Fecha;
                    //JC COBRANZA AUTOMATICA
                    //  compCart.CondicionVenta = "Efectivo";
                    compCart.IDCondicionVenta = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, usuario.IDUsuario, "Efectivo").IDCondicionVenta;
                    compCart.TipoConcepto = ((int)comprobante.Concepto).ToString();
                    compCart.FechaVencimiento = comprobante.Fecha.AddDays(30);
                    compCart.IDPuntoVenta = punto.IDPuntoVenta;
                    compCart.TipoComprobante = tipoComprobante;
                    compCart.Observaciones = comprobante.Observaciones;
                    compCart.IDPersona = Cliente.IDPersona;
                    compCart.IDUsuario = usuario.IDUsuario;
                    compCart.Items = new List<ComprobantesDetalleViewModel>();

                    int contador = 0;
                    //if (comprobante.TipoComprobante == FETipoComprobante.FACTURAS_B ||
                    //    comprobante.TipoComprobante == FETipoComprobante.NOTAS_CREDITO_B ||
                    //    comprobante.TipoComprobante == FETipoComprobante.NOTAS_DEBITO_B)
                    //{
                    //    comp.ID = contador;
                    //    comp.IDConcepto = null;
                    //    comp.IDPlanesPagos = null;

                    //    comp.Codigo = "";
                    //    comp.Cantidad = 1;
                    //    comp.Iva = decimal.Parse("21,00");
                    //    comp.Concepto = "Item recuperado con IVA " + comp.Iva;
                    //    comp.PrecioUnitario = Convert.ToDecimal(comprobante.ImpNeto / 1.21);
                    //    compCart.Items.Add(comp);
                    //}
                    //else
                    //{

                    foreach (var item in comprobante.DetalleIva)
                    {
                        comp.ID = contador;
                        comp.IDConcepto = null;
                        comp.IDPlanesPagos = null;

                        comp.Codigo = "";
                        comp.Cantidad = 1;
                        switch (item.TipoIva)
                        {
                            case FETipoIva.Iva0:
                                comp.Iva = decimal.Parse("0,00");
                                break;
                            case FETipoIva.Iva10_5:
                                comp.Iva = decimal.Parse("10,50");
                                break;
                            case FETipoIva.Iva21:
                                comp.Iva = decimal.Parse("21,00");
                                break;
                            case FETipoIva.Iva27:
                                comp.Iva = decimal.Parse("27,00");
                                break;
                            case FETipoIva.Iva5:
                                comp.Iva = decimal.Parse("5,00");
                                break;
                            case FETipoIva.Iva2_5:
                                comp.Iva = decimal.Parse("2,50");
                                break;
                        }
                        comp.Concepto = "Item recuperado con IVA " + comp.Iva;

                        comp.PrecioUnitario = Convert.ToDecimal(item.BaseImp);
                        compCart.Items.Add(comp);
                        contador++;
                    }
                    //}

                    var cmp = Guardar(dbContext, compCart);

                    cmp.FechaCAE = comprobante.FechaVencCAE;
                    cmp.CAE = comprobante.CAE;
                    cmp.Numero = comprobante.NumeroComprobante;
                    cmp.Error = null;
                    cmp.FechaError = null;
                    cmp.FechaProceso = DateTime.Now;
                    dbContext.SaveChanges();
                    //// para generarPDF
                    generarPDF(comprobante, usuario, Cliente, compCart.TipoComprobante);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static void generarPDF(FEComprobante comprobanteAfip, string cuitEmisor, string tipoComprobante)
        {
            var docNroReceptor = comprobanteAfip.DocNro.ToString();

            Usuarios usuario = UsuarioCommon.ObtenerUsuarioPorNroDoc(cuitEmisor);
            Personas Cliente = PersonasCommon.ObtenerPersonaPorNroDoc(docNroReceptor, usuario.IDUsuario);

            if (Cliente == null)
                throw new Exception("Error al recuperar el cliente con documento " + docNroReceptor);

            Comprobantes comprobante = null;

            using (var dbContext = new ACHEEntities())
            {
                comprobante = dbContext.Comprobantes.Include("Personas").Include("ComprobantesDetalle").Include("ComprobantesTributos")
                    .Include("PuntosDeVenta").Include("ComprobantesDetalle.Conceptos")
                    .Where(x => x.IDPersona == Cliente.IDPersona && x.IDUsuario == usuario.IDUsuario && x.CAE == comprobanteAfip.CAE).FirstOrDefault();
            }

            if (comprobante == null)
                throw new Exception("Error al recuperar el comprobante desde Contabilium");

            generarPDF(comprobanteAfip, comprobante, usuario, Cliente, tipoComprobante);
        }

        private static void generarPDF(FEComprobante comprobante, Comprobantes comp, Usuarios usuario, Personas Cliente, string tipo)
        {
            comprobante.Cuit = Convert.ToInt64(usuario.CUIT);
            comprobante.CiudadProvincia = (usuario.Ciudades == null)
                ? ""
                : usuario.Ciudades.Nombre + ", " + usuario.Provincias.Nombre;
            comprobante.Domicilio = usuario.Domicilio + " " + usuario.PisoDepto;
            comprobante.Telefono = usuario.Telefono;
            comprobante.CondicionIva = UsuarioCommon.GetCondicionIvaDesc(usuario.CondicionIva);
            comprobante.IIBB = usuario.IIBB;
            comprobante.FechaInicioActividades = Convert.ToDateTime(usuario.FechaInicioActividades).ToString("dd/MM/yyyy");
            comprobante.CondicionVenta = comp.CondicionVenta;
            comprobante.RazonSocial = usuario.RazonSocial;
            comprobante.ClienteCondiionIva = Cliente.CondicionIva;
            comprobante.ClienteDomicilio = Cliente.Domicilio + " " + Cliente.PisoDepto;
            comprobante.ClienteLocalidad = (Cliente.Provincias == null) ? "" : Cliente.Provincias.Nombre;
            comprobante.ClienteNombre = Cliente.RazonSocial;
            comprobante.ImpTotConc = (double)comp.ImporteNoGrabado;

            ComprobanteCart.Retrieve().Init(comp);
            var compCartDto = ComprobanteCart.Retrieve().GetComprobanteCartDto();
            compCartDto.IDUsuario = comp.IDUsuario;

            foreach (var item in compCartDto.Items)
            {
                //double precioFinal = item.;
                //switch (item.TipoIva)
                //{
                //    case FETipoIva.Iva10_5:
                //        precioFinal = precioFinal * 1.105;
                //        break;
                //    case FETipoIva.Iva21:
                //        precioFinal = precioFinal * 1.21;
                //        break;
                //    case FETipoIva.Iva27:
                //        precioFinal = precioFinal * 1.27;
                //        break;
                //    case FETipoIva.Iva5:
                //        precioFinal = precioFinal * 1.050;
                //        break;
                //    case FETipoIva.Iva2_5:
                //        precioFinal = precioFinal * 1.025;
                //        break;
                //}

                if (comp.Tipo == "FCA")
                {

                    comprobante.ItemsDetalle.Add(new FEItemDetalle()
                    {
                        //Cantidad = item.Cantidad,
                        //Descripcion = item.Concepto,
                        //Precio = double.Parse(item.PrecioUnitario.ToString()),
                        //PrecioParaPDF = double.Parse(item.PrecioUnitario.ToString()),
                        //Codigo = item.Codigo,
                        //Bonificacion = item.Bonificacion,
                        //Iva = item.Iva
                        Cantidad = item.Cantidad,
                        Descripcion = item.Concepto,
                        Precio = Math.Round(double.Parse(item.PrecioUnitarioSinIVA.ToString()), 2),
                        PrecioParaPDF = Math.Round(double.Parse(item.PrecioUnitarioSinIVA.ToString()), 2),
                        Codigo = item.IDConcepto.HasValue ? item.Codigo : "",
                        Bonificacion = item.Bonificacion,
                        Iva = item.Iva
                    });
                }
                else
                {
                    comprobante.ItemsDetalle.Add(new FEItemDetalle()
                    {
                        //Cantidad = item.Cantidad,
                        //Descripcion = item.Concepto,
                        //Precio = double.Parse(item.PrecioUnitario.ToString()),
                        //PrecioParaPDF = double.Parse(item.PrecioUnitario.ToString()),
                        //Codigo = item.Codigo,
                        //Bonificacion = item.Bonificacion,
                        //Iva = item.Iva
                        Cantidad = item.Cantidad,
                        Descripcion = item.Concepto,
                        Precio = Math.Round(double.Parse(item.PrecioUnitarioConIva.ToString()), 2),
                        PrecioParaPDF = Math.Round(double.Parse(item.PrecioUnitarioConIvaSinBonificacion.ToString()), 2),
                        Codigo = item.IDConcepto.HasValue ? item.Codigo : "",
                        Bonificacion = item.Bonificacion,
                        Iva = item.Iva
                    });
                }
            }

            var PathBaseWeb = ConfigurationManager.AppSettings["PathBaseWeb"];
            var imgLogo = "logo-fc-" + usuario.TemplateFc + ".png";
            if (!string.IsNullOrEmpty(usuario.Logo))
            {
                if (File.Exists(Path.Combine(PathBaseWeb + "/files/usuarios/" + usuario.Logo)))
                    //HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
                    imgLogo = usuario.Logo;
            }

            var pathTemplateFc = Path.Combine(PathBaseWeb + ConfigurationManager.AppSettings["FE.Template"].Replace("~", "") + usuario.TemplateFc + ".pdf");
            var pathLogo = Path.Combine(PathBaseWeb + "/files/usuarios/" + imgLogo);
            var pathPdf = Path.Combine(PathBaseWeb + "/files/explorer/" + usuario.IDUsuario + "/Comprobantes/" + comprobante.Fecha.Year.ToString() + "/");
            var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usuario.IDUsuario + ".png");

            if (!Directory.Exists(pathPdf))
                Directory.CreateDirectory(pathPdf);

            string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" + comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
            pathPdf = pathPdf + Cliente.RazonSocial.RemoverCaracteresParaPDF() + "_";
            pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

            FEFacturaElectronica fe = new FEFacturaElectronica();
            comprobante.ArchivoFactura = fe.GetStreamPDF(comprobante, pathTemplateFc, pathLogo, usuario.MostrarBonificacionFc, true, pathMarcaAgua);
            fe.GrabarEnDisco(comprobante.ArchivoFactura, pathPdf, pathTemplateFc, pathLogo);
        }

        private static void generarPDF(FEComprobante comprobante, Usuarios usuario, Personas Cliente, string tipo)
        {
            comprobante.Cuit = Convert.ToInt64(usuario.CUIT);
            comprobante.CiudadProvincia = (usuario.Ciudades == null)
                ? ""
                : usuario.Ciudades.Nombre + ", " + usuario.Provincias.Nombre;
            comprobante.Domicilio = usuario.Domicilio + " " + usuario.PisoDepto;
            comprobante.Telefono = usuario.Telefono;
            comprobante.ClienteCondiionIva = usuario.CondicionIva;
            comprobante.IIBB = usuario.IIBB;
            comprobante.FechaInicioActividades =
                Convert.ToDateTime(usuario.FechaInicioActividades).ToString("dd/MM/yyyy");
            comprobante.CondicionVenta = "Efectivo";

            comprobante.ClienteCondiionIva = Cliente.CondicionIva;
            comprobante.ClienteDomicilio = Cliente.Domicilio + " " + Cliente.PisoDepto;
            comprobante.ClienteLocalidad = (Cliente.Provincias == null) ? "" : Cliente.Provincias.Nombre;
            comprobante.ClienteNombre = Cliente.RazonSocial;


            foreach (var item in comprobante.DetalleIva)
            {
                double precioFinal = item.BaseImp;
                switch (item.TipoIva)
                {
                    case FETipoIva.Iva10_5:
                        precioFinal = precioFinal * 1.105;
                        break;
                    case FETipoIva.Iva21:
                        precioFinal = precioFinal * 1.21;
                        break;
                    case FETipoIva.Iva27:
                        precioFinal = precioFinal * 1.27;
                        break;
                    case FETipoIva.Iva5:
                        precioFinal = precioFinal * 1.050;
                        break;
                    case FETipoIva.Iva2_5:
                        precioFinal = precioFinal * 1.025;
                        break;
                }

                comprobante.ItemsDetalle.Add(new FEItemDetalle()
                {

                    Cantidad = 1,
                    Descripcion = "Item Recuperado",
                    Precio = item.BaseImp,
                    PrecioParaPDF = Cliente.CondicionIva == "RI" ? Math.Round(double.Parse(item.BaseImp.ToString()), 2) : Math.Round(double.Parse(precioFinal.ToString()), 2),
                    Codigo = "",
                    Bonificacion = 0,
                    //Iva = item.TipoIva.ToDescription() == "Iva21" ? 21 : decimal.Parse(item.TipoIva.ToDescription())//TODO: FIX ESTO AL RECUPERAR CAE
                    Iva = decimal.Parse(item.TipoIva.ToDescription().Replace("Iva", "").Replace("_", "."))
                });
            }

            var PathBaseWeb = ConfigurationManager.AppSettings["PathBaseWeb"];
            var imgLogo = "logo-fc-" + usuario.TemplateFc + ".png";
            if (!string.IsNullOrEmpty(usuario.Logo))
            {
                if (File.Exists(Path.Combine(PathBaseWeb + "/files/usuarios/" + usuario.Logo)))
                    //HttpContext.Current.Server.MapPath("~/files/usuarios/" + usu.Logo)))
                    imgLogo = usuario.Logo;
            }

            var pathTemplateFc = Path.Combine(PathBaseWeb + ConfigurationManager.AppSettings["FE.Template"].Replace("~", "") + usuario.TemplateFc + ".pdf");
            var pathLogo = Path.Combine(PathBaseWeb + "/files/usuarios/" + imgLogo);
            var pathPdf = Path.Combine(PathBaseWeb + "/files/explorer/" + usuario.IDUsuario + "/Comprobantes/" + comprobante.Fecha.Year.ToString() + "/");
            var pathMarcaAgua = HttpContext.Current.Server.MapPath("~/files/usuarios/waterMark_" + usuario.IDUsuario + ".png");

            if (!Directory.Exists(pathPdf))
                Directory.CreateDirectory(pathPdf);

            string numeroComprobante = comprobante.PtoVta.ToString().PadLeft(4, '0') + "-" +
                                       comprobante.NumeroComprobante.ToString().PadLeft(8, '0');
            pathPdf = pathPdf + Cliente.RazonSocial.RemoverCaracteresParaPDF() + "_";
            pathPdf = pathPdf + tipo + "-" + numeroComprobante + ".pdf";

            FEFacturaElectronica fe = new FEFacturaElectronica();
            comprobante.ArchivoFactura = fe.GetStreamPDF(comprobante, pathTemplateFc, pathLogo,
                usuario.MostrarBonificacionFc, true, pathMarcaAgua);
            fe.GrabarEnDisco(comprobante.ArchivoFactura, pathPdf, pathTemplateFc, pathLogo);
        }

        public static void GuardarComprobantesEnviados(ACHEEntities dbContext, int? idcomprobante, string mensajeOriginal, int? idCobranza,
            string mensaje, bool resultado, WebUser usu, string destinatarios)
        {
            try
            {
                var entity = new ComprobantesEnviados();
                entity.IDUsuario = usu.IDUsuario;
                entity.Mensaje = mensaje;
                entity.Resultado = resultado;
                entity.MensajeOriginal = mensajeOriginal;
                entity.FechaEnvio = DateTime.Now;
                entity.Destinatarios = destinatarios;
                if (idcomprobante != null)
                    entity.IDComprobante = idcomprobante;
                if (idCobranza != null)
                    entity.IDCobranza = idCobranza;
                dbContext.ComprobantesEnviados.Add(entity);
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region crear comprobante para contabilium

        /// <summary>
        /// Crea una fc electronico y la cobranza asociada del plan a un cliente determinado, si el cliente no existe lo crea.
        /// al cliente lo asocia a CONTABILIUM
        /// </summary>
        /// <param name="nroReferencia"> numero de trasferencia bancario o de idmp</param>
        /// <param name="IdUsuario">usuario al que se ve a imputar el pago</param>
        /// <param name="dbContext"></param>
        /// <param name="planesPagos">plan a facturar</param>
        /// <param name="pathBase">Ruta de donde se BASE de donde se va a guardar la factura electronica</param>
        /// <returns></returns>
        public static ComprobanteNroViewModel CrearDatosParaContabilium(string nroReferencia, int IdUsuario,
            ACHEEntities dbContext, PlanesPagos planesPagos, string pathBase, string condicionVenta)
        {
            var comprobanteViewModel = new ComprobanteNroViewModel();
            // ** CREACION DE USUARIO FACTURA Y COBRANZA ** //
            var idContabilium = Convert.ToInt32(ConfigurationManager.AppSettings["Usu.idContabilium"]);
            var contabilium = dbContext.Usuarios.Where(x => x.IDUsuario == idContabilium).FirstOrDefault();
            if (contabilium == null)
                throw new Exception("No se encontro el usuario de Contabilium");
            // ** CREACION DE USUARIO ** //
            var Cliente = PersonasCommon.GuardarDesdeIDUsuario(dbContext, idContabilium, IdUsuario, "C");
            // ** CREACION DE FACTURA ** //
            int idcondicion = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorNombre(dbContext, idContabilium, condicionVenta).IDCondicionVenta;

            var comprobante = CrearComprobante(dbContext, planesPagos, Cliente, contabilium, idcondicion);
            // ** CREACION FACTURA ELECTRONICA ** //
            var nroComprobante = CrearComprobanteElectronico(dbContext, comprobante, planesPagos, contabilium, pathBase);
            // ** CREACION COBRANZA **//
            if (condicionVenta != "MercadoPago")
                CrearCobranza(dbContext, comprobante, planesPagos, nroReferencia, contabilium);

            comprobanteViewModel.comprobante = comprobante;
            comprobanteViewModel.nroComprobanteElectronico = nroComprobante;
            return comprobanteViewModel;
        }

        private static Comprobantes CrearComprobante(ACHEEntities dbContext, PlanesPagos planesPagos, Personas Cliente, Usuarios usuario, int condicionVenta)
        {
            //Determinamos cantidad de decimales que posee configurado el usuario
            //int cantidadDecimales = 2;// ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usuario.IDUsuario);

            ComprobanteCartDto compCart = new ComprobanteCartDto();
            ComprobantesDetalleViewModel comp = new ComprobantesDetalleViewModel(DecimalFormatter.GetCantidadDecimales(usuario.IDUsuario));

            compCart.IDComprobante = 0;
            compCart.TipoComprobante = ObtenerTipoDeFacturaAFacturar(Cliente.CondicionIva, usuario.CondicionIva);
            compCart.IDPuntoVenta = Convert.ToInt32(ConfigurationManager.AppSettings["Usu.idPuntoDeVenta"]);
            compCart.Numero = ObtenerProxNroComprobante(compCart.TipoComprobante, usuario.IDUsuario, compCart.IDPuntoVenta);
            compCart.Modo = "E";
            compCart.FechaComprobante = DateTime.Now.Date;
            compCart.IDCondicionVenta = condicionVenta;
            compCart.CondicionVenta = "MercadoPago";
            compCart.TipoConcepto = "2";
            compCart.FechaVencimiento = DateTime.Now.Date;
            compCart.Observaciones = "";

            compCart.IDPersona = Cliente.IDPersona;
            compCart.IDUsuario = usuario.IDUsuario;

            compCart.Items = new List<ComprobantesDetalleViewModel>();

            comp.ID = 0;

            int? idConcepto = null;
            string codigo = string.Empty;

            switch (planesPagos.IDPlan)
            {
                case 2://Profesional
                    idConcepto = 5886;
                    codigo = "00020";
                    break;
                case 3://Pyme
                    idConcepto = 5887;
                    codigo = "00021";
                    break;
                case 4://Empresa
                    idConcepto = 5888;
                    codigo = "00022";
                    break;
                case 5://Corpo
                    idConcepto = 7265;
                    codigo = "00023";
                    break;
            }

            comp.IDConcepto = idConcepto;
            comp.IDPlanesPagos = planesPagos.IDPlanesPagos;

            comp.Codigo = codigo;
            comp.Concepto = "Plan " + planesPagos.Planes.Nombre;
            if (planesPagos.PagoAnual)
            {
                //comp.Bonificacion = 10;TODO: CORREGIR ESTO
                comp.Cantidad = 12;
                comp.PrecioUnitario = Math.Round(ConceptosCommon.ObtenerPrecioFinal((planesPagos.ImportePagado / 12), "21,00", 2), 2);
            }
            else
            {
                comp.Cantidad = 1;
                comp.PrecioUnitario = Math.Round(ConceptosCommon.ObtenerPrecioFinal((planesPagos.ImportePagado), "21,00", 2), 2);
            }

            comp.Iva = 21;

            compCart.Items.Add(comp);
            var comprobante = Guardar(dbContext, compCart);
            return comprobante;
        }

        /// <summary>
        /// Esta funcion solo se usa cuando se crean las fc de contabilium
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="comprobante"></param>
        /// <param name="planesPagos"></param>
        /// <param name="Contabilium"></param>
        /// <param name="pathBase"></param>
        /// <returns></returns>

        private static string CrearComprobanteElectronico(ACHEEntities dbContext, Comprobantes comprobante,
            PlanesPagos planesPagos, Usuarios Contabilium, string pathBase)
        {
            var usu = dbContext.UsuariosView.Where(x => x.IDUsuario == Contabilium.IDUsuario).FirstOrDefault();

            //bool UsaPlanDeCuenta = ContabilidadCommon.UsaPlanContable(dbContext, usu.IDUsuario, usu.CondicionIva);
            var webUser = new WebUser(
                usu.IDUsuario, usu.IDUsuarioAdicional, usu.Tipo, usu.RazonSocial, usu.CUIT, usu.CondicionIva,
                usu.Email, usu.EmailFrom, "", usu.Domicilio + " " + usu.PisoDepto, usu.Pais, usu.IDProvincia,
                usu.IDCiudad, usu.Telefono, usu.TieneFacturaElectronica, usu.IIBB, usu.FechaInicioActividades,
                usu.Logo, usu.TemplateFc, usu.IDUsuarioPadre, usu.SetupRealizado, true /*tieneMultiempresa*/,
                !usu.UsaProd,
                6 /*idPlan*/, usu.EmailAlertas, usu.Provincia, usu.Ciudad, usu.EsAgentePercepcionIVA,
                usu.EsAgentePercepcionIIBB,
                usu.EsAgenteRetencion, true /*PlanVigente*/, usu.TieneDebitoAutomatico, usu.UsaFechaFinPlan, usu.ApiKey, usu.ExentoIIBB,
                usu.UsaPrecioFinalConIVA, usu.FechaAlta, usu.EnvioAutomaticoComprobante, usu.EnvioAutomaticoRecibo,
                usu.IDJurisdiccion, usu.UsaPlanCorporativo,
                usu.ObservacionesFc, usu.ObservacionesRemito, usu.ObservacionesPresupuestos, "", usu.MostrarBonificacionFc,
                usu.PorcentajeDescuento);

            int id = comprobante.IDComprobante;
            int idPersona = comprobante.IDPersona;
            string tipo = comprobante.Tipo;
            string modo = comprobante.Modo;
            string fecha = comprobante.FechaAlta.ToString("dd/MM/yyyy");
            int idCondicionVenta = comprobante.IDCondicionVenta;

            int tipoConcepto = comprobante.TipoConcepto;
            string fechaVencimiento = comprobante.FechaVencimiento.ToString("dd/MM/yyyy");
            int idPuntoVenta = comprobante.IDPuntoVenta;
            string nroComprobante = comprobante.Numero.ToString("#00000000");
            string obs = comprobante.Observaciones;

            ComprobanteCartDto compCart = new ComprobanteCartDto();
            compCart.Items = new List<ComprobantesDetalleViewModel>();
            ComprobantesDetalleViewModel comp = new ComprobantesDetalleViewModel(2);
            comp.ID = 0;
            comp.IDConcepto = null;
            comp.IDPlanesPagos = planesPagos.IDPlanesPagos;

            comp.Codigo = "";
            comp.Concepto = "Plan: " + planesPagos.Planes.Nombre;
            if (planesPagos.PagoAnual)
            {
                //comp.Bonificacion = 10;TODO: CORREGIR ESTO
                comp.Cantidad = 12;
                comp.PrecioUnitario = comprobante.ImporteTotalBruto / 12;
            }
            else
            {
                comp.Cantidad = 1;
                comp.PrecioUnitario = comprobante.ImporteTotalBruto;
            }

            comp.Iva = decimal.Parse("21,00");
            compCart.Items.Add(comp);
            var testIVA = compCart.GetIva();

            CrearComprobanteElectro(webUser, id, idPersona, tipo, modo, fecha, idCondicionVenta,
                        tipoConcepto, fechaVencimiento, idPuntoVenta, ref nroComprobante, obs, ComprobanteModo.Generar, compCart,
                        pathBase);
            return nroComprobante;
        }

        /// <summary>
        /// Esta funcion solo se usa cuando se crean las fc de contabilium
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="comprobante"></param>
        /// <param name="planesPagos"></param>
        /// <param name="nroReferencia"></param>
        /// <param name="contabilium"></param>

        private static void CrearCobranza(ACHEEntities dbContext, Comprobantes comprobante, PlanesPagos planesPagos,
            string nroReferencia, Usuarios contabilium)
        {
            //Determinamos cantidad de decimales que posee configurado el usuario
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(contabilium.IDUsuario);

            var punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == comprobante.IDPuntoVenta).FirstOrDefault();

            CobranzaCartDto cobrCartdto = new CobranzaCartDto();
            cobrCartdto.IDCobranza = 0;
            cobrCartdto.IDPersona = comprobante.IDPersona;
            cobrCartdto.Tipo = "RC";
            cobrCartdto.Fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
            cobrCartdto.IDPuntoVenta = comprobante.IDPuntoVenta;
            cobrCartdto.NumeroCobranza = CobranzasCommon.obtenerProxNroCobranza(cobrCartdto.Tipo, contabilium.IDUsuario);
            cobrCartdto.Observaciones = "Cobranza generada automáticamente";

            CobranzasDetalleViewModel item = new CobranzasDetalleViewModel(2);

            cobrCartdto.Items = new List<CobranzasDetalleViewModel>();
            item.ID = 1;
            item.Comprobante = comprobante.Tipo + " " + punto.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
            item.Importe = comprobante.Saldo;
            item.IDComprobante = comprobante.IDComprobante;
            cobrCartdto.Items.Add(item);

            CobranzasFormasDePagoViewModel formasDePago = new CobranzasFormasDePagoViewModel();
            cobrCartdto.FormasDePago = new List<CobranzasFormasDePagoViewModel>();
            formasDePago.ID = 1;
            formasDePago.Importe = planesPagos.ImportePagado;
            formasDePago.FormaDePago = planesPagos.FormaDePago;
            formasDePago.NroReferencia = nroReferencia;
            cobrCartdto.FormasDePago.Add(formasDePago);

            cobrCartdto.Retenciones = new List<CobranzasRetencionesViewModel>();

            var usu = TokenCommon.ObtenerWebUser(dbContext, contabilium.IDUsuario); //TODO: Mejorar esto
            CobranzasCommon.Guardar(dbContext, cobrCartdto, usu, false);
        }

        public static void CrearCobranzaPorMPAutomática(ACHEEntities dbContext, Comprobantes comprobante, int idUsuario, int idCondicionVenta)
        {
            //Determinamos cantidad de decimales que posee configurado el usuario
            //var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(idUsuario);

            var punto = dbContext.PuntosDeVenta.Where(x => x.IDPuntoVenta == comprobante.IDPuntoVenta).FirstOrDefault();
            var condicion = CondicionDeVentaCommon.ObtenerCondicionDeVentaPorID(dbContext, idUsuario, idCondicionVenta);
            CobranzaCartDto cobrCartdto = new CobranzaCartDto();
            cobrCartdto.IDCobranza = CobranzasCommon.ObtenerIDCobranzaPorDetalle(comprobante.IDComprobante);
            cobrCartdto.IDPersona = comprobante.IDPersona;
            cobrCartdto.Tipo = "RC";
            cobrCartdto.Fecha = DateTime.Now.Date.ToString("dd/MM/yyyy");
            cobrCartdto.IDPuntoVenta = comprobante.IDPuntoVenta;
            cobrCartdto.NumeroCobranza = CobranzasCommon.obtenerProxNroCobranza(cobrCartdto.Tipo, idUsuario);
            cobrCartdto.Observaciones = "Cobranza generada automáticamente por " + condicion.Nombre;

            CobranzasDetalleViewModel item = new CobranzasDetalleViewModel(2);
            cobrCartdto.Items = new List<CobranzasDetalleViewModel>();
            item.ID = 1;
            item.Comprobante = comprobante.Tipo + " " + punto.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
            item.Importe = comprobante.ImporteTotalNeto;
            item.IDComprobante = comprobante.IDComprobante;
            cobrCartdto.Items.Add(item);

            var usu = TokenCommon.ObtenerWebUser(dbContext, idUsuario); //TODO: Mejorar esto
            var idBanco = 0;
            /*   JC COBRANZA AUTOMATICA*/
            /*         var bancoBaseId = condicionVenta == "MercadoPago"
                          ? BANCO_MP
                          : dbContext.BancosBase.FirstOrDefault(b => b.Nombre == condicionVenta).IDBancoBase;
            
                      var bancoBaseId = condicion.IDBanco ?? dbContext.BancosBase.FirstOrDefault(b => b.Nombre == condicion.Nombre).IDBancoBase;
                      /*PREGUNTAR*/
            /*        int idBancoMP = 0;
                    var bancoMP = dbContext.Bancos.Where(x => x.IDUsuario == idUsuario && x.IDBancoBase == bancoBaseId).FirstOrDefault();

                    if (bancoMP == null) 
                        idBancoMP = BancosCommon.GuardarBanco(dbContext, 0, bancoBaseId, "", "Pesos Argentinos", 1, "0", "", "", "", "", "", idUsuario);
                    else
                        idBancoMP = bancoMP.IDBanco

                    if (!dbContext.BancosPlanDeCuenta.Any(x => x.IDBanco == idBancoMP))
                        ContabilidadCommon.CrearCuentaBancos(idBancoMP, usu);
                    idBanco = idBancoMP;*/


            CobranzasFormasDePagoViewModel formasDePago = new CobranzasFormasDePagoViewModel();
            cobrCartdto.FormasDePago = new List<CobranzasFormasDePagoViewModel>();
            formasDePago.ID = 1;
            formasDePago.Importe = comprobante.ImporteTotalNeto;
            formasDePago.FormaDePago = condicion.FormaDePago;
            formasDePago.IDBanco = condicion.IDBanco;
            formasDePago.IDCaja = condicion.IDCaja;
            formasDePago.NroReferencia = "";
            cobrCartdto.FormasDePago.Add(formasDePago);

            cobrCartdto.Retenciones = new List<CobranzasRetencionesViewModel>();


            var cobranza = CobranzasCommon.Guardar(dbContext, cobrCartdto, usu, false);
            //CobranzasCommon.EnviarCobranzaAutomaticamente(cobranza.IDCobranza, "", usu);
        }

        #endregion

        public static bool EliminarComprobante(int id, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Comprobantes.Where(x => x.IDComprobante == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null)
                {
                    if (entity.FechaCAE.HasValue)
                        throw new CustomException("No se puede eliminar por estar informado a la AFIP");
                    else if (dbContext.CobranzasDetalle.Any(x => x.IDComprobante == id && x.Cobranzas != null))
                        throw new CustomException("No se puede eliminar por tener cobranzas asociadas");
                    else if (entity.Tipo != "COT" && ContabilidadCommon.ValidarCierreContable(idUsuario, entity.FechaComprobante))
                        throw new CustomException("El comprobante no puede eliminarse ya que el año contable ya fue cerrado.");
                    else if (ContabilidadCommon.ValidarFechaLimiteCarga(idUsuario, Convert.ToDateTime(entity.FechaComprobante)))
                        throw new CustomException("El comprobante no puede eliminarse ya que la fecha se encuentra fuera de la fecha límite de carga.");

                    
                    InventariosCommon.RestaurarStockComprobante(dbContext, entity.Tipo, idUsuario, entity.ComprobantesDetalle.ToList(), entity.IDInventario ?? 0, id);

                    dbContext.Database.ExecuteSqlCommand("DELETE Asientos WHERE IDComprobante=" + id);

                    dbContext.Comprobantes.Remove(entity);
                    dbContext.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
        }

        public static int ConvertirFacturaEnAbono(int id, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                return 0; // dbContext.ConvertirFacturaEnAbono(id, idUsuario);
            }
        }

        public static void EnviarComprobanteAutomaticamente(int idComprobante, WebUser usu)
        {
            if (usu.EnvioAutomaticoComprobante)
            {
                var mensaje = "";
                var mail = "";

                using (var dbContext = new ACHEEntities())
                {
                    try
                    {
                        var plandeCuenta = PermisosModulosCommon.ObtenerPlanActual(dbContext, usu.IDUsuario);
                        if (plandeCuenta.IDPlan >= 3) //Plan Pyme
                        {
                            //bool CompEnviado = dbContext.ComprobantesEnviados.Any(x => x.IDUsuario == usu.IDUsuario && x.IDComprobante == idComprobante && x.Resultado == true);
                            //if (!CompEnviado)
                            //{
                            var avisos = dbContext.AvisosVencimiento.Where(x => x.IDUsuario == usu.IDUsuario && x.TipoAlerta == "Envio FE").FirstOrDefault();
                            if (avisos != null)
                            {
                                var c = dbContext.Comprobantes.Include("PuntosDeVenta").Include("Personas")
                                        .Where(x => x.IDComprobante == idComprobante && x.IDUsuario == usu.IDUsuario)
                                        .FirstOrDefault();
                                mail = string.IsNullOrEmpty(c.Personas.EmailsEnvioFc) ? c.Personas.Email : c.Personas.EmailsEnvioFc;

                                if (!string.IsNullOrEmpty(mail))
                                {
                                    mensaje = avisos.Mensaje.ReplaceAll("\n", "<br/>");
                                    mensaje = mensaje.Replace("LINKPAGO", "https://clientes.contabilium.com/public/factura?comprobante=" + Cryptography.Encrypt(idComprobante.ToString(), true));

                                    //var linkPago = "https://clientes.contabilium.com/public/factura?comprobante=" + Cryptography.Encrypt(idComprobante.ToString(), true);

                                    var file = c.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_" + c.Tipo +
                                               "-" + c.PuntosDeVenta.Punto.ToString("#0000") + "-" +
                                               c.Numero.ToString("#00000000") + ".pdf";

                                    var emailUsuarioTo = !string.IsNullOrEmpty(usu.EmailAlerta) ? usu.EmailAlerta : usu.Email;


                                    GuardarEnvioComprobanteMail(c.Personas.RazonSocial, mail, avisos.Asunto, mensaje, c.FechaComprobante.Year, file, usu.Logo, usu.IDUsuario, emailUsuarioTo, usu.EmailDisplayFrom ?? usu.RazonSocial, c.IDComprobante);
                                    //EnviarComprobantePorMail(c.Personas.RazonSocial, mail, avisos.Asunto, mensaje, c.FechaComprobante.Year, file, usu.Logo, usu.IDUsuario, emailUsuarioTo, usu.EmailDisplayFrom ?? usu.RazonSocial, idComprobante);

                                    //TODO: MEJORAR ESTO
                                    var msjSave = mensaje;
                                    if (mail.Contains("mail.mercadolibre.com"))
                                    {
                                        msjSave = msjSave.CleanHtmlTags();
                                        msjSave = msjSave.RemoverAcentosHTML();
                                    }
                                    ComprobantesCommon.GuardarComprobantesEnviados(dbContext, idComprobante, msjSave, null, "Comprobante enviado correctamente", true, usu, mail);
                                }
                                else
                                    throw new Exception("El cliente no tiene configurado direcciones de correo electrónico");
                            }
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        ComprobantesCommon.GuardarComprobantesEnviados(dbContext, idComprobante, mensaje, null, "Comprobante: " + ex.Message, false, usu, mail);
                        throw new Exception("No se pudo enviar automáticamente la factura electrónonica: " + ex.Message);
                    }
                }
            }
        }

        public static string Exportar(string condicion, string periodo, string fechaDesde, string fechaHasta, WebUser usu)
        {

            string fileName = "Comprobantes_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var ptosVentaIds = UsuarioCommon.ObtenerPuntosDeVenta(usu).Select(p => p.ID).ToList();

                    condicion = (!string.IsNullOrWhiteSpace(condicion)) ? condicion.ToLower() : "";
                    var results =
                        dbContext.Comprobantes.Include("Personas")
                            .Include("PuntosDeVenta")
                            .Where(x => x.IDUsuario == usu.IDUsuario && ptosVentaIds.Contains(x.PuntosDeVenta.IDPuntoVenta))
                            .AsQueryable();

                    Int32 numero = 0;

                    if (Int32.TryParse(condicion, out numero))
                        results = results.Where(x => x.Numero == Math.Abs(numero));
                    else if (condicion.Contains("-"))
                    {
                        var punto = (string.IsNullOrWhiteSpace(condicion.Split("-")[0])) ? "" : condicion.Split("-")[0];
                        var nro = (string.IsNullOrWhiteSpace(condicion.Split("-")[1])) ? "" : condicion.Split("-")[1];

                        Int32 ptoAux = 0;
                        Int32 nroAux = 0;

                        if (Int32.TryParse(punto, out ptoAux) || Int32.TryParse(nro, out nroAux))
                        {
                            if (punto != "" && Int32.TryParse(punto, out ptoAux))
                                results = results.Where(x => x.PuntosDeVenta.Punto == ptoAux);

                            if (nro != "" && Int32.TryParse(nro, out nroAux))
                                results = results.Where(x => x.Numero == nroAux);
                        }
                        else
                            results =
                                results.Where(
                                    x =>
                                        x.Personas.RazonSocial.ToLower().ToLower().Contains(condicion) ||
                                        x.Personas.NombreFantansia.ToLower().Contains(condicion) ||
                                        x.Tipo.ToLower().Contains(condicion));
                    }
                    else if (condicion != "")
                        results =
                            results.Where(
                                x =>
                                    x.Personas.RazonSocial.ToLower().Contains(condicion) ||
                                    x.Personas.NombreFantansia.ToLower().Contains(condicion) ||
                                    x.Tipo.ToLower().Contains(condicion));

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(fechaDesde))
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta))
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.FechaComprobante) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    dt = results.OrderBy(x => x.FechaComprobante).ToList().Select(x => new
                    {
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        TipoDoc = x.TipoDocumento,
                        Documento = x.NroDocumento,
                        Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                        FechaVencimiento = x.FechaComprobante.ToString("dd/MM/yyyy"),
                        Tipo = x.Tipo,
                        Modo = x.Modo == "E" ? "Electrónica" : (x.Modo == "T" ? "Talonario" : "Otro"),
                        Numero = x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                        Importe = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE") ? -x.ImporteTotalBruto : x.ImporteTotalBruto,
                        TotalFact = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE") ? -x.ImporteTotalNeto : x.ImporteTotalNeto,
                        Saldo = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE") ? -x.Saldo : x.Saldo,
                        CondicionVenta = x.CondicionesVentas.Nombre,
                        Observaciones = x.Observaciones,
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        public static string ImprimirMasivamente(string fechaDesde, string fechaHasta, string tipoComprobante, string puntos, string hojas, WebUser usu)
        {
            string fileName = "Comprobantes_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            string newFile = path + fileName + DateTime.Now.ToString("yyyyMMddHHMMss") + ".pdf";
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results =
                        dbContext.Comprobantes.Include("Personas")
                            .Include("PuntosDeVenta")
                            .Where(x => x.IDUsuario == usu.IDUsuario && x.FechaCAE.HasValue)
                            .AsQueryable();

                    if (tipoComprobante != string.Empty)
                    {
                        results = results.Where(x => x.Tipo == tipoComprobante);
                    }

                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaComprobante >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 11:59:59 pm");
                        results = results.Where(x => x.FechaComprobante <= dtHasta);
                    }
                    if (puntos != string.Empty)
                    {
                        var puntosAux = puntos.Split(",");
                        var searchIds = new List<int>();
                        foreach (var pto in puntosAux)
                        {
                            if (pto.Trim() != string.Empty)
                                searchIds.Add(int.Parse(pto.Trim()));
                        }

                        results = results.Where(x => searchIds.Contains(x.PuntosDeVenta.Punto));
                    }

                    //var pathComprobante = HttpContext.Current.Server.MapPath("~/files/explorer/" + CurrentUser.IDUsuario.ToString() + "/Comprobantes/" + entity.FechaAlta.Year.ToString() + "/" + fileName);=

                    var list = results.OrderBy(x => x.FechaComprobante).ToList().Select(x => new
                    {
                        RazonSocial = x.Personas.RazonSocial,
                        TipoDoc = x.Tipo,
                        FechaAlta = x.FechaAlta,
                        FechaComprobante = x.FechaComprobante,
                        Tipo = x.Tipo,
                        Punto = x.PuntosDeVenta.Punto,
                        Numero = x.Numero

                    }).ToList();

                    if (list.Count() > 2000)
                        throw new Exception("La cantidad de comprobantes es superior a 2000. Seleccione un rango de fechas menor");

                    List<String> listFile = new List<string>();
                    foreach (var item in list)
                    {
                        var auxFileName = item.RazonSocial.RemoverCaracteresParaPDF() + "_" + item.Tipo + "-" + item.Punto.ToString("#0000") + "-" + item.Numero.ToString("#00000000") + ".pdf";
                        var pathComprobante = HttpContext.Current.Server.MapPath("~/files/explorer/" + usu.IDUsuario.ToString() + "/Comprobantes/" + item.FechaAlta.Year.ToString() + "/" + auxFileName);
                        if (System.IO.File.Exists(pathComprobante))
                            listFile.Add(pathComprobante);
                    }

                    if (listFile.Any())
                    {
                        //itextsharp 5 - tarda mas y archivo mas pesado
                        /*using (FileStream stream = new FileStream(HttpContext.Current.Server.MapPath(newFile), FileMode.Create))
                        {
                            Document document = new Document();
                            PdfCopy pdf = new PdfCopy(document, stream);
                            PdfReader reader = null;
                            try
                            {
                                document.Open();
                                foreach (string file in listFile)
                                {
                                    reader = new PdfReader(file);
                                    //not needed I guess
                                    reader.ConsolidateNamedDestinations();
                                    pdf.AddDocument(reader);
                                    //pdf.AddPage(pdf.GetImportedPage(reader, 2));
                                    reader.Close();
                                }
                            }
                            catch (Exception)
                            {
                                //merged = false;
                                if (reader != null)
                                {
                                    reader.Close();
                                }
                            }
                            finally
                            {
                                if (pdf != null)
                                {
                                    pdf.Close();
                                }
                                if (document != null)
                                {
                                    document.Close();
                                }
                            }
                        }*/


                        //itext7 - much fosterr and file size chico
                        PdfWriter writer = new PdfWriter(HttpContext.Current.Server.MapPath(newFile));
                        writer.SetSmartMode(true);
                        writer.SetCompressionLevel(CompressionConstants.BEST_SPEED);

                        PdfDocument pdfDoc = new PdfDocument(writer);

                        //pdfDoc.InitializeOutlines();

                        int index = 1;
                        foreach (string file in listFile)
                        {

                            PdfDocument addedDoc = new PdfDocument(new PdfReader(file));
                            if (hojas == "D")
                            {
                                if (addedDoc.GetNumberOfPages() > 2)
                                    addedDoc.CopyPagesTo(2, 2, pdfDoc);
                                else
                                    addedDoc.CopyPagesTo(1, 1, pdfDoc);
                            }
                            else if (hojas == "O")
                                addedDoc.CopyPagesTo(1, 1, pdfDoc);
                            else
                            {
                                addedDoc.CopyPagesTo(1, 1, pdfDoc);
                                if (addedDoc.GetNumberOfPages() > 2)
                                    addedDoc.CopyPagesTo(2, 2, pdfDoc);
                            }

                            addedDoc.Close();
                            //pdfDoc. FreeReader(reader);

                            index++;

                            /*if (index > 500)
                            {
                                writer.Flush();
                                index = 0;
                            }*/
                        }

                        //writer.Flush();
                        //writer.Close();
                        pdfDoc.Close();


                        //var bytes = NFPDFWriter.MergePDFsInMemory(listFile, 2);
                        //File.WriteAllBytes(HttpContext.Current.Server.MapPath(newFile), bytes);
                        //NFPDFWriter.MergePDFs(listFile, HttpContext.Current.Server.MapPath(newFile), 2);
                    }
                    else
                        throw new Exception("No se encuentran archivos generados");
                }

                //if (dt.Rows.Count > 0)
                //    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                //else
                //    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (newFile).Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }


        public static ResultadosImportacionesComprobantesViewModel ObtenerComprobantesAFacturar(int idUsu, int page, int pageSize)
        {
            try
            {

                using (var dbContext = new ACHEEntities())
                {
                    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                    var results =
                        dbContext.Comprobantes.Include("Personas")
                            .Include("PuntosDeVenta")
                            .Where(x => x.IDUsuario == idUsu && x.Personas.IDUsuario == idUsu && x.Tipo != "COT" && x.Modo == "E" && x.FechaCAE == null)
                            .AsQueryable();
                    page--;
                    ResultadosImportacionesComprobantesViewModel resultado = new ResultadosImportacionesComprobantesViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    //Determinamos el formato decimal
                    var decimalStringFormat = "N2";// DecimalFormatter.GetDecimalStringFormat(idUsu);

                    var list = results.OrderBy(x => x.FechaComprobante)
                        .ThenBy(x => x.Numero)
                        .Skip(page * pageSize)
                        .Take(pageSize)
                        .ToList()
                        .Select(x => new ComprobantesViewModel()
                        {
                            ID = x.IDComprobante,
                            IDComprobante = x.IDComprobante,
                            RazonSocial =
                                (x.Personas.NombreFantansia == ""
                                    ? x.Personas.RazonSocial.ToUpper()
                                    : x.Personas.NombreFantansia.ToUpper()),
                            Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                            Tipo = x.Tipo,
                            Modo = x.Modo == "E" ? "Electrónica" : (x.Modo == "T" ? "Talonario" : "Otro"),
                            Numero = x.PuntosDeVenta.Punto.ToString("#0000"),// + "-" + x.Numero.ToString("#00000000"),
                            ImporteTotalNeto =
                                (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE")
                                    ? "-" + x.ImporteTotalNeto.ToString(decimalStringFormat)
                                    : x.ImporteTotalNeto.ToString(decimalStringFormat),
                            ImporteTotalBruto =
                                (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCE")
                                    ? "-" + x.ImporteTotalBruto.ToString(decimalStringFormat)
                                    : x.ImporteTotalBruto.ToString(decimalStringFormat),
                            PuedeAdm = x.FechaCAE.HasValue ? "F" : "T",
                            PuedeAnular = (x.Tipo == "FCA" || x.Tipo == "FCB" || x.Tipo == "FCC" || x.Tipo == "FCM" || x.Tipo == "FCE") ? "T" : "F",
                            IdPersona = x.IDPersona,
                            TipoConcepto = x.TipoConcepto,
                            CondicionVenta = x.CondicionesVentas.Nombre,
                            FechaAlta = x.FechaAlta,
                            FechaVencimiento = x.FechaVencimiento,
                            Observaciones = x.Observaciones,
                            IdPuntoVenta = x.IDPuntoVenta,
                            Cae = x.CAE,

                            Saldo = x.Saldo
                        });
                    resultado.Items = list.ToList();

                    return resultado;
                }


            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void EnviarComprobantePorMail(string nombre, string para, string asunto, string mensaje,
            int year, string file, string logo, int idUsuario, string emailUsuario, string displayFrom, int idComprobante)
        {
            EnviarComprobantePorMail(nombre, para, asunto, mensaje,
                year, file, logo, idUsuario, emailUsuario, displayFrom, idComprobante, "comprobantes");
        }

        public static void EnviarComprobantePorMail(string nombre, string para, string asunto, string mensaje,
         int year, string file, string logo, int idUsuario, string emailUsuario, string displayFrom, int idComprobante, string tipoComprobante)
        {
            MailAddressCollection listTo = new MailAddressCollection();
            bool esMailDeML = false;

            string linkPago = "https://clientes.contabilium.com/public/factura?comprobante=" + Cryptography.Encrypt(idComprobante.ToString(), true);

            if (para.Contains("mail.mercadolibre.com"))
                esMailDeML = true;

            foreach (var mail in para.Split(','))
            {
                if (mail != string.Empty)
                {
                    listTo.Add(new MailAddress(mail));
                }
            }

            if (esMailDeML)
            {
                mensaje = mensaje.CleanHtmlTags();
                mensaje = mensaje.RemoverAcentosHTML();
                mensaje = mensaje.Replace("<USUARIO>", "");
                mensaje = mensaje.Replace("<p>", "");
                mensaje = mensaje.Replace("</p>", "");
            }
            else
            {
                mensaje = mensaje.ReplaceAll("\n", "<br/>");
                mensaje = mensaje.Replace("LINKPAGO", linkPago);
            }

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<NOTIFICACION>", mensaje);
            replacements.Add("<LINKPAGO>", linkPago);
            if (nombre.Trim() != string.Empty)
            {
                replacements.Add("<CLIENTE>", nombre);
                replacements.Add("<USUARIO>", nombre);
            }
            else
            {
                replacements.Add("<CLIENTE>", "usuario");
                replacements.Add("<USUARIO>", "usuario");
            }

            if (logo != string.Empty)
                replacements.Add("<LOGOEMPRESA>", "/files/usuarios/" + logo);
            else
                replacements.Add("<LOGOEMPRESA>", "/images/logo-mail-gris.png");


            List<string> attachments = new List<string>();
            var physicalPath = HttpContext.Current.Server.MapPath("~/files/explorer/" + idUsuario + "/" + tipoComprobante + "/" + year.ToString() + "/" + file);
            physicalPath = physicalPath.Replace("rest", "app");//TODO: FIx para WebApi. Mejorar

            attachments.Add(physicalPath);

            bool send = false;

            if (!esMailDeML)
                send = EmailCommon.SendMessage(idUsuario, EmailTemplate.EnvioComprobanteConFoto, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuario, asunto, attachments, displayFrom);
            else
                send = EmailCommon.SendMessage(idUsuario, EmailTemplate.EnvioComprobanteSinFormato, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuario, asunto, attachments, displayFrom);

            if (!send)
                throw new Exception("El mensaje no pudo ser enviado. Por favor, intente nuevamente. En caso de continuar el error, escribenos a <a href='" + ConfigurationManager.AppSettings["Email.Ayuda"] + "'>" + ConfigurationManager.AppSettings["Email.Ayuda"] + "</a>");
        }


        public static void GuardarEnvioComprobanteMail(string nombre, string para, string asunto, string mensaje,
      int year, string file, string logo, int idUsuario, string emailUsuario, string displayFrom, int idComprobante)
        {
            MailAddressCollection listTo = new MailAddressCollection();
            bool esMailDeML = false;

            string linkPago = "https://clientes.contabilium.com/public/factura?comprobante=" + Cryptography.Encrypt(idComprobante.ToString(), true);

            if (para.Contains("mail.mercadolibre.com"))
                esMailDeML = true;

            foreach (var mail in para.Split(','))
            {
                if (mail != string.Empty)
                {
                    listTo.Add(new MailAddress(mail));
                }
            }

            if (esMailDeML)
            {
                mensaje = mensaje.CleanHtmlTags();
                mensaje = mensaje.RemoverAcentosHTML();
                mensaje = mensaje.Replace("<USUARIO>", "");
                mensaje = mensaje.Replace("<p>", "");
                mensaje = mensaje.Replace("</p>", "");
            }
            else
            {
                mensaje = mensaje.ReplaceAll("\n", "<br/>");
                mensaje = mensaje.Replace("LINKPAGO", linkPago);
            }

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<NOTIFICACION>", mensaje);
            replacements.Add("<LINKPAGO>", linkPago);


            if (nombre.Trim() != string.Empty)
            {
                replacements.Add("<CLIENTE>", nombre);
                replacements.Add("<USUARIO>", nombre);
            }
            else
            {
                replacements.Add("<CLIENTE>", "usuario");
                replacements.Add("<USUARIO>", "usuario");
            }

            if (logo != string.Empty)
                replacements.Add("<LOGOEMPRESA>", "/files/usuarios/" + logo);
            else
                replacements.Add("<LOGOEMPRESA>", "/images/logo-mail-gris.png");


            List<string> attachments = new List<string>();
            var physicalPath = HttpContext.Current.Server.MapPath("~/files/explorer/" + idUsuario + "/comprobantes/" + year.ToString() + "/" + file);
            physicalPath = physicalPath.Replace("rest", "app");//TODO: FIx para WebApi. Mejorar

            attachments.Add(physicalPath);

            bool send = false;

            if (!esMailDeML)
                send = EmailCommon.SaveMessage(idUsuario, EmailTemplate.EnvioComprobanteConFoto, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuario, asunto, attachments, displayFrom);
            else
                send = EmailCommon.SaveMessage(idUsuario, EmailTemplate.EnvioComprobanteSinFormato, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], emailUsuario, asunto, attachments, displayFrom);

            if (!send)
                throw new Exception("El mensaje no pudo ser enviado. Por favor, intente nuevamente. En caso de continuar el error, escribenos a <a href='" + ConfigurationManager.AppSettings["Email.Ayuda"] + "'>" + ConfigurationManager.AppSettings["Email.Ayuda"] + "</a>");
        }

        /*public static void EnviarComprobantePorMail(string nombre, string para, string asunto, string mensaje, string file, string logo, int idUsuario, string emailUsuario)
        {
            EnviarComprobantePorMail(nombre, para, asunto, mensaje, file, logo, idUsuario, emailUsuario, "");
        }*/


        internal static bool ExisteComprobantePorVentaIntegracion(int idIntegracion, string ordenId)
        {
            using (var dbContext = new ACHEEntities())
            {
                return
                    dbContext.Comprobantes.Any(c => c.IDIntegracion == idIntegracion && c.IDVentaIntegracion == ordenId);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Productos;
using ACHE.Model.Negocio;
using ACHE.Negocio.Contabilidad;
using System.IO;
using ACHE.Extensions;
using System.Configuration;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Web;
using ACHE.Negocio.Helpers;
using ACHE.Negocio.Common;
using System.Data;
namespace ACHE.Negocio.Facturacion {
    public static class CobranzasCommon {
        public static Cobranzas Guardar(ACHEEntities dbContext, CobranzaCartDto cobranza, WebUser usu, bool sobranteACuenta) {
            if (ContabilidadCommon.ValidarCierreContable(usu.IDUsuario, Convert.ToDateTime(cobranza.Fecha)))
                throw new CustomException("No puede agregar ni modificar una cobranza que se encuentre en un periodo cerrado.");

            //Determinamos la cantidad de decimales configurada para el usuario
            //var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

            //Obtenemos el formato decimal
            //var decimalFormat = DecimalFormatter.GetDecimalStringFormat(usu.IDUsuario);

            Cobranzas entity;
            if (cobranza.IDCobranza > 0) {
                entity = dbContext.Cobranzas
                    .Include("CobranzasDetalle").Include("CobranzasFormasDePago").Include("CobranzasRetenciones").Include("CobranzasFormasDePago.Comprobantes")
                    .Where(x => x.IDCobranza == cobranza.IDCobranza && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                entity.UsuarioModifica = usu.Email;
            }
            else {
                entity = new Cobranzas();
                entity.FechaAlta = DateTime.Now;
                entity.IDUsuario = usu.IDUsuario;
                entity.UsuarioAlta = usu.Email;
            }

            Personas persona = dbContext.Personas.Where(x => x.IDPersona == cobranza.IDPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            if (persona == null)
                throw new CustomException("El cliente/proveedor es inexistente");

            entity.IDPersona = cobranza.IDPersona;
            entity.Tipo = cobranza.Tipo;
            entity.Modo = "T";
            entity.FechaCobranza = DateTime.Parse(cobranza.Fecha);
            entity.TipoDestinatario = persona.Tipo.ToUpper();
            entity.NroDocumento = persona.NroDocumento;
            entity.TipoDocumento = persona.TipoDocumento;

            entity.IDPuntoVenta = cobranza.IDPuntoVenta;
            if (cobranza.NumeroCobranza != "") {
                entity.Numero = int.Parse(cobranza.NumeroCobranza);
                entity.FechaProceso = DateTime.Now;
            }
            entity.Observaciones = cobranza.Observaciones;

            #region Detalle, Formas y Retenciones

            List<int> listComp = new List<int>();
            if (entity.CobranzasDetalle.Any()) {
                foreach (var item in entity.CobranzasDetalle) {
                    if (item.IDComprobante.HasValue) {
                        if (!listComp.Any(x => x == item.IDComprobante.Value))
                            listComp.Add(item.IDComprobante.Value);
                    }
                }

                dbContext.CobranzasDetalle.RemoveRange(entity.CobranzasDetalle);
            }

            decimal total = 0;
            foreach (var det in cobranza.Items) {
                CobranzasDetalle compDet = new CobranzasDetalle();
                compDet.IDComprobante = det.IDComprobante;
                compDet.IDCobranza = entity.IDCobranza;
                compDet.Importe = det.Importe;

                total += det.Total;
                entity.CobranzasDetalle.Add(compDet);
            }

            entity.ImporteTotal = total;// Math.Round(total, cantidadDecimales);

            if (entity.CobranzasFormasDePago.Any()) {
                var nota = entity.CobranzasFormasDePago.Where(x => x.IDNotaCredito != null && x.IDCobranza == cobranza.IDCobranza).FirstOrDefault();
                if (nota != null)
                    nota.Comprobantes.Saldo = nota.Importe;

                dbContext.CobranzasFormasDePago.RemoveRange(entity.CobranzasFormasDePago);
            }
            var tieneNC = false;
            decimal totalFormasDePago = 0;
            foreach (var det in cobranza.FormasDePago) {
                CobranzasFormasDePago compForm = new CobranzasFormasDePago();
                compForm.IDCobranza = entity.IDCobranza;
                compForm.Importe = det.Importe;
                compForm.FormaDePago = det.FormaDePago;
                compForm.NroReferencia = det.NroReferencia;
                compForm.IDCheque = det.IDCheque;
                compForm.IDBanco = det.IDBanco;
                compForm.IDCaja = det.IDCaja;
                compForm.IDNotaCredito = det.IDNotaCredito;

                totalFormasDePago += det.Importe;

                entity.CobranzasFormasDePago.Add(compForm);

                var cheques = dbContext.Cheques.Where(x => x.IDCheque == det.IDCheque).FirstOrDefault();
                if (cheques != null) {
                    if (!dbContext.PagosFormasDePago.Any(x => x.IDCheque == det.IDCheque))
                        cheques.Estado = "Libre";
                }

                if (compForm.IDNotaCredito != null)
                    tieneNC = true;
            }

            if (entity.CobranzasRetenciones.Any())
                dbContext.CobranzasRetenciones.RemoveRange(entity.CobranzasRetenciones);

            foreach (var det in cobranza.Retenciones) {
                CobranzasRetenciones compRet = new CobranzasRetenciones();
                compRet.IDCobranza = entity.IDCobranza;
                compRet.Importe = det.Importe;
                compRet.Tipo = det.Tipo;
                compRet.NroReferencia = det.NroReferencia;
                if (string.IsNullOrEmpty(det.Fecha))
                    compRet.FechaRetencion = null;
                else
                    compRet.FechaRetencion = DateTime.Parse(det.Fecha);
                if (det.IDJurisdiccion > 0)
                    compRet.IDJurisdiccion = det.IDJurisdiccion;

                totalFormasDePago += det.Importe;

                entity.CobranzasRetenciones.Add(compRet);
            }

            #endregion

            if (total == 0 && !tieneNC)
                throw new CustomException("No puede generar un recibo con Importe 0");

            if (totalFormasDePago != total)// && !tieneNC)
            {
                if (sobranteACuenta)
                    entity.CobranzasDetalle.Add(new CobranzasDetalle { IDCobranza = entity.IDCobranza, IDComprobante = null, Importe = (totalFormasDePago - total) });
                else {
                    var dif = (total - totalFormasDePago);
                    if (dif > 0)
                        throw new CustomException("El monto a pagar no coincide. Hay una diferencia de " + (dif).ToString("N2"));
                    else
                        throw new CustomException("Las formas de cobro deben coincidir. Hay una diferencia de " + (dif).ToString("N2"));
                }
            }

            if (cobranza.IDCobranza > 0) {
                dbContext.SaveChanges();
            }
            else {
                dbContext.Cobranzas.Add(entity);
                dbContext.SaveChanges();
            }

            dbContext.ActualizarSaldosPorCobranza(entity.IDCobranza);

            //Se hace esto por si se eliminan items de la cobranzas. Para que den los saldos.
            foreach (var item in listComp)
                dbContext.ActualizarSaldosPorComprobante(item);

            ContabilidadCommon.AgregarAsientoDeCobranza(usu, entity.IDCobranza);
            return entity;
        }

        public static Cobranzas Guardar(CobranzaCartDto cobrCartdto, WebUser usu, bool sobranteACuenta) {
            using (var dbContext = new ACHEEntities()) {
                return Guardar(dbContext, cobrCartdto, usu, sobranteACuenta);
            }
        }

        public static string obtenerProxNroCobranza(string tipo, int idUsuario) {
            try {
                var nro = "00000001";

                if (idUsuario == 2157)//TODO: que sea configurable por usuario
                    nro = "00001000";

                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.Cobranzas.Any(x => x.IDUsuario == idUsuario && x.Tipo == tipo)) {
                        var aux = dbContext.Cobranzas.Where(x => x.IDUsuario == idUsuario && x.Tipo == tipo).ToList();
                        if (aux.Count() > 0)
                            nro = (aux.Max(x => x.Numero) + 1).ToString("#00000000");
                        //else
                        //    nro = "00000001";
                    }
                    //else
                    //    nro = "00000001";
                }

                return nro;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static bool EliminarCobranza(int id, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var entity = dbContext.Cobranzas
                        .Include("CobranzasDetalle").Include("CobranzasFormasDePago").Include("CobranzasRetenciones")
                        .Where(x => x.IDCobranza == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null) {
                        if (entity.FechaCAE.HasValue)
                            throw new CustomException("No se puede eliminar por estar informado a la AFIP");
                        else if (ContabilidadCommon.ValidarCierreContable(idUsuario, entity.FechaCobranza))
                            throw new CustomException("El comprobante no puede eliminarse ya que el año contable ya fue cerrado.");

                        List<int> listComp = new List<int>();
                        foreach (var item in entity.CobranzasDetalle) {
                            if (item.IDComprobante.HasValue) {
                                if (!listComp.Any(x => x == item.IDComprobante.Value))
                                    listComp.Add(item.IDComprobante.Value);
                            }
                        }
                        foreach (var item in entity.CobranzasFormasDePago) {
                            if (item.IDCheque.HasValue) {
                                var cheque = dbContext.Cheques.Where(x => x.IDCheque == item.IDCheque.Value).FirstOrDefault();
                                if (cheque != null) {
                                    cheque.Estado = "Libre";
                                }
                            }
                            else if (item.IDNotaCredito.HasValue) {
                                if (!listComp.Any(x => x == item.IDNotaCredito.Value))
                                    listComp.Add(item.IDNotaCredito.Value);
                            }
                        }

                        int idPersona = entity.IDPersona;
                        var archivo = entity.Personas.RazonSocial.RemoverCaracteresParaPDF() + "_RC-" + entity.Numero.ToString("#00000000") + ".pdf";
                        var fecha = entity.FechaAlta.Year.ToString();
                        dbContext.Database.ExecuteSqlCommand("DELETE Asientos WHERE IDCobranza=" + id);
                        //dbContext.Database.ExecuteSqlCommand("DELETE CobranzasDetalle WHERE IDCobranza=" + id);// No se porqué no anda bien el detele en cascada
                        //dbContext.Database.ExecuteSqlCommand("DELETE CobranzasRetenciones WHERE IDCobranza=" + id);// No se porqué no anda bien el detele en cascada
                        //dbContext.Database.ExecuteSqlCommand("DELETE CobranzasFormasDePago WHERE IDCobranza=" + id);// No se porqué no anda bien el detele en cascada

                        dbContext.Cobranzas.Remove(entity);
                        dbContext.SaveChanges();

                        foreach (var item in listComp)
                            dbContext.ActualizarSaldosPorComprobante(item);

                        //dbContext.ActualizarSaldosPorPersona(idPersona);
                        var path = Path.Combine(ConfigurationManager.AppSettings["PathBaseWeb"] + "/files/explorer/" + idUsuario.ToString() + "/Comprobantes/" + fecha + "/" + archivo);
                        if (File.Exists(path))
                            File.Delete(path);

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static ResultadosComprobantesViewModel ObtenerCobranzas(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize, WebUser usu) {
            var ptosVentaIds = UsuarioCommon.ObtenerPuntosDeVenta(usu).Select(p => p.ID).ToList();

            using (var dbContext = new ACHEEntities()) {
                //Obtenemos el formato decimal
                var decimalFormat = "N2";//DecimalFormatter.GetDecimalStringFormat(usu.IDUsuario);

                var results = dbContext.Cobranzas.Include("Personas").Include("PuntosDeVenta")
                    .Where(x => x.IDUsuario == usu.IDUsuario && ptosVentaIds.Contains(x.PuntosDeVenta.IDPuntoVenta)).AsQueryable();

                Int32 numero = 0;
                if (Int32.TryParse(condicion, out numero))
                    results = results.Where(x => x.Numero == numero);
                else if (!string.IsNullOrWhiteSpace(condicion))
                    results = results.Where(x => x.Personas.RazonSocial.ToLower().Contains(condicion.ToLower()) || x.Personas.NombreFantansia.ToLower().Contains(condicion.ToLower()));

                switch (periodo) {
                    case "30":
                        fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                        break;
                    case "15":
                        fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                        break;
                    case "7":
                        fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                        break;
                    case "1":
                        fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                        break;
                    case "0":
                        fechaDesde = DateTime.Now.ToShortDateString();
                        break;
                }

                if (!string.IsNullOrWhiteSpace(fechaDesde)) {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.FechaCobranza >= dtDesde);
                }
                if (!string.IsNullOrWhiteSpace(fechaHasta)) {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    results = results.Where(x => x.FechaCobranza <= dtHasta);
                }

                page--;
                ResultadosComprobantesViewModel resultado = new ResultadosComprobantesViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.FechaCobranza).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new ComprobantesViewModel() {
                        ID = x.IDCobranza,
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        Fecha = x.FechaCobranza.ToString("dd/MM/yyyy"),
                        Tipo = x.Tipo == "SIN" ? "-" : "RC",
                        Numero = x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                        ImporteTotalNeto = x.ImporteTotal.ToString(decimalFormat),
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }

        public static void EnviarCobranzaAutomaticamente(int idCobranza, string file, WebUser usu) {
            if (usu.EnvioAutomaticoRecibo) {
                using (var dbContext = new ACHEEntities()) {
                    var mensaje = "";
                    var mail = "";

                    try {
                        var plandeCuenta = PermisosModulosCommon.ObtenerPlanActual(dbContext, usu.IDUsuario);
                        if (plandeCuenta.IDPlan >= 3)//Plan Pyme
                        {
                            //bool CompEnviado = dbContext.ComprobantesEnviados.Any(x => x.IDUsuario == usu.IDUsuario && x.IDCobranza == idCobranza && x.Resultado == true);
                            //if (!CompEnviado)
                            //{
                            var avisos = dbContext.AvisosVencimiento.Where(x => x.IDUsuario == usu.IDUsuario && x.TipoAlerta == "Envio CR").FirstOrDefault();
                            if (avisos != null) {
                                var c = dbContext.Cobranzas.Where(x => x.IDCobranza == idCobranza && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                                mail = string.IsNullOrEmpty(c.Personas.EmailsEnvioFc) ? c.Personas.Email : c.Personas.EmailsEnvioFc;

                                if (!string.IsNullOrEmpty(mail)) {
                                    mensaje = avisos.Mensaje.ReplaceAll("\n", "<br/>");

                                    EnviarComprobantePorMail(c.Personas.RazonSocial, mail, avisos.Asunto, mensaje, c.FechaCobranza.Year, file, usu.Logo, usu.IDUsuario, usu.EmailDisplayFrom ?? usu.RazonSocial);
                                    ComprobantesCommon.GuardarComprobantesEnviados(dbContext, null, mensaje, idCobranza, "Cobranza enviada correctamente", true, usu, mail);
                                }
                                else
                                    throw new Exception("El cliente no tiene configurado direcciones de correo electrónico");
                            }
                            //}
                        }
                    }
                    catch (Exception ex) {
                        ComprobantesCommon.GuardarComprobantesEnviados(dbContext, null, mensaje, idCobranza, "Cobranza: " + ex.Message, false, usu, mail);
                        throw new Exception("No se pudo enviar automáticamente el recibo de cobranza.");
                    }
                }
            }
        }

        public static void EnviarComprobantePorMail(string nombre, string para, string asunto, string mensaje, int year, string file, string logo, int idUsuario, string displayFrom) {
            MailAddressCollection listTo = new MailAddressCollection();
            foreach (var mail in para.Split(',')) {
                if (mail != string.Empty)
                    listTo.Add(new MailAddress(mail));
            }

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<NOTIFICACION>", mensaje);
            if (nombre.Trim() != string.Empty) {
                replacements.Add("<CLIENTE>", nombre);
                replacements.Add("<USUARIO>", nombre);
            }
            else {
                replacements.Add("<CLIENTE>", "usuario");
                replacements.Add("<USUARIO>", "usuario");
            }
            if (logo != string.Empty)
                replacements.Add("<LOGOEMPRESA>", "/files/usuarios/" + logo);
            else
                replacements.Add("<LOGOEMPRESA>", "/images/logo-mail-gris.png");


            List<string> attachments = new List<string>();
            attachments.Add(HttpContext.Current.Server.MapPath("~/files/explorer/" + idUsuario + "/comprobantes/" + year.ToString() + "/" + file));

            bool send = EmailCommon.SendMessage(idUsuario, EmailTemplate.EnvioCobranza, replacements, listTo, ConfigurationManager.AppSettings["Email.Notifications"], asunto, attachments, displayFrom);
            if (!send)
                throw new Exception("El mensaje no pudo ser enviado. Por favor, intente nuevamente. En caso de continuar el error, escribenos a <a href='" + ConfigurationManager.AppSettings["Email.Ayuda"] + "'>" + ConfigurationManager.AppSettings["Email.Ayuda"] + "</a>");
        }

        public static ResultadosComprobantesViewModel ObtenerCobranzasACuenta(int idPersona, int page, int pageSize, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var results =
                    dbContext.CobranzasDetalle.Include("Cobranzas").Include("Cobranzas.Personas").Include("Cobranzas.PuntosDeVenta")
                        .Where(x => x.Cobranzas.IDUsuario == idUsuario && !x.IDComprobante.HasValue)
                        .ToList();

                if (idPersona > 0)
                    results = results.Where(x => x.Cobranzas.IDPersona == idPersona).ToList();

                //Determinamos el formato decimal
                var decimalFormat = "N2";// DecimalFormatter.GetDecimalStringFormat(idUsuario);

                page--;
                ResultadosComprobantesViewModel resultado = new ResultadosComprobantesViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.Cobranzas.FechaCobranza).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new ComprobantesViewModel() {
                        ID = x.IDDetalle,
                        RazonSocial = (x.Cobranzas.Personas.NombreFantansia == "" ? x.Cobranzas.Personas.RazonSocial.ToUpper() : x.Cobranzas.Personas.NombreFantansia.ToUpper()),
                        Fecha = x.Cobranzas.FechaCobranza.ToString("dd/MM/yyyy"),
                        Numero = x.Cobranzas.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Cobranzas.Numero.ToString("#00000000"),
                        ImporteTotalNeto = x.Importe.ToString(decimalFormat),
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }

        public static void AplicarCobranzasACuenta(int idPersona, Dictionary<int, List<int>> cobranzasAAplicar) {
            using (var dbContext = new ACHEEntities()) {
                var comprobantes = dbContext.Comprobantes.Where(x => cobranzasAAplicar.Keys.Contains(x.IDComprobante)).ToList();
                var cobranzasDetallesIds = cobranzasAAplicar.Values.SelectMany(l => l).ToList();
                var cobranzasDetalles =
                    dbContext.CobranzasDetalle.Where(pd => cobranzasDetallesIds.Contains(pd.IDDetalle))
                        .ToDictionary(c => c.IDDetalle, c => c);

                foreach (var comprobante in comprobantes) {
                    var montoComprabante = comprobante.Saldo;
                    foreach (var cobranzaDetalleId in cobranzasAAplicar[comprobante.IDComprobante]) {
                        if (montoComprabante > 0)
                            if (cobranzasDetalles[cobranzaDetalleId].Importe <= montoComprabante) {
                                cobranzasDetalles[cobranzaDetalleId].IDComprobante = comprobante.IDComprobante;

                                montoComprabante -= cobranzasDetalles[cobranzaDetalleId].Importe;
                            }
                            else {
                                var cobranzaDetalle = cobranzasDetalles[cobranzaDetalleId];
                                dbContext.CobranzasDetalle.Add(new CobranzasDetalle {
                                    Importe = cobranzaDetalle.Importe - montoComprabante,
                                    IDCobranza = cobranzaDetalle.IDCobranza
                                });
                                cobranzaDetalle.Importe = montoComprabante;
                                cobranzaDetalle.IDComprobante = comprobante.IDComprobante;
                                montoComprabante = 0;
                            }



                    }
                }
                dbContext.SaveChanges();

                foreach (var comprobante in comprobantes) {
                    dbContext.ActualizarSaldosPorComprobante(comprobante.IDComprobante);
                }
            }
        }

        public static int ObtenerIDCobranzaPorDetalle(int idDetalle) {
            using (var dbContext = new ACHEEntities()) {
                int idCobranza = dbContext.CobranzasDetalle.Where(x => x.IDComprobante == idDetalle).Select(x => x.IDCobranza).FirstOrDefault();
                return idCobranza;
            }
        }

        public static string ExportarCobranzas(int idPersona, string condicion, string periodo, string fechaDesde, string fechaHasta, int idUsuario) {

            string fileName = "Cobranzas_" + idUsuario + "_";
            string path = "~/tmp/";
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var results = dbContext.Cobranzas.Include("Personas").Include("PuntosDeVenta").Where(x => x.IDUsuario == idUsuario).AsQueryable();
                    Int32 numero = 0;
                    if (Int32.TryParse(condicion, out numero)) {
                        results = results.Where(x => x.Numero == numero);
                    }
                    else if (condicion != string.Empty) {
                        results = results.Where(x => x.Personas.RazonSocial.Contains(condicion) || x.Personas.NombreFantansia.Contains(condicion));
                    }


                    switch (periodo) {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (fechaDesde != string.Empty) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaCobranza >= dtDesde);
                    }
                    if (fechaHasta != string.Empty) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.FechaCobranza) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    dt = results.OrderBy(x => x.FechaCobranza).ToList().Select(x => new {
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        Documento = x.TipoDocumento + " " + x.NroDocumento,
                        Fecha = x.FechaCobranza.ToString("dd/MM/yyyy"),
                        Tipo = x.Tipo == "SIN" ? "" : "RC",
                        //Modo = x.Modo == "E" ? "Electrónica" : (x.Modo == "T" ? "Talonario" : "Otro"),
                        //Numero = x.Tipo == "SIN" ? "" : x.Numero.ToString("#00000000"),
                        Numero = x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                        ImporteTotalNeto = x.ImporteTotal,
                        Observaciones = x.Observaciones,
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }


        }

        public static string ObtenerAcuenta(int idUsuario) {
            var html = "";

            using (var dbContext = new ACHEEntities()) {
                var cobranzas = dbContext.CobranzasDetalle.Where(x => !x.IDComprobante.HasValue && x.Cobranzas.IDUsuario == idUsuario).OrderBy(x => x.Cobranzas.FechaCobranza).ToList();
                foreach (var det in cobranzas.GroupBy(x => x.IDCobranza)) {
                    html += "<tr><td>" + det.First().Cobranzas.FechaCobranza.ToString("dd/MM/yyyy") + "</td>";
                    html += "<td>" + det.First().Cobranzas.PuntosDeVenta.Punto.ToString("#0000") + "-" + det.First().Cobranzas.Numero.ToString("#00000000") + "</td>";
                    html += "<td>" + (det.First().Cobranzas.Personas.NombreFantansia == "" ? det.First().Cobranzas.Personas.RazonSocial.ToUpper() : det.First().Cobranzas.Personas.NombreFantansia.ToUpper()) + "</td>";
                    html += "<td>" + det.Sum(x => x.Importe).ToString("N2") + "</td></tr>";
                }
            }

            return html;
        }


        public static void GenerarAlertas(Cobranzas cobranzas) {
            using (var dbContext = new ACHEEntities()) {
                var listaAlertas = dbContext.Alertas.Where(x => x.IDUsuario == cobranzas.IDUsuario).ToList();

                foreach (var alertas in listaAlertas) {
                    if (alertas.AvisoAlerta == "El cobro a un cliente es") {
                        switch (alertas.Condicion) {
                            case "Mayor o igual que":
                                if (cobranzas.ImporteTotal >= alertas.Importe) {
                                    CobranzasCommon.InsertarAlerta(dbContext, cobranzas, alertas);
                                }
                                break;
                            case "Menor o igual que":
                                if (cobranzas.ImporteTotal <= alertas.Importe) {
                                    CobranzasCommon.InsertarAlerta(dbContext, cobranzas, alertas);
                                }
                                break;
                        }
                    }
                }
            }
        }

        public static void InsertarAlerta(ACHEEntities dbContext, Cobranzas cobranzas, Alertas alertas) {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            AlertasGeneradas entity = new AlertasGeneradas();

            entity.IDAlerta = alertas.IDAlerta;
            entity.IDUsuario = cobranzas.IDUsuario;
            entity.IDPersona = cobranzas.IDPersona;

            entity.ImportePagado = cobranzas.ImporteTotal;
            entity.Visible = true;
            entity.Fecha = DateTime.Now.Date;
            entity.IDCobranzas = cobranzas.IDCobranza;

            entity.NroComprobante = "Se cobraron los Comprobante/s: ";
            foreach (var item in cobranzas.CobranzasDetalle) {
                if (item.IDComprobante.HasValue) {
                    var comprobante = dbContext.Comprobantes.Where(x => x.IDComprobante == item.IDComprobante).FirstOrDefault();
                    entity.NroComprobante += comprobante.Tipo + " " + comprobante.PuntosDeVenta.Punto.ToString("#0000") + "-" + cobranzas.Numero.ToString("#00000000") + " ; ";
                }
                else {
                    entity.NroComprobante += "Cobranza a cuenta" + " ; ";
                }
            }

            entity.NroComprobante = entity.NroComprobante.Substring(0, entity.NroComprobante.Length - 3);
            entity.NroComprobante += ".";

            dbContext.AlertasGeneradas.Add(entity);
            dbContext.SaveChanges();

            var alerta = alertas.AvisoAlerta + " - " + alertas.Condicion + " - $" + alertas.Importe;
            var descripcion = entity.NroComprobante;
            CobranzasCommon.EnviarEmailAlerta(alerta, descripcion);
        }

        public static void EnviarEmailAlerta(string alerta, string descripcion) {
            if (HttpContext.Current.Session["CurrentUser"] != null) {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                var email = (usu.EmailAlerta == "") ? usu.Email : usu.EmailAlerta;

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<ALERTA>", alerta);
                replacements.Add("<USUARIO>", usu.RazonSocial);
                replacements.Add("<DESCRIPCION>", descripcion);
                replacements.Add("<EMAIL>", email);

                bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Alertas, replacements, ConfigurationManager.AppSettings["Email.FromAlertas"], email, "Alerta en CONTABILIUM");

                if (!send)
                    throw new Exception("Comprobante El mensaje no pudo ser enviado. Por favor, escribenos a <a href='mailto:ayuda@contabilium.com'>ayuda@contabilium.com</a>");
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }

        public static List<Combo2ViewModel> ObtenerFormasDePagoCobranzas(int idPersona, int idUsuario) {

            using (var dbContext = new ACHEEntities()) {
                var aux = dbContext.Comprobantes.Where(x => x.IDUsuario == idUsuario && x.IDPersona == idPersona && x.Saldo > 0 && x.Tipo != "COT" && x.Tipo != "FCA" && x.Tipo != "FCB" && x.Tipo != "FCC" && x.Tipo != "NDA" && x.Tipo != "NDB" && x.Tipo != "NDC").ToList()
                         .Select(x => new Combo2ViewModel() {
                             ID = x.IDComprobante,
                             Nombre = x.Tipo + " " + x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000") + " (Saldo: $ " + x.Saldo.ToMoneyFormat(UsuarioCommon.ObtenerCantidadDecimales(idUsuario)) + ")"
                         }).OrderBy(x => x.Nombre).OrderByDescending(x => x.ID).ToList();

                return aux;
            }
        }

        public static List<Combo2ViewModel> ObtenerComprobantesPendientes(int id, int idCobranza, int idUsuario) {
            var list = new List<Combo2ViewModel>();
            using (var dbContext = new ACHEEntities()) {
                List<Comprobantes> lista = new List<Comprobantes>();

                if (idCobranza == 0) {
                    list = dbContext.Comprobantes.Where(x => x.IDUsuario == idUsuario && x.IDPersona == id && x.Saldo > 0 && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE").ToList()
                         .Select(x => new Combo2ViewModel() {
                             ID = x.IDComprobante,
                             Nombre = x.Tipo + " " + x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000") + " (Saldo: $ " + x.Saldo.ToMoneyFormat(2) + ")"
                         }).OrderBy(x => x.Nombre).OrderByDescending(x => x.ID).ToList();
                }
                else {
                    list = dbContext.CobranzasDetalle.Where(x => x.Comprobantes != null && x.Comprobantes.IDUsuario == idUsuario && x.Comprobantes.IDPersona == id && x.IDCobranza == idCobranza && x.Comprobantes.Tipo != "NCA" && x.Comprobantes.Tipo != "NCB" && x.Comprobantes.Tipo != "NCC" && x.Comprobantes.Tipo != "NCM" && x.Comprobantes.Tipo != "NCE").ToList()
                     .Select(x => new Combo2ViewModel() {
                         ID = x.Comprobantes.IDComprobante,
                         Nombre = x.Comprobantes.Tipo + " " + x.Comprobantes.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Comprobantes.Numero.ToString("#00000000") + " (Saldo: $ " + x.Comprobantes.Saldo.ToMoneyFormat(2) + ")"
                     }).ToList();

                    var list2 = dbContext.Comprobantes.Where(x => x.IDUsuario == idUsuario && x.IDPersona == id && x.Saldo > 0 && x.Tipo != "NCA" && x.Tipo != "NCB" && x.Tipo != "NCC" && x.Tipo != "NCM" && x.Tipo != "NCE").ToList()
                         .Select(x => new Combo2ViewModel() {
                             ID = x.IDComprobante,
                             Nombre = x.Tipo + " " + x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000") + " (Saldo: $ " + x.Saldo.ToMoneyFormat(2) + ")"
                         }).OrderBy(x => x.Nombre).OrderByDescending(x => x.ID).ToList();

                    if (list2.Any())
                        list.AddRange(list2);
                }
                list.Add(new Combo2ViewModel { ID = -1, Nombre = "Cobranza a cuenta" });
            }
            return list;


        }

        public static CobranzasEditViewModel ObtenerCobranza(int id, int idUsuario) {

            using (var dbContext = new ACHEEntities()) {
                Cobranzas entity = dbContext.Cobranzas
                    .Include("CobranzasDetalle").Include("CobranzasFormasDePago").Include("CobranzasRetenciones")
                    .Where(x => x.IDCobranza == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null) {
                    CobranzasEditViewModel result = new CobranzasEditViewModel();
                    result.ID = id;
                    result.IDPersona = entity.IDPersona;
                    result.Fecha = entity.FechaCobranza.ToString("dd/MM/yyyy");
                    result.Modo = entity.Modo;
                    result.Tipo = entity.Tipo;
                    result.Numero = entity.Numero.ToString("#00000000");
                    result.IDPuntoVenta = entity.IDPuntoVenta;
                    result.Observaciones = entity.Observaciones;


                    var personas = dbContext.Personas.Where(x => x.IDPersona == entity.IDPersona).FirstOrDefault();
                    PersonasEditViewModel PersonasEdit = new PersonasEditViewModel();
                    PersonasEdit.ID = id;
                    PersonasEdit.RazonSocial = personas.RazonSocial.ToUpper();
                    PersonasEdit.Email = string.IsNullOrEmpty(personas.EmailsEnvioFc) ? personas.Email : personas.EmailsEnvioFc; //personas.Email.ToLower();
                    PersonasEdit.CondicionIva = personas.CondicionIva;
                    PersonasEdit.Domicilio = personas.Domicilio.ToUpper() + " " + personas.PisoDepto;
                    PersonasEdit.Ciudad = personas.Ciudades.Nombre.ToUpper();
                    PersonasEdit.Provincia = personas.Provincias.Nombre;
                    PersonasEdit.TipoDoc = personas.TipoDocumento;
                    PersonasEdit.NroDoc = personas.NroDocumento;
                    result.Personas = PersonasEdit;

                    foreach (var det in entity.CobranzasDetalle) {
                        var tra = new CobranzasDetalleViewModel(UsuarioCommon.ObtenerCantidadDecimales(idUsuario));
                        tra.ID = CobranzaCart.Retrieve().Items.Count() + 1;
                        if (det.Comprobantes != null)
                            tra.Comprobante = det.Comprobantes.Tipo + " " +
                                              det.Comprobantes.PuntosDeVenta.Punto.ToString("#0000") + "-" +
                                              det.Comprobantes.Numero.ToString("#00000000");
                        else
                            tra.Comprobante = "Cobranza a cuenta";
                        tra.Importe = det.Importe;

                        tra.IDComprobante = det.IDComprobante;

                        CobranzaCart.Retrieve().Items.Add(tra);
                    }

                    foreach (var det in entity.CobranzasFormasDePago) {
                        var tra = new CobranzasFormasDePagoViewModel();
                        tra.ID = CobranzaCart.Retrieve().FormasDePago.Count() + 1;
                        tra.FormaDePago = det.FormaDePago;
                        tra.NroReferencia = det.NroReferencia;
                        tra.Importe = det.Importe;
                        tra.IDCheque = det.IDCheque;
                        tra.IDBanco = det.IDBanco;
                        tra.IDCaja = det.IDCaja;
                        tra.IDNotaCredito = det.IDNotaCredito;

                        CobranzaCart.Retrieve().FormasDePago.Add(tra);
                    }

                    foreach (var det in entity.CobranzasRetenciones) {
                        var tra = new CobranzasRetencionesViewModel();
                        tra.ID = CobranzaCart.Retrieve().Retenciones.Count() + 1;
                        tra.Tipo = det.Tipo;
                        tra.NroReferencia = det.NroReferencia;
                        tra.Importe = det.Importe;
                        tra.IDJurisdiccion = det.IDJurisdiccion ?? 0;
                        tra.Fecha = det.FechaRetencion.HasValue ? det.FechaRetencion.Value.ToString("dd/MM/yyyy") : "";
                        CobranzaCart.Retrieve().Retenciones.Add(tra);
                    }

                    return result;
                }
                else
                    throw new Exception("Error al obtener los datos");
            }
        }
    }


}
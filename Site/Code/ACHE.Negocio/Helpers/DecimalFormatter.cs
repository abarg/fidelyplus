﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Negocio.Helpers
{
    public static class DecimalFormatter
    {
        public static Byte GetCantidadDecimales(int IDUsuario)
        {
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(IDUsuario);
            return cantidadDecimales;
        }

        public static String GetDecimalStringFormat(int IDUsuario)
        {
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(IDUsuario);
            return String.Format("N{0}", cantidadDecimales);
        }
    }
}

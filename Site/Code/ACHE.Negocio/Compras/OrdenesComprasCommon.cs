﻿using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ACHE.Negocio.Facturacion {
    public static class OrdenesComprasCommon {
        public static bool EliminarOrden(int id, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.Compras.Any(x => x.IDOrdenCompra == id))
                        throw new CustomException("No se puede eliminar por tener compras asociados");


                    var entity = dbContext.OrdenesCompras.Where(x => x.IDOrdenCompra == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null) {
                        if (entity.OrdenesComprasDetalle.Any())
                            dbContext.OrdenesComprasDetalle.RemoveRange(entity.OrdenesComprasDetalle.ToList());

                        dbContext.OrdenesCompras.Remove(entity);
                        dbContext.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }

        }

        public static ResultadosOrdenesViewModel ObtenerOrdenes(string idPersona, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario, string Numero) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var results = dbContext.OrdenesCompras.Include("Personas").Where(x => x.IDUsuario == idUsuario).AsQueryable();


                    if (idPersona != "") {
                        var idproveedor = int.Parse(idPersona);
                        results = results.Where(x => x.IDProveedor == idproveedor);
                    }

                    switch (periodo) {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(fechaDesde)) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta)) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.Fecha <= dtHasta);
                    }

                    Int32 numero = 0;
                    if (Int32.TryParse(Numero, out numero)) {
                        results = results.Where(x => x.NroOrden.ToString().Contains(Numero));
                    }
                    else if (Numero != string.Empty) {
                        results = results.Where(x => x.Personas.RazonSocial.Contains(Numero) || x.Personas.NombreFantansia.Contains(Numero));
                    }

                    //Determinamos el formato decimal
                    var decimalFormat = DecimalFormatter.GetDecimalStringFormat(idUsuario);

                    page--;
                    ResultadosOrdenesViewModel resultado = new ResultadosOrdenesViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new OrdenesComprasViewModel() {
                            ID = x.IDOrdenCompra,
                            Proveedor = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                            Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                            FechaEntrega = (x.FechaEntrega.HasValue) ? x.FechaEntrega.Value.ToString("dd/MM/yyyy") : "",
                            Area = x.Area,

                            Solicitante = x.Solicitante,
                            NumeroOrden = x.NroOrden.ToString(),
                            Total = x.Total.ToString(decimalFormat),
                            TotalNeto = x.TotalNeto.ToString(decimalFormat),
                            Estado = x.Estado
                        });
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static OrdenesCompras Guardar(int idUsuario, int id, int idPersona, string nroOrden, string area,
        string solicitante, string fecha, string fechaEntrega, string obs, List<DetalleViewModel> items, string estado) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    int nro = int.Parse(nroOrden);
                    if (dbContext.OrdenesCompras.Any(x => x.IDOrdenCompra != id && x.NroOrden == nro && x.IDUsuario == idUsuario))
                        throw new Exception("Ya existe una orden de compra con el nro: " + nroOrden);

                    OrdenesCompras entity;
                    if (id > 0) {
                        entity = dbContext.OrdenesCompras.Where(x => x.IDOrdenCompra == id).FirstOrDefault();
                        if (entity.OrdenesComprasDetalle.Any()) {
                            var detalles = entity.OrdenesComprasDetalle.ToList();
                            dbContext.OrdenesComprasDetalle.RemoveRange(detalles);
                        }
                    }
                    else
                        entity = new OrdenesCompras();
                    entity.IDUsuario = idUsuario;
                    entity.IDProveedor = idPersona;
                    entity.NroOrden = nro;
                    entity.Area = area;
                    entity.Solicitante = solicitante;
                    entity.Fecha = DateTime.Parse(fecha);
                    entity.Estado = estado;
                    if (fechaEntrega != "")
                        entity.FechaEntrega = DateTime.Parse(fechaEntrega);
                    else
                        entity.FechaEntrega = null;
                    entity.Observaciones = obs;
                    decimal total = 0;
                    decimal totalNeto = 0;
                    if (items != null) {
                        List<OrdenesComprasDetalle> list = new List<OrdenesComprasDetalle>();
                        foreach (var item in items) {
                            OrdenesComprasDetalle aux = new OrdenesComprasDetalle();
                            aux.Cantidad = item.Cantidad;
                            if (item.IDConcepto.HasValue && item.IDConcepto.Value > 0)//LH: Fix que viene de JS
                                aux.IDConcepto = item.IDConcepto;
                            aux.Precio = item.PrecioUnitario;
                            aux.Descripcion = item.Concepto;
                            aux.Iva = item.Iva;

                            aux.Bonificacion = item.Bonificacion;
                            total += item.TotalSinIva;
                            totalNeto += item.TotalConIva;
                            list.Add(aux);
                        }
                        entity.Total = total;
                        entity.TotalNeto = totalNeto;
                        entity.OrdenesComprasDetalle = list;
                    }
                    if (id == 0)
                        dbContext.OrdenesCompras.Add(entity);
                    dbContext.SaveChanges();
                    items.Clear();
                    return entity;
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static string ExportarOrdenesCompras(string idPersona, string periodo, string fechaDesde, string fechaHasta, string Numero, int idUsuario) {

            string fileName = "Ordenes_" + idUsuario + "_";
            string path = "~/tmp/";
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {

                    var results = dbContext.OrdenesCompras.Include("Personas").Where(x => x.IDUsuario == idUsuario).AsQueryable();

                    if (idPersona != "") {
                        var idproveedor = int.Parse(idPersona);
                        results = results.Where(x => x.IDProveedor == idproveedor);
                    }
                    Int32 numero = 0;
                    if (Int32.TryParse(Numero, out numero)) {
                        results = results.Where(x => x.NroOrden.ToString().Contains(Numero));
                    }
                    else if (Numero != string.Empty) {
                        results = results.Where(x => x.Personas.RazonSocial.Contains(Numero) || x.Personas.NombreFantansia.Contains(Numero));
                    }


                    switch (periodo) {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(fechaDesde)) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta)) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.Fecha <= dtHasta);
                    }


                    dt = results.OrderBy(x => x.Fecha).ToList().Select(x => new {
                        NumeroOrden = x.NroOrden.ToString(),

                        Proveedor = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),

                        Area = x.Area,

                        Solicitante = x.Solicitante,
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        FechaEntrega = (x.FechaEntrega.HasValue) ? x.FechaEntrega.Value.ToString("dd/MM/yyyy") : "",
                        ImorteNeto = x.TotalNeto,
                        Total = x.TotalNeto,
                        Estado = x.Estado
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        public static void AgregarItem(int id, int? idConcepto, string concepto, decimal iva, decimal precio, decimal bonif, decimal cantidad, string codigo, byte cantDecimales) {
            int index = id;
            if (id != 0) {
                var aux = ComprobanteCart.Retrieve().Items.Where(x => x.ID == id).FirstOrDefault();
                ComprobanteCart.Retrieve().Items.Remove(aux);
            }
            else
                index = ComprobanteCart.Retrieve().Items.Count() + 1;

            var tra = new ComprobantesDetalleViewModel(cantDecimales);
            tra.ID = index;// ComprobanteCart.Retrieve().Items.Count() + 1;
            tra.Concepto = concepto;
            tra.Iva = iva;
            tra.Bonificacion = bonif;
            tra.PrecioUnitario = precio;
            tra.Cantidad = cantidad;
            tra.IDConcepto = idConcepto;

            if (id != 0)
                ComprobanteCart.Retrieve().Items.Insert(id - 1, tra);
            else
                ComprobanteCart.Retrieve().Items.Add(tra);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.Negocio;
using ACHE.Negocio.Banco;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Helpers;
using System.Data;
using System.Configuration;
using System.Web;
using System.IO;

namespace ACHE.Negocio.Facturacion
{
    public class PagosCommon
    {
        public const int BANCO_MP = 80;

        public static Pagos Guardar(ACHEEntities dbContext, PagosCartDto PagosCart, WebUser usu, bool sobranteACuenta)
        {
            Personas persona = dbContext.Personas.Where(x => x.IDPersona == PagosCart.IDPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
            if (persona == null)
                throw new CustomException("El cliente/proveedor es inexistente");

            if (!PagosCart.FormasDePago.Any())
                throw new CustomException("Ingrese una forma de pago");

            if (!PagosCart.Items.Any())
                throw new CustomException("Ingrese un comprobante a pagar");

            Pagos entity;
            if (PagosCart.IDPago > 0)
            {
                entity = dbContext.Pagos
                    .Include("PagosDetalle").Include("PagosFormasDePago").Include("PagosRetenciones")
                    .Where(x => x.IDPago == PagosCart.IDPago && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                entity.UsuarioModifica = usu.Email;
            }
            else
            {
                entity = new Pagos();
                entity.FechaAlta = DateTime.Now;
                entity.IDUsuario = usu.IDUsuario;
                entity.UsuarioAlta = usu.Email;
            }
            entity.NroPago = PagosCart.NroPago;

            if (entity.NroPago == 0)
            {
                if (dbContext.Pagos.Any(x => x.IDUsuario == usu.IDUsuario))
                {
                    var aux = dbContext.Pagos.Where(x => x.IDUsuario == usu.IDUsuario).Max(x => x.NroPago);
                    if (aux != null)
                        entity.NroPago = aux + 1;
                    else
                        entity.NroPago = 1;
                }
                else
                    entity.NroPago = 1;
            }

            entity.ImporteTotal = 0;
            entity.Observaciones = PagosCart.Observaciones;
            entity.IDPersona = PagosCart.IDPersona;
            entity.FechaPago = Convert.ToDateTime(PagosCart.FechaPago);

            #region Detalle, Formas y Retenciones

            //////////////PagosFormasDePago
            if (entity.PagosFormasDePago.Any())
            {
                foreach (var itemCheque in entity.PagosFormasDePago.Where(x => x.IDCheque.HasValue))
                    itemCheque.Cheques.Estado = "Libre";

                dbContext.PagosFormasDePago.RemoveRange(entity.PagosFormasDePago);
            }
            var tieneNC = false;
            decimal totalFormasDePago = 0;
            foreach (var item in PagosCart.FormasDePago.ToList())
            {
                PagosFormasDePago formas = new PagosFormasDePago();
                formas.FormaDePago = item.FormaDePago;
                formas.Importe = item.Importe;
                formas.NroReferencia = item.NroReferencia;
                formas.IDCheque = item.IDCheque;
                formas.IDCaja = item.IDCaja;
                formas.IDNotaCredito = item.IDNotaCredito;
                if (item.FormaDePago != "Efectivo")
                {
                    formas.IDBanco = item.IDBanco;
                    if (formas.FormaDePago == "Mercado Pago" && !formas.IDBanco.HasValue)
                    {
                        int idBancoMP = 0;
                        var bancoMP = dbContext.Bancos.Where(x => x.IDUsuario == usu.IDUsuario && x.IDBancoBase == BANCO_MP).FirstOrDefault();

                        if (bancoMP == null)
                            idBancoMP = BancosCommon.GuardarBanco(dbContext, 0, BANCO_MP, "", "Pesos Argentinos", 1, "0", "", "", "", "", "", usu.IDUsuario);
                        else
                            idBancoMP = bancoMP.IDBanco;

                        formas.IDBanco = idBancoMP;
                    }
                }
                else
                    formas.IDBanco = null;

                entity.PagosFormasDePago.Add(formas);

                if (item.IDCheque.HasValue)
                {
                    var cheques = dbContext.Cheques.Where(x => x.IDCheque == item.IDCheque).FirstOrDefault();
                    if (cheques != null)
                        cheques.Estado = "Usado";
                }

                totalFormasDePago += item.Importe;

                if (formas.IDNotaCredito != null)
                    tieneNC = true;
            }
            //////////////PagosRetenciones
            if (entity.PagosRetenciones.Any())
                dbContext.PagosRetenciones.RemoveRange(entity.PagosRetenciones);

            decimal total = 0;

            foreach (var item in PagosCart.Retenciones.ToList())
            {
                PagosRetenciones restriccciones = new PagosRetenciones();
                restriccciones.Tipo = item.Tipo;
                restriccciones.NroReferencia = item.NroReferencia;
                restriccciones.Importe = item.Importe;
                restriccciones.CodigoRegimen = item.CodigoRegimen;
                if (item.IDJurisdiccion > 0)
                    restriccciones.IDJurisdiccion = item.IDJurisdiccion;
                if (string.IsNullOrEmpty(item.Fecha))
                    restriccciones.FechaRetencion = null;
                else
                    restriccciones.FechaRetencion = DateTime.Parse(item.Fecha);

                totalFormasDePago += item.Importe;

                entity.PagosRetenciones.Add(restriccciones);
            }
            //////////////PagosDetalle
            List<int> listCompras = new List<int>();
            if (entity.PagosDetalle.Any())
            {
                foreach (var item in entity.PagosDetalle)
                {
                    if (item.IDCompra.HasValue)
                    {
                        if (!listCompras.Any(x => x == item.IDCompra.Value))
                            listCompras.Add(item.IDCompra.Value);
                    }
                }

                dbContext.PagosDetalle.RemoveRange(entity.PagosDetalle);
            }


            foreach (var item in PagosCart.Items)
            {
                total += item.Importe;

                PagosDetalle pagoDetalle = new PagosDetalle();
                pagoDetalle.Importe = item.Importe;
                pagoDetalle.IDCompra = item.IDCompra;
                entity.PagosDetalle.Add(pagoDetalle);
            }
            entity.ImporteTotal = total;

            if (total == 0 && !tieneNC)
                throw new CustomException("No puede ingresar un pago con Importe 0");

            if (totalFormasDePago != total && !tieneNC)
            {
                var decimalFormat = DecimalFormatter.GetDecimalStringFormat(usu.IDUsuario);
                if (sobranteACuenta)
                    entity.PagosDetalle.Add(new PagosDetalle { IDPago = entity.IDPago, IDCompra = null, Importe = (totalFormasDePago - total) });
                else
                    throw new CustomException("Las formas de pago deben coincidir. Hay una diferencia de " +
                                          (total - totalFormasDePago).ToString(decimalFormat));
            }


            #endregion

            if (PagosCart.IDPago > 0)
                dbContext.SaveChanges();
            else
            {
                dbContext.Pagos.Add(entity);
                dbContext.SaveChanges();
            }

            //foreach (var item in entity.PagosDetalle)
            dbContext.ActualizarSaldosPorPago(entity.IDPago);
            foreach (var item in listCompras)
                dbContext.ActualizarSaldosPorCompra(item);

            return entity;
        }

        public static Pagos Guardar(PagosCartDto PagosCart, WebUser usu, bool sobranteAcuenta)
        {
            using (var dbContext = new ACHEEntities())
            {
                return Guardar(dbContext, PagosCart, usu, sobranteAcuenta);
            }
        }

        public static bool EliminarPago(int id, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.Pagos.Include("PagosDetalle").Include("PagosFormasDePago").Include("PagosRetenciones")
                    .Where(x => x.IDPago == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null)
                {
                    int idPersona = entity.IDPersona;
                    if (ContabilidadCommon.ValidarCierreContable(idUsuario, entity.FechaPago))
                        throw new CustomException("El comprobante no puede eliminarse ya que el año contable ya fue cerrado.");

                    //var list = entity.PagosDetalle.Select(x => new { IDCompra = x.IDCompra }).ToList();
                    List<int> listCompras = new List<int>();
                    List<int> listNc = new List<int>();

                    foreach (var item in entity.PagosDetalle)
                    {
                        if (item.IDCompra.HasValue)
                        {
                            if (!listCompras.Any(x => x == item.IDCompra.Value))
                                listCompras.Add(item.IDCompra.Value);
                        }
                    }

                    foreach (var forma in entity.PagosFormasDePago)
                    {
                        if (forma.IDCheque.HasValue)
                        {
                            var cheque = dbContext.Cheques.Where(x => x.IDCheque == forma.IDCheque.Value).FirstOrDefault();
                            if (cheque != null)
                            {
                                cheque.Estado = "Libre";
                            }
                        }
                        else if (forma.IDNotaCredito.HasValue)
                        {
                            if (!listNc.Any(x => x == forma.IDNotaCredito.Value))
                                listNc.Add(forma.IDNotaCredito.Value);
                        }
                    }

                    dbContext.Database.ExecuteSqlCommand("DELETE Asientos WHERE IDPago=" + id);

                    dbContext.Pagos.Remove(entity);
                    dbContext.SaveChanges();

                    foreach (var item in listCompras)
                        dbContext.ActualizarSaldosPorCompra(item);

                    foreach (var item in listNc)
                        dbContext.ActualizarSaldosPorCompra(item);

                    return true;
                }
                else
                    return false;
            }
        }

        public static ResultadosPagosViewModel ObtenerPagos(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.Pagos.Include("Personas").Include("PagosDetalle").Where(x => x.IDUsuario == idUsuario).AsQueryable();
                Int32 numero = 0;
                if (Int32.TryParse(condicion, out numero))
                    results = results.Where(x => x.PagosDetalle.Any(y => y.Compras != null && y.Compras.NroFactura.Contains(condicion)) || x.NroPago == numero);
                else if (!string.IsNullOrWhiteSpace(condicion))
                    results = results.Where(x => x.Personas.RazonSocial.ToLower().Contains(condicion.ToLower()) || x.Personas.NombreFantansia.ToLower().Contains(condicion.ToLower()));

                switch (periodo)
                {
                    case "30":
                        fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                        break;
                    case "15":
                        fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                        break;
                    case "7":
                        fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                        break;
                    case "1":
                        fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                        break;
                    case "0":
                        fechaDesde = DateTime.Now.ToShortDateString();
                        break;
                }

                if (!string.IsNullOrWhiteSpace(fechaDesde))
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.FechaPago >= dtDesde);
                }
                if (!string.IsNullOrWhiteSpace(fechaHasta))
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    results = results.Where(x => x.FechaPago <= dtHasta);
                }

                //Determinamos el formato decimal
                var decimalFormat = "N2";//DecimalFormatter.GetDecimalStringFormat(idUsuario);

                page--;
                ResultadosPagosViewModel resultado = new ResultadosPagosViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.FechaPago).ThenBy(x => x.NroPago).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new PagosViewModel()
                    {
                        NroPago = "0001-" + x.NroPago.ToString("#00000000"),
                        ID = x.IDPago,
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        Fecha = x.FechaPago.ToString("dd/MM/yyyy"),
                        Iva = (x.PagosDetalle.Where(y => y.Compras != null).Sum(y => y.Compras.Iva)).ToString(decimalFormat),
                        NoGravado = (x.PagosDetalle.Where(y => y.Compras != null).Sum(y => y.Compras.NoGravado) + x.PagosDetalle.Where(y => y.Compras != null).Sum(y => y.Compras.Exento)).ToString(decimalFormat),
                        Retenciones = Convert.ToDecimal(x.PagosDetalle.Where(y => y.Compras != null).Sum(y => y.Compras.TotalImpuestos)).ToString(decimalFormat),
                        ImporteNeto = Convert.ToDecimal(x.ImporteTotal).ToString(decimalFormat),
                        Total = (x.ImporteTotal).ToString(decimalFormat)
                    });
                resultado.Items = list.ToList();
                return resultado;
            }
        }

        public static ResultadosPagosViewModel ObtenerPagosACuenta(int idPersona, int page, int pageSize, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results =
                    dbContext.PagosDetalle.Include("Pagos").Include("Pagos.Personas")
                        .Where(x => x.Pagos.IDUsuario == idUsuario && !x.IDCompra.HasValue)
                        .AsQueryable();

                if (idPersona > 0)
                    results = results.Where(x => x.Pagos.IDPersona == idPersona);

                //Determinamos el formato decimal
                var decimalFormat = "N2";// DecimalFormatter.GetDecimalStringFormat(idUsuario);

                page--;
                ResultadosPagosViewModel resultado = new ResultadosPagosViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.Pagos.FechaPago).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new PagosViewModel()
                    {
                        ID = x.IDPagosDetalle,
                        NroPago = "0001-" + x.Pagos.NroPago.ToString("#00000000"),

                        RazonSocial =
                            (x.Pagos.Personas.NombreFantansia == ""
                                ? x.Pagos.Personas.RazonSocial.ToUpper()
                                : x.Pagos.Personas.NombreFantansia.ToUpper()),
                        Fecha = x.Pagos.FechaPago.ToString("dd/MM/yyyy"),
                        Iva = 0.ToString(decimalFormat),
                        NoGravado = 0.ToString(decimalFormat),
                        Retenciones = 0.ToString(decimalFormat),
                        ImporteNeto = x.Importe.ToString(decimalFormat),
                        Total = x.Importe.ToString(decimalFormat)
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }

        public static void AplicarPagosACuenta(int idPersona, Dictionary<int, List<int>> pagosAAplicar)
        {
            using (var dbContext = new ACHEEntities())
            {
                var compras = dbContext.Compras.Where(x => pagosAAplicar.Keys.Contains(x.IDCompra)).ToList();
                var pagosDetallesIds = pagosAAplicar.Values.SelectMany(l => l).ToList();
                var pagosDetalles =
                    dbContext.PagosDetalle.Where(pd => pagosDetallesIds.Contains(pd.IDPagosDetalle))
                        .ToDictionary(c => c.IDPagosDetalle, c => c);

                //foreach (var compra in compras)
                //{
                //    var total = pagosAAplicar[compra.IDCompra].Sum(pdId => pagosDetalles[pdId].Importe);
                //    if (total > compra.Saldo)
                //        throw new CustomException(String.Format("Se superó el saldo de la factura {0}.",
                //            compra.NroFactura));
                //}

                foreach (var compra in compras)
                {
                    var montoCompra = compra.Saldo;
                    foreach (var pagoDetalleId in pagosAAplicar[compra.IDCompra])
                    {
                        if (montoCompra > 0)
                            if (pagosDetalles[pagoDetalleId].Importe <= montoCompra)
                            {
                                pagosDetalles[pagoDetalleId].IDCompra = compra.IDCompra;
                                montoCompra -= pagosDetalles[pagoDetalleId].Importe;
                            }
                            else
                            {
                                var pagoDetalle = pagosDetalles[pagoDetalleId];
                                dbContext.PagosDetalle.Add(new PagosDetalle
                                {
                                    Importe = pagoDetalle.Importe - montoCompra,
                                    IDPago = pagoDetalle.IDPago
                                });
                                pagoDetalle.Importe = montoCompra;
                                pagoDetalle.IDCompra = compra.IDCompra;
                                montoCompra = 0;
                            }
                    }
                }
                dbContext.SaveChanges();

                foreach (var compra in compras)
                {
                    dbContext.ActualizarSaldosPorCompra(compra.IDCompra);
                }
            }
        }

        public static void Eliminar(int idCompra)
        {
            using (var dbContext = new ACHEEntities())
            {
                dbContext.Database.ExecuteSqlCommand("delete PagosDetalle where IDCompra=" + idCompra);
            }
        }

        public static string ObtenerACuenta(int idUsuario) {
            var html = "";

            using (var dbContext = new ACHEEntities()) {
                var pagos = dbContext.PagosDetalle.Where(x => !x.IDCompra.HasValue && x.Pagos.IDUsuario == idUsuario).OrderBy(x => x.Pagos.FechaPago).ToList();
                foreach (var det in pagos.GroupBy(x => x.IDPago)) {
                    html += "<tr><td>" + det.First().Pagos.FechaPago.ToString("dd/MM/yyyy") + "</td>";
                    html += "<td>" + "0001-" + det.First().Pagos.NroPago.ToString("#00000000") + "</td>";
                    html += "<td>" + (det.First().Pagos.Personas.NombreFantansia == "" ? det.First().Pagos.Personas.RazonSocial.ToUpper() : det.First().Pagos.Personas.NombreFantansia.ToUpper()) + "</td>";
                    html += "<td>" + det.Sum(x => x.Importe).ToString("N2") + "</td></tr>";
                }
            }

            return html;
        }

        public static string ExportarPagos(string condicion, string periodo, string fechaDesde, string fechaHasta,int idUsuario) {
            string fileName = "Pagos_" + idUsuario + "_";
            string path = "~/tmp/";
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var results = dbContext.Pagos.Include("Personas").Include("PagosDetalle").Where(x => x.IDUsuario == idUsuario).AsQueryable();

                    Int32 numero = 0;
                    if (Int32.TryParse(condicion, out numero)) {
                        results = results.Where(x => x.PagosDetalle.Any(y => y.Compras.NroFactura.Contains(condicion)) || x.NroPago == numero);
                    }
                    else if (condicion != string.Empty) {
                        results = results.Where(x => x.Personas.RazonSocial.Contains(condicion) || x.Personas.NombreFantansia.Contains(condicion));
                    }

                    switch (periodo) {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (fechaDesde != string.Empty) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaPago >= dtDesde);
                    }
                    if (fechaHasta != string.Empty) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaPago <= dtHasta);
                    }

                    dt = results.OrderBy(x => x.FechaPago).ToList().Select(x => new {
                        // = x.IDPago,
                        NroPago = "0001-" + x.NroPago.ToString("#00000000"),
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        Fecha = x.FechaPago.ToString("dd/MM/yyyy"),
                        ImporteNeto = x.ImporteTotal,
                        //Iva = x.PagosDetalle.Sum(y => y.Compras.Iva),//.ToMoneyFormat(),
                        NoGravado = (x.PagosDetalle.Where(y => y.IDCompra.HasValue).Sum(y => y.Compras.NoGravado + y.Compras.Exento)),//.ToMoneyFormat(),
                        Retenciones = x.PagosDetalle.Where(y => y.IDCompra.HasValue).Sum(y => y.Compras.TotalImpuestos.Value),
                        Total = x.ImporteTotal
                    }).ToList().ToDataTable();

                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }
    }
}
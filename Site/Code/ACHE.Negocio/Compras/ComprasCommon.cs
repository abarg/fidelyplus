﻿using ACHE.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using ACHE.Model.Negocio;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Contabilidad;
using ACHE.Negocio.Productos;
using ACHE.Negocio.Helpers;
using ACHE.Negocio.Inventario;
using System.Web;
using System.IO;
using System.Data;
using System.Configuration;

namespace ACHE.Negocio.Facturacion {
    public static class ComprasCommon {
        public static Compras Guardar(int id, int idPersona, string fecha, string nroFactura,
        string iva, string importe2, string importe5, string importe10, string importe21, string importe27, string noGrav, string importeMon,
        string impNacional, string impMunicipal, string impInterno, string percepcionIva, string otros,
        string obs, string tipo, string idCategoria, string rubro, string exento, string FechaEmision,
            int idPlanDeCuenta, int idUsuario, List<JurisdiccionesViewModel> Jurisdicciones, string fechaPrimerVencimiento,
            string fechaSegundoVencimiento, bool detallado, List<DetalleViewModel> items, int idInventario, int IdOrden, string email) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var usu = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    if (tipo != "COT" && ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, Convert.ToDateTime(fecha)))
                        throw new CustomException("No puede agregar ni modificar un comprobante con fecha anterior a la fecha límite de carga.");
                    bool CompraExistente = false;
                    Compras entity;
                    if (id > 0) {
                        entity = dbContext.Compras.Where(x => x.IDCompra == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (entity != null) {
                            InventariosCommon.RestaurarStockCompra(dbContext, idUsuario, entity.Tipo, entity.IDCompra, entity.IDPersona, items, entity.ComprasDetalle.ToList(), entity.IDInventario ?? 0);
                        }

                        entity.UsuarioModifica = email;
                    }
                    else {
                        entity = new Compras();
                        entity.FechaAlta = DateTime.Now;
                        entity.IDUsuario = usu.IDUsuario;
                        entity.UsuarioAlta = email;
                    }

                    CompraExistente = dbContext.Compras.Any(x => x.IDPersona == idPersona && x.Tipo == tipo && x.NroFactura == nroFactura && x.IDUsuario == usu.IDUsuario && x.IDCompra != id);

                    if (CompraExistente)
                        throw new Exception("Ya existe la factura " + nroFactura + " para el proveedor/cliente seleccionado");

                    Personas persona = dbContext.Personas.Where(x => x.IDPersona == idPersona && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (persona == null)
                        throw new Exception("El cliente/proveedor es inexistente");

                    if (nroFactura.StartsWith("0000"))
                        throw new Exception("El punto de venta no puede ser 0");

                    entity.IDPersona = idPersona;
                    entity.Tipo = tipo;

                    entity.Fecha = DateTime.Parse(fecha);
                    entity.FechaEmision = DateTime.Parse(FechaEmision);

                    if (entity.Fecha.Date != DateTime.Parse(fecha).Date)//para que agregue la hora si es una edicion/alta
                        entity.Fecha = DateTime.Parse(fecha).Date.Add(DateTime.Now.TimeOfDay);
                    else
                        entity.Fecha = DateTime.Parse(fecha).Date.Add(entity.FechaAlta.TimeOfDay);

                    if (entity.FechaEmision.Date != DateTime.Parse(FechaEmision).Date)//para que agregue la hora si es una edicion/alta
                        entity.FechaEmision = DateTime.Parse(FechaEmision).Date.Add(DateTime.Now.TimeOfDay);
                    else
                        entity.FechaEmision = DateTime.Parse(FechaEmision).Date.Add(entity.FechaAlta.TimeOfDay);


                    if (!string.IsNullOrWhiteSpace(fechaPrimerVencimiento))
                        entity.FechaPrimerVencimiento = DateTime.Parse(fechaPrimerVencimiento);
                    else
                        entity.FechaPrimerVencimiento = DateTime.Parse(FechaEmision);
                    if (!string.IsNullOrWhiteSpace(fechaSegundoVencimiento))
                        entity.FechaSegundoVencimiento = DateTime.Parse(fechaSegundoVencimiento);
                    else
                        entity.FechaSegundoVencimiento = null;
                    entity.Detallado = detallado;

                    entity.NroFactura = nroFactura;
                    if (idInventario > 0)
                        entity.IDInventario = idInventario;
                    else
                        entity.IDInventario = null;

                    entity.Iva = iva != string.Empty ? decimal.Parse(iva.Replace(".", ",")) : 0;

                    //TODO DECIMALES: SE REDONDEA A 2 DECIMALES
                    entity.Iva = Math.Round(entity.Iva, 2);

                    if (!entity.Detallado) {
                        entity.Importe2 = importe2 != string.Empty ? decimal.Parse(importe2.Replace(".", ",")) : 0;
                        entity.Importe5 = importe5 != string.Empty ? decimal.Parse(importe5.Replace(".", ",")) : 0;
                        entity.Importe10 = importe10 != string.Empty ? decimal.Parse(importe10.Replace(".", ",")) : 0;
                        entity.Importe21 = importe21 != string.Empty ? decimal.Parse(importe21.Replace(".", ",")) : 0;
                        entity.Importe27 = importe27 != string.Empty ? decimal.Parse(importe27.Replace(".", ",")) : 0;
                        entity.NoGravado = noGrav != string.Empty ? decimal.Parse(noGrav.Replace(".", ",")) : 0;
                        entity.ImporteMon = importeMon != string.Empty ? decimal.Parse(importeMon.Replace(".", ",")) : 0;
                        entity.Exento = (exento != string.Empty) ? decimal.Parse(exento.Replace(".", ",")) : 0;

                        //TODO DECIMALES: SE REDONDEA A 2 DECIMALES
                        entity.Importe2 = Math.Round(entity.Importe2, 2);
                        entity.Importe5 = Math.Round(entity.Importe5, 2);
                        entity.Importe10 = Math.Round(entity.Importe10, 2);
                        entity.Importe21 = Math.Round(entity.Importe21, 2);
                        entity.Importe27 = Math.Round(entity.Importe27, 2);
                        entity.NoGravado = Math.Round(entity.NoGravado, 2);
                        entity.ImporteMon = Math.Round(entity.ImporteMon, 2);
                        entity.Exento = Math.Round(entity.Exento, 2);
                    }
                    else {
                        entity.Importe2 = 0;
                        entity.Importe5 = 0;
                        entity.Importe10 = 0;
                        entity.Importe21 = 0;
                        entity.Importe27 = 0;
                        entity.NoGravado = 0;
                        entity.ImporteMon = 0;
                        entity.Exento = 0;
                    }


                    //IMPUESTOS
                    entity.ImpNacional = impNacional != string.Empty ? decimal.Parse(impNacional.Replace(".", ",")) : 0;
                    entity.ImpMunicipal = impMunicipal != string.Empty ? decimal.Parse(impMunicipal.Replace(".", ",")) : 0;
                    entity.ImpInterno = impInterno != string.Empty ? decimal.Parse(impInterno.Replace(".", ",")) : 0;
                    entity.PercepcionIVA = percepcionIva != string.Empty ? decimal.Parse(percepcionIva.Replace(".", ",")) : 0;
                    entity.Otros = otros != string.Empty ? decimal.Parse(otros.Replace(".", ",")) : 0;


                    //TODO DECIMALES: SE REDONDEA A 2 DECIMALES
                    entity.ImpNacional = Math.Round(entity.ImpNacional, 2);
                    entity.ImpMunicipal = Math.Round(entity.ImpMunicipal, 2);
                    entity.ImpInterno = Math.Round(entity.ImpInterno, 2);
                    entity.PercepcionIVA = Math.Round(entity.PercepcionIVA, 2);
                    entity.IIBB = Math.Round(entity.IIBB, 2);
                    entity.Otros = Math.Round(entity.Otros, 2);


                    if (Jurisdicciones != null) {
                        if (id > 0) {
                            var jurisdiccion = dbContext.Jurisdicciones.Where(x => x.IDCompra == id).ToList();
                            foreach (var item in jurisdiccion)
                                dbContext.Jurisdicciones.Remove(item);
                        }
                        foreach (var item in Jurisdicciones) {
                            entity.Jurisdicciones.Add(new Jurisdicciones() {
                                Compras = entity,
                                IDProvincia = item.IDJurisdicion,
                                Importe = item.Importe
                            });
                        }
                    }

                    List<int> itemsAActualizar = new List<int>();

                    if (items != null) {
                        foreach (var i in items) {
                            ComprasDetalle ad = new ComprasDetalle();

                            if (id > 0)
                                ad.IDCompra = id;
                            else
                                ad.IDCompra = i.IDContenedor;
                            ad.PrecioUnitario = i.PrecioUnitario;
                            ad.Iva = i.Iva;
                            ad.Concepto = i.Concepto;
                            ad.Cantidad = i.Cantidad;
                            ad.Bonificacion = i.Bonificacion;
                            if (i.IDPlanDeCuenta > 0)
                                ad.IDPlanDeCuenta = i.IDPlanDeCuenta;

                            if (i.IDConcepto != null && i.IDConcepto != 0)
                                ad.IDConcepto = i.IDConcepto;

                            switch (i.Iva.ToString("#0.00")) {
                                case "2,50":
                                    entity.Importe2 += i.TotalSinIva;
                                    break;
                                case "5,00":
                                    entity.Importe5 += i.TotalSinIva;
                                    break;
                                case "10,50":
                                    entity.Importe10 += i.TotalSinIva;
                                    break;
                                case "21,00":
                                    entity.Importe21 += i.TotalSinIva;
                                    break;
                                case "27,00":
                                    entity.Importe27 += i.TotalSinIva;
                                    break;
                                case "0,00":
                                    entity.NoGravado += i.TotalSinIva;
                                    break;

                            }

                            //TODO DECIMALES: SE REDONDEA A 2 DECIMALES
                            entity.Importe2 = Math.Round(entity.Importe2, 2);
                            entity.Importe5 = Math.Round(entity.Importe5, 2);
                            entity.Importe10 = Math.Round(entity.Importe10, 2);
                            entity.Importe21 = Math.Round(entity.Importe21, 2);
                            entity.Importe27 = Math.Round(entity.Importe27, 2);
                            entity.NoGravado = Math.Round(entity.NoGravado, 2);
                            entity.ImporteMon = Math.Round(entity.ImporteMon, 2);
                            entity.Exento = Math.Round(entity.Exento, 2);


                            dbContext.ComprasDetalle.Add(ad);

                            var auxList = dbContext.ComboConceptos.Where(x => x.IDConceptoItem == i.IDConcepto && x.Conceptos.Estado == "A").Select(x => x.IDConceptoCombo).Distinct().ToList();
                            foreach (var itemAux in auxList)
                            {
                                if (!itemsAActualizar.Any(x => x == itemAux))
                                    itemsAActualizar.Add(itemAux);
                            }
                        }

                    }
                    if (IdOrden > 0)
                        entity.IDOrdenCompra = IdOrden;
                    else
                        entity.IDOrdenCompra = null;

                    if (entity.Jurisdicciones != null)
                        entity.IIBB = (entity.Jurisdicciones.Count > 0) ? entity.Jurisdicciones.Sum(x => x.Importe) : 0;

                    // cta contables
                    if (idPlanDeCuenta > 0)
                        entity.IDPlanDeCuenta = idPlanDeCuenta;

                    entity.Observaciones = obs;

                    if (idCategoria != "" && idCategoria != "0")
                        entity.IDCategoria = int.Parse(idCategoria);
                    else
                        entity.IDCategoria = null;
                    entity.Rubro = rubro;

                    if (id > 0) {

                        dbContext.Database.ExecuteSqlCommand("DELETE ComprasDetalle WHERE IDCompra=" + id, new object[] { });
                        dbContext.SaveChanges();
                    }
                    else {
                        dbContext.Compras.Add(entity);
                        dbContext.SaveChanges();
                    }
                    //stock

                    if (items != null)
                    {
                        InventariosCommon.ActualizarStockCompra(dbContext, usu.IDUsuario, entity.Tipo, entity.IDCompra, entity.IDPersona, items, entity.IDInventario ?? 0);
                        
                        
                    }
                    dbContext.SaveChanges();


                    if (IdOrden > 0) {
                        var orden = dbContext.OrdenesCompras.Where(x => x.IDOrdenCompra == IdOrden).FirstOrDefault();
                        if (orden != null) {

                            decimal totales = 0;
                            var ax = entity.Saldo;

                            var total = entity.Importe2 + entity.Importe5 + entity.Importe10 + entity.Importe21 + entity.Importe27 + entity.NoGravado;
                            totales += total;
                            if (orden.Compras.Any(x => x.IDCompra != id))
                                totales += orden.Compras.Where(x => x.IDCompra != id).Sum(x => x.Total ?? 0);
                            //totales += orden.Compras.Sum(x =>( (x.Detallado)?(x.Total+x.Iva)??0: x.Total??0));
                            if (totales == orden.Total)
                                orden.Estado = "Conciliado";
                            else
                                orden.Estado = "Pagado";

                        }
                    }
                    dbContext.SaveChanges();

                    dbContext.ActualizarSaldosPorCompra(Convert.ToInt32(entity.IDCompra));
                    return entity;
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }


        public static Compras Guardar(ComprasDto cdto) {
            return Guardar(cdto.IDCompra, cdto.IDPersona, cdto.Fecha, cdto.NroFactura, cdto.Iva, cdto.Importe2,
                cdto.Importe5, cdto.Importe10, cdto.Importe21, cdto.Importe27, cdto.NoGrav, cdto.ImporteMon,
                cdto.ImpNacional, cdto.ImpMunicipal, cdto.ImpInterno, cdto.PercepcionIva, cdto.Otros, cdto.Obs,
                cdto.Tipo, cdto.IdCategoria, cdto.Rubro, cdto.Exento, cdto.FechaEmision, cdto.IdPlanDeCuenta,
                cdto.IDUsuario, cdto.Jurisdicciones, cdto.FechaPrimerVencimiento, cdto.FechaSegundoVencimiento,
                cdto.Detallado, cdto.Items, cdto.IdInventario, cdto.IdOrden, cdto.EmailUsuario);
        }

        public static void EliminarFoto(int idCheque, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var entity = dbContext.Compras.Where(x => x.IDCompra == idCheque && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null) {
                    string Serverpath = HttpContext.Current.Server.MapPath("~/files/explorer/" + idUsuario + "/Compras/" + DateTime.Now.Year.ToString() + "/" + entity.Foto);

                    if (File.Exists(Serverpath)) {
                        File.Delete(Serverpath);

                        entity.Foto = "";
                        dbContext.SaveChanges();
                    }
                    else {
                        throw new Exception("El cheque no tiene una imagen guardada");
                    }
                }
            }
        }

        public static void CargarItems(int idOrden, byte cantDecimales) {
            using (var dbContext = new ACHEEntities()) {
                var orden = dbContext.OrdenesCompras.Include("OrdenesComprasDetalle").Where(x => x.IDOrdenCompra == idOrden).FirstOrDefault();
                if (orden.OrdenesComprasDetalle.Any()) {
                    foreach (var item in orden.OrdenesComprasDetalle.ToList()) {
                        int index = DetalleCart.Retrieve().Items.Count() + 1;
                        var tra = new DetalleViewModel(cantDecimales);
                        tra.ID = index;// DetalleCart.Retrieve().Items.Count() + 1;
                        tra.Concepto = item.Descripcion;

                        decimal precioUnitario = decimal.Parse(item.Precio.ToString().Replace(".", ","));
                        tra.PrecioUnitario = precioUnitario;
                        tra.Bonificacion = item.Bonificacion;
                        tra.Iva = item.Iva;
                        tra.Codigo = item.IDConcepto.HasValue ? item.Conceptos.Codigo : "";
                        tra.Cantidad = decimal.Parse(item.Cantidad.ToString().Replace(".", ",")); ;
                        tra.IDConcepto = item.IDConcepto;


                        DetalleCart.Retrieve().Items.Add(tra);
                    }
                }

            }
        }

        public static string ExportarCompras(int idPersona, string condicion, string periodo, string fechaDesde, string fechaHasta, int idUsuario) {

            string fileName = "Compras_" + idUsuario + "_";
            string path = "~/tmp/";
            try {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities()) {
                    var results = dbContext.Compras.Include("Personas").Where(x => x.IDUsuario == idUsuario).AsQueryable();

                    Int32 numero = 0;
                    if (Int32.TryParse(condicion, out numero)) {
                        results = results.Where(x => x.NroFactura.Contains(condicion));
                    }
                    else if (condicion != string.Empty) {
                        results = results.Where(x => x.Personas.RazonSocial.Contains(condicion) || x.Personas.NombreFantansia.Contains(condicion));
                    }

                    switch (periodo) {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (fechaDesde != string.Empty) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaEmision >= dtDesde);
                    }
                    if (fechaHasta != string.Empty) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaEmision <= dtHasta);
                    }

                    dt = results.OrderBy(x => x.Fecha).ToList().Select(x => new {
                        RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                        FechaEmision = x.FechaEmision.ToString("dd/MM/yyyy"),
                        FechaContable = x.Fecha.ToString("dd/MM/yyyy"),
                        Tipo = x.Tipo,
                        NroFactura = x.NroFactura,
                        Importe = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? ((x.Total.Value - (x.NoGravado + x.Exento)) * -1) : (x.Total.Value - (x.NoGravado + x.Exento)),
                        Iva = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? x.Iva * -1 : x.Iva,
                        NoGravado = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? (-1 * (x.NoGravado + x.Exento)) : (x.NoGravado + x.Exento),
                        Retenciones = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? -1 * x.TotalImpuestos.Value : x.TotalImpuestos.Value,
                        Total = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? (-1 * (x.TotalImpuestos.Value + x.Total.Value + x.Iva)) : (x.TotalImpuestos.Value + x.Total.Value + x.Iva),
                        Categoria = x.IDCategoria.HasValue ? x.Categorias.Nombre : "",
                        Rubro = x.Rubro,
                        Observaciones = x.Observaciones
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e) {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        public static List<JurisdiccionesViewModel> ObtenerJurisdicciones(int idCompra) {
            using (var dbContext = new ACHEEntities()) {
                var lista = dbContext.Jurisdicciones.Where(x => x.IDCompra == idCompra).ToList().Select(x => new JurisdiccionesViewModel() {
                    IDJurisdicion = x.IDProvincia,
                    IDCompra = x.IDCompra,
                    Importe = x.Importe,
                    NombreJurisdiccion = x.Provincias.Nombre
                }).ToList();

                return lista;
            }
        }

        public static List<Combo2ViewModel> ObtenerJurisdiccionUsuario(string idJurisdiccion) {
            List<Combo2ViewModel> select = new List<Combo2ViewModel>();

            using (var dbContext = new ACHEEntities()) {
                if (idJurisdiccion != null && idJurisdiccion != "") {
                    var provincias = idJurisdiccion.Split(',');
                    List<Int32> idjurisdicciones = new List<Int32>();

                    foreach (var item in provincias)
                        idjurisdicciones.Add(Convert.ToInt32(item));

                    select = dbContext.Provincias.Where(x => idjurisdicciones.Contains(x.IDProvincia)).Select(x => new Combo2ViewModel() {
                        ID = x.IDProvincia,
                        Nombre = x.Nombre
                    }).ToList();
                }
            }
            return select;

        }

        public static bool EliminarCompra(int id, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    if (dbContext.PagosDetalle.Any(x => x.IDCompra == id))
                        throw new CustomException("No se puede eliminar por tener pagos asociados");
                    else if (dbContext.Activos.Any(x => x.IDCompra == id))
                        throw new CustomException("No se puede eliminar por tener Activos asociados");
                    if (dbContext.PagosFormasDePago.Any(x => x.Compras.IDCompra == id))
                        throw new CustomException("No se puede eliminar por tener pagos asociados");

                    var entity = dbContext.Compras.Where(x => x.IDCompra == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null) {
                        if (ContabilidadCommon.ValidarCierreContable(idUsuario, entity.Fecha))
                            throw new CustomException("El comprobante no puede eliminarse ya que el año contable ya fue cerrado.");
                        if (entity.Tipo != "COT" && ContabilidadCommon.ValidarFechaLimiteCarga(idUsuario, entity.Fecha))
                            throw new CustomException("El comprobante no puede eliminarse ya que la fecha se encuentra fuera de la fecha límite de carga.");

                        var jurisdicciones = dbContext.Jurisdicciones.Where(x => x.IDCompra == id).ToList();
                        foreach (var item in jurisdicciones)
                            dbContext.Jurisdicciones.Remove(item);

                        InventariosCommon.RestarStockCompra(dbContext, idUsuario, entity.ComprasDetalle.ToList(), entity.IDInventario ?? 0, id);

                        var orden = dbContext.OrdenesCompras.Where(x => x.IDOrdenCompra == entity.IDOrdenCompra).FirstOrDefault();
                        if (orden != null) {

                            decimal totales = 0;

                            if (orden.Compras.Any(x => x.IDCompra != id))
                                totales += orden.Compras.Where(x => x.IDCompra != id).Sum(x => x.Total ?? 0);
                            //totales += orden.Compras.Sum(x =>( (x.Detallado)?(x.Total+x.Iva)??0: x.Total??0));
                            if (totales == orden.Total)
                                orden.Estado = "Conciliado";
                            else
                                orden.Estado = "Pagado";

                        }

                        dbContext.Database.ExecuteSqlCommand("DELETE Asientos WHERE IDCompra=" + id);

                        dbContext.Compras.Remove(entity);

                        dbContext.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (CustomException ex) {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }

        }

        public static ResultadosComprasViewModel ObtenerCompras(string condicion, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario) {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var results = dbContext.Compras.Include("Personas").Where(x => x.IDUsuario == idUsuario).AsQueryable();

                    /*Int32 numero = 0;
                    if (Int32.TryParse(condicion, out numero))
                        results = results.Where(x => x.NroFactura.Contains(condicion));
                    else if (!string.IsNullOrWhiteSpace(condicion))
                        results = results.Where(x => x.Personas.RazonSocial.ToLower().Contains(condicion.ToLower()) || x.Personas.NombreFantansia.ToLower().Contains(condicion.ToLower()));*/

                    Int32 numero = 0;

                    if (Int32.TryParse(condicion, out numero))
                        results = results.Where(x => x.NroFactura.Contains(condicion));
                    else if (condicion.Contains("-")) {
                        var punto = (string.IsNullOrWhiteSpace(condicion.Split('-')[0])) ? "" : condicion.Split('-')[0];
                        var nro = (string.IsNullOrWhiteSpace(condicion.Split('-')[1])) ? "" : condicion.Split('-')[1];
                        if (punto != string.Empty && nro != string.Empty) {
                            results = results.Where(x => x.NroFactura == condicion);
                        }
                        else
                            results = results.Where(x => x.NroFactura.Contains(condicion));
                    }
                    else if (condicion != "")
                        results = results.Where(x => x.Personas.RazonSocial.ToLower().Contains(condicion) || x.Personas.NombreFantansia.ToLower().Contains(condicion) || x.Tipo.ToLower().Contains(condicion));

                    switch (periodo) {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(fechaDesde)) {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.FechaEmision >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta)) {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.FechaEmision <= dtHasta);
                    }

                    //Determinamos el formato decimal
                    var decimalFormat = "N2";//DecimalFormatter.GetDecimalStringFormat(idUsuario);

                    page--;
                    ResultadosComprasViewModel resultado = new ResultadosComprasViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new ComprasViewModel() {
                            ID = x.IDCompra,
                            RazonSocial = (x.Personas.NombreFantansia == "" ? x.Personas.RazonSocial.ToUpper() : x.Personas.NombreFantansia.ToUpper()),
                            Fecha = x.FechaEmision.ToString("dd/MM/yyyy"),
                            Tipo = x.Tipo,
                            NroFactura = x.NroFactura,

                            Iva = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? (x.Iva * -1).ToString(decimalFormat) : x.Iva.ToString(decimalFormat),
                            NoGravado = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? (-1 * (x.NoGravado + x.Exento)).ToString(decimalFormat) : (x.NoGravado + x.Exento).ToString(decimalFormat),
                            Retenciones = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? (-1 * x.TotalImpuestos.Value).ToString(decimalFormat) : x.TotalImpuestos.Value.ToString(decimalFormat),
                            Importe = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? ("-" + (x.Total.Value - (x.NoGravado + x.Exento)).ToString(decimalFormat)) : (x.Total.Value - (x.NoGravado + x.Exento)).ToString(decimalFormat),
                            Total = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC" || x.Tipo == "NCM" || x.Tipo == "NCE") ? ("-" + Convert.ToDecimal(x.TotalImpuestos + x.Total + x.Iva).ToString(decimalFormat)) : Convert.ToDecimal(x.TotalImpuestos + x.Total + x.Iva).ToString(decimalFormat),
                            Detallado = x.Detallado,
                        });
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public static string ObtenerCategorias(int idUsuario, string claseJS) {
            var html = "";
            using (var dbContext = new ACHEEntities()) {
                var list = dbContext.Categorias.Where(x => x.IDUsuario == idUsuario).OrderBy(x => x.Nombre).ToList();
                if (list.Any()) {
                    foreach (var detalle in list) {
                        html += "<tr>";
                        html += "<td>" + detalle.Nombre + "</td>";
                        html += "<td><a href='#' title='Editar' style='font-size: 16px;' onclick=\"" + claseJS + ".editarCategoria(" + detalle.IDCategoria + ",'" + detalle.Nombre.Trim() + "');\"><i class='fa fa-pencil'></i></a>&nbsp;<a href='#' title='Eliminar' style='font-size: 16px;' onclick='" + claseJS + ".eliminarCategoria(" + detalle.IDCategoria + ");'><i class='fa fa-times'></i></a></td>";
                        html += "</tr>";
                    }
                }
                else
                    html += "<tr><td colspan='2'>No hay un detalle disponible</td></tr>";
            }

            return html;
        }

        public static void GuardarCategoria(int id, string nombre, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                if (id == 0 && dbContext.Categorias.Any(x => x.Nombre.ToLower() == nombre.ToLower() && x.IDUsuario == idUsuario))
                    throw new Exception("El nombre ingresado ya se encuentra creado");

                Categorias entity;
                if (id > 0)
                    entity = dbContext.Categorias.Where(x => x.IDCategoria == id && x.IDUsuario == idUsuario).FirstOrDefault();
                else {
                    entity = new Categorias();
                    entity.IDUsuario = idUsuario;
                }
                entity.Nombre = nombre;

                if (id > 0) {
                    dbContext.SaveChanges();
                }
                else {
                    dbContext.Categorias.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }

        public static void EliminarCategoria(int id, int idUsuario) {

            using (var dbContext = new ACHEEntities()) {
                if (!dbContext.Compras.Any(x => x.IDCategoria == id && x.IDUsuario == idUsuario)) {
                    Categorias entity = dbContext.Categorias.Where(x => x.IDCategoria == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null) {
                        dbContext.Categorias.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
                else
                    throw new Exception("La categoría se encuentra asociada a 1 o más pagos registrados.");

            }
        }

        public static string ObtenerImporteDeOC(int idOrden, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var entity = dbContext.OrdenesCompras.Where(x => x.IDOrdenCompra == idOrden && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null) {
                    return entity.Total.ToString().Replace(",", ".");

                }
            }
            return "";
        }

    }
}

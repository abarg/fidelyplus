﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ACHE.Negocio.Reportes {
    public class ReportesCommon {
        #region PERCEPCIONES

        public static ResultadosPercepcionesViewModel ObtenerPercepcionesEmitidas(int idPersona, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario, string impuesto) {
            //Determinamos cantidad de decimales que posee configurado el usuario
            var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(idUsuario);

            using (var dbContext = new ACHEEntities()) {
                var results = dbContext.Comprobantes.Where(x => x.IDUsuario == idUsuario && x.PercepcionIIBB > 0).AsQueryable();
                if (idPersona > 0)
                    results = results.Where(x => x.IDPersona == idPersona);
                if (fechaDesde != string.Empty) {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.FechaComprobante >= dtDesde);
                }
                if (fechaHasta != string.Empty) {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    results = results.Where(x => x.FechaComprobante <= dtHasta);
                }

                page--;
                ResultadosPercepcionesViewModel resultado = new ResultadosPercepcionesViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                List<PercepcionesViewModel> list = new List<PercepcionesViewModel>();


                switch (impuesto) {
                    case "IIBB":
                        list = results.OrderBy(x => x.FechaComprobante).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new PercepcionesViewModel() {
                            Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                            RazonSocial = x.Personas.RazonSocial,
                            Cuit = x.Personas.NroDocumento,
                            CondicionIVA = x.Personas.CondicionIva,
                            NroComprobante = x.Tipo + " " + x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                            Jurisdiccion = (x.Provincias == null) ? "" : x.Provincias.Nombre,
                            Importe = Math.Round(((x.ImporteTotalBruto * x.PercepcionIIBB) / 100), cantidadDecimales).ToMoneyFormat(2),
                            ImporteTotal = x.ImporteTotalNeto.ToMoneyFormat(2)
                        }).ToList();
                        break;
                    case "IVA":
                        list = results.OrderBy(x => x.FechaComprobante).Skip(page * pageSize).Take(pageSize).ToList()
                        .Select(x => new PercepcionesViewModel() {
                            Fecha = x.FechaComprobante.ToString("dd/MM/yyyy"),
                            RazonSocial = x.Personas.RazonSocial,
                            Cuit = x.Personas.NroDocumento,
                            CondicionIVA = x.Personas.CondicionIva,
                            NroComprobante = x.Tipo + " " + x.PuntosDeVenta.Punto.ToString("#0000") + "-" + x.Numero.ToString("#00000000"),
                            Importe = Math.Round(((x.ImporteTotalBruto * x.PercepcionIVA) / 100), cantidadDecimales).ToMoneyFormat(2),
                            ImporteTotal = x.ImporteTotalNeto.ToMoneyFormat(2)
                        }).ToList();
                        break;
                }
                resultado.Items = list;
                return resultado;
            }
        }

        public static ResultadosPercepcionesViewModel ObtenerPercepcionesSufridas(int idPersona, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var results = dbContext.Jurisdicciones.Where(x => x.Compras.IDUsuario == idUsuario).AsQueryable();
                if (idPersona > 0)
                    results = results.Where(x => x.Compras.IDPersona == idPersona);
                if (fechaDesde != string.Empty) {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.Compras.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty) {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    results = results.Where(x => x.Compras.Fecha <= dtHasta);
                }

                page--;
                ResultadosPercepcionesViewModel resultado = new ResultadosPercepcionesViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.Compras.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new PercepcionesViewModel() {
                        Fecha = x.Compras.Fecha.ToString("dd/MM/yyyy"),
                        RazonSocial = x.Compras.Personas.RazonSocial,
                        Cuit = x.Compras.Personas.NroDocumento,
                        CondicionIVA = x.Compras.Personas.CondicionIva,
                        NroComprobante = x.Compras.Tipo + " " + x.Compras.NroFactura,
                        Jurisdiccion = (x.Provincias == null) ? "" : x.Provincias.Nombre,
                        Importe = (x.Compras.Tipo == "NCA" || x.Compras.Tipo == "NCB" || x.Compras.Tipo == "NCC" || x.Compras.Tipo == "NCM" || x.Compras.Tipo == "NCE") ? (-1 * x.Importe).ToMoneyFormat(2) : x.Importe.ToMoneyFormat(2),
                        ImporteTotal = (x.Compras.Tipo == "NCA" || x.Compras.Tipo == "NCB" || x.Compras.Tipo == "NCC" || x.Compras.Tipo == "NCM" || x.Compras.Tipo == "NCE") ? (-1 * x.Compras.Total.Value).ToMoneyFormat(2) : x.Compras.Total.Value.ToMoneyFormat(2)
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }

        public static ResultadosPercepcionesViewModel ObtenerPercepcionesSufridasIVA(int idPersona, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {
                var results = dbContext.Compras.Where(x => x.IDUsuario == idUsuario && x.PercepcionIVA > 0).AsQueryable();
                if (idPersona > 0)
                    results = results.Where(x => x.IDPersona == idPersona);
                if (fechaDesde != string.Empty) {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty) {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    results = results.Where(x => x.Fecha <= dtHasta);
                }

                page--;
                ResultadosPercepcionesViewModel resultado = new ResultadosPercepcionesViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new PercepcionesViewModel() {
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        RazonSocial = x.Personas.RazonSocial,
                        Cuit = x.Personas.NroDocumento,
                        CondicionIVA = x.Personas.CondicionIva,
                        NroComprobante = x.Tipo + " " + x.NroFactura,
                        Importe = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC") ? (-1 * x.PercepcionIVA).ToMoneyFormat(2) : x.PercepcionIVA.ToMoneyFormat(2),
                        ImporteTotal = (x.Tipo == "NCA" || x.Tipo == "NCB" || x.Tipo == "NCC") ? (-1 * x.Total.Value).ToMoneyFormat(2) : x.Total.Value.ToMoneyFormat(2)
                    });
                resultado.Items = list.ToList();

                return resultado;
            }
        }

        #endregion

        #region DETALLE BANCARIO
        public static ResultadosDetalleBancarioViewModel ObtenerDetalleBancario(int idBanco, string filtro, string fechaDesde, string fechaHasta, int page, int pageSize, int idUsuario) {
            using (var dbContext = new ACHEEntities()) {

                string formato = ConfigurationManager.AppSettings["FormatoFechasSQL"];

                string sql = "select * from RptBancarioView where IDUsuario =" + idUsuario;
                if (idBanco > 0)
                    sql += " and IDBanco =" + idBanco;
                if (!string.IsNullOrWhiteSpace(fechaDesde))
                    sql += " and fecha >='" + DateTime.Parse(fechaDesde).Date.ToString(formato) + "'";
                if (!string.IsNullOrWhiteSpace(fechaHasta))
                    sql += " and fecha <='" + DateTime.Parse(fechaHasta).Date.ToString(formato) + " 23:59:59 pm'";
                if (!string.IsNullOrWhiteSpace(filtro))
                    sql += " and (Tipo like'%" + filtro + "%' or NumeroComprobante like'%" + filtro + "%' or NroReferencia like'%" + filtro + "%')";

                var results = dbContext.Database.SqlQuery<RptBancarioView>(sql, new object[] { }).ToList();

                //Obtenemos el formato decimal
                var decimalFormat = DecimalFormatter.GetDecimalStringFormat(idUsuario);

                page--;
                ResultadosDetalleBancarioViewModel resultado = new ResultadosDetalleBancarioViewModel();
                resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.Count();

                var list = results.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new DetalleBancarioViewModel() {
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        Nombre = x.Nombre,
                        Tipo = x.Tipo,
                        FormaDePago = x.FormaDePago,
                        NroReferencia = x.NroReferencia,
                        TipoMovimiento = x.TipoMovimiento,
                        Comprobante = (x.Tipo == "COBRANZA" || x.Tipo == "PAGO" || x.Tipo == "DEBITO") ? x.TipoComprobante + " " + x.NumeroComprobante : "",
                        Importe = (x.TipoMovimiento == "Ingreso") ? Convert.ToDecimal(x.Importe).ToString(decimalFormat) : (Convert.ToDecimal(x.Importe) * -1).ToMoneyFormat(2),
                        Entidad = x.Entidad,
                        IDEntidad = x.ID,
                        PuedeEditar = (x.Entidad == "Pago" || x.Entidad == "Cobranza" || x.Entidad == "Movimiento de Fondo" || x.Entidad == "Banco Detalle" || x.Entidad == "Asiento Manual") ? "SI" : "NO"
                    });
                resultado.Items = list.ToList();
                return resultado;
            }
        }
        #endregion

        
      
    }
}

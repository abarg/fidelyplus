﻿using ACHE.Model;
using ACHE.Negocio.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Extensions;
using ACHE.Negocio.Helpers;

namespace ACHE.Negocio.Tesoreria
{
    public class CajaCommon
    {
        #region ABM CAJA

        public static void EliminarCajas(int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (!dbContext.PagosFormasDePago.Any(x => x.IDCaja == id && x.Pagos.IDUsuario == idUsuario))
                    {
                        if (!dbContext.CobranzasFormasDePago.Any(x => x.IDCaja == id && x.Cobranzas.IDUsuario == idUsuario))
                        {
                            Cajas entity = dbContext.Cajas.Where(x => x.IDCaja == id && x.IDUsuario == idUsuario).FirstOrDefault();
                            if (entity != null)
                            {
                                dbContext.Cajas.Remove(entity);
                                dbContext.SaveChanges();
                            }
                        }
                        else
                            throw new Exception("La caja se encuentra asociada a 1 o más cobranzas registradas.");
                    }
                    else
                        throw new Exception("La caja se encuentra asociada a 1 o más pagos registrados.");



                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<Cajas> ObtenerCajas(int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Cajas.Where(x => x.IDUsuario == idUsuario)
                        .OrderBy(x => x.Nombre).ToList();
            }
        }

        public static int GuardarCaja(CajaViewModel caja, int idUsuario)
        {
            try
            {

                using (var dbContext = new ACHEEntities())
                {
                    Caja entity;
                    if (caja.ID > 0)
                        entity = dbContext.Caja.Where(x => x.IDCaja == caja.ID && x.IDUsuario == idUsuario).FirstOrDefault();
                    else
                    {
                        entity = new Caja();
                        entity.FechaAlta = DateTime.Now.Date;
                        entity.Estado = "Cargado";
                        entity.Importe = Math.Abs(caja.Importe);
                        entity.Fecha = Convert.ToDateTime(caja.Fecha);
                    }
                    if (string.IsNullOrWhiteSpace(caja.Concepto))
                        entity.IDConceptosCaja = null;
                    else
                        entity.IDConceptosCaja = Convert.ToInt32(caja.Concepto);

                    if (!dbContext.Cajas.Any(x => x.IDUsuario == idUsuario && x.IDCaja == caja.IDCajas))
                        throw new CustomException("Error con la caja seleccionada. Contacte  soporte");

                    entity.TipoMovimiento = caja.tipoMovimiento;
                    entity.IDUsuario = idUsuario;
                    entity.IDCajas = caja.IDCajas;
                    entity.Observaciones = caja.Observaciones;
                    entity.MedioDePago = caja.MedioDePago;
                    entity.Ticket = caja.Ticket;

                    if (caja.IDPlanDeCuenta != 0)
                        entity.IDPlanDeCuenta = caja.IDPlanDeCuenta;

                    if (caja.ID == 0)
                        dbContext.Caja.Add(entity);
                    dbContext.SaveChanges();

                    return entity.IDCaja;
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static int GuardarCajaMovimiento(CajaViewModel caja, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    Caja entity;
                    if (caja.ID > 0)
                        entity = dbContext.Caja.Where(x => x.IDCaja == caja.ID && x.IDUsuario == idUsuario).FirstOrDefault();
                    else
                    {
                        entity = new Caja();
                        entity.FechaAlta = DateTime.Now.Date;
                        entity.Estado = "Cargado";
                        entity.TipoMovimiento = caja.tipoMovimiento;
                        entity.Fecha = Convert.ToDateTime(caja.Fecha);
                        entity.IDUsuario = idUsuario;
                    }

                    var concepto = dbContext.ConceptosCaja.Where(x => x.IDUsuario == idUsuario && x.Nombre == caja.Concepto).FirstOrDefault();
                    if (concepto == null)
                        entity.ConceptosCaja = new ConceptosCaja { IDUsuario = idUsuario, Nombre = caja.Concepto };
                    else
                        entity.IDConceptosCaja = concepto.IDConceptoCaja;

                    entity.Importe = Math.Abs(caja.Importe);
                    entity.IDUsuario = entity.IDUsuario;
                    entity.Observaciones = caja.Observaciones;
                    entity.MedioDePago = caja.MedioDePago;
                    entity.Ticket = caja.Ticket;

                    if (caja.IDPlanDeCuenta != 0)
                        entity.IDPlanDeCuenta = caja.IDPlanDeCuenta;

                    if (caja.ID == 0)
                        dbContext.Caja.Add(entity);
                    dbContext.SaveChanges();

                    return entity.IDCaja;
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void RevertirMovimiento(int id, string motivo, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Caja.Where(x => x.IDCaja == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        //if (ContabilidadCommon.ValidarFechaLimiteCarga(usu.IDUsuario, entity.Fecha))
                        //    throw new CustomException("El movimiento no puede eliminarse ya que la fecha se encuentra fuera de la fecha limite de carga.");
                        if (entity.Estado == "Cargado")
                        {
                            entity.Fecha = DateTime.Now.Date;
                            entity.Estado = "Anulado";
                            dbContext.SaveChanges();
                            entity.TipoMovimiento = (entity.TipoMovimiento == "Ingreso") ? "Egreso" : "Ingreso";
                            entity.Observaciones = motivo;
                            entity.IDConceptosCaja = null;
                            entity.MedioDePago = "";
                            dbContext.Caja.Add(entity);
                            dbContext.SaveChanges();

                            if (usu.UsaPlanCorporativo) //Plan Corporativo
                                ContabilidadCommon.InvertirAsientoDeCaja(usu, id);
                        }
                        else
                            throw new CustomException("El movimiento no se puede eliminar ya que se encuentra conciliado");
                    }
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void EliminarMovimiento(int id, string motivo, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Caja.Where(x => x.IDCaja == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.Estado == "Cargado")
                        {

                            dbContext.Database.ExecuteSqlCommand("DELETE Asientos WHERE IDCaja=" + id);

                            dbContext.Caja.Remove(entity);
                            dbContext.SaveChanges();
                        }
                        else
                            throw new CustomException("El movimiento no se puede eliminar ya que se encuentra conciliado");
                    }
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void EliminarCajaFisica(int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.Caja.Where(x => x.IDCaja == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.Caja.Remove(entity);
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static ResultadosCajaViewModel ObtenerCaja(int idCaja, string tipoMovimiento, string fechaDesde, string fechaHasta, string periodo, bool incluirAsientos, int page, int pageSize, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.CajaView.Where(x => x.IDUsuario == idUsuario && x.IDCajas == idCaja).OrderBy(x => x.Fecha).AsQueryable();

                if (!incluirAsientos)
                    results = results.Where(x => x.MedioDePago != "Asiento manual");

                if (!string.IsNullOrWhiteSpace(tipoMovimiento))
                    results = results.Where(x => x.TipoMovimiento == tipoMovimiento);

                switch (periodo)
                {
                    case "30":
                        fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                        break;
                    case "15":
                        fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                        break;
                    case "7":
                        fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                        break;
                    case "1":
                        fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                        break;
                    case "0":
                        fechaDesde = DateTime.Now.ToShortDateString();
                        break;
                }

                //Obtenemos el formato decimal
                var decimalFormat = "N2";//DecimalFormatter.GetDecimalStringFormat(idUsuario);

                page--;
                ResultadosCajaViewModel resultado = new ResultadosCajaViewModel();

                var list = results.OrderByDescending(x => x.Fecha).ToList()
                 .Select(x => new CajaViewModel()
                 {
                     ID = x.ID,
                     NombreCaja = x.NombreCaja,
                     tipoMovimiento = x.TipoMovimiento,
                     Fecha = Convert.ToDateTime(x.Fecha).ToString("dd/MM/yyyy"),
                     Concepto = (x.ConceptosCaja == null) ? "" : x.ConceptosCaja,
                     Estado = x.Estado,
                     Observaciones = x.Observaciones,
                     Ingreso = (x.TipoMovimiento == "Ingreso") ? Math.Abs(x.Importe).ToString(decimalFormat) : "",
                     Egreso = (x.TipoMovimiento == "Egreso") ? (-1 * Math.Abs(x.Importe)).ToString(decimalFormat) : "",
                     MedioDePago = x.MedioDePago,
                     PuedeEditar = x.PuedeEditar,
                     Ticket = x.Ticket
                 }).ToList();

                decimal saldo = 0;

                for (int i = list.Count - 1; i >= 0; i--)
                {
                    var ingreso = Convert.ToDecimal((list[i].Ingreso == "") ? "0" : list[i].Ingreso);
                    var egreso = Convert.ToDecimal((list[i].Egreso == "") ? "0" : list[i].Egreso);
                    saldo = saldo + (Math.Abs(ingreso) - Math.Abs(egreso));
                    list[i].Saldo = saldo.ToString(decimalFormat);
                }

                if (!string.IsNullOrWhiteSpace(fechaDesde))
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    list = list.Where(x => Convert.ToDateTime(x.Fecha) >= dtDesde).ToList();
                }
                if (!string.IsNullOrWhiteSpace(fechaHasta))
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    list = list.Where(x => Convert.ToDateTime(x.Fecha) <= dtHasta).ToList();
                }

                resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                resultado.TotalItems = list.Count();
                list = list.Skip(page * pageSize).Take(pageSize).ToList();

                var listaAux = list.GroupBy(x => new { Convert.ToDateTime(x.Fecha).Date.Month, Convert.ToDateTime(x.Fecha).Date.Year }).ToList().Select(x => new ConceptosMeses()
                {
                    concepto = Convert.ToDateTime(x.FirstOrDefault().Fecha).ToString("MMMMMMMMM yyyy").ToProperCase(),
                    Conceptos = x.ToList()
                });

                resultado.Items = listaAux.ToList();

                var totalIngresoSin = dbContext.CajaView.Where(x => x.Estado == "Cargado" && x.IDUsuario == idUsuario && x.IDCajas == idCaja && x.TipoMovimiento == "Ingreso").Select(x => x.Importe).DefaultIfEmpty(0).Sum(x => Math.Abs(x));
                var totalEgresoSin = dbContext.CajaView.Where(x => x.Estado == "Cargado" && x.IDUsuario == idUsuario && x.IDCajas == idCaja && x.TipoMovimiento == "Egreso").Select(x => x.Importe).DefaultIfEmpty(0).Sum(x => Math.Abs(x));

                var totalIngreso = dbContext.CajaView.Where(x => x.IDUsuario == idUsuario && x.IDCajas == idCaja && x.TipoMovimiento == "Ingreso").Select(x => x.Importe).DefaultIfEmpty(0).Sum(x => Math.Abs(x));
                var totalEgreso = dbContext.CajaView.Where(x => x.IDUsuario == idUsuario && x.IDCajas == idCaja && x.TipoMovimiento == "Egreso").Select(x => x.Importe).DefaultIfEmpty(0).Sum(x => Math.Abs(x));

                resultado.TotalSinConsolidar = (totalIngresoSin - totalEgresoSin).ToString(decimalFormat);
                resultado.Total = (totalIngreso - totalEgreso).ToString(decimalFormat);

                return resultado;
            }
        }

        #endregion
    }
}
﻿using ACHE.Model;
using ACHE.Negocio.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACHE.Negocio.Banco
{
    public class BancosCommon
    {
        #region ABM Bancos

        public static int GuardarBanco(ACHEEntities dbContext, int id, int idBancoBase, string nroCuenta, string moneda, int activo, string saldoInicial, string ejecutivo, string direccion, string telefono, string email, string observacion, int idUsuario)
        {
            try
            {

                if (!(idBancoBase > 0))
                    throw new CustomException("El banco es obligatorio");
                if (dbContext.Bancos.Any(x => x.IDUsuario == idUsuario && x.IDBancoBase == idBancoBase && x.NroCuenta == nroCuenta && x.IDBanco != id))
                    throw new CustomException("El nro de cuenta ingresado ya se encuentra registrado.");

                Bancos entity;
                if (id > 0)
                    entity = dbContext.Bancos.Where(x => x.IDBanco == id && x.IDUsuario == idUsuario).FirstOrDefault();
                else
                {
                    entity = new Bancos();
                    entity.FechaAlta = DateTime.Now;
                    entity.IDUsuario = idUsuario;
                }

                entity.IDBancoBase = idBancoBase;
                entity.NroCuenta = nroCuenta.ToString();
                entity.Moneda = moneda;
                entity.Activo = Convert.ToBoolean(activo);

                entity.Ejecutivo = ejecutivo;
                entity.Direccion = direccion;
                entity.Telefono = telefono;
                entity.Email = email;
                entity.Observaciones = observacion;

                entity.SaldoInicial = (!string.IsNullOrWhiteSpace(saldoInicial)) ? decimal.Parse(saldoInicial.Replace(".", ",")) : 0;

                if (id == 0)
                    dbContext.Bancos.Add(entity);

                dbContext.SaveChanges();
                return entity.IDBanco;

            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static int GuardarBanco(int id, int idBancoBase, string nroCuenta, string moneda, int activo, string saldoInicial, string ejecutivo, string direccion, string telefono, string email, string observacion, int idUsuario)
        {
            using (var dbContext = new ACHEEntities())
            {
                return GuardarBanco(dbContext, id, idBancoBase, nroCuenta, moneda, activo, saldoInicial, ejecutivo, direccion, telefono, email, observacion, idUsuario);
            }
        }

        public static bool EliminarBancos(int id, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (dbContext.GastosBancarios.Any(x => x.IDBanco == id))
                        throw new CustomException("No se puede eliminar el banco, por tener Gastos bancarios asociados");
                    else if (dbContext.PagosFormasDePago.Any(x => x.IDBanco == id))
                        throw new CustomException("No se puede eliminar el banco, por tener pagos asociados");
                    else if (dbContext.CobranzasFormasDePago.Any(x => x.IDBanco == id))
                        throw new CustomException("No se puede eliminar el banco, por tener cobranzas asociadas");
                    else if (dbContext.CondicionesVentas.Any(x => x.IDBanco == id))
                        throw new CustomException("No se puede eliminar el banco por estar asociado a una cobranza automática");

                    var banco = dbContext.BancosPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario && x.IDBanco == id).FirstOrDefault();
                    if (banco != null)
                    {
                        if (dbContext.AsientoDetalle.Any(x => x.Asientos.IDUsuario == usu.IDUsuario && x.IDPlanDeCuenta == banco.IDPlanDeCuenta))
                            throw new CustomException("No se puede eliminar por tener asientos contables asociados");

                        var cuenta = dbContext.PlanDeCuentas.Where(x => x.IDPlanDeCuenta == banco.IDPlanDeCuenta).FirstOrDefault();
                        dbContext.BancosPlanDeCuenta.Remove(banco);
                        dbContext.PlanDeCuentas.Remove(cuenta);
                        dbContext.SaveChanges();
                    }

                    var entity = dbContext.Bancos.Where(x => x.IDBanco == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.Bancos.Remove(entity);
                        dbContext.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static ResultadosBancosViewModel ObtenerBancos(string condicion, int page, int pageSize, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.Bancos.Where(x => x.IDUsuario == idUsuario).AsQueryable();

                    if (!string.IsNullOrWhiteSpace(condicion))
                        results = results.Where(x => x.BancosBase.Nombre.ToLower().Contains(condicion.ToLower()) || x.NroCuenta.ToLower().Contains(condicion.ToLower()));

                    //Obtenemos el formato decimal
                    var decimalFormat = DecimalFormatter.GetDecimalStringFormat(idUsuario);

                    page--;
                    ResultadosBancosViewModel resultado = new ResultadosBancosViewModel();

                    var list = results.OrderBy(x => x.BancosBase.Nombre).Skip(page * pageSize).Take(pageSize).ToList()
                     .Select(x => new BancosViewModel()
                     {
                         ID = x.IDBanco,
                         Nombre = x.BancosBase.Nombre.ToUpper(),
                         NroCuenta = x.NroCuenta,
                         Moneda = x.Moneda,
                         Activo = (x.Activo) ? "SI" : "NO",
                         SaldoInicial = x.SaldoInicial.ToString(decimalFormat),
                         SaldoActual = ObtenerSaldoActual(dbContext, x.IDBanco, idUsuario, x.SaldoInicial),
                         Ejecutivo = x.Ejecutivo,
                         Telefono = x.Telefono,
                         Direccion = x.Direccion,
                         Email = x.Email,
                         Observacion = x.Observaciones,
                         IDBancoBase = x.IDBancoBase
                     });

                    resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = list.Count();
                    resultado.Items = list.ToList();
                    return resultado;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private static string ObtenerSaldoActual(ACHEEntities dbContext, int idBanco, int idUsuario, decimal saldoInicial)
        {
            //Obtenemos el formato decimal
            var decimalFormat = DecimalFormatter.GetDecimalStringFormat(idUsuario);

            //El id<>0 es para excluir el saldo inicial

            string sql = string.Empty;
            sql = "select SUM(Importe) from RptBancarioView where IDUsuario=" + idUsuario + " and IDBanco=" + idBanco + " and tipoMovimiento='Ingreso' and id<>0 and Fecha<='" + DateTime.Now.ToString("yyy-MM-dd") + "'";
            var Ingresos = dbContext.Database.SqlQuery<decimal?>(sql, new object[] { }).FirstOrDefault();

            sql = "select SUM(Importe) from RptBancarioView where IDUsuario=" + idUsuario + " and IDBanco=" + idBanco + " and tipoMovimiento='Egreso' and id<>0 and Fecha<='" + DateTime.Now.ToString("yyy-MM-dd") + "'";
            var Egresos = dbContext.Database.SqlQuery<decimal?>(sql, new object[] { }).FirstOrDefault();

            var saldo = (saldoInicial + Convert.ToDecimal(Ingresos) - Convert.ToDecimal(Egresos)).ToString(decimalFormat);
            return saldo;
        }

        #endregion

        #region ABM Bancos Detalle

        public static int GuardarDetalle(BancosDetalleViewModel detalle, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    BancosDetalle entity;
                    if (detalle.ID > 0)
                        entity = dbContext.BancosDetalle.Where(x => x.IDBancoDetalle == detalle.ID && x.IDUsuario == idUsuario).FirstOrDefault();
                    else
                    {
                        entity = new BancosDetalle();
                        entity.FechaAlta = DateTime.Now.Date;
                    }

                    entity.TipoMovimiento = detalle.tipoMovimiento;
                    entity.Fecha = Convert.ToDateTime(detalle.Fecha);
                    entity.IDBanco = detalle.IDBanco;
                    entity.Importe = Math.Abs(detalle.Importe);
                    entity.IDUsuario = idUsuario;
                    entity.Observaciones = detalle.Observaciones;
                    entity.MedioDePago = "";// detalle.MedioDePago;
                    entity.Ticket = detalle.Ticket;
                    entity.Concepto = detalle.Concepto;

                    if (detalle.idCategoria != "" && detalle.idCategoria != "0")
                        entity.IDCategoria = int.Parse(detalle.idCategoria);
                    else
                        entity.IDCategoria = null;


                    if (detalle.IDPlanDeCuenta != 0)
                        entity.IDPlanDeCuenta = detalle.IDPlanDeCuenta;

                    if (detalle.ID == 0)
                        dbContext.BancosDetalle.Add(entity);
                    dbContext.SaveChanges();

                    return entity.IDBancoDetalle;
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool EliminarDetalle(int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var entity = dbContext.BancosDetalle.Where(x => x.IDBancoDetalle == id && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.BancosDetalle.Remove(entity);
                        dbContext.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        #endregion
    }
}
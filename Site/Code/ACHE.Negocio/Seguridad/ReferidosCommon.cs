﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ACHE.Model;
using ACHE.Negocio.Common;
using ClosedXML.Excel;

namespace ACHE.Negocio.Seguridad
{
    public class ReferidosCommon
    {
        public static void EnviarMailsReferidos(int idUsuario, string para, string asunto, string mensaje, bool incluirClientes, bool incluirProveedores, string emailUsuario, string displayFrom)
        {
            using (var dbContext = new ACHEEntities())
            {

                var listMailsUsuariosExistentes = dbContext.Usuarios.Where(x => x.Activo && !x.FechaBaja.HasValue).Select(u => u.Email).ToList();
                List<string> listPara = para.Split(',').Where(m => !listMailsUsuariosExistentes.Contains(m)).ToList();

                List<string> tiposPersonas = new List<string>();
                if (incluirClientes) tiposPersonas.Add("C");
                if (incluirProveedores) tiposPersonas.Add("P");

                if (tiposPersonas.Count > 0)
                {
                    listPara.AddRange(dbContext.Personas.Where(p => p.IDUsuario == idUsuario && p.Email != "" && tiposPersonas.Contains(p.Tipo) && !listMailsUsuariosExistentes.Contains(p.Email)).Select(p => p.Email));
                }

                MailAddressCollection listTo = new MailAddressCollection();
                foreach (var mail in listPara.Distinct())
                {
                    if (mail != string.Empty)
                    {
                        try
                        {
                            listTo.Add(new MailAddress(mail));
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<NOTIFICACION>", mensaje);
                replacements.Add("<LINK>", "https://app.contabilium.com/landings/referidos?ref=CODE-" + idUsuario);
                
                bool send = EmailCommon.SendMessage(idUsuario, EmailTemplate.EnvioInvitacionReferidos, replacements, listTo, "", null, asunto, null, displayFrom);
                if (send)
                {
                    //listTo.ForEach(
                    //    m =>
                    //        dbContext.UsuariosReferidos.Add(new UsuariosReferidos
                    //        {
                    //            Email = m.Address,
                    //            FechaEnvio = DateTime.Now,
                    //            Estado = "Pendiente",
                    //            IDUsuario = idUsuario
                    //        }));
                    //dbContext.SaveChanges();
                }
                else
                {
                    throw new Exception(
                        "El mensaje no pudo ser enviado. Por favor, intente nuevamente. En caso de continuar el error, escribenos a <a href='" +
                        ConfigurationManager.AppSettings["Email.Ayuda"] + "'>" +
                        ConfigurationManager.AppSettings["Email.Ayuda"] + "</a>");
                }
            }
        }
    }
}

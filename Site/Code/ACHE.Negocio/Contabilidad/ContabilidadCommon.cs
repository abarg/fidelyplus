﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Productos;
using ACHE.Model.Negocio;
using ACHE.Negocio.Helpers;
using RestSharp.Extensions;
using ACHE.Extensions;
using System.Data.Entity;
using System.Data;
using System.Web;
using System.Configuration;
using System.IO;

namespace ACHE.Negocio.Contabilidad
{
    public static class ContabilidadCommon
    {
        public const int BANCO_MP = 80;

        #region Agregar

        public static void AgregarAsientoDeCompra(int idCompra, WebUser user)
        {
            try
            {
                if (user.UsaPlanCorporativo)//Plan Corporativo
                {
                    //Determinamos cantidad de decimales que posee configurado el usuario
                    //var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(user.IDUsuario);

                    using (var dbContext = new ACHEEntities())
                    {
                        Asientos asiento;
                        var compra = dbContext.Compras.Where(x => x.IDCompra == idCompra && x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDCompra == idCompra && x.IDUsuario == user.IDUsuario).FirstOrDefault();

                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = compra.Fecha.Date;// DateTime.Now.Date;
                            asiento.Leyenda = "Compra - Factura " + compra.Tipo + " " + compra.NroFactura;
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = compra.Fecha.Date;
                            asiento.IDUsuario = user.IDUsuario;
                            asiento.Leyenda = "Compra - Factura " + compra.Tipo + " " + compra.NroFactura;
                            asiento.IDCompra = idCompra;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false;
                            asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        var Debe = (compra.Tipo != "NCA" && compra.Tipo != "NCB" && compra.Tipo != "NCC" && compra.Tipo != "NCM" && compra.Tipo != "NCE") ? "D" : "H";
                        var Haber = (compra.Tipo != "NCA" && compra.Tipo != "NCB" && compra.Tipo != "NCC" && compra.Tipo != "NCM" && compra.Tipo != "NCE") ? "H" : "D";

                        // cta que selecciona el usuario
                        //var total = Convert.ToDecimal(compra.Total) + compra.ImpInterno + compra.ImpMunicipal + compra.ImpNacional + compra.Otros;
                        //decimal totalProveedores = 0;
                        //if (!compra.Detallado)
                        //    totalProveedores = (total + compra.IIBB + compra.PercepcionIVA + compra.Iva + compra.NoGravado);
                        //else
                        //    totalProveedores = (total + compra.IIBB + compra.PercepcionIVA + compra.Iva);

                        //--------------------

                        decimal total = 0;
                        decimal totalIVA = 0;
                        if (!compra.Detallado)
                        {
                            total = Convert.ToDecimal(compra.Total);
                            totalIVA = Convert.ToDecimal(compra.Iva);
                        }
                        else
                        {

                            foreach (var item in compra.ComprasDetalle)
                            {
                                //total = total + (item.Cantidad * item.PrecioUnitario);
                                total = total + item.Cantidad * (item.PrecioUnitario - ((item.PrecioUnitario * item.Bonificacion) / 100));

                                if (item.Iva > 0)
                                {
                                    if (item.Bonificacion > 0)
                                    {
                                        decimal aux = item.PrecioUnitario;
                                        aux = aux - ((aux * item.Bonificacion) / 100);
                                        aux = aux * item.Cantidad;

                                        totalIVA += (aux * item.Iva) / 100;
                                    }
                                    else
                                        totalIVA += (item.Cantidad * item.PrecioUnitario * item.Iva) / 100;

                                }
                            }
                            //total = total + compra.ImpInterno + compra.ImpMunicipal + compra.ImpNacional + compra.Otros;
                        }

                        //totalProveedores = 0;
                        //if (!compra.Detallado)
                        //total = total + compra.ImpInterno + compra.ImpMunicipal + compra.ImpNacional + compra.Otros;
                        decimal totalProveedores = (total + compra.IIBB + compra.PercepcionIVA + totalIVA + compra.ImpMunicipal + compra.ImpNacional + compra.ImpInterno + compra.Otros);// + compra.NoGravado);
                        //else
                        //    totalProveedores = (total + compra.IIBB + compra.PercepcionIVA + totalIVA);

                        //----------------------------

                        if (!compra.Detallado)
                        {
                            if (compra.IDPlanDeCuenta != null)
                            {
                                var aux = total - compra.NoGravado;
                                if (aux > 0)
                                    dbContext.AsientoDetalle.Add(insertarCuenta(aux, compra.IDPlanDeCuenta.Value, asiento, Debe));
                            }
                            // cta IVA credito fiscal
                            if (totalIVA > 0)
                                dbContext.AsientoDetalle.Add(insertarCuenta(totalIVA, config.IDCtaIVACreditoFiscal, asiento, Debe));
                        }
                        else
                        {
                            foreach (var item in compra.ComprasDetalle)
                            {
                                if (item.IDPlanDeCuenta != null)
                                    // dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(item.Cantidad * (item.PrecioUnitario - ((item.PrecioUnitario * item.Bonificacion) / 100)), cantidadDecimales), item.IDPlanDeCuenta.Value, asiento, Debe));
                                    dbContext.AsientoDetalle.Add(insertarCuenta(item.Cantidad * (item.PrecioUnitario - ((item.PrecioUnitario * item.Bonificacion) / 100)), item.IDPlanDeCuenta.Value, asiento, Debe));

                                // cta IVA credito fiscal
                                if (item.Iva > 0)
                                    // dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(item.Cantidad * (((item.PrecioUnitario - ((item.PrecioUnitario * item.Bonificacion) / 100)) * (item.Iva)) / 100), cantidadDecimales), config.IDCtaIVACreditoFiscal, asiento, Debe));
                                    dbContext.AsientoDetalle.Add(insertarCuenta(item.Cantidad * (((item.PrecioUnitario - ((item.PrecioUnitario * item.Bonificacion) / 100)) * (item.Iva)) / 100), config.IDCtaIVACreditoFiscal, asiento, Debe));

                                //else
                                //    dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(item.Cantidad * (((item.PrecioUnitario - ((item.PrecioUnitario * item.Bonificacion) / 100)) * (item.Iva)) / 100), cantidadDecimales), item.IDPlanDeCuenta.Value, asiento, Debe));
                            }
                        }
                        // cta Percepcion IVA
                        if (compra.PercepcionIVA > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(compra.PercepcionIVA, config.IDCtaPercepcionIVASufrida, asiento, Debe));
                        // cta IIBB
                        if (compra.IIBB > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(compra.IIBB, config.IDCtaIIBBSufrida, asiento, Debe));

                        // cta No gravado
                        if (!compra.Detallado && compra.NoGravado > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(compra.NoGravado, config.IDCtaConceptosNoGravadosxCompras, asiento, Debe));

                        if (compra.ImpMunicipal > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(compra.ImpMunicipal, config.IDCtaImpSellos, asiento, Debe));
                        if (compra.ImpNacional > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(compra.ImpNacional, config.IDCtaImpSellos, asiento, Debe));
                        if (compra.ImpInterno > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(compra.ImpInterno, config.IDCtaImpSellos, asiento, Debe));
                        if (compra.Otros > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(compra.Otros, config.IDCtaImpSellos, asiento, Debe));

                        // cta proveedores
                        dbContext.AsientoDetalle.Add(insertarCuenta(totalProveedores, config.IDCtaProveedores, asiento, Haber));

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoDePago(WebUser user, int idPago)
        {
            try
            {
                if (user.UsaPlanCorporativo)//Plan Corporativo
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        Asientos asiento;
                        asiento = dbContext.Asientos.Where(x => x.IDPago == idPago && x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        var pago = dbContext.Pagos.Where(x => x.IDPago == idPago && x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();

                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = pago.FechaPago.Date;
                            var nombreComprobantes = string.Empty;
                            foreach (var item in pago.PagosDetalle)
                            {
                                if (item.IDCompra.HasValue)
                                    nombreComprobantes += item.Compras.Tipo + " " + item.Compras.NroFactura + ", ";
                                else
                                    nombreComprobantes += "Pago a cuenta";
                            }

                            var leyenda = "Pago de los comprobantes: " + nombreComprobantes;
                            if (leyenda.Length > 150)
                                asiento.Leyenda = "Pago de " + pago.PagosDetalle.Count() + " comprobantes";
                            else
                                asiento.Leyenda = leyenda;
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = pago.FechaPago.Date;
                            asiento.IDUsuario = user.IDUsuario;

                            var nombreComprobantes = string.Empty;
                            foreach (var item in pago.PagosDetalle)
                            {
                                if (item.IDCompra.HasValue)
                                    nombreComprobantes += item.Compras.Tipo + " " + item.Compras.NroFactura + ", ";
                                else
                                    nombreComprobantes += "Pago a cuenta";
                            }

                            var leyenda = "Pago de los comprobantes: " + nombreComprobantes;
                            if (leyenda.Length > 150)
                                asiento.Leyenda = "Pago de " + pago.PagosDetalle.Count() + " comprobantes";
                            else
                                asiento.Leyenda = leyenda;

                            asiento.IDPago = idPago;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        decimal totalNc = 0;

                        foreach (var item in pago.PagosFormasDePago)
                        {
                            //Cta banco que el usuario elija
                            if (item.FormaDePago == "Cheque Propio" || item.FormaDePago == "Depósito" || item.FormaDePago == "Débito" || item.FormaDePago == "Transferencia" || item.FormaDePago == "Mercado Pago" || item.FormaDePago == "PayU")
                            {
                                if (item.FormaDePago == "Cheque Propio")
                                    dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaChequesDiferidosAPagar, asiento, "H"));
                                else
                                {
                                    if (item.IDBanco == null && item.FormaDePago == "Mercado Pago")//Validacion por las dudas
                                        item.IDBanco = BANCO_MP;

                                    if (item.IDBanco == null)
                                        throw new CustomException("La cuenta banco no se encuentra vinculada a ninguna cuenta.");
                                    var bancoPlan = item.Bancos.BancosPlanDeCuenta.Where(x => x.IDBanco == item.IDBanco && x.IDUsuario == user.IDUsuario).FirstOrDefault();//.IDPlanDeCuenta;
                                    if (bancoPlan != null)
                                        dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, bancoPlan.IDPlanDeCuenta, asiento, "H"));
                                    else
                                        throw new CustomException("No se encuentra creada la cuenta contable del banco. Notifiquelo a soporte");

                                    //dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, idCuenta, asiento, "H"));
                                }

                            }
                            //Cta Valores a depositar
                            if (item.FormaDePago == "Cheque de Terceros")
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaValoresADepositar, asiento, "H"));
                            //Cta Caja
                            if (item.FormaDePago == "Efectivo")
                            {
                                var idCuentaCaja = item.Cajas.CajasPlanDeCuenta.Where(x => x.IDCaja == item.IDCaja && x.IDUsuario == user.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, idCuentaCaja, asiento, "H"));
                            }

                            if (item.FormaDePago == "Nota de credito")
                                totalNc += item.Importe;
                        }

                        var totalACuenta = pago.PagosDetalle.Where(x => !x.IDCompra.HasValue).Sum(x => x.Importe);
                        var total = pago.PagosDetalle.Sum(x => x.Importe) - totalNc - totalACuenta;// +pago.PagosRetenciones.Sum(x => x.Importe) - totalNc;
                        if (total > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(total, config.IDCtaProveedores, asiento, "D"));
                        if (totalACuenta > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(totalACuenta, config.IDCtaAnticipoProveedores, asiento, "D"));

                        foreach (var item in pago.PagosRetenciones)
                        {
                            //Cta Ganancias
                            if (item.Tipo == "Ganancias")
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDctaRetGanancias, asiento, "H"));
                            //Cta IIBB
                            if (item.Tipo == "IIBB")
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDctaRetIIBB, asiento, "H"));
                            //Cta IVA
                            if (item.Tipo == "IVA")
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDctaRetIVA, asiento, "H"));

                            if (item.Tipo == "SUSS")
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaRetSUSS, asiento, "H"));
                        }

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new CustomException("Pago generado correctamente, pero los asientos contables asociados a este comprobante no pudieron ser guardados");
                    }
                }
            }
            catch (CustomException ex)
            {
                throw new CustomException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoDeVentas(WebUser usu, int idComprobante)
        {
            try
            {
                if (usu.UsaPlanCorporativo)//Plan Corporativo
                {
                    // Determinamos cantidad de decimales que posee configurado el usuario
                    // var cantidadDecimales = ACHE.Negocio.Common.UsuarioCommon.ObtenerCantidadDecimales(usu.IDUsuario);

                    using (var dbContext = new ACHEEntities())
                    {
                        Asientos asiento;
                        var Venta = dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == idComprobante && x.Comprobantes.IDUsuario == usu.IDUsuario).ToList();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDComprobante == idComprobante && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                        var comprobante = Venta.First().Comprobantes;

                        var cuentaMercaderia = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.Nombre == "Mercaderías" && x.TipoDeCuenta == "Activo").FirstOrDefault();

                        //TODO: MEJORAR ESTO
                        if (Venta.Any(x => !x.IDPlanDeCuenta.HasValue))
                        {
                            var cuentaBase = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.Nombre == "Ingresos Por Servicios").FirstOrDefault();
                            if (cuentaBase != null)
                            {
                                foreach (var item in Venta.Where(x => !x.IDPlanDeCuenta.HasValue))
                                {
                                    item.IDPlanDeCuenta = cuentaBase.IDPlanDeCuenta;
                                }
                            }
                        }


                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = comprobante.FechaComprobante.Date;
                            asiento.Leyenda = "Venta - Factura " + comprobante.Tipo + " " + comprobante.PuntosDeVenta.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = comprobante.FechaComprobante.Date;
                            asiento.IDUsuario = usu.IDUsuario;
                            asiento.Leyenda = "Venta - Factura " + comprobante.Tipo + " " + comprobante.PuntosDeVenta.Punto.ToString("#0000") + "-" + comprobante.Numero.ToString("#00000000");
                            asiento.IDComprobante = idComprobante;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        var Debe = (comprobante.Tipo != "NCA" && comprobante.Tipo != "NCB" && comprobante.Tipo != "NCC" && comprobante.Tipo != "NCE") ? "D" : "H";
                        var Haber = (comprobante.Tipo != "NCA" && comprobante.Tipo != "NCB" && comprobante.Tipo != "NCC" && comprobante.Tipo != "NCE") ? "H" : "D";



                        // cta que de IVA debito fiscal
                        decimal totalIVA = comprobante.ImporteTotalNeto - comprobante.ImporteTotalBruto;
                        /*decimal totalIVA = 0;
                        foreach (var item in Venta)
                        {
                            if (item.Iva > 0)
                            {
                                if (item.Bonificacion > 0)
                                {
                                    decimal aux = item.PrecioUnitario;
                                    aux = aux - ((aux * item.Bonificacion) / 100);
                                    aux = aux * item.Cantidad;

                                    totalIVA += (aux * item.Iva) / 100;
                                }
                                else
                                    totalIVA += (item.Cantidad * item.PrecioUnitario * item.Iva) / 100;

                            }
                        }*/
                        //dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(totalIVA, cantidadDecimales), config.IDCtaIVADebitoFiscal, asiento, Haber));
                        if (totalIVA > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(totalIVA, config.IDCtaIVADebitoFiscal, asiento, Haber));

                        decimal totalPercepcionesIVA = 0;
                        decimal totalPercepcionesIIBB = 0;
                        decimal totalNoGravado = 0;
                        // cta que de percepcion IVA
                        if (comprobante.PercepcionIVA > 0)
                        {
                            totalPercepcionesIVA = (comprobante.PercepcionIVA * comprobante.ImporteTotalBruto) / 100;
                            // dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(totalPercepcionesIVA, cantidadDecimales), config.IDCtaPercepcionIVA, asiento, Haber));
                            dbContext.AsientoDetalle.Add(insertarCuenta(totalPercepcionesIVA, config.IDCtaPercepcionIVA, asiento, Haber));

                        }
                        // cta que de percepcion IIBB
                        if (comprobante.PercepcionIIBB > 0)
                        {
                            totalPercepcionesIIBB = (comprobante.PercepcionIIBB * comprobante.ImporteTotalBruto) / 100;
                            // dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(totalPercepcionesIIBB, cantidadDecimales), config.IDCtaIIBB, asiento, Haber));
                            dbContext.AsientoDetalle.Add(insertarCuenta(totalPercepcionesIIBB, config.IDCtaIIBB, asiento, Haber));

                        }
                        // cta que de No gravado
                        if (comprobante.ImporteNoGrabado > 0)
                        {
                            totalNoGravado = comprobante.ImporteNoGrabado;
                            // dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(comprobante.ImporteNoGrabado, cantidadDecimales), config.IDCtaConceptosNoGravadosxVentas, asiento, Haber));
                            dbContext.AsientoDetalle.Add(insertarCuenta(totalNoGravado, config.IDCtaConceptosNoGravadosxVentas, asiento, Haber));

                        }
                        decimal totalImportes = 0;
                        decimal totalImportesSinDec = 0;
                        decimal cmv = 0;

                        foreach (var cuenta in Venta.Where(x => x.IDPlanDeCuenta.HasValue).GroupBy(x => x.IDPlanDeCuenta))
                        {
                            // cta que selecciona el usuario
                            //if (item.FirstOrDefault().IDPlanDeCuenta != null)
                            //{ }
                            int idPlanCuenta = cuenta.First().IDPlanDeCuenta.Value;
                            decimal importe = 0;
                            foreach (var item in Venta.Where(x => x.IDPlanDeCuenta == idPlanCuenta))
                            {
                                decimal importeItem = item.PrecioUnitario;
                                if (item.Bonificacion > 0)
                                    importeItem = importeItem - ((importeItem * item.Bonificacion) / 100);

                                importeItem = importeItem * item.Cantidad;

                                importe += importeItem;

                                if (item.IDConcepto.HasValue)
                                {
                                    if (item.Conceptos.Tipo == "P")
                                    {
                                        var sql = "select dbo.ObtenerPrecioMedioPonderado(" + item.IDConcepto + ",'" + comprobante.FechaComprobante.ToString("yyyy-MM-dd") + "')";
                                        var precioMedioPonderado = dbContext.Database.SqlQuery<Decimal>(sql, new object[] { }).FirstOrDefault();
                                        cmv += precioMedioPonderado * item.Cantidad;
                                    }
                                    else if (item.Conceptos.Tipo == "C")
                                    {
                                        var sql = "select dbo.ObtenerPrecioMedioPonderadoCombo(" + item.IDConcepto + ",'" + comprobante.FechaComprobante.ToString("yyyy-MM-dd") + "')";
                                        var precioMedioPonderado = dbContext.Database.SqlQuery<Decimal>(sql, new object[] { }).FirstOrDefault();
                                        cmv += precioMedioPonderado * item.Cantidad;
                                    }
                                }
                            }
                            // dbContext.AsientoDetalle.Add(insertarCuenta(Math.Round(importe, cantidadDecimales), idPlanCuenta, asiento, Haber));
                            dbContext.AsientoDetalle.Add(insertarCuenta(importe, idPlanCuenta, asiento, Haber));
                            totalImportes += importe;// Math.Round(importe, 2);
                            totalImportesSinDec += importe;
                        }

                        //CMV
                        if (cmv > 0 && cuentaMercaderia != null)//si hay cuenta de mercaderia
                        {
                            dbContext.AsientoDetalle.Add(insertarCuenta(cmv, config.IDCtaCMV, asiento, Debe));
                            dbContext.AsientoDetalle.Add(insertarCuenta(cmv, cuentaMercaderia.IDPlanDeCuenta, asiento, Haber));
                        }

                        //Ver ejemplo con FC: ID 641760 

                        //Cuenta de deudores por venta
                        //decimal totalDebe = Math.Round(totalImportesSinDec, 2) + totalIVA + totalPercepcionesIVA + totalPercepcionesIIBB + totalNoGravado;
                        decimal totalDebe = totalImportes + totalIVA + totalPercepcionesIVA + totalPercepcionesIIBB + totalNoGravado;
                        dbContext.AsientoDetalle.Add(insertarCuenta(totalDebe, config.IDCtaDeudoresPorVentas, asiento, Debe));


                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new CustomException("Factura generada correctamente, pero los asientos contables asociados a este comprobante no pudieron ser guardados ya que no coinciden el Debe y el Haber");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoDeCobranza(WebUser user, int idCobranza)
        {
            try
            {
                if (user.UsaPlanCorporativo)//Plan Corporativo
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        Asientos asiento;
                        asiento = dbContext.Asientos.Where(x => x.IDCobranza == idCobranza && x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        var cobranza = dbContext.Cobranzas.Where(x => x.IDCobranza == idCobranza && x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = cobranza.FechaCobranza.Date;
                            var nombreComprobantes = string.Empty;
                            foreach (var item in cobranza.CobranzasDetalle)
                            {
                                if (item.IDComprobante.HasValue)
                                    nombreComprobantes += item.Comprobantes.Tipo + " " + item.Comprobantes.Numero.ToString("#00000000") + ", ";
                                else
                                    nombreComprobantes += "Cobranza a cuenta";
                            }

                            var leyenda = "Cobranza de los comprobantes: " + nombreComprobantes;
                            if (leyenda.Length > 150)
                                asiento.Leyenda = "Cobranza de " + cobranza.CobranzasDetalle.Count() + " comprobantes";
                            else
                                asiento.Leyenda = leyenda;
                            //asiento.Leyenda = "Cobranza de los comprobantes : " + nombreComprobantes;
                            //if (nombreComprobantes.Length > 150)
                            //    asiento.Leyenda = "Cobranza de " + cobranza.CobranzasDetalle.Count() + " comprobantes";
                            //else
                            //    asiento.Leyenda = "Cobranza de los comprobantes: " + nombreComprobantes;
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = cobranza.FechaCobranza.Date;
                            asiento.IDUsuario = user.IDUsuario;

                            var nombreComprobantes = string.Empty;
                            foreach (var item in cobranza.CobranzasDetalle)
                            {
                                if (item.IDComprobante.HasValue)
                                    nombreComprobantes += item.Comprobantes.Tipo + " " + item.Comprobantes.Numero.ToString("#00000000") + ", ";
                                else
                                    nombreComprobantes += "Cobranza a cuenta";
                            }

                            var leyenda = "Cobranza de los comprobantes: " + nombreComprobantes;
                            if (leyenda.Length > 150)
                                asiento.Leyenda = "Cobranza de " + cobranza.CobranzasDetalle.Count() + " comprobantes";
                            else
                                asiento.Leyenda = leyenda;

                            asiento.IDCobranza = idCobranza;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        decimal totalNc = 0;

                        foreach (var item in cobranza.CobranzasFormasDePago)
                        {
                            //Cta banco que el usuario elija
                            //if (item.FormaDePago == "Cheque Propio" || item.FormaDePago == "Débito" || item.FormaDePago == "Transferencia" || item.FormaDePago == "Mercado Pago")
                            if (item.FormaDePago == "Depósito" || item.FormaDePago == "Débito" || item.FormaDePago == "Transferencia" || item.FormaDePago == "Mercado Pago" || item.FormaDePago == "PayU")
                            {
                                var bancoPlan = item.Bancos.BancosPlanDeCuenta.Where(x => x.IDBanco == item.IDBanco && x.IDUsuario == user.IDUsuario).FirstOrDefault();//.IDPlanDeCuenta;
                                if (bancoPlan != null)
                                    dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, bancoPlan.IDPlanDeCuenta, asiento, "D"));
                                else
                                    throw new CustomException("No se encuentra creada la cuenta contable del banco. Notifiquelo a soporte");
                            }
                            else if (item.FormaDePago == "Cheque")//Cta Valores a depositar
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaValoresADepositar, asiento, "D"));
                            else if (item.FormaDePago == "Efectivo")
                            {//Cta Caja, item.FormaDePago == "Depósito" || 
                                var idCuentaCaja = item.Cajas.CajasPlanDeCuenta.Where(x => x.IDCaja == item.IDCaja && x.IDUsuario == user.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, idCuentaCaja, asiento, "D"));
                                //dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaCaja, asiento, "D"));
                            }
                            if (item.FormaDePago == "Nota de credito")
                            {
                                totalNc += item.Importe;
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaDeudoresPorVentas, asiento, "D"));
                            }
                        }
                        foreach (var item in cobranza.CobranzasRetenciones)
                        {

                            if (item.Tipo == "Ganancias")//Cta Ganancias
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaRetGananciasSufrida, asiento, "D"));
                            else if (item.Tipo == "IIBB")//Cta IIBB
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaRetIIBBSufrida, asiento, "D"));
                            else if (item.Tipo == "IVA")//Cta IVA
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaRetIVASufrida, asiento, "D"));
                            else if (item.Tipo == "SUSS")//Cta SUSS
                                dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaRetSUSSSufrida, asiento, "D"));
                        }

                        var totalACuenta = cobranza.CobranzasDetalle.Where(x => !x.IDComprobante.HasValue).Sum(x => x.Importe);
                        var total = cobranza.ImporteTotal - totalACuenta;// CobranzasFormasDePago.Sum(x => x.Importe) + cobranza.CobranzasRetenciones.Sum(x => x.Importe);

                        if (total > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(total, config.IDCtaDeudoresPorVentas, asiento, "H"));
                        if (totalACuenta > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(totalACuenta, config.IDCtaAnticipoCliente, asiento, "H"));
                        ////Pagos a cuenta
                        //foreach (var item in cobranza.CobranzasDetalle.Where(x => !x.IDComprobante.HasValue))
                        //{
                        //    dbContext.AsientoDetalle.Add(insertarCuenta(item.Importe, config.IDCtaDeudoresPorVentas, asiento, "H"));

                        //}

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new CustomException("Cobranza generada correctamente, pero los asientos contables asociados a este comprobante no pudieron ser guardados ya que no coinciden el Debe y el Haber");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoDeGastoBancario(int idGastosBancarios, WebUser usu)
        {
            try
            {
                if (usu.UsaPlanCorporativo)//Plan Corporativo
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        Asientos asiento;
                        var gastoBancario = dbContext.GastosBancarios.Where(x => x.IDGastosBancarios == idGastosBancarios && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDGastosBancarios == idGastosBancarios && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = gastoBancario.Fecha.Date;
                            asiento.Leyenda = "Gasto Bancario - " + gastoBancario.Concepto;
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = gastoBancario.Fecha.Date;
                            asiento.IDUsuario = usu.IDUsuario;
                            asiento.Leyenda = "Gasto Bancario - " + gastoBancario.Concepto;
                            asiento.IDGastosBancarios = idGastosBancarios;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        // cta Gastos bancarios
                        //23.06.2016 - Saco esto cuenta y detallo todo. dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.TotalImportes), config.IDCtaGastosBancarios, asiento, "D"));

                        if (gastoBancario.CreditoComputable > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.CreditoComputable), config.IDCtaSircreb, asiento, "D"));
                        if ((gastoBancario.Importe10 + gastoBancario.Importe21) > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal((gastoBancario.Importe10 + gastoBancario.Importe21)), config.IDCtaComisiones, asiento, "D"));
                        if (gastoBancario.Importe > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.Importe), config.IDCtaGastosBancarios, asiento, "D"));
                        if (gastoBancario.Otros > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.Otros), config.IDCtaOtrosGastosBancarios, asiento, "D"));
                        if (gastoBancario.SIRCREB > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.SIRCREB), config.IDCtaSircreb, asiento, "D"));
                        if (gastoBancario.IIBB > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.IIBB), config.IDctaRetIIBB, asiento, "D"));
                        if (gastoBancario.IVA > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.IVA), config.IDCtaIVACreditoFiscal, asiento, "D"));
                        if (gastoBancario.PercepcionIVA > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.PercepcionIVA), config.IDCtaPercepcionIVA, asiento, "D"));
                        if ((gastoBancario.Debito + gastoBancario.Credito) > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal((gastoBancario.Debito + gastoBancario.Credito)), config.IDCtaImpDebCred, asiento, "D"));
                        if (gastoBancario.Intereses > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.Intereses), config.IDCtaInteresBancos, asiento, "D"));
                        if (gastoBancario.ImpuestosYSellos > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.ImpuestosYSellos), config.IDCtaImpSellos, asiento, "D"));

                        // cta banco que selecciono el usuario
                        var bancosPlanDeCuenta = gastoBancario.Bancos.BancosPlanDeCuenta.FirstOrDefault();
                        if (bancosPlanDeCuenta != null)
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(gastoBancario.TotalImportes), bancosPlanDeCuenta.IDPlanDeCuenta, asiento, "H"));

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new CustomException("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoChequeAccion(WebUser user, int idChequeAccion)
        {
            try
            {
                if (user.UsaPlanCorporativo)//Plan Corporativo
                {
                    using (var dbContext = new ACHEEntities())
                    {
                        Asientos asiento;
                        var chequeAccion = dbContext.ChequeAccion.Where(x => x.IDChequeAccion == idChequeAccion && x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == user.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDCheque == chequeAccion.IDCheque && x.IDUsuario == user.IDUsuario).FirstOrDefault();

                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = chequeAccion.Fecha.Date;
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = chequeAccion.Fecha.Date;
                            asiento.IDUsuario = user.IDUsuario;
                            asiento.IDCheque = chequeAccion.IDCheque;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        if (chequeAccion.Accion == "Depositado")
                            asiento.Leyenda = "Depósito del cheque nro: " + chequeAccion.Cheques.Numero.ToString();
                        else if (chequeAccion.Accion == "Ventanilla")
                            asiento.Leyenda = "Cobro por ventanilla del cheque nro: " + chequeAccion.Cheques.Numero.ToString();
                        else if (chequeAccion.Accion == "Debitado" && chequeAccion.Cheques.EsPropio)
                            asiento.Leyenda = "Debito del cheque nro: " + chequeAccion.Cheques.Numero.ToString();

                        int idBancoAccion = 0;
                        int idCuenta = 0;
                        switch (chequeAccion.Accion)
                        {
                            case "Rechazado":
                                break;
                            case "Depositado":
                                idBancoAccion = dbContext.ChequeAccion.Where(x => x.IDCheque == chequeAccion.IDCheque && x.Accion == "Depositado").FirstOrDefault().IDBanco.Value;

                                idCuenta = dbContext.BancosPlanDeCuenta.Where(x => x.IDBanco == idBancoAccion && x.IDUsuario == user.IDUsuario).FirstOrDefault().IDPlanDeCuenta;

                                if (!chequeAccion.Cheques.EsPropio)
                                {
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, config.IDCtaValoresADepositar, asiento, "H"));
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, idCuenta, asiento, "D"));
                                }
                                else
                                {
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, config.IDCtaChequesDiferidosAPagar, asiento, "D"));
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, idCuenta, asiento, "H"));
                                }

                                break;
                            case "Acreditado":
                                break;
                            case "Debitado":
                                if (!chequeAccion.Cheques.EsPropio)
                                {
                                    idBancoAccion = dbContext.ChequeAccion.Where(x => x.IDCheque == chequeAccion.IDCheque && x.Accion == "Depositado").FirstOrDefault().IDBanco.Value;
                                    idCuenta = dbContext.BancosPlanDeCuenta.Where(x => x.IDBanco == idBancoAccion && x.IDUsuario == user.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, idCuenta, asiento, "H"));
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, config.IDCtaChequesDiferidosAPagar, asiento, "D"));
                                }
                                else
                                {
                                    idBancoAccion = dbContext.ChequeAccion.Where(x => x.IDCheque == chequeAccion.IDCheque && x.Accion == "Debitado").FirstOrDefault().IDBanco.Value;
                                    idCuenta = dbContext.BancosPlanDeCuenta.Where(x => x.IDBanco == idBancoAccion && x.IDUsuario == user.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, idCuenta, asiento, "H"));
                                    dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, config.IDCtaChequesDiferidosAPagar, asiento, "D"));
                                }
                                break;
                            case "Ventanilla":
                                var idCuentaCaja = dbContext.CajasPlanDeCuenta.Where(x => x.IDCaja == chequeAccion.IDCaja && x.IDUsuario == user.IDUsuario).FirstOrDefault().IDPlanDeCuenta;

                                //dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, config.IDCtaCaja, asiento, "D"));
                                //dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, config.IDCtaValoresADepositar, asiento, "H"));

                                dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, idCuentaCaja, asiento, "D"));
                                dbContext.AsientoDetalle.Add(insertarCuenta(chequeAccion.Cheques.Importe, config.IDCtaValoresADepositar, asiento, "H"));
                                break;

                        }

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoManual(List<AsientosManualesViewModel> listaAsientos, string leyenda, string fecha, int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    Asientos entity;
                    if (id > 0)
                    {
                        entity = dbContext.Asientos.Where(x => x.IDAsiento == id && x.IDUsuario == idUsuario).FirstOrDefault();

                        var lAsientos = entity.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                        var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                        foreach (var item in asientosPartes)
                            dbContext.AsientoDetalle.Remove(item);
                    }
                    else
                    {
                        entity = new Asientos();
                        entity.IDUsuario = idUsuario;
                        entity.EsManual = true;
                    }
                    entity.Fecha = Convert.ToDateTime(fecha);
                    entity.Leyenda = leyenda;

                    if (id == 0)
                        dbContext.Asientos.Add(entity);

                    foreach (var item in listaAsientos)
                    {
                        if (Convert.ToDecimal(item.Debe) > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(item.Debe, item.IDPlanDeCuenta, entity, "D"));
                        else
                            dbContext.AsientoDetalle.Add(insertarCuenta(item.Haber, item.IDPlanDeCuenta, entity, "H"));
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoModelo(List<AsientosManualesViewModel> listaAsientos, string leyenda, int id, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    AsientosModelo entity;
                    if (id > 0)
                    {
                        entity = dbContext.AsientosModelo.Where(x => x.IDAsientoModelo == id && x.IDUsuario == idUsuario).FirstOrDefault();

                        var lAsientos = entity.AsientoModeloDetalle.ToList().Select(x => x.IDAsientoModelo);
                        var asientosPartes = dbContext.AsientoModeloDetalle.Where(x => lAsientos.Contains(x.IDAsientoModelo)).ToList();
                        foreach (var item in asientosPartes)
                            dbContext.AsientoModeloDetalle.Remove(item);
                    }
                    else
                    {
                        entity = new AsientosModelo();
                        entity.IDUsuario = idUsuario;
                    }
                    entity.Leyenda = leyenda;

                    if (id == 0)
                        dbContext.AsientosModelo.Add(entity);

                    foreach (var item in listaAsientos)
                    {
                        if (Convert.ToDecimal(item.Debe) > 0)
                            dbContext.AsientoModeloDetalle.Add(insertarCuentaModelo(Convert.ToDecimal(item.Debe), item.IDPlanDeCuenta, entity, "D"));
                        else
                            dbContext.AsientoModeloDetalle.Add(insertarCuentaModelo(Convert.ToDecimal(item.Haber), item.IDPlanDeCuenta, entity, "H"));
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoCierreDelEjercicio(ResultadosLibroMayorViewModel BalanceGeneral, DateTime fechaCierre, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo)//Plan Corporativo
                    {
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        var TieneAsiento = dbContext.Asientos.Where(x => x.EsAsientoCierre && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (TieneAsiento != null)
                        {
                            dbContext.Asientos.Remove(TieneAsiento);
                            dbContext.SaveChanges();
                        }

                        var asiento = new Asientos();
                        asiento.Fecha = fechaCierre.Date;
                        asiento.IDUsuario = usu.IDUsuario;
                        asiento.Leyenda = "Cierre del ejercicio contable fecha: " + fechaCierre.Date.ToString("dd/MM/yyyy");
                        asiento.EsAsientoCierre = true;
                        asiento.EsAsientoInicio = false; asiento.EsManual = false;
                        dbContext.Asientos.Add(asiento);

                        foreach (var item in BalanceGeneral.Asientos)
                        {
                            if (item.TipoDeCuenta.ToUpper() == "ACTIVO" && Convert.ToDecimal(item.TotalActivo) > 0)
                                dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(item.TotalActivo), item.IDPlanDeCuenta, asiento, "H"));
                        }

                        foreach (var item in BalanceGeneral.Asientos)
                        {
                            if (item.TipoDeCuenta.ToUpper() == "RESULTADO" && Convert.ToDecimal(item.TotalPerdidas) > 0 && Convert.ToDecimal(item.TotalGanancias) == 0)
                                dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(item.TotalPerdidas), item.IDPlanDeCuenta, asiento, "H"));
                        }

                        foreach (var item in BalanceGeneral.Asientos)
                        {
                            if (item.TipoDeCuenta.ToUpper() == "PASIVO" && Convert.ToDecimal(item.TotalPasivo) > 0)
                                dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(item.TotalPasivo), item.IDPlanDeCuenta, asiento, "D"));
                        }

                        foreach (var item in BalanceGeneral.Asientos)
                        {
                            if (item.TipoDeCuenta.ToUpper() == "RESULTADO" && Convert.ToDecimal(item.TotalGanancias) > 0 && Convert.ToDecimal(item.TotalPerdidas) == 0)
                                dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(item.TotalGanancias), item.IDPlanDeCuenta, asiento, "D"));
                        }

                        //if (validarPartidaDoble(asiento))
                        dbContext.SaveChanges();
                        //else
                        //    throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoInicioDelEjercicio(DateTime fechaInicio, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo)//Plan Corporativo
                    {
                        var asientoCierre = dbContext.Asientos.Include("AsientoDetalle").Where(x => x.EsAsientoCierre).OrderByDescending(x => x.IDAsiento).FirstOrDefault();

                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        var TieneAsiento = dbContext.Asientos.Where(x => x.EsAsientoInicio && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (TieneAsiento != null)
                        {
                            dbContext.Asientos.Remove(TieneAsiento);
                            dbContext.SaveChanges();
                        }

                        var asiento = new Asientos();
                        asiento.Fecha = fechaInicio.Date;
                        asiento.IDUsuario = usu.IDUsuario;
                        asiento.Leyenda = "Inicio del ejercicio contable fecha: " + fechaInicio.Date.ToString("dd/MM/yyyy");
                        asiento.EsAsientoCierre = false;
                        asiento.EsAsientoInicio = true;

                        dbContext.Asientos.Add(asiento);

                        foreach (var item in asientoCierre.AsientoDetalle)
                        {
                            if (item.PlanDeCuentas.TipoDeCuenta == "Activo")
                                dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(item.Importe), item.IDPlanDeCuenta, asiento, "D"));
                        }

                        foreach (var item in asientoCierre.AsientoDetalle)
                        {
                            if (item.PlanDeCuentas.TipoDeCuenta == "Pasivo")
                                dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(item.Importe), item.IDPlanDeCuenta, asiento, "H"));
                        }

                        var TotalDebe = asiento.AsientoDetalle.Where(x => x.TipoDeAsiento == "D").Sum(x => x.Importe);
                        var TotalHaber = asiento.AsientoDetalle.Where(x => x.TipoDeAsiento == "H").Sum(x => x.Importe);
                        var resultado = TotalDebe - TotalHaber;

                        if (resultado > 0)
                            dbContext.AsientoDetalle.Add(insertarCuenta(resultado, config.IDCtaResultadoEjercicio, asiento, "H"));
                        else
                            dbContext.AsientoDetalle.Add(insertarCuenta(Math.Abs(resultado), config.IDCtaResultadoEjercicio, asiento, "D"));


                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoDeCaja(WebUser usu, int idCaja)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo)//Plan Corporativo
                    {
                        Asientos asiento;
                        var caja = dbContext.Caja.Where(x => x.IDCaja == idCaja && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDCaja == idCaja && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = caja.Fecha.Value.Date;

                            if (caja.ConceptosCaja != null)
                                asiento.Leyenda = "Caja - " + caja.ConceptosCaja.Nombre;
                            else
                                asiento.Leyenda = "Caja - " + caja.TipoMovimiento;
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = caja.Fecha.Value.Date;
                            asiento.IDUsuario = usu.IDUsuario;
                            if (caja.ConceptosCaja != null)
                                asiento.Leyenda = "Caja - " + caja.ConceptosCaja.Nombre;
                            else
                                asiento.Leyenda = "Caja - " + caja.TipoMovimiento;
                            asiento.IDCaja = idCaja;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        var Debe = (caja.TipoMovimiento == "Ingreso") ? "D" : "H";
                        var Haber = (caja.TipoMovimiento == "Ingreso") ? "H" : "D";

                        /*if (caja.TipoMovimiento == "Egreso" && caja.Estado == "Anulado")
                        {
                            Debe = "D";// (caja.TipoMovimiento == "Ingreso") ? "D" : "H";
                            Haber = "H";
                        }*/

                        // cta Caja 
                        int idCuentaCaja = 0;
                        var idCuentaCajaAux = dbContext.CajasPlanDeCuenta.Where(x => x.IDCaja == caja.IDCajas && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        if (idCuentaCajaAux != null)
                            idCuentaCaja = idCuentaCajaAux.IDPlanDeCuenta;
                        else
                            throw new Exception("No se encuentra creada correctamente la cuenta contable de la caja.");

                        dbContext.AsientoDetalle.Add(insertarCuenta(caja.Importe, idCuentaCaja, asiento, Debe));
                        //dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(caja.Importe), config.IDCtaCaja, asiento, Debe));

                        // cta Caja que selecciono el usuario
                        dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(caja.Importe), Convert.ToInt32(caja.IDPlanDeCuenta), asiento, Haber));

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoDeBanco(WebUser usu, int idDetalle)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo)//Plan Corporativo
                    {
                        Asientos asiento;
                        var detalle = dbContext.BancosDetalle.Where(x => x.IDBancoDetalle == idDetalle && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDBancoDetalle == idDetalle && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.IDUsuario = usu.IDUsuario;
                            asiento.IDBancoDetalle = idDetalle;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        asiento.Fecha = detalle.Fecha.Date;
                        if (detalle.Concepto != null)
                            asiento.Leyenda = "Banco - " + detalle.Concepto;
                        else
                            asiento.Leyenda = "Banco - " + detalle.TipoMovimiento;

                        var Debe = (detalle.TipoMovimiento == "Ingreso") ? "D" : "H";
                        var Haber = (detalle.TipoMovimiento == "Ingreso") ? "H" : "D";

                        //cta Banco 
                        var idCuentaBanco = dbContext.BancosPlanDeCuenta.Where(x => x.IDBanco == detalle.IDBanco && x.IDUsuario == usu.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                        dbContext.AsientoDetalle.Add(insertarCuenta(detalle.Importe, idCuentaBanco, asiento, Debe));

                        //cuenta contable que selecciono el usuario
                        dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(detalle.Importe), Convert.ToInt32(detalle.IDPlanDeCuenta), asiento, Haber));

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void AgregarAsientoDeMovDeFondo(int idMovimiento, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo)//Plan Corporativo
                    {
                        Asientos asiento;
                        var movFondo = dbContext.MovimientoDeFondos.Where(x => x.IDMovimientoDeFondo == idMovimiento && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDMovimientoDeFondo == idMovimiento && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                        if (asiento != null)
                        {
                            var lAsientos = asiento.AsientoDetalle.ToList().Select(x => x.IDAsiento);
                            var asientosPartes = dbContext.AsientoDetalle.Where(x => lAsientos.Contains(x.IDAsiento)).ToList();
                            foreach (var item in asientosPartes)
                                dbContext.AsientoDetalle.Remove(item);

                            asiento.Fecha = movFondo.FechaMovimiento.Date;
                        }
                        else
                        {
                            asiento = new Asientos();
                            asiento.Fecha = movFondo.FechaMovimiento.Date;
                            asiento.IDUsuario = usu.IDUsuario;

                            asiento.Leyenda = "Movimiento de fondo";
                            asiento.IDMovimientoDeFondo = idMovimiento;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        if (movFondo.Destino == "BANCO")
                        {
                            var ctaBanco = dbContext.BancosPlanDeCuenta.Where(x => x.IDBanco == movFondo.IDBancoDestino && x.IDUsuario == usu.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(movFondo.Importe), Convert.ToInt32(ctaBanco), asiento, "D"));
                        }
                        else
                        {
                            //dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(movFondo.Importe), Convert.ToInt32(config.IDCtaCaja), asiento, "D"));
                            var idCuentaCaja = dbContext.CajasPlanDeCuenta.Where(x => x.IDCaja == movFondo.IDCajaDestino && x.IDUsuario == usu.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                            dbContext.AsientoDetalle.Add(insertarCuenta(movFondo.Importe, idCuentaCaja, asiento, "D"));
                        }

                        if (movFondo.Origen == "BANCO")
                        {
                            var ctaBanco = dbContext.BancosPlanDeCuenta.Where(x => x.IDBanco == movFondo.IDBancoOrigen && x.IDUsuario == usu.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                            dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(movFondo.Importe), Convert.ToInt32(ctaBanco), asiento, "H"));
                        }
                        else
                        {
                            var idCuentaCaja = dbContext.CajasPlanDeCuenta.Where(x => x.IDCaja == movFondo.IDCajaOrigen && x.IDUsuario == usu.IDUsuario).FirstOrDefault().IDPlanDeCuenta;
                            dbContext.AsientoDetalle.Add(insertarCuenta(movFondo.Importe, idCuentaCaja, asiento, "H"));
                            //dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(movFondo.Importe), Convert.ToInt32(config.IDCtaCaja), asiento, "H"));
                        }


                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Obtener

        public static void ObtenerLibroDiario()
        {

        }

        public static void ObtenerLibroMayor()
        {

        }

        public static ResultadosLibroMayorViewModel ObtenerBalanceDeResultados(ACHEEntities dbContext, int idUsuario,
    string fechaDesde, string fechaHasta, int page, int pageSize)
        {
            try
            {
                var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == idUsuario).AsQueryable();
                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    results = results.Where(x => x.Fecha <= dtHasta);
                }

                //Obtenemos el formato decimal
                var decimalFormat = DecimalFormatter.GetDecimalStringFormat(idUsuario);

                page--;
                ResultadosLibroMayorViewModel resultado = new ResultadosLibroMayorViewModel();
                resultado.TotalPage = ((results.GroupBy(x => x.Codigo).Count() - 1) / pageSize) + 1;
                resultado.TotalItems = results.GroupBy(x => x.Codigo).Count();


                var aux = results.GroupBy(x => x.Codigo).ToList().Skip(page * pageSize).Take(pageSize).ToList();
                //.OrderBy(x => x.FirstOrDefault()Codigo)
                //List<CuentasViewModel> asientos=new List<CuentasViewModel>();
                //if (padresNoIncluidos.Count > 0)
                //    asientos.AddRange(
                //         padresNoIncluidos.Select(
                //             x => new CuentasViewModel()
                //             {
                //                 nroCuenta = x.Codigo,
                //                 NombreCuenta = x.Nombre,
                //                 TipoDeCuenta = x.TipoDeCuenta,
                //                 IDPlanDeCuenta = x.IDPlanDeCuenta,
                //                 IDPadre = x.IDPadre.HasValue ? x.IDPadre.Value.ToString() : "",
                //                 TotalDebe = "0",
                //                 TotalHaber = "0",
                //                 Saldo = "0",
                //                 TotalDeudor = "0",
                //                 TotalAcreedor = "0",

                //                 TotalActivo = "0",
                //                 TotalPasivo = "0",

                //                 TotalPerdidas = "0",
                //                 TotalGanancias = "0",
                //             }
                //             ).ToList());

                resultado.Asientos = aux
                .Select(x => new CuentasViewModel()
                {
                    nroCuenta = x.FirstOrDefault().Codigo,
                    NombreCuenta = x.FirstOrDefault().Nombre,
                    TipoDeCuenta = x.FirstOrDefault().TipoDeCuenta.ToUpper(),
                    IDPlanDeCuenta = x.FirstOrDefault().IDPlanDeCuenta,
                    IDPadre = x.FirstOrDefault().IDPadre.HasValue ? x.FirstOrDefault().IDPadre.Value.ToString() : "",
                    TotalDebe = x.Sum(y => y.Debe).ToString(decimalFormat),
                    TotalHaber = x.Sum(y => y.Haber).ToString(decimalFormat),
                    Saldo = (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat),
                    TotalDeudor =
                        (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) > 0)
                            ? (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",
                    TotalAcreedor =
                        (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) < 0)
                            ? Math.Abs(x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",

                    //TotalActivo = (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "ACTIVO") ? (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat) : "0",
                    //TotalPasivo = (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "PASIVO") ? Math.Abs(x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat) : "0",

                    TotalActivo =
                        (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) > 0 &&
                         (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "ACTIVO" ||
                          x.FirstOrDefault().TipoDeCuenta.ToUpper() == "PASIVO"))
                            ? (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",
                    TotalPasivo =
                        (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) < 0 &&
                         (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "ACTIVO" ||
                          x.FirstOrDefault().TipoDeCuenta.ToUpper() == "PASIVO"))
                            ? Math.Abs((x.Sum(y => y.Debe) - x.Sum(y => y.Haber))).ToString(decimalFormat)
                            : "0",

                    TotalPerdidas =
                        (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "RESULTADO" &&
                         (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) > 0))
                            ? (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",
                    TotalGanancias =
                        (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "RESULTADO" &&
                         (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) < 0))
                            ? Math.Abs(x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",
                }).ToList();


                //resultado.Asientos = asientos.OrderBy(a => a.IDPlanDeCuenta).ThenBy(a => a.IDPadre).ToList();
                //resultado.TotalPage = ((asientos.Count() - 1) / pageSize) + 1;
                //resultado.TotalItems = asientos.Count();
                resultado.TotalDebe = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalDebe)).ToString(decimalFormat);
                resultado.TotalHaber = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalHaber)).ToString(decimalFormat);

                resultado.TotalDeudor = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalDeudor)).ToString(decimalFormat);
                resultado.TotalAcreedor = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalAcreedor)).ToString(decimalFormat);

                resultado.TotalActivo = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalActivo)).ToString(decimalFormat);
                resultado.TotalPasivo = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalPasivo)).ToString(decimalFormat);

                resultado.TotalPerdidas = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalPerdidas)).ToString(decimalFormat);
                resultado.TotalGanancias = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalGanancias)).ToString(decimalFormat);

                return resultado;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static ResultadosBalanceGeneralViewModel ObtenerBalanceGeneral(ACHEEntities dbContext, int idUsuario,
            string fechaDesde, string fechaHasta)
        {
            try
            {
                var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == idUsuario).AsQueryable();
                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                    results = results.Where(x => x.Fecha <= dtHasta);
                }

                //Obtenemos el formato decimal
                var decimalFormat = "N2";// DecimalFormatter.GetDecimalStringFormat(idUsuario);


                ResultadosBalanceGeneralViewModel resultado = new ResultadosBalanceGeneralViewModel();

                List<PlanDeCuentas> padresNoIncluidos = new List<PlanDeCuentas>();
                List<PlanDeCuentas> abuelosNoIncluidos = new List<PlanDeCuentas>();
                var idplanes = new List<int>();
                var idpadres = new List<int>();
                if (results.Any()) //se necesitan todos los padres
                {
                    var res = results.ToList();

                    idplanes = res.Select(r => r.IDPlanDeCuenta).Distinct().ToList();

                    idpadres = res.Where(r => r.IDPadre.HasValue).Select(r => r.IDPadre.Value).Distinct().ToList();

                    var idsPadresNoIncluidos = idpadres.Where(p => !idplanes.Contains(p)).ToList();

                    padresNoIncluidos = dbContext.PlanDeCuentas.Where(p => idsPadresNoIncluidos.Contains(p.IDPlanDeCuenta)).ToList();

                    var idsAbuelosNoIncluidos = padresNoIncluidos.Where(p => p.IDPadre.HasValue && !idplanes.Contains(p.IDPadre.Value)).Select(p => p.IDPadre.Value).ToList();

                    abuelosNoIncluidos = dbContext.PlanDeCuentas.Where(p => idsAbuelosNoIncluidos.Contains(p.IDPlanDeCuenta)).ToList();
                    abuelosNoIncluidos.AddRange(padresNoIncluidos);
                    abuelosNoIncluidos = abuelosNoIncluidos.Distinct().ToList();
                    idpadres.AddRange(idsAbuelosNoIncluidos);
                }
                var aux = results.GroupBy(x => x.Codigo).ToList();

                List<CuentasViewModel> asientos = new List<CuentasViewModel>();
                asientos.AddRange(
                     abuelosNoIncluidos.Select(
                         x => new CuentasViewModel()
                         {
                             MostrarResultado = x.TipoDeCuenta.ToUpper() == "RESULTADO" && x.Codigo.StartsWith("4"),
                             EsPadre = idpadres.Contains(x.IDPlanDeCuenta),
                             nroCuenta = x.Codigo,
                             NombreCuenta = x.Nombre,
                             TipoDeCuenta = x.TipoDeCuenta.ToUpper(),
                             IDPlanDeCuenta = x.IDPlanDeCuenta,
                             IDPadre = x.IDPadre.HasValue ? x.IDPadre.Value.ToString() : "",
                             TotalDebe = "0",
                             TotalHaber = "0",
                             Saldo = "0",
                             TotalDeudor = "0",
                             TotalAcreedor = "0",
                             TotalActivo = "0",
                             TotalPasivo = "0",
                             TotalPN = "0",
                             TotalPerdidas = "0",
                             TotalGanancias = "0",
                         }
                         ).ToList());


                asientos.AddRange(aux
                .Select(x => new CuentasViewModel()
                {
                    MostrarResultado = x.FirstOrDefault().TipoDeCuenta.ToUpper() == "RESULTADO" && x.FirstOrDefault().Codigo.StartsWith("4"),
                    EsPadre = idpadres.Contains(x.FirstOrDefault().IDPlanDeCuenta),
                    nroCuenta = x.FirstOrDefault().Codigo,
                    NombreCuenta = x.FirstOrDefault().Nombre,
                    TipoDeCuenta = x.FirstOrDefault().TipoDeCuenta.ToUpper(),
                    IDPlanDeCuenta = x.FirstOrDefault().IDPlanDeCuenta,
                    IDPadre = x.FirstOrDefault().IDPadre.HasValue ? x.FirstOrDefault().IDPadre.Value.ToString() : "",
                    TotalDebe = x.Sum(y => y.Debe).ToString(decimalFormat),
                    TotalHaber = x.Sum(y => y.Haber).ToString(decimalFormat),
                    Saldo = (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat),
                    TotalDeudor =
                        (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) > 0)
                            ? (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",
                    TotalAcreedor =
                        (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) < 0)
                            ? Math.Abs(x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",

                    TotalActivo =
                         (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) > 0 && !x.FirstOrDefault().Codigo.StartsWith("3.") &&
                          (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "ACTIVO" ||
                           x.FirstOrDefault().TipoDeCuenta.ToUpper() == "PASIVO"))
                             ? (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                             : "0",
                    TotalPasivo =
                        (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) < 0 && !x.FirstOrDefault().Codigo.StartsWith("3.") &&
                         (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "ACTIVO" ||
                          x.FirstOrDefault().TipoDeCuenta.ToUpper() == "PASIVO"))
                            ? Math.Abs((x.Sum(y => y.Debe) - x.Sum(y => y.Haber))).ToString(decimalFormat)
                            : "0",

                    TotalPN =
                (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) > 0 && x.FirstOrDefault().Codigo.StartsWith("3."))
                    ? Math.Abs((x.Sum(y => y.Debe) - x.Sum(y => y.Haber))).ToString(decimalFormat)
                    : "0",

                    TotalPerdidas =
                        (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "RESULTADO" && !x.FirstOrDefault().Codigo.StartsWith("3.") &&
                         (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) > 0))
                            ? (x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",
                    TotalGanancias =
                        (x.FirstOrDefault().TipoDeCuenta.ToUpper() == "RESULTADO" && !x.FirstOrDefault().Codigo.StartsWith("3.") &&
                         (x.Sum(y => y.Debe) - x.Sum(y => y.Haber) < 0))
                            ? Math.Abs(x.Sum(y => y.Debe) - x.Sum(y => y.Haber)).ToString(decimalFormat)
                            : "0",
                }).ToList());


                resultado.Asientos = asientos.OrderBy(a => a.nroCuenta).ToList();
                resultado.TotalItems = resultado.Asientos.Count;
                resultado.TotalDebe = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalDebe)).ToString(decimalFormat);
                resultado.TotalHaber = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalHaber)).ToString(decimalFormat);

                resultado.TotalDeudor = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalDeudor)).ToString(decimalFormat);
                resultado.TotalAcreedor = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalAcreedor)).ToString(decimalFormat);

                resultado.TotalActivo = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalActivo)).ToString(decimalFormat);
                resultado.TotalPasivo = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalPasivo)).ToString(decimalFormat);
                resultado.TotalPN = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalPN)).ToString(decimalFormat);

                resultado.TotalPerdidas = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalPerdidas)).ToString(decimalFormat);
                resultado.TotalGanancias = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalGanancias)).ToString(decimalFormat);

                return resultado;

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static ResultadosLibroMayorViewModel ObtenerBalanceDeResultados(int idUsuario, string fechaDesde, string fechaHasta, int page, int pageSize)
        {
            using (var dbContext = new ACHEEntities())
            {
                return ObtenerBalanceDeResultados(dbContext, idUsuario, fechaDesde, fechaHasta, page, pageSize);
            }
        }

        public static ResultadosBalanceGeneralViewModel ObtenerBalanceGeneral(int idUsuario, string fechaDesde, string fechaHasta)
        {
            using (var dbContext = new ACHEEntities())
            {
                return ObtenerBalanceGeneral(dbContext, idUsuario, fechaDesde, fechaHasta);
            }
        }

        public static ResultadoAsientosManualesViewModel ObtenerAsientosManuales(WebUser usu, int id)
        {
            using (var dbContext = new ACHEEntities())
            {
                ResultadoAsientosManualesViewModel resultado = new ResultadoAsientosManualesViewModel();
                resultado.items = dbContext.rptImpositivoLibroDiario.Where(x => x.IDAsiento == id && x.IDUsuario == usu.IDUsuario).ToList()
                                                         .Select(x => new AsientosManualesViewModel()
                                                         {
                                                             IDPlanDeCuenta = x.IDPlanDeCuenta,
                                                             NombreCuenta = x.Nombre,
                                                             Debe = x.Debe,
                                                             Haber = x.Haber
                                                             //Debe = x.Debe,
                                                             //Haber = x.Haber

                                                         }).ToList();

                var asiento = dbContext.rptImpositivoLibroDiario.Where(x => x.IDAsiento == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                resultado.Leyenda = asiento.Leyenda;
                resultado.Fecha = asiento.Fecha.ToString("dd/MM/yyyy");

                return resultado;
            }
        }

        public static ResultadoAsientosManualesViewModel ObtenerAsientoModelo(WebUser usu, int id)
        {
            using (var dbContext = new ACHEEntities())
            {
                ResultadoAsientosManualesViewModel resultado = new ResultadoAsientosManualesViewModel();
                resultado.items = dbContext.AsientoModeloDetalle.Where(x => x.IDAsientoModelo == id && x.AsientosModelo.IDUsuario == usu.IDUsuario).ToList()
                                                                        .Select(x => new AsientosManualesViewModel()
                                                                        {
                                                                            IDPlanDeCuenta = x.IDPlanDeCuenta,
                                                                            NombreCuenta = x.PlanDeCuentas.Codigo + " - " + x.PlanDeCuentas.Nombre,
                                                                            Debe = x.Importe,
                                                                            Haber = x.Importe
                                                                            //Debe = x.Debe,
                                                                            //Haber = x.Haber

                                                                        }).ToList();

                var asiento = dbContext.AsientosModelo.Where(x => x.IDAsientoModelo == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();

                resultado.Leyenda = asiento.Leyenda;
                resultado.Fecha = DateTime.Now.ToString("dd/MM/yyyy");
                return resultado;
            }
        }

        public static ResultadosAsientoManualViewModel ObtenerAsientosModelo(WebUser usu, string leyenda, int page, int pageSize)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.AsientosModelo.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();

                if (!string.IsNullOrEmpty(leyenda))
                    results = results.Where(x => x.Leyenda.ToLower().Contains(leyenda.ToLower()));

                //if (fechaDesde != string.Empty)
                //{
                //    DateTime dtDesde = DateTime.Parse(fechaDesde);
                //    results = results.Where(x => x.Fecha >= dtDesde);
                //}
                //if (fechaHasta != string.Empty)
                //{
                //    DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                //    results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                //}

                var resultsFinal = results.ToList();

                page--;
                ResultadosAsientoManualViewModel resultado = new ResultadosAsientoManualViewModel();
                resultado.TotalPage = ((resultsFinal.GroupBy(x => x.IDAsientoModelo).Count() - 1) / pageSize) + 1;
                resultado.TotalItems = resultsFinal.GroupBy(x => x.IDAsientoModelo).Count();


                var CantAsientos = 1;
                resultado.Asientos = resultsFinal.GroupBy(x => x.IDAsientoModelo).OrderBy(x => x.FirstOrDefault().IDAsientoModelo).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new AsientoManualViewModel()
                    {
                        IDAsiento = x.FirstOrDefault().IDAsientoModelo,
                        NroAsiento = NroAsiento(ref CantAsientos).ToString(),
                        Leyenda = x.FirstOrDefault().Leyenda,
                    }).ToList();

                resultado.TotalDebe = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalDebe)).ToMoneyFormat(2);
                resultado.TotalHaber = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalHaber)).ToMoneyFormat(2);
                return resultado;

            }
        }

        public static ResultadosAsientoManualViewModel ObtenerAsientosManualesResultados(WebUser usu, string leyenda, string fechaDesde, string fechaHasta, int page, int pageSize)
        {
            using (var dbContext = new ACHEEntities())
            {
                var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario && x.EsManual).AsQueryable();

                if (!string.IsNullOrEmpty(leyenda))
                    results = results.Where(x => x.Leyenda.ToLower().Contains(leyenda.ToLower()));

                if (fechaDesde != string.Empty)
                {
                    DateTime dtDesde = DateTime.Parse(fechaDesde);
                    results = results.Where(x => x.Fecha >= dtDesde);
                }
                if (fechaHasta != string.Empty)
                {
                    DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                    results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                }

                var resultsFinal = results.ToList();

                page--;
                ResultadosAsientoManualViewModel resultado = new ResultadosAsientoManualViewModel();
                resultado.TotalPage = ((resultsFinal.GroupBy(x => x.IDAsiento).Count() - 1) / pageSize) + 1;
                resultado.TotalItems = resultsFinal.GroupBy(x => x.IDAsiento).Count();


                var CantAsientos = 1;
                resultado.Asientos = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new AsientoManualViewModel()
                    {
                        IDAsiento = x.FirstOrDefault().IDAsiento,
                        //NroAsiento = NroAsiento(ref CantAsientos).ToString(),
                        Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
                        Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
                        AsientoDebe = TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "D"),
                        AsientoHaber = TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "H"),
                        TotalDebe = x.Sum(y => y.Debe).ToMoneyFormat(2),
                        TotalHaber = x.Sum(y => y.Haber).ToMoneyFormat(2),
                    }).ToList();

                resultado.TotalDebe = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalDebe)).ToMoneyFormat(2);
                resultado.TotalHaber = resultado.Asientos.Sum(x => Convert.ToDecimal(x.TotalHaber)).ToMoneyFormat(2);
                return resultado;
            }
        }

        public static List<PlanDeCuentasViewModel> ObtenerPlanDeCuentas(WebUser usu)
        {
            List<PlanDeCuentasViewModel> listaPlanDeCuenta = new List<PlanDeCuentasViewModel>();
            using (var dbContext = new ACHEEntities())
            {
                var planDeCuenta = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario).OrderBy(x => x.Codigo).ToList();
                listaPlanDeCuenta = planDeCuenta.Where(x => x.IDPadre == null).Select(x => new PlanDeCuentasViewModel()
                {
                    id = x.IDPlanDeCuenta,
                    text = x.Codigo + " - " + x.Nombre,// + ((x.Codigo == "1") ? "<span class='spanDetalle' style='float: right; margin-right: 20px;'>Débitos / Créditos</span>" : ""),
                    Codigo = x.Codigo,
                    Nombre = x.Nombre,
                    icon = "",
                    AdminiteAsientoManual = (x.AdminiteAsientoManual) ? "SI" : "NO",
                    children = ObtenerHijo(dbContext, x.IDPlanDeCuenta, planDeCuenta)
                }).ToList();

                return listaPlanDeCuenta;
            }
        }

        private static List<PlanDeCuentasViewModel> ObtenerHijo(ACHEEntities dbContext, int id, List<PlanDeCuentas> planDeCuenta)
        {
            List<PlanDeCuentasViewModel> listaPlanAux = new List<PlanDeCuentasViewModel>();

            var list = planDeCuenta.Where(x => x.IDPadre == id).Select(x => new PlanDeCuentasViewModel()
            {
                id = x.IDPlanDeCuenta,
                text = EsCuentaPadre(dbContext, x.IDPlanDeCuenta) ? x.Codigo + " - " + x.Nombre : x.Codigo + " - " + x.Nombre,// + ObtenerTextoSaldo(x.IDPlanDeCuenta),
                Codigo = x.Codigo,
                Nombre = x.Nombre,
                children = (planDeCuenta.Any(y => y.IDPadre == x.IDPlanDeCuenta)) ? ObtenerHijo(dbContext, x.IDPlanDeCuenta, planDeCuenta) : null,
                icon = (planDeCuenta.Any(y => y.IDPadre == x.IDPlanDeCuenta)) ? "" : "fa fa-file-text",
                AdminiteAsientoManual = (x.AdminiteAsientoManual) ? "SI" : "NO",
                TipoDeCuenta = x.TipoDeCuenta.ToUpper()
            }).ToList();

            return list;
        }

        public static PlanDeCuentasViewModel ObtenerCuenta(WebUser usu, int idPlanDeCuenta)
        {
            PlanDeCuentasViewModel Cuenta = new PlanDeCuentasViewModel();
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPlanDeCuenta == idPlanDeCuenta).Select(x => new PlanDeCuentasViewModel()
                {
                    id = x.IDPlanDeCuenta,
                    Codigo = x.Codigo,
                    Nombre = x.Nombre,
                    text = x.Codigo + " - " + x.Nombre,
                    AdminiteAsientoManual = (x.AdminiteAsientoManual) ? "SI" : "NO",
                    IDPadre = (x.IDPadre == null) ? 0 : (int)x.IDPadre,
                    TipoDeCuenta = x.TipoDeCuenta.ToUpper()
                }).FirstOrDefault();

            }
        }




        #endregion

        #region Crear/Insertar/Guardar

        private static AsientoDetalle insertarCuenta(decimal importe, int idCuenta, Asientos asiento, string tipoAsiente)
        {
            var asientoP = new AsientoDetalle();
            asientoP.Asientos = asiento;
            asientoP.TipoDeAsiento = tipoAsiente;
            //asientoP.Importe = Math.Round(importe, 2);
            asientoP.Importe = importe;
            asientoP.IDPlanDeCuenta = idCuenta;
            return asientoP;
        }

        private static AsientoModeloDetalle insertarCuentaModelo(decimal importe, int idCuenta, AsientosModelo asiento, string tipoAsiente)
        {
            var asientoP = new AsientoModeloDetalle();
            asientoP.AsientosModelo = asiento;
            asientoP.TipoDeAsiento = tipoAsiente;
            //asientoP.Importe = Math.Round(importe, 2);
            asientoP.Importe = 0;
            asientoP.IDPlanDeCuenta = idCuenta;
            return asientoP;
        }

        public static void CrearCuentaBancos(int idBancos, string idPlanDeCuenta, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo) //Plan Corporativo
                    {
                        var bancosPlanDeCtaActual = dbContext.BancosPlanDeCuenta.FirstOrDefault(x => x.IDBanco == idBancos);

                        if (bancosPlanDeCtaActual == null || !string.IsNullOrEmpty(idPlanDeCuenta))
                        {
                            var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                            if (config == null)
                                throw new Exception(String.Format("No existe la configuracion de plan de cuenta para el usuario {0}", usu.IDUsuario));

                            var banco = dbContext.Bancos.Where(x => x.IDUsuario == usu.IDUsuario && x.IDBanco == idBancos).FirstOrDefault();
                            if (banco == null)
                                throw new Exception(String.Format("No existe el banco {0} configurado para el usuario {1}", idBancos, usu.IDUsuario));

                            var padre = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPlanDeCuenta == config.IDCtaBancos).FirstOrDefault();
                            if (padre == null)
                                throw new Exception(String.Format("No existe el plan de cuentas {0} configurado para el usuario {1}", config.IDCtaBancos, usu.IDUsuario));

                            var maxCodigo = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPadre == padre.IDPlanDeCuenta).ToList().Max(x => x.Codigo);
                            if (maxCodigo == null)
                                throw new Exception("La cuenta contable padre de Bancos es incorrecta");

                            int codAux = 0;
                            string codigo = "";

                            if (maxCodigo.Contains("."))
                            {
                                codAux = Convert.ToInt32(maxCodigo.Split('.')[(maxCodigo.Split('.').Length - 1)]) + 1;
                                codigo = padre.Codigo + "." + (codAux).ToString();
                            }
                            else
                            {
                                codAux = Convert.ToInt32(maxCodigo) + 1;
                                codigo = codAux.ToString();
                            }

                            var nombreCuenta = "Cta cte banco " + banco.BancosBase.Nombre;
                            var entity = new PlanDeCuentas();
                            if (string.IsNullOrEmpty(idPlanDeCuenta))
                            {
                                entity.Codigo = codigo;
                                entity.Nombre = nombreCuenta;
                                entity.IDUsuario = usu.IDUsuario;
                                entity.IDPadre = padre.IDPlanDeCuenta;
                                entity.AdminiteAsientoManual = true;
                                entity.TipoDeCuenta = "ACTIVO";
                                dbContext.PlanDeCuentas.Add(entity);
                            }
                            else
                            {
                                int idCuenta = int.Parse(idPlanDeCuenta);
                                entity = dbContext.PlanDeCuentas.Where(x => x.IDPlanDeCuenta == idCuenta && x.IDUsuario == usu.IDUsuario).First();
                            }

                            //var bancosPlanDeCta = new BancosPlanDeCuenta();
                            if (bancosPlanDeCtaActual == null)
                            {
                                bancosPlanDeCtaActual = new BancosPlanDeCuenta();
                                bancosPlanDeCtaActual.IDBanco = idBancos;
                                bancosPlanDeCtaActual.PlanDeCuentas = entity;
                                bancosPlanDeCtaActual.IDUsuario = usu.IDUsuario;
                                entity.BancosPlanDeCuenta.Add(bancosPlanDeCtaActual);
                            }
                            else
                                bancosPlanDeCtaActual.PlanDeCuentas = entity;

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Diagnostics.Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void CrearCuentaCaja(int idCaja, WebUser usu)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo) //Plan Corporativo
                    {
                        if (!dbContext.CajasPlanDeCuenta.Any(x => x.IDCaja == idCaja && x.Cajas != null))
                        {
                            var config = dbContext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                            if (config == null)
                                throw new Exception(String.Format("No existe la configuracion de plan de cuenta para el usuario {0}", usu.IDUsuario));

                            var caja = dbContext.Cajas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDCaja == idCaja).FirstOrDefault();
                            if (caja == null)
                                throw new Exception(String.Format("No existe la caja {0} configurado para el usuario {1}", idCaja, usu.IDUsuario));

                            var padre = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPlanDeCuenta == config.IDCtaBancos).FirstOrDefault();
                            if (padre == null)
                                throw new Exception(String.Format("No existe el plan de cuentas {0} configurado para el usuario {1}", config.IDCtaBancos, usu.IDUsuario));

                            var maxCodigo = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPadre == padre.IDPadre).ToList().Max(x => x.Codigo);
                            if (maxCodigo == null)
                                throw new Exception("La cuenta contable padre de Cajas es incorrecta");

                            int codAux = 0;
                            string codigo = "";

                            if (maxCodigo.Contains("."))
                            {
                                codAux = Convert.ToInt32(maxCodigo.Split('.')[(maxCodigo.Split('.').Length - 1)]) + 1;
                                codigo = padre.Codigo + "." + (codAux).ToString();
                            }
                            else
                            {
                                codAux = Convert.ToInt32(maxCodigo) + 1;
                                codigo = codAux.ToString();
                            }


                            var entity = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario && x.Nombre == caja.Nombre).FirstOrDefault();
                            if (entity == null)
                            {

                                var nombreCuenta = caja.Nombre;
                                entity = new PlanDeCuentas();

                                entity.Codigo = codigo;
                                entity.Nombre = nombreCuenta;
                                entity.IDUsuario = usu.IDUsuario;
                                entity.IDPadre = padre.IDPlanDeCuenta;
                                entity.AdminiteAsientoManual = true;
                                entity.TipoDeCuenta = "ACTIVO";
                                dbContext.PlanDeCuentas.Add(entity);
                            }

                            var cajasPlanDeCta = new CajasPlanDeCuenta();
                            cajasPlanDeCta.IDCaja = idCaja;
                            cajasPlanDeCta.PlanDeCuentas = entity;
                            cajasPlanDeCta.IDUsuario = usu.IDUsuario;
                            entity.CajasPlanDeCuenta.Add(cajasPlanDeCta);

                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Diagnostics.Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void GuardarPlanDeCuenta(WebUser usu, int id, string codigo, string nombre, int idPadre, string adminiteAsientoManual, string tipoDeCuenta)
        {
            using (var dbContext = new ACHEEntities())
            {
                if (dbContext.PlanDeCuentas.Any(x => x.Codigo == codigo && x.IDUsuario == usu.IDUsuario && x.IDPlanDeCuenta != id))
                    throw new Exception("Ya existe una cuenta con el codigo ingresado");
                if (dbContext.PlanDeCuentas.Any(x => x.Nombre == nombre && x.IDUsuario == usu.IDUsuario && x.IDPlanDeCuenta != id))
                    throw new Exception("Ya existe una cuenta con el nombre ingresado");
                //else if (id > 0 && EsCuentaPadre(dbContext, id))
                //    throw new Exception("No se puede modificar las las cuentas superiores");
                else if (ContabilidadCommon.VerificarCodigo(codigo))
                    throw new Exception("El código contiene caracteres invalidos");

                PlanDeCuentas entity;
                if (id > 0)
                    entity = dbContext.PlanDeCuentas.Where(x => x.IDPlanDeCuenta == id && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                else
                {
                    entity = new PlanDeCuentas();
                    entity.IDUsuario = usu.IDUsuario;
                }
                entity.Codigo = codigo;
                entity.Nombre = nombre;

                if (idPadre != 0)
                {
                    entity.IDPadre = idPadre;
                    entity.TipoDeCuenta = dbContext.PlanDeCuentas.Where(x => x.IDPlanDeCuenta == idPadre && x.IDUsuario == usu.IDUsuario).FirstOrDefault().TipoDeCuenta;
                }
                else
                    entity.TipoDeCuenta = tipoDeCuenta;

                entity.AdminiteAsientoManual = (adminiteAsientoManual == "SI") ? true : false;

                if (id > 0)
                    dbContext.SaveChanges();
                else
                {
                    dbContext.PlanDeCuentas.Add(entity);
                    dbContext.SaveChanges();
                }
            }
        }


        #endregion

        #region Validar

        public static bool VerificarCodigo(string codigo)
        {
            var listaCodigos = codigo.Split('.').ToList();

            foreach (var item in listaCodigos)
            {
                long n = 0;
                if (!long.TryParse(item, out n))
                    return true;
            }

            return false;
        }

        public static bool ValidarCierreContable(int idUsuario, DateTime fecha)
        {
            using (var dbContext = new ACHEEntities())
            {
                var añoContableFinalizado = dbContext.Asientos.Where(x => x.EsAsientoCierre && x.IDUsuario == idUsuario).OrderByDescending(x => x.Fecha).FirstOrDefault();
                if (añoContableFinalizado != null)
                {
                    if (fecha <= añoContableFinalizado.Fecha)
                        return true;
                }
            }

            return false;
        }

        public static bool ValidarNroPago(int idusuario, int idpago, int nroPago)
        {
            using (var dbContext = new ACHEEntities())
            {
                if (dbContext.Pagos.Any(x => x.IDPago != idpago && x.NroPago == nroPago && x.IDUsuario == idusuario))
                    return true;
            }

            return false;
        }

        public static bool ValidarCorrelatividad(int id, int idUsuario, string modo, int idPuntoVenta, string nroComprobante, string tipoComprobante)
        {
            if (modo != "E")
            {
                using (var dbContext = new ACHEEntities())
                {
                    int nro = int.Parse(nroComprobante);
                    bool existe = dbContext.Comprobantes.Any(x => x.IDUsuario == idUsuario && x.IDPuntoVenta == idPuntoVenta && x.Tipo == tipoComprobante && x.Modo != "E" && x.Numero == nro && x.IDComprobante != id);
                    return existe;
                }
            }
            else
                return false;

        }

        private static bool validarPartidaDoble(Asientos asiento)
        {
            var TotalDebe = asiento.AsientoDetalle.Where(x => x.TipoDeAsiento == "D").Sum(x => x.Importe);
            var TotalHaber = asiento.AsientoDetalle.Where(x => x.TipoDeAsiento == "H").Sum(x => x.Importe);


            string msg = "asiento: " + asiento.IDAsiento + " debe: " + TotalDebe.ToString() + " haber: " + TotalHaber.ToString();

            var col = asiento.AsientoDetalle.ToList();
            
            foreach(var c in col) {
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), "asiento detalle: "+c.IDAsientoParte, c.Importe.ToString());
            }


            if (TotalDebe == TotalHaber)
                return true;
            else
                return false;
        }

        public static bool ValidarFechaLimiteCarga(int idUsuario, DateTime? fecha)
        {
            using (var dbContext = new ACHEEntities())
            {
                if (fecha.HasValue)
                {
                    var fechaLimiteCarga = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).FirstOrDefault().FechaLimiteCarga;
                    if (fechaLimiteCarga != null)
                    {
                        if (fecha < fechaLimiteCarga)
                            return true;
                    }
                }
            }

            return false;
        }

        #endregion

        #region Eliminar

        public static void EliminarConfiguracionPlanDeCuenta(int idUsuario)
        {
            try
            {
                using (var dbcontext = new ACHEEntities())
                {
                    if (dbcontext.Asientos.Any(x => x.IDUsuario == idUsuario))
                        throw new CustomException("La configuración del plan de cuentas no se puede ser eliminado por tener asientos contables.");

                    var config = dbcontext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    if (config != null)
                    {
                        dbcontext.ConfiguracionPlanDeCuenta.Remove(config);
                        dbcontext.SaveChanges();
                    }
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void EliminarPlanDeCuentasActual(int idUsuario)
        {
            try
            {
                using (var dbcontext = new ACHEEntities())
                {
                    if (dbcontext.Asientos.Any(x => x.IDUsuario == idUsuario))
                        throw new CustomException("El plan de cuentas actual no se puede eliminar por tener asientos contables.");
                    //if (ContabilidadCommon.ValidarFechaLimiteCarga(idUsuario, DateTime.Now))
                    //    throw new CustomException("No puede agregar ni modificar un comprobante que se encuentre fuera de la fecha limite de carga.");

                    var listaCuentas = dbcontext.PlanDeCuentas.Where(x => x.IDUsuario == idUsuario).ToList();
                    var config = dbcontext.ConfiguracionPlanDeCuenta.Where(x => x.IDUsuario == idUsuario).FirstOrDefault();
                    var bancos = dbcontext.BancosPlanDeCuenta.Where(x => x.IDUsuario == idUsuario).ToList();
                    var cajas = dbcontext.CajasPlanDeCuenta.Where(x => x.IDUsuario == idUsuario).ToList();

                    foreach (var item in cajas)
                    {
                        dbcontext.CajasPlanDeCuenta.Remove(item);
                        dbcontext.SaveChanges();
                    }

                    foreach (var item in bancos)
                    {
                        dbcontext.BancosPlanDeCuenta.Remove(item);
                        dbcontext.SaveChanges();
                    }

                    foreach (var item in listaCuentas)
                    {
                        dbcontext.PlanDeCuentas.Remove(item);
                        dbcontext.SaveChanges();
                    }

                    if (config != null)
                    {
                        dbcontext.ConfiguracionPlanDeCuenta.Remove(config);
                        dbcontext.SaveChanges();
                    }
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void EliminarAsientoModelo(int idUsuario, int idAsientoModelo)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var entity = dbContext.AsientosModelo.Where(x => x.IDAsientoModelo == idAsientoModelo && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        dbContext.AsientosModelo.Remove(entity);
                        dbContext.SaveChanges();
                    }

                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
        }

        public static void EliminarAsientoManual(int idUsuario, int idAsiento)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {

                    var entity = dbContext.Asientos.Where(x => x.IDAsiento == idAsiento && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (entity != null)
                    {
                        if (entity.IDMovimientoDeFondo != null || entity.IDCheque != null || entity.IDCaja != null || entity.IDGastosBancarios != null || entity.IDCobranza != null || entity.IDCompra != null || entity.IDComprobante != null || entity.IDPago != null)
                            throw new Exception("Solo los asientos manuales se pueden eliminar.");
                        else if (!entity.EsAsientoInicio && !entity.EsAsientoCierre && ContabilidadCommon.ValidarCierreContable(idUsuario, entity.Fecha))
                            throw new Exception("El comprobante no puede eliminarse ya que el año contable ya fue cerrado.");
                        dbContext.Asientos.Remove(entity);
                        dbContext.SaveChanges();
                    }

                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
        }

        public static void EliminarPlanDeCuenta(int idUsuario, int id)
        {
            using (var dbContext = new ACHEEntities())
            {
                if (dbContext.PlanDeCuentas.Any(x => x.IDPadre == id))
                    throw new CustomException("No se puede eliminar por tener asientos cuentas hijas asociados");
                else if (dbContext.AsientoDetalle.Any(x => x.IDPlanDeCuenta == id))
                    throw new CustomException("No se puede eliminar por tener asientos contables asociados");
                //else if (id > 0 && EsCuentaPadre(dbContext, id))
                //    throw new CustomException("No se puede eliminar las las cuentas superiores");

                var entity = dbContext.PlanDeCuentas.Where(x => x.IDPlanDeCuenta == id && x.IDUsuario == idUsuario).FirstOrDefault();
                if (entity != null)
                {
                    dbContext.PlanDeCuentas.Remove(entity);
                    dbContext.SaveChanges();
                }
            }
        }


        #endregion

        #region Exportar

        public static string ExportarAsientosManuales(WebUser usu, string leyenda, string fechaDesde, string fechaHasta)
        {
            string fileName = "AsientosManuales_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario && x.EsManual).AsQueryable();
                    if (!string.IsNullOrEmpty(leyenda))
                        results = results.Where(x => x.Leyenda.ToLower().Contains(leyenda.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    var resultsFinal = results.ToList();

                    var CantAsientos = 1;
                    var listaCuentas = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).ToList()
                    .Select(x => new AsientoManualViewModel()
                    {
                        IDAsiento = x.FirstOrDefault().IDAsiento,
                        //NroAsiento = NroAsiento(ref CantAsientos).ToString(),
                        Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
                        Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
                        AsientoDebe = ContabilidadCommon.TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "D"),
                        AsientoHaber = ContabilidadCommon.TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "H"),
                        TotalDebe = x.Sum(y => y.Debe).ToMoneyFormat(2),
                        TotalHaber = x.Sum(y => y.Haber).ToMoneyFormat(2)
                    }).ToList();

                    List<AsientoManualExportacionViewModel> listaFinal = new List<AsientoManualExportacionViewModel>();
                    AsientoManualExportacionViewModel asiento;
                    foreach (var item in listaCuentas)
                    {
                        asiento = new AsientoManualExportacionViewModel();
                        asiento.NroAsiento = item.NroAsiento;
                        asiento.Fecha = item.Fecha;
                        asiento.Leyenda = item.Leyenda;
                        asiento.Debe = item.AsientoDebe;
                        asiento.Haber = item.AsientoHaber;
                        listaFinal.Add(asiento);

                    }

                    dt = listaFinal.ToList().Select(x => new
                    {
                        //NroAsiento = x.NroAsiento,
                        Fecha = x.Fecha,
                        Leyenda = x.Leyenda,
                        Debe = x.Debe,
                        Haber = x.Haber
                    }).ToList().ToDataTable();
                }

                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        public static string ExportarAsientosManualesPDF(WebUser usu, string leyenda, string fechaDesde, string fechaHasta)
        {
            string fileName = "AsientosManuales_" + usu.IDUsuario + "_";
            string path = "~/tmp/";
            try
            {
                string TotalDebito = "0";
                string TotalCredito = "0";
                DataTable dt = new DataTable();
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.rptImpositivoLibroDiario.Where(x => x.IDUsuario == usu.IDUsuario && x.EsManual).AsQueryable();
                    if (!string.IsNullOrEmpty(leyenda))
                        results = results.Where(x => x.Leyenda.ToLower().Contains(leyenda.ToLower()));
                    if (fechaDesde != string.Empty)
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (fechaHasta != string.Empty)
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta);// + " 23:59:59 pm");
                        results = results.Where(x => DbFunctions.TruncateTime(x.Fecha) <= DbFunctions.TruncateTime(dtHasta));
                    }

                    var resultsFinal = results.ToList();

                    var CantAsientos = 1;
                    var listaCuentas = resultsFinal.GroupBy(x => x.IDAsiento).OrderBy(x => x.FirstOrDefault().Fecha).ThenBy(x => x.FirstOrDefault().IDAsiento).ToList()
                    .Select(x => new AsientoManualViewModel()
                    {
                        NroAsiento = NroAsiento(ref CantAsientos).ToString(),
                        Fecha = x.FirstOrDefault().Fecha.ToString("dd/MM/yyyy"),
                        Leyenda = x.FirstOrDefault().Leyenda + (x.FirstOrDefault().RazonSocial != "" ? " (" + x.FirstOrDefault().RazonSocial + ")" : ""),
                        AsientoDebe = ContabilidadCommon.TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "D"),
                        AsientoHaber = ContabilidadCommon.TotalesAsientosManuales(x.FirstOrDefault().IDAsiento, results.ToList(), "H"),
                    }).ToList();

                    List<AsientoManualExportacionViewModel> listaFinal = new List<AsientoManualExportacionViewModel>();
                    AsientoManualExportacionViewModel asiento;
                    foreach (var item in listaCuentas)
                    {
                        asiento = new AsientoManualExportacionViewModel();
                        asiento.NroAsiento = item.NroAsiento;
                        asiento.Fecha = item.Fecha;
                        asiento.Leyenda = item.Leyenda;
                        asiento.Debe = item.AsientoDebe;
                        asiento.Haber = item.AsientoHaber;

                        listaFinal.Add(asiento);
                    }

                    dt = listaFinal.ToList().Select(x => new AsientoManualExportacionViewModel()
                    {
                        NroAsiento = x.NroAsiento,
                        Fecha = x.Fecha,
                        Leyenda = x.Leyenda,
                        Debe = x.Debe,
                        Haber = x.Haber
                    }).ToList().ToDataTable();


                    TotalDebito = listaFinal.Sum(x => Convert.ToDecimal(x.Debe)).ToMoneyFormat(2);
                    TotalCredito = listaFinal.Sum(x => Convert.ToDecimal(x.Haber)).ToMoneyFormat(2);
                }



                if (dt.Rows.Count > 0)
                {
                    ContenidoArchivoViewModel contenido = new ContenidoArchivoViewModel();
                    ///razon social , cuit y periodo de fechas 
                    List<DetalleCabecera> cabecera = new List<DetalleCabecera>();
                    DetalleCabecera c = new DetalleCabecera();
                    c.Titulo = "Razón Social";
                    c.Valor = usu.RazonSocial;
                    cabecera.Add(c);

                    DetalleCabecera c1 = new DetalleCabecera();
                    c1.Titulo = "CUIT";
                    c1.Valor = usu.CUIT;
                    cabecera.Add(c1);
                    DetalleCabecera c2 = new DetalleCabecera();

                    c2.Titulo = "Período";
                    c2.Valor = fechaDesde + " - " + fechaHasta;
                    cabecera.Add(c2);
                    contenido.Cabecera = cabecera;

                    contenido.TituloReporte = "ASIENTOS MANUALES";
                    List<string> ultimaFilaTotales = new List<string>();
                    ultimaFilaTotales.Add("");
                    ultimaFilaTotales.Add("");

                    ultimaFilaTotales.Add("Totales");

                    ultimaFilaTotales.Add(TotalDebito);
                    ultimaFilaTotales.Add(TotalCredito);



                    //ultimaFilaTotales.Add(TotalServicios.ToMoneyFormat(4));
                    //ultimaFilaTotales.Add(TotalProductos.ToMoneyFormat(4));

                    ultimaFilaTotales.Add("");
                    contenido.UltimaFila = ultimaFilaTotales;
                    contenido.Widths = new float[] { 20f, 20f, 80f, 30f, 30f };

                    CommonModel.GenerarArchivoPDF(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName, contenido, false, true);
                }
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".pdf").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        public static string ExportarPlanDeCuentas(WebUser usu)
        {
            string fileName = "PlanDeCuentas_" + usu.IDUsuario + "_";
            string path = "~/tmp/";

            DataTable dt = new DataTable();
            List<PlanDeCuentasViewModel> results = new List<PlanDeCuentasViewModel>();
            List<PlanDeCuentasViewModel> resultsFinal = new List<PlanDeCuentasViewModel>();
            using (var dbContext = new ACHEEntities())
            {
                var planDeCuenta = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario).OrderBy(x => x.Codigo).ToList();
                results = planDeCuenta.Where(x => x.IDPadre == null).Select(x => new PlanDeCuentasViewModel()
                {
                    id = x.IDPlanDeCuenta,
                    text = x.Codigo + " - " + x.Nombre,// + ((x.Codigo == "1") ? "<span class='spanDetalle' style='float: right; margin-right: 20px;'>Débitos / Créditos</span>" : ""),
                    Codigo = x.Codigo,
                    Nombre = x.Nombre,
                    icon = "",
                    AdminiteAsientoManual = (x.AdminiteAsientoManual) ? "SI" : "NO",
                    children = ObtenerHijo(dbContext, x.IDPlanDeCuenta, planDeCuenta),
                    TipoDeCuenta = x.TipoDeCuenta.ToUpper(),
                }).ToList();
                #region anterior
                //todo: hacer recursivo

                /*
                      foreach (var item in results)
                      {
                          resultsFinal.Add(
                                  new PlanDeCuentasViewModel()
                                  {
                                      text = item.Codigo,// + ((x.Codigo == "1") ? "<span class='spanDetalle' style='float: right; margin-right: 20px;'>Débitos / Créditos</span>" : ""),
                                      Codigo = item.Codigo,
                                      Nombre = item.Nombre.ToProperCase(),
                                      AdminiteAsientoManual = item.AdminiteAsientoManual,
                                      TipoDeCuenta = item.TipoDeCuenta
                                  });

                          foreach (var cuenta in item.children)
                          {
                              resultsFinal.Add(
                                  new PlanDeCuentasViewModel()
                                  {
                                      text = item.Codigo,// + ((x.Codigo == "1") ? "<span class='spanDetalle' style='float: right; margin-right: 20px;'>Débitos / Créditos</span>" : ""),
                                      Codigo = cuenta.Codigo,
                                      Nombre = cuenta.Nombre.ToProperCase(),
                                      AdminiteAsientoManual = cuenta.AdminiteAsientoManual,
                                      TipoDeCuenta = cuenta.TipoDeCuenta
                                  });

                              if (cuenta.children != null && cuenta.children.Any())
                              {
                                  foreach (var subcuenta in cuenta.children)
                                  {
                                      resultsFinal.Add(
                                          new PlanDeCuentasViewModel()
                                          {
                                              text = cuenta.Codigo,// + ((x.Codigo == "1") ? "<span class='spanDetalle' style='float: right; margin-right: 20px;'>Débitos / Créditos</span>" : ""),
                                              Codigo = subcuenta.Codigo,
                                              Nombre = subcuenta.Nombre.ToProperCase(),
                                              AdminiteAsientoManual = subcuenta.AdminiteAsientoManual,
                                              TipoDeCuenta = subcuenta.TipoDeCuenta
                                          });

                                      if (subcuenta.children != null && subcuenta.children.Any())
                                      {
                                          foreach (var subsubcuenta in cuenta.children)
                                          {
                                              resultsFinal.Add(
                                                  new PlanDeCuentasViewModel()
                                                  {
                                                      text = subcuenta.Codigo,// + ((x.Codigo == "1") ? "<span class='spanDetalle' style='float: right; margin-right: 20px;'>Débitos / Créditos</span>" : ""),
                                                      Codigo = subsubcuenta.Codigo,
                                                      Nombre = subsubcuenta.Nombre.ToProperCase(),
                                                      AdminiteAsientoManual = subsubcuenta.AdminiteAsientoManual,
                                                      TipoDeCuenta = subsubcuenta.TipoDeCuenta
                                                  });
                                          }
                                      }
                                  }
                              }
                          }
                      }*/
                #endregion

                resultsFinal = armarLista(resultsFinal, results, "");
                dt = resultsFinal.Select(x => new
                {
                    Nombre = x.Nombre,
                    Codigo = x.Codigo,
                    CodigoPadre = x.text == x.Codigo ? "" : x.text,//fix para cuentaspades
                    AdminiteAsientoManual = x.AdminiteAsientoManual,
                    TipoDeCuenta = x.TipoDeCuenta
                }).ToList().ToDataTable();
            }

            if (dt.Rows.Count > 0)
                CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
            else
                throw new Exception("No se encuentran datos para los filtros seleccionados");

            return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");

        }


        #endregion

        #region Reprocesar

        public static string ReprocesarComprobantes(WebUser usu, string tipo, string fechaDesde, string fechaHasta)
        {
            var html = "";
            var detErrores = "";
            int total = 0;
            int ok = 0;
            int error = 0;

            DateTime dtDesde = DateTime.Parse(fechaDesde);
            DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");

            using (var dbContext = new ACHEEntities())
            {
                switch (tipo)
                {
                    case "Venta":
                        var list1 = dbContext.Comprobantes.Include("PuntosDeVenta").Where(x => x.IDUsuario == usu.IDUsuario && x.FechaComprobante >= dtDesde && x.FechaComprobante <= dtHasta).ToList();
                        total = list1.Count();
                        foreach (var det in list1)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeVentas(usu, det.IDComprobante);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores += "<br>ID: " + det.IDComprobante + " - " + det.Tipo + " " + det.FechaComprobante.ToString("dd/MM/yyyy") + " - " + det.PuntosDeVenta.Punto.ToString("#0000") + "-" + det.Numero.ToString("#00000000") + ": " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"Venta\"," + det.IDComprobante + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    case "Compra":
                        var list2 = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.FechaEmision >= dtDesde && x.FechaEmision <= dtHasta).ToList();
                        total = list2.Count();
                        foreach (var det in list2)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeCompra(det.IDCompra, usu);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message;//.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores += "<br>ID: " + det.IDCompra + " - " + det.FechaEmision.ToString("dd/MM/yyyy") + " - " + det.Tipo + " " + det.NroFactura + ": " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"Compra\"," + det.IDCompra + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    case "Cobranza":
                        var list3 = dbContext.Cobranzas.Include("CobranzasDetalle").Where(x => x.IDUsuario == usu.IDUsuario && x.FechaCobranza >= dtDesde && x.FechaCobranza <= dtHasta).ToList();
                        total = list3.Count();
                        foreach (var det in list3)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeCobranza(usu, det.IDCobranza);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message;
                                detErrores += "<br>ID: " + det.IDCobranza + " - " + det.FechaCobranza.ToString("dd/MM/yyyy") + " - " + det.PuntosDeVenta.Punto.ToString("#0000") + "-" + det.Numero.ToString("#00000000") + ": " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"Cobranza\"," + det.IDCobranza + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    case "Pago":
                        var list4 = dbContext.Pagos.Where(x => x.IDUsuario == usu.IDUsuario && x.FechaPago >= dtDesde && x.FechaPago <= dtHasta).ToList();
                        total = list4.Count();
                        foreach (var det in list4)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDePago(usu, det.IDPago);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message;//.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores += "<br>ID: " + det.IDPago + " - " + det.FechaPago.ToString("dd/MM/yyyy") + " - " + det.Personas.RazonSocial + ": " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"Pago\"," + det.IDPago + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    case "MovFondos":
                        var list5 = dbContext.MovimientoDeFondos.Where(x => x.IDUsuario == usu.IDUsuario && x.FechaMovimiento >= dtDesde && x.FechaMovimiento <= dtHasta).ToList();
                        total = list5.Count();
                        foreach (var det in list5)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeMovDeFondo(det.IDMovimientoDeFondo, usu);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message;//.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores += "<br>ID: " + det.IDMovimientoDeFondo + " - " + det.FechaMovimiento.ToString("dd/MM/yyyy") + " - " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"MovFondos\"," + det.IDMovimientoDeFondo + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    case "Caja":
                        var list6 = dbContext.Caja.Where(x => x.IDUsuario == usu.IDUsuario && x.Estado != "Anulado" && x.Fecha >= dtDesde && x.Fecha <= dtHasta).ToList();
                        total = list6.Count();
                        foreach (var det in list6)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeCaja(usu, det.IDCaja);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message;//.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores += "<br>ID: " + det.IDCaja + " - " + det.Fecha.Value.ToString("dd/MM/yyyy") + " - " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"Caja\"," + det.IDCaja + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    case "BancosDetalle":
                        var list7 = dbContext.BancosDetalle.Where(x => x.IDUsuario == usu.IDUsuario && x.Fecha >= dtDesde && x.Fecha <= dtHasta).ToList();
                        total = list7.Count();
                        foreach (var det in list7)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeBanco(usu, det.IDBancoDetalle);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message;//.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores += "<br>ID: " + det.IDBancoDetalle + " - " + det.Fecha.ToString("dd/MM/yyyy") + " - " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"BancosDetalle\"," + det.IDBancoDetalle + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    case "Cheque":
                        var list8 = dbContext.ChequeAccion.Where(x => x.IDUsuario == usu.IDUsuario && x.Fecha >= dtDesde && x.Fecha <= dtHasta).ToList();
                        total = list8.Count();
                        foreach (var det in list8)
                        {
                            try
                            {
                                if (det.Accion == "Depositado" || det.Accion == "Ventanilla" || (det.Accion == "Debitado" && det.Cheques.EsPropio))
                                    ContabilidadCommon.AgregarAsientoChequeAccion(usu, det.IDChequeAccion);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message;//.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores += "<br>ID: " + det.IDChequeAccion + " - " + det.Fecha.ToString("dd/MM/yyyy") + " - " + msg + ". <a href='javascript:asientosPendientes.reprocesarPorID(\"Cheque\"," + det.IDChequeAccion + ")'>reprocesar</a>";
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            html = "Se han procesado con éxito " + ok + " de un total de " + total;
            if (error > 0)
                html += ". Se han encontrado los siguientes errores: " + detErrores;

            return html;

        }

        public static string ReprocesarComprobantesPorID(WebUser usu, string tipo, int id)
        {
            var html = "";
            var detErrores = "";
            int total = 1;
            int ok = 0;
            int error = 0;
            using (var dbContext = new ACHEEntities())
            {
                switch (tipo)
                {
                    case "Venta":
                        var comp = dbContext.Comprobantes.Include("PuntosDeVenta").Where(x => x.IDUsuario == usu.IDUsuario && x.IDComprobante == id).FirstOrDefault();
                        if (comp != null)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeVentas(usu, comp.IDComprobante);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                var msg = ex.Message.Replace("Factura generada correctamente, pero los asientos", "Los asientos");
                                detErrores = comp.Tipo + " " + comp.PuntosDeVenta.Punto.ToString("#0000") + "-" + comp.Numero.ToString("#00000000") + ": " + msg;
                            }
                        }
                        break;
                    case "Compra":
                        var compra = dbContext.Compras.Where(x => x.IDUsuario == usu.IDUsuario && x.IDCompra == id).FirstOrDefault();
                        if (compra != null)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeCompra(id, usu);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                detErrores = ex.Message;
                            }
                        }
                        break;
                    case "Cobranza":
                        var cob = dbContext.Cobranzas.Include("CobranzasDetalle").Where(x => x.IDUsuario == usu.IDUsuario && x.IDCobranza == id).FirstOrDefault();
                        if (cob != null)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeCobranza(usu, id);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                detErrores = ex.Message;
                            }
                        }
                        break;
                    case "Pago":
                        var pago = dbContext.Pagos.Where(x => x.IDUsuario == usu.IDUsuario && x.IDPago == id).FirstOrDefault();
                        if (pago != null)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDePago(usu, id);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                detErrores = ex.Message;
                            }
                        }
                        break;
                    case "MovFondos":
                        var list5 = dbContext.MovimientoDeFondos.Where(x => x.IDUsuario == usu.IDUsuario && x.IDMovimientoDeFondo == id).ToList();
                        total = list5.Count();
                        foreach (var det in list5)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeMovDeFondo(det.IDMovimientoDeFondo, usu);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                detErrores = ex.Message;
                            }
                        }
                        break;
                    case "Caja":
                        var list6 = dbContext.Caja.Where(x => x.IDUsuario == usu.IDUsuario && x.Estado != "Anulado" && x.IDCaja == id).ToList();
                        total = list6.Count();
                        foreach (var det in list6)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeCaja(usu, det.IDCaja);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                detErrores = ex.Message;
                            }
                        }
                        break;
                    case "BancosDetalle":
                        var list7 = dbContext.BancosDetalle.Where(x => x.IDUsuario == usu.IDUsuario && x.IDBanco == id).ToList();
                        total = list7.Count();
                        foreach (var det in list7)
                        {
                            try
                            {
                                ContabilidadCommon.AgregarAsientoDeBanco(usu, det.IDBancoDetalle);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                detErrores = ex.Message;
                            }
                        }
                        break;
                    case "Cheque":
                        var list8 = dbContext.ChequeAccion.Where(x => x.IDUsuario == usu.IDUsuario && x.IDChequeAccion == id).ToList();
                        total = list8.Count();
                        foreach (var det in list8)
                        {
                            try
                            {
                                if (det.Accion == "Depositado" || det.Accion == "Ventanilla" || (det.Accion == "Debitado" && det.Cheques.EsPropio))
                                    ContabilidadCommon.AgregarAsientoChequeAccion(usu, det.IDChequeAccion);
                                ok++;
                            }
                            catch (Exception ex)
                            {
                                error++;
                                detErrores = ex.Message;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            html = "Se ha procesado con éxito " + ok + " de un total de " + total;
            if (error > 0)
                html += ". Se ha encontrado el siguiente errore: " + detErrores;

            return html;

        }

        #endregion

        private static int NroAsiento(ref int cantidad)
        {
            return cantidad++;
        }

        public static void InvertirAsientoDeCaja(WebUser usu, int idCaja)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    if (usu.UsaPlanCorporativo)//Plan Corporativo
                    {
                        Asientos asiento;
                        var caja = dbContext.Caja.Where(x => x.IDCaja == idCaja && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        asiento = dbContext.Asientos.Where(x => x.IDCaja == idCaja && x.IDUsuario == usu.IDUsuario).FirstOrDefault();
                        var asientoDebe = dbContext.AsientoDetalle.Where(x => x.IDAsiento == asiento.IDAsiento && x.TipoDeAsiento == "D").FirstOrDefault();
                        var asientoHaber = dbContext.AsientoDetalle.Where(x => x.IDAsiento == asiento.IDAsiento && x.TipoDeAsiento == "H").FirstOrDefault();

                        if (asiento != null)
                        {
                            asiento = new Asientos();
                            asiento.Fecha = caja.Fecha.Value.Date;
                            asiento.IDUsuario = usu.IDUsuario;
                            asiento.Leyenda = "Caja - Anulada";
                            asiento.IDCaja = idCaja;
                            asiento.EsAsientoCierre = false;
                            asiento.EsAsientoInicio = false; asiento.EsManual = false;
                            dbContext.Asientos.Add(asiento);
                        }

                        // cta Caja 
                        dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(caja.Importe), asientoDebe.IDPlanDeCuenta, asiento, "H"));

                        // cta Caja que selecciono el usuario
                        dbContext.AsientoDetalle.Add(insertarCuenta(Convert.ToDecimal(caja.Importe), Convert.ToInt32(asientoHaber.IDPlanDeCuenta), asiento, "D"));

                        if (validarPartidaDoble(asiento))
                            dbContext.SaveChanges();
                        else
                            throw new Exception("Los cuentas del debe no son iguales al haber.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string TotalesAsientosManuales(int IDAsiento, List<rptImpositivoLibroDiario> results, string tipo)
        {
            if (tipo == "D")
                return results.Where(x => x.IDAsiento == IDAsiento).Sum(x => x.Debe).ToMoneyFormat(2);
            else
                return results.Where(x => x.IDAsiento == IDAsiento).Sum(x => x.Haber).ToMoneyFormat(2);

        }

        private static bool EsCuentaPadre(ACHEEntities dbContext, int id)
        {
            var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];
            var ctaPadres = dbContext.PlanDeCuentas.Where(x => x.IDPadre == null && x.IDUsuario == usu.IDUsuario).ToList();
            var cta = dbContext.PlanDeCuentas.Where(x => x.IDPlanDeCuenta == id).FirstOrDefault();
            var listaCtas = dbContext.PlanDeCuentas.Where(x => x.IDUsuario == usu.IDUsuario).ToList();

            if (cta.IDPadre == null)
                return true;
            else
                return listaCtas.Any(x => x.IDPadre == cta.IDPlanDeCuenta);
        }

        private static List<PlanDeCuentasViewModel> armarLista(List<PlanDeCuentasViewModel> listaALLenar, List<PlanDeCuentasViewModel> listaConDatos, string cuentaPadre)
        {
            foreach (var item in listaConDatos)
            {

                listaALLenar.Add(
                        new PlanDeCuentasViewModel()
                        {
                            text = cuentaPadre,// + ((x.Codigo == "1") ? "<span class='spanDetalle' style='float: right; margin-right: 20px;'>Débitos / Créditos</span>" : ""),
                            Codigo = item.Codigo,

                            Nombre = item.Nombre.ToProperCase(),
                            AdminiteAsientoManual = item.AdminiteAsientoManual,
                            TipoDeCuenta = item.TipoDeCuenta
                        });
                if (item.children != null && item.children.Any())
                    armarLista(listaALLenar, item.children, item.Codigo);
            }
            return listaALLenar;

        }

        public static void CierreContable(WebUser usu)
        {
            var fechaDesde = DateTime.Now.Date.ToString("dd/MM/yyyy");
            var fechaCierre = DateTime.Now.Date;

            using (var dbContext = new ACHEEntities())
            {
                var aux = dbContext.Usuarios.Where(x => x.IDUsuario == usu.IDUsuario).First();
                if (aux.FechaCierreContable.HasValue)
                {
                    fechaDesde = aux.FechaCierreContable.Value.AddDays(1).AddYears(-1).ToString("dd/MM/yyyy");
                    fechaCierre = aux.FechaCierreContable.Value;
                }
                else
                    throw new Exception("Por favor, configure desde la sección Mi Cuenta la fecha de cierre contable");
            }

            var fechaHasta = fechaCierre.Date.ToString("dd/MM/yyyy");

            ContabilidadCommon.AgregarAsientoCierreDelEjercicio(ContabilidadCommon.ObtenerBalanceDeResultados(usu.IDUsuario, fechaDesde, fechaHasta, 1, 10000000), fechaCierre, usu);
            ContabilidadCommon.AgregarAsientoInicioDelEjercicio(fechaCierre.AddDays(1), usu);
        }


        //public static bool UsaPlanContable(ACHEEntities dbContext, int idusuario, string CondicionIVA)
        //{
        //    bool UsaPlanDeCuenta = false;
        //    var plandeCuenta = PermisosModulosCommon.ObtenerPlanActual(dbContext, idusuario);
        //    if (plandeCuenta != null)
        //    {
        //        if (plandeCuenta.IDPlan == 5 && CondicionIVA == "RI") //Plan Corporativo
        //            UsaPlanDeCuenta = true;
        //    }
        //    return UsaPlanDeCuenta;
        //}
        //public static bool UsaPlanContable(int idusuario, string CondicionIVA)
        //{
        //    using (var dbContext = new ACHEEntities())
        //    {
        //        bool UsaPlanDeCuenta = false;
        //        var plandeCuenta = PermisosModulosCommon.ObtenerPlanActual(dbContext, idusuario);
        //        if (plandeCuenta != null)
        //        {
        //            if (plandeCuenta.IDPlan == 5 && CondicionIVA == "RI") //Plan Corporativo
        //                UsaPlanDeCuenta = true;
        //        }
        //        return UsaPlanDeCuenta;
        //    }
        //}
    }
}

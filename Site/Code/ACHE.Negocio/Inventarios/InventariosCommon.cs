﻿using ACHE.Extensions;
using ACHE.Model;
using ACHE.Model.ViewModels;
using ACHE.Negocio.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.IO;
using ACHE.Negocio.Common;
using System.Collections.Specialized;
using System.Net.Mail;
using ACHE.Model.Negocio;
using ClosedXML.Excel;
using ACHE.Negocio.Productos;



namespace ACHE.Negocio.Inventario
{
    public enum TipoMovimientoStock
    {
        Compra,
        Comprobante,
        CambioDeposito,
        ModificacionStock,
        Reserva
    }
    public class InventariosCommon
    {

        #region ABM Inventarios

        
        public static bool InventarioEnUso(int idInventario)
        {
            bool inventarioEnUso = true;

            using (var dbContext = new ACHEEntities())
            {

                //var entity = dbContext.MovimientosDeStock.Where(x => x.IDInventarioDestino == idInventario || x.IDInventarioOrigen == idInventario).FirstOrDefault();

                var entity = dbContext.StockConceptos.Where(x => x.IDInventario == idInventario).FirstOrDefault();
                if (entity == null)
                {
                    inventarioEnUso = false;
                }
                else
                    inventarioEnUso = true;
            }

            return inventarioEnUso;
        }

        #endregion

        #region Inventarios Manual

        public static void ActualizarStockMasivo(int idInventario, string descripcion, int idUsuario, List<MovimientosDeStockDetalle> listaConceptos)
        {//La lista de conceptos debe tener la cantidad (en positivo) que se esta enviando de cada producto.
            try
            {

                using (var dbContext = new ACHEEntities())
                {
                    var InventarioOrigen = dbContext.Inventarios.FirstOrDefault(x => x.IDInventario == idInventario && x.IDUsuario == idUsuario);
                    if (InventarioOrigen == null)
                        throw new Exception("Inventario inexistente");

                    MovimientosDeStock entity = new MovimientosDeStock();
                    entity.IDInventarioOrigen = idInventario;
                    entity.IDInventarioDestino = null;
                    entity.FechaMovimiento = DateTime.Now;
                    entity.IDUsuario = idUsuario;
                    entity.Descripcion = descripcion != "" && descripcion != null ? descripcion : "Modificacion Stock";
                    dbContext.MovimientosDeStock.Add(entity);

                    foreach (var item in listaConceptos)
                    {
                        var stockInventarioConcepto = dbContext.StockConceptos.FirstOrDefault(x => x.IDConcepto == item.IDConcepto && x.IDInventario == idInventario);

                        decimal stockAnterior = 0;
                        if (stockInventarioConcepto != null)
                        {
                            stockAnterior = stockInventarioConcepto.Stock;
                            stockInventarioConcepto.Stock = item.CantidadTransferida;
                        }
                        else
                        {
                            stockInventarioConcepto = new StockConceptos();
                            stockInventarioConcepto.IDInventario = idInventario;
                            stockInventarioConcepto.IDConcepto = item.IDConcepto;
                            stockInventarioConcepto.Stock = item.CantidadTransferida;
                            dbContext.StockConceptos.Add(stockInventarioConcepto);
                        }

                        GuardarHistorialStock(idUsuario, item.IDConcepto, idInventario, TipoMovimientoStock.ModificacionStock, null, null, item.CantidadTransferida, item.CantidadTransferida, null, entity.Descripcion);

                        MovimientosDeStockDetalle det = new MovimientosDeStockDetalle();
                        det.IDConcepto = item.IDConcepto;
                        det.IDMovimiento = entity.IDMovimiento;

                        det.CantidadTransferida = item.CantidadTransferida - stockAnterior;
                        det.StockAnterior = stockAnterior;
                        dbContext.MovimientosDeStockDetalle.Add(det);

                       
                    }

                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void GenerarMovimientoStockEntreInventarios(int idInventarioOrigen, int idInventarioDestino, string descripcion, int idUsuario, List<MovimientosDeStockDetalle> listaConceptos)
        {//La lista de conceptos debe tener la cantidad (en positivo) que se esta enviando de cada producto.
            try
            {

                using (var dbContext = new ACHEEntities())
                {
                    var InventarioOrigen = dbContext.Inventarios.Where(x => x.IDInventario == idInventarioOrigen && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (InventarioOrigen == null)
                        throw new Exception("Inventario inexistente");

                    var InventarioDestino = dbContext.Inventarios.Where(x => x.IDInventario == idInventarioDestino && x.IDUsuario == idUsuario).FirstOrDefault();
                    if (InventarioOrigen == null)
                        throw new Exception("Inventario inexistente");

                    if (InventarioOrigen != null && InventarioDestino != null)
                    {

                        MovimientosDeStock entity = new MovimientosDeStock();
                        entity.IDInventarioOrigen = idInventarioOrigen;
                        entity.IDInventarioDestino = idInventarioDestino;
                        entity.FechaMovimiento = DateTime.Now;
                        entity.IDUsuario = idUsuario;
                        entity.Descripcion = descripcion != "" && descripcion != null ? descripcion : "Transferencia Stock";
                        dbContext.MovimientosDeStock.Add(entity);

                        GenerarMovimientosStockDetalle(dbContext, idUsuario, listaConceptos, entity.IDMovimiento, idInventarioOrigen, idInventarioDestino);//La lista de conceptos debe tener la cantidad (en positivo) que se esta enviando de cada producto.
                        dbContext.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void GenerarMovimientosStockDetalle(ACHEEntities dbContext, int idUsuario, List<MovimientosDeStockDetalle> listaConceptos, int idMovimiento, int idInventarioOrigen, int idInventarioDestino)
        {//La lista de conceptos debe tener la cantidad (en positivo) que se esta enviando de cada producto.
            try
            {
                foreach (var concepto in listaConceptos)
                {

                    MovimientosDeStockDetalle entity = new MovimientosDeStockDetalle();
                    entity.IDConcepto = concepto.IDConcepto;
                    entity.IDMovimiento = idMovimiento;
                    entity.CantidadTransferida = concepto.CantidadTransferida;
                    dbContext.MovimientosDeStockDetalle.Add(entity);

                    ActualizarStockConceptos(dbContext, idUsuario, idInventarioOrigen, concepto.IDConcepto, false, concepto.CantidadTransferida, TipoMovimientoStock.CambioDeposito, null, null);//Se descuenta el stock del inventario origen, se considera como una venta.
                    ActualizarStockConceptos(dbContext, idUsuario, idInventarioDestino, concepto.IDConcepto, true, concepto.CantidadTransferida, TipoMovimientoStock.CambioDeposito, null, null);//Se aumenta el stock del inventario destion, se considera como una compra.                  
                }
                //dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static List<StockConceptosViewModel> ObtenerStockConceptosInventario(int id)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.StockConceptos.Include("Conceptos").Where(x => x.IDInventario == id).Select(x => new StockConceptosViewModel
                {
                    Nombre = x.Conceptos.Nombre,
                    Codigo = x.Conceptos.Codigo,
                    Cantidad = x.Stock
                }).ToList();
            }

        }

        public static List<StockConceptosViewModel> ObtenerStockDetallado(int id)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.StockConceptos.Include("Inventarios").Where(x => x.IDConcepto == id).Select(x => new StockConceptosViewModel
                {
                    Nombre = x.Inventarios.Nombre,
                    Cantidad = x.Stock
                }).ToList();
            }
        }


        #endregion

        public static InventarioViewModel ObtenerInventario(int id)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    InventarioViewModel entity;
                    entity = dbContext.Inventarios.ToList().Where(x => x.IDInventario == id).Select(x => new InventarioViewModel
                    {
                        ID = x.IDInventario,
                        Nombre = x.Nombre,
                        IDProvincia = x.IDProvincia,
                        IDCiudad = x.IDCiudad,
                        IDUsuario = x.IDUsuario,
                        Activo = x.Activo ? "SI" : "NO",
                        PorDefectoReservas = x.PorDefectoReservas ? "SI" : "NO",
                        Direccion = x.Direccion,
                        Telefono = x.Telefono,
                        FechaAlta = x.FechaAlta.ToString("dd/MM/yyyy"),
                        FechaUltimoInventario = x.FechaUltimoInventarioFisico.ToString("dd/MM/yyyy")

                    }).FirstOrDefault();
                    return entity;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static ResultadosInventarioViewModel ObtenerInventarios(string condicion, string estado, int page, int pageSize, int idUsuario)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = ObtenerInventarios(idUsuario, condicion, estado).AsQueryable();

                    //Obtenemos el formato decimal
                    var decimalFormat = DecimalFormatter.GetDecimalStringFormat(idUsuario);

                    page--;
                    ResultadosInventarioViewModel resultado = new ResultadosInventarioViewModel();

                    var list = results.OrderBy(x => x.Nombre).Skip(page * pageSize).Take(pageSize).ToList();

                    resultado.TotalPage = ((list.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = list.Count();
                    resultado.Items = list.ToList();
                    return resultado;
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static List<InventarioViewModel> ObtenerInventarios(int idUsuario, string condicion, string estado)
        {
            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.Inventarios.Where(x => x.IDUsuario == idUsuario).AsQueryable();

                    if (!string.IsNullOrWhiteSpace(condicion))
                        results = results.Where(x => x.Nombre.ToLower().Contains(condicion.ToLower()));

                    if (!string.IsNullOrWhiteSpace(estado))
                        if (estado == "1")
                            results = results.Where(x => x.Activo);
                        else
                            results = results.Where(x => !x.Activo);

                    ResultadosInventarioViewModel resultado = new ResultadosInventarioViewModel();

                    var list = results.OrderBy(x => x.Nombre).ToList()
                     .Select(x => new InventarioViewModel()
                     {
                         ID = x.IDInventario,
                         Nombre = x.Nombre.ToUpper(),
                         IDProvincia = x.IDProvincia,
                         IDCiudad = x.IDCiudad,
                         IDUsuario = x.IDUsuario,
                         Activo = (Convert.ToBoolean(x.Activo)) ? "SI" : "NO",
                         PorDefectoReservas = (Convert.ToBoolean(x.PorDefectoReservas)) ? "SI" : "NO",
                         Direccion = x.Direccion,
                         Telefono = x.Telefono,
                         FechaAlta = x.FechaAlta.ToString("dd/MM/yyyy"),
                         FechaUltimoInventario = x.FechaUltimoInventarioFisico.ToString("dd/MM/yyyy")
                     });

                    return list.ToList();
                }
            }
            catch (CustomException e)
            {
                throw new CustomException(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static void Facturar(int idUsuario, List<MovimientosDeStockDetalle> listaConceptos, int idInventario, bool compra)
        {//La lista de conceptos debe tener la cantidad (en positivo) que se esta enviando de cada producto.

            try
            {
                using (var dbContext = new ACHEEntities())
                {
                    foreach (var concepto in listaConceptos)
                    {

                        //MovimientosDeStockDetalle entity = new MovimientosDeStockDetalle();
                        //entity.IDConcepto = concepto.IDConcepto;
                        //entity.IDMovimiento = idMovimiento;
                        //entity.CantidadTransferida = concepto.CantidadTransferida;
                        //dbContext.MovimientosDeStockDetalle.Add(entity);
                        ActualizarStockConceptos(dbContext, idUsuario, idInventario, concepto.IDConcepto, compra, concepto.CantidadTransferida, TipoMovimientoStock.CambioDeposito, null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void enviarMail(Usuarios usu, StockConceptos stockConcepto)
        {
            string mensaje = "El concepto " + stockConcepto.Conceptos.Nombre + " de su inventario " + stockConcepto.Inventarios + " ha alcanzado el mínimo de stock";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<USUARIO>", usu.RazonSocial);
            replacements.Add("<NOTIFICACION>", mensaje);

            bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.NotificacionDeContabilium, replacements, ConfigurationManager.AppSettings["Email.FromAlertas"], usu.Email, "Alerta de stock mínimo");
            if (!send)
                throw new Exception("El mail no pudo ser enviado.");
        }

        public static ConceptosViewModel obtenerConcepto(int idUsuario, int idInventario, int idConcepto)
        {
            using (var dbContext = new ACHEEntities())
            {
                //se debe restar la reserva
                var porDefectoReservas = dbContext.Inventarios.Any(
                i => i.IDInventario == idInventario && i.PorDefectoReservas);
                decimal reserva = 0;
                if (porDefectoReservas)
                {
                    var stockConceptosReserva = dbContext.StockConceptosReserva.FirstOrDefault(r => r.IDUsuario == idUsuario && r.IDConcepto == idConcepto);
                    if (
                        stockConceptosReserva != null)
                        reserva = stockConceptosReserva.Reserva;
                }


                StockConceptos entity = dbContext.StockConceptos.Where(x => x.IDInventario == idInventario && x.IDConcepto == idConcepto).FirstOrDefault();
                if (entity != null)
                {
                    ConceptosViewModel result = new ConceptosViewModel();
                    result.Nombre = (entity.Stock).ToString();
                    result.Stock = (entity.Stock).ToString();
                    result.StockReservado = reserva.ToString();
                    result.ID = entity.IDConcepto;

                    return result;
                }
                else
                    throw new Exception("Error al obtener los datos");
            }
        }

        #region Manejo Stock

        private static void ActualizarStockConceptos(ACHEEntities dbContext, int idUsuario, int idInventario, int idConcepto, bool suma, decimal cantidad, TipoMovimientoStock tipoMovimiento, int? idComprobante, int? idCompra)
        {
            string key = idUsuario + "-" + idConcepto + "-" + idInventario;
            lock (string.Intern(key))
            {

                var stockInventarioConcepto = dbContext.StockConceptos.FirstOrDefault(x => x.IDConcepto == idConcepto && x.IDInventario == idInventario);
                decimal stockActualizado = 0;
                if (stockInventarioConcepto != null)
                {
                    if (suma)
                    {
                        stockInventarioConcepto.Stock += cantidad;
                        stockActualizado = stockInventarioConcepto.Stock;
                    }
                    else
                    {
                        stockInventarioConcepto.Stock -= cantidad;
                        stockActualizado = stockInventarioConcepto.Stock;
                    }
                }
                //validarStockMinimo(idInventario, idConcepto);                }
                else
                {
                    StockConceptos entity = new StockConceptos();
                    entity.IDInventario = idInventario;
                    entity.IDConcepto = idConcepto;
                    if (suma)
                    {
                        entity.Stock = cantidad;
                        stockActualizado = entity.Stock;
                    }
                    else
                    {
                        entity.Stock = -cantidad;
                        stockActualizado = entity.Stock;
                    }
                    dbContext.StockConceptos.Add(entity);
                }

                //dbContext.SaveChanges();
                //GUARDO HISTORIAL
                var cantHistorial = suma ? cantidad : cantidad * -1;


                GuardarHistorialStock(idUsuario, idConcepto, idInventario, tipoMovimiento, idComprobante, idCompra, cantHistorial, stockActualizado, null, null);

                
            }
        }

        private static void GuardarHistorialStock(int idUsuario, int idConcepto, int idInventario, TipoMovimientoStock tipoMovimiento, int? idComprobante, int? idCompra, decimal cantidad, decimal? stockActualizado, decimal? reservaActualizada, string descripcion)
        {
            using (var dbContext = new ACHEEntities())
            {

                var historial = new StockHistorial();
                historial.IDUsuario = idUsuario;
                historial.Fecha = DateTime.Now;
                historial.IDConcepto = idConcepto;
                historial.IDInventario = idInventario;
                historial.TipoMovimiento = tipoMovimiento.ToString();
                historial.IDComprobante = idComprobante;
                historial.IDCompra = idCompra;
                historial.Cantidad = cantidad;
                historial.StockActualizado = stockActualizado;
                historial.ReservaActualizada = reservaActualizada;
                historial.Descripcion = descripcion;
                dbContext.StockHistorial.Add(historial);
                dbContext.SaveChanges();
            }

        }

        public static bool validarStockMinimo(int idInventario, int idConcepto)
        {
            using (var dbContext = new ACHEEntities())
            {
                var entity = dbContext.StockConceptos.Include("Conceptos").FirstOrDefault(x => x.IDInventario == idInventario && x.IDConcepto == idConcepto);
                if (entity != null && entity.Conceptos != null)
                {
                    //validar reserva
                    decimal reserva = 0;
                    var porDefectoReservas =
                        dbContext.Inventarios.Any(i => i.IDInventario == idInventario && i.PorDefectoReservas);
                    if (porDefectoReservas)
                    {
                        var stockConceptosReserva =
                            dbContext.StockConceptosReserva.FirstOrDefault(
                                r => r.IDUsuario == entity.Inventarios.Usuarios.IDUsuario && r.IDConcepto == idConcepto);
                        if (stockConceptosReserva != null)
                            reserva = stockConceptosReserva.Reserva;
                    }


                    var stock = entity.Stock - reserva;
                    if (stock <= entity.Conceptos.StockMinimo)
                        return false;
                    return true;
                }
                return true;
            }
        }

        public static StockDto obtenerStockProducto(int idUsuario, int idInventario, int idConcepto)
        {
            //Este metodo es solo para productos
            using (var dbContext = new ACHEEntities())
            {
                //se debe restar la reserva. 2018.05.15: Comento esto para que siempre muestre el stock reservado
                //var porDefectoReservas = dbContext.Inventarios.Any(
                //i => i.IDInventario == idInventario && i.PorDefectoReservas);
                decimal reserva = 0;
                //if (porDefectoReservas)
                //{
                var stockConceptosReserva = dbContext.StockConceptosReserva.FirstOrDefault(r => r.IDUsuario == idUsuario && r.IDConcepto == idConcepto);
                if (
                    stockConceptosReserva != null)
                    reserva = stockConceptosReserva.Reserva;
                //}
                //fix: se duplican cosas en StockConceptos
                var listAux = dbContext.StockConceptos.Where(x => x.IDInventario == idInventario && x.IDConcepto == idConcepto).ToList();
                if (listAux.Any())
                {
                    if (listAux.Count() == 1)
                    {
                        return new StockDto
                        {
                            StockActual = listAux[0].Stock,
                            StockReservado = reserva
                        };
                    }
                    else
                    {
                        var total = listAux[0].Stock;

                        listAux.RemoveAt(0);
                        dbContext.StockConceptos.RemoveRange(listAux);
                        dbContext.SaveChanges();

                        return new StockDto
                        {
                            StockActual = total,
                            StockReservado = reserva
                        };
                    }
                }
                else
                {
                    return new StockDto
                    {
                        StockActual = 0,
                        StockReservado = 0
                    };
                }

                //var total = dbContext.StockConceptos.Where(x => x.IDInventario == idInventario && x.IDConcepto == idConcepto).Select(x => x.Stock).DefaultIfEmpty(0).Sum();
                //return new StockDto
                //{
                //    StockActual = total,
                //    StockReservado = reserva
                //};
            }
        }

        public static void ActualizarStockComprobante(ACHEEntities dbContext, string tipoComprobante, int idUsuario,
            List<ComprobantesDetalle> comprobanteDetalle, int idInventario, int idComprobante)
        {
            if (tipoComprobante == "NCA" || tipoComprobante == "NCB" || tipoComprobante == "NCC" || tipoComprobante == "NCE")
            {
                InventariosCommon.SumarStockComprobante(dbContext, idUsuario, comprobanteDetalle, idInventario, idComprobante);
            }
            else
            {
                InventariosCommon.RestarStockComprobante(dbContext, idUsuario, comprobanteDetalle, idInventario, idComprobante);
            }
        }

        public static void RestaurarStockComprobante(ACHEEntities dbContext, string tipoComprobante, int idUsuario,
           List<ComprobantesDetalle> comprobanteDetalle, int idInventario, int idComprobante)
        {
            if (tipoComprobante == "NCA" || tipoComprobante == "NCB" || tipoComprobante == "NCC" || tipoComprobante == "NCE")
            {
                InventariosCommon.RestarStockComprobante(dbContext, idUsuario, comprobanteDetalle, idInventario, idComprobante);

            }
            else
            {
                InventariosCommon.SumarStockComprobante(dbContext, idUsuario, comprobanteDetalle, idInventario, idComprobante);
            }
        }

        public static void RestaurarStockCompra(ACHEEntities dbContext, int idUsuario, string tipoComprobante, int idCompra, int idPersona,
        List<DetalleViewModel> items, List<ComprasDetalle> comprasDetalle, int idInventario)
        {
            if (tipoComprobante == "NCA" || tipoComprobante == "NCB" || tipoComprobante == "NCC" || tipoComprobante == "NCE")
                InventariosCommon.SumarStockCompraYModificarPrecio(dbContext, idUsuario, items, idPersona, idInventario, idCompra);

            else
                InventariosCommon.RestarStockCompra(dbContext, idUsuario, comprasDetalle, idInventario, idCompra);
        }

        public static void ActualizarStockCompra(ACHEEntities dbContext, int idUsuario, string tipoComprobante, int idCompra, int idPersona,
        List<DetalleViewModel> items, int idInventario)
        {
            if (tipoComprobante == "NCA" || tipoComprobante == "NCB" || tipoComprobante == "NCC" || tipoComprobante == "NCE")
                InventariosCommon.RestarStockCompra(dbContext, idUsuario, items, idInventario, idCompra);
            else
                InventariosCommon.SumarStockCompraYModificarPrecio(dbContext, idUsuario, items, idPersona, idInventario,
                    idCompra);
        }

        public static void RestarStockComprobante(ACHEEntities dbContext, int idUsuario, List<ComprobantesDetalle> comprobanteDetalle, int idInventario, int idComprobante)
        {

            foreach (var item in comprobanteDetalle)
            {
                var idConcepto = item.IDConcepto;
                var cantidad = item.Cantidad;
                if (idConcepto != null)
                {
                    RestarStock(dbContext, idUsuario, idInventario, idConcepto, cantidad, false, idComprobante, null, TipoMovimientoStock.Comprobante);
                }
            }
        }

        public static void RestarStockCompra(ACHEEntities dbContext, int idUsuario, List<DetalleViewModel> detalles, int idInventario, int idCompra)
        {
            foreach (var item in detalles)
            {
                var idConcepto = item.IDConcepto;
                var cantidad = item.Cantidad;
                if (idConcepto != null)
                {
                    RestarStock(dbContext, idUsuario, idInventario, idConcepto, cantidad, false, null, idCompra, TipoMovimientoStock.Comprobante);
                }
            }
        }

        public static void RestarStockCompra(ACHEEntities dbContext, int idUsuario, List<ComprasDetalle> ComprasDetalle, int idInventario, int idCompra)
        {
            foreach (var item in ComprasDetalle)
            {
                var idConcepto = item.IDConcepto;
                var cantidad = item.Cantidad;
                if (idConcepto != null)
                {
                    RestarStock(dbContext, idUsuario, idInventario, idConcepto, cantidad, false, null, idCompra, TipoMovimientoStock.Compra);
                }
            }
        }

        private static void RestarStock(ACHEEntities dbContext, int idUsuario, int idInventario, int? idConcepto, decimal cantidad, bool suma, int? idComprobante, int? idCompra, TipoMovimientoStock tipoMovimiento)
        {
            //se obtiene producto o combo
            var c = dbContext.Conceptos
                    .Include("ComboConceptos")
                    .FirstOrDefault(x => x.IDConcepto == idConcepto && x.Tipo != "S");

            if (c != null)
            {
                //si es producto
                if (c.Tipo == "P")
                    InventariosCommon.ActualizarStockConceptos(dbContext, idUsuario, idInventario, idConcepto ?? 0, suma, cantidad, tipoMovimiento, idComprobante, idCompra);
                //si es combo
                if (c.ComboConceptos.Any())
                {
                    foreach (var comboConceptose in c.ComboConceptos.Where(x => x.Conceptos1.Tipo == "P"))
                    {

                        InventariosCommon.ActualizarStockConceptos(dbContext, idUsuario, idInventario,
                            comboConceptose.Conceptos1.IDConcepto, suma, cantidad * comboConceptose.Cantidad, tipoMovimiento, idComprobante, idCompra);

                    }
                }
            }
        }

        public static void SumarStockComprobante(ACHEEntities dbContext, int idUsuario, List<ComprobantesDetalle> comprobanteDetalle, int idInventario, int idComprobante)
        {
            foreach (var item in comprobanteDetalle)
            {
                var idConcepto = item.IDConcepto;
                var cantidad = item.Cantidad;
                if (idConcepto != null)
                {
                    SumarStock(dbContext, idUsuario, idInventario, idConcepto, cantidad, true, idComprobante, null, TipoMovimientoStock.Comprobante);
                }
            }
        }

        public static void SumarStockCompra(ACHEEntities dbContext, int idUsuario, List<DetalleViewModel> detalles, int idInventario, int idCompra)
        {
            foreach (var item in detalles)
            {
                var idConcepto = item.IDConcepto;
                var cantidad = item.Cantidad;
                if (idConcepto != null)
                {
                    SumarStock(dbContext, idUsuario, idInventario, idConcepto, cantidad, true, null, idCompra, TipoMovimientoStock.Compra);
                }
            }
        }

        private static void SumarStock(ACHEEntities dbContext, int idUsuario, int idInventario, int? idConcepto, decimal cantidad, bool suma, int? idComprobante, int? idCompra, TipoMovimientoStock tipoMovimiento)
        {
            //Se obtiene combo o producto
            var c = dbContext.Conceptos
                    .Include("ComboConceptos")
                    .FirstOrDefault(x => x.IDConcepto == idConcepto && x.Tipo != "S");
            if (c != null)
            {
                //Si es producto
                if (c.Tipo == "P")
                    InventariosCommon.ActualizarStockConceptos(dbContext, idUsuario, idInventario, idConcepto ?? 0, suma, cantidad, tipoMovimiento, idComprobante, idCompra);
                //si es combo
                if (c.ComboConceptos.Any())
                {
                    foreach (var comboConceptose in c.ComboConceptos.Where(x => x.Conceptos1.Tipo == "P"))
                    {

                        InventariosCommon.ActualizarStockConceptos(dbContext, idUsuario, idInventario,
                            comboConceptose.Conceptos1.IDConcepto, suma, cantidad * comboConceptose.Cantidad, tipoMovimiento, idComprobante, idCompra);

                    }
                }
            }
        }

        public static void SumarStockCompraYModificarPrecio(ACHEEntities dbContext, int idUsuario, List<DetalleViewModel> detalles, int idPersona, int idInventario, int idCompra)
        {
            foreach (var item in detalles)
            {
                if (item.IDConcepto != null)
                {
                    //Se obtiene combo o producto
                    var c = dbContext.Conceptos.Include("ComboConceptos").FirstOrDefault(x => x.IDConcepto == item.IDConcepto && x.Tipo != "S");
                    if (c != null)
                    {
                        //Si es producto
                        if (c.Tipo == "P")
                            InventariosCommon.ActualizarStockConceptos(dbContext, idUsuario, idInventario, item.IDConcepto ?? 0, true, item.Cantidad, TipoMovimientoStock.Compra, null, idCompra);

                        //si es combo
                        if (c.ComboConceptos.Any())
                        {
                            foreach (var comboConceptose in c.ComboConceptos.Where(x => x.Conceptos1.Tipo == "P"))
                            {
                                InventariosCommon.ActualizarStockConceptos(dbContext, idUsuario, idInventario,
                                    comboConceptose.Conceptos1.IDConcepto, true, item.Cantidad * comboConceptose.Cantidad, TipoMovimientoStock.Compra, null, idCompra);
                            }
                        }

                        //se actualiza el precio
                        if (item.PrecioUnitario != c.CostoInterno)
                        {
                            c.CostoInterno = item.PrecioUnitarioSinIVA;
                            var usu = dbContext.Usuarios.Where(x => x.IDUsuario == c.IDUsuario).FirstOrDefault();

                            if (c.Rentabilidad > 0 && c.CostoInterno.HasValue)
                            {
                                c.PrecioUnitario = (c.CostoInterno.Value + ((c.CostoInterno.Value * c.Rentabilidad) / 100));

                                if (usu.UsaPrecioFinalConIVA && c.Iva > 0)
                                    c.PrecioUnitario = c.PrecioUnitario * (1 + (c.Iva / 100));
                            }

                            PreciosHistorial historial = new PreciosHistorial();
                            historial.IDConcepto = c.IDConcepto;
                            historial.IDUsuario = c.IDUsuario;
                            historial.Fecha = DateTime.Now;
                            historial.CostoInterno = c.CostoInterno;
                            historial.PrecioUnitario = c.PrecioUnitario;

                            historial.IDPersona = idPersona;
                            historial.UsuarioAlta = usu.Email;
                            dbContext.PreciosHistorial.Add(historial);
                        }
                    }
                }
            }
        }


        public static List<string> StockValido(ACHEEntities dbContext, List<ComprobantesDetalleViewModel> Detalle, int idUsuario, int idInventario, ComprobanteCartDto entity)
        {
            bool esValido = true;
            //si es edicion cambiar la validacion!!
            List<string> prodsinstock = new List<string>();
            var VenderSinStock = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).Select(x => x.VenderSinStock).FirstOrDefault();
            if (!VenderSinStock)
            {
                var list = Detalle.Where(x => x.TipoConcepto != "S").GroupBy(x => x.IDConcepto).Select(x => new
                {
                    IDConcepto = x.FirstOrDefault().IDConcepto,
                    Cantidad = x.Sum(y => y.Cantidad),
                    Concepto = x.FirstOrDefault().Codigo
                }).ToList();

                var porDefectoReservas = dbContext.Inventarios.Any(
                    i => i.IDInventario == idInventario && i.PorDefectoReservas);

                foreach (var item in list)
                {
                    if (item.IDConcepto != null)
                    {
                        var c = dbContext.Conceptos.Include("ComboConceptos").FirstOrDefault(x => x.IDConcepto == item.IDConcepto);
                        if (c != null)
                        {
                            //producto
                            if (c.Tipo == "P")
                            {
                                ValidarStock(dbContext, idUsuario, idInventario, entity, prodsinstock, item.Cantidad, item.IDConcepto.Value, item.Concepto, porDefectoReservas);

                            }
                            else
                            {//combo
                                foreach (var itemCombo in c.ComboConceptos.Where(x => x.Conceptos1.Tipo == "P").ToList())
                                {
                                    ValidarStock(dbContext, idUsuario, idInventario, entity, prodsinstock, itemCombo.Cantidad * item.Cantidad, itemCombo.IDConceptoItem, item.Concepto, porDefectoReservas);

                                }
                            }
                        }
                    }
                }
            }

            return prodsinstock;
        }

        private static void ValidarStock(ACHEEntities dbContext, int idUsuario, int idInventario, ComprobanteCartDto entity,
             List<string> prodsinstock, decimal itemCantidad, int itemIdConcepto, string itemCodigoConcepto, bool porDefectoReservas)
        {
            //restar reserva
            decimal reserva = 0;
            if (porDefectoReservas)
            {
                var stockConceptosReserva = dbContext.StockConceptosReserva.FirstOrDefault(r => r.IDUsuario == idUsuario && r.IDConcepto == itemIdConcepto);
                if (
                    stockConceptosReserva != null)
                    reserva = stockConceptosReserva.Reserva;
            }

            var stock =
                dbContext.StockConceptos.Where(x => x.IDInventario == idInventario && x.IDConcepto == itemIdConcepto && x.Inventarios.Activo)
                    .Select(x => x.Stock)
                    .FirstOrDefault();
            stock = stock - reserva;

            var cantAnt =
                dbContext.ComprobantesDetalle.Where(x => x.IDConcepto == itemIdConcepto && x.IDComprobante == entity.IDComprobante)
                    .Select(x => x.Cantidad)
                    .FirstOrDefault();
            decimal aux = 0;
            if (entity.IDComprobante > 0)
                aux = (stock + cantAnt) - itemCantidad;
            else
                aux = stock - itemCantidad;
            if (aux < 0)
                prodsinstock.Add(itemCodigoConcepto);
        }

        public static StockDto ObtenerStockTotal(Conceptos concepto)
        {
            using (var dbContext = new ACHEEntities())
            {

                if (concepto.Tipo == "P")
                {
                    var reserva = dbContext.StockConceptosReserva.Where(r => r.IDConcepto == concepto.IDConcepto)
                        .Select(r => r.Reserva)
                        .DefaultIfEmpty(0)
                        .Sum();
                    var stock = concepto.StockConceptos.Count == 0
                        ? (decimal?)null
                        : concepto.StockConceptos.Where(s => s.Inventarios.Activo).Sum(s => s.Stock);
                    return new StockDto { StockActual = stock ?? 0, StockReservado = reserva };
                }
                else //es combo
                {
                    decimal cantCombo = -1; //inicializo en -1
                    //decimal cantReserva = 0;

                    var cantReserva = dbContext.StockConceptosReserva.Where(r => r.IDConcepto == concepto.IDConcepto)
                       .Select(r => r.Reserva)
                       .DefaultIfEmpty(0)
                       .Sum();


                    //using (var dbContext = new ACHEEntities())
                    //{
                    try
                    {
                        foreach (var itemCombo in concepto.ComboConceptos.Where(x => x.Conceptos1.Tipo == "P").ToList())
                        {
                            if (itemCombo.Conceptos1 != null)
                            {
                                var stockConcepto =
                                    itemCombo.Conceptos1.StockConceptos.Where(
                                        x => x.Inventarios.Activo && x.IDConcepto == itemCombo.IDConceptoItem)
                                        .Select(x => x.Stock)
                                       .DefaultIfEmpty(0)
                                       .Sum();

                                var reserva = dbContext.StockConceptosReserva.Where(r => r.IDConcepto == itemCombo.IDConceptoItem)
                                .Select(r => r.Reserva)
                                .DefaultIfEmpty(0)
                                .Sum();

                                if ((itemCombo.Cantidad) > 0)
                                {
                                    if (cantCombo == -1) //asigno el valor del primr item del combo
                                    {
                                        cantCombo = (stockConcepto - reserva) / itemCombo.Cantidad;
                                    }
                                    else if (cantCombo > 0)
                                    //si por ahora el combo tiene stock solo asigno el valor cuando el mismo es menor. La cant disponible de un combo es siempr el valor menor posible.
                                    {
                                        if (((stockConcepto - reserva) / itemCombo.Cantidad) < cantCombo)
                                        {
                                            cantCombo = (stockConcepto - reserva) / itemCombo.Cantidad;
                                            if (cantCombo < 0)
                                                cantCombo = 0;
                                        }
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        cantCombo = 0;
                    }

                    //un combo no tiene reservas, los productos si
                    return new StockDto { StockActual = Math.Round((decimal)((int)(cantCombo)), 0), StockReservado = cantReserva };
                }
            }
        }

        public static string ObtenerItems()
        {
            var html = string.Empty;
            var list = MovimientosStockCart.Retrieve().Items.OrderBy(x => x.IDMovimiento).ToList();
            if (list.Any())
            {
                int index = 1;
                foreach (var detalle in list)
                {
                    html += "<tr>";
                    html += "<td>" + index + "</td>";

                    html += "<td style='text-align:left'>" + detalle.Codigo + "</td>";
                    html += "<td style='text-align:left'>" + detalle.Nombre + "</td>";
                    html += "<td style='text-align:left'>" + detalle.StockActual.Replace(",0000", "") + "</td>";
                    html += "<td style='text-align:left'>" + detalle.CantidadTransferida.ToString("#0.00") + "</td>";

                    html += "<td><a title='Eliminar' style='font-size: 16px' href='javascript:modificacionStock.eliminarItem(" + detalle.IDMovimiento + ");'><i class='fa fa-times'></i></a>&nbsp;&nbsp;";
                    html += "<a title='Modificar' style='font-size: 16px' href=\"javascript:modificacionStock.modificarItem(" + detalle.IDMovimiento + ", '"
                        + detalle.IDConcepto.ToString()
                        + "' ,'" + detalle.CantidadTransferida.ToString().Replace(",", ".")
                        + "');\"><i class='fa fa-edit'></i></a></td>";
                    html += "</tr>";

                    index++;
                }
            }

            if (html == "")
                html = "<tr><td colspan='6' style='text-align:center'>No tienes items agregados</td></tr>";

            return html;
        }

        public static void AgregarItem(int idConcepto, int idInventario, string cantidad, int idUsuario)
        {

            if (idConcepto > 0)
            {
                var aux = MovimientosStockCart.Retrieve().Items.Where(x => x.IDConcepto == idConcepto).FirstOrDefault();
                MovimientosStockCart.Retrieve().Items.Remove(aux);
            }
            else
                throw new Exception("Seleccione un producto para poder continuar");

            if (idConcepto > 0 && idInventario > 0)
            {
                var stock = InventariosCommon.obtenerStockProducto(idUsuario, idInventario, idConcepto);
                if (decimal.Parse(cantidad.Replace(".", ",")) < stock.StockReservado)
                    throw new Exception(string.Format("El nuevo stock debe ser mayor o igual a la reserva."));

                var det = new MovimientoDetalleViewModel();
                det.IDMovimiento = MovimientosStockCart.Retrieve().Items.Count() + 1;
                det.CantidadTransferida = decimal.Parse(cantidad.Replace(".", ","));
                det.IDConcepto = idConcepto;

                var prod = ConceptosCommon.GetConcepto(idConcepto, idUsuario);
                var concepto = new Conceptos();
                det.Nombre = prod.Nombre;
                det.Codigo = prod.Codigo;
                det.StockActual = InventariosCommon.obtenerStockProducto(idUsuario, idInventario, idConcepto).StockActual.ToString();
                MovimientosStockCart.Retrieve().Items.Add(det);

            }
        }

        public static void EliminarItem(int id)
        {
            var aux = MovimientosStockCart.Retrieve().Items.Where(x => x.IDMovimiento == id).FirstOrDefault();
            if (aux != null)
                MovimientosStockCart.Retrieve().Items.Remove(aux);
        }

        public static void AlertaStock(ACHEEntities dbContext, int idComprobante, int idUsuario)
        {
            var detalle = dbContext.ComprobantesDetalle.Where(x => x.IDComprobante == idComprobante).ToList();
            var avisosYalertas = dbContext.AvisosVencimiento.Where(x => x.IDUsuario == idUsuario && x.TipoAlerta == "Stock" && x.Activa == true).FirstOrDefault();
            if (avisosYalertas != null)
            {
                if (PermisosModulos.tienePlan("alertas.aspx"))
                {
                    foreach (var item in detalle)
                    {
                        var conceptos = dbContext.Conceptos
                            .Include("StockConceptos")
                            .Include("StockConceptos.Inventarios")
                            .Where(x => x.IDUsuario == idUsuario && x.IDConcepto == item.IDConcepto && x.Tipo == "P").FirstOrDefault();

                        if (conceptos != null && conceptos.StockMinimo.HasValue)
                        {
                            var cantidadReserva = 0;
                            var reserva =
                                dbContext.StockConceptosReserva.FirstOrDefault(
                                    r => r.IDUsuario == idUsuario && r.IDConcepto == item.IDConcepto);
                            if (reserva != null)
                                cantidadReserva = (int)reserva.Reserva;

                            int stockActual = (int)conceptos.StockConceptos.Sum(x => x.Stock) - cantidadReserva;



                            if (item.Conceptos != null && stockActual <= conceptos.StockMinimo)
                            {
                                var mensaje = "El Producto " + item.Conceptos.Nombre + " - Código: " + item.Conceptos.Codigo + ", llegó a su stock mínimo: " +
                                              item.Conceptos.StockMinimo + ". Su stock actual es: " + stockActual + "<br>";

                                foreach (var dep in conceptos.StockConceptos)
                                {
                                    if (dep.Inventarios.PorDefectoReservas)
                                        mensaje += "<br>Depósito " + dep.Inventarios.Nombre + ": " + dep.Stock + " Reservado: " + reserva;
                                    else
                                        mensaje += "<br>Depósito " + dep.Inventarios.Nombre + ": " + dep.Stock;
                                }
                                var mailUsuario = dbContext.Usuarios.Where(x => x.IDUsuario == idUsuario).Select(x => x.EmailStock == "" ? x.Email : x.EmailStock).FirstOrDefault();
                                EnviarEmailAlertaStock("Alerta de stock mínimo", mensaje, mailUsuario);
                            }
                        }
                    }
                }
            }
        }

        public static void EnviarEmailAlertaStock(string alerta, string descripcion, string email)
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];


                MailAddressCollection listTo = new MailAddressCollection();

                foreach (var mail in email.Split(','))
                {
                    if (mail != string.Empty)
                    {
                        listTo.Add(new MailAddress(mail));
                    }
                }

                ListDictionary replacements = new ListDictionary();
                replacements.Add("<ALERTA>", alerta);
                replacements.Add("<USUARIO>", usu.RazonSocial);
                replacements.Add("<DESCRIPCION>", descripcion);
                replacements.Add("<EMAIL>", email);

                bool send = EmailCommon.SendMessage(usu.IDUsuario, EmailTemplate.Alertas, replacements, listTo, ConfigurationManager.AppSettings["Email.FromAlertas"], null, "Alerta de stock mínimo", null, "");
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }

        #endregion

        #region Manejo Reservas

        public static void SumarReservaStock(ACHEEntities dbContext, int idUsuario, int idConcepto, decimal cantidad, string descripcion)
        {
            //se obtiene producto o combo
            var concepto = dbContext.Conceptos
                .Include("ComboConceptos")
                .FirstOrDefault(x => x.IDConcepto == idConcepto && x.Tipo != "S");
            //se obtiene el inventario de reservas
            var invenario =
                dbContext.Inventarios.FirstOrDefault(i => i.IDUsuario == idUsuario && i.Activo && i.PorDefectoReservas);
            if (invenario == null)
                throw new Exception(string.Format("No existe un inventario de reservas para el usuario {0}", idUsuario));

            if (concepto != null)
            {
                //si es producto
                if (concepto.Tipo == "P" || concepto.Tipo == "C")
                    SumarReservaConceptos(dbContext, idUsuario, idConcepto, invenario.IDInventario, cantidad, concepto.Tipo);
                if (concepto.Tipo == "C")
                {
                    //si es combo se reservan los productos que pertenecen al combo
                    if (concepto.ComboConceptos.Any())
                    {
                        foreach (var comboConceptose in concepto.ComboConceptos.Where(x => x.Conceptos1.Tipo == "P"))
                        {
                            SumarReservaConceptos(dbContext, idUsuario, comboConceptose.Conceptos1.IDConcepto, invenario.IDInventario, cantidad * comboConceptose.Cantidad, "P");
                        }
                    }
                }
            }

        }

        public static void QuitarReservaStock(ACHEEntities dbContext, int idUsuario, int idConcepto, decimal cantidad, string descripcion)
        {

            //se obtiene producto o combo
            var concepto = dbContext.Conceptos
                .Include("ComboConceptos")
                .FirstOrDefault(x => x.IDConcepto == idConcepto && x.Tipo != "S");

            if (concepto != null)
            {
                //si es producto o concepto
                if (concepto.Tipo == "P" || concepto.Tipo == "C")
                    RestarReservaConceptos(dbContext, idUsuario, idConcepto, cantidad, concepto.Tipo);

                if (concepto.Tipo == "C")
                {
                    //si es combo se restan las reservas de los productos que pertenecen al combo
                    if (concepto.ComboConceptos.Any())
                    {
                        foreach (var comboConceptose in concepto.ComboConceptos.Where(x => x.Conceptos1.Tipo == "P"))
                        {
                            RestarReservaConceptos(dbContext, idUsuario, comboConceptose.Conceptos1.IDConcepto, cantidad * comboConceptose.Cantidad, "P");
                        }
                    }
                }
            }
        }

        public static void ValidarReservaStock(ACHEEntities dbContext, int idUsuario, int idConcepto, decimal cantidad)
        {
            //se obtiene el inventario de reservas
            var invenario =
                dbContext.Inventarios.FirstOrDefault(i => i.IDUsuario == idUsuario && i.Activo && i.PorDefectoReservas);
            if (invenario == null)
                throw new Exception(string.Format("No existe un inventario de reservas para el usuario {0}", idUsuario));

            var aux = dbContext.Conceptos.Where(x => x.IDConcepto == idConcepto).FirstOrDefault();
            if (aux != null)
            {
                /*decimal stockActual=0;
                if (aux.Tipo != "C")
                {
                    stockActual =
                        dbContext.StockConceptos.Where(x => x.IDConcepto == idConcepto && x.IDInventario == invenario.IDInventario)
                            .Sum(x => x.Stock);
                }
                else*/
                var stockActual = InventariosCommon.ObtenerStockTotal(aux).StockConReservas;
                /*
                var reserva =
                    dbContext.StockConceptosReserva.FirstOrDefault(
                        r => r.IDUsuario == idUsuario && r.IDConcepto == idConcepto);

                if (reserva != null)
                    stockActual = stockActual - reserva.Reserva;*/
                if (stockActual < cantidad)
                    throw new Exception(string.Format("No alcanza el stock para reservar el producto {0}, inventario {1}", idConcepto, invenario.IDInventario));
            }
        }

        private static void SumarReservaConceptos(ACHEEntities dbContext, int idUsuario, int idConcepto, int idInventario, decimal cantidad, string tipo)
        {

            string key = idUsuario + "-" + idConcepto + "-" + idInventario;
            lock (string.Intern(key))
            {
                var reserva = dbContext.StockConceptosReserva.FirstOrDefault(r => r.IDUsuario == idUsuario && r.IDConcepto == idConcepto);
                if (reserva == null)
                {
                    reserva = new StockConceptosReserva();
                    reserva.IDUsuario = idUsuario;
                    reserva.IDConcepto = idConcepto;
                    reserva.Reserva = cantidad;
                    dbContext.StockConceptosReserva.Add(reserva);
                }
                else
                {
                    reserva.Reserva += cantidad;
                }
                GuardarHistorialStock(idUsuario, idConcepto, idInventario, TipoMovimientoStock.Reserva, null, null, cantidad, null, reserva.Reserva, null);

               
            }
        }

        private static void RestarReservaConceptos(ACHEEntities dbContext, int idUsuario, int idConcepto, decimal cantidad, string tipo)
        {
            string key = idUsuario + "-" + idConcepto + "-";
            lock (string.Intern(key))
            {
                var reserva = dbContext.StockConceptosReserva.FirstOrDefault(r => r.IDUsuario == idUsuario && r.IDConcepto == idConcepto);
                if (reserva != null)
                {
                    //Si se saca de la reserva entonces se saca del stock
                    reserva.Reserva -= cantidad;
                    if (reserva.Reserva < 0)
                        reserva.Reserva = 0;

                    GuardarHistorialStock(idUsuario, idConcepto, 0, TipoMovimientoStock.Reserva, null, null, (cantidad * -1), null, reserva.Reserva, null);

                    
                }
            }

        }

        public static bool TieneInventarioReservas(int usuarioId)
        {
            using (var dbContext = new ACHEEntities())
            {
                return dbContext.Inventarios.Any(i => i.IDUsuario == usuarioId && i.Activo && i.PorDefectoReservas);
            }
        }

        public static List<ConceptosReservadosViewModel> ObtenerReservasDeProducto(int idUsuario, int idConcepto, string tipo)
        {
            List<ConceptosReservadosViewModel> list = new List<ConceptosReservadosViewModel>();

            using (var dbContext = new ACHEEntities())
            {
                var ordenes = dbContext.OrdenVentaDetalle.Include("OrdenVenta").Include("OrdenVenta.Integraciones")
                    .Include("OrdenVenta.Personas")
                    .Where(x => x.OrdenVenta.IDUsuario == idUsuario && x.OrdenVenta.Estado != "Finalizada" && x.OrdenVenta.Estado != "Cancelada"
                    && x.IDConcepto == idConcepto && x.StockReservado).ToList();

                foreach (var item in ordenes)
                {
                    list.Add(new ConceptosReservadosViewModel()
                    {
                        ID = item.IDOrdenVenta,
                        NroOrden = item.OrdenVenta.NroOrden,
                        Cantidad = item.Cantidad,
                        Fecha = item.OrdenVenta.FechaCreacion,//.ToString("dd/MM/yyyy"),
                        Origen = !item.OrdenVenta.IDIntegracion.HasValue ? "Manual" : item.OrdenVenta.Integraciones.Tipo,
                        Cliente = (!item.OrdenVenta.IDIntegracion.HasValue || item.OrdenVenta.ApodoComprador == "") ? item.OrdenVenta.Personas.RazonSocial : item.OrdenVenta.ApodoComprador,
                        PuedeEliminar = true
                    });
                }

                if (tipo != "C")
                {
                    //Agrego las reservas de dicho producto que fueron vendidas por combos
                    var combosList = dbContext.ComboConceptos.Include("Conceptos").Where(x => x.IDConceptoItem == idConcepto).ToList();
                    foreach (var combo in combosList)
                    {
                        ordenes = dbContext.OrdenVentaDetalle.Include("OrdenVenta").Include("OrdenVenta.Integraciones")
                        .Include("OrdenVenta.Personas")
                        .Where(x => x.OrdenVenta.IDUsuario == idUsuario && x.OrdenVenta.Estado != "Finalizada" && x.OrdenVenta.Estado != "Cancelada"
                        && x.IDConcepto == combo.IDConceptoCombo && x.StockReservado).ToList();

                        foreach (var item in ordenes)
                        {
                            list.Add(new ConceptosReservadosViewModel()
                            {
                                ID = item.IDOrdenVenta,
                                NroOrden = item.OrdenVenta.NroOrden,
                                Cantidad = item.Cantidad * combo.Cantidad,
                                Fecha = item.OrdenVenta.FechaCreacion,//.ToString("dd/MM/yyyy"),
                                Origen = !item.OrdenVenta.IDIntegracion.HasValue ? "Manual" : item.OrdenVenta.Integraciones.Tipo,
                                Cliente = (!item.OrdenVenta.IDIntegracion.HasValue || item.OrdenVenta.ApodoComprador == "") ? item.OrdenVenta.Personas.RazonSocial : item.OrdenVenta.ApodoComprador,
                                PuedeEliminar = false,
                                ComboDesc = combo.Conceptos.Codigo,// + "-" + combo.Conceptos.Nombre,
                                IDCombo = combo.IDConceptoCombo,
                            });
                        }
                    }
                }

            }

            return list.OrderBy(x => x.Fecha).ToList();
        }

        #endregion

        #region Reporte Stock

        public static ResultadosRptRnkViewModel ReporteStockProductosInventarios(int idInventario, string estado, int page, int pageSize)
        {
            try
            {
                if (HttpContext.Current.Session["CurrentUser"] != null)
                {
                    var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                    using (var dbContext = new ACHEEntities())
                    {
                        var results = dbContext.RptStock.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                        if (estado != string.Empty)
                            results = results.Where(x => x.Estado == estado);
                        ////if (idInventario > 0 && idInventario != null)
                        ////    results = results.Where(x => x.IDInventario == idInventario);

                        page--;
                        ResultadosRptRnkViewModel resultado = new ResultadosRptRnkViewModel();
                        resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                        resultado.TotalItems = results.Count();

                        var aux = results.GroupBy(x => new { x.Codigo, x.Nombre, x.PrecioUnitario })
                         .Select(g => new
                         {
                             Codigo = g.Key.Codigo.ToUpper(),
                             Nombre = g.Key.Nombre,
                             Stock = g.Sum(x => x.Stock),
                             CostoInterno = g.Sum(x => x.CostoInterno),
                             PrecioUnitario = g.Key.PrecioUnitario,
                             Valor = g.Sum(x => x.PrecioUnitario * x.Stock),
                             //Inventario = 
                         }).ToList();


                        var list = aux.OrderByDescending(x => x.Valor).Skip(page * pageSize).Take(pageSize).ToList()
                            .Select(x => new RptRnkViewModel()
                            {
                                Valor1 = x.Nombre,
                                Valor2 = x.Codigo,
                                Cantidad = x.Stock.ToMoneyFormat(4),
                                CostoInterno = x.CostoInterno.HasValue ? x.CostoInterno.Value.ToMoneyFormat(4) : "",
                                Precio = x.PrecioUnitario.ToMoneyFormat(4),
                                Total = x.Valor.ToMoneyFormat(4)
                            });
                        resultado.Items = list.ToList();

                        return resultado;
                    }
                }
                else
                    throw new Exception("Por favor, vuelva a iniciar sesión");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        public static string Exportar(int idInventario, string estado)
        {
            if (HttpContext.Current.Session["CurrentUser"] != null)
            {
                var usu = (WebUser)HttpContext.Current.Session["CurrentUser"];

                string fileName = "Stock_" + usu.IDUsuario + "_";
                string path = "~/tmp/";
                try
                {
                    DataTable dt = new DataTable();
                    using (var dbContext = new ACHEEntities())
                    {
                        var results = dbContext.RptStock.Where(x => x.IDUsuario == usu.IDUsuario).AsQueryable();
                        if (estado != string.Empty)
                            results = results.Where(x => x.Estado == estado);

                        var aux = results.GroupBy(x => new { x.Codigo, x.Nombre, x.PrecioUnitario })
                         .Select(g => new
                         {
                             Codigo = g.Key.Codigo.ToUpper(),
                             Nombre = g.Key.Nombre,
                             Stock = g.Sum(x => x.Stock),
                             CostoInterno = g.Sum(x => x.CostoInterno),
                             PrecioUnitario = g.Key.PrecioUnitario,
                             Valor = g.Sum(x => x.PrecioUnitario * x.Stock)
                         }).ToList();

                        dt = aux.OrderByDescending(x => x.Valor).ToList().Select(x => new
                        {
                            Nombre = x.Nombre,
                            Codigo = x.Codigo,
                            Stock = x.Stock.ToString(),
                            CostoInterno = Convert.ToInt32(x.CostoInterno),
                            PrecioUnitario = x.PrecioUnitario,
                            Valor = x.Valor
                        }).ToList().ToDataTable();

                    }

                    if (dt.Rows.Count > 0)
                        CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                    else
                        throw new Exception("No se encuentran datos para los filtros seleccionados");

                    return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
                }
                catch (Exception e)
                {
                    var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                    throw e;
                }
            }
            else
                throw new Exception("Por favor, vuelva a iniciar sesión");
        }

        public static string ExportarMovimientos(int idOrigen, int idDestino, string concepto, string periodo, string fechaDesde, string fechaHasta, int idUsuario)
        {
            string fileName = "MovEntreDepositos_" + idUsuario + "_";
            string path = "~/tmp/";
            try
            {
                DataTable dt = new DataTable();

                var results = InventariosCommon.ReporteMovimientosEntreDepositos(idUsuario, idOrigen, idDestino, concepto, periodo, fechaDesde, fechaHasta, 1, 10000);
                dt = results.Items.Select(x => new
                {
                    Fecha = x.Fecha,
                    Origen = x.Origen,
                    Destino = x.Destino,
                    Codigo = x.Codigo,
                    Producto = x.Producto,
                    Cantidad = x.Cantidad,
                    Descripcion = x.Descripcion
                }).ToList().ToDataTable();


                if (dt.Rows.Count > 0)
                    CommonModel.GenerarArchivo(dt, HttpContext.Current.Server.MapPath(path) + Path.GetFileName(fileName), fileName);
                else
                    throw new Exception("No se encuentran datos para los filtros seleccionados");

                return (path + fileName + DateTime.Now.ToString("yyyyMMdd") + ".xlsx").Replace("~", "");
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        #endregion

        #region Reporte Mov Entre Depositos

        public static ResultadosRptMovDepositosModelViewModel ReporteMovimientosEntreDepositos(int idUsuario, int idOrigen, int idDestino, string concepto, string periodo, string fechaDesde, string fechaHasta, int page, int pageSize)
        {
            try
            {

                using (var dbContext = new ACHEEntities())
                {
                    var results = dbContext.RptMovimientosEntreDepositos.Where(x => x.IDUsuario == idUsuario).AsQueryable();
                    if (idOrigen > 0)
                        results = results.Where(x => x.IDInventarioOrigen == idOrigen);
                    if (idDestino > 0)
                        results = results.Where(x => x.IDInventarioDestino == idDestino);
                    if (concepto != string.Empty)
                        results = results.Where(x => x.Codigo.Contains(concepto) || x.Nombre.Contains(concepto));

                    switch (periodo)
                    {
                        case "30":
                            fechaDesde = DateTime.Now.AddDays(-30).ToShortDateString();
                            break;
                        case "15":
                            fechaDesde = DateTime.Now.AddDays(-15).ToShortDateString();
                            break;
                        case "7":
                            fechaDesde = DateTime.Now.AddDays(-7).ToShortDateString();
                            break;
                        case "1":
                            fechaDesde = DateTime.Now.AddDays(-1).ToShortDateString();
                            break;
                        case "0":
                            fechaDesde = DateTime.Now.ToShortDateString();
                            break;
                    }

                    if (!string.IsNullOrWhiteSpace(fechaDesde))
                    {
                        DateTime dtDesde = DateTime.Parse(fechaDesde);
                        results = results.Where(x => x.Fecha >= dtDesde);
                    }
                    if (!string.IsNullOrWhiteSpace(fechaHasta))
                    {
                        DateTime dtHasta = DateTime.Parse(fechaHasta + " 23:59:59 pm");
                        results = results.Where(x => x.Fecha <= dtHasta);
                    }

                    page--;
                    ResultadosRptMovDepositosModelViewModel resultado = new ResultadosRptMovDepositosModelViewModel();
                    resultado.TotalPage = ((results.Count() - 1) / pageSize) + 1;
                    resultado.TotalItems = results.Count();

                    var list = results.OrderBy(x => x.Fecha).Skip(page * pageSize).Take(pageSize).ToList()
                    .Select(x => new RptMovDepositosModelViewModel()
                    {
                        Fecha = x.Fecha.ToString("dd/MM/yyyy"),
                        Origen = x.Origen,
                        Destino = x.Destino,
                        Descripcion = x.Descripcion,
                        Producto = x.Nombre,
                        Codigo = x.Codigo,
                        Cantidad = x.Cantidad.ToString("#0.00").Replace(",00", "")
                    });
                    resultado.Items = list.ToList();

                    return resultado;
                }
            }
            catch (Exception e)
            {
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["BasicLogError"]), msg, e.ToString());
                throw e;
            }
        }

        //public static string Exportar(int idUsuario, int idOrigen, int idDestino, string periodo, string fechaDesde, string fechaHasta)
        //{

        //}

        #endregion
    }
}

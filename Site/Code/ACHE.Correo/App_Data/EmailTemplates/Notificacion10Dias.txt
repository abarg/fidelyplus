﻿<table cellpadding="0" cellspacing="0" border="0" align="center" style="background:#f9f9f9;border-collapse:collapse;line-height:100%!important;margin:0;padding:0;width:100%!important" bgcolor="#f9f9f9">
  <tbody>
	<tr>
		<td>
			<table style="border-collapse: collapse; margin: auto; max-width: 635px; min-width: 320px; width: 100%">
				<tbody>
					<tr>
						<td valign="top">
							<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color: #c0c0c0; font-family: 'Helvetica Neue',Arial,sans-serif; font-size: 13px; line-height: 26px; margin: 0 auto 26px; width: 100%">
							</table>
						</td>
					</tr>
					<tr>
						<td valign="top" style="padding: 0 20px">

							<table cellpadding="0" cellspacing="0" border="0" align="center" style="background-clip: padding-box; border-collapse: collapse; border-radius: 3px; color: #545454; font-family: 'Helvetica Neue',Arial,sans-serif; font-size: 13px; line-height: 20px; margin: 0 auto; width: 100%">
								<tbody>
									<tr>
										<td valign="top">
											<table cellpadding="0" cellspacing="0" border="0" style="border: none; border-collapse: separate; font-size: 1px; height: 2px; line-height: 3px; width: 100%">
												<tbody>
													<tr>
														<td valign="top" style="background: #5d998e; border: none; font-family: 'Helvetica Neue',Arial,sans-serif; width: 100%" bgcolor="#95d0c3">&nbsp;</td>
													</tr>
												</tbody>
											</table>
											<table cellpadding="0" cellspacing="0" border="0" style="background-clip: padding-box; border-collapse: collapse; border-color: #dddddd; border-radius: 0 0 3px 3px; border-style: solid; border-width: 0 1px 1px; width: 100%">
												<tbody>

													<tr>
														<td>
															<div style="border-bottom: solid thin #ddd; padding: 15px 40px 40px; background-color: #fff;">
																<div style="width: 100%; text-align: center;">
																	<p style="font-size:15px;font-family: 'Helvetica Neue',Arial,sans-serif;color:#8a6d3b; border-radius: 3px; border: solid thin #f3e2bf;background-color: #fcf8e3; padding: 8px; margin-bottom: 40px; margin-top: 0;">
																		<img src="https://app.contabilium.com/images/mails/exclamation.png" style="position: relative; top: 2px;">Tu plan venció <strong>hoy</strong> <a href="https://app.contabilium.com/modulos/seguridad/elegir-plan.aspx" style="color: #17a08c; margin-left: 5px;">Subí de plan ahora</a>
																	</p>
																</div>
																<a href="http://www.contabilium.com"><img src="https://app.contabilium.com/images/mails/simplifica.png" style="margin: auto; display: table;"></a>
															</div>
														</td>
													</tr>

													<tr>
														<td style="background: white; background-clip: padding-box; border-radius: 0 0 3px 3px; color: #525252; font-family: 'Helvetica Neue',Arial,sans-serif; font-size: 15px; line-height: 22px; overflow: hidden; padding: 20px 40px 30px" bgcolor="white">
															<div style="margin-bottom: 16px; margin-top: 0; padding-top: 0; text-align: center!important" align="center">
																<h1 style="font-size:20px;margin-top:10px;color:#525252;font-family: 'Helvetica Neue',Arial,sans-serif;margin-bottom: 0px;">
																	¡Hola <USUARIO>!
																</h1>
																<p style="font-size:17px;font-family: 'Helvetica Neue',Arial,sans-serif;color:#525252;line-height: 1.5;">
																	<strong>Hoy finalizó tu Plan de Prueba en Contabilium.</strong>
																</p>
															</div>	
															<div style="margin-bottom: 16px; margin-top: 0; padding-top: 0; text-align: left!important" align="left">
																<p style="font-size:15px;font-family: 'Helvetica Neue',Arial,sans-serif;color:#525252;line-height: 1.5;">
																	<strong>¿Cómo adquiero el sistema?</strong>
																</p>
															</div>	
															<div style="margin-bottom: 16px; margin-top: 0; padding-top: 0; text-align: left!important" align="left">
																<p style="font-size:15px;font-family: 'Helvetica Neue',Arial,sans-serif;color:#525252;line-height: 1.5;">
																	Podés elegir uno de <a href="https://app.contabilium.com/modulos/seguridad/elegir-plan.aspx" style="color:#17a08c">nuestros Planes</a> y contratarlo con Tarjeta de crédito, dinero de MercadoPago, Transferencia/Depósito bancario o efectivo (en un Pago Fácil, Rapipago, red Link o Provincia Net):
																</p>
															</div>

															<div style="margin-bottom: 16px; margin-top: 0; padding-top: 0; text-align: center!important" align="center">
																<img src="https://app.contabilium.com/images/mails/planes.png" style="margin-button: 17px 0; max-width: 100%">
															</div>
															<div align="left">
																<p style="font-size:15px;font-family: 'Helvetica Neue',Arial,sans-serif;color:#525252;line-height: 1.5;">
																	<br />
																	Si necesitás que un Asesor de Ventas te ayude a elegir el Plan más adecuado para vos o tenés consultas pendientes, llamanos: (011) 5263-2062 int. 104.<br>También podés completar un Test orientador:
																</p>
															</div>	
															<div align="center">
																<div style="border-radius: 5px;padding:10px; width:350px;font-size: 13px;font-family: 'Helvetica Neue',Arial,Helvetica;text-align: center;color: #fff;font-weight: 700;padding-left: 5px;padding-right: 5px;word-break: break-word;line-height: 21px;background-color: #bf3157;background-clip: padding-box;border-collapse: collapse;">
																	<a href="https://goo.gl/forms/3mDEyCzqZNnXvtkM2" style="color:#fff;text-decoration: none;">COMPLETAR EL TEST: “¿QUÉ PLAN ELIJO?”</a>
																</div>
															</div>
														</td>
													</tr>

													<tr>
														<td style="padding: 15px 0 20px; background: #fff;">
															<div align="center" style="background: url(https://app.contabilium.com/images/mails/fondoverde.png) center no-repeat; background-size: cover; padding: 10px 0;">
																<p style="font-size:15px;font-family: 'Helvetica Neue',Arial,sans-serif;color:white;line-height: 1.5; padding:30px; margin: 0;">
																	<strong style="font-size: 18px; margin-bottom: 15px; display: inline-block;">¿Querés pagar con un 20% OFF?</strong>
																	<br>Cualquiera de nuestros Planes tienen el 20% de Descuento si se los contrata por 12 meses. Aprovechá!
																</p><div align="center">
																<div style="">
																	<a href="https://app.contabilium.com/modulos/seguridad/elegir-plan.aspx" style="color:#fff;text-decoration: none; font-weight: 700; letter-spacing: .5px; border-radius: 5px;padding:10px; width:240px;font-size: 12px;font-family: 'Helvetica Neue',Arial,Helvetica;text-align: center;color: #fff;padding-left: 5px;padding-right: 5px;word-break: break-word;line-height: 24px;background-color: #bf3157;background-clip: padding-box;border-collapse: collapse; border: solid thin #fff; display: inline-block;">QUIERO 20% DE DESCUENTO</a>
																</div>
																</div>
																<p></p>
																<br>
															</div>
														</td>
													</tr>





													<tr>
														<td style="background-color: #fff; padding: 0 40px;">
															<div style="margin-bottom: 16px; margin-top: 0; padding-top: 0; text-align: left!important" align="left">
																<p style="font-size:15px;font-family: 'Helvetica Neue',Arial,sans-serif;color:#525252;line-height: 1.5;">
																	<strong>Esperamos que quieras seguir utilizando el Sistema de Gestión y Contabilidad Online más elegido por los los Emprendedores, Profesionales y PyMEs del país; y que día a día optimice tus tareas como Solución Integral.</strong>
																</p>
															</div>
														</td>
													</tr>

													<tr>
														<td>
															<div style="border-top: solid thin #ddd; background-color: #fff; padding: 10px;">
																<div style="display: flex;">
																	<div style="display: inline-flex;"><img src="https://app.contabilium.com/images/mails/tipa.png" style="width: 140px; height: 140px; vertical-align: middle; margin: 15px 0;"></div>
																	<div style="display: inline-flex; padding-right: 20px;">
																		<p style="font-size: 14px; color: #525252;">
																			<span style=" display: block; margin-bottom: 15px;">Si tenés alguna duda, inquietud o sugerencia, contactanos mediante:</span>
																			<span style=" display: block; margin-bottom: 3px;"><b><img src="https://app.contabilium.com/images/mails/tel.png" style="position: relative; top: 3px;"> Tel: (011) 5263-2062</b>  (L a V de 9:00 a 18:00 hs.)</span>
																			<span style=" display: block; margin-bottom: 15px;"><b><img src="https://app.contabilium.com/images/mails/email.png" style="position: relative; top: 3px;"> Email: <a href="mailto:comercial@contabilium.com" style="color: #525252 !important;">comercial@contabilium.com</a></b></span>
																			o hablá con nosotros en el momento a través del <b>Chat Online</b> de la web para ayudarte a optimizar el sistema de acuerdo a tus necesidades!
																		</p>
																	</div>
																</div>
															</div>
														</td>
													</tr>
													
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>

						</td>
					</tr>
					<tr>
						<td valign="top" height="20"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
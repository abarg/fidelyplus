﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACHE.Negocio;
using ACHE.Model;
using System.Net.Mail;
using System.Collections.Specialized;
using System.Configuration;
using ACHE.Model.ViewModels;
using ACHE.Extensions;
using ACHE.Model.Negocio;
using ACHE.Negocio.Common;

namespace ACHE.EnviosMail {
    class Program {
        private static List<TotalesNotificacionesCorreoViewModel> TOTALNOTIFICACIONES { get; set; }
        private static Dictionary<int, SmtpSettings> SmtpSettings { get; set; }

        private static SmtpSettings smtpPropio = new SmtpSettings() { TieneAddinMails = false };

        static void Main(string[] args) {
            try {


                SmtpSettings = AddinsCommon.ObtenerSmtpSettings(ConfigurationManager.AppSettings["Addins.MailsCustomizable"]);
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var ejecutarServicio = ConfigurationManager.AppSettings["EjecutarServicios"];

                //BasicLog.AppendToFile(path, "****** Inicia proceso de envio de email ******", "");
                TOTALNOTIFICACIONES = new List<TotalesNotificacionesCorreoViewModel>();
                //BasicLog.AppendToFile(path, "Inicia función ObtenerUsuariosPrueba", "");
                EnviarMailsPeriodoPrueba();
                //BasicLog.AppendToFile(path, "Termina función ObtenerUsuariosPrueba", "");

                //BasicLog.AppendToFile(path, "Inicia función ObtenerUsuariosPagos", "");
                EnviarRecordatoriosPagos();
                //BasicLog.AppendToFile(path, "Termina función ObtenerUsuariosPagos", "");

                //BasicLog.AppendToFile(path, "Inicia función ObtenerUsuariosInduccion", "");
                EnviarMailsInduccion();
                //BasicLog.AppendToFile(path, "Termina función ObtenerUsuariosInduccion", "");

                //BasicLog.AppendToFile(path, "Inicia función ObtenerUsuariosCheques", "");
                EnviarMailsCheques();
                //BasicLog.AppendToFile(path, "Termina función ObtenerUsuariosCheques", "");

                //BasicLog.AppendToFile(path, "Inicia función ObtenerUsuariosAvisosVencimientos", "");
                //if (ConfigurationManager.AppSettings["Ejecutar.AvisosVencimientos"] == "1")
                EnviarAvisosVencimientos();
                //BasicLog.AppendToFile(path, "Termina función ObtenerUsuariosAvisosVencimientos", "");

                //BasicLog.AppendToFile(path, "Inicia función EnviarEmailAdministrador", "");
                EnviarAvisosSuscripciones();
                
                EnviarEmailAdministrador();
                //BasicLog.AppendToFile(path, "Termina función EnviarEmailAdministrador", "");

                BasicLog.AppendToFile(path, "****** Termina proceso de envio de email ******", "");
            }
            catch (Exception ex) {

                throw;
            }
        }

        #region Envio de corres usuarios registrados

        private static void EnviarMailsPeriodoPrueba() {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var listUsuarios = dbContext.UsuariosPlanesView.Where(x => x.Activo && x.IDPlan == 6 && x.AntiguedadMeses <= 1).OrderByDescending(x => x.FechaAlta).ToList();
                    var subject = string.Empty;

                    foreach (var usu in listUsuarios) {
                        var fechaActual = DateTime.Now.Date;
                        var dias = (fechaActual - usu.FechaAlta.Date).Days;


                        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                        var emailUsuario = (string.IsNullOrWhiteSpace(usu.EmailAlertas)) ? usu.Email : usu.EmailAlertas;
                        totalesNotificaciones.Usuarios = "IDUSUARIO : " + usu.IDUsuario + ", EMAIL: " + emailUsuario + ", RAZON SOCIAL: " + usu.RazonSocial;


                        //if (usu.SetupRealizado) {
                        if (dias == 2 && ConfigurationManager.AppSettings["Ejecutar.2Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.2Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion2Dias, subject, "Notificacion2Dias");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion2Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 4 && ConfigurationManager.AppSettings["Ejecutar.4Dias"] == "1") {//NUEVO
                            //if (usu.FechaUltLogin.Date > usu.FechaAlta.Date) {
                            subject = ConfigurationManager.AppSettings["Subject.4Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion4Dias, subject, "Notificacion4Dias");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion4Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                            //}
                            //else {
                            //    subject = ConfigurationManager.AppSettings["Subject.4DiasSinUso"];
                            //    var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion4DiasSinUso, subject, "Notificacion4DiasSinUso");
                            //    totalesNotificaciones.TotalCorreos++;
                            //    if (send)
                            //        totalesNotificaciones.TotalCorreosEnviados++;
                            //    totalesNotificaciones.TipoNotificacion = "Notificacion4DiasSinUso";
                            //    TOTALNOTIFICACIONES.Add(totalesNotificaciones);

                            //}
                        }
                        else if (dias == 6 && ConfigurationManager.AppSettings["Ejecutar.6Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.6Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion6Dias, subject, "Notificacion6Dias");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion6Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        //else if (dias == 7 && ConfigurationManager.AppSettings["Ejecutar.7Dias"] == "1") {
                        //    subject = ConfigurationManager.AppSettings["Subject.7Dias"];
                        //    var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion7Dias, subject, "Notificacion7Dias");
                        //    totalesNotificaciones.TotalCorreos++;
                        //    if (send)
                        //        totalesNotificaciones.TotalCorreosEnviados++;
                        //    totalesNotificaciones.TipoNotificacion = "Notificacion7Dias";
                        //    TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        //}
                        else if (dias == 8 && ConfigurationManager.AppSettings["Ejecutar.8Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.8Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion8Dias, subject, "Notificacion8Dias");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion8Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 9 && ConfigurationManager.AppSettings["Ejecutar.9Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.9Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion9Dias, subject, "Notificacion9Dias");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion9Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 10 && ConfigurationManager.AppSettings["Ejecutar.10Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.10Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion10Dias, subject, "Notificacion10Dias");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion10Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 25 && ConfigurationManager.AppSettings["Ejecutar.ReactivarPDP"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.ReactivarPDP"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionReactivar, subject, "NotificacionReactivarPDP");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionReactivarPDP";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        //else if (dias == 35 && ConfigurationManager.AppSettings["Ejecutar.Capacitacion"] == "1")
                        //{//NUEVO
                        //    subject = ConfigurationManager.AppSettings["Subject.Capacitacion"];
                        //    var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionCapacitacion, subject, "NotificacionCapacitacion");
                        //    totalesNotificaciones.TotalCorreos++;
                        //    if (send)
                        //        totalesNotificaciones.TotalCorreosEnviados++;
                        //    totalesNotificaciones.TipoNotificacion = "NotificacionCapacitacion";
                        //    TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        //}
                        //else if (dias == 13 && ConfigurationManager.AppSettings["Ejecutar.VeiticincoDias"] == "1")
                        //{
                        //    subject = ConfigurationManager.AppSettings["Subject.VeiticincoDias"];
                        //    var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionVeiticincoDias, subject, "NotificacionVeiticincoDias");
                        //    totalesNotificaciones.TotalCorreos++;
                        //    if (send)
                        //        totalesNotificaciones.TotalCorreosEnviados++;
                        //    totalesNotificaciones.TipoNotificacion = "NotificacionVeiticincoDias";
                        //    TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        //}
                        /*}
                        else {
                            if (dias == 2 && ConfigurationManager.AppSettings["Ejecutar.SinSetup"] == "1") { // NUEVO
                                subject = ConfigurationManager.AppSettings["Subject.SinSetup"];
                                var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionSinSetup, subject, "NotificacionSinSetup");
                                totalesNotificaciones.TotalCorreos++;
                                if (send)
                                    totalesNotificaciones.TotalCorreosEnviados++;
                                totalesNotificaciones.TipoNotificacion = "NotificacionSinSetup";
                                TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                            }*/
                        //else if (dias == 12 && ConfigurationManager.AppSettings["Ejecutar.ReactivarPDP"] == "1")
                        //{//NUEVO
                        //    subject = ConfigurationManager.AppSettings["Subject.ReactivarPDP"];
                        //    var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionReactivar, subject, "NotificacionReactivarPDP");
                        //    totalesNotificaciones.TotalCorreos++;
                        //    if (send)
                        //        totalesNotificaciones.TotalCorreosEnviados++;
                        //    totalesNotificaciones.TipoNotificacion = "NotificacionReactivarPDP";
                        //    TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        //}
                        else if (dias == 15 && ConfigurationManager.AppSettings["Ejecutar.Capacitacion"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.Capacitacion"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionCapacitacion, subject, "NotificacionCapacitacion");
                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionCapacitacion";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        //}
                    }
                }
            }
            catch (Exception e) {
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
            }
        }

        //Envio recordatorios de pago
        private static void EnviarRecordatoriosPagos() {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var dtHoy = DateTime.Now.Date;
                    var dt48 = DateTime.Now.AddDays(-5).Date;


                    var listUsuarios = dbContext.UsuariosPlanesView.Where(x => x.Activo && x.IDPlan != 6 && x.IDPlan != 1 && x.FechaFinPlan >= dtHoy).OrderByDescending(x => x.FechaFinPlan).ToList();
                    var subject = string.Empty;
                    foreach (var usu in listUsuarios) {
                        //var fechaActual = DateTime.Now.Date;
                        var dias = (Convert.ToDateTime(usu.FechaFinPlan).Date - dtHoy).Days;

                        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                        var emailUsuario = (string.IsNullOrWhiteSpace(usu.EmailAlertas)) ? usu.Email : usu.EmailAlertas;
                        totalesNotificaciones.Usuarios = "IDUSUARIO : " + usu.IDUsuario + ", EMAIL: " + emailUsuario + ", RAZON SOCIAL: " + usu.RazonSocial;

                        if (dias == 5 && ConfigurationManager.AppSettings["Ejecutar.5DiasVencimiento"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.5DiasVencimiento"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion5DiasVencimiento, subject, "Notificacion5DiasVencimiento");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion5DiasVencimiento";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 1 && ConfigurationManager.AppSettings["Ejecutar.1DiasVencimiento"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.1DiasVencimiento"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion1DiasVencimiento, subject, "Notificacion1DiasVencimiento");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion1DiasVencimiento";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 0 && ConfigurationManager.AppSettings["Ejecutar.0DiasVencimiento"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.0DiasVencimiento"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.Notificacion0DiasVencimiento, subject, "Notificacion0DiasVencimiento");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "Notificacion0DiasVencimiento";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                    }
                    //Envio mail a los que se les venció el pago hace 2 o 7 dias.
                    //listUsuarios = dbContext.UsuariosPlanesView.Where(x => x.Activo && x.SetupRealizado && x.IDPlan != 6 && x.IDPlan != 1 && x.FechaFinPlan < dtHoy && x.FechaFinPlan > dt48).OrderByDescending(x => x.FechaFinPlan).ToList();
                    //foreach (var usu in listUsuarios) {
                    //    var dias = (Convert.ToDateTime(usu.FechaFinPlan).Date - dtHoy).Days;
                    //    if ((dias == -2 || dias == -7) && ConfigurationManager.AppSettings["Ejecutar.Vencidos"] == "1") {
                    //        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                    //        subject = ConfigurationManager.AppSettings["Subject.Vencido"];
                    //        var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionVencido, subject, "NotificacionVencido");

                    //        totalesNotificaciones.TotalCorreos++;
                    //        if (send)
                    //            totalesNotificaciones.TotalCorreosEnviados++;
                    //        totalesNotificaciones.TipoNotificacion = "NotificacionVencido";
                    //        TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                    //    }
                    //}
                }
            }
            catch (Exception e) {
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
            }
        }

        //ENVIO DE MAILS PARA INDUCCIÓN
        private static void EnviarMailsInduccion() {
            try {
                using (var dbContext = new ACHEEntities()) {
                    var dtHoy = DateTime.Now.Date;
                    //var dt48 = DateTime.Now.AddDays(-2).Date;

                    var listUsuarios = dbContext.UsuariosPlanesView.Where(x => x.Activo && x.IDPlan != 6 && x.IDPlan != 1 && x.MesesPagos == 1).OrderByDescending(x => x.FechaFinPlan).ToList();
                    var subject = string.Empty;
                    foreach (var usu in listUsuarios) {
                        //var fechaActual = DateTime.Now.Date;
                        //if (usu.MesesPagos == 1)
                        //{
                        var dias = (dtHoy - Convert.ToDateTime(usu.FechaInicioPlan).Date).Days;

                        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                        var emailUsuario = (string.IsNullOrWhiteSpace(usu.EmailAlertas)) ? usu.Email : usu.EmailAlertas;
                        totalesNotificaciones.Usuarios = "IDUSUARIO : " + usu.IDUsuario + ", EMAIL: " + emailUsuario + ", RAZON SOCIAL: " + usu.RazonSocial;

                        if (dias == 0 && ConfigurationManager.AppSettings["Ejecutar.PrimerPago"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.PrimerPago"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionPrimerPago, subject, "NotificacionPrimerPago");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionPrimerPago";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 7 && ConfigurationManager.AppSettings["Ejecutar.PrimerPago7Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.PrimerPago7Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionPrimerPago7Dias, subject, "NotificacionPrimerPago7Dias");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionPrimerPago7Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 15 && ConfigurationManager.AppSettings["Ejecutar.PrimerPago15Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.PrimerPago15Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionPrimerPago15Dias, subject, "NotificacionPrimerPago15Dias");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionPrimerPago15Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 20 && ConfigurationManager.AppSettings["Ejecutar.PrimerPago20Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.PrimerPago20Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionPrimerPago20Dias, subject, "NotificacionPrimerPago20Dias");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionPrimerPago20Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if (dias == 27 && ConfigurationManager.AppSettings["Ejecutar.PrimerPago27Dias"] == "1") {//NUEVO
                            subject = ConfigurationManager.AppSettings["Subject.PrimerPago27Dias"];
                            var send = EnvioCorreos(dbContext, usu, EmailTemplateApp.NotificacionPrimerPago28Dias, subject, "NotificacionPrimerPago28Dias");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionPrimerPago28Dias";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        //}
                    }
                }
            }
            catch (Exception e) {
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
            }
        }

        private static bool EnvioCorreos(ACHEEntities dbContext, UsuariosPlanesView usu, EmailTemplateApp template, string subject, string tipoNotificacion) {
            var usuario = "";
            if (!string.IsNullOrEmpty(usu.RazonSocial)) {
                if (usu.RazonSocial != "undefined")
                    usuario = usu.RazonSocial;
            }

            bool send = false;
            try {
                var emailUsuario = (string.IsNullOrWhiteSpace(usu.EmailAlertas)) ? usu.Email : usu.EmailAlertas;
                ListDictionary replacements = new ListDictionary();
                replacements.Add("<USUARIO>", usuario);

                #region replacementsActividad

                if (tipoNotificacion == "NotificacionReactivarPDP" || tipoNotificacion == "Notificacion4Dias" || tipoNotificacion == "NotificacionPrimerPago"
                    || tipoNotificacion == "NotificacionPrimerPago7Dias" || tipoNotificacion == "NotificacionPrimerPago20Dias") {
                    var block = "display:block";
                    var none = "display:none";
                    var actividad = usu.Actividad ?? "";

                    if (actividad.ToLower() == "vendo productos") {
                        replacements.Add("<DISPLAYGENERAL>", none);
                        replacements.Add("<DISPLAYPRODUCTOS>", block);
                        replacements.Add("<DISPLAYSERVICIOS>", none);
                        replacements.Add("<DISPLAYCONTADORES>", none);
                    }
                    else if (actividad.ToLower() == "vendo servicios") {
                        replacements.Add("<DISPLAYGENERAL>", none);
                        replacements.Add("<DISPLAYPRODUCTOS>", none);
                        replacements.Add("<DISPLAYSERVICIOS>", block);
                        replacements.Add("<DISPLAYCONTADORES>", none);
                    }
                    else if (actividad.ToLower() == "soy contador") {
                        replacements.Add("<DISPLAYGENERAL>", none);
                        replacements.Add("<DISPLAYPRODUCTOS>", none);
                        replacements.Add("<DISPLAYSERVICIOS>", none);
                        replacements.Add("<DISPLAYCONTADORES>", block);
                    }
                    else {
                        replacements.Add("<DISPLAYGENERAL>", block);
                        replacements.Add("<DISPLAYPRODUCTOS>", none);
                        replacements.Add("<DISPLAYSERVICIOS>", none);
                        replacements.Add("<DISPLAYCONTADORES>", none);
                    }
                }

                #endregion

                #region nombresPlanes

                if (tipoNotificacion == "Notificacion0DiasVencimiento" || tipoNotificacion == "Notificacion1DiasVencimiento" || tipoNotificacion == "Notificacion5DiasVencimiento") {
                    replacements.Add("<NOMBREPLAN>", usu.PlanActual);
                }

                #endregion

                send = EmailCommon.SendMessage(usu.IDUsuario, template, replacements, emailUsuario, subject, ConfigurationManager.AppSettings["Email.AtencionCliente"], "CONTABILIUM", smtpPropio);

                if (send) {
                    var mensaje = "Envios de correo finalizados correctamente";
                    GuardarObservacionesUsuario(dbContext, mensaje, false, "", usu.IDUsuario, tipoNotificacion);
                    return true;
                }
                else
                    throw new Exception("El correo no fue enviado");
            }
            catch (Exception ex) {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                GuardarObservacionesUsuario(dbContext, "Error usu: " + usuario, true, msg, usu.IDUsuario, tipoNotificacion);

                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                BasicLog.AppendToFile(path, msg, ex.ToString());
            }

            return false;
        }

        #endregion

        #region Envio de correo recordatorios de cheques

        private static void EnviarMailsCheques() {
            try {
                var fechaActual = DateTime.Now.Date;

                using (var dbContext = new ACHEEntities()) {
                    //la vista solo se usa para esto

                    #region Cheques de terceros a vencer

                    var listCheques = dbContext.AlertasChequesView.GroupBy(x => new { x.IDUsuario, x.FechaCobro }).ToList();
                    foreach (var item in listCheques) {

                        var dias = (Convert.ToDateTime(item.FirstOrDefault().FechaCobro).Date - fechaActual).Days;
                        var descripcion = string.Empty;

                        foreach (var itemdetalle in item) {
                            //descripcion += "Emisor: " + itemdetalle.Emisor + " - Cheque nro: " + itemdetalle.Numero + "<br><br>";
                            descripcion += "<tr>";
                            descripcion += "<td>" + itemdetalle.Emisor + "</td>";
                            descripcion += "<td>" + itemdetalle.Numero + "</td>";
                            descripcion += "<td style='text-align:right'>$" + itemdetalle.Importe.ToMoneyFormat(2) + "</td>";
                            descripcion += "<td>" + itemdetalle.FechaCobro.Value.ToString("dd/MM/yyyy") + "</td>";
                            descripcion += "<td>" + itemdetalle.FechaVencimiento.Value.ToString("dd/MM/yyyy") + "</td>";
                            descripcion += "</tr>";
                        }

                        var razonSocial = (item.FirstOrDefault().RazonSocial != "") ? item.FirstOrDefault().RazonSocial : "usuario";
                        var emailUsuario = (string.IsNullOrWhiteSpace(item.FirstOrDefault().EmailAlertas)) ? item.FirstOrDefault().Email : item.FirstOrDefault().EmailAlertas;

                        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                        totalesNotificaciones.Usuarios = "IDUSUARIO : " + item.FirstOrDefault().IDUsuario + ", EMAIL: " + emailUsuario + ", RAZON SOCIAL: " + item.FirstOrDefault().RazonSocial;// +", Datos del o los cheques: " + descripcion;


                        if (dias == 1 && ConfigurationManager.AppSettings["Ejecutar.ChequesACobrar"] == "1") {
                            var send = EnvioCorreosCheques(dbContext, emailUsuario, item.FirstOrDefault().IDUsuario, razonSocial, EmailTemplateApp.AlertasCheque, "Cheques disponibles para depositar", "Los siguientes cheques se encuentran disponibles para su depósito el día de mañana:", descripcion, "NotificacionChequesACobrar");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionChequesACobrar";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                        else if ((dias == -1 || dias == -5 || dias == -10) && ConfigurationManager.AppSettings["Ejecutar.ChequesAVencer"] == "1") {
                            var send = EnvioCorreosCheques(dbContext, emailUsuario, item.FirstOrDefault().IDUsuario, razonSocial, EmailTemplateApp.AlertasCheque, "Cheques pendientes de depósito", "Los siguientes cheques aún no han sido depositados:", descripcion, "NotificacionChequesAVencer");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionChequesAVencer";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                    }

                    #endregion

                    #region cheques propios a debitar

                    //Debito de cheques propios
                    var fechaDebito = DateTime.Now.AddDays(-1).Date;
                    var listChequesADebitar = dbContext.Cheques
                        .Include("Usuarios")
                        .Where(x => x.EsPropio && x.FechaCobro >= fechaDebito && x.Usuarios.Activo && !x.Usuarios.FechaBaja.HasValue)
                        .GroupBy(x => new { x.IDUsuario, x.FechaCobro }).ToList();

                    foreach (var item in listChequesADebitar) {
                        var dias = (Convert.ToDateTime(item.FirstOrDefault().FechaCobro).Date - fechaDebito).Days;
                        var descripcion = string.Empty;

                        foreach (var itemdetalle in item) {
                            //descripcion += "Emisor: " + itemdetalle.Emisor + " - Cheque nro: " + itemdetalle.Numero + "<br><br>";
                            descripcion += "<tr>";
                            // descripcion += "<td>" + itemdetalle.Emisor + "</td>";
                            descripcion += "<td>" + itemdetalle.Numero + "</td>";
                            descripcion += "<td style='text-align:right'>$" + itemdetalle.Importe.ToMoneyFormat(2) + "</td>";
                            descripcion += "<td>" + itemdetalle.FechaCobro.Value.ToString("dd/MM/yyyy") + "</td>";
                            descripcion += "<td>" + itemdetalle.FechaVencimiento.Value.ToString("dd/MM/yyyy") + "</td>";
                            descripcion += "</tr>";
                        }

                        var razonSocial = (item.FirstOrDefault().Usuarios.RazonSocial != "") ? item.FirstOrDefault().Usuarios.RazonSocial : "usuario";
                        var emailUsuario = (string.IsNullOrWhiteSpace(item.FirstOrDefault().Usuarios.EmailAlertas)) ? item.FirstOrDefault().Usuarios.Email : item.FirstOrDefault().Usuarios.EmailAlertas;

                        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                        totalesNotificaciones.Usuarios = "IDUSUARIO : " + item.FirstOrDefault().IDUsuario + ", EMAIL: " + emailUsuario + ", RAZON SOCIAL: " + item.FirstOrDefault().Usuarios.RazonSocial;// +", Datos del o los cheques: " + descripcion;


                        if (dias == 1)// && ConfigurationManager.AppSettings["Ejecutar.ChequesACobrar"] == "1")
                        {
                            var send = EnvioCorreosCheques(dbContext, emailUsuario, item.FirstOrDefault().IDUsuario, razonSocial, EmailTemplateApp.AlertasChequePropio, "Cheques a ser debitados", "Los siguientes cheques se encuentran disponibles para su débito desde mañana:", descripcion, "NotificacionChequesADebitar");

                            totalesNotificaciones.TotalCorreos++;
                            if (send)
                                totalesNotificaciones.TotalCorreosEnviados++;
                            totalesNotificaciones.TipoNotificacion = "NotificacionChequesADebitar";
                            TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                        }
                    }

                    #endregion

                }
            }
            catch (Exception e) {
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
            }
        }

        private static bool EnvioCorreosCheques(ACHEEntities dbContext, string email, int idUsuario, string razonSocial, EmailTemplateApp template, string subject, string titulo, string descripcion, string tipoNotificacion) {
            bool send = false;
            try {
                ListDictionary replacements = new ListDictionary();
                replacements.Add("<USUARIO>", razonSocial);
                replacements.Add("<TITULO>", titulo);
                replacements.Add("<DESCRIPCION>", descripcion);

                send = EmailCommon.SendMessage(idUsuario, template, replacements, email, subject, ConfigurationManager.AppSettings["Email.Alertas"], "CONTABILIUM", smtpPropio);

                if (send) {
                    var mensaje = "Envio de la notificación de cheques finalizado correctamente";
                    GuardarObservacionesUsuario(dbContext, mensaje, false, "", idUsuario, tipoNotificacion);
                    return true;
                }
                else
                    throw new Exception("El correo no fue enviado");
            }
            catch (Exception ex) {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                GuardarObservacionesUsuario(dbContext, "Error usu: " + razonSocial, true, msg, idUsuario, tipoNotificacion);
            }

            return false;
        }

        #endregion

        #region Envio de correo avisos de suscripciones

        private static void EnviarAvisosSuscripciones() {
            try {

                if (ConfigurationManager.AppSettings["Ejecutar.AvisosDebitos"] == "1")
                    using (var dbContext = new ACHEEntities()) {
                        var dtHoy = DateTime.Now;
                        var dtDiaDebito = dtHoy.AddDays(3).Day;

                        var suscripcionesActivas = dbContext.Suscripciones.Include("SuscripcionesPersonas").Include("SuscripcionesPagos").Include("Usuarios").Include("Usuarios.PlanesPagos").Include("Usuarios.UsuarioAddins")
                            .Where(x => x.Estado == "A" //SUSCRIPCIONES ACTIVAS
                        && !x.Usuarios.FechaBaja.HasValue && x.Usuarios.Activo//que no tenga fecha baja y activo true
                        && (x.Usuarios.PlanesPagos.Any(
                            y => y.FechaInicioPlan <= dtHoy && (y.FechaFinPlan ?? dtHoy) >= dtHoy && y.IDPlan != 6)
                            ||
                            x.Usuarios.TieneDebitoAutomatico)//QUE EL USUARIO TENGA UN PLAN ACTIVO QUE NO SEA DE PRUEBA O QUE ESTE ADHERIDO AL DEBITO AUTOMATICO EN SU CUENTA
                        && x.Usuarios.UsuarioAddins.Any(y => y.Activo && y.FechaDesde <= dtHoy && (y.FechaHasta ?? dtHoy) >= dtHoy && y.Addins.Nombre == "DebitosAutomaticos") //QUE TENGA EL ADDIN DE DEBITOS ACTIVO
                        && x.FechaInicio <= dtHoy && (x.FechaFin ?? dtHoy) >= dtHoy//QUE LA SUSCRIPCION ESTE ACTIVA A HOY
                        && x.DiaVencimiento == dtDiaDebito//VERIFICO QUE FALTEN 3 DIAS PARA LA FECHA DE VENCIMIENTO
                        ).ToList();

                        foreach (var sa in suscripcionesActivas) {
                            var personasActivas = sa.SuscripcionesPersonas.Where(x => x.Estado == "A").ToList();

                            int resto = 0;
                            bool enviarNotificacionBool = false;
                            string tipoFrecuencia = "";
                            string precioConIva = "";
                            string debitoTotalConIva = "";

                            //if (sa.FechaFin == null)
                            //    sa.FechaFin = dtHoy;
                            //if (sa.FechaInicio <= dtHoy && sa.FechaFin >= dtHoy && (sa.DiaVencimiento-1) == dtHoyDia) {
                            precioConIva = (sa.PrecioUnitario + ((sa.PrecioUnitario * sa.Iva) / 100)).ToMoneyFormat(2);
                            debitoTotalConIva = (decimal.Parse(precioConIva) * personasActivas.Count()).ToMoneyFormat(2);
                            if (personasActivas.Count() > 0) {
                                switch (sa.Frecuencia) {
                                    case "A":
                                        resto = verificarFrecuencia(12, dtHoy.Year, sa.FechaInicio.Year, dtHoy.Month, sa.FechaInicio.Month);
                                        if (resto == 0) {
                                            enviarNotificacionBool = true;
                                            tipoFrecuencia = "Anual";
                                        }
                                        break;
                                    case "S":
                                        resto = verificarFrecuencia(6, dtHoy.Year, sa.FechaInicio.Year, dtHoy.Month, sa.FechaInicio.Month);
                                        if (resto == 0) {
                                            enviarNotificacionBool = true;
                                            tipoFrecuencia = "Semestral";
                                        }
                                        break;
                                    case "T":
                                        resto = verificarFrecuencia(3, dtHoy.Year, sa.FechaInicio.Year, dtHoy.Month, sa.FechaInicio.Month);
                                        if (resto == 0) {
                                            enviarNotificacionBool = true;
                                            tipoFrecuencia = "Trimestral";
                                        }
                                        break;
                                    case "M":
                                        resto = verificarFrecuencia(1, dtHoy.Year, sa.FechaInicio.Year, dtHoy.Month, sa.FechaInicio.Month);
                                        if (resto == 0) {
                                            enviarNotificacionBool = true;
                                            tipoFrecuencia = "Mensual";
                                        }
                                        break;
                                }

                                if (enviarNotificacionBool)//ENVIO MAIL A CLIENTE INFORMANDO QUE PRONTO SE VAN A GENERAR LOS PAGOS
                                    EnvioCorreosSuscripciones(dbContext, sa.IDUsuario, sa.Usuarios.Email, sa.Usuarios.RazonSocial, sa.Nombre, sa.DiaVencimiento.ToString(), tipoFrecuencia, precioConIva, debitoTotalConIva, personasActivas.Count(), "NotificacionDebitosARealizar", ConfigurationManager.AppSettings["Subject.AvisosDebitos"]);
                            }
                            //}
                        }
                    }
            }
            catch (Exception e) {
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
            }
        }

        private static bool EnvioCorreosSuscripciones(ACHEEntities dbContext, int idUsuario, string emailUsuario, string razonSocial, string nombreSuscripcion, string diaVencimiento, string tipoFrecuencia, string precioUnitario, string totalDebitos, int cantPersonas, string tipoNotificacion, string subject) {
            bool send = false;
            try {
                ListDictionary replacements = new ListDictionary();
                replacements.Add("<RAZONSOCIAL>", razonSocial);
                replacements.Add("<PRECIOUNITARIO>", precioUnitario);
                replacements.Add("<TOTAL>", totalDebitos);
                replacements.Add("<CANTDEBITOS>", cantPersonas);
                replacements.Add("<MENSAJE>", "En el día de mañana se generarán las solicitudes de pago por la suscripción <b>" + tipoFrecuencia + "</b> con nombre <b>" + nombreSuscripcion + "</b> y con fecha de vencimiento <b>" + diaVencimiento + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year + "</b>. ");

                send = EmailCommon.SendMessage(idUsuario, EmailTemplateApp.NotificacionPrevioDA, replacements, emailUsuario, subject, ConfigurationManager.AppSettings["Email.Alertas"], "CONTABILIUM", smtpPropio);

                if (send) {
                    var mensaje = "Envio de la notificación de debitos a realizar por suscripciones finalizo correctamente";
                    GuardarObservacionesUsuario(dbContext, mensaje, false, "", idUsuario, tipoNotificacion);
                    return true;
                }
                else
                    throw new Exception("El correo no fue enviado");
            }
            catch (Exception ex) {
                var msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                GuardarObservacionesUsuario(dbContext, "Error usu: " + razonSocial, true, msg, idUsuario, tipoNotificacion);
            }

            return false;
        }
        #endregion

        #region Envio de correo a los clientes segun AvisosVencimientos

        //mails que se envian de avisos de vencimiento
        private static void EnviarAvisosVencimientos() {
            using (var dbContext = new ACHEEntities()) {
                var listaAvisos = dbContext.AvisosVencimientosView.Where(x => x.IDPlan >= 4 && x.Saldo > 0 && x.Activa == true).ToList();

                foreach (var item in listaAvisos) {
                    var dia = (item.FechaVencimiento - DateTime.Now.Date).Days;


                    //Modo envio=1 es antes. O= despues.
                    if (item.ModoDeEnvio == "1" && dia == item.CantDias) {
                        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                        totalesNotificaciones.Usuarios = "IDUSUARIO : " + item.IDUsuario + ", EMAIL: " + item.EmailUsuario + ", RAZON SOCIAL: " + item.RazonSocialUsuario + ", TIPO de ALERTA: AvisosVencimiento: " + item.TipoAlerta;
                        totalesNotificaciones.TotalCorreos++;

                        var mensaje = item.Mensaje.ReplaceAll("\n", "<br/>");
                        var send = EnvioCorreosAvisosVencimientos(dbContext, item, EmailTemplateApp.AvisosVencimientoConFoto, item.Asunto, mensaje, "AvisosVencimiento: " + item.TipoAlerta);

                        if (send)
                            totalesNotificaciones.TotalCorreosEnviados++;
                        totalesNotificaciones.TipoNotificacion = "AvisosVencimiento" + item.TipoAlerta;
                        TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                    }
                    else if (item.ModoDeEnvio == "0" && dia == -item.CantDias) {
                        var totalesNotificaciones = new TotalesNotificacionesCorreoViewModel();
                        totalesNotificaciones.Usuarios = "IDUSUARIO : " + item.IDUsuario + ", EMAIL: " + item.EmailUsuario + ", RAZON SOCIAL: " + item.RazonSocialUsuario + ", TIPO de ALERTA: AvisosnVecimiento: " + item.TipoAlerta;
                        totalesNotificaciones.TotalCorreos++;
                        var mensaje = item.Mensaje.ReplaceAll("\n", "<br/>");
                        var send = EnvioCorreosAvisosVencimientos(dbContext, item, EmailTemplateApp.AvisosVencimientoConFoto, item.Asunto, mensaje, "AvisosVencimiento: " + item.TipoAlerta);

                        if (send)
                            totalesNotificaciones.TotalCorreosEnviados++;
                        totalesNotificaciones.TipoNotificacion = "AvisosVencimiento" + item.TipoAlerta;
                        TOTALNOTIFICACIONES.Add(totalesNotificaciones);
                    }
                }
            }
        }

        private static bool EnvioCorreosAvisosVencimientos(ACHEEntities dbContext, AvisosVencimientosView avisos, EmailTemplateApp template, string subject, string descripcion, string tipoNotificacion) {
            try {
                var usu = dbContext.UsuariosView.Where(x => x.IDUsuario == avisos.IDUsuario).FirstOrDefault();
                if (usu != null) {
                    ListDictionary replacements = new ListDictionary();
                    replacements.Add("<CLIENTE>", avisos.RazonSocialPersona);
                    replacements.Add("<COMPROBANTE>", avisos.Tipo + "-" + avisos.Punto.ToString("#0000") + "-" + avisos.Numero.ToString("#00000000"));
                    replacements.Add("<SALDO>", avisos.Saldo.ToString("N2"));
                    replacements.Add("<NOTIFICACION>", descripcion);
                    //replacements.Add("<LINKPAGO>", descripcion);
                    replacements.Add("LINKPAGO", "https://clientes.contabilium.com/public/factura?comprobante=" + Cryptography.Encrypt(avisos.IDComprobante.ToString(), true));
                    replacements.Add("<LOGOEMPRESA>", "/files/usuarios/" + usu.Logo);

                    //var emailFrom = avisos.EmailUsuario;

                    //if (SmtpSettings != null && SmtpSettings.ContainsKey(avisos.IDUsuario))
                    //    emailFrom = SmtpSettings[avisos.IDUsuario].EmailFrom;

                    var send = EmailCommon.SendMessage(avisos.IDUsuario, template, replacements, avisos.EmailPersona,
                        avisos.EmailUsuario, avisos.EmailUsuario, subject, (usu.EmailFrom ?? usu.RazonSocial), (SmtpSettings != null && SmtpSettings.ContainsKey(avisos.IDUsuario)) ? SmtpSettings[avisos.IDUsuario] : null);

                    if (send)
                        return true;
                    else
                        throw new Exception("El correo no fue enviado");
                }
                else
                    return false;
            }
            catch (Exception ex) {
                var msg = tipoNotificacion;
                msg += ex.InnerException != null ? " - " + ex.InnerException.Message : " - " + ex.Message;

                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                BasicLog.AppendToFile(path, msg, ex.ToString());
                return false;
            }
        }

        #endregion

        private static void EnviarEmailAdministrador() {
            try {
                using (var dbContext = new ACHEEntities()) {
                    ListDictionary replacements = new ListDictionary();

                    var listaFinal = TOTALNOTIFICACIONES.GroupBy(x => x.TipoNotificacion).Select(x => new TotalesNotificacionesCorreoViewModel() {
                        TipoNotificacion = x.FirstOrDefault().TipoNotificacion,
                        TotalCorreos = x.Sum(y => y.TotalCorreos),
                        TotalCorreosEnviados = x.Sum(y => y.TotalCorreosEnviados),
                        Usuarios = String.Join(" <br/> ", x.Select(n => n.Usuarios))
                    }).ToList();

                    string htmlMails = "";

                    foreach (var item in listaFinal) {
                        htmlMails += "<u><b>Notificacion" + item.TipoNotificacion + "</b></u>";
                        htmlMails += "<br />Cantidad de correos a enviar: " + item.TotalCorreos.ToString();
                        htmlMails += "<br />Cantidad de correos enviados: " + item.TotalCorreosEnviados.ToString();
                        htmlMails += "<br />Usuarios:<br /><br/> " + item.Usuarios;
                        htmlMails += "<br /><br />";
                    }
                    replacements.Add("<MAILS>", htmlMails);

                    var correoAdmin = ConfigurationManager.AppSettings["Email.Admin"];
                    EmailCommon.SendMessage(EmailTemplateApp.NotificacionAdministrador.ToString(), replacements, correoAdmin, "Resultados de los envios de correo");
                }
            }
            catch (Exception e) {
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
            }
        }

        private static void GuardarObservacionesUsuario(ACHEEntities dbContext, string mensaje, bool tieneError, string mensajeError, int idUSuario, string tipoNotificacion) {
            try {
                var ObsUsu = new ObservacionesUsuario();
                ObsUsu.Fecha = DateTime.Now;
                ObsUsu.Observacion = mensaje;
                ObsUsu.TieneError = tieneError;
                ObsUsu.MensajeError = mensajeError;
                ObsUsu.IDUsuario = idUSuario;
                ObsUsu.TipoNotificacion = tipoNotificacion;
                ObsUsu.Proyecto = "ACHE.EnviosMails";

                dbContext.ObservacionesUsuario.Add(ObsUsu);
                dbContext.SaveChanges();
            }
            catch (Exception e) {
                var path = ConfigurationManager.AppSettings["NotificacionesCorreoLogError"];
                var msg = e.InnerException != null ? e.InnerException.Message : e.Message;
                BasicLog.AppendToFile(path, msg, e.ToString());
            }
        }

        private static int verificarFrecuencia(int frecuencia, int year, int inicioyear, int month, int iniciomonth) {
            var difMeses = ((year - inicioyear) * 12) + month - iniciomonth;
            var resto = difMeses % frecuencia;
            return resto;
        }

    }
}